<?php
class Blog_model extends CI_Model
{   
	// blog model
	function select_all_blog()
	{
		$this->db->order_by('id','DESC');
		$data= $this->db->get("tbl_blog");
		return $data->result();		
	}
	function select_blog_by_catid($id)
	{
		$this->db->where('cat_id', $id);
		$this->db->order_by('id','DESC');
		$data= $this->db->get('tbl_blog');
		return $data->result();
	}
	function select_blog_by_uid($id)
	{
		$this->db->where('uid', $id);
		$this->db->order_by('id','DESC');
		$data= $this->db->get('tbl_blog');
		return $data->result();
	}
	function select_blog_byid($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_blog');
		return $data->result();
	}
	function insert($data)
	{
		$result= $this->db->insert('tbl_blog', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_blog',$data);
	}
	
	function delete_blog($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_blog');
	}
	
	// blog catecory model
	function select_all_blogcat()
	{
		$data= $this->db->get("tbl_blogcategory");
		return $data->result();		
	}	
	function select_all_active_blogcat()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_blogcategory");
		return $data->result();		
	}	
	function select_blog_cat_byid($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_blogcategory');
		return $data->result();
	}
	
	function insert_cat($data)
	{
		$result= $this->db->insert('tbl_blogcategory', $data);
		return $result;
	}
	
	function update_cat($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_blogcategory',$data);
	}
	
	function delete_cat($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_blogcategory');
	}
	
	
}
?>