<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo base_url('index.php/admin/dashboard'); ?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li> 
					<li>
                        <a href="<?php echo base_url('index.php/banner/listing/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Banner</i>
						</a>
                    </li>
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Users<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/admin/userList'); ?>">Users List</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/adduser'); ?>">Add New</a>
                            </li>
                        </ul>
                    </li>  
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#cat">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Category<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="cat" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/category/listing'); ?>">List Category</a>
                            </li>
                        </ul>
                    </li>  
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#home">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Product<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="home" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/product/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Product Listing
								</a>
                            </li>							                           
                        </ul>
                    </li>
					 
					                     <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#advertisement">
                            <i class="fa fa-fw fa-arrows-v"></i>Manage Colors<i class="fa fa-fw fa-caret-down"></i>
                        </a>
                        <ul id="advertisement" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/color/listing'); ?>">
                                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                    Colors
                                </a>
                            </li>                                                      
                        </ul>
                    </li>
                   
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#cms">
                            <i class="fa fa-fw fa-arrows-v"></i>Manage Coupon Code<i class="fa fa-fw fa-caret-down"></i>
                        </a>
                        <ul id="cms" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/offer/listing'); ?>">
                                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                    Manage Coupon Listing
                                </a>
                            </li>
                            
                        </ul>
                    </li>

					 
					 <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#cms">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Cms<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="cms" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/cms/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Page Listing
								</a>
                            </li>
							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#faq">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Clients<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="faq" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/faq/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Clients Listing
								</a>
                            </li>							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#farmer">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Farmer<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="farmer" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/farmer/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Farmer Listing
								</a>
                            </li>							
                        </ul>
                    </li>
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#blog">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Blog<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="blog" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/blog/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Blog Listing
								</a>
                            </li>							
                        </ul>
                    </li>
					<li>
                        <a href="<?php echo base_url('index.php/block/listing/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Static Block</i>
						</a>
                    </li>
					
                </ul>
            </div>