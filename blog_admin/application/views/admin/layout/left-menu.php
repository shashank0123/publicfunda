<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo base_url('index.php/admin/dashboard'); ?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>	


<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#blog">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Blog<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="blog" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/blogcategory/cat_listing'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Manage Category
								</a>
                            </li>
							<li>
								<a href="<?php echo base_url('index.php/blog/listing/'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Manage Blog
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('index.php/admin/edit_blogbanner/'); ?>">
									<i class="fa fa-arrow-circle-right"></i> Blog Banner
								</a>
							</li>
                        </ul>
                    </li>					
			
                        </ul>
                    </li>
					
            </div>