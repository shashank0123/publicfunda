<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Edit Blog Banner</h2>
                         <form role="form" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>URL</label>
                                <input type="text" class="form-control" placeholder="category Title" value="<?php echo $EDITBLOG[0]->blogimageurl; ?>" name="blogimageurl">
                            </div>
							<div class="form-group">
								<?php if(!empty($EDITBLOG[0]->blogimage))	{	?>
                                <img src="<?php echo base_url('uploads/blog/'.$EDITBLOG[0]->blogimage); ?>" width="80">
								<?php }	?>
								<input type="hidden" name="oldimage" value="<?php echo $EDITBLOG[0]->blogimage; ?>">								
                            </div>
							<div class="form-group">
                                <label>Image</label>
                                <input type="file" class="form-control"  name="blogimage">
                            </div>
                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
