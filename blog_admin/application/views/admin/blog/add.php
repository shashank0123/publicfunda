<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Add New blog</h2>
                         <form role="form" method="post" enctype="multipart/form-data">
							
							<div class="form-group">
                                <label> Select Category</label>
                                <select class="form-control" name="cat_id" required>
									<?php foreach($CATEGORY as  $cat){ ?>
										<option value="<?php echo $cat->id; ?>"><?php echo $cat->title; ?></option>
									<?php } ?>
								</select>
                            </div>
                            <div class="form-group">
                                <label> Title</label>
                                <input type="text" class="form-control" placeholder="category Title" name="title" required>
                            </div>
							<div class="form-group">
                                <label>Image</label>
                                <input type="file" class="form-control"  name="image" >
                            </div>	
							<div class="form-group">
                                <label> Content</label>
                                <textarea class="form-control" name="content"></textarea>
                            </div>
                            <div name="status" class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control" required>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>                            

                            <button type="submit" name="savedata" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script>
function checkpancard()
{
	if(document.getElementById("nopancard").checked==true)
	{
		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');
		document.getElementById("pancard").style.backgroundColor = "#ccc";
		document.getElementById("pancard").readOnly = true;
	}
	else
	{
		document.getElementById("pancard").style.backgroundColor = "#fff";
		document.getElementById("pancard").readOnly = false;
	}	
}

</script>
</body>

</html>
