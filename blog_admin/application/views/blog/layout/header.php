<?php error_reporting(0); ?>
<!DOCTYPE HTML>
<html>
<head>
<title>Blog </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Business_Blog " />

<link href="<?php echo base_url('assets/blog/'); ?>/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url('assets/blog/'); ?>/css/style.css" rel='stylesheet' type='text/css' />	
<script src="<?php echo base_url('assets/blog/'); ?>/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url('assets/blog/'); ?>/js/bootstrap.min.js"></script>
<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 10px 28px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
}

.button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
}
</style>
</head>
<body>
	<!--start-main-->
           <div class="header">
		        <div class="header-top">
			        <div class="container">
						<div class="logo">
							<a target="_blank" title="Logo" href="<?php echo base_url(''); ?>"><img alt="Logo" src="http://www.publicfunda.com/assets/front/images/logo.png<?php //echo base_url('assets/front/images/logo-blue.png'); ?>" width="100"></a>
						</div>
						
						
						<div class="search">
							<form>
								<input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
								<input type="submit" value="">
							</form>
						</div>
						<div class="social">
							<ul>
								<li><a href="#" class="facebook"> </a></li>
								<li><a href="#" class="facebook twitter"> </a></li>
								<li><a href="#" class="facebook chrome"> </a></li>
								<li><a href="#" class="facebook in"> </a></li>
								<li><a href="#" class="facebook beh"> </a></li>
								<li><a href="#" class="facebook vem"> </a></li>
								<li><a href="#" class="facebook yout"> </a></li>
							</ul>
							<a href="http://www.publicfunda.com/index.php/user/login" class="button button1">Sign in</a>
							<a href="http://www.publicfunda.com/index.php/user/signup" class="button button1">Register</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			
<!--head-bottom-->
<div class="head-bottom">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
		<?php $blod_cat = $this->blog_model->select_all_active_blogcat(); ?>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
		    <li class="active_1"><a href="<?php echo base_url('blog'); ?>">Home</a></li>  
			<?php foreach($blod_cat as $menu){ ?>
            <li class="active_1"><a href="<?php echo base_url('index.php/blog/display/'.$menu->id); ?>"><?php echo $menu->title; ?></a></li>           
			<?php } ?>	
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>
<!--head-bottom-->
</div>	