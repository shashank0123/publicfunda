<?php 
class Invoicecontent extends CI_Controller
{	
	function edit()
	{
		$args = func_get_args();
		
		if(isset($_POST['updateData']))
		{
			
			if($_FILES['logo']['name']!="")
			{
				$logo = time().'_'.$_FILES['logo']['name'];
				$file_tmp = $_FILES['logo']['tmp_name'];
				$path = 'uploads/invoice/'.$logo;
				move_uploaded_file($file_tmp,$path);
			}
			else
			{
				$logo = $_POST['logo_old'];
			}
			
			if($_FILES['signature']['name']!="")
			{
				$signature = time().'_'.$_FILES['signature']['name'];
				$file_tmp = $_FILES['signature']['tmp_name'];
				$path = 'uploads/invoice/'.$signature;
				move_uploaded_file($file_tmp,$path);
			}
			else
			{
				$signature = $_POST['signature_old'];
			}
			
			$edata['logo']= $logo;
			$edata['from_address']= $this->input->post('from_address');
			$edata['declaration']= $this->input->post('declaration');
			$edata['terms_condition']= $this->input->post('terms_condition');
			$edata['signature']= $signature;
			$this->invoicecontent_model->updateBlockData(0,$edata);		
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/invoicecontent/edit/1');	
		}	
		$data['invoicedata'] = $this->invoicecontent_model->getBlockById(0);
		$this->load->view('admin/invoicecontent/edit',$data);
	}
	
}
