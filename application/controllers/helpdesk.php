<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Helpdesk extends CI_Controller 
{
	
	function listing()
	{
		$this->load->model('helpdesk_model');
		
		if(isset($_POST['checkHelpDesk']))
		{
			$sdate = $this->input->post('startDate');
			$edate = $this->input->post('endDate');
			if(!empty($sdate) && !empty($edate))
			{						
			   //$date_1 = 
				$this->db->where('queryDate >=',date('Y-m-d',strtotime($sdate)));
				$this->db->where('queryDate <=',date('Y-m-d',strtotime($edate)));
			}	
		}	
		
		$data['helpdesk']= $this->helpdesk_model->getAllQuery();
		$this->load->view('admin/helpdesk/list',$data);
	}
	function department()
	{
		$this->load->model('helpdesk_model');
		$data['helpdesk']= $this->helpdesk_model->getAllDepartment();
		$this->load->view('admin/helpdesk/list-department',$data);
	}
	function add_department()
	{
		$args = func_get_args();
		$this->load->model('helpdesk_model');		
		if(isset($_POST['updateData']))
		{
			$data['title'] = $this->input->post('title');
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['status'] = $this->input->post('status');
			$this->helpdesk_model->insertDepartmentData($data);
			redirect('index.php/helpdesk/department/');
		}		
		$this->load->view('admin/helpdesk/add-department');
	}
	function edit_department()
	{
		$args = func_get_args();
		$this->load->model('helpdesk_model');		
		if(isset($_POST['updateData']))
		{
			$data['title'] = $this->input->post('title');
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['status'] = $this->input->post('status');
			$this->helpdesk_model->updateDepartmentData($args[0],$data);
			redirect('index.php/helpdesk/department/');
		}	
		
		$data['helpdesk']= $this->helpdesk_model->selectDepartmentByID($args[0]);
		$this->load->view('admin/helpdesk/edit-department',$data);
	}
	
	function query()
	{
		$this->load->model('helpdesk_model');
		$args = func_get_args();
		
		if(isset($_POST['submitComment']))
		{
			$adata['query_id'] = $args[0];
			$adata['comment'] = $this->input->post('comment');
			$adata['comment_type'] = 'admin';
			$adata['uid'] = $this->input->post('depatyment');
			$adata['comment_time'] = time();
			$adata['comment_date'] = date('Y-m-d');	
			$this->helpdesk_model->getAllQueryComment($adata);
			//$edata['status'] = 	'open';
			//$this->helpdesk_model->updateQueryData($args[0],$edata);
			redirect('index.php/helpdesk/query/'.$args[0]);
		}	
		
		if(isset($_POST['updateSatatus']))
		{
			$adata['status'] = $_POST['status'];	
			$this->helpdesk_model->updateQueryData($args[0],$adata);
			redirect('index.php/helpdesk/query/'.$args[0]);
		}	
		
		$data['query']= $this->helpdesk_model->selectQueryByID($args[0]);
		$data['query_comments'] = $this->helpdesk_model->selectHelpdeskCommentsByQueryID($args[0]);
		$this->load->view('admin/helpdesk/helpdesk-query',$data);		
	}
	
	function index()
	{	
		error_reporting(0);
		$this->load->model('helpdesk_model');
		$user_login_id = $this->session->userdata('USERID');
		//echo $user_login_id;
		if(isset($_POST['submitQuery']))
		{
			if(!empty($_POST['type']) && !empty($_POST['subject']) && !empty($_POST['query']))
			{
				$opendata = $this->helpdesk_model->checkCommentOpen($user_login_id,$_POST['type']);
				//print_r($_POST);
			        //print_r($opendata); die();
				if(count($opendata)>0)
				{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><b>you already have lodge complaint into same department please enter your remarks into existing open status complaint id.</b></div>');
					redirect('index.php/helpdesk/index');
				}	

if($_FILES['file']['name']!="")
				{
					$file = time().'_'.$_FILES['file']['name'];
					$file_tmp = $_FILES['file']['tmp_name'];
					$path = 'uploads/helpdesk/'.$file;
					move_uploaded_file($file_tmp,$path);
				}
				else
				{ 
					$file = ""; 
				}

				$insData['uid'] = $user_login_id;
				$insData['type'] = $this->input->post('type');
				$insData['subject'] = $this->input->post('subject');
				$insData['query'] = $this->input->post('query');
				$insData['file'] = $file;
				$insData['status'] = 'pending'; //$this->input->post('status');
				$insData['queryTime'] = time();
				$insData['queryDate'] = date('Y-m-d');
				$insID = $this->helpdesk_model->insertData($insData);
				$department = $this->helpdesk_model->selectDepartmentByID($this->input->post('type'));
				if($insID)
				{
					$this->email->set_newline("\r\n");
					$this->email->set_mailtype("html");
					//$this->email->to('vikaspal7623@gmail.com');
					$this->email->to($department[0]->email);
					// $this->email->from('vikaspal7623@gmail.com', 'samridhbharat.biz');
					$this->email->subject('Public Funda Help Desk');
					$msg = "Ticket ID - MOTIF".$insID.",<br>"; 
					$msg .= "USERID - ".$user_login_id.",<br>"; 
					$msg .= "Query Type - ".$_POST['type'].",<br>"; 	
					$msg .= "Subject - ".$_POST['subject'].",<br>"; 
					$msg .= "Query - ".$_POST['query'].",<br>";
					$msg .= "PublicFunda.com Support Team";
					$this->email->message($msg);
					$this->email->send();
				}	
				
				
				$this->session->set_flashdata('message','<div class="alert alert-success">Your query has been successfully submitted.</div>');
				redirect('index.php/helpdesk/index');
			}
			else
			{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>error! </strong> fill all field</div>');
				redirect('index.php/helpdesk/index');
			}	
		}	
		
		$data['queryData'] = $this->helpdesk_model->selectQueryByUID($user_login_id);
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$data["popup"]= $this->popup_model->selectPopupByID(13);
		$this->load->view('front/helpdesk',$data);
	}
	
	public function view()
	{
		$this->load->model('helpdesk_model');
		$user_login_id = $this->session->userdata('USERID');
		$args = func_get_args();
		if(count($args)==0){ redirect('index.php/helpdesk/index');	}
		
		if(isset($_POST['submitComment']))
		{
			$adata['query_id'] = $args[0];
			$adata['comment'] = $this->input->post('comment');
			$adata['comment_type'] = 'user';
			$adata['uid'] = $user_login_id;
			$adata['comment_time'] = time();
			$adata['comment_date'] = date('Y-m-d');	
			$this->helpdesk_model->getAllQueryComment($adata);
			
			$edata['status'] = 	'open';
			$this->helpdesk_model->updateQueryData($args[0],$edata);
			redirect('index.php/helpdesk/view/'.$args[0]);
		}	
		
		$data['queryData'] = $this->helpdesk_model->selectQueryByID($args[0]);
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$data['query_comments'] = $this->helpdesk_model->selectHelpdeskCommentsByQueryID($args[0]);
		$this->load->view('front/helpdesk-view',$data);
	}
}
