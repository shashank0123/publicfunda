<?php 
class Bookadd extends CI_Controller
{
	function listing()
	{
		$this->load->model('bookadd_model');
		$data['bookdata'] = $this->bookadd_model->getAllbooking();
		$this->load->view('admin/bookadd/listing',$data);
	}
	function edit()
	{
		$args = func_get_args();
		//echo $args[0];
		$this->load->model('bookadd_model');
		$data['bookdata'] = $this->bookadd_model->selectBookingAddByID($args[0]);
		if(isset($_POST['editData']))
		{
			//print_r($_POST); die();
			$edata['status']= $this->input->post('paymentStatus');
			$this->bookadd_model->updateData($args[0],$edata);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/bookadd/listing');	
		}	
		
		$this->load->view('admin/bookadd/edit',$data);
	}
	
	function deleteRecord()
	{
		$args = func_get_args();
		$this->load->model('bookadd_model');		
		$this->bookadd_model->deleteBookingData($args[0]);			
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/bookadd/listing');	
	}
	
	function downloadMis()
	{
		$this->load->model('bookadd_model');
		
		$args = func_get_args();			
		$data['bookdata'] = $this->bookadd_model->getAllbooking();		
		// Load all views as normal
		$this->load->view('admin/bookadd/download-mis',$data);		
		// Get output html
		$html = $this->output->get_output();		
		// Load library
		$this->load->library('dompdf_gen');		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."mis-addbooking-report.pdf");				
		
	}
	
}
