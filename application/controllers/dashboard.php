<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller{

	public function listing(){

		$this->load->model('dashboard_model');

		$data["Dashboard"]=$this->dashboard_model->list_dashboard();

        $this->load->view("admin/Dashboard/listing",$data);  

	}

	public function edit_listing($id){

		$this->load->model('dashboard_model');

		$this->data['Dashboard']= $this->dashboard_model->edit_listing($id);

		if(isset($_POST['edit_listing'])){

			$data["title"]=         $this->input->post("title");

			$data["description"]=         $this->input->post("description");

			$data["status"]=         $this->input->post("status");

			$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully update dashboard</div>');

			$this->dashboard_model->update_daseboard($id,$data);

			redirect("Dashboard/listing/".$id);       	

			}  

		$this->load->view('admin/Dashboard/edit_listing', $this->data, FALSE);

		}

		public function cover_image()
		{
			// phpinfo();
			// var_dump($_FILES);
			$this->db->where('id', 13);
			$olddata = $this->db->get('tbl_dashboard');
			$datacheck = $olddata->result();
			if($_FILES['cover_image']['name']!="")
			{
				$file = time().'_'.$_FILES['cover_image']['name'];
				$file_tmp = $_FILES['cover_image']['tmp_name'];
				$path = 'uploads/dashboard/'.$file;
				
				move_uploaded_file($file_tmp,$path);
				unlink('uploads/dashboard/'.$datacheck[0]->description);
				// var_dump($check);
				// die();
				$data['description'] = $file;
				$data['url'] = $_POST['cover_image_link'];

				$this->db->where('id', 13);
				$this->db->update('tbl_dashboard', $data);

			}
				redirect('dashboard/listing');
		}

	public function list_slider(){

		$this->load->model('dashboard_model');

		$data["slider"]=$this->dashboard_model->list_slider();

        $this->load->view("admin/Dashboard/list_slider",$data);  

	}	

}

