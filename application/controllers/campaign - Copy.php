<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Campaign extends CI_Controller
{
	public function index()
	{
		$user_login_id = $this->session->userdata('USERID'); 
		$campaignData = $this->campaign_model->selectCampaignByUserID($user_login_id);
		$login_user = $this->user_model->selectUserByID($user_login_id);
		$total_poits = $this->getUserPoints();
		$total_used_points = $this->getAllUsedPoints();	
		
		//if(count($campaignData)<5)
		//{ 	//if($total_poits<=$total_used_points)
			//{					
				if(isset($_POST['addCampagn'])) 
				{
					$cdata['user_id'] = $user_login_id;
					$cdata['campaign'] = $this->input->post('campaign');
					$cdata['campaignType'] = $this->input->post('campaignType');
					$cdata['campaignTitle'] = $this->input->post('campaignTitle');
					$cdata['campaignUrl'] = $this->input->post('campaignUrl');
					$cdata['campaignPoints'] = $this->input->post('campaignPoints');
					$cdata['sdate'] = $this->input->post('sdate');
					$cdata['edate'] = $this->input->post('edate');
					$cdata['sage'] = $this->input->post('sage');
					$cdata['eage'] = $this->input->post('eage');
					$cdata['gender'] = $this->input->post('gender');
					$cdata['profession'] = $this->input->post('profession');
					$cdata['industry'] = $this->input->post('industry');
					$cdata['state'] = $this->input->post('state');
					$cdata['city'] = $this->input->post('city');
					//$cdata['updateDate'] = time();
					$cdata['createDate'] = time();
					//if($total_poits>=$total_used_points+$this->input->post('campaignPoints'))
					//{
						$this->campaign_model->insertCampaignData($cdata);					
						$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>your campaign has been successfully created.</div>');	
						redirect('index.php/campaign/index');					
					//}
					//else
					//{
					//	$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>error! </div>');	
					//	redirect('index.php/campaign/index');
					//}
				}
				if(isset($_POST['updateCampagn']))
				{									
					$cdata['campaign'] = $this->input->post('campaign');
					$cdata['campaignType'] = $this->input->post('campaignType');
					$cdata['campaignTitle'] = $this->input->post('campaignTitle');
					$cdata['campaignUrl'] = $this->input->post('campaignUrl');
					
					$points = $this->input->post('campaignPoints');
					if(!empty($points) && $points>0)
					{
						$cdata['campaignPoints'] = $this->input->post('campaignPoints')+$_POST['oldctp'];
					}				
					
					//$cdata['sdate'] = $this->input->post('sdate');
					//$cdata['edate'] = $this->input->post('edate');
					$cdata['sage'] = $this->input->post('sage');
					$cdata['eage'] = $this->input->post('eage');
					$cdata['gender'] = $this->input->post('gender');
					$cdata['profession'] = $this->input->post('profession');
					$cdata['industry'] = $this->input->post('industry');
					$cdata['state'] = $this->input->post('state');
					$cdata['city'] = $this->input->post('city');
					$cdata['updateDate'] = time();
					$cid = base64_decode($this->input->post('cid'));
					$this->campaign_model->updateCampaignData($cid,$cdata);					
					$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>your campaign has been successfully Updated.</div>');	
					redirect('index.php/campaign/index');
					
				}
				
				if(isset($_POST['paynow']))
				{
					if($_POST['totalAmount']!="")
					{
						$_POST['uid'] = $user_login_id;
						redirect('index.php/payment/pay?'.http_build_query($_POST));
					}	
				}
				$planInvData = $this->user_model->selectPlanInvByUid($user_login_id);
				if(isset($_POST['submitPlanInv']))
				{
						
					
					//$invdata['invID'] = $this->input->post();
					$invdata['accountHolderName'] = $this->input->post('accountHolderName');
					$invdata['fromAccount'] = $this->input->post('fromAccount');
					$invdata['bankName'] = $this->input->post('bankName');
					$invdata['toBank'] = $this->input->post('toBank');
					$invdata['toAccount'] = $this->input->post('toAccount');
					$invdata['ePoints'] = $this->input->post('ePoints');
					$invdata['amount'] = $this->input->post('amount');
					$invdata['utrNumber'] = $this->input->post('utrNumber');
					$invdata['neftDate'] = $this->input->post('neftDate');					
					
					if(count($planInvData)==0)
					{
						$invdata['invTime'] = time();
						$invdata['uid'] = $user_login_id;
						$invdata['status'] = 0; 
						$this->user_model->insertPlanInv($invdata);
						$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>plan invoice has been successfully created.</div>');
						redirect('index.php/campaign/index?actTab=planinv');
					}
					else
					{
						$this->user_model->updatePlanInv($planInvData[0]->id,$invdata);
						$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>plan invoice has been successfully Updated.</div>');
						redirect('index.php/campaign/index?actTab=planinv');
					}	
					
					
				}	
				
			//}	
		//}
		
		if(isset($_POST['addPayNow']))
		{
			$this->load->model('bookadd_model');
			$pageName['user_id'] = $user_login_id;
			$pageName['addType'] = $this->input->post('addType');
			$pageName['pageName'] = $this->input->post('pageName');
			$pageName['days'] = $this->input->post('days');
			$pageName['tax'] = 10;
			$pageName['totalAmount'] = $this->input->post('totalAmount');
			$pageName['grandtotal'] = $this->input->post('grandtotal');
			$pageName['addTime'] = time();
			$pageName['addDate'] = date('Y-m-d');
			$pageName['status'] = 'pending';
			
			$this->bookadd_model->insertData($pageName);
			redirect('https://www.payumoney.com/paybypayumoney/#/257899');	
		}	
		
		if(isset($_POST['payNowPurchasePoints']))
		{
			$this->load->model('bookadd_model');
			$pageName['user_id'] = $user_login_id;
			$pageName['addType'] = 'Purchase Points';
			$pageName['pageName'] = 'N/A';
			$pageName['days'] = $this->input->post('purchaseTotalPoints');//'N/A';
			$pageName['tax'] = 10;
			$pageName['totalAmount'] = $this->input->post('totalAmount');
			$pageName['grandtotal'] = $this->input->post('grandtotal');
			$pageName['addTime'] = time();
			$pageName['addDate'] = date('Y-m-d');
			$pageName['status'] = 'pending';
			
			$this->bookadd_model->insertData($pageName);
			redirect('https://www.payumoney.com/paybypayumoney/#/257899');	
		}
		
		$data['planInvoice'] = $planInvData;
		$data['total_poits'] = $total_poits;
		$data['total_used_points'] = $total_used_points;
		$data['campaignData'] = $campaignData;
		$data['login_user'] = $login_user;
		$this->load->view('front/campaign/manage-campaign',$data);
	}
	
	function getAllUsedPoints()
	{
		$user_login_id = $this->session->userdata('USERID'); 
		$campaignData = $this->campaign_model->selectCampaignByUserID($user_login_id);
		if($campaignData>0)
		{
			$points = 0;
			foreach($campaignData  as $cd)
			{
				$points +=$cd->campaignPoints;
			}
			return $points;
		}
		else
		{
			return 0;
		}		
	}
	
	function getUserPoints()
	{
		$user_login_id = $this->session->userdata('USERID'); 
		$userData = $this->user_model->selectUserByID($user_login_id);
		$totalPoints = ($userData[0]->totalPoints+$userData[0]->paidPoints);
		if($totalPoints>0)
		{	
			return $totalPoints;
		}
		else
		{
			return 0;
		}		
	}
	
	function updatePoints()
	{
		$totalPoints = $this->getUserPoints();
		$usedPoints = $this->getAllUsedPoints();
				
		if(isset($_POST) && !empty($_POST['cid']))
		{
			if($totalPoints>=$usedPoints+$_POST['tp'])
			{
				$cdata['campaignPoints'] = $_POST['tp'];
				$this->campaign_model->updateCampaignDataByUidAndCid($_POST['uid'],$_POST['cid'],$cdata);
				echo 1;
			}
			else
			{
				echo 0;
			}	
		}		
	}
	
	function checkcampaignStatus()
	{
		if(isset($_POST)) 
		{
			if(!empty($_POST['startDate']) && !empty($_POST['endDate']) && !empty($_POST['cid']))
			{
				if($_POST['cid']=='all')
				{
					$userId = $_POST['cuserid'];	
					$campaignData = $this->campaign_model->selectCampaignByUserID($userId);	
					$ids = array();
					foreach($campaignData as $campData)
					{
						array_push($ids,$campData->id);						
					} 
					
					$first_date = date('Y-m-d',strtotime($_POST['startDate']));
					$second_date = date('Y-m-d',strtotime($_POST['endDate']));
					$data['cstatus'] = $this->campaign_model->checkCampaignStatusId_in($_POST['cuserid'],$ids,$first_date,$second_date);
					$this->load->view('front/ajax-campaign-status',$data);
				}
				else
				{
					$first_date = date('Y-m-d',strtotime($_POST['startDate']));
					$second_date = date('Y-m-d',strtotime($_POST['endDate']));
					$data['cstatus'] = $this->campaign_model->checkCampaignStatus($_POST['cuserid'],$_POST['cid'],$first_date,$second_date);
					$this->load->view('front/ajax-campaign-status',$data);
				}				
			}	
		}	
	}
	
	function campaign_status_report()
	{
		$first_date="";
		$second_date="";
		$status = "";
		//echo date('Y-m-d',strtotime('02/24/2017'));
		if(isset($_POST['search']))
		{
			if($_POST['startDate']!=""){ $first_date = date('Y-m-d',strtotime($_POST['startDate'])); }
			if($_POST['endDate']!=""){ $second_date = date('Y-m-d',strtotime($_POST['endDate'])); }
			$status = $_POST['status'];
		}	
		
		$this->load->model('helpdesk_model');
		$data['cstatus']= $this->campaign_model->checkAllCampaignStatus($first_date,$second_date,$status);
		$this->load->view('admin/user/campaign-status',$data);
	}
	
}	