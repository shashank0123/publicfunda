<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Blog extends CI_Controller 
{
	public function myblog()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['blod_data'] = $this->blog_model->select_blog_by_uid($loginUserId);
		$this->load->view('blog/my-blog',$data);
	}	

	//featured list on and off
	public function listingchange()
	{
		$id = $_REQUEST['id'];
		$this->db->where('id', $id);
		$blog = $this->db->get('tbl_blog');
		$blog = $blog->result();
		
		if (isset($blog[0]->featured)){
			if ($blog[0]->featured == 1){
				$data['featured'] = 0;
			}
			else{
				$data['featured'] = 1;

			}
		}
		else
			$data['featured'] = 1;
		// $data
			$this->db->where('id', $id);
			$this->db->update('tbl_blog',$data);
		
		echo 'done';
	}

	public function primarychange()
	{
		$id = $_REQUEST['id'];
		$this->db->where('id', $id);
		$blog = $this->db->get('tbl_blog');
		$blog = $blog->result();
		
		if (isset($blog[0]->highlight_primary)){
			if ($blog[0]->highlight_primary == 1){
				$data['highlight_primary'] = 0;
			}
			else{
				$data['highlight_primary'] = 1;

			}
		}
		else
			$data['highlight_primary'] = 1;
		// $data
			$this->db->where('id', $id);
			$this->db->update('tbl_blog',$data);
		
		echo 'done';
	}

	public function otherchange()
	{
		$id = $_REQUEST['id'];
		$this->db->where('id', $id);
		$blog = $this->db->get('tbl_blog');
		$blog = $blog->result();
		
		if (isset($blog[0]->highlist_other)){
			if ($blog[0]->highlist_other == 1){
				$data['highlist_other'] = 0;
			}
			else{
				$data['highlist_other'] = 1;

			}
		}
		else
			$data['highlist_other'] = 1;
		// $data
			$this->db->where('id', $id);
			$this->db->update('tbl_blog',$data);
		
		echo 'done';
	}

	public function post_blog()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if(isset($_POST['savedata']))
		{			
			if($_FILES['image']['name']!="")

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];

				$path = 'uploads/blog/'.$file;				

				move_uploaded_file($file_temp,$path);				

			}

			else

			{

				$file = "";

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');

			$data['uid'] = $loginUserId;

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	

			$data['status'] = 1;

			$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->insert($data);			

			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully saved.</div>');

			redirect('index.php/blog/myblog');			

		}

		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		$this->load->view('blog/post-blog',$catdata);

	}

	

	public function edit_myblog()

	{

		$args = func_get_args(); 		

		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');

		if(isset($_POST['savedata']))

		{			

			if($_FILES['image']['name']=="")

			{

				$file = $_POST['oldimage'];

			}

			else

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];

				$path = 'uploads/blog/'.$file;				

				move_uploaded_file($file_temp,$path);	

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');

			$data['uid'] = $loginUserId;

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	

			$data['status'] = 1;

			$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->update($args[0],$data);			

			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully saved.</div>');

			redirect('index.php/blog/myblog');			

		}

		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		$catdata['EDITBLOG']= $this->blog_model->select_blog_byid($args[0]);

		$this->load->view('blog/edit-blog',$catdata);

	}

	

	function delete_myblog()

	{

		$this->load->model('blog_model');

		$args=func_get_args();		

		$this->blog_model->delete_blog($args[0]);

		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');

		redirect('index.php/blog/myblog');

	}

	

	public function index()

	{
		$this->db->where('status', 1);
		$this->db->group_by('cat_id');
		$this->db->order_by('create_date', 'DESC');
		$blogdata = $this->db->get('tbl_blog');
		$data['sliders'] = $blogdata->result();
		$this->db->where('highlight_primary', 1);
		$highlight_result = $this->db->get('tbl_blog');
		$highlight_primary = $highlight_result->result();
		if (isset($highlight_primary[0])){
			$data['highlight_primary'] = $highlight_primary[0];
		}
		else
			$data['highlight_primary'] = [];
		$this->db->where('highlist_other', 1);
		$this->db->order_by('create_date', 'DESC');
		$highlight_result = $this->db->get('tbl_blog');
		$highlight_primary = $highlight_result->result();
		if (isset($highlight_primary[0])){
			$data['highlist_other'] = $highlight_primary;
		}
		else
			$data['highlist_other'] = [];

		$this->db->where('status', 1);
		$categories = $this->db->get('tbl_blogcategory'); 
		$categorylist = $categories->result();

		foreach ($categorylist as $key => $value) {
			$this->db->where('cat_id', $value->id);
			$this->db->where('status', 1);
			$this->db->order_by('create_date', 'DESC');
			$this->db->order_by('id', 'DESC');
			$this->db->limit(6);
			$category = $this->db->get('tbl_blog');
			$categorylist[$key]->blogs = $category->result();

		}
		$data['categorylist'] = $categorylist;
		$this->load->view('blog/index2', $data);
		// $this->load->view('blog/index');

	}

	function display()

	{

		$args=func_get_args();		

		$catdata['blogdata'] = $this->blog_model->select_blog_by_catid($args[0]);

		$this->load->view('blog/blog-listing',$catdata);

	}

	

	function details()
	{

		$args=func_get_args();	
		if (isset($_POST['submitcomment'])){
			$this->db->where('post_id', $args[0]);
			$this->db->where('user_id', $this->session->userdata('USERID'));
			$this->db->where('comment', $_POST['comment']);
			$catdata1 = $this->db->get('tbl_blog_comments');
			$category = $catdata1->result();
			if (count($category)<1){
				$data['user_id'] = $this->session->userdata('USERID');
				if (isset($_POST['comment_id'])){
					$data['parent_comment_id'] = $_POST['comment_id'];
				}
				$data['post_id'] = $args[0];
				$data['comment'] = $_POST['comment'];
				$this->db->insert('tbl_blog_comments', $data);
			}
		}	
		if ($this->session->userdata('USERID') != null){
			$this->db->where('user_id', $this->session->userdata('USERID'));
			$this->db->where('post_id', $args[0]);
			$checkset = $this->db->get('tbl_blog_inappropriate');
			$abuse = $checkset->result();
			$catdata['abuse'] = count($abuse);
		}
		else
			$catdata['abuse'] = -1;


		$catdata['blogdata'] = $this->blog_model->select_blog_byid($args[0]);
		$this->db->where('id', $catdata['blogdata'][0]->cat_id);
		$catdata1 = $this->db->get('tbl_blogcategory');
		$category = $catdata1->result();
		$catdata['category'] = $category;

		$this->db->where('post_id', $args[0]);
		$catdata1 = $this->db->get('tbl_blog_comments');

		$catdata['userblogs'] = $this->blog_model->select_blog_by_uid($catdata['blogdata'][0]->user_id);
		$category = $catdata1->result();
		foreach ($category as $key => $value) {
			if ($value->parent_comment_id == null){
				$catdata['comments'][0][] = $value;
			}
			else
				$catdata['comments'][$value->parent_comment_id][] = $value;
		}
		$image = "";
		if ($catdata['blogdata'][0]->user_id != ""){
			$this->db->where('id', $catdata['blogdata'][0]->user_id);
			$uname1 = $this->db->get('tbl_users');
			$uname = $uname1->result();
			$username = $uname[0]->name;
			$image = $uname[0]->image;
		}
		else
			$username = "";
		$catdata['username'] = $username;
		$catdata['image'] = $image;
		$this->load->view('blog/blog-details',$catdata);

	}

	

	function listing()

	{		

	    $this->load->model('blog_model');

		$data['BLOGDATA']= $this->blog_model->select_all_blog();

		$this->load->view('admin/blog/listing',$data);

	}

	function add()

	{	

		$this->load->model('blog_model');
		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		if(isset($_POST['savedata']))

		{			

			if($_FILES['image']['name']!="")

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];
				$size = getimagesize($file_temp);
				if ($size['mime'] == 'image/jpeg' && $size[0]/$size[1] == 1280/720){
					$path = 'uploads/blog/'.$file;				

					move_uploaded_file($file_temp,$path);				

				}
				else{
					$this->session->set_flashdata('message','<div class="alert alert-success">Please upload correct image in size ratio 1280x720</div>');
					// $this->load->view('admin/blog/add',$catdata);
					redirect('index.php/blog/add');
					exit();
				}


			}

			else

			{

				$file = "";

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	

			$data['status'] = $this->input->post('status');

			$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->insert($data);			

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');

			redirect('index.php/blog/listing');			

		}		

		

		$this->load->view('admin/blog/add',$catdata);

	}

	function edit()

	{	

		$this->load->model('blog_model');

		$args=func_get_args();

		if(isset($_POST['updatedata']))

		{

			if($_FILES['image']['name']=="")

			{

				$file = $_POST['oldimage'];

			}

			else

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];

				$size = getimagesize($file_temp);
				if ($size['mime'] == 'image/jpeg' && $size[0]/$size[1] == 1280/720){
					$path = 'uploads/blog/'.$file;				

					move_uploaded_file($file_temp,$path);				

				}
				else{
					$this->session->set_flashdata('message','<div class="alert alert-success">Please upload correct image in size ratio 1280x720</div>');
					// $this->load->view('admin/blog/add',$catdata);
					redirect('index.php/blog/add');
					exit();
				}

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	

			$data['status'] = $this->input->post('status');

			//$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->update($args[0],$data);

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');

			redirect('index.php/blog/listing');

		}

		$catdata['EDITBLOG']= $this->blog_model->select_blog_byid($args[0]);

		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		$this->load->view('admin/blog/edit',$catdata);

	}

	

	function delete()

	{

		$this->load->model('blog_model');

		$args=func_get_args();		

		$this->blog_model->delete_blog($args[0]);

		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');

		redirect('index.php/blog/listing');

	}

	function inappropriate()

	{

		$this->load->model('blog_model');

		$args=func_get_args();		

		$this->blog_model->update_inappropriate_status($args[0]);

		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');

		redirect('index.php');

	}



}