<?php 

class Royalgroups extends CI_Controller

{
	var $transaction_table = 'tbl_royal_group_transactions';

	function listing()
	{

		$args = func_get_args();

		$data['type'] = $args[0];

		$this->load->model('admin_royal_group');

		$data['bookdata'] = $this->admin_royal_group->getAllGroup($args[0]);


		$this->load->view('admin/royalgroup/listing',$data);
	}

	

	function add()

	{

		$args = func_get_args();

		$this->load->model('admin_royal_group');

		if(isset($_POST['addData']))

		{

			if($_FILES['image']['name']!="")

			{

				$file = time().'_'.$_FILES['image']['name'];

				$file_tmp = $_FILES['image']['tmp_name'];

				$path = 'uploads/royalgroups/'.$file;

				move_uploaded_file($file_tmp,$path);

			}

			else

			{

				$file = "";

			}

		    $edata['image'] = $file;

						 

		    $edata["group_name"]= $this->input->post("group_name");
		    $edata["no_of_members"]= $this->input->post("no_of_members");
		    $edata["enter_amount"]= $this->input->post("enter_amount");
		    $edata["per_user_max"]= $this->input->post("per_user_max");
		    $edata["level1"]= $this->input->post("level1");
		    $edata["level2"]= $this->input->post("level2");
		    $edata["level3"]= $this->input->post("level3");
		    $edata["minimum_direct"]= $this->input->post("minimum_direct");
		    $edata["condition"]= $this->input->post("condition");

        	// $edata['text'] = $this->input->post('text');

        	// $edata['youtubelink'] = $this->input->post('youtubelink');

        	$edata['type'] = 'levelwise';
        	$edata['calculation_time'] = '1';

        	$edata['status'] = $this->input->post('status');

			// $edata['type'] = $args[0];

			$this->admin_royal_group->insertData($edata);			

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully added.</div>');

			

			redirect('index.php/royalgroups/listing/'.$args[0],$edata);	

		}	

		$data['type'] = $args[0];

		$this->load->view('admin/royalgroup/add',$data);

	}

	function edit()

	{

		$args = func_get_args();

		//echo $args[0];

		$this->load->model('admin_royal_group');

		

		if(isset($_POST['editData']))

		{

			if($_FILES['image']['name']!="")

			{

				$file = time().'_'.$_FILES['image']['name'];

				$file_tmp = $_FILES['image']['tmp_name'];

				$path = 'uploads/royalgroups/'.$file;

				move_uploaded_file($file_tmp,$path);

			}

			else

			{

				$file = $_POST['oldImage'];

			}

		    $edata['image'] = $file;

			

		    $edata["group_name"]= $this->input->post("group_name");
		    $edata["no_of_members"]= $this->input->post("no_of_members");
		    $edata["enter_amount"]= $this->input->post("enter_amount");
		    $edata["per_user_max"]= $this->input->post("per_user_max");
		    $edata["level1"]= $this->input->post("level1");
		    $edata["level2"]= $this->input->post("level2");
		    $edata["level3"]= $this->input->post("level3");
		    $edata["minimum_direct"]= $this->input->post("minimum_direct");
		    $edata["condition"]= $this->input->post("condition");

        	
        	$edata['calculation_time'] = '1';

        	$edata['status'] = $this->input->post('status');

			$this->admin_royal_group->updateData($args[0],$edata);			

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');

			redirect('index.php/royalgroups/listing/'.$args[1]);	

		}	

		$data['REWARDS'] = $this->admin_royal_group->selectGroupsByID($args[0]);

		$data['type'] = $args[1];

		$this->load->view('admin/royalgroup/edit',$data);

	}

	

	function deleteRecord()

	{

		$args = func_get_args();

		$this->load->model('admin_royal_group');		

		$this->admin_royal_group->deleteRewards($args[0]);			

		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');

		redirect('index.php/royalgroups/listing/'.$args[1]);	

	}	

	function payment_history(){
		$args = func_get_args();
		$this->load->model('admin_royal_group');
		$sdate = $this->input->get('start');
		$edate = $this->input->get('end');
		if (!empty($sdate) && !empty($edate)){
			
			$this->db->where('created_at >=' , date('Y-m-d',strtotime($sdate)));
			$this->db->where('created_at <=',date('Y-m-d',strtotime($edate)));

		}
		$this->db->where('group_id', $args[0]);
		$transactions = $this->db->get($this->transaction_table);
		$data['transactions'] = $transactions->result();
		$data['group_id'] = $args[0];
		$this->load->view('admin/royalgroup/transactions',$data);

		
	}

	function calculatebalance(){
		if ($this->input->get('token') == "isuofhnsdjvfndsjkdfoiunvcmsdioa" || $this->session->userdata('ADMINID') != null ){
			$this->load->model('royal_group_tree');
			$payments = $this->royal_group_tree->calculationandupgrade();
		}
		else{
			redirect('/');
		}
	}

	

}

