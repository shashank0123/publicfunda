<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Userblog extends CI_Controller 
{
	public function post_blog()
	{
		$args = func_get_args(); 		
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		
		if(isset($_POST['savedata']))
		{			
			if($_FILES['image']['name']!="")

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];

				$size = getimagesize($file_temp);
				if ($size['mime'] == 'image/jpeg' && $size[0]/$size[1] == 1280/720){
					$path = 'uploads/blog/'.$file;				

					move_uploaded_file($file_temp,$path);				

				}
				else{
					$this->session->set_flashdata('message','<div class="alert alert-success">Please upload correct image in size ratio 1280x720</div>');
					// $this->load->view('admin/blog/add',$catdata);
					redirect('index.php/user/dashboard');
					exit();
				}				

			}

			else

			{

				$file = "";

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');

			$data['uid'] = $loginUserId;

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	
			// $data['youtube_video'] = $this->input->post('youtube_video');	

			$data['status'] = 1;

			$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->insert($data);			

			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully saved.</div>');

			redirect('index.php/user/dashboard');			

		}

		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		$this->load->view('blog/post-blog',$catdata);

	}

	

	public function edit_myblog()

	{

		$args = func_get_args(); 		

		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');


		if(isset($_POST['savedata']))

		{			

			if($_FILES['image']['name']=="")

			{

				$file = $_POST['oldimage'];

			}

			else

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];

				$size = getimagesize($file_temp);
				if ($size['mime'] == 'image/jpeg' && $size[0]/$size[1] == 1280/720){
					$path = 'uploads/blog/'.$file;				

					move_uploaded_file($file_temp,$path);				

				}
				else{
					$this->session->set_flashdata('message','<div class="alert alert-success">Please upload correct image in size ratio 1280x720</div>');
					// $this->load->view('admin/blog/add',$catdata);
					redirect('index.php/user/dashboard');
					exit();
				}	

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');

			$data['uid'] = $loginUserId;

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	

			$data['status'] = 1;

			$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->insert($data);			

			$this->session->set_flashdata('message','<div class="alert alert-success">blog has been successfully saved.</div>');

			redirect('index.php/blog/myblog');			

		}

		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		$catdata['EDITBLOG']= $this->blog_model->select_blog_byid($args[0]);

		$this->load->view('blog/edit-blog',$catdata);

	}

	

	function delete_myblog()

	{

		$this->load->model('blog_model');

		$args=func_get_args();		

		$this->blog_model->delete_blog($args[0]);

		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');

		redirect('index.php/blog/myblog');

	}

	

	public function index()

	{
		$this->db->where('status', 1);
		$this->db->group_by('cat_id');
		$this->db->order_by('create_date', 'DESC');
		$blogdata = $this->db->get('tbl_blog');
		$data['sliders'] = $blogdata->result();
		$this->load->view('blog/index2', $data);
		// $this->load->view('blog/index');

	}

	function display()

	{

		$args=func_get_args();		

		$catdata['blogdata'] = $this->blog_model->select_blog_by_catid($args[0]);

		$this->load->view('blog/blog-listing',$catdata);

	}
	function myposts()
	{
		$args=func_get_args();		
		$catdata['blogdata'] = $this->blog_model->select_blog_by_uid($args[0]);
		$this->load->view('blog/blog-listing',$catdata);
		# code...
	}
	

	function details()
	{

		$args=func_get_args();	

		if (isset($_POST['submitcomment'])){
			$this->db->where('post_id', $args[0]);
			$this->db->where('user_id', $this->session->userdata('USERID'));
			$this->db->where('comment', $_POST['comment']);
			$catdata1 = $this->db->get('tbl_blog_comments');
			$category = $catdata1->result();
			if (count($category)<1){
				$data['user_id'] = $this->session->userdata('USERID');
				if (isset($_POST['comment_id'])){
					$data['parent_comment_id'] = $_POST['comment_id'];
				}
				$data['post_id'] = $args[0];
				$data['comment'] = $_POST['comment'];
				$this->db->insert('tbl_blog_comments', $data);
			}
		}	

		$catdata['blogdata'] = $this->blog_model->select_blog_byid($args[0]);
		$this->db->where('id', $catdata['blogdata'][0]->cat_id);
		$catdata1 = $this->db->get('tbl_blogcategory');
		$category = $catdata1->result();
		$catdata['category'] = $category;

		$this->db->where('post_id', $args[0]);
		$catdata1 = $this->db->get('tbl_blog_comments');
		$category = $catdata1->result();
		foreach ($category as $key => $value) {
			if ($value->parent_comment_id == null){
				$catdata['comments'][0][] = $value;
			}
			else
				$catdata['comments'][$value->parent_comment_id][] = $value;
		}
		// echo "<pre>";
		// var_dump($catdata['comments']);
		// die();
		// $catdata['comments'] = $category;


		$this->load->view('blog/blog-details',$catdata);

	}

	

	function listingblog()

	{		

	    $this->load->model('blog_model');
	    $this->load->model('user_model');
	    $data['login_user'] = $this->user_model->selectUserByID($user_login_id);

		$data['BLOGDATA']= $this->blog_model->select_blog_by_userid($this->session->userdata('USERID'));

		$this->load->view('front/blog/listing',$data);

	}

	function add()

	{	

		$this->load->model('blog_model');

		if(isset($_POST['savedata']))

		{			

			if($_FILES['image']['name']!="")

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];

				$size = getimagesize($file_temp);
				if ($size['mime'] == 'image/jpeg' && $size[0]/$size[1] == 1280/720){
					$path = 'uploads/blog/'.$file;				

					move_uploaded_file($file_temp,$path);				

				}
				else{
					$this->session->set_flashdata('message','<div class="alert alert-success">Please upload correct image in size ratio 1280x720</div>');
					// $this->load->view('admin/blog/add',$catdata);
					redirect('index.php/user/dashboard');
					exit();
				}				

			}

			else

			{

				$file = "";

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');
			$data['user_id'] = $this->session->userdata('USERID');

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	

			$data['status'] = $this->input->post('status');
			$data['youtube_video'] = $this->input->post('youtube_video');

			$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->insert($data);			

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');

			redirect('index.php/user/dashboard');			

		}		

		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		$this->load->view('front/blog/add',$catdata);

	}

	function edit()

	{	

		$this->load->model('blog_model');

		$args=func_get_args();

		if(isset($_POST['savedata']))

		{

			if($_FILES['image']['name']=="")

			{

				$file = $_POST['oldimage'];

			}

			else

			{

				$file = createUniqueFile($_FILES['image']['name']);

				$file_temp = $_FILES['image']['tmp_name'];

				$size = getimagesize($file_temp);
				if ($size['mime'] == 'image/jpeg' && $size[0]/$size[1] == 1280/720){
					$path = 'uploads/blog/'.$file;				

					move_uploaded_file($file_temp,$path);				

				}
				else{
					$this->session->set_flashdata('message','<div class="alert alert-success">Please upload correct image in size ratio 1280x720</div>');
					// $this->load->view('admin/blog/add',$catdata);
					redirect('index.php/user/dashboard');
					exit();
				}	

			}

			$data['image'] = $file;

			$data['cat_id'] = $this->input->post('cat_id');

			$data['title'] = $this->input->post('title');

			$data['content'] = $this->input->post('content');	
			$data['user_id'] = $this->session->userdata('USERID');	

			$data['status'] = $this->input->post('status');

			//$data['create_date'] = date('Y-m-d');

			

			$this->blog_model->update($args[0],$data);

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');

			redirect('index.php/user/dashboard');

		}

		$catdata['EDITBLOG']= $this->blog_model->select_blog_byid($args[0]);

		$catdata['CATEGORY'] = $this->blog_model->select_all_active_blogcat();

		redirect('index.php/user/dashboard');

	}

	

	function delete()

	{

		$this->load->model('blog_model');

		$args=func_get_args();		

		$this->blog_model->delete_blog($args[0]);

		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');

		redirect('index.php/user/dashboard');

	}



}