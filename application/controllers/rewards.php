<?php 

class Rewards extends CI_Controller

{

	function listing()
	{

		$args = func_get_args();

		$data['type'] = $args[0];

		$this->load->model('rewards_model');

		$data['bookdata'] = $this->rewards_model->getAllRewardsByType($args[0]);

		$this->load->view('admin/rewards/listing',$data);
	}

	

	function add()

	{

		$args = func_get_args();

		$this->load->model('rewards_model');

		if(isset($_POST['addData']))

		{

			if($_FILES['image']['name']!="")

			{

				$file = time().'_'.$_FILES['image']['name'];

				$file_tmp = $_FILES['image']['tmp_name'];

				$path = 'uploads/rewards/'.$file;

				move_uploaded_file($file_tmp,$path);

			}

			else

			{

				$file = "";

			}

		    $edata['image'] = $file;

			if($args[0]=='special')

			{

				$edata["user_id"]= $this->input->post("user_id");

			}				 

		    $edata["title"]= $this->input->post("title");

        	$edata['text'] = $this->input->post('text');

        	$edata['youtubelink'] = $this->input->post('youtubelink');

        	$edata['type'] = $this->input->post('type');

        	$edata['status'] = $this->input->post('status');

			$edata['type'] = $args[0];

			$this->rewards_model->insertData($edata);			

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully added.</div>');

			

			redirect('index.php/rewards/listing/'.$args[0],$edata);	

		}	

		$data['type'] = $args[0];

		$this->load->view('admin/rewards/add',$data);

	}

	function edit()

	{

		$args = func_get_args();

		//echo $args[0];

		$this->load->model('rewards_model');

		

		if(isset($_POST['editData']))

		{

			if($_FILES['image']['name']!="")

			{

				$file = time().'_'.$_FILES['image']['name'];

				$file_tmp = $_FILES['image']['tmp_name'];

				$path = 'uploads/rewards/'.$file;

				move_uploaded_file($file_tmp,$path);

			}

			else

			{

				$file = $_POST['oldImage'];

			}

		    $edata['image'] = $file;

			if($args[1]=='special')

			{

				$edata["user_id"]= $this->input->post("user_id");

			}

		    $edata["title"]= $this->input->post("title");

        	$edata['text'] = $this->input->post('text');

        	$edata['youtubelink'] = $this->input->post('youtubelink');

        	$edata['type'] = $args[1];

        	$edata['status'] = $this->input->post('status');

			$this->rewards_model->updateData($args[0],$edata);			

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');

			redirect('index.php/rewards/listing/'.$args[1]);	

		}	

		$data['REWARDS'] = $this->rewards_model->selectRewardsByID($args[0]);

		$data['type'] = $args[1];

		$this->load->view('admin/rewards/edit',$data);

	}

	

	function deleteRecord()

	{

		$args = func_get_args();

		$this->load->model('rewards_model');		

		$this->rewards_model->deleteRewards($args[0]);			

		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');

		redirect('index.php/rewards/listing/'.$args[1]);	

	}	

	

}

