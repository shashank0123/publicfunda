<?php 
class Directincome extends CI_Controller
{
	function index()
	{ 
			$this->load->model('user_model');	
			$this->load->model('admin_model');	
			$this->load->model('booster_model');
			$this->load->model('directincome_model');
			$userData = $this->user_model->selectActiveUser();  // get active all user
			foreach($userData as $user)
			{
				$user_id = $user->id;
				$user = $this->user_model->selectUserByID($user_id);    // get user data by id
				$lastdate_promotional = $user[0]->directincome_lastdate;  // last date amount
				
				
				$user_invoice_total = $this->getTotalOfUserInvoice($user_id);

				$final_income =  $this->directincome_model->getBonusIncome_direct($lastdate_promotional,$user_id);
				
				if($final_income!=0 && !empty($final_income))
				{
					if($user_invoice_total>0)
					{
						
						$direct_sale = $this->admin_model->getAllTaxes(1);
						$commission = ($final_income*$direct_sale[0]->directincome)/100;//$direct_sale[0]->directincome;  //10;		
		                //$this->my_tds($commission).'<br>';
						$inv_data['uid'] = $user_id;
						$inv_data['totalincome'] = $final_income;
						$inv_data['income'] = $commission;
						$inv_data['tds'] = $this->my_tds($commission); //10;
						$inv_data['admincharge'] =$this->my_admincharge($commission);
						$inv_data['netIncome'] = $this->applytax($commission);
						$inv_data['status'] = 'pending';
						$inv_data['crDate'] = date('Y-m-d');
						
						//$final_income.'---'.$inv_data['netIncome'].'<br>';
						$this->directincome_model->insertBonusIncome_direct($inv_data);
						
						$update_user['directincome_lastdate']= time(); //date('Y-m-d');
						$this->user_model->updateData($user_id,$update_user);
					}
				}	
			}
			echo 'command executed';
 	}	
	
	function getTotalOfUserInvoice($userid)
	{
		$this->load->model('booster_model');
		$invoiceData = $this->booster_model->getUserInvoiceByUserID($userid);
		$bonus =0;
		if(count($invoiceData)>0)
		{
			foreach($invoiceData as $inv)
			{
				if($inv->status=='paid')
				{
					$bonus += $inv->amount;
				}						
			}					
		}
		return $bonus;
	}
	
	function applycapping($amount,$planid)
	{
		error_reporting(0); 
		$this->load->model('plan_model');
		$plandata = $this->plan_model->selectPlanByID($planid);
		$capping = $plandata[0]->capping;	
		//echo $capping; die();	
		if($amount>$capping)
		{
			return $capping;
		}
		else
		{
			return $amount;
		}	

	}
	
	function my_tds($amount)
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$tax = $tax[0]->tds;  //10;
		$tax_amount = ($amount*$tax)/100;
		return $tax_amount;
	}
	function my_admincharge($amount)
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$admin_charge = $tax[0]->admin_charge; //5;
		$tax_amount = ($amount*$admin_charge)/100;
		return $tax_amount;
	}
	
	function applytax($amount)
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$admin_charge = $tax[0]->admin_charge; //5;
		$tax = $tax[0]->tds;  //10;
		$total_tax= $admin_charge+$tax;
		$tax_amount = ($amount*$total_tax)/100;
		//return $tax_amount+$amount;
		return abs($amount-$tax_amount);
	}
	function get_promotional_income($amount)
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$amount_per = $tax[0]->promotinalincomme;  //8;
		$final_amount = ($amount*$amount_per)/100;
		return $final_amount;
	}	


	function test()
	{
		$this->load->model('directincome_model');
		//$data = $this->directincome_model->getTreeIncome(10001);
		//print_r($data);
	}
	
}

?>