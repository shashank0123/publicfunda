<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blogcategory extends CI_Controller 
{	function cat_listing()
	{		
	    $this->load->model('blog_model');
		$data['BLOGDATA']= $this->blog_model->select_all_blogcat();
		$this->load->view('admin/blog/cat_listing',$data);
	}

	function add_cat()
	{	
		$this->load->model('blog_model');
		if(isset($_POST['savedata']))
		{
			if($_FILES['category_image']['name']!=""){
				$file = createUniqueFile($_FILES['category_image']['name']);
				$file_temp = $_FILES['category_image']['tmp_name'];
				$path = 'uploads/category/'.$file;			
				move_uploaded_file($file_temp,$path);			
			}
			else
			{
				$file = "";
			}
			$data['category_image'] = $file;		

			$data['title'] = $this->input->post('title');
			
			$data['status'] = $this->input->post('status');	
			$this->blog_model->insert_cat($data);			

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');

			redirect('index.php/blogcategory/cat_listing');			

		}

		$this->load->view('admin/blog/add-cat');

	}

	function edit_cat()

	{	

		$this->load->model('blog_model');

		$args=func_get_args();

		if(isset($_POST['updatedata']))
		{
			$data['title'] = $this->input->post('title');
			$data['status'] = $this->input->post('status');
			if($_FILES['category_image']['name']!=""){
				$file = createUniqueFile($_FILES['category_image']['name']);
				$file_temp = $_FILES['category_image']['tmp_name'];
				$path = 'uploads/category/'.$file;			
				move_uploaded_file($file_temp,$path);			
			}
			else
			{
				$file = "";
			}
			$data['category_image'] = $file;

			$this->blog_model->update_cat($args[0],$data);

			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');

			redirect('index.php/blogcategory/cat_listing');

		}

		$data['EDITBLOG']= $this->blog_model->select_blog_cat_byid($args[0]);

		$this->load->view('admin/blog/edit-cat',$data);

	}

	

	function delete_cat()

	{

		$this->load->model('blog_model');

		$args=func_get_args();		

		$this->blog_model->delete_cat($args[0]);

		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');

		redirect('index.php/blogcategory/cat_listing');

	}

	

}