<?php 
class Comments extends CI_Controller
{
	function listing()
	{
		if(isset($_POST['checkComments']))
		{
			$sdate = $this->input->post('startDate');
			$edate = $this->input->post('endDate');
			if(!empty($sdate) && !empty($edate))
			{						
			   //$date_1 = 
				$this->db->where('comment_date >=',date('Y-m-d',strtotime($sdate)));
				$this->db->where('comment_date <=',date('Y-m-d',strtotime($edate)));
			}	
		}
		$data['commentsdata'] = $this->comment_model->selectAllComments();
		$this->load->view('admin/comments/listing',$data);
	}
	
	function like_listing()
	{
		if(isset($_POST['checkComments']))
		{
			$sdate = $this->input->post('startDate');
			$edate = $this->input->post('endDate');
			if(!empty($sdate) && !empty($edate))
			{						
			   //$date_1 = 
				$this->db->where('like_date >=',date('Y-m-d',strtotime($sdate)));
				$this->db->where('like_date <=',date('Y-m-d',strtotime($edate)));
			}	
		}
		$data['commentsdata'] = $this->comment_model->selectAllLikeData();
		$this->load->view('admin/comments/listing_like',$data);
	}
	
	function edit()
	{
		$args = func_get_args();
		$data['bookdata'] = $this->staticblock_model->getBlockById($args[0]);
		if(isset($_POST['editData']))
		{
			$edata['content']= $this->input->post('content');
			$this->staticblock_model->updateBlockData($args[0],$edata);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/staticblock/listing');	
		}	
		
		$this->load->view('admin/staticblock/edit',$data);
	}
	
	function delete_comments()
	{
		$args = func_get_args();
		$this->comment_model->deleteComments($args[0]);			
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/comments/listing');	
	}
	
	function delete_all_like()
	{
		$args = func_get_args();
		$this->comment_model->deleteAllLikes();			
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/comments/like_listing');	
	}
	
}
