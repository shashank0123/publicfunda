<?php 
class Bookadd_model extends CI_Model
{
	var $book_table = 'tbl_bookadd';
	public function insertData($data)
	{		
		$this->db->insert($this->book_table,$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
		//return  $returnData;
	}
	
	public function updateData($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update($this->book_table,$data);
		return $returnData;
	}
	
	public function getAllbooking()
	{
		$this->db->order_by('id','DESC');
		$data = $this->db->get($this->book_table);
		return $data->result();
	}
	
	public function deleteBookingData($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->delete($this->book_table);
	}
	
	public function selectBookingAddByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->book_table);
		return $data->result();
	}

}