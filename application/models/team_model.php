<?php 
class team_model extends CI_Model
{
	var $user_table = 'tbl_users';	
	public function getTreeLeftOrRight($id,$pos,$status,$first_date,$second_date)
	{
		$category_data = array();
		$this->db->where('sponsor_id',$id);
		if($pos!="0")
		{
			$this->db->where('postion',$pos);
		}
		$this->db->where('rdate >=', $first_date);
		$this->db->where('rdate <=', $second_date);
		if($status!="all")
		{
			$this->db->where('status',$status);
		}	
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				$category_data[] = array('ids' => $res->id);
				$children = $this->getTreeLeftOrRight($res->id,0,$status,$first_date,$second_date);
				if($children)
				{
					$category_data = array_merge($children, $category_data);
				}
			}
			return $category_data;
		}
	}
	
	public function getTreeLeftOrRight_withdirectid($id,$pos,$status,$first_date,$second_date)
	{
		$category_data = array();
		$this->db->where('sponsor_id',$id);
		//$this->db->or_where('direct_id',$id); 
		if($pos!="0")
		{
			$this->db->where('postion',$pos);
		}
		$this->db->where('rdate >=', $first_date);
		$this->db->where('rdate <=', $second_date);
		if($status!="all")
		{
			$this->db->where('status',$status);
		}	
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				$category_data[] = array('ids' => $res->id);
			}
			return $category_data;
		}
	}
	
	public function getDirectTree($id,$status,$first_date,$second_date)
	{
		$category_data = array();
		$this->db->where('direct_id',$id);
		$this->db->where('rdate >=', $first_date);
		$this->db->where('rdate <=', $second_date);
		if($status!="all")
		{
			$this->db->where('status',$status);
		}	
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				$category_data[] = array('ids' => $res->id);
			}
			return $category_data;
		}
	}
	
	public function getAllDirectUsers($id)
	{
		$category_data = array();
		$this->db->where('direct_id',$id);
		$this->db->where('status',1);
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				$category_data[] = array('uid' => $res->id);
			}
			return $category_data;
		}
	}	
	function getDirectTotalIncome($pos)
	{
		$user_Ids = $this->getAllDirectUsers($pos);
		$bonus = 0;
		if(count($user_Ids)>0)
		{
			foreach($user_Ids as $user)
			{
				$invoiceData = $this->getUserInvoiceByUserID($user['uid']);//$plan['planid'];
				if(count($invoiceData)>0)
				{
					foreach($invoiceData as $inv)
					{
						if($inv->status=='paid')
						{
							$bonus += $inv->amount;
						}						
					}					
				}	
			}
			return $bonus;
		}
		else
		{
			return $bonus;
		}	
	}
	public function getUserInvoiceByUserID($id)
	{
		$this->db->where('user_id',$id);
		$this->db->where('status !=','pending');
		$invdata = $this->db->get('tbl_user_invoice')->result();
		return $invdata;
	}
		
}