<?php 
class Trackadminwork_model extends CI_Model
{
	var $paln_table = 'tbl_trackadminwork';
	public function insertData($data)
	{
		$this->db->insert($this->paln_table,$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
		//return  $returnData;
	}
	
	public function updateData($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update($this->paln_table,$data);
		return $returnData;
	}	
	
	public function getAlltrackadminwork()
	{
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}
	
	public function selecttrackadminworkByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}
	
	public function selecttrackadminworkByInvoiceID($id)
	{
		$this->db->where('invoice_id',$id);
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}


}