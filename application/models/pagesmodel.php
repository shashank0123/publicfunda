<?php

class Pagesmodel extends CI_Model

{

	function selectallpage()
	{
		$data= $this->db->get("tbl_pages");
		return $data->result();		
	}
	function insertdata($data)
	{
		$this->db->insert('tbl_pages', $data);
		return $this->db->insert_id();
	}
	function selectclintpagebyid($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_pages');
		return $data->row();
	}

	function updatemetabyid($id,$data){
		$this->db->where('id',$id);
		$this->db->update("tbl_pages",$data);
	}

	public function deleteclintimage($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->delete('our_clint');
		//return $data->result();
	}
}
?>