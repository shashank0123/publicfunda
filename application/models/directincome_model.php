<?php 
class Directincome_model extends CI_Model
{
	var $user_table = 'tbl_users';
	
	public function getTreeIncome_direct($id)
	{
		$category_data = array();
		$this->db->where('direct_id',$id);
		$this->db->where('status',1);
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				$suid = $res->id;
				$category_data[] = array('uid' => $suid);
			}
			return $category_data;
		}
	}
	
	function getBonusIncome_direct($ldate,$id)
	{
		$user_Ids = $this->getTreeIncome_direct($id);
		$bonus = 0;
		if(count($user_Ids)>0)
		{
			foreach($user_Ids as $user)
			{
				$invoiceData = $this->getUserInvoiceByUserID_direct($ldate,$user['uid']);//$plan['planid'];
				if(count($invoiceData)>0)
				{
					foreach($invoiceData as $inv)
					{
						if($inv->status=='paid')
						{
							$bonus += $inv->amount;
						}						
					}					
				}	
			}
			return $bonus;
		}
		else
		{
			return $bonus;
		}	
	}
	
	public function getPlanAmountByID_direct($id)
	{
		$this->db->where('id',$id);
		$plan = $this->db->get('tbl_plans')->result();
		return $plan[0]->planFees;
	}
	
	public function getUserInvoiceByUserID_direct($date,$id)
	{
		//$this->db->where('invDate > ',$date);
		$this->db->where('invTime > ',$date);
		$this->db->where('user_id',$id);
		$this->db->where('status !=','pending');
		//$this->db->where('bonus_status',1);
		$invdata = $this->db->get('tbl_user_invoice')->result();
		return $invdata;
	}
	
	function insertBonusIncome_direct($data)
	{
		$returnData = $this->db->insert('tbl_directincome',$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	
}
	