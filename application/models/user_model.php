<?php 
class User_model extends CI_Model
{
	var $user_table = 'tbl_users';
	public function insertData($data)
	{
		$this->db->insert($this->user_table,$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
		//return  $returnData;
	}
	
	public function updateData($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update($this->user_table,$data);
		return $returnData;
	}
	
	public function getAllUsers()
	{
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	public function query($sql)
	{
		$data = $this->db->query($sql);
		return $data->result();
	}
	
	public function insert($tbl,$data)
	{
	         $this->db->insert($tbl,$data);
		return $this->db->insert_id();
	}
	
	public function update($tbl,$data,$con)
	{
	         $this->db->where($con);
	         $this->db->update($tbl,$data);
	}
	
	public function selectUserByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	
	public function selectUserBySponsorID($id)
	{
		$this->db->where('sponsor_id',$id);
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	
	public function selectUserByIdAndOtp($id,$otp)
	{
		$this->db->where('id',$id); 
		$this->db->where('otp',$otp);
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	
	
	public function selectUserByEmail($email)
	{
		$this->db->where('email',$email);
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	
	public function selectUserByDocument($doc,$field)
	{
		if($doc=='pancard'){ $this->db->where('pancard_no',$field); }	
		if($doc=='adhaar') { $this->db->where('adhaar_no',$field); }
		if($doc=='voterid'){ $this->db->where('voterid_no',$field); }
		if($doc=='passport'){ $this->db->where('passport_no',$field); }
		
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	
	public function checkDocumentWhenEditProfile($doc,$field,$uid)
	{
		if($doc=='pancard'){ $this->db->where('pancard_no',$field); }	
		if($doc=='adhaar') { $this->db->where('adhaar_no',$field); }
		if($doc=='voterid'){ $this->db->where('voterid_no',$field); }
		if($doc=='passport'){ $this->db->where('passport_no',$field); }
		
		$this->db->where('id !=',$uid);
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	
	public function userLogin($id,$password)
	{
		$this->db->where('id',$id);
		$this->db->where('password',base64_encode($password));
		//$this->db->where('status',1);
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	
	function insertPlanInvoice($data)
	{
		$returnData = $this->db->insert('tbl_user_invoice',$data);
		return  $returnData;
	}
	
	public function gettransactiondetails($userid)
	{
		$this->db->where('uid',$userid);
		$returnData = $this->db->get('tbl_planinvoice');
		return  $returnData->result();
	}
	
	function getKycDocument($id)
	{
		$this->db->where('user_id',$id);
		$data = $this->db->get('tbl_kyc');
		return $data->result();
	}
	
	function updateKyc($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update('tbl_kyc',$data);
		return $returnData;
	}
	
	function insertKyc($data)
	{
		$returnData = $this->db->insert('tbl_kyc',$data);
		return  $returnData;
	}
	
	public function selectActiveUser()
	{
		$this->db->where('status',1);
		$this->db->order_by("registration_date", "asc");
		$data = $this->db->get($this->user_table);
		return $data->result();
	}

	public function selectAllState()
	{
		$data = $this->db->get('tbl_states');
		return $data->result();
	}
	
	public function selectAllCitiesBystateId($sid)
	{
		$this->db->where('s_id',$sid);
		$data = $this->db->get('tbl_cities');
		return $data->result();
	}
	
	public function selectCityById($sid)
	{
		$this->db->where('id',$sid);
		$data = $this->db->get('tbl_cities');
		return $data->result();
	}
	
	function insertWalletStatement($data)
	{
		$returnData = $this->db->insert('tbl_walletstatement',$data);
		return  $returnData;
	}
	
	public function getWalletStatementHistory($neftno,$uid,$first_date,$second_date)
	{
		if($neftno!="")
		{
			$this->db->where('neftNo',$neftno);
		}
		$this->db->where('user_id',$uid);
		$this->db->where('trDate >=', $first_date);
		$this->db->where('trDate <=', $second_date);
		//$this->db->order_by('invID', 'DESC');
		$data = $this->db->get('tbl_walletstatement');
		return $data->result();
	}
	
	public function getMonthlyWalletStatementCount($uid)
	{	
		$date = date('Y-m-');
		$days_in_month = cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));
		$first_date = $date.'1';
		$second_date = $date.$days_in_month;
		
		//$second_date =  
		$this->db->where('user_id',$uid);
		$this->db->where('information !=','Redeem CTP Points');
		$this->db->where('trDate >=', $first_date);
		$this->db->where('trDate <=', $second_date);			
		$data = $this->db->get('tbl_walletstatement');
		return count($data->result());
	}
	
	public function getDailyWalletStatementCount($uid)
	{	
			
		$this->db->where('user_id',$uid);
		$this->db->where('information !=','Redeem CTP Points');
		$this->db->where('trDate', date('Y-m-d'));			
		$data = $this->db->get('tbl_walletstatement');
		return count($data->result());
	}
	
	/*
	public function getTodayWalletStatementHistory($uid)
	{		
		$this->db->where('user_id',$uid);
		$this->db->where('trDate', date('Y-m-d'));			
		$data = $this->db->get('tbl_walletstatement');
		return $data->result();
	}
	*/
	
	public function downloadWalletStatementHistory($uid)
	{		
		$this->db->where('user_id',$uid);		
		$data = $this->db->get('tbl_walletstatement');
		return $data->result();
	}
	
	public function userWalletInfoForMis($uid,$information)
	{	
		if($information=='send money to friend'){ $field = 'debit_amount'; }
		if($information=='Receive from friend'){ $field = 'credit_amount'; }
	    if($information=='send money to bank'){ $field = 'debit_amount'; }
		
		$ballance = 00;
		$this->db->where('user_id',$uid);
		$this->db->where('information',$information);		
		$data = $this->db->get('tbl_walletstatement');
		foreach($data->result() as $data_result)
		{
			$ballance += $data_result->$field;
		}
		return $ballance;
	}
	
	public function getTotalNEFT($uid)
	{	
		$this->db->where('user_id',$uid);
		$this->db->where('information','send money to bank');		
		$data = $this->db->get('tbl_walletstatement');		
		return count($data->result());
	}
	
	public function totalRedeempoints($uid,$type)
	{	
		$returnData = 00;
		$this->db->where('user_id',$uid);
		$data = $this->db->get('tbl_redeempoints');
		foreach($data->result() as $data_result)
		{
			if($type=='points')
			{
				$returnData += $data_result->points;
			}
			if($type=='amount')
			{
				$returnData += $data_result->finalAmount;
			}	
		}
		return $returnData;
	}
	
	public function userTotalCrDrAmount($uid,$type)
	{	
		$returnData = 00;
		$this->db->where('user_id',$uid);
		$data = $this->db->get('tbl_walletstatement');
		foreach($data->result() as $data_result)
		{
			if($type=='cr')
			{
				$returnData += $data_result->credit_amount;
			}
			if($type=='dr')
			{
				$returnData += $data_result->debit_amount;
			}	
		}
		return $returnData;
	}
	
	function insertPlanInv($data)
	{
		$returnData = $this->db->insert('tbl_planinvoice',$data);
		return  $returnData;
	}
	public function selectPlanInvByUid($sid)
	{
		$this->db->where('uid',$sid);
		$data = $this->db->get('tbl_planinvoice');
		return $data->result();
	}
	
	public function selectPlanInvByLattest()
	{
		$this->db->order_by('id','DESC');
		$data = $this->db->get('tbl_planinvoice');
		return $data->result();
	}
	function updatePlanInv($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update('tbl_planinvoice',$data);
		return $returnData;
	}
	
	public function getPromostionalIncome($uid,$fd,$ed,$ststus)
	{
		$this->db->where('uid',$uid);
		if($fd!=""){ $this->db->where('crDate >=', $fd); }
		if($ed!=""){ $this->db->where('crDate <=', $ed); }
		//if($ststus!=""){ $this->db->where('status >=', $ststus); }		
		$data = $this->db->get('tbl_promotionalincome');
		return $data->result();
	}
	public function getDirectIncome($uid,$fd,$ed,$ststus)
	{
		$this->db->where('uid',$uid);
		if($fd!=""){ $this->db->where('crDate >=', $fd); }
		if($ed!=""){ $this->db->where('crDate <=', $ed); }
		//if($ststus!=""){ $this->db->where('status >=', $ststus); }		
		$data = $this->db->get('tbl_directincome');
		return $data->result();
	}
	public function getTreeInformation($id,$pos,$status)
	{
		$category_data = array();
		$this->db->where('sponsor_id',$id);
		if($pos!="0")
		{
			$this->db->where('postion',$pos);
		}
		$this->db->where('status',$status);
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				$category_data[] = array('planid' => $res->plan);
				$children = $this->getTreeIncome($res->id,0);
				if($children)
				{
					$category_data = array_merge($children, $category_data);
				}
			}
			return $category_data;
		}
	}
	
	public function getTreeIncome($id,$pos)
	{
		$category_data = array();
		$this->db->where('sponsor_id',$id);
		if($pos!="0")
		{
			$this->db->where('postion',$pos);
		}
		$this->db->where('status',1);
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				//if($res->direct_id=='NULL')
				//{
					$suid = $res->id;
				//}
				//else
				//{
					//$suid = 0;
				//}
				$category_data[] = array('uid' => $suid);
				$children = $this->getTreeIncome($res->id,0);
				if($children)
				{
					$category_data = array_merge($children, $category_data);
				}
			}
			return $category_data;
		}
	}
	
	function getBonusIncome($ldate,$id,$pos)
	{
		$user_Ids = $this->getTreeIncome($id,$pos);
		$bonus = 0;
		if(count($user_Ids)>0)
		{
			foreach($user_Ids as $user)
			{
				$invoiceData = $this->getUserInvoiceByUserID($ldate,$user['uid']);//$plan['planid'];
				if(count($invoiceData)>0)
				{
					foreach($invoiceData as $inv)
					{
						if($inv->status=='paid')
						{
							$bonus += $inv->amount;
						}						
					}					
				}	
			}
			return $bonus;
		}
		else
		{
			return $bonus;
		}	
	}
	
	public function getPlanAmountByID($id)
	{
		$this->db->where('id',$id);
		$plan = $this->db->get('tbl_plans')->result();
		return $plan[0]->planFees;
	}
	
	public function getUserInvoiceByUserID($date,$id)
	{
		//$this->db->where('invDate > ',$date);
		$this->db->where('invTime > ',$date);
		$this->db->where('user_id',$id);
		$this->db->where('status !=','pending');
		//$this->db->where('bonus_status',1);
		$invdata = $this->db->get('tbl_user_invoice')->result();
		return $invdata;
	}
	
	function updateUserInvoiceStatus($id)
	{
		$this->db->where('user_id',$id);
		//$data['bonus_status'] = 0;
		$returnData = $this->db->update('tbl_user_invoice',$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	
	function insertBonusIncome($data)
	{
		$returnData = $this->db->insert('tbl_promotionalincome',$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	
		
	public function getTreeLeftOrRight($id,$pos)
	{
		$category_data = array();
		$this->db->where('sponsor_id',$id);
		if($pos!="0")
		{
			$this->db->where('postion',$pos);
		}
		$this->db->where('status',1);
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				$category_data[] = array('ids' => $res->id);
				$children = $this->getTreeLeftOrRight($res->id,0);
				if($children)
				{
					$category_data = array_merge($children, $category_data);
				}
			}
			return $category_data;
		}
	}
	public function getDirectIncomeAdmin()
	{
		$this->db->order_by('id','DESC');
		$data = $this->db->get('tbl_directincome');
		return $data->result();
	}
	public function getDirectIncomeAdminByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_directincome');
		return $data->result();
	}
	public function updateDataDirectIncome($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update('tbl_directincome',$data);
		return $returnData;
	}
	public function getPromostionalIncomeAdmin()
	{
		$this->db->order_by('id','DESC');
		$data = $this->db->get('tbl_promotionalincome');
		return $data->result();
	}
	public function getPromostionalIncomeAdminByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_promotionalincome');
		return $data->result();
	}
	
	public function updateDataPromotional($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update('tbl_promotionalincome',$data);
		return $returnData;
	}
	
	public function getAllActiveOldToNewUsers()
	{
		$this->db->where('status','1');
		$this->db->order_by('id','ASC');	
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	
	public function getUserInvoiceLattestOne($id)
	{
		$this->db->where('user_id',$id);
		$this->db->order_by('id','DESC');
        $this->db->limit(1);		
		$data = $this->db->get('tbl_user_invoice');
		return $data->result();
	}
	
}