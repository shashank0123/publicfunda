<?php 

class Admin_royal_group extends CI_Model

{

	var $group_table = 'tbl_royal_groups';

	public function insertData($data)

	{

		$this->db->insert($this->group_table,$data);

		$insert_id = $this->db->insert_id();		 

		return  $insert_id;

		//return  $returnData;

	}

	

	public function updateData($id,$data)

	{

		$this->db->where('royal_group_id',$id);

		$returnData = $this->db->update($this->group_table,$data);

		return $returnData;

	}

	

	public function getAllGroup($type)

	{

		$this->db;

		$data = $this->db->get($this->group_table);

		return $data->result();

	}

	

	public function getAllActiveRewards()

	{

		$this->db->where('type','common');

		$this->db->where('status',1);

		$data = $this->db->get($this->group_table);

		return $data->result();

	}

	

	public function getAllRewardsByUser($uid)

	{

		$this->db->where('user_id',$uid);

		$this->db->where('type','special');

		$data = $this->db->get($this->group_table);

		return $data->result();

	}

	public function getAllRewards()

	{

		$data = $this->db->get($this->group_table);

		return $data->result();

	}

	

	public function selectGroupsByID($id)

	{

		$this->db->where('royal_group_id',$id);

		$data = $this->db->get($this->group_table);

		return $data->result();

	}

	

	public function deleteRewards($id)

	{

		$this->db->where('royal_group_id', $id);

		$this->db->delete('tbl_royal_groups');

	}



}