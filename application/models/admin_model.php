<?php 
class Admin_model extends CI_Model
{
	var $admin_table = 'tbl_admin';
	public function adminLogin($username,$password)
	{
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByPassword($id,$password)
	{
		$this->db->where('id',$id);
		$this->db->where('password',$password);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByUsername($username)
	{
		$this->db->where('username',$username);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	public function checkAdminByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	public function getAllSubAdmin()
	{
		$this->db->where('type',2);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByEmail($email)
	{
		$this->db->where('email',$email);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByToken($key)
	{
		$this->db->where('forgot_password',$key);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	
	public function deleteAdmin($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->delete($this->admin_table);		
	}
	 
	public function selectAdminById($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function updateAdminById($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->admin_table,$data);	
	}
	
	public function insertAdmin($data)
	{
		return $this->db->insert($this->admin_table,$data);	
	}
	
	/* awdesh code */
	public function list_plans(){
		$this->db->select("id,planName,planFees,status");
        $this->db->from('tbl_plans');
        $query = $this->db->get();

      //print_r($query);
		return $query->result();
	}
	public function edit_list_planes($id){
	 $query=$this->db->query("SELECT id,planName,planFees,totalVisitors,validity,linkPerDay,earningPerClick,capping,turnover,plancode,paymentLink FROM tbl_plans  WHERE id = $id");
        return $query->result_array();
	}
	public function update_list_plane(){
		$id=$this->input->POST('id');
		$data=array(
            'planName'=>$this->input->POST("planName"),
            'planFees'=>$this->input->POST("planFees")
			);
		$this->db->update('tbl_plans', $data);
	}

	/* manage tax */
	public function getAllTaxes($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_taxes');
		return $data->result();		
	}
	public function updateTaxes($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_taxes',$data);	
	}
	public function selectAllWalletStatement($condition = false)
	{
		//$this->db->where('id',$id);
		if ($condition){
			$this->db->where('information', "Worth of Discount Value Spent");
			$this->db->where('status', "pending");
			$this->db->order_by('user_id', "desc");
		}
		
		$data = $this->db->get('tbl_walletstatement');
		return $data->result();		
	}
	
	public function selectAllWalletAmount()
	{		
		$data = $this->db->get('tbl_users');
		$amount = 0;
		foreach($data->result() as $walletamount)
		{
			if($walletamount->wallet==0)
			{
				$wa = 0;
			}
			else
			{
				$wa = $walletamount->wallet;
			}	
			$amount += $wa;
		}
		return $amount;
	}
}