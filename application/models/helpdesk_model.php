<?php 
class Helpdesk_model extends CI_Model
{
	var $tbl_helpdesk = 'tbl_helpdesk';
	public function insertData($data)
	{
		$this->db->insert($this->tbl_helpdesk,$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	public function updateQueryData($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update($this->tbl_helpdesk,$data);
		return $returnData;
	}
	
	public function getAllQuery()
	{
		$this->db->order_by('id','DESC');
		$data = $this->db->get($this->tbl_helpdesk);
		return $data->result();
	}
	
	public function getAllStatusQuery($status)
	{
		$this->db->where('status',$status);
		$data = $this->db->get($this->tbl_helpdesk);
		return $data->result();
	}
	
	public function checkCommentOpen($uid,$type)
	{
		$this->db->where('uid',$uid);
		$this->db->where('type',$type);
		$this->db->where("(status = 'pending' OR status = 'open')");
		//$this->db->or_where('status','pending');
		$data = $this->db->get($this->tbl_helpdesk);
		return $data->result();
	}
	/*
	public function selectQueryByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->tbl_helpdesk);
		return $data->result();
	}*/
	
	public function selectQueryByUID($id)
	{
		//$this->db->where('id',$id);
		$this->db->where('uid',$id);
		$this->db->order_by('id','DESC');
		$data = $this->db->get($this->tbl_helpdesk);
		return $data->result();
	}
	public function selectQueryByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->tbl_helpdesk);
		return $data->result();
	}
	
	public function selectDepartmentByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_helpdesk_department');
		return $data->result();
	}
	public function insertDepartmentData($data)
	{
		$this->db->insert('tbl_helpdesk_department',$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	public function updateDepartmentData($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update('tbl_helpdesk_department',$data);
		return $returnData;
	}
	public function getAllDepartment()
	{
		$this->db->order_by('id','DESC');
		$data = $this->db->get('tbl_helpdesk_department');
		return $data->result();
	}
	
	public function getAllQueryComment($data)
	{
		$this->db->insert('tbl_helpdesk_comments',$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	public function selectHelpdeskCommentsByQueryID($id)
	{
		$this->db->where('query_id',$id);
		$data = $this->db->get('tbl_helpdesk_comments');
		return $data->result();
	}
}