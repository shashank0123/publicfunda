<?php 
class Royal_Group_Tree extends CI_Model
{
	var $user_table = 'tbl_users';
	var $group_table = 'tbl_royal_groups';
	var $assignment_table = 'tbl_users_to_group';
	var $transaction_table = 'tbl_royal_group_transactions';
	var $user_transaction_table = 'tbl_royal_group_user_payment';
	var $royality_group_table = 'tbl_royality_income_group';

	public function TotalMembers($group_id = false)
	{
		if (!$group_id){
			$group_id = 2;
		}
		$this->db->where('tbl_users_to_group.group_id', $group_id);
		$this->db->join('tbl_users', 'tbl_users.id = tbl_users_to_group.user_id', 'left');
		$users = $this->db->get($this->assignment_table);
		$data = $users->result(); 

		return $data;
		
	}

	public function checkEligibility($user_id, $group_id){
		$this->load->model('user_model');
		$sponsors = $this->user_model->selectUserBySponsorID($user_id);
		$group_details = $this->getGroups($group_id);
		if (isset($group_details[0])){
			if (count($sponsors) >= $group_details[0]->minimum_direct){

				$this->db->where('group_id', $group_id);
				$this->db->where('user_id', $user_id);
				$totalusers = $this->db->get($this->assignment_table);
				$totalusersresult = $totalusers->result();
				if (count($totalusersresult)>0){
					return "already-there";
				}else{
					if ($group_id > 2){
						$this->db->where('status', "1");
						$this->db->where('royal_group_id', $group_id);
						$currentgroup = $this->db->get($this->group_table);
						$currentgroupresult = $currentgroup->result();
						if (isset($currentgroupresult[0])){
							$this->db->where('group_id', $group_id-1);
							$this->db->where('user_id', $user_id);
							// $this->db->where('payment_received', '>',$currentgroupresult[0]->enter_amount);
							$this->db->where('account_status', 'Closed');
							$totalusers = $this->db->get($this->assignment_table);
							$totalusersresult = $totalusers->result();
							if (count($totalusersresult)>0){
								$money = $this->myMoney($user_id, $group_id-1);								
								if ($money >= $currentgroupresult[0]->enter_amount)
									return "eligible";
								else
									return "eligible with money";
							}
							else{
								return 'No chance';
							}
						}
						else
							return 'No chance';

					}

				}
			}

		}
		return 'No chance';
	}

	public function saveTransaction($group_id, $user_id, $transaction_type, $particulars, $amount){
		$this->db->where('group_id', $group_id);
		$this->db->order_by("transaction_id", "desc");
		$dataset = $this->db->get($this->transaction_table);
		$tax = $this->admin_model->getAllTaxes(1);
		$admin_charge = $tax[0]->admin_charge; 
		$tax = $tax[0]->tds; 
		$data = $dataset->result();
		if (count($data)>0){
			$previousamount = $data[0]->final_amount;
		}
		else
			$previousamount = 0;
		$data1['group_id'] = $group_id;
		$data1['user_id'] = $user_id;
		$data1['transaction_type'] = $transaction_type;
		$data1['particulars'] = $particulars;
		$data1['amount'] = $amount;
		$data1['admin_charge'] = $admin_charge;
		$data1['tds'] = $tax;
		$data1['final_amount'] = $amount+$previousamount;
		$data1['date'] = date('Y-m-d');
		$data1['time'] = date("h:i:sa");

		$data = $this->db->insert($this->transaction_table, $data1);
	}

	public function saveTransactionUser($group_id, $user_id, $transaction_type, $particulars, $amount, $date = false, $time = false){
		// $this->db->where('group_id', $group_id);
		$this->db->where('user_id', $user_id);
		$this->db->order_by("transaction_id", "desc");
		$dataset = $this->db->get($this->user_transaction_table);
		$tax = $this->admin_model->getAllTaxes(1);
		$admin_charge = $tax[0]->admin_charge; 
		$tax = $tax[0]->tds; 
		$data = $dataset->result();
		if (count($data)>0){
			$previousamount = $data[0]->final_amount;
		}
		else
			$previousamount = 0;
		if ($transaction_type == "Debit"){
			$final_amount1 = $previousamount - $amount;
		}
		else
			$final_amount1 = $previousamount + $amount;
		$data1['group_id'] = $group_id;
		$data1['user_id'] = $user_id;
		$data1['transaction_type'] = $transaction_type;
		$data1['particulars'] = $particulars;
		$data1['amount'] = $amount;
		$data1['admin_charge'] = $admin_charge;
		$data1['tds'] = $tax;
		$data1['final_amount'] = $final_amount1;
		$data1['created_at'] = date('Y-m-d h:i:sa');
		if ($date){
			$data1['date'] = $date;
			$data1['time'] = $time;

		}
		$data1['date'] = date('Y-m-d');
		$data1['time'] = date("h:i:sa");
		$data = $this->db->insert($this->user_transaction_table, $data1);
	}


	public function AddRefererBenefit($user_id, $group_id, $amount, $referred_user){
		$this->db->where('group_id', $group_id);
		$this->db->where('user_id', $user_id);
		$totalusers = $this->db->get($this->assignment_table);
		$totalusersresult = $totalusers->result();
		if (count($totalusersresult)>0){
			if ($totalusersresult[0]->account_status == "Initiated"){
				$data['account_status'] = "Active";
				$data['activation_date'] = date('Y-m-d H:i:sa');
			}			
			$data['payment_received'] = $totalusersresult[0]->payment_received + $amount;

			// $addtotracation = $this->saveTransaction(2, $user_id, "Debit", "Royality Income", $amount);
			$addtotracation = $this->addReferenceinTable($user_id, $referred_user, $amount);
			
			$this->db->where('group_id', $group_id);
			$this->db->where('user_id', $user_id);
			$totalusers = $this->db->update($this->assignment_table, $data);

		}

	}

	public function insertNewMember($user_id, $group_id, $status,$payment_made,$to_be_received){
		$data['user_id'] = $user_id;
		$data['group_id'] = $group_id;
		$data['account_status'] = $status;
		$data['payment_made'] = $payment_made;
		$data['to_be_received'] = $to_be_received;
		$this->db->insert($this->assignment_table, $data);
		$addtotracation = $this->saveTransaction($group_id, $user_id, "Credit", "New Member added", $payment_made);

	}

	public function addReferenceinTable($user_id, $referred_user, $amount, $date = false, $time = false){

		$tax = $this->admin_model->getAllTaxes(1);
		$admin_charge = $tax[0]->admin_charge; 
		$tax = $tax[0]->tds; 
		$this->db->where('user_id',$user_id);
		$this->db->order_by('transaction_id', 'DESC');
		$dataset = $this->db->get($this->royality_group_table);
		$data1 = $dataset->result();
		if (count($data1)>0){
			$previousamount = $data1[0]->final_amount;
		}
		else
			$previousamount = 0;

		$data['user_id'] = $user_id;
		$data['referred_user'] = $referred_user;
		$data['amount'] = $amount;
		$data['tds'] = $tax;
		$data['admin_charge'] = $admin_charge;
		$data['final_amount'] = $amount + $previousamount;
		$data['particulars'] = "Royality Income";
		$data['transaction_type'] = "Credit";
		$data['created_at'] = date('Y-m-d H:i:sa');
		if ($date){
			$data['date'] = $date;
			$data['time'] = $time;

		}
		$data['date'] = date('Y-m-d');
		$data['time'] = date("h:i:sa");
		$this->db->insert($this->royality_group_table, $data);
		
	}

	public function getGroups($id = false){
		if ($id){
			$this->db->where('status', "1");
			$this->db->where('royal_group_id', $id);
			$totalusers = $this->db->get($this->group_table);
		}
		else{
			$this->db->where('status', "1");
			$totalusers = $this->db->get($this->group_table);
		}
		
		$totalgroupresult = $totalusers->result();

		
		return $totalgroupresult;

	}

	public function groupMoney($group_id){
		if ($group_id){
		}
		else
			$group_id = 2;;
			$this->db->where('group_id', $group_id);
		if ($group_id == 2){
			$this->db->where('processed_transaction', 0);
		}
		$this->db->where('particulars', "New Member added");
		
		// $payments = $this->db->get($this->transaction_table);
		$totalusers = $this->db->get($this->transaction_table);
		$totalusersresult = $totalusers->result();
		$sum = 0;

		if (count($totalusersresult)>0){
			foreach ($totalusersresult as $key => $value) {
				$sum+=$value->amount;
			}
		}


		if ($group_id > 2){
			$this->db->where('group_id', $group_id);
		
			$payments = $this->db->get($this->user_transaction_table);
			$payments = $payments->result();
		
			$totalpayments = 0;
			foreach ($payments as $key => $value) {
				if ($value->particulars == "New Member added" || $value->particulars == "Royality Income"){
					continue;
				}
				if ($value->transaction_type == "Credit"){
					$totalpayments += $value->amount;
				}
			}
			$sum-=$totalpayments;
		}
		return $sum;
	}


	public function myMoney($user_id, $group_id = false){
		// $this->db->where('processed_transaction', 0);
		// $this->db->where('group_id', $group->royal_group_id);
		$this->db->where('user_id', $user_id);
		if ($group_id){
			// $this->db->where('group_id', $group_id);
		}
		else{
			$group_id = 2;
		}
			$this->db->where('group_id', $group_id);
			// $this->db->where('particulars','!=' ,'Transfered to Main Wallet');
			// $this->db->or_where('group_id', 0);
		// $this->db->where('transaction_type', 'Debit');

		$payments = $this->db->get($this->user_transaction_table);
		$payments = $payments->result();
		
		$totalpayments = 0;
		$joiningDate1 = $this->joiningDate($user_id, $group_id);
		$joiningDate2 = $this->joiningDate($user_id, $group_id+1);
		// var_dump($payments);
		// die;
		foreach ($payments as $key => $value) {
			// if ($value->group_id != $group_id && $value->group_id != 0)
				// continue;
			if ($value->particulars == "New Member added" || $value->particulars == "Royality Income"){
				continue;
			}
			// echo $value->transaction_type;
			if ($value->transaction_type == "Debit")
				$totalpayments -= $value->amount;
			elseif ($value->transaction_type == "Credit"){
				$totalpayments += $value->amount;
			}
		}

		$this->db->where('user_id', $user_id);
		
			$this->db->where('group_id', 0);
			if ($group_id>2){
				$this->db->where('date', '>=', $joiningDate1);
				$this->db->where('date', '<=', $joiningDate2);
			}
		
		$payments = $this->db->get($this->user_transaction_table);
		$payments = $payments->result();

		
		// $totalpayments = 0;
		// var_dump($payments);
		// die;
		foreach ($payments as $key => $value) {
			if ($value->group_id != $group_id && $value->group_id != 0)
				continue;
			if ($value->particulars == "New Member added" || $value->particulars == "Royality Income"){
				continue;
			}
			// echo $value->transaction_type;
			if ($value->transaction_type == "Debit")
				$totalpayments -= $value->amount;
			elseif ($value->transaction_type == "Credit"){
				$totalpayments += $value->amount;
			}
		}
		// die;
		return $totalpayments;
	}


	public function joiningDate($user_id, $group_id){
		$this->db->where('group_id', $group_id);
		$this->db->where('user_id', $user_id);
		$users = $this->db->get($this->assignment_table);
		$data = $users->result(); 
		if (count($data) > 0){
			return explode(' ', $data[0]->created_at)[0];
		}
		else
			return date('Y-m-d');
	}

	public function myEarnedMoney($user_id, $group_id = false){
		// $this->db->where('processed_transaction', 0);
		// $this->db->where('group_id', $group->royal_group_id);
		$this->db->where('user_id', $user_id);
		if ($group_id){
			// $this->db->where('group_id', $group_id);
		}
		else{
			$group_id = 2;
		}
			$this->db->where('group_id', $group_id);
			// $this->db->where('particulars','!=' ,'Transfered to Main Wallet');
			// $this->db->or_where('group_id', 0);
		// $this->db->where('transaction_type', 'Debit');

		$payments = $this->db->get($this->user_transaction_table);
		$payments = $payments->result();
		
		$totalpayments = 0;
		// var_dump($payments);
		// die;
		foreach ($payments as $key => $value) {
			// if ($value->group_id != $group_id && $value->group_id != 0)
				// continue;
			if ($value->particulars == "New Member added" || $value->particulars == "Royality Income"){
				continue;
			}
			// echo $value->transaction_type;
			if ($value->transaction_type == "Credit"){
				$totalpayments += $value->amount;
			}
		}

		
		// $totalpayments = 0;
		// var_dump($payments);
		// die;
		
		// die;
		return $totalpayments;
	}

	public function myroyaltyMoney($user_id, $group_id = false, $first_date = false, $second_date = false){
		// $this->db->where('processed_transaction', 0);
		// $this->db->where('group_id', $group->royal_group_id);
		$this->db->where('user_id', $user_id);
		// if ($group_id){
		// 	$this->db->where('group_id', $group_id);
		// }
		// $this->db->where('transaction_type', 'Debit');
		$payments = $this->db->get($this->royality_group_table);
		$payments = $payments->result();

		$totalpayments = 0;
		foreach ($payments as $key => $value) {
			if ($value->particulars != "Royality Income"){
				continue;
			}
			else
				$totalpayments += $value->amount;
			
		}
		// if (isset($payments[0]->payment_received))
		// 	return $payments[0]->payment_received;
		// else return 0;
		return $totalpayments;
	}

	public function myroyaltyMoneydata($user_id, $group_id = false, $first_date = false, $second_date = false){
		$this->db->where('user_id', $user_id);
		$this->db->where('date >=', $first_date);

		$this->db->where('date <=', $second_date);
		$payments = $this->db->get($this->royality_group_table);
		$payments = $payments->result();
		$totalpayments = array();
		foreach ($payments as $key => $value) {
			if ($value->particulars != "Royality Income"){
				continue;
			}
			else
				array_push($totalpayments, $value);
			
		}
		return $totalpayments;
	}

	public function getGroupStatementHistory($user_id, $group_id = false, $first_date = false, $second_date = false){
		// $this->db->where('particulars', "New Member added");
		$this->db->where('user_id', $user_id);
		$this->db->where('date >=', $first_date);

		$this->db->where('date <=', $second_date);
		$payments = $this->db->get($this->user_transaction_table);
		$payments = $payments->result();
		$totalpayments = array();
		foreach ($payments as $key => $value) {
			// if ($value->particulars == "Royality Income"){
			// 	continue;
			// }
			// else
				array_push($totalpayments, $value);
			
		}
		return $totalpayments;
	}
	public function nextPayment($group_id, $id = false){
		$this->db->where('tbl_users_to_group.group_id', $group_id);
		$this->db->where('tbl_users_to_group.account_status', 'Active');
		$this->db->order_by('tbl_users_to_group.activation_date', 'ASC');
		$this->db->join('tbl_users', 'tbl_users.id = tbl_users_to_group.user_id', 'left');
		$totalusers = $this->db->get($this->assignment_table);
		$totalusersresult = $totalusers->result();
		$sum = 0;
		if (count($totalusersresult)>0){
			if ($id){
				return $totalusersresult[0]->id;
			}
			else
			$name = $totalusersresult[0]->name;
		}
		else
			$name = "NA";
		return $name;
	}


	// public function calculateearning(){
	// 	$this->db->where('account_status', 'Active');
	// 	$data = $this->db->get($assignment_table);


		

	// }

	function unpaidmoney($group){
		if ($group->royal_group_id == 2){
			$this->db->where('processed_transaction', 0);
			$this->db->where('group_id', $group->royal_group_id);
			$this->db->where('transaction_type', 'Credit');
			$this->db->where('particulars', 'New Member added');
			$payments = $this->db->get($this->transaction_table);
			$payments = $payments->result();
			$totalpayments = 0;
			foreach ($payments as $key => $value) {
				$totalpayments += $value->amount;
			}
			return $totalpayments;
		}
		else{
			$this->db->where('group_id', $group->royal_group_id);
			$this->db->where('transaction_type', 'Credit');
			$this->db->where('particulars', 'New Member added');
			$payments = $this->db->get($this->transaction_table);
			$payments = $payments->result();
			$totalpayments = 0;
			foreach ($payments as $key => $value) {
				$totalpayments += $value->amount;
			}
			return $totalpayments;
		}
	}

	function sendmoneytouser($money, $group, $maxperheadlimit){
		$nextperson = $this->nextPayment($group->royal_group_id, 1);
		if ($nextperson == 'NA'){
			return 0;
		}
		
		$personspayment = $this->myMoney($nextperson, $group->royal_group_id);
		if ($personspayment < $maxperheadlimit){
			$amountpayable = min($maxperheadlimit, $maxperheadlimit - $personspayment);
			if ($amountpayable == $money){
				$addtotracation = $this->saveTransactionUser($group->royal_group_id, $nextperson, "Credit", "Payment to user", $amountpayable);
				return 1;
			}
			elseif ($amountpayable < $money) {
				$addtotracation = $this->saveTransactionUser($group->royal_group_id, $nextperson, "Credit", "Payment to user", $amountpayable);
				$data['account_status'] = "Closed";
				$this->db->where('group_id', $group->royal_group_id);
				$this->db->where('user_id', $nextperson);
				$totalusers = $this->db->update($this->assignment_table, $data);
				$money = $money-$amountpayable;
				$addagain = $this->sendmoneytouser($money, $group, $maxperheadlimit);
				return 1;
			}
			else{
				
				$addtotracation = $this->saveTransactionUser($group->royal_group_id, $nextperson, "Credit", "Payment to user", $money);
				
				return 1;

			}
		}

	}

	public function processmoney($group){
		$money = $this->unpaidmoney($group);
		$maxperheadlimit = $group->per_user_max;
		if ($money>0){
			$money1 = $this->sendmoneytouser($money, $group, $maxperheadlimit);
		}
		else $money1 = 0;
		if ($money1){
			$data['processed_transaction'] = 1;
		}
		else
			$data['processed_transaction'] = 0;
		$this->db->where('processed_transaction', 0);
		$this->db->where('group_id', $group->royal_group_id);
		$this->db->where('transaction_type', 'Credit');
		$payments = $this->db->update($this->transaction_table, $data);
		// die();	
	}



	public function calculationandupgrade(){
		// $data = $this->movedata();
		$getallgroup = $this->getGroups();
		foreach ($getallgroup as $key => $value) {
			$closecompleteaccount = $this->closecompletedaccount($value);
			if ($value->royal_group_id == 2)
				$todayspayment = $this->processmoney($value);
			else
				$todayspayment = $this->processgroupmoney($value);
			$upgradeusers = $this->upgradeusersofgroup($value);		
			
		}	
		$this->royalitypayment();

	}



	public function processgroupmoney($group)
	{
		$members = $this->TotalMembers($group->royal_group_id);
		if (count($members) > 0){
			$tree =	$this->createtree($members, $group);
			foreach ($members as $key => $value) {
				$level1 = array();
				$level2 = array();
				$level3 = array();
				if ($tree[$value->id]->left != ""){
					array_push($level1, $tree[$value->id]->left->id);
				}
				if ($tree[$value->id]->center != ""){
					array_push($level1, $tree[$value->id]->center->id);
				}

				if ($tree[$value->id]->right != ""){
					array_push($level1, $tree[$value->id]->right->id);
				}
				$data['group_id'] = $group->royal_group_id;
				$data['user_id'] = $value->id;
				$data['level1'] = implode(',', $level1);
				$this->db->where('user_id', $value->id);
				$this->db->where('group_id', $group->royal_group_id);
						// var_dump($group);
				$check = $this->db->get('tbl_royal_group_tree');
				$prevdata = $check->result();
				if (count($prevdata) > 0){
					$this->db->where('user_id', $value->id);
					$this->db->where('group_id', $group->royal_group_id);
					$check = $this->db->update('tbl_royal_group_tree', $data);
				}
				else{
					$data['group_id'] = $group->royal_group_id;
					$data['user_id'] = $value->id;
					$data['level1'] = implode(',', $level1);
					$this->db->insert('tbl_royal_group_tree', $data);
				}
				$this->db->where('user_id', $value->id);
				if (isset($prevdata[0]->level1)){
					$oldlevel1 = explode(',', $prevdata[0]->level1);
					if (count($oldlevel1) >= count($level1)){
						continue;
					}
					else{
						$diff = count($level1) - count($oldlevel1);
					}
					for ($i=1; $i <=$diff ; $i++) { 
						$addtotracation = $this->saveTransactionUser($group->royal_group_id, $value->id, "Credit", "Payment to user", $group->level1);
					}
				}
				else{
					$diff = count($level1);
					for ($i=1; $i <=$diff ; $i++) { 
						$addtotracation = $this->saveTransactionUser($group->royal_group_id, $value->id, "Credit", "Payment to user", $group->level1);
					}
				}
			}
		}
	}

	public function createtree($members, $group)
	{
		$tree = array();
		$pos = 0;
		$i = 0;
		// echo "<pre>";
		foreach ($members as $key => $value) {
			
				$obj = new StdClass;
				$obj->id = $value->id;
				$obj->parent = 0;
				$obj->left = "";
				$obj->center = "";
				$obj->right = "";
			$tree[$value->id] = $obj;
		}
			$key1 = 0;

			$pre = $members[$key1]->id;
			// var_dump($tree[10209]);
		foreach ($tree as $key => $value) {
			if ($pre != $value->id){
				if ($tree[$pre]->left == ""){
					$tree[$pre]->left = $value;
				}
				elseif ($tree[$pre]->center == ""){
					$tree[$pre]->center = $value;
				}

				elseif ($tree[$pre]->right == ""){
					$tree[$pre]->right = $value;
				}
				else{
					$key1++;
					$pre = $members[$key1]->id;
					$tree[$pre]->left = $value;
				}
			}
		}
		// var_dump($tree);
		$data['user_id'] = 0;
		$data['group_id'] = $group->royal_group_id;
		$data['tree'] = json_encode($tree);
		$this->db->where('group_id',  $group->royal_group_id);
		$this->db->where('user_id',  0);

		$this->db->insert('tbl_royal_group_tree', $data);
		return $tree;
	}
	public function movedata()
	{
		$dataset = $this->db->get($this->transaction_table);
		$data = $dataset->result();
		foreach ($data as $key => $value) {
			if ($value->particulars == "New Member added"){
				continue;
			}
			elseif ($value->particulars == "Payment to user"  || $value->particulars == "Payment deducted for upgrade"){
				
				$addtotracation = $this->saveTransactionUser($value->group_id, $value->user_id, $value->transaction_type, $value->particulars, $value->amount, $value->date, $value->time);
				$this->db->where('transaction_id', $value->transaction_id);
				$dataset = $this->db->delete($this->transaction_table);
				var_dump($addtotracation);
			}
			elseif ($value->particulars == "Royality Income"){
				// echo "hi";
				// continue;
				$this->addReferenceinTable($value->user_id, '10001', $value->amount, $value->date, $value->time);
				$this->db->where('transaction_id', $value->transaction_id);
				$dataset = $this->db->delete($this->transaction_table);
				// var_dump($addtotracation);
			}
			
		}
	}

	public function closecompletedaccount($group){
		$groupuser = $this->TotalMembers($group->royal_group_id);
		foreach ($groupuser as $key => $value) {

			if ($value->account_status == 'Active'){

				$mymoney = $this->myEarnedMoney($value->user_id, $group->royal_group_id);
				

				if ($mymoney >= $value->to_be_received){
					$data['account_status'] = "Closed";
					$this->db->where('group_id', $group->royal_group_id);
					$this->db->where('user_id', $value->user_id);
					$totalusers = $this->db->update($this->assignment_table, $data);
				}
			}
			else
				continue;
		}
	}

	public function royalitypayment()
	{
		$this->db->where('processed_transaction', 0);
		$this->db->where('particulars', "Royality Income");

		$dataset = $this->db->get('tbl_royality_income_group');
		$data = $dataset->result();
		$tax = $this->admin_model->getAllTaxes(1);
		foreach ($data as $key => $value) {
			$amount = $value->amount*(float)(100 - $tax[0]->tds-$tax[0]->admin_charge)/100;
			$loginUser = $this->user_model->selectUserByID($value->user_id);
			$walletAmount = $loginUser[0]->wallet;
			$walletStatement['statementId'] = $value->user_id;	//sender information			
			$walletStatement['user_id'] = $value->user_id;
			$walletStatement['another_user'] = $value->user_id;
			$walletStatement['debit_amount'] = "";
			$walletStatement['credit_amount'] = $amount;
			$walletStatement['finalAmount'] = $walletAmount+$amount;
			$walletStatement['information'] = "Royality Income credit";
			$walletStatement['trDate'] =  date('Y-m-d');
			$walletStatement['trTime'] = time(); 
			$walletStatement['tranType'] = 'credit';
			$walletStatement['status'] = 'complete';
			$this->user_model->insertWalletStatement($walletStatement);
			$data7['processed_transaction'] = 1;
			$this->db->where('transaction_id', $value->transaction_id);
			$this->db->update('tbl_royality_income_group', $data7);
			$friendUpdateData['wallet'] = $walletAmount+$amount;
			$this->user_model->updateData($value->user_id,$friendUpdateData);
		}
		

	}

	public function upgradeusersofgroup($group)
	{
		$closedusers = $this->getclosedusers($group->royal_group_id);
		
		foreach ($closedusers as $key => $value) {
			$checkEligibility = $this->checkEligibility($value->user_id, $group->royal_group_id+1);
			
			if ($checkEligibility == "already-there"){
				$data['account_status'] = "Promoted";
				$this->db->where('user_id', $value->user_id);
				$this->db->where('group_id', $group->royal_group_id);
				$this->db->update('tbl_users_to_group', $data);
			}
			elseif ($checkEligibility == "eligible") {
				$group_details = $this->getGroups($group->royal_group_id+1);
				if (isset($group_details[0]->royal_group_id)){
					$addtotracation = $this->saveTransactionUser($group->royal_group_id, $value->user_id, "Debit", "Payment deducted for upgrade", $group_details[0]->enter_amount);
					
					$addusertogroup = $this->insertNewMember($value->user_id, $group->royal_group_id+1, 'Active' ,$group_details[0]->enter_amount,$group_details[0]->per_user_max);
					return 1;
				}
			}
		}
	}

	public function getclosedusers($group_id){
		if (!$group_id){
			return 0 ;
		}
		$this->db->where('tbl_users_to_group.group_id', $group_id);
		$this->db->where('tbl_users_to_group.account_status', "Closed");
		$this->db->join('tbl_users', 'tbl_users.id = tbl_users_to_group.user_id', 'left');
		$users = $this->db->get($this->assignment_table);
		$data = $users->result();
		// $count = count($data);
			// var_dump($count);
		
		// $insert_id = $this->db->insert_id();		 

		return $data;
	}

	public function nextgroupupdate($group_id, $user_id, $force = false){



		$data['account_status'] = "Closed";
		$this->db->where('group_id', $group->royal_group_id);
		$this->db->where('user_id', $nextperson);
		$totalusers = $this->db->update($this->assignment_table, $data);

				
		$checkEligibility = $this->checkEligibility($user_id, $group_id);
		$group_details = $this->getGroups($group_id);
		if (isset($group_details[0]->royal_group_id)){
			if ($checkEligibility == 'eligible'){
				$addusertogroup = $this->insertNewMember($user_id, $group_id, 'Initiated' ,$group_details[0]->enter_amount,$group_details[0]->per_user_max);
			}
			return 1;
		}
		else{
			return 0;
		}
	}


	


	

	
	}
