<?php 
class Booster extends CI_Controller
{
	function booster_activate($uid)
	{
			$this->load->model('booster_model');						
			$user_id = $uid;
			$user = $this->user_model->selectUserByID($user_id);    // get user data by id
			
			//echo 'user date- '.$user[0]->rdate.'<br>';
			//echo 'user date- '.$this->getboosterexpiredate($user[0]->rdate).'<br>';

			if($user[0]->rdate < $this->getboosterexpiredate($user[0]->rdate))
			{
				if($user[0]->boosterused==0)
				{
					$user_invoice_total = $this->getTotalOfAllInvoice($user_id);
					if($user_invoice_total>0)
					{
						$left_income  = $this->booster_model->getBonusIncome($user_id,'L');	// get left bussiness
						$right_income  = $this->booster_model->getBonusIncome($user_id,'R');  // get right bussiness
						
						$lowerAmount = $this->getLowerAmount($left_income,$right_income);             
						$boosterlink = round($lowerAmount*($user[0]->linkPerDay/$user_invoice_total));
//echo $boosterlink;						
						if($boosterlink!=0 && !empty($boosterlink))
						{	
							$update_user['boosterused'] = 0;
							$update_user['boosterStatus'] = 1;
							$update_user['booster_task'] = $boosterlink;
							$this->user_model->updateData($user_id,$update_user);	
						}
					}				
				}
			}	
			else
			{
				echo "exp";
			}					
			
	}

	function getboosterexpiredate($date)
	{
		//$date = date("Y-m-d");
		$newDate = date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +20 day"));
		return $newDate;		
	}
	
	function getTotalOfAllInvoice($userid)
	{
		$invoiceData = $this->booster_model->getUserInvoiceByUserID($userid);
		$bonus =0;
		if(count($invoiceData)>0)
		{
			foreach($invoiceData as $inv)
			{
				if($inv->status=='paid')
				{
					$bonus += $inv->amount;
				}						
			}					
		}
		return $bonus;
	}
	
	function getLowerAmount($amount_one,$amount_one)
	{
		return min($amount_one, $amount_one);
	}
		
	function acivate()
	{
		$user_login_id = $this->session->userdata('USERID');
		if($user_login_id=="")
		{
			redirect('index.php/user/profile');
		}	
		$user_id = $user_login_id;
		$this->load->model('booster_model');
		$user = $this->user_model->selectUserByID($user_id); // user details
		if($user[0]->boosterused==0)  // check booster used or not
		{
			$user_plan = $this->booster_model->getPlanAmountByID($user[0]->plan);
			//echo $user_plan.'<br>'; boosterused
			$right_data = $this->booster_model->getBonusIncome($user_id,'R');
			$left_data = $this->booster_model->getBonusIncome($user_id,'L');		
			$lower_side = min($right_data,$left_data);
			if($lower_side>0)
			{
				$reg_date = $user[0]->rdate;//'2017-02-6';
				$exp_date = date('Y-m-d',strtotime("$reg_date +20 days"));
				if(date('Y-m-d')<$exp_date)
				{			
					if($user_plan>=$lower_side)
					{
						//echo 'user+1';
						$update_user['boosterused']= 1;	
						$update_user['boosterStatus']= 1;
						$update_user['booster_task']= round($user[0]->linkPerDay/2);						
						$this->user_model->updateData($user_id,$update_user);
					}
					else
					{
						$update_user['boosterused']= 1;
						$update_user['boosterStatus']= 1;
						$update_user['booster_task']= $user[0]->linkPerDay;						
						$this->user_model->updateData($user_id,$update_user);
					}
				}
				else
				{
					echo 'Booster Not Available';
				}	
			}
		}
        redirect($_SERVER['HTTP_REFERER']);		
	}

	function abc()
	{	
		$reg_date = '2017-02-6';
		$final_date = date('Y-m-d',strtotime("$reg_date +20 days"));
		echo 'account date ----'.$reg_date.'<br>';
		echo '+ 20 date -------'.$final_date.'<br>';
		if(date('Y-m-d')<$final_date)
		{
			echo 'available';
		}
		else
		{
			echo 'expire';	
		}	
	}	
	
}

?>