

<footer id="footer">
<div class="footer-content">
<div class="container">
<div class="row">
<div class="col-lg-4">
<div class="widget">
<div class="widget-title">Public Funda</div>
<p class="mb-5">India's First Public Funda of "Sabka Saath Sabka Vikas". Copyright © 2017. PublicFunda.</p>
<a href="http://www.publicfunda.com/user/signup" class="btn btn-inverted">Register Now</a>
</div>
</div>
<div class="col-lg-8">
<div class="row">
<div class="col-lg-4">
<div class="widget">
<div class="widget-title">Information</div>
<ul class="list">
	<?php 
	$data= $this->db->get("tbl_pages");
	$menufooter = $data->result();		

	// $menufooter = $this->getmenuitems() ?>
	<?php foreach ($menufooter as $key => $value) { ?>
<li><a href="<?php echo $value->url ?>"><?php echo $value->title ?></a></li>
<?php	} ?>

</ul>
</div>
</div>
<div class="col-lg-3">
<div class="widget">
<div class="widget-title">Features</div>
<ul class="list">
<li><a href="http://www.publicfunda.com/index.php/user/rewards">Exciting Prizes of the Month</a></li>
<li><a href="http://www.publicfunda.com/index.php/user/wallet">Check your Balance</a></li>
<li><a href="http://www.publicfunda.com/index.php/helpdesk/index">Help Desk</a></li>
<li><a href="http://www.publicfunda.com/index.php/user/royal_group">My Group</a></li>
</ul>
</div>
</div>
<div class="col-lg-3">
<div class="widget">
<div class="widget-title">Finance Calculation</div>
<ul class="list">
<li><a href="#">Wallet Statement</a></li>
<li><a href="http://www.publicfunda.com/">Home</a></li>
<li><a href="http://www.discountidea.com/">Shop</a></li>
</ul>
</div>
</div>
<div class="col-lg-2">
<div class="widget">
<div class="widget-title">Support</div>
<ul class="list">
<li><a href="http://www.publicfunda.com/index.php/helpdesk/index">Help Desk</a></li>
<li><a href="http://www.publicfunda.com/index.php/user/profile">My Profile</a></li>
<li><a href="http://www.publicfunda.com/index.php/user/kyc">KYC</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="copyright-content">
<div class="container">
<div class="copyright-text text-center">&copy; India's First Public Funda of "Sabka Saath Sabka Vikas". Copyright © 2017. PublicFunda.
All Rights Reserved.<a href="http://www.publicfunda.com/" target="_blank"> MOTIF INTERNATIONAL PVT LTD</a> </div>
</div>
</div>
</footer>

</div>


<a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

<script src="<?php echo base_url(); ?>assets/blog/js/jquery.js"></script>
 <script src="<?php echo base_url(); ?>assets/blog/plugins/metafizzy/infinite-scroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/blog/js/plugins.js"></script>

<script src="<?php echo base_url(); ?>assets/blog/js/functions.js"></script>
</body>


</html>