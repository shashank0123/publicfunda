<?php $this->load->view('blog/layout/header'); ?>
<div class="container">
	<div class="page-title">
		<h1>Blog - Category</h1>
		<div class="breadcrumb float-left">
			<ul>
				<li><a href="#">Home</a>
				</li>
				<li class="active"> <a href="#">Blogs</a>
				</li>

			</ul>
		</div>
	</div>


	<div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">


		<?php foreach($blogdata as $blog){ ?>			

			<div class="post-item border">
				<div class="post-item-wrap">
					<div class="post-image">
						<a href="<?php echo base_url('blog/details/'.$blog->id); ?>">
							<?php if ($blog->image != "") {?>
							<img alt="" src="<?php echo base_url('uploads/blog/'.$blog->image); ?>">
						<?php } else { ?>
							<?php if (isset($blog->youtube_video) && $blog->youtube_video != null){ 
								if (isset(explode('youtube.com', $blog->youtube_video)[1])){
								$youtube_id  = explode('=', $blog->youtube_video)[1];
								$youtube_id = explode('&', $youtube_id)[0];
							}
							else {
								$youtube_id = $blog->youtube_video;
								$youtube_id = explode('&', $youtube_id)[0];
							}}
								?>
							<iframe width="120" height="115" src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						<?php } ?>
						</a>
						
						<!--<span class="post-meta-category"> <a href="#"><?php //echo $blog->cat_id ?></a> </span>-->
					</div>
					<div class="post-item-description">
						<span class="post-meta-date"><i class="fa fa-calendar-o"></i><?php echo $blog->create_date ?></span>
						<span class="post-meta-comments"><a href="#"><i class="fa fa-comments-o"></i>33 Comments</a></span>
						<h2><a href="<?php echo base_url('blog/details/'.$blog->id); ?>"><?php echo substr($blog->title, 0, 250) ?>
						</a></h2>
						<p><?php echo substr($blog->content, 0, 250)?></p>
						<a href="<?php echo base_url('blog/details/'.$blog->id); ?>" class="item-link">Read More <i class="icon-chevron-right"></i></a>
					</div>
				</div>
			</div>

		<?php } ?>
	</div>


	<div id="pagination" class="infinite-scroll">
		<a href="#"></a>
	</div>


	<div id="showMore">
		<a href="#" class="btn btn-rounded btn-light"><i class="icon-refresh-cw"></i> Load More Posts</a>
	</div>
</div>
	<?php $this->load->view('blog/layout/footer'); ?>
