<?php 
header("Access-Control-Allow-Origin: *");
$this->load->view('blog/layout/header') ?>

<section class="no-padding">
	<div class="grid-articles carousel arrows-visibile" data-items="3" data-margin="0" data-dots="false">
		<?php foreach ($sliders as $key => $value) { ?>
		
		<article class="post-entry">
			<a href="<?php echo base_url().'index.php/blog/details/'.$value->id ?>" class="post-image"><img alt="" src="<?php echo base_url('uploads/blog/'.$value->image); ?>"></a>
			<div class="post-entry-overlay">
				<div class="post-entry-meta">
					<div class="post-entry-meta-category">
						<span class="badge badge-danger"><?php echo $value->cat_id ?></span>
					</div>
					<div class="post-entry-meta-title">
						<h2><a href="<?php echo base_url().'index.php/blog/details/'.$value->id ?>"><?php echo $value->title ?></a></h2>
					</div>
					<span class="post-date"><i class="far fa-clock"></i> 6m ago</span>
				</div>
			</div>
		</article>
		<?php } ?>
		
	</div>
</section>


<div class="news-ticker">
	<div class="news-ticker-title">
		<h4>BREAKING NEWS</h4>
	</div>
	<div class="carousel news-ticker-content" data-margin="20" data-items="3" data-autoplay="true" data-autoplay="3000" data-loop="true" data-arrows="false" data-dots="false" data-auto-width="true">
		<a href="http://www.publicfunda.com/plan-updates-with-image-representation">Your Success Mantra, that never been told!</a>
		<a href="http://www.publicfunda.com/contact-us">Contact us</a>
		<a href="http://www.publicfunda.com/privacy-policy">Privacy Policy</a>
		<a href="http://www.publicfunda.com/terms-and-conditions">Terms & Conditions</a>
		<a href="http://www.publicfunda.com/about-us">About Us</a>
	</div>
</div>

<section class="p-t-40 p-b-40">
	<div class="container">
		<div class="heading-text heading-line">
			<h4>HIGHTLIGHTS</h4>
		</div>
		<div class="row">
			<?php if (isset($highlight_primary->image)){ ?>
			<div class="col-lg-5">
				<div class="post-thumbnail">
					<div class="post-thumbnail-entry">
						<?php if ($highlight_primary->image != ""){ ?>
						<img alt="" src="<?php echo base_url(); ?>/uploads/blog/<?php echo $highlight_primary->image ?>">
						<?php } else { ?>							
							<img alt="" src="<?php echo base_url('uploads/blog/placeimage.jpg'); ?>">				
						<?php } ?>
						<div class="post-thumbnail-content">
							<h3><a href="<?php echo base_url().'index.php/blog/details/'.$highlight_primary->id ?>"><?php echo $highlight_primary->title ?></a></h3>
							<p> <?php echo substr($highlight_primary->content, 0, 500); ?> </p>
							<!-- <span class="post-date"><i class="far fa-clock"></i> 6m ago</span>
							<span class="post-category"><i class="fa fa-tag"></i> Technology</span> -->
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php if (isset($highlist_other)){ ?>
			<div class="col-lg-7">
				<div class="row">
					<div class="col-lg-6">
						<div class="post-thumbnail-list">
							<?php $i = 1; foreach ($highlist_other as $key => $value) { ?>
							<div class="post-thumbnail-entry">
								<?php if ($value->image != ""){ ?>
								<img alt="" src="<?php echo base_url(); ?>/uploads/blog/<?php echo $value->image ?>">
								<?php } else { ?>							
							<img alt="" src="<?php echo base_url('uploads/blog/placeimage.jpg'); ?>">				
						<?php } ?>
								<div class="post-thumbnail-content">
									<a href="<?php echo base_url().'index.php/blog/details/'.$value->id ?>"><?php echo $value->title;  $i++; ?></a>
									<!-- <span class="post-date"><i class="far fa-clock"></i> 6m ago</span>
									<span class="post-category"><i class="fa fa-tag"></i> Technology</span> -->
								</div>
							</div>

							<?php  if ($i > 8) break;?>
							<?php  if ($i == 5){?>
								
							
							
						</div>
						<div class="marketing-box">ADVERTISEMENT</div>
					</div>
					<div class="col-lg-6">
						<div class="post-thumbnail-list">
							<?php } ?>
							
							
							
							
					<?php } ?>
						</div>
						<div class="marketing-box">ADVERTISEMENT</div>
					</div>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
</section>


<section class="p-t-60 p-b-40">
	<div class="container">
		<div class="row">
						<?php $i = 1; ?>
			<?php foreach ($categorylist as $key => $value) {?>
				<?php if (!isset($value->blogs[0]->title)) continue; ?>
				<div class="col-lg-4">
					<div class="heading-text heading-line">
						<h4><?php echo $value->title ?></h4>
					</div>
					<div class="post-thumbnail">
						<div class="post-thumbnail-entry">
							<?php if ($value->blogs[0]->image != "") {?>
							<img alt="" src="<?php echo base_url(); ?>/uploads/blog/<?php echo $value->blogs[0]->image ?>">
						<?php } else { ?>							
							<img alt="" src="<?php echo base_url('uploads/blog/placeimage.jpg'); ?>">				
						<?php } ?>
							
							<div class="post-thumbnail-content">
	<!-- 							<span class="post-date"><i class="far fa-clock"></i> 6m ago</span>
								<span class="post-category"><i class="fa fa-tag"></i> Technology</span>
	 -->							<h3><a href="/index.php/blog/display/<?php echo $value->id ?>"><?php echo $value->blogs[0]->title ?></a></h3>
								<!-- <p><?php //echo $value->blogs[0]->title ?></p> -->
							</div>
						</div>
					</div>
					<div class="post-thumbnail-list">
					<?php foreach ($value->blogs as $key1 => $value1) {?>
						
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="/index.php/blog/display/<?php echo $value->id ?>"><?php echo $value1->title ?></a>
							</div>
						</div>
					<?php } ?>				
					</div>
				</div>
				<?php if ($i%3 == 0) { ?>
						</div>
					</div>
				</section>


				<section class="p-t-0 p-b-40">
					<div class="container">
						<div class="marketing-box">ADVERTISEMENT</div>
					</div>
				</section>


				
				<section class="p-t-60 p-b-40">
					<div class="container">
						<div class="row">

<?php } ?>	
			<?php $i++;} ?>
			
		</div>
	</div>
</section>


<section class="p-t-20 p-b-40">
	<div class="container">
		<div class="marketing-box">ADVERTISEMENT</div>
	</div>
</section>


<section class="p-t-0 p-b-40">
	<div class="container">
		<?php $i = 1; ?>

		<div class="row"><?php foreach ($categorylist as $key => $value) {?>
				<?php  if (!isset($value->blogs[0]->title)) continue; ?>
			<div class="col-lg-4">
				<div class="heading-text heading-line">
					<h4><?php echo $value->title ?></h4>
				</div>
				<div class="post-thumbnail-list">
<?php foreach ($value->blogs as $key1 => $value1) {?>
					<div class="post-thumbnail-entry">
						<?php if ($value1->image != "") {?>
							<img alt="" src="<?php echo base_url('uploads/blog/'.$value1->image); ?>">
						<?php } else { ?>
							<?php 
						// 	if (isset($value1->youtube_video) && $value1->youtube_video != null){ 
						// 		if (isset(explode('youtube.com', $value1->youtube_video)[1])){
						// 		$youtube_id  = explode('=', $value1->youtube_video)[1];
						// 		$youtube_id = explode('&', $youtube_id)[0];
						// 	}
						// 	else {
						// 		$youtube_id = $value1->youtube_video;
						// 		$youtube_id = explode('&', $youtube_id)[0];
						// 	}
						// }
								?>
							<img alt="" src="<?php echo base_url('uploads/blog/placeimage.jpg'); ?>">
							<!-- <iframe width="80" height="60" src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
						<?php } ?>
						<div class="post-thumbnail-content">
							<a href="<?php echo base_url('blog/details/'.$value1->id); ?>"><?php echo $value1->title ?></a>
<!-- 							<span class="post-date"><i class="far fa-clock"></i> 6m ago</span>
							<span class="post-category"><i class="fa fa-tag"></i> Technology</span>
 -->						</div>
					</div>
				<?php } ?>						
				</div>
			</div>
			<?php if ($i%3 == 0) { ?>
						</div>
					</div>
				</section>


				<section class="p-t-0 p-b-40">
					<div class="container">
						<div class="marketing-box">ADVERTISEMENT</div>
					</div>
				</section>


				<section class="p-t-0 p-b-40">
					<div class="container">
						<div class="row">

<?php } ?>	
<?php $i++;} ?>	
	
		</div>
	</div>
</section>


<section class="p-t-0 p-b-40">
	<div class="container">
		<div class="marketing-box">ADVERTISEMENT</div>
	</div>
</section>





<div class="call-to-action call-to-action-colored background-colored m-b-0">
	<div class="container">
		<div class="col-lg-10">
			<h3>Ready to Explore the Benefits of "Royal Craft Membership"?</h3>
			<p>This is the real concept of "Sabka Saath Sabka Vikas".
			Join The Royal Craft Family.</p>
		</div>
		<div class="col-lg-2"> <a href="http://www.publicfunda.com/plan-updates-with-image-representation" class="btn btn-light btn-outline">Explore...</a> </div>
	</div>
</div>

<!DOCTYPE html>
<html>
<body>
<head><meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<style>
div.cities {
    background-color: #FCF804;
    color: black;
    margin: 180px 0 180px 0;
    padding: 180px;
}
.responsive iframe {
    position: absolute;
    width: default;
    height:30%;
    }
    
</style>
</head>

<div class="cities">
  		 
<script src="https://apis.google.com/js/platform.js"></script>

<script>
  function onYtEvent(payload) {
    if (payload.eventType == 'subscribe') {
      // Add code to handle subscribe event.
    } else if (payload.eventType == 'unsubscribe') {
      // Add code to handle unsubscribe event.
    }
    if (window.console) { // for debugging only
      window.console.log('YT event: ', payload);
    }
  }
</script>
<iframe height="500" src="https://www.youtube.com/embed/_5C0KUOVwRw?autoplay=1" frameborder="10" allowfullscreen></iframe>
<br><br>

<b>Click this button to Subscribe our Youtube Channel</b>
<li><div class="g-ytsubscribe" data-channelid="UCM80rqno4ZpRcSONfTGFZFQ" data-layout="default" data-count="default" data-onytevent="onYtEvent"></div></li>
</body>
</html>

<!DOCTYPE html>
<html>
<body>
<meta name="viewport" content="width=device-width, initial-scale=1">
<div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
<html lang="en">
<html itemscope>
<head><meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<style>
div.cities {
    background-color: #FDFEFE;
    color: black;
    margin: 10px 0 10px 0;
    padding: 10px;
}

img {
  display: block;
  margin-left: auto;
  margin-right: auto;
}
.responsive {
    width: 250%;
  max-width: 400px;
  height: auto;
}
.responsive iframe {
    position: absolute;
    width: 70%;
    height:60%;
    }
    
</style>
</head>       

<body>

<div class="cities">
  		 
<script src="https://apis.google.com/js/platform.js"></script>

<script>
  function onYtEvent(payload) {
    if (payload.eventType == 'subscribe') {
      // Add code to handle subscribe event.
    } else if (payload.eventType == 'unsubscribe') {
      // Add code to handle unsubscribe event.
    }
    if (window.console) { // for debugging only
      window.console.log('YT event: ', payload);
    }
  }
</script>
         
<h1 align="left">&ldquo;Public Funda&rsquo; is a supporting solution to strength the public to grow and to be capable to play their role successfully into the concept of Startup India to earn money online with a simple method of refer to earn without investment.</h1>
<br><br>
<!--<iframe width="400" height="500" src="https://www.youtube.com/embed/_5C0KUOVwRw" frameborder="10" allowfullscreen></iframe><br><br>
<b>Click this button to Subscribe our Youtube Channel</b>
<li><div class="g-ytsubscribe" data-channelid="UCM80rqno4ZpRcSONfTGFZFQ" data-layout="default" data-count="default" data-onytevent="onYtEvent"></div></li>-->

<br><br>
<div class="g-plus" data-action="share" data-annotation="vertical-bubble" data-height="100" data-href="http://www.publicfunda.com"></div><script type="text/javascript">(function(){ var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.src = 'https://apis.google.com/js/platform.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s); })();</script><br>

<div class="fb-share-button" data-href="https://www.publicfunda.com/" data-layout="box_count" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.publicfunda.com%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>

<a href="https://twitter.com/share" class="twitter-share-button" data-via="@DiscountIdea" data-count="horizontal">Tweet</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<div class="fb-follow" data-href="https://www.facebook.com/Public-Funda-100601058295346/" data-width="100" data-height="100" data-layout="standard" data-size="large" data-show-faces="true"></div>

<div class="fb-send" 
    data-href="https://www.publicfunda.com"
    data-layout="button_count">
  </div>
<div class="fb-like" data-href="https://www.facebook.com/Public-Funda-100601058295346/" data-width="100" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
<br><br>
<p align="left">&nbsp;</p>
<p align="left">Although it is very easy to say &ldquo;Startup India&rsquo; but not too easy to make this dream true for anyone without such an investor, Bank, other investment.</p>
<p align="left">It is really tough to convince anyone to invest in your business if you are a beginner or Fresher moreover for an experienced person as well. <strong><em>How will you get an investor for your business?</em></strong></p>
<p align="left"><strong><em>Who can help you to start your &ldquo;Startup Business&rdquo;.</em></strong></p>
<p align="left">"<strong>Public Funda</strong>" is a business Idea with a concept of Equal profit sharing among every Member of group supporting the Indian Government initiative program of "Startup India" to increase Employment rate into our nation, here every member gets an opportunity to Grow and earn money online with the concept of refer to earn on our online portal <a href="https://www.publicfunda.com/">www.publicfunda.com</a>.</p>
<p align="left">&nbsp;</p>
<h2 align="left">What is Public Funda and how is it relating to Startup India ?</h2>
<p align="left"><strong><em>Public Funda</em></strong> is the name of this concept of profit sharing causing its model of <strong><em>"Sabka Saath Sabka Vikas"</em></strong> involving the mass to support Indian public to grow and <strong><em>earn money online</em></strong>.</p>
<p align="left">&nbsp;</p>
<p align="left">It is relates to <strong><em>&ldquo;Startup India&rdquo; </em></strong>by its principle of giving opportunities to every group member of it to generate online fund <strong><em>without any investment</em></strong>.</p>
<p align="left">This earned money from our online portal <a href="http://www.publicfunda.com/">www.publicfunda.com</a> is going to help everyone to start their own Business Startup. <strong><em>Is not it a wonderful and exciting information to know ?</em></strong></p>
<p align="left"><strong><em>&nbsp;</em></strong></p><br><br>

<a href="https://www.publicfunda.com">
  <img src="https://scontent.fdel3-2.fna.fbcdn.net/v/t1.0-9/p720x720/93389349_114823776851542_8830685908580696064_o.jpg?_nc_cat=110&_nc_sid=e007fa&_nc_ohc=LRO-xazLZQAAX8Xz1VV&_nc_ht=scontent.fdel3-2.fna&_nc_tp=6&oh=b7cb11fd929f06f74b2d8fbac53271f8&oe=5EBA985B" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br>
<h2 align="left">Let&rsquo;s understand the complete &ldquo;Public Funda Concept&rdquo; and how everyone is going to earn money online with the concept of refer to earn.</h2>
<p align="left">&nbsp;</p>
<p align="left">This concept is all about the &ldquo;<strong>Free Membership program</strong>&rdquo; by company <strong><em>&ldquo;Motif International Pvt Ltd&rsquo; </em></strong>on its portal <a href="http://www.publicfunda.com/">www.publicfunda.com</a>. Wherein you are supposed to get a free membership on a retail outlet of Fashion product named as <strong><em>&ldquo;Royal Craft The Fashion House&rdquo; </em></strong>by Signing up on its web portal <a href="http://www.publicfunda.com/">www.publicfunda.com</a> without any charges or Fees.</p><br>

<a href="https://www.publicfunda.com">
  <img src="https://scontent.fdel3-2.fna.fbcdn.net/v/t1.0-9/p720x720/93711441_114823716851548_7058149516792299520_o.jpg?_nc_cat=105&_nc_sid=e007fa&_nc_ohc=vCeGITOdqbIAX9uy6h5&_nc_ht=scontent.fdel3-2.fna&_nc_tp=6&oh=3059fcdbbc34fcd4668f11532d225872&oe=5EBC5FAC" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br>
<p align="left">&nbsp;</p>
<h3 align="left">There below kind of Earning opportunities are available on this web portal for every Group member who has <a href="http://www.publicfunda.com/">Created an account</a> on our web portal <a href="http://www.publicfunda.com/">www.publicfunda.com</a></h3>
<p align="left">&nbsp;</p>
<h4 align="left">A Cash Amount of Rs.4500/-</h4>
<p align="left">A Cash Amount of Rs.4500/- is given to every member in this group who refers only 1 more member for shopping of Rs.1500/-</p><br><br>

<a href="https://www.publicfunda.com">
  <img src="https://scontent.fdel3-1.fna.fbcdn.net/v/t1.0-9/p720x720/93257838_114823970184856_5401063928979521536_o.jpg?_nc_cat=103&_nc_sid=e007fa&_nc_ohc=vTz6eCuwLAQAX-_qNYV&_nc_ht=scontent.fdel3-1.fna&_nc_tp=6&oh=b1293e11764374225644e2eaa34725e1&oe=5EBC37CD" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br>
<h4 align="left">A Lifetime Royalty Refer income as 10% of shopping value by the member referred by you.</h4>
<p align="left">When you refer only 1 member as said above, you Earn Rs.4500/- as well as 10% of shopping value spent by the member referred by you.</p>
<p align="left">Not only one time rather it is a Lifetime Royalty as 10% of all his shopping is always credit to your bank account regularly on every shopping instantly in your wallet.</p><br><br>

<a href="https://www.publicfunda.com">
  <img src="https://scontent.fdel3-1.fna.fbcdn.net/v/t1.0-9/p720x720/92595993_114823910184862_2946474743332601856_o.jpg?_nc_cat=104&_nc_sid=e007fa&_nc_ohc=6uUxiRS6WhAAX9xY79D&_nc_ht=scontent.fdel3-1.fna&_nc_tp=6&oh=83680a9a531e69644bc55bff65698aef&oe=5EBADFF4" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>
<p align="left">&nbsp;</p>
<h4 align="left">An Endless Group Income of INR 2 Crore 40 Lack and more.</h4>
<p align="left">Here once you have earned your first income of Rs.4500/- from this group. You get Eligible to Enter into the Group income wherein every group will give you a certain Amount to Earn and/or Spend.<br /> Here you get through a series of Endless Groups and in every group you earn a lot of amount and total you earn INR 2crore 40 lacks.</p><br><br>

<a href="https://www.publicfunda.com">
  <img src="https://scontent.fdel3-1.fna.fbcdn.net/v/t1.0-9/p720x720/92446132_114824320184821_7158547186244911104_o.jpg?_nc_cat=103&_nc_sid=e007fa&_nc_ohc=5AdEa1GZTcoAX-BObK4&_nc_ht=scontent.fdel3-1.fna&_nc_tp=6&oh=a92d9dc7c00bc43eb7ece90cf3124a72&oe=5EBC2F6C" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>
<p align="left">&nbsp;</p>
<p align="left">&nbsp;</p>
<p align="left">&nbsp;</p>
<h2 align="left">What should you do to earn money online with the concept of refer to earn?</h2>
<p align="left">&nbsp;</p>
<p align="left">Sign-up:</p>
<p align="left">First of all you just need to Register yourself by Sign-up on www.PublicFunda.com at the top of &nbsp;&nbsp;the website. <strong><em>(Now you are the Registered on website and has got your - Inactive user-id)</em></strong></p>
<p align="left">&nbsp;</p>
<p align="left">&nbsp;</p>
<p align="left">Get the Membership:</p>
<p align="left">Here this membership and service of "Earn from home" are available only for the customers of the Company (Motif International Pvt Ltd). You must make your first purchase from this company only one time for Rs.1500/- . (<strong><em>Now you are the Registered Member of the Company with an -initiated user-id)</em></strong></p>
<p align="left">&nbsp;</p>
<p align="left">Refer only one customer:</p>
<p align="left">If you are satisfied with our product quality and rates, give your feedback to one other customer about our products and its quality and Rates. If he is satisfied with you, please refer him to purchase for Rs.1500/- like you have done the same. If your only one referred person has made a purchase of Rs.1500/-, <strong><em>(Now your User-id has got Active and you are eligible to Earn an Amount of Rs.4500/- also you have got a permanent team member who's lifetime purchasing will give you a "Life Time Royalty Reward" of 10% of Purchasing amount every time and ever made by your team member)</em></strong></p>
<p align="left">&nbsp;</p>
<h3 align="left">How to earn fast and huge Amount to get Income more than of INR 20 Crores and 40 Lacs?</h3>
<p align="left">Here your Earning Methods are not limited. Your world of possibilities is endless and here you have many opportunities to earn more and multiply your income many times with Group Methods and "Life Time Royalty Reward" Method.</p>
<p align="left">&nbsp;</p>
<h4 align="left">Increase your "Life Time Royalty Reward&rdquo;:</h4>
<p align="left">Although here you need to refer only one Customer to be Eligible for all the income plans available into the concept Example as "Life time royalty Reward" "multi group income". But you may refer an unlimited number of paid customers to increase probability of your Regular income "Life Time Royalty Reward". It also Reduce your time to reach to your Earning Goal and Increase your "Group Income" in multiply method&rdquo;</p><br><br>

<h2>Royal Craft | Promotional Income Plan</h2>
<a href="https://www.publicfunda.com/user/signup">
  <img src="https://scontent.fdel3-1.fna.fbcdn.net/v/t1.0-9/93179177_114813990185854_3177701755186053120_n.jpg?_nc_cat=102&_nc_sid=e007fa&_nc_ohc=Q3lz-YAcZqIAX9PQp4b&_nc_ht=scontent.fdel3-1.fna&oh=72ee86aaf1b42d5ab1b2e3dcbe522606&oe=5EBB02B1" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>

<a href="https://www.publicfunda.com/user/signup">
  <img src="https://scontent-del1-1.xx.fbcdn.net/v/t1.0-9/92946495_115885046745415_8204104906567581696_n.png?_nc_cat=106&_nc_sid=e007fa&_nc_ohc=rg3MJEOx7UUAX_m4znC&_nc_ht=scontent-del1-1.xx&oh=04289cee28330023be7e75137a4e7292&oe=5EBE8D1C" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>

<a href="https://www.publicfunda.com/user/signup">
  <img src="https://scontent.fdel3-1.fna.fbcdn.net/v/t1.0-9/92801375_114813730185880_3852082375949287424_n.jpg?_nc_cat=100&_nc_sid=e007fa&_nc_ohc=KSoZ9pkvSeYAX_qs-PW&_nc_ht=scontent.fdel3-1.fna&oh=00b73da61e843e80c7da81442883d06a&oe=5EB93BAE" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>

<a href="https://www.publicfunda.com/user/signup">
  <img src="https://scontent-del1-1.xx.fbcdn.net/v/t1.0-9/92829440_114813683519218_109557949931716608_n.jpg?_nc_cat=105&_nc_sid=e007fa&_nc_ohc=zMiw-hkPvCMAX-66bX6&_nc_ht=scontent-del1-1.xx&oh=99be608a008644a5aed24d1b5a8e189d&oe=5EBF7445" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>

<a href="https://www.publicfunda.com/user/signup">
  <img src="https://scontent.fdel3-1.fna.fbcdn.net/v/t1.0-9/92568438_114813926852527_2388201747303628800_n.jpg?_nc_cat=106&_nc_sid=e007fa&_nc_ohc=m7KHLR9m7TwAX9LUbRi&_nc_ht=scontent.fdel3-1.fna&oh=a9d763d8950d5c3ea01e9ffd9060c6ee&oe=5EBC67FF" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>

<a href="https://www.publicfunda.com/user/signup">
  <img src="https://scontent.fdel3-1.fna.fbcdn.net/v/t1.0-9/93618066_114813963519190_922304670167203840_n.jpg?_nc_cat=106&_nc_sid=e007fa&_nc_ohc=wzPvf6cBLpAAX9l-nx_&_nc_ht=scontent.fdel3-1.fna&oh=0b759246f9cf64d62fe7f095b5c17517&oe=5EBB7BA5" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>

<a href="https://www.publicfunda.com/user/signup">
  <img src="https://scontent.fdel3-1.fna.fbcdn.net/v/t1.0-9/93376543_114813796852540_395050585079939072_n.jpg?_nc_cat=103&_nc_sid=e007fa&_nc_ohc=pyd-eBjc2NUAX-ZaaG4&_nc_ht=scontent.fdel3-1.fna&oh=7c38f2d0e6b0a421d1dcfa9a0486abea&oe=5EBCBF99" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>


<a href="https://www.publicfunda.com/user/signup">
  <img src="https://scontent-del1-1.xx.fbcdn.net/v/t1.0-9/93640540_114813663519220_1313320979581108224_n.jpg?_nc_cat=106&_nc_sid=e007fa&_nc_ohc=HWl0AekhMsgAX-jvFfV&_nc_ht=scontent-del1-1.xx&oh=3dc1cca8a7967b9157b90ff65c2225b1&oe=5EBF8078" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>

<a href="https://www.publicfunda.com/user/signup">
  <img src="https://scontent-del1-1.xx.fbcdn.net/v/t1.0-9/93253458_114813703519216_7296360776829239296_n.jpg?_nc_cat=103&_nc_sid=e007fa&_nc_ohc=9QKGwvjc2FMAX-gFzGo&_nc_ht=scontent-del1-1.xx&oh=e0c2c9dd69214c1e6c75bf733c04547d&oe=5EBC173A" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>
<br><br><br>

<a href="https://www.publicfunda.com/user/signup">
  <img src="https://scontent.fdel3-1.fna.fbcdn.net/v/t1.0-9/92962916_114813646852555_6246495650985279488_n.jpg?_nc_cat=100&_nc_sid=e007fa&_nc_ohc=yAhq53jjYXEAX_hqcna&_nc_ht=scontent.fdel3-1.fna&oh=62e15a97fe2ca47620a7931334763fd5&oe=5EBC1983" alt="Royal Craft : The Fashion House" style="width:300px;border:1;">
</a>

</div>
<p align="left">&nbsp;</p>
<h4 align="left">Click the Link Below for Registration:</h4>
<p align="left"><a href="../../user/signup">https://www.publicfunda.com/user/signup</a></p>
<p align="left">&nbsp;</p>
<h4 align="left">Click the Link Below for More Information :</h4>
<p align="left"><a href="../../plan-updates-with-image-representation">https://www.publicfunda.com/plan-updates-with-image-representation</a></p>
<h3 align="left">Best Helpdesk Solution</h3>
<p align="left"><a href=>https://www.publicfunda.com/user/signup</a></p>
<p align="left">&nbsp;</p>
<p align="left">&nbsp;</p>
<h2 align="left">For more Information:</h2>
<p align="left">Please Contact: <strong><em>+91-9911391463</em></strong></p>
Email-ID: <strong><em>indiandiscountidea@gmail.com</em></strong>

<div class="fb-comments" data-href="http://www.publicfunda.com/" data-width="100%" data-numposts="50"></div>

<!--<centre><div class="fb-page" data-href="https://www.facebook.com/Discount-Idea-712660268934928/" data-tabs="timeline"  data-width="500" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Discount-Idea-712660268934928/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Discount-Idea-712660268934928/">Discount Idea</a></blockquote></centre></div>-->



<div class="fb-quote"></div></div>


<?php $this->load->view('blog/layout/footer') ?>
</body>
</html>

    
  </section>	
<script>