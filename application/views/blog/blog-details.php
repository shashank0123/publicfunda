<?php $this->load->view('blog/layout/header'); ?>
<section id="page-content" class="sidebar-right">
	<div class="container">
		<div class="row">

			<div class="content col-lg-9">

				<div id="blog" class="single-post">

					<div class="post-item">
						<div class="post-item-wrap">
							<?php if (isset($blogdata[0]->youtube_video) && $blogdata[0]->youtube_video != null){ 
								if (isset(explode('youtube.com', $blogdata[0]->youtube_video)[1])){
								$youtube_id  = explode('=', $blogdata[0]->youtube_video)[1];
								$youtube_id = explode('&', $youtube_id)[0];
							}
							else {
								$youtube_id = $blogdata[0]->youtube_video;
								$youtube_id = explode('&', $youtube_id)[0];
							}
								?>
							<div class="post-image">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						<?php } ?>
							<div class="post-image">
								<a href="#">
									<img alt="" src="<?php echo base_url().'uploads/blog/'.$blogdata[0]->image ?>">
								</a>
							</div>
							<div class="post-item-description">
								<h2><?php echo $blogdata[0]->title; ?></h2>
								<div class="post-meta">
									<?php if ($blogdata[0]->user_id != ''){ $user_id = $blogdata[0]->user_id;?>
									<span class="post-meta-date"><a href="<?php echo base_url('index.php/userblog/myposts/'.$user_id); ?>"><?php if ($image != "") {?><img src="<?php echo base_url()."uploads/user/".$image ?>"><?php } else { ?><i style="font-size: 5rem" class="fa fa-user"></i><?php } ?><span style="color: blue;"><?php  echo $username; ?></span></a></span>
								<?php }else { ?>
									<span class="post-meta-date"><i style="font-size: 5rem" class="fa fa-user"></i><?php  echo " Admin" ?></span>
								<?php } ?>
									<span class="post-meta-date"><i class="fa fa-calendar-o"></i><?php echo $blogdata[0]->create_date ?></span>
									<span class="post-meta-comments"><a href="#"><i class="fa fa-comments-o"></i>33 Comments</a></span>
									<span class="post-meta-category"><a href="#"><i class="fa fa-tag"></i><?php echo $category[0]->title ?></a></span>
									<?php if ($abuse == 0) { ?>
									<span class="post-meta-category"><a href="<?php echo base_url().'index.php/blog/inappropriate/'.$blogdata[0]->id ?>"><i class="fa fa-danger"></i>Report Abuse</a></span>
								<?php } ?>
									<div class="post-meta-share">
										<a class="btn btn-xs btn-slide btn-facebook" href="#">
											<i class="fab fa-facebook-f"></i>
											<span>Facebook</span>
										</a>
										<a class="btn btn-xs btn-slide btn-twitter" href="#" data-width="100">
											<i class="fab fa-twitter"></i>
											<span>Twitter</span>
										</a>
										<a class="btn btn-xs btn-slide btn-instagram" href="#" data-width="118">
											<i class="fab fa-instagram"></i>
											<span>Instagram</span>
										</a>
										<a class="btn btn-xs btn-slide btn-googleplus" href="mailto:#" data-width="80">
											<i class="icon-mail"></i>
											<span>Mail</span>
										</a>
									</div>
								</div>
								<p><?php echo $blogdata[0]->content; ?></p>
							</div>
							<div class="post-tags">
								<a href="#">My Blog Library</a>
								<a href="#">My Video Collection</a>
								<a href="#">My Community</a>
								<a href="#">Coming Soon</a>
							</div>
							<div class="post-navigation">
								<a href="blog-single-slider.html" class="post-prev">
									<div class="post-prev-title"><span>Previous Post</span>Post with a slider and lightbox</div>
								</a>
								<a href="blog-masonry-3.html" class="post-all">
									<i class="icon-grid"> </i>
								</a>
								<a href="blog-single-video.html" class="post-next">
									<div class="post-next-title"><span>Next Post</span>Post with YouTube Video</div>
								</a>
							</div>
							<?php if ($this->session->userdata('USERID') != null){ ?>
								<div class="respond-form" id="respond">
									<div class="respond-comment">

										Leave a <span>Comment </span></div>
										<form class="form-gray-fields" method="POST">

											<div class="row">
												<div class="col-lg-12">
													<div class="form-group">
														<label class="upper" for="comment">Your comment</label>
														<textarea class="form-control required" name="comment" rows="9" placeholder="Enter comment" id="comment" aria-required="true"></textarea>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group text-center">
														<button class="btn" name="submitcomment" type="submit">Submit Comment</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>

							<?php } ?>

							<div class="comments" id="comments">
								<div class="comment_number">
									Comments <span>(<?php if (isset($comments)) echo count($comments); else echo 0; ?>)</span>
								</div>
								<div class="comment-list">
									<?php  if (isset($comments)) foreach ($comments[0] as $key => $value) {?>
										

										<div class="comment" id="comment-1">
											<div class="image"><img alt="" src="images/blog/author.jpg" class="avatar"></div>
											<div class="text">
												<h5 class="name"><?php echo $value->user_id ?></h5>
												<span class="comment_date">Posted at <?php echo $value->date_added ?></span>
												<a class="comment-reply-link" onclick="togglediv(reply<?php echo $value->comment_id?>)" href="#">Reply</a>
												<form class="form-gray-fields" method="POST">

													<div id="reply<?php echo $value->comment_id ?>" style="display: none" class="row">
														<div class="col-lg-12">
															<div class="form-group">
																<label class="upper" for="comment">Your comment</label>
																<input type="hidden" name="comment_id" value="<?php echo $value->comment_id ?>">
																<textarea class="form-control required" name="comment" rows="3" placeholder="Enter comment" id="comment" aria-required="true"></textarea>
															</div>
														</div>
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group text-center">
																	<button class="btn" name="submitcomment" type="submit">Submit Comment</button>
																</div>
															</div>
														</div>
													</div>
												</form>
												<div class="text_holder">
													<p><?php echo $value->comment ?></p>
												</div>

												<?php if (isset($comments[$value->comment_id])) foreach ($comments[$value->comment_id] as $key1 => $value1) {?>


													<div class="comment" id="comment-1-1">
														<div class="image"><img alt="" src="images/blog/author2.jpg" class="avatar"></div>
														<div class="text">
															<h5 class="name"><?php echo $value1->user_id ?></h5>
															<span class="comment_date">Posted at <?php echo $value1->date_added ?></span>
															<div class="text_holder">
																<p><?php echo $value1->comment ?></p>
															</div>
														</div>
													</div>


												<?php } ?>
											</div>
										</div>
									<?php } else echo ""; ?>					
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="sidebar sticky-sidebar col-lg-3">
						<div class="widget  widget-newsletter">
							<form id="widget-search-form-sidebar" action="https://inspirothemes.com/polo/search-results-page.html" method="get">
								<div class="input-group">
									<input type="text" aria-required="true" name="q" class="form-control widget-search-form" placeholder="Search for pages...">
									<div class="input-group-append">
										<button class="btn" type="submit"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form></div>


							<div class="widget">
								<div class="tabs">
									<ul class="nav nav-tabs" id="tabs-posts" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" id="home-tab" data-toggle="tab" href="#popular" role="tab" aria-controls="popular" aria-selected="true">Popular</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="profile-tab" data-toggle="tab" href="#featured" role="tab" aria-controls="featured" aria-selected="false">Featured</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="contact-tab" data-toggle="tab" href="#recent" role="tab" aria-controls="recent" aria-selected="false">Recent</a>
										</li>
									</ul>
									<div class="tab-content" id="tabs-posts-content">
										<div class="tab-pane fade show active" id="popular" role="tabpanel" aria-labelledby="popular-tab">
											<div class="post-thumbnail-list">
												<div class="post-thumbnail-entry">
													<img alt="" src="images/blog/thumbnail/5.jpg">
													<div class="post-thumbnail-content">
														<a href="#">A true story, that never been told!</a>
														<span class="post-date"><i class="icon-clock"></i> 6m ago</span>
														<span class="post-category"><i class="fa fa-tag"></i> Technology</span>
													</div>
												</div>
												<div class="post-thumbnail-entry">
													<img alt="" src="images/blog/thumbnail/6.jpg">
													<div class="post-thumbnail-content">
														<a href="#">Beautiful nature, and rare feathers!</a>
														<span class="post-date"><i class="icon-clock"></i> 24h ago</span>
														<span class="post-category"><i class="fa fa-tag"></i> Lifestyle</span>
													</div>
												</div>
												<div class="post-thumbnail-entry">
													<img alt="" src="images/blog/thumbnail/7.jpg">
													<div class="post-thumbnail-content">
														<a href="#">The most happiest time of the day!</a>
														<span class="post-date"><i class="icon-clock"></i> 11h ago</span>
														<span class="post-category"><i class="fa fa-tag"></i> Lifestyle</span>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="featured" role="tabpanel" aria-labelledby="featured-tab">
											<div class="post-thumbnail-list">
												<div class="post-thumbnail-entry">
													<img alt="" src="images/blog/thumbnail/6.jpg">
													<div class="post-thumbnail-content">
														<a href="#">Beautiful nature, and rare feathers!</a>
														<span class="post-date"><i class="icon-clock"></i> 24h ago</span>
														<span class="post-category"><i class="fa fa-tag"></i> Lifestyle</span>
													</div>
												</div>
												<div class="post-thumbnail-entry">
													<img alt="" src="images/blog/thumbnail/7.jpg">
													<div class="post-thumbnail-content">
														<a href="#">The most happiest time of the day!</a>
														<span class="post-date"><i class="icon-clock"></i> 11h ago</span>
														<span class="post-category"><i class="fa fa-tag"></i> Lifestyle</span>
													</div>
												</div>
												<div class="post-thumbnail-entry">
													<img alt="" src="images/blog/thumbnail/8.jpg">
													<div class="post-thumbnail-content">
														<a href="#">New costs and rise of the economy!</a>
														<span class="post-date"><i class="icon-clock"></i> 11h ago</span>
														<span class="post-category"><i class="fa fa-tag"></i> Lifestyle</span>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="recent" role="tabpanel" aria-labelledby="recent-tab">
											<div class="post-thumbnail-list">
												<div class="post-thumbnail-entry">
													<img alt="" src="images/blog/thumbnail/7.jpg">
													<div class="post-thumbnail-content">
														<a href="#">The most happiest time of the day!</a>
														<span class="post-date"><i class="icon-clock"></i> 11h ago</span>
														<span class="post-category"><i class="fa fa-tag"></i> Lifestyle</span>
													</div>
												</div>
												<div class="post-thumbnail-entry">
													<img alt="" src="images/blog/thumbnail/8.jpg">
													<div class="post-thumbnail-content">
														<a href="#">New costs and rise of the economy!</a>
														<span class="post-date"><i class="icon-clock"></i> 11h ago</span>
														<span class="post-category"><i class="fa fa-tag"></i> Lifestyle</span>
													</div>
												</div>
												<div class="post-thumbnail-entry">
													<img alt="" src="images/blog/thumbnail/6.jpg">
													<div class="post-thumbnail-content">
														<a href="#">Beautiful nature, and rare feathers!</a>
														<span class="post-date"><i class="icon-clock"></i> 24h ago</span>
														<span class="post-category"><i class="fa fa-tag"></i> Lifestyle</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="widget widget-tweeter" data-username="envato" data-limit="2">
								<h4 class="widget-title">Recent Tweets</h4>
							</div>


							<div class="widget  widget-tags">
								<h4 class="widget-title">Tags</h4>
								<div class="tags">
									<a href="#">Design</a>
									<a href="#">Portfolio</a>
									<a href="#">Digital</a>
									<a href="#">Branding</a>
									<a href="#">HTML</a>
									<a href="#">Clean</a>
									<a href="#">Peace</a>
									<a href="#">Love</a>
									<a href="#">CSS3</a>
									<a href="#">jQuery</a>
								</div>
							</div>

						</div>

					</div>
				</div>
			</section>
			<?php 
			$this->load->view('blog/layout/footer')
			?>
			<script>
				function togglediv(divid) {
					event.preventDefault();
					console.log(divid)
				// $('#'+divid).show();
				$(divid).toggle();
			}
		</script>
