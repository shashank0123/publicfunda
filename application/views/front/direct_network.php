<?php $this->load->view('front/layout/header-inner'); ?>
	<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>My Network</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>My Team Network</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12">
							<section class="panel">
									<header class="panel-heading">	
											<h2 class="panel-title"><a class="btn btn-danger" href="<?php echo base_url('index.php/user/direct_network'); ?>">My Direct Network </a><a class="btn btn-danger" href="<?php echo base_url('index.php/user/royal_group'); ?>"> The Royal Craft Group</a>			
											</h2> 
									
											
									</header>
									
									<div class="panel-body">
										 <div class="tree" id="treedata">
											<ul>
												<li>
													<a href="#" class="thumbnail1">
														<center>
															<img  src="<?php echo base_url(''); ?>assets/front/images/activate.png" style="height:30px;width:30px;">
														</center><?php echo $login_user[0]->name; ?>
													</a>
													<div id="title">
														<span id="ctl00_ContentPlaceHolder1_lbl0_Root">
															<table style="text-align:Left;">
																<tbody>
																	<tr>
																		<td width="100px">Name</td>
																		<td width="5px">:</td>
																		<td style="padding-left:5px;" width="160px"><?php echo $login_user[0]->name; ?></td>
																	</tr>																			
																	<tr>
																		<td>User ID</td>
																		<td>:</td>
																		<td style="padding-left:5px;"> <?php echo $login_user[0]->id; ?> </td>
																	</tr>																			
																	<tr>
																		<td>Sponsor ID</td>
																		<td>:</td>
																		<td style="padding-left:5px;">  <?php 
																				if($login_user[0]->direct_id=='NULL')
																				{
																					echo $login_user[0]->sponsor_id;
																				}
																				else
																				{
																					echo $login_user[0]->direct_id;
																				}	
																				?> </td>
																	</tr>																			
																	<tr>
																		<td>Date Of Registration</td> 
																		<td>:</td>
																		<td style="padding-left:5px;"> <?php echo date('d-M-Y',$login_user[0]->registration_date); ?></td>
																	</tr>																	
																</tbody>
															</table>
														</span>
													</div>													
													<?php echo $this->tree_model->getDirectTreeData($login_user[0]->id); ?>
												</li>
											</ul>
										</div>
									</div>
								</section>                         
						</div>
						

					</div>
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
					<!-- end: page -->
				</section>
			</div>
			
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<script>
			function openChildTree(UID)
			{
				 jQuery.ajax({
						url: "<?php echo base_url('index.php/user/getchildtree'); ?>",
						data:'UID='+UID,
						beforeSend: function() {  $('#treedata').html('<i class="fa fa-spinner faa-spin animated"></i> Loading ...'); },
						type: "POST",
						success:function(treeData)
						{								
						   $('#treedata').html(treeData);
						},
						error:function (){}
					});	
			}
		</script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	</body>


</html>