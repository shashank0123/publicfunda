<?php $this->load->view('front/layout/header-inner'); ?>
<style>
.search-results-list {
    max-width: 100%;
}
</style>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Help Desk</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Help Desk</span></a></li>
								
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12">
							<?php echo $this->session->flashdata('message'); ?>
							<a href="<?php echo base_url('index.php/helpdesk/index'); ?>" class="pull-right btn btn-info">Back</a>
							<div class="tabs">
								<ul class="nav nav-tabs tabs-danger">
									<li class="active"><a href="#query-status" data-toggle="tab">Query View</a></li>									
								</ul>
								
								<div class="tab-content">
								<?php
									$oprator = $this->helpdesk_model->selectDepartmentByID($queryData[0]->type);
								?>
								<div id="query-status" class="tab-pane active">        
									<table class="table table-bordered table-striped table-condensed mb-none">
										<tr>
											<td><strong>Department</strong></td>
											<td><?php echo(!isset($oprator[0]->title))?'N/A':$oprator[0]->title; ?></td>
										</tr>
										<tr>
											<td><strong>Ticket ID</strong></td>
											<td>MOTIF<?php echo $queryData[0]->id; ?></td>
										</tr>
										<tr>
											<td><strong>Subject</strong></td>
											<td><?php echo $queryData[0]->subject; ?></td>
										</tr>	
																			
										<tr>
											<td><strong>Status</strong></td>
											<td><?php echo $queryData[0]->status; ?></td>
										</tr>
										<tr>
											<td><strong>Executive</strong></td>
											<td><?php echo(!isset($oprator[0]->name))?'N/A':$oprator[0]->name; ?></td>
										</tr>
										<tr>
											<td><strong>Date</strong></td>
											<td><?php echo date('d-m-Y h:i a',$queryData[0]->queryTime); ?></td>
										</tr>
										<tr>
											<td><strong>Query</strong></td>
											<td><?php echo $queryData[0]->query; ?></td>
										</tr>
									</table>
									<?php 
										if($queryData[0]->status=='close')
										{
											$formOption = 'disabled';
										}
										else
										{
											$formOption = '';
										}	
									?>
						            <form method="post" <?php echo $formOption; ?>>
											<input type="hidden" name="depatyment" value="<?php echo $queryData[0]->type; ?>">
											<table class="table table-bordered table-striped" id="datatable-default">
												<tbody>										
													<tr>
														<td><strong>Comment</strong></td>
														<td><textarea name="comment" class="form-control" required <?php echo $formOption; ?> ></textarea></td>										
													 </tr>	
													 <tr>
														<td colspan="2">
															<button class="btn btn-info pull-right" type="submit" name="submitComment" <?php echo $formOption; ?> >
																Submit
															</button>
														</td>
													 </tr>
												</tbody>
											</table>
										</form>
								</div>  <br>                                 
                                <ul class="list-unstyled search-results-list">
											<?php if(count($query_comments)>0){ ?>
											<?php foreach($query_comments as $comments){ ?>
											<li>												
												<span>
													<i class="fa fa-user"></i> By <?php echo($comments->comment_type=='admin')?'admin':'User'; ?>
													&nbsp;&nbsp;
													<i class="fa fa fa-clock-o"></i> <?php echo date('d-m-Y h:s a',$comments->comment_time); ?>
												</span>
												<div class="result-data">														
													<p class="description">
														<?php echo $comments->comment ?>
													</p>
												</div>
											</li>
											<?php } ?>
											<?php } ?>
							</ul>  
                                    
                                   
								</div>
							</div>
                         
						</div>
						

					</div>									
					<!--footer start-->
					<?php $this->load->view('front/layout/footer'); ?>	
					<!--end start-->
					<!-- end: page -->
				</section>
			</div>
						
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>


	</body>


</html>