<?php $this->load->view('front/layout/header-inner'); ?>
<style>
	.css-serial {
		counter-reset: serial-number;  /* Set the serial number counter to 0 */
	}

	.css-serial td:first-child:before {
		counter-increment: serial-number;  /* Increment the serial number counter */
		content: counter(serial-number);  /* Display the counter */
	}
	.card {
		/* Add shadows to create the "card" effect */
		margin: 25px;
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
	}

	/* On mouse-over, add a deeper shadow */
	.card:hover {
		box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
	}

	/* Add some padding inside the card container */
	.card-container {
		padding: 2px 16px;
	}
	.card-container p{
		font-weight: 900;
		font-size: large;
	}
	.active{
		color:white;
		background: red;
	}
</style>
<button id="otppopupmodalBootstrapsendmodeytobank" data-toggle="modal" data-target="#modalBootstrapsendmodeytobank" style="display:none;">openpopup</button>							

<button id="otppopup" data-toggle="modal" data-target="#otpModel" style="display:none;">openpopup</button>
<div class="inner-wrapper">
	<!-- start: sidebar -->
	<?php $this->load->view('front/layout/left-menu'); ?>
	<!-- end: sidebar -->

	<section role="main" class="content-body">
		<header class="page-header">
			<h2>My Team</h2>					
			<div class="right-wrapper pull-right">
				<ol class="breadcrumbs">
					<li>
						<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
							<i class="fa fa-home"></i>
						</a>
					</li>								
					<li><a href=""><span>My Team</span></a></li>
				</ol>					
				<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
			</div>
		</header>
		<!-- start: page -->

		<div class="row">						
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading">	
						<h2 class="panel-title">My Team
							<?php foreach ($groups as $key => $value) { ?>
								<a  href="<?php echo base_url('index.php/user/royal_group'); ?>?group_id=<?php echo $value->royal_group_id ?>"><button class="<?php if ($active_group == $value->royal_group_id) echo 'active'; ?>"><?php echo $value->group_name; ?></button></a>
							<?php } ?>
						</h2>
					</header>

					<div class="panel-body" >
						<?php 

						if ($checkeligibility == 'eligible') {?>
							<a href="<?php echo base_url('index.php/user/upgrade_user_next_group'); ?>?group_id=<?php echo @$_GET['group_id']?>"><button class="active">Join This Group</button></a>
						<?php } ?>

						<?php 

						if ($checkeligibility == 'eligible with money') {?>
							<a href="<?php echo base_url('index.php/user/update_group_wallet_amount'); ?>?group_id=<?php echo @$_GET['group_id']?>"><button class="active">Please Credit the Wallet</button></a>
						<?php } ?>

						<div>

							<?php 
							// adding button in group 1 so that person can upgrade without completing the first cycle. Check condition > money should be there, 
							if ($checkeligibility == 'No chance' && isset($_GET['group_id']) && $_GET['group_id'] == 3){?>
								<a type="button" id="upgradeuserbtn" onclick="upgradeaccount()" name="upgradeaccount1" class="btn btn-danger">Join Group 1</a>		
							<?php }
							?>

							<?php 

							if ($checkeligibility == 'already-there'){
							?>
							<a type="button" id="quit_groupbtn" onclick="quituserbyotp();" name="quit_group" class="btn btn-danger">Quit Group</a>							<?php } ?>
							<form method="post" id="sendmonyform">
								<span id="errormsg"></span>	
													
							<p >
								<div class="modal fade" id="otpModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

									<div class="modal-dialog">

										<div class="modal-content" >

											<div class="modal-header">

												<button type="button" class="close" data-dismiss="modal" id="closeotp"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

												<h4 class="modal-title" id="myModalLabel">Enter OTP</h4>

											</div>

											<div class="modal-body" >

												<div class="table-responsive">

													<span id="lastmsg"></span>

													<table class="table mb-none">															

														<tr>

															<td>

																<span id="optmsg"></span>	

																<input type="text" name="otpnumber" id="otpnumber" placeholder="Enter OTP" class="form-control"><br>

																<button type="button" id="sendmoneybutton" onclick="return sendMoneyToUser();" name="sendMoneyToFriendbyOtp" class="btn btn-info pull-right">Submit</button>

															</td>

														</tr>																	

													</table>

												</div>

											</div>												

										</div>

									</div>

								</div>
								<h4 style="color: red;">Terms & Conditions</h4>



								<p style="color: red;"><?php if (isset($usersgroup[0]->group_id)) {$group_details = $this->royal_group_tree->getGroups($usersgroup[0]->group_id); echo $group_details[0]->condition; } ?></p>
							</p>
						</div>
						

						<div class="col-md-12">
							<div class="col-md-2">
								<div class="card " >
									<div class="card-container">
										<h5 class="card-title"><?php echo count($usersgroup); ?></h5>
										<p class="card-text">Total Users</p>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="card" >
									<div class="card-container">
										<h5 class="card-title"><?php if (isset($usersgroup[0])) echo $this->royal_group_tree->groupMoney($usersgroup[0]->group_id); else echo 0 ?></h5>
										<p class="card-text">Balance Available in this Group</p>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="card " >
									<div class="card-container">
										<h5 class="card-title"><?php  echo $this->royal_group_tree->myroyaltyMoney($login_user[0]->id); ?></h5>
										<p class="card-text">My Royality Income</p>
									</div>
								</div>
							</div>


							<div class="col-md-2">
								<div class="card " >
									<div class="card-container">
										<h5 class="card-title"><?php echo $this->royal_group_tree->myMoney($login_user[0]->id); ?></h5>
										<p class="card-text">Income from All Groups</p>
									</div>
								</div>
							</div>	
							<div class="col-md-2">
								<div class="card " >
									<div class="card-container">
										<h5 class="card-title"><?php if (isset($usersgroup[0])) echo $this->royal_group_tree->myMoney($login_user[0]->id, $usersgroup[0]->group_id); else echo 0; ?></h5>
										<p class="card-text">Income From this Group</p>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="card " >
									<div class="card-container">
										<h5 class="card-title"><?php if (isset($usersgroup[0])) echo $this->royal_group_tree->nextPayment($usersgroup[0]->group_id); else echo "NA"; ?></h5>
										<p class="card-text">Next Beneficiary</p>
									</div>
								</div>
							</div>	
						</div>
						<div class="clearfix"></div>

						<!-- </div> -->
						<form class="form-horizontal" method="post">
							<h4 class="mb-xlg"></h4>
							<fieldset>

								<div class="form-group">

									<div class="col-md-5">
										<div class="input-daterange input-group" data-plugin-datepicker="" >
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
											<input type="text" class="form-control" name="start" required value="<?php echo(isset($_POST['start']))?$_POST['start']:''; ?>">
											<span class="input-group-addon">to</span>
											<input type="text" class="form-control" name="end" required value="<?php echo(isset($_POST['end']))?$_POST['end']:''; ?>">                                                        
										</div>
									</div>
                                                    <!---
														<div class="col-md-2">
															<select class="form-control mb-md">
																<option>Select Registration Type</option>
																<option>Direct</option>
																<option>All</option>                                                      
															</select>                                                        
														</div>
														--->
														<div class="col-md-3">
															<select class="form-control mb-md" name="status" required>
																<option value="">Select Account Status</option>
																<option <?php echo(isset($_POST['status']) && $_POST['status']=='all')?'selected':''; ?> value="all">All</option>
																<option <?php echo(isset($_POST['status']) && $_POST['status']=='1')?'selected':''; ?> value="1">Initiated</option>
																<option <?php echo(isset($_POST['status']) && $_POST['status']=='0')?'selected':''; ?> value="0">Active</option>
																<option <?php echo(isset($_POST['status']) && $_POST['status']=='0')?'selected':''; ?> value="0">Closed</option>
																<option <?php echo(isset($_POST['status']) && $_POST['status']=='0')?'selected':''; ?> value="0">Quit</option>
															</select>                                                        
														</div>
														<div class="col-md-2">
															<select class="form-control mb-md" name="position" required>
																<option value="">Select Position</option>															
																<option <?php echo(isset($_POST['position']) && $_POST['position']=='L')?'selected':''; ?> value="L">Left</option>
																<option <?php echo(isset($_POST['position']) && $_POST['position']=='R')?'selected':''; ?> value="R">Right</option>
																<option <?php echo(isset($_POST['position']) && $_POST['position']=='all')?'selected':''; ?> value="all">Both</option>
															</select>                                                        
														</div>

														<div class="col-md-2">
															<button type="submit" name="searchMyTeam" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
															<button type="button" onclick="window.location.reload()" class="btn btn-warning"><i class="fa  fa-repeat"></i> Reset </button>   
														</div>
													</div>                                                
												</fieldset>											
											</form>                                        
											<div class="table-responsive">
												<table class="table table-bordered table-striped table-condensed mb-none css-serial">
													<thead>
														<tr>
															<th>S.No</th>
															<th>User ID</th>
															<th>Name</th>

															<th>Group Payment</th>
															<th>Status</th>
															<th>Activation Date</th>

														</tr>
													</thead>
													<tbody>	
														<?php 
														if(!isset($_POST['searchMyTeam']))
														{
													// $sdate = date('Y-m-d',strtotime($this->input->post('start')));
													// $esdate = date('Y-m-d',strtotime($this->input->post('end')));
													// $status = $this->input->post('status');
													// $postion = $this->input->post('position');

															?>
															<?php if(count($usersgroup)>0){ ?>
																<?php foreach($usersgroup as $luser){ ?>

																	<tr>
																		<td><?php //echo $k; ?></td>
																		<td><?php echo $luser->id; ?></td>
																		<td><?php echo $luser->name; ?></td>
																		<td><?php echo $luser->account_status; ?></td>
																		<td><?php echo($luser->status==1)?'Active':'Inactive'; ?></td>
																		<td><?php echo date('d-M-Y',$luser->registration_date); ?></td>

																	</tr>
																<?php }}  ?>


																<?php
															}

															?>
														</tbody>


													</table>
												</div>

											</div>
										</div>
									</section>                         
								</div>


							</div>
							<!--footer start-->
							<?php $this->load->view('front/layout/footer'); ?>	
							<!--end start-->
							<!-- end: page -->
						</section>
					</div>

				</section>

				<!-- Vendor -->
				<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
				<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
				<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
				<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
				<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
				<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
				<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
				<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
				<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>

				<!-- Specific Page Vendor -->		
				<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>

				<!-- Theme Base, Components and Settings -->
				<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>

				<!-- Theme Custom -->
				<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>

				<!-- Theme Initialization Files -->
				<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
				<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
				<?php if(isset($_GET['mydata'])){ ?>

					<script>

						document.getElementById('otppopupmodalBootstrapsendmodeytobank').click();

					</script>

				<?php } ?>
				<script type="text/javascript">
					
					function upgradeaccount()
					{						
						errormessage = "Caution : Force Promotion to 'Group-1' will stop your all earnings from Basic Group !,\n\nYes I understand and accept the policy to proceed further";
						if (window.confirm(errormessage.replace('/\\n/g', '\n'))) {
							if(1)
							{	
								

								jQuery.ajax({

									url: "<?php echo base_url('index.php/user/upgradeuser'); ?>",
									data:{user_id:"<?php echo $this->session->userdata('USERID');?>"},
									type: "POST",  
									beforeSend : function()
									{
										$('#upgradeuserbtn').append('<i class="fa fa-spinner faa-spin animated"></i>');
									},	
									success:function(mydata)
									{
										location.reload();
										//refresh the page when the request succeed
									},
									error:function ()
									{
										$('#errormsg').html('<div class="alert alert-danger"><strong>error ! </strong>Invalid USER ID or Amount</div>');
										
										return false;
									}

								});	

							}

						
						}
					}






					function quituserbyotp()
					{						
						var uid = $('#userid').val();
						if (window.confirm("Are you sure?")) {
							if(uid!="")
							{	
								var sendmoneyform = $("#sendmonyform").serialize();

								jQuery.ajax({

									url: "<?php echo base_url('index.php/user/exit_group_by_otp'); ?>",
									data:sendmoneyform,
									type: "POST",  
									beforeSend : function()
									{
										$('#quit_groupbtn').append('<i class="fa fa-spinner faa-spin animated"></i>');
									},	
									success:function(mydata)
									{
										if(mydata!=0)
										{
											$('#lastmsg').html('<div class="alert alert-success">Your account is being deleted.Please confirm to proceed with otp.</div>');
											document.getElementById('otppopup').click();
											
											return false;
										}
										else
										{
											$('#errormsg').html('<div class="alert alert-danger"><strong>error ! </strong> Invalid USER ID or Amount</div>');
											$('#sendmoneybutton').html('Generate OTP');
											return false;
										}	
									},
									error:function ()
									{
										$('#errormsg').html('<div class="alert alert-danger"><strong>error ! </strong>Invalid USER ID or Amount</div>');
										$('#sendmoneybutton').html('Generate OTP');
										return false;
									}

								});	

							}

						
						}
					}


					function sendMoneyToUser()
					{
						var uid = $('#userid').val();
						var send_amount = $('#sendamount').val();
						var OTP = $('#otpnumber').val();
						console.log(OTP)
						if(OTP!="")
						{	
							var sendmoneyform = $("#sendmonyform").serialize();
								console.log(OTP);
							jQuery.ajax({
								url: "<?php echo base_url('index.php/user/deletegroupaccountfinally'); ?>",
								data:sendmoneyform,
								type: "POST",			
								success:function(mydata)
								{
									if(mydata!=0)
									{
										$('#closeotp').click();
										$('#errormsg').html('<div class="alert alert-success">Account successfully Closed.</div>');	
										$('#quit_groupbtn').html('Quit Group');				
										return false;
									}
									else
									{
										$('#optmsg').html('<div class="alert alert-danger"><strong>error ! </strong> Invalid OTP </div>');
										return false;
									}	
								},
								error:function ()
								{
									$('#optmsg').html('<div class="alert alert-danger"><strong>error ! </strong> Invalid OTP </div>');
									return false;
								}
							});	
						}
						else
						{
							$('#optmsg').html('<div class="alert alert-danger"><strong>error ! </strong>Invalid OTP </div>');
						}	return false;
					}
				</script>
			</body>
			</html>