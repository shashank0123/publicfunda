<?php $this->load->view('front/layout/header-inner'); ?>
<style>
.inactiveClick{color: #c5c5c5 !important;}
</style>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Today's Task</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Today's Task</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">						
						<div class="col-md-12">
							<section class="panel">
									<header class="panel-heading">
										<div class="panel-actions"> 
										 <p>
											<strong>Total CTP Points: <?php echo count($this->campaign_model->getTotalPoints($login_user[0]->id)); ?></strong><br>										
											<strong>Today's Earned CTP Points : <?php echo count($TOTALCLICKEDLINK); ?></strong><br>
										 </p>
										</div>															
										<form action="<?php echo base_url('index.php/user/wallet'); ?>" method="post">
											<h2 class="panel-title">Today's Task											
											</h2> 
										</form>	
										<p><br>
										<?php 
											if($login_user[0]->boosterStatus==1)
											{
												$tasklink = ($login_user[0]->linkPerDay+$login_user[0]->booster_task);
											}
											else
											{
												$tasklink =  $login_user[0]->linkPerDay;
											}
											$checkDistributedLink = $this->campaign_model->selectDistributionLinkByUserIdAndTodayDate($login_user[0]->id);
										?>
										
										<?php if($login_user[0]->wallet_display==1){ ?>
										<a href="<?php echo base_url('index.php/user/wallet'); ?>" class="btn btn-warning"><i class="fa fa-money"></i> Wallet</a></p>
										<?php } ?>	
										<?php $workdata = $this->admin_model->getAllTaxes(1); ?>
										<?php if($workdata[0]->requestwork==1){ ?>
                                        <?php $datename = date ('l'); ?>
										<?php if($datename!='Saturday' && $datename!='Sunday'){ ?>	
											<center><a href="javascript:void(0);" onclick="return requestforwork();"><button type="button" class="mb-xs mt-xs mr-xs btn btn-success"><b>Request For Work</b></button></a></button>
										<?php }} ?>
									</header>									
									<div class="panel-body">
										<div class="table-responsive" id="todaystask">
											<?php if($login_user[0]->today_task=='1'){ ?>
											<table class="table mb-none">
												<thead>
													<tr>
														<th>Sr No.</th>
														<th>Page Title</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php $activeRecord = $this->campaign_model->todayTaskLinkDisributionCheckOneLink($login_user[0]->id); ?>
													<?php $i=0; foreach($todayTaskLink as $task){ $i++; ?>
													<?php $campaign = $this->campaign_model->selectCampaignByID($task->link_id); ?>													
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $campaign[0]->campaignTitle; ?></td>
														<td>
															<?php if($task->status==1){ ?>
																<button type="button" class="btn btn-danger">Pending</button>
															<?php }else{ ?>
																<button type="button" class="btn btn-success">Clicked</button>
															<?php } ?>
                                                        </td>														
														<td class="actions">
															<ul class="notifications">
																<?php 
																if($task->status==1)
																{ 
																	if($activeRecord[0]->id==$task->id){
																?>																
																<li><a href="javascript:void(0);" onclick="getTaskWindow('<?php echo $campaign[0]->campaignUrl; ?>','<?php echo $task->id; ?>','<?php echo $task->link_id; ?>')" class="notification-icon" rel="tooltip"  data-original-title="Click"> <i class="fa fa-hand-o-up"></i></a></li>                                                           
																<li id="refreshMy<?php echo $task->id; ?>"><a href="javascript:void(0);" onclick="return refreshLinkCampaign('<?php echo $task->id; ?>','<?php echo $task->link_id; ?>','<?php echo $task->user_id; ?>');" class="refresh" rel="tooltip" data-original-title="Refresh"><i class="fa fa-refresh"></i></a></li>
																<?php }else{ ?>
																<li><a href="javascript:void(0);" class="notification-icon"> <i class="fa fa-hand-o-up inactiveClick"></i></a></li>
																<li><a href="javascript:void(0);" class="refresh" ><i class="fa fa-refresh"></i></a></li>
																<?php } ?>
																<?php }else{ ?>
																<li><a href="javascript:void(0);" class="notification-icon" rel="tooltip"  data-original-title="Clicked"> <i class="fa fa-hand-o-up inactiveClick"></i></a></li>
																<li><a href="javascript:void(0);" class="refresh" ><i class="fa fa-refresh"></i></a></li>
																<?php if($login_user[0]->free_viewtab=='1'){ ?>
																<li><a href="javascript:void(0);" data-toggle="modal" data-target="#modalx<?php echo $task->id; ?>" ><button type="button" class="btn btn-info">Free View</button></a></li>
																<?php } ?>
																<div class="modal fade" id="modalx<?php echo $task->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																	<div class="modal-dialog">
																		<div class="modal-content">																			
																			<div class="modal-body">
																				 <div class="table-responsive">
																					<strong>Dear User, Please read the message carefully and accept and agree that your name, Contact Number, and Email-ID would be shared with the owner of the Campaign.
And you agree and allowed the Campaign owner to contact you through promotional call, Message, and Mails, If you proceed by clicking the below  Confirm Tab.</strong>
																				</div>
																			</div>
																			<div class="modal-footer">																				
																				<button class="btn btn-primary modal-confirm" data-dismiss="modal" id="fl<?php echo $task->id; ?>"  onclick="return openDuplicateWindow('<?php echo $campaign[0]->campaignUrl; ?>','<?php echo $task->link_id; ?>','<?php echo $task->user_id; ?>','<?php echo $task->id; ?>');">Confirm</button>
																				
																				<button type="button" class="btn btn-default" id="close<?php echo $task->id; ?>" data-dismiss="modal">Cancel</button>
																			</div>
																		</div>
																	</div>
																</div>
																<?php } ?>																
															</ul>                                                            
														</td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
											<?php } ?>
										</div>
									</div>
								</section>                         
						</div>
						

					</div>
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
					<!-- end: page -->
				</section>
			</div>			
			
		
		</section>

		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>

<script>
function openDuplicateWindow(WURL,CID,UID,TASKID)
{
    if (WURL!='' && CID!="")
	{
		jQuery.ajax({
            url: "<?php echo base_url('index.php/user/updateDuplicateWindowTask'); ?>",
            data:'CID='+CID+'&UID='+UID,
			async:  false,
            type: "POST",
			beforeSend: function()
			{
				$('#fl'+TASKID).append('<i class="fa fa-spinner faa-spin animated"></i>');	
			},
            success:function(mydata)
			{
				if(mydata==1)
				{
					//window.open(WURL, "_blank");
					var win = window.open(WURL);
					win.focus();
					//window.open(WURL, "", "width="+screen.width+", height="+screen.height+", scrollbars=yes");
					$('#fl'+TASKID).html('Confirm');
					$('#close'+TASKID).close();							
				}
				else
				{
					 alert('error'+mydata);
				}				
               
            },
            error:function (){}
        });	 
    }  	
}

/*function myFunction(winUrl) {
    var myWindow = window.open(winUrl, "", "width=250");
	setTimeout(function () { myWindow.close(); }, 10000);	
}*/

 function getTaskWindow(url, linkID, campaignID) 
 {
    var lnkFlag = JSON.stringify(lnkFlag);
	var w  = 1400; //1024;
    var h = 850;
	 var left = (window.screen.width / 2) - ((w / 2) + 50);
    var top = (window.screen.height / 2) - ((h / 2 ) + 50);
    myWindow = window.open(url,/* this.href, */"myWindow", "width=" + w + ",height=" + h + ",resizable=yes,left="
    + left + ",top=" + top + ",screenX=" + left + ",screenY=" + top + "");
	$('#refreshMy'+linkID).html('<a href="javascript:void(0);" class="refresh" ><i class="fa fa-refresh"></i></a>');
    CloseTimeout = setTimeout('updateMyTask(' + linkID + ',' + campaignID +');', 30000);
    interval = window.setInterval(function () 
	{
		try {
				if (myWindow == null || myWindow.closed) 
				{
					window.clearInterval(interval);
					clearTimeout(CloseTimeout);
				}
			}
			catch (e) {}
     }, 1000);
}

function updateMyTask(LID,CID)
{
	 myWindow.close();
	 jQuery.ajax({
            url: "<?php echo base_url('index.php/user/updateMyTask'); ?>",
            data:'LID='+LID+'&CID='+CID,
            type: "POST",
            success:function(mydata){
				window.location.reload();	
               // alert(mydata);
            },
            error:function (){}
        });	
}

function refreshLinkCampaign(LID, CID, UID)
{
	 jQuery.ajax({
            url: "<?php echo base_url('index.php/user/refreshLinkCampaign'); ?>",
            data:'LID='+LID+'&CID='+CID+'&UID='+UID,
            type: "POST",
            success:function(mydata){
				window.location.reload();
            },
            error:function (){}
        });
}


function requestforwork(UID)
{
	 jQuery.ajax({
            url: "<?php echo base_url('index.php/work/request/'.$login_user[0]->id); ?>",
            data:'UID='+UID,
            type: "POST",
			beforeSend: function(){
				$('#todaystask').html("<center><img src='<?php echo base_url('assets/front/images/loading.gif'); ?>'></center>");
			},
            success:function(mydata){
			   window.location.reload();
            },
            error:function (){}
        });
}
</script>	
	

	</body>

<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
</html>