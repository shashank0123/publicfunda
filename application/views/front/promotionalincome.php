<?php $this->load->view('front/layout/header-inner'); ?>
	<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Promotional Income</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Promotional Income</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12">
							<section class="panel">
									<header class="panel-heading">	
											<h2 class="panel-title">Promotional Income
																						
											</h2> 
									</header>
									
									<div class="panel-body" >
									<form class="form-horizontal" method="post">
											<h4 class="mb-xlg"></h4>
											<fieldset>
												
												<div class="form-group">
                                                													
													<div class="col-md-6 mb-md">
														<div class="input-daterange input-group" data-plugin-datepicker="" >
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="start" required value="<?php echo(isset($_POST['start']))?$_POST['start']:''; ?>">
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="end" required value="<?php echo(isset($_POST['end']))?$_POST['end']:''; ?>">                                                        
														</div>
													</div>
                                                   	<?php /* ?>
                                                    <div class="col-md-3">
														<select class="form-control mb-md" name="status" required>
															<option value="">Select Status</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='pending')?'selected':''; ?> value="pending">Pending</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='paid')?'selected':''; ?> value="paid">Paid</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='cancelled')?'selected':''; ?> value="cancelled">Cancelled</option>
														</select>                                                        
													</div>
													<?php */ ?>
													
                                                    <div class="col-md-3">
														<button type="submit" name="searchMyTeam" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
														<button type="button" onclick="window.location.reload();" class="btn btn-warning"><i class="fa  fa-repeat"></i> Reset </button>   
													</div>
												</div>
                                                                                                
											</fieldset>
										
										</form>
                                        
                                        <div class="table-responsive">
									<table class="table table-bordered table-striped mb-none" id="datatable-default">	
										<thead>
											<tr>
												<th>S.No</th>
                                                <th>Left TR</th>
                                                <th>Right TR</th>
                                                <th>CLTR</th>
                                                <th>CRTR</th>
												<th>Income</th>
												<th>Deduction</th>
                                                <th>Admin Charges</th>
                                                <th>Net Income</th>
                                                <th>Transaction ID</th>	
												<th>Status</th>	
                                                <th>Date</th>																								
											</tr>
										</thead>
										<tbody>	
											<?php if(count($pIncome)>0){ ?>
											<?php $i=0; foreach($pIncome as $income){ $i++; ?>
											<tr>
												<td><?php echo $i; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $income->left_bv; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $income->right_bv; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $income->clbv; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $income->crbv; ?></td>
												<td><i class="fa fa-rupee"></i><?php echo $income->income; ?></td>
												<td><i class="fa fa-rupee"></i><?php echo $income->tds; ?></td>
												<td><i class="fa fa-rupee"></i> <?php echo $income->admincharge  ; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $income->netIncome; ?></td>
                                                <td><?php echo $income->transactionID; ?></td>
												<td><?php echo $income->status; ?></td>		
                                                <td><?php echo $income->crDate; ?></td>                                                
                                            </tr>
											<?php } ?>
											<?php }else{ ?>
												<tr>
													<td colspan="12">Record not found</td>
												</tr>
											<?php } ?>
										</tbody>   
									</table>
                                    
								</div>
								</div>
								</section>                         
						</div>
						

					</div>
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
					<!-- end: page -->
				</section>
			</div>
			
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	</body>


</html>