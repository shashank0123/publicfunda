<?php $this->load->view('front/layout/header-inner'); ?>
<?php //print_r($KYCDATA);
if(count($KYCDATA)>0){
	if($KYCDATA[0]->idstatus=="1"){	$idproof = 'disabled'; }else{ $idproof = ''; }
	if($KYCDATA[0]->pancardstatus=="1"){	$pancard = 'disabled'; }else{ $pancard = ''; }
	if($KYCDATA[0]->bankdetailsstatus=="1"){	$bankdetails = 'disabled'; }else{ $bankdetails = ''; }
}
else
{
	$idproof = ''; $pancard = ''; $bankdetails = '';
}
?>
<?php 
$kycStatus = $login_user[0]->kyc_status; 
if($kycStatus==1)
{
	$kyc_status = "disabled";
}
else
{
	$kyc_status = "";
}	
?>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>KYC</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>KYC</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">						
						<div class="col-md-12">
							<section class="panel">
							<header class="panel-heading">
								<div class="panel-actions">
									
								</div>						
								<h2 class="panel-title">KYC</h2>
							</header>
							<div class="panel-body">
								<?php echo $this->session->flashdata('message'); ?>
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed mb-none">
										<thead>
											<tr>
												<th>Document</th>
												<th  width="15%">Document Type</th>
												<th class="area-table">Select File</th>
												<th width="13%">Upload File</th>
												<th>Upload Date</th>
												<th>Approved Date</th>
												<th>Status</th>
												<th  width="10%">View</th>
												
											</tr>
										</thead>
										<tbody>
											<form method="post" enctype="multipart/form-data">
											<tr>											
												<td>Identity Proof</td>
												<td>
                                                	<select class="form-control" name="idtype" <?php echo $idproof.$kyc_status; ?>>
														<option value="Aadhar Card">Aadhar Card</option> 
														<option value="Voter ID">Voter ID</option>
														<option value="Driving Licence">Driving Licence</option>
														<option value="Passport">Passport</option>
													</select>
                                                </td>
												<td width="13%">
                                               	 <input type="file" style="width: 178px;"  name="identityproof" <?php echo $idproof.$kyc_status; ?> required >
                                                </td>
												<td class="text-right"><button class="btn btn-warning" type="submit" <?php echo $idproof.$kyc_status; ?> name="uploadIdProof">UPLOAD <i class="fa fa-upload"></i></button></td>
												<td class="text-right"><?php echo(isset($KYCDATA[0]->iduploaded_date) && $KYCDATA[0]->iduploaded_date!="")?date('d-M-Y h:s A',$KYCDATA[0]->iduploaded_date):'N/A'; ?></td>
												<td class="text-right"><?php echo(isset($KYCDATA[0]->idapproved_date) && $KYCDATA[0]->idapproved_date!="")?date('d-M-Y h:s A',$KYCDATA[0]->idapproved_date):'N/A'; ?></td>
												<td class="text-right"><?php echo($login_user[0]->kyc_status==1)?'Approved':'Pending For Approval'; ?></td>
												<td class="text-right"><a class="btn btn-warning" href="<?php echo(isset($KYCDATA[0]->identityproof))?base_url('uploads/document/'.$KYCDATA[0]->identityproof):''; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }'><i class="fa fa-search"></i> View</a> </td>											
											</tr>
											</form>
											<form method="post" enctype = "multipart/form-data">
											<tr>
												<td>PAN Card</td>
												<td>
                                                	<select class="form-control" name="pancardType" <?php echo $pancard.$kyc_status; ?>>
														<option value="Pan Card">Pan Card</option>
													</select>
                                                </td>
												<td class="text-right"><input style="width: 178px;" <?php echo $pancard.$kyc_status; ?> type="file" name="pancard" required></td>
												<td class="text-right"><button class="btn btn-warning" <?php echo $pancard.$kyc_status; ?> name="uploadPancard" type="submit">UPLOAD <i class="fa fa-upload"></i></button></td>
												<td class="text-right"><?php echo(isset($KYCDATA[0]->pancarduploaded_date) && $KYCDATA[0]->pancarduploaded_date!="")?date('d-M-Y h:s A',$KYCDATA[0]->pancarduploaded_date):'N/A'; ?></td>
												<td class="text-right"><?php echo(isset($KYCDATA[0]->pancardapproved_date) && $KYCDATA[0]->pancardapproved_date!="")?date('d-M-Y h:s A',$KYCDATA[0]->pancardapproved_date):'N/A'; ?></td>
												<td class="text-right"><?php echo($login_user[0]->kyc_status==1)?'Approved':'Pending For Approval'; ?></td>
												<td class="text-right"><a class="btn btn-warning" href="<?php echo(isset($KYCDATA[0]->pancard))?base_url('uploads/document/').'/'.$KYCDATA[0]->pancard:''; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }'><i class="fa fa-search"></i> View</a> </td>												
											</tr>
											</form>
											<form method="post" enctype="multipart/form-data">
											<tr>
												<td>Bank Passbook</td>
												<td>
                                                	<select class="form-control" name="bankdetailstype" <?php echo $bankdetails.$kyc_status; ?>>
														<option value="Bank Passbook">Bank Passbook</option> 
														<option value="Bank Statement">Bank Statement</option>
													</select>
                                                </td>
												<td class="text-right"><input name="bankdetails" <?php echo $bankdetails.$kyc_status; ?> style="width: 178px;" type="file" required></td>
												<td class="text-right"><button class="btn btn-warning" <?php echo $bankdetails.$kyc_status; ?> name="uploadBank" type="submit">UPLOAD <i class="fa fa-upload"></i></button></td>
												<td class="text-right"><?php echo(isset($KYCDATA[0]->bankdetailsuploaded_date) && $KYCDATA[0]->bankdetailsuploaded_date!="")?date('d-M-Y h:s A',$KYCDATA[0]->bankdetailsuploaded_date):'N/A'; ?></td>
												<td class="text-right"><?php echo(isset($KYCDATA[0]->bankdetailsapproved_date) && $KYCDATA[0]->bankdetailsapproved_date!="")?date('d-M-Y h:s A',$KYCDATA[0]->bankdetailsuploaded_date):'N/A'; ?></td>
												<td class="text-right"><?php echo($login_user[0]->kyc_status==1)?'Approved':'Pending For Approval'; ?></td>
												<td class="text-right"><a class="btn btn-warning" href="<?php echo(isset($KYCDATA[0]->bankdetails))?base_url('uploads/document/'.$KYCDATA[0]->bankdetails):''; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }'><i class="fa fa-search"></i> View</a> </td>												
											</tr>
											</form>
										</tbody>
									</table>
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
					<!-- end: page -->
				</section>
			</div>

			
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
	<!-- Analytics to Track Preview Website -->	
			<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
  $.validate({
    lang: 'en'
  });
</script>
<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	</body>


</html>