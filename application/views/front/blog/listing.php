

<?php $this->load->view('front/layout/header'); ?>
<div class="inner-wrapper pt-30W">
            <?php $this->load->view('front/layout/left-menu'); ?>
                <!-- Page Heading -->

               <section role="main" class="content-body">

                <header class="page-header">
                        <h2 class="page-header">Blog Listing <a href="<?php echo base_url('index.php/blog/add'); ?>" class="btn btn-primary pull-right">Add New</a></h2>
                        </header>
                        <div class="clearfix"></div>
                         <?php echo $this->session->flashdata('message'); ?>

						 <div class="table-responsive">

                            <table class="table table-bordered table-hover">

                                <thead>

                                    <tr>

										<th width="5%">S.No.</th>

                                        <th>Title</th>

                                        <th width="8%">Status</th>

                                        <th width="20%">Action</th>

                                    </tr>

                                </thead>

                                <tbody>

								<?php $i=0; foreach($BLOGDATA as $data){ $i++; ?>

                                    <tr>

										<td><?php echo $i; ?></td>

                                        <td><?php echo $data->title; ?></td>										

                                        <td><img src="<?php echo($data->status=='0')?base_url('assets/admin/image/inactive.png'):base_url('assets/admin/image/active.png'); ?>"> </td>

                                        <td>

											<a href="<?php echo base_url('blog/edit/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

											<a href="<?php echo base_url('blog/delete/'.$data->id); ?>" onclick="return deleteConfirm();" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i>delete</a>																	

										</td>

                                    </tr>

                                <?php } ?>                                       

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->


<script>

function checkpancard()

{

	if(document.getElementById("nopancard").checked==true)

	{

		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');

		document.getElementById("pancard").style.backgroundColor = "#ccc";

		document.getElementById("pancard").readOnly = true;

	}

	else

	{

		document.getElementById("pancard").style.backgroundColor = "#fff";

		document.getElementById("pancard").readOnly = false;

	}	

}



</script>

</body>



</html>

