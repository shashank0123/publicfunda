<?php $this->load->view('front/layout/header'); ?>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/style-crop.css" />

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/jquery.Jcrop.min.css" type="text/css" />

            <div class="inner-wrapper pt-100">

                <!-- start: sidebar -->

                <?php $this->load->view('front/layout/left-menu'); ?>

                <!-- end: sidebar -->

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h2 class="page-header">Edit Blog Category</h2>

                         <form role="form" method="post" enctype="multipart/form-data">							

                            <div class="form-group">

                                <label>Title</label>

                                <input type="text" class="form-control" placeholder="category Title" value="<?php echo $EDITBLOG[0]->title; ?>" name="title">

                            </div>							

							<div class="form-group">

                                <label>Status</label>

                                <select name="status" class="form-control">

                                    <option <?php echo($EDITBLOG[0]->status==1)?'selected':''; ?> value="1">Active</option>

                                    <option <?php echo($EDITBLOG[0]->status==0)?'selected':''; ?> value="0">Inactive</option>

                                </select>

                            </div>

                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>

                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<?php $this->load->view('admin/layout/footer-js'); ?>

</body>



</html>

