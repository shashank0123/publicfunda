
<!DOCTYPE html>

<html lang="en">

<?php $this->load->view('front/layout/head'); ?>

<body>



    <div id="wrapper">

        <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->

            <?php $this->load->view('front/layout/header'); ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <?php $this->load->view('admin/layout/left-menu'); ?>

            <!-- /.navbar-collapse -->

        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h2 class="page-header">Edit Blog</h2>

                         <form role="form" method="post" enctype="multipart/form-data">

							<div class="form-group">

                                <label> Select Category</label>

                                <select class="form-control" name="cat_id" required>

									<?php foreach($CATEGORY as  $cat){ ?>

										<option value="<?php echo $cat->id; ?>" <?php echo($EDITBLOG[0]->cat_id==$cat->id)?'selected':''; ?>><?php echo $cat->title; ?></option>

									<?php } ?>

								</select>

                            </div>

                            <div class="form-group">

                                <label>Title</label>

                                <input type="text" class="form-control" placeholder="category Title" value="<?php echo $EDITBLOG[0]->title; ?>" name="title">

                            </div>

							<div class="form-group">

								<?php if(!empty($EDITBLOG[0]->image))	{	?>

                                <img src="<?php echo base_url('uploads/blog/'.$EDITBLOG[0]->image); ?>" width="80">

								<?php }	?>

								<input type="hidden" name="oldimage" value="<?php echo $EDITBLOG[0]->image; ?>">								

                            </div>

							<div class="form-group">

                                <label>Image</label>

                                <input type="file" class="form-control"  name="image">

                            </div>

							<div class="form-group">

                                <label> Content</label>

                                <textarea class="form-control" name="content"><?php echo $EDITBLOG[0]->content; ?></textarea>

                            </div>

							<div class="form-group">

                                <label>Status</label>

                                <select name="status" class="form-control">

                                    <option <?php echo($EDITBLOG[0]->status==1)?'selected':''; ?> value="1">Active</option>

                                    <option <?php echo($EDITBLOG[0]->status==0)?'selected':''; ?> value="0">Inactive</option>

                                </select>

                            </div>

                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>

                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<?php $this->load->view('admin/layout/footer-js'); ?>

</body>



</html>

