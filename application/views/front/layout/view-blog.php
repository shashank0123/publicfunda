<?php $BLOGDATA= $this->blog_model->select_blog_by_userid($this->session->userdata('USERID')); ?>
<div class="table-responsive">

                            <table class="table table-bordered table-hover">

                                <thead>

                                    <tr>

										<th width="5%">S.No.</th>

                                        <th>Title</th>

                                        <th width="8%">Status</th>

                                        <th width="20%">Action</th>

                                    </tr>

                                </thead>

                                <tbody>

								<?php $i=0; foreach($BLOGDATA as $data){ $i++; ?>

                                    <tr>

										<td><?php echo $i; ?></td>

                                        <td><?php echo $data->title; ?></td>										

                                        <td><img src="<?php echo($data->status=='0')?base_url('assets/admin/image/inactive.png'):base_url('assets/admin/image/active.png'); ?>"> </td>

                                        <td>

											<a href="<?php echo base_url('user/dashboard/?blog_id='.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

											<a href="<?php echo base_url('userblog/delete/'.$data->id); ?>" onclick="return deleteConfirm();" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i>delete</a>																	

										</td>

                                    </tr>

                                <?php } ?>                                       

                                </tbody>

                            </table>

                        </div>