<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li class="nav-active">
										<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('index.php/user/profile'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Profile</span>
										</a>
									</li>
<?php 
$campaign = ($login_user[0]->workpayout=='1')?'1':'0';
if($campaign=='1'){ ?>
									<li>
										<a href="<?php echo base_url('index.php/campaign/index'); ?>">
											<i class="fa fa-signal" aria-hidden="true"></i>
											<span>Manage Campaign</span>
										</a>
									</li>
<?php } ?>
									<?php if($login_user[0]->today_task=='1'){ ?>
									<li>
										<a href="<?php echo base_url('index.php/user/todaytask'); ?>">
											<i class="fa fa-calendar" aria-hidden="true"></i>
											<span>Today's Task</span>
										</a>										
									</li>
									<?php } ?>
									<?php if($login_user[0]->workpayout=='1'){ ?>
									<li>
										<a href="<?php echo base_url('index.php/user/myworkpayouts'); ?>">
                                        	<i class="fa fa-money" aria-hidden="true"></i>
											<span>My Work Payouts</span>
										</a>										
									</li>
                                    <?php } ?>
									<?php if($login_user[0]->workHistory=='1'){ ?>
									<li>
										<a href="<?php echo base_url('index.php/user/workHistory'); ?>">
                                        	<i class="fa fa-history" aria-hidden="true"></i>
											<span>Work History</span>
										</a>
									</li>
                                    <?php } ?>
									<?php if($login_user[0]->my_network=='1'){ ?>
									<li>
										<a href="<?php echo base_url('index.php/user/mynetworks'); ?>">
											<i class="fa fa-sitemap" aria-hidden="true"></i>
											<span>My Networks</span>
										</a>										
									</li>
<li>
										<a href="<?php echo base_url('index.php/user/direct_network'); ?>">
											<i class="fa fa-sitemap" aria-hidden="true"></i>
											<span>My Team Networks</span>
										</a>										
									</li>


                                    <?php } ?>
									<?php if($login_user[0]->promotional_income=='1'){ ?>
									<li>
										<a href="<?php echo base_url('index.php/user/promotionalincome'); ?>">
											<i class="fa fa-columns" aria-hidden="true"></i>
											<span>Promotional Income</span>
										</a>
									</li>
<li>
										<a href="<?php echo base_url('index.php/user/directincome'); ?>">
											<i class="fa fa-columns" aria-hidden="true"></i>
											<span>Direct Sale Income</span>
										</a>
									</li>

                                    <?php } ?>

									<?php if($login_user[0]->myteam=='1'){ ?>
									<li>
										<a href="<?php echo base_url('index.php/user/myteam'); ?>"><i class="fa fa-users" aria-hidden="true"></i>
											<span>My Team</span>
										</a>
									</li>
                                    <?php } ?>
									<?php if($login_user[0]->my_bussiness=='1'){ ?>
									<li>
										<a href="<?php echo base_url('index.php/user/mybusiness'); ?>" >
											<i class="fa fa-external-link" aria-hidden="true"></i>
											<span>My Business</span>
										</a>
									</li> 
<li>
										<a href="<?php echo base_url('index.php/user/mydirectbusiness'); ?>" >
											<i class="fa fa-external-link" aria-hidden="true"></i>
											<span>My Sale Report</span>
										</a>
									</li>
									<?php } ?>	
 

                                    <li>
										<a href="<?php echo base_url('index.php/user/kyc'); ?>">
											<i class="fa fa-book" aria-hidden="true"></i>
											<span>KYC</span>
										</a>
									</li>
                                     <li>
										<a href="<?php echo base_url('index.php/user/rewards'); ?>">
											<i class="fa fa-gift" aria-hidden="true"></i>
											<span>Rewards</span>
										</a>
									</li>                                    
                                     <li>
										<a target="_blank" href="<?php echo base_url('index.php/userblog/myposts/'.$this->session->userdata('USERID')); ?>">
											<i class="fa fa-comment" aria-hidden="true"></i>
											<span>Blogs</span>
										</a>
									</li>
									<!-- <li>
										<a href="<?php //echo base_url('index.php/userblog/listing'); ?>">
											<i class="fa fa-comment" aria-hidden="true"></i>
											<span>Bloglist</span>
										</a>
									</li> -->
									<li>
										<a href="<?php echo base_url('index.php/helpdesk/index'); ?>">
											<i class="fa fa-comment" aria-hidden="true"></i>
											<span>Help Desk</span>
										</a>
									</li>
								</ul>
							</nav>
				
						</div>
				
					</div>
				
				</aside>
<!--- Developed by Naseem Ahmad || e-mail anaseem711@gmail.com --->