<script type="text/javascript" src="<?php echo base_url('assets/admin'); ?>/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        skin : "o2k7",
        forced_root_block : "",
        plugins : "openmanager,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,openmanager",
        //theme_advanced_buttons4 : "openmanager",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,     
        //Open Manager Options
        file_browser_callback: "openmanager",
        open_manager_upload_path: '../../../../uploads/',
        // Example content CSS (should be your site CSS)
        content_css : "css/content.css",
        content_css: ['https://fonts.googleapis.com/css?family=Noto+Sans&display=swap',"css/content.css"],
        font_formats: 'Arial Black=arial black,avant garde;noto=Noto Sans, sans-serif, cursive;Times New Roman=times new roman,times;',
        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Style formats
        style_formats : [
        {title : 'Bold text', inline : 'b'},
        {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
        {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
        {title : 'Example 1', inline : 'span', classes : 'example1'},
        {title : 'Example 2', inline : 'span', classes : 'example2'},
        {title : 'Table styles'},
        {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
        ],
         language: 'hi_IN',

        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
    });
</script>
<?php $this->load->model('blog_model');$CATEGORY = $this->blog_model->select_all_active_blogcat(); 
if (isset($_GET['blog_id'])){
    $this->db->where('id', $_GET['blog_id']);
    $blog_detailss = $this->db->get('tbl_blog');
    $blog_detail = $blog_detailss->result();
    if (count($blog_detail)>0){
        $blog_content = $blog_detail[0];
    }
}
?>
<?php if (isset($blog_content)){ ?>
<form role="form" method="post" action="<?php echo base_url().'index.php/userblog/edit/'.$blog_content->id ?>" enctype="multipart/form-data">
    <input type="hidden" name="oldimage" value="<?php echo $blog_content->image ?>">
<?php } else { ?>
<form role="form" method="post" action="<?php echo base_url().'index.php/userblog/add' ?>" enctype="multipart/form-data">
<?php } ?>
    <br>
    <br>
    <br>


    <div class="form-group">

        <label> Select Category</label>

        <select class="form-control" name="cat_id" required>

            <?php foreach($CATEGORY as  $cat){ ?>

                <option <?php if (isset($blog_content) && $blog_content->cat_id == $cat->id) echo "selected"; ?> value="<?php echo $cat->id; ?>"><?php echo $cat->title; ?></option>

            <?php } ?>

        </select>

    </div>

    <div class="form-group">

        <label> Title</label>

        <input type="text" class="form-control" placeholder="category Title" name="title" value="<?php if (isset($blog_content) && $blog_content->title) echo $blog_content->title; ?>" required>

    </div>

    <div class="form-group">


        <label>Post your Youtube Link here</label>

        <input type="text" class="form-control" value="<?php if (isset($blog_content) && $blog_content->youtube_video) echo $blog_content->youtube_video; ?>"  name="youtube_video" >

    </div>  

    <div class="form-group">

        <label>Image</label>

        <input type="file" class="form-control"  name="image" >

    </div>
    

    <div class="form-group">

        <label> Content</label>

        <textarea id="postcontent" class="form-control" rows="40" name="content"><?php if (isset($blog_content) && $blog_content->content) echo $blog_content->content; ?>
            
        </textarea>

    </div>

    <div name="status" class="form-group">

        <label>Status</label>

        <select name="status" class="form-control" required>

            <option <?php if (isset($blog_content) && $blog_content->status == 1) echo "selected"; ?> value="1">Active</option>

            <option <?php if (isset($blog_content) && $blog_content->status == 0) echo "selected"; ?> value="0">Inactive</option>

        </select>

    </div>                            



    <button type="submit" name="savedata" class="btn btn-default">Submit Button</button>

    <button type="reset" class="btn btn-default">Reset Button</button>



</form>
<script type="">
    CKEDITOR.replace( '#postcontent' );
</script>