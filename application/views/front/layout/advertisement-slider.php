<?php //$slider = $this->banner_model->selectbannerbyid(1); // print_r($slider);?>

<?php if($slider[0]->type=='slider'){ ?> 

 <div id="myCarousel" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->

	 <?php $sliderimages1 = $this->banner_model->selectSliderBanner($slider[0]->id); ?>

    <ol class="carousel-indicators">

	   <?php $j=0; foreach($sliderimages1 as $banner){ $j++; ?>	

      <li data-target="#myCarousel" data-slide-to="0" class="<?php echo($j=='1')?'active':''; ?>"></li>

      <?php }  ?>

    </ol>

    <!-- Wrapper for slides -->

    <div class="carousel-inner" role="listbox">

     

	  <?php $i=0; foreach($sliderimages1 as $banner){ $i++; ?>	

      <div class="item <?php echo($i=='1')?'active':''; ?>">

       <a href="<?php echo $banner->sliderimgLink; ?>" target="_blank" ><img src="<?php echo base_url(); ?>uploads/banner/<?php echo $banner->image; ?>" alt="Flower" style="height:406px"></a>

      </div>

	  <?php }  ?>

    </div>



    <!-- Left and right controls -->

    <a class="left prev carousel-control" href="#myCarousel" role="button" data-slide="prev">

      <span class="glyphicon fa fa-2x fa-angle-left" aria-hidden="true"></span>

      <span class="sr-only">Previous</span>

    </a>

    <a class="right next carousel-control" href="#myCarousel" role="button" data-slide="next">

      <span class="glyphicon fa fa-2x fa-angle-right" aria-hidden="true"></span>

      <span class="sr-only">Next</span>

    </a>

  </div>

    

<?php } ?>

<?php if($slider[0]->type=='youtube'){ ?>

	<iframe class="videosec" src="https://www.youtube.com/embed/<?php echo $slider[0]->youtube; ?>?autoplay=1" width="98%" height="400px" frameborder="0" allowfullscreen></iframe>

<?php } ?>

<?php if($slider[0]->type=='flash'){ ?>

	<?php echo $slider[0]->flash; ?>

<?php } ?>

<?php $block_dashboard = $this->staticblock_model->getBlockById(6);  ?>

<?php $c_clock = ($login_user[0]->comment_section==0)?'disabled':''; ?>

<div class="panel-body">

	<div class="widget-summary">

		<div class="widget-summary-col">

			<a href="#"><h4 class="banner-title"><?php echo $block_dashboard[0]->content; ?></h4></a>

				<form role="form" class="form-horizontal" method="post" <?php echo $c_clock; ?>>

					<div class="form-group no-m">

							<!--<input type="text" class="form-control input-sm no-border" name="comment" id="#comment" required placeholder="Comments" autocomplete='off' >-->

							
							<input type="textarea"  <?php echo $c_clock; ?> class="form-control input-sm no-border mb-10" rows='4' name="comment"  required placeholder="Comments" autocomplete='off'>

							

							<span class="input-group-btn">

								<button class="btn btn-default btn-sm" name="postComments" type="submit" <?php echo $c_clock; ?>>POST </button>

							</span>

					</div>

				</form>

				<p class="small m-md">

				<a href="<?php echo base_url('index.php/user/like_comments/LIKE'); ?>" class="mr10 text-danger"><i class="fa fa-thumbs-up mr5"></i> Like (<?php echo count($this->comment_model->selectAllLikeUnlikeData(1)); ?>) </a>

				<a href="<?php echo base_url('index.php/user/like_comments/UNLIKE'); ?>" class="mr10 text-danger"><i class="fa fa-thumbs-down mr5"></i> UnLike (<?php echo count($this->comment_model->selectAllLikeUnlikeData(0)); ?>)</a>

				<script src="https://apis.google.com/js/platform.js"></script>

				<a class="g-ytsubscribe" data-channelid="UCoS_IOvV4_Wb9bXWSTvsulQ" data-layout="default" data-count="hidden"></a>

			</p>

		</div>

	</div>

</div>

										