 <?php $this->load->view('admin/layout/header'); ?>
<script type="text/javascript" src="<?php echo base_url('assets/admin'); ?>/tiny_mce/tiny_mce.js"></script>
	<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
		forced_root_block : "",
		plugins : "openmanager,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,openmanager",
		//theme_advanced_buttons4 : "openmanager",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,		
		//Open Manager Options
		file_browser_callback: "openmanager",
		open_manager_upload_path: '../../../../uploads/',
		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",
		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Style formats
		style_formats : [
			{title : 'Bold text', inline : 'b'},
			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
			{title : 'Example 1', inline : 'span', classes : 'example1'},
			{title : 'Example 2', inline : 'span', classes : 'example2'},
			{title : 'Table styles'},
			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
		],

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Taxes & Transactions</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Taxes & Transactions</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">						
						<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>                           
							<section class="panel">							
							<div class="panel-body">								
								<div class="table-responsive">
										<form method="post">
											<h2 class="panel-title pull-left">Manage Transactions</h2><br><br>
											<table class="table table-bordered table-striped table-condensed" >	
												<thead>
													<tr>														
														<th>Maximum Number of transactions in a day</th> 
														<th>Maximum amount in a single transactions</th> 
														<th>Maximum Number of transactions in a month</th> 
													</tr>	
												</thead>
												<tbody>
													<tr>																											
														<td>														
															<input type="number" name="no_transaction_day" value="<?php echo $tax[0]->no_transaction_day; ?>" placeholder="Maximum Number of transactions in a day" class="form-control">
														</td>	
														<td>														
															<input type="number" name="amount_single_transaction" value="<?php echo $tax[0]->amount_single_transaction; ?>" placeholder="Maximum amount in a single transactions" class="form-control">
														</td>
														<td>														
															<input type="number" name="no_transaction_month" value="<?php echo $tax[0]->no_transaction_month; ?>" placeholder="Maximum Number of transactions in a month" class="form-control">
														</td>
													</tr>
												</tbody>
											</table><br>
											<h2 class="panel-title pull-left">Manage Taxes</h2><br><br>
											<table class="table table-bordered table-striped table-condensed" >	
												<thead>
													<tr>														
														<th>TDS</th> 
														<th>Admin Charge</th> 
														<th>Tax</th>
														<th>Promotional income (%)</th> 
<th>Direct Sale Commission(%)</th>
													</tr>	
												</thead>
												<tbody>
													<tr>																											
														<td>														
															<input type="number" name="tds" value="<?php echo $tax[0]->tds; ?>" placeholder="TDS" class="form-control">
														</td>	
														<td>														
															<input type="number" name="admin_charge" value="<?php echo $tax[0]->admin_charge; ?>" placeholder="Charge" class="form-control">
														</td>
														<td>														
															<input type="number" name="tax" value="<?php echo $tax[0]->tax; ?>" placeholder="Tax" class="form-control">
														</td>
														<td>														
															<input type="number" name="promotinalincomme" value="<?php echo $tax[0]->promotinalincomme; ?>" placeholder="promotinalincomme %" class="form-control">
														</td>
<td>														
															<input type="number" name="directincome" value="<?php echo $tax[0]->directincome; ?>" placeholder="directincome %" class="form-control">
														</td>
													</tr>
												</tbody>
											</table>
											<h2 class="panel-title pull-left">Manage Bonus</h2><br><br>
											<table class="table table-bordered table-striped table-condensed" >	
												<thead>
														
												</thead>
												<tbody>
												  <tr>	
														<th>Condition 1 </th>
														<th>Days </th> 
														<th>Total Sale </th> 
														<th>Bonus (%)  </th>
													</tr>
													<tr>
														<td></td>
														<td>														
															<input type="number" name="cond_oneday" value="<?php echo $tax[0]->cond_oneday; ?>"  class="form-control">
														</td>	
														<td>														
															<input type="number" name="cond_onesale" value="<?php echo $tax[0]->cond_onesale; ?>" class="form-control">
														</td>
														<td>														
															<input type="number" name="cond_onebonus" value="<?php echo $tax[0]->cond_onebonus; ?>" class="form-control">
														</td>														
													</tr>
													 <tr>	
														<th>Condition 2</th>
														<th>Days </th> 
														<th>Total Sale </th> 
														<th>Bonus (%)  </th>
													</tr>
													<tr>
														<td></td>
														<td>														
															<input type="number" name="cond_twoday" value="<?php echo $tax[0]->cond_twoday; ?>" class="form-control">
														</td>	
														<td>														
															<input type="number" name="cond_twosale" value="<?php echo $tax[0]->cond_twosale; ?>" class="form-control">
														</td>
														<td>														
															<input type="number" name="cond_twobonus" value="<?php echo $tax[0]->cond_twobonus	; ?>" class="form-control">
														</td>														
													</tr>
													 <tr>	
														<th>Condition 3 </th>
														<th>Days </th> 
														<th>Total Sale </th> 
														<th>Bonus (%)  </th>
													</tr>
													<tr>
														<td></td>
														<td>														
															<input type="number" name="cond_threeday" value="<?php echo $tax[0]->cond_threeday; ?>" class="form-control">
														</td>	
														<td>														
															<input type="number" name="cond_threesale" value="<?php echo $tax[0]->cond_threesale; ?>" class="form-control">
														</td>
														<td>														
															<input type="number" name="cond_threebonus" value="<?php echo $tax[0]->cond_threebonus; ?>"  class="form-control">
														</td>														
													</tr>
													 <tr>	
														<th>Condition 4 </th>
														<th>Days </th> 
														<th>Total Sale </th> 
														<th>Bonus (%)  </th>
													</tr>
													<tr>
														<td></td>
														<td>														
															<input type="number" name="cond_fourday" value="<?php echo $tax[0]->cond_fourday; ?>" class="form-control">
														</td>	
														<td>														
															<input type="number" name="cond_foursale" value="<?php echo $tax[0]->cond_foursale; ?>" class="form-control">
														</td>
														<td>														
															<input type="number" name="cond_fourbonus" value="<?php echo $tax[0]->cond_fourbonus; ?>" class="form-control">
														</td>														
													</tr>
													 <tr>	
														<th>Condition 5 </th>
														<th>Days </th> 
														<th>Total Sale </th> 
														<th>Bonus (%)  </th>
													</tr>
													<tr>
														<td></td>
														<td>														
															<input type="number" name="cond_fiveday" value="<?php echo $tax[0]->cond_fiveday; ?>" class="form-control">
														</td>	
														<td>														
															<input type="number" name="cond_fivesale" value="<?php echo $tax[0]->cond_fivesale; ?>" class="form-control">
														</td>
														<td>														
															<input type="number" name="cond_fivebonus" value="<?php echo $tax[0]->cond_fivebonus; ?>" class="form-control">
														</td>														
													</tr>
												</tbody>
											</table>
											<h2 class="panel-title pull-left">Manage Request For Work</h2><br><br>
											<table class="table table-bordered table-striped table-condensed" >	
												<thead>
													<tr>														
														<th>Request For Work</th>
													</tr>	
												</thead>
												<tbody>
													<tr>																											
														<td align="left">
															<select name="requestwork">
																<option <?php echo($tax[0]->requestwork==1)?'selected':''; ?> value="1">Show</option>
																<option <?php echo($tax[0]->requestwork==0)?'selected':''; ?> value="0">Hide</option>
															</select>	
														</td>														
													</tr>
												</tbody>
											</table>
											<h2 class="panel-title pull-left">Wallet</h2><br><br>
											<table class="table table-bordered table-striped table-condensed" >	
												<thead>
													<tr>														
														<th>SMB Title</th>
														<th>SMF Title</th>
													</tr>	
												</thead>
												<tbody>
													<tr>																											
														<td align="left">
															<input type="text" name="walletTitleOne" value="<?php echo $tax[0]->walletTitleOne; ?>" placeholder="promotinalincomme %" class="form-control">	
														</td>	
<td align="left">
															<input type="text" name="walletTitleTwo" value="<?php echo $tax[0]->walletTitleTwo; ?>" placeholder="promotinalincomme %" class="form-control">	
														</td>														
													</tr>
													
												</tbody>
											</table>
											
											
											<h2 class="panel-title pull-left">Mail Template</h2><br><br>
											<table class="table table-bordered table-striped table-condensed" >	
												<thead>
													<tr>														
														<th>Registration mail template({$name},{$USERID},{$password})</th>
													</tr>	
												</thead>
												<tbody>
													<tr>																											
														<td align="left">
															<textarea class="form-control" style="height:300px;" name="regmailcontent"><?php echo $tax[0]->regmailcontent; ?></textarea>
														</td>																												
													</tr>
													
												</tbody>
											</table>
											<table class="table table-bordered table-striped table-condensed" >	
												<thead>
													<tr>														
														<th>Newslatter mail template</th>
													</tr>	
												</thead>
												<tbody>
													<tr>
														<td align="left">
															<textarea class="form-control" name=""><?php //echo $tax[0]->regmailcontent; ?></textarea>
														</td>														
													</tr>
													
												</tbody>
											</table>
											
											<button type="submit" name="updateTaxSetting" class="btn btn-info"> Update </button>
										</form>
										
								</div>
							</div>
						</section>                         
						</div>
					</div>					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>	
		
		
	

	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>