 <?php $this->load->view('admin/layout/header'); ?>
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Promotional Income</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Promotional Income</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">						
						<div class="col-md-12">										
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions"></div>
									<h2 class="panel-title"></h2>								
								</header>
								<div class="panel-body" >
									<form class="form-horizontal" method="post">
										<h4 class="mb-xlg"></h4>
											<fieldset>												
												<div class="form-group">													
													<div class="col-md-5">
														<div class="input-daterange input-group" data-plugin-datepicker="">
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="startDate" placeholder="start Date" value="<?php echo(isset($_POST['startDate']))?$_POST['startDate']:''; ?>" >
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="endDate" placeholder="end Date" value="<?php echo(isset($_POST['endDate']))?$_POST['endDate']:''; ?>" >                                                        
														</div>
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control" name="user_id" placeholder="Enter USERID" value="<?php echo(isset($_POST['user_id']))?$_POST['user_id']:''; ?>" >
													</div>													
                                                    <div class="col-md-2">															
														<select class="form-control mb-md" name="status">
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='all')?'selected':'' ?> value="all">All</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='pending')?'selected':'' ?> value="pending">Pending</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='paid')?'selected':'' ?> value="paid">Paid</option>
														</select>                                                        
													</div>                                                    
                                                    <div class="col-md-3">
														<button type="submit" name="searchproincome" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
													</div>
												</div>                                                  
											</fieldset>
										</form>     
                                        <div class="table-responsive">
									<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" >	
										<thead>
											<tr>
												<th>S.No</th>
												<th>USERID</th>
												<th>Left TR</th>
												<th>Right TR</th>
												<th>CLTR</th>
												<th>CRTR</th>
												<th>Income</th>
												<th>TDS</th>
												<th>Admin Charges</th>
												<th>Net Income</th>
												<th>Transaction ID</th>
												<th>Status</th>
												<th>Date</th>
												<th>Edit</th>
											</tr>
										</thead>
										<tbody>
										
										<?php 
										$tds = 0; 
										$left_bv = 0;
										$right_bv = 0;
										$right_bv = 0;
										$clbv = 0;
										$crbv = 0;
										$income = 0;
										$admincharge = 0;
										$netIncome = 0;
										if(count($incomedata)>0)
										{ $i=0;
											foreach($incomedata as $data)
											{
												$tds += $data->tds;
												$left_bv += $data->left_bv;
												$right_bv += $data->right_bv;
												$clbv += $data->clbv;
												$crbv += $data->crbv;
												$income += $data->income;
												$admincharge += $data->admincharge;
												$netIncome += $data->netIncome;
											} 
										}
										?>
											<tr>
													<td></td>
													<td><strong><?php echo $data->uid; ?></strong></td>
													<td><strong><?php echo $left_bv; ?></strong></td>
													<td><strong><?php echo $right_bv; ?></strong></td>
													<td><strong><?php echo $clbv; ?></strong></td>
													<td><strong><?php echo $crbv; ?></strong></td>
													<td><strong><?php echo $income; ?></strong></td>
													<td><strong><?php echo $tds; ?></strong></td>
													<td><strong><?php echo $admincharge; ?></strong></td>
													<td><strong><?php echo $netIncome; ?></strong></td>
													<td></td>
													<td></td> 
													<td></td>
													<td></td> 
												</tr>
										<?php 
										if(count($incomedata)>0){ $i=0;
										foreach($incomedata as $data){	 $i++;
										?> 
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $data->uid; ?></td>
													<td><?php echo $data->left_bv; ?></td>
													<td><?php echo $data->right_bv; ?></td>
													<td><?php echo $data->clbv; ?></td>
													<td><?php echo $data->crbv; ?></td>
													<td><?php echo $data->income; ?></td>
													<td><?php echo $data->tds; ?></td>
													<td><?php echo $data->admincharge; ?></td>
													<td><?php echo $data->netIncome; ?></td>
													<td><?php echo $data->transactionID; ?></td>
													<td><?php echo $data->status; ?></td> 
													<td><?php echo $data->crDate; ?></td>
													<td><a target="_blank" href="<?php echo base_url('index.php/admin/managepromotionalincome_edit/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td> 
												</tr>
										<?php 
										} }
										?>		
										</tbody>    
									</table>
                                    
								</div>
								</div>
							</section>						
						</div>						

					</div>					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>	
		
	

<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		
		<script src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
				
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.tabletools.js"></script>
		
		
</body>
		</html>		