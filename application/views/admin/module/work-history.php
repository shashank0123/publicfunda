 <?php $this->load->view('admin/layout/header'); ?>
<?php 
		if(isset($_POST['checkWorkHistory']))
		{
			$userID = (!empty($_POST['user_id']))?$_POST['user_id']:'0';
			$sd = strtotime($_POST['startDate']);
			$ed = strtotime($_POST['endDate']);
			$status = $_POST['status'];
			$cid = $_POST['campaign_id'];
		}
		else
		{
			$userID = 0;
			$sd = 0;
			$ed = 0;
			$status = 0;
			$cid = 0;
		}	
?>
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Work History</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Work History</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">						
						<div class="col-md-12">										
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions"></div>
									<h2 class="panel-title">Work History</h2>								
								</header>
								<div class="panel-body" >
									<form class="form-horizontal" method="post">
										<h4 class="mb-xlg"></h4>
											<fieldset>												
												<div class="form-group">													
													<div class="col-md-4">
														<div class="input-daterange input-group" data-plugin-datepicker="">
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="startDate" placeholder="start Date" value="<?php echo(isset($_POST['startDate']))?$_POST['startDate']:''; ?>" required>
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="endDate" placeholder="end Date" value="<?php echo(isset($_POST['endDate']))?$_POST['endDate']:''; ?>" required>                                                        
														</div>
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control" name="user_id" placeholder="Enter USERID" value="<?php echo(isset($_POST['user_id']))?$_POST['user_id']:''; ?>" >
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control" name="campaign_id" placeholder="Campaign ID" value="<?php echo(isset($_POST['campaign_id']))?$_POST['campaign_id']:''; ?>" >
													</div>
                                                    <div class="col-md-1">															
														<select class="form-control mb-md" name="status">
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='all')?'selected':'' ?> value="all">All</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='1')?'selected':'' ?> value="1">Pending</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='0')?'selected':'' ?> value="0">Clicked</option>
														</select>                                                        
													</div>                                                    
                                                    <div class="col-md-3">
														<button type="submit" name="checkWorkHistory" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
														<a target="_blank" href="<?php echo base_url('index.php/mis/work_history_mis/'.$userID.'/'.$sd.'/'.$ed.'/'.$status.'/'.$cid);  ?>" class="btn btn-warning pull-right"><i class="fa  fa-repeat"></i> Download PDF </a>
													</div>
												</div>                                                  
											</fieldset>
										</form>     
                                        <div class="table-responsive">
									<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" >	
										<thead>
											<tr>
												<th>S.No</th>
												<th>Page Title</th>
												<th>USERID</th>
												<th>Campaign ID</th>
												<th>System IP</th>
												<th>Server IP</th>
												<th>Place</th>
												<th>Clicked Time</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
										<?php 
										if(count($workHistoryData)>0){ $i=0;
										foreach($workHistoryData as $workData){	 $i++;
										?> 
										<?php $campaign = $this->campaign_model->selectCampaignByID($workData->link_id); ?>	
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $campaign[0]->campaignTitle; ?></td>
													<td><?php echo $workData->user_id; ?></td>
													<td><?php echo $workData->link_id; ?></td>
													<td><?php echo($workData->system_ip!="")?$workData->system_ip:'N/A'; ?></td>
													<td><?php echo($workData->clickedIP!="")?$workData->clickedIP:'N/A'; ?></td>
													<td><?php echo($workData->clickedIP!="")?getLocation($workData->clickedIP):'N/A';//$workData->clickedIP; ?></td>
													<td><?php echo(!empty($workData->clickDate))?date('d-m-Y h:m'):'N/A'; ?></td>
													<td>
														<?php if($workData->status==1){ ?>
															<button class="btn btn-danger">Pending</button>
														<?php }else{ ?>
															<button class="btn btn-success">Clicked</button>
														<?php } ?>
													</td> 
												</tr>
										<?php 
										} }
										?>		
										</tbody>    
									</table>
                                    
								</div>
								</div>
							</section>						
						</div>						

					</div>					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>	
		
	

<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		
		<script src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
				
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.tabletools.js"></script>
		
		
</body>
		</html>		