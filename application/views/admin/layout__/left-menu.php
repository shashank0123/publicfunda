<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li class="nav-active">
										<a href="<?php echo base_url('index.php/admin/dashboard'); ?>">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('index.php/admin/list_user'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Users</span>
										</a>
									</li>									
									<li>
										<a href="<?php echo base_url('index.php/admin/list_plans'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Plan</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('index.php/rewards/listing/common'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Rewards</span>
										</a>
									</li>									
									<li>
										<a href="<?php echo base_url('index.php/admin/list_helpdepartment'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Help Department Listing</span>
										</a>
									</li>
									
									<li>
										<a href="<?php echo base_url('index.php/bookadd/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Book Your Add</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('index.php/purchasecalculator/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Purchase Calculator</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('index.php/bookingcalculator/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Book Your Add Calculator</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('index.php/banner/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Slider</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('index.php/dashboard/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Dashboard</span>
										</a>
									</li>
								</ul>
							</nav>
				
						</div>
				
					</div>
				
				</aside>