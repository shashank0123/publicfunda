 <?php $this->load->view('admin/layout/header'); ?>

<style>

.material-icons{

	font-size: 14px;

}

</style>

			<div class="inner-wrapper pt-30">

				<!-- start: sidebar -->

				<?php $this->load->view('admin/layout/left-menu'); 
				?>

				<!-- end: sidebar -->

				<section role="main" class="content-body">

					<header class="page-header">

						<h2>Transaction History</h2>					

						<div class="right-wrapper pull-right">

							<ol class="breadcrumbs">

								<li>

									<a href="<?php //echo base_url('index.php/user/dashboard'); ?>">

										<i class="fa fa-home"></i>

									</a>

								</li>								

								<li><a href=""><span>Transaction History</span></a></li>

							</ol>					

							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>

						</div>

					</header>

					<!-- start: page -->



					<div class="row">	

						<div class="col-md-12"> <br>

							<?php echo $this->session->flashdata('message'); ?> 							

							<section class="panel">								

							<div class="panel-body">		

								<div id="group-info" class="tab-pane <?php echo(isset($_POST['checkGroup']))?'active':''; ?>">

							<?php 

							if(isset($_POST['checkGroup']))
							{
								$sd = strtotime($_POST['start']);
								$ed = strtotime($_POST['end']);

							}

							else

							{

								$neftNo = 0;

								$sd = 0;

								$ed = 0;

							}	

							?>

							<form class="form-horizontal" method="get">

								<h4 class="mb-xlg"></h4>

								<fieldset>												

									<div class="form-group">													

										

										<!-- <div class="col-md-4">

											<input type="text" name="neftNo" placeholder="NEFT NO. / RTGS" class="form-control" value="<?php echo(isset($_POST['neftNo']))?$_POST['neftNo']:''; ?>">

										</div> -->

										<div class="col-md-6 mb-md" >

											<div class="input-daterange input-group" data-plugin-datepicker="">

												<span class="input-group-addon">
													From &nbsp 

													<i class="fa fa-calendar"></i>

												</span>

												<input type="text" class="form-control" name="start" value="<?php echo(isset($_POST['start']))?$_POST['start']:''; ?>" required>

												<span class="input-group-addon">to <i class="fa fa-calendar"></i></span>

												<input type="text" class="form-control" name="end" value="<?php echo(isset($_POST['end']))?$_POST['end']:''; ?>" required>                                                        

											</div>

										</div>



										<div class="col-md-3">

											<button type="submit" name="checkGroup" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>

											<button type="button" onClick="window.location.reload()" class="btn btn-warning"><i class="fa  fa-repeat"></i> Reset </button>	
											<a href="<?php echo base_url('index.php/admin/createGroupStatementInPdf/'.$group_id.'/'.$sd.'/'.$ed);  ?>" class="btn btn-warning pull-right"><i class="fa  fa-repeat"></i> Download PDF </a>														

										</div>

									</div>                                                  

								</fieldset>

							</form>						

								<div class="table-responsive">                             

									<table class="table table-bordered table-striped" id="datatable-default">									

							<thead>

								<tr class="gradeX">

									<th>SNo.</th>
									<th>Date Time</th>
									<th>USER ID</th>
									<th>NEFT</th>
									<th>Particulars</th>
									<th>Debit</th>
									<th>Credit</th>
									<th>Total Outstanding</th>

									

								</tr>

							</thead>

							<tbody>

							 <?php $i=0; $total = 0; foreach($transactions as $data){ $i++; ?>
							 	<?php if ($data->particulars == "Royality Income") continue;?>
								<tr class="gradeX">

									<td><?php echo  $i; ?></td>
									
									<td><?php echo $data->created_at; ?></td>
									<td><?php echo $data->user_id; ?></td>
									<td>NA</td>
									<td><?php echo $data->particulars; ?></td>
									<td><?php if ($data->transaction_type == "Debit") echo $data->amount; else echo "NA"; ?></td>
									<td><?php if ($data->transaction_type == "Credit") echo $data->amount; else echo "NA"; ?></td>
									<td><?php if ($data->transaction_type == "Credit") echo $total = $data->amount + $total;
									if ($data->transaction_type == "Debit") echo  $total = $total - $data->amount; ?></td>
									

																	</tr>

								<?php } ?>																				

							</tbody>

						</table>

								</div>

							</div>

						</section>

                         

						</div>

						



					</div>

					

					<!-- end: page -->

				</section>

			</div>			

			

		</section>

	





		

		<!-- Vendor -->

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>

		

	





		

		

	</body>



<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->

</html>