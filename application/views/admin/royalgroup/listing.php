 <?php $this->load->view('admin/layout/header'); ?>

<style>

.material-icons{

	font-size: 14px;

}

</style>

			<div class="inner-wrapper pt-30">

				<!-- start: sidebar -->

				<?php $this->load->view('admin/layout/left-menu'); ?>

				<!-- end: sidebar -->

				<section role="main" class="content-body">

					<header class="page-header">

						<h2>Manage Group</h2>					

						<div class="right-wrapper pull-right">

							<ol class="breadcrumbs">

								<li>

									<a href="<?php //echo base_url('index.php/user/dashboard'); ?>">

										<i class="fa fa-home"></i>

									</a>

								</li>								

								<li><a href=""><span>Manage Group</span></a></li>

							</ol>					

							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>

						</div>

					</header>

					<!-- start: page -->



					<div class="row">	

						<div class="col-md-12"> <br>

							<?php echo $this->session->flashdata('message'); ?> 							

							<section class="panel">								

							<div class="panel-body">								

								<div class="table-responsive">

								<a href="<?php echo base_url('index.php/royalgroups/add/'.$type); ?>" class="mb-xs mt-xs mr-xs btn btn-primary pull-right">Add New</a>	

								<a href="<?php echo base_url('index.php/royalgroups/calculatebalance'); ?>/<?php echo ($type=='common')?'special':'common'; ?>" class="mb-xs mt-xs mr-xs btn btn-primary pull-right">Distribute Income</a>	

                                 

									<table class="table table-bordered table-striped" id="datatable-default">									

							<thead>

								<tr class="gradeX">

									<th>SNo.</th>

									<?php if($type=='special'){ ?>

									<th>USERID</th>

									<?php } ?>

									<th>Title</th>
									<th>Entry Price</th>
									<th>User Earning</th>
									<th>Level wise</th>

									<th>Status</th>

									<th>Action</th>

								</tr>

							</thead>

							<tbody>

							 <?php $i=0; foreach($bookdata as $data){ $i++; ?>

								<tr class="gradeX">

									<td><?php echo  $i; ?></td>
									
									<td><?php echo $data->group_name; ?></td>
									<td><?php echo $data->enter_amount; ?></td>
									<td><?php echo $data->per_user_max; ?></td>
									<td><?php echo $data->level1.','.$data->level2.','.$data->level3; ?></td>
									

									<td><?php echo($data->status==1)?'<i data="2" class="status_checks btn btn-success">Active</i>':'<i data="2" class="status_checks btn btn-danger">Inactive</i>'; ?></td>

									<td>

										<a href="<?php echo base_url('index.php/royalgroups/edit/'.$data->royal_group_id.'/'.$type); ?>" title="edit record"><i class="fa fa-pencil"></i></a>&nbsp&nbsp
										<?php if ($data->type != "Basic"){?>
										<a href="<?php //echo base_url('index.php/royalgroups/deleteRecord/'.$data->royal_group_id.'/'.$type); ?>" title="delete record"><i class="fa fa-trash1-o"></i></a>&nbsp&nbsp
									<?php } ?>
									<a href="<?php echo site_url('index.php/royalgroups/payment_history/'.$data->royal_group_id.''); ?>"><i class="fa fa-gift"></i>
								   </a>

									</td> 

								</tr>

								<?php } ?>																				

							</tbody>

						</table>

								</div>

							</div>

						</section>

                         

						</div>

						



					</div>

					

					<!-- end: page -->

				</section>

			</div>			

			

		</section>

	





		

		<!-- Vendor -->

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>	
	</body>
	</html>