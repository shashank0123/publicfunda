<?php $invoicedata = $this->invoicecontent_model->getBlockById(0); ?>
<style>
.midiv{max-width:1170px; margin:0 auto;}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $(function(){
    $( "#datepicker" ).datepicker();
	$( "#datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
	$( "#datepicker2" ).datepicker();
	$( "#datepicker2" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
  });
  </script>
<div class="midiv">
<table>
	<tr>
		<form method="post" >
		<td><input type="text" placeholder="Start Date" id="datepicker" name="sdate" style="padding:5px;" value=""><?php echo(isset($_POST['sdate']))?$_POST['sdate']:'' ?></td>
		<td><input type="text" placeholder="End Date" name="edate" id="datepicker2" style="padding:5px;" value=""><?php echo(isset($_POST['edate']))?$_POST['edate']:'' ?></td>
		<td><input type="text" placeholder="User ID" name="userid" style="padding:5px;" value="<?php echo(isset($_POST['userid']))?$_POST['userid']:'' ?>"></td>
		<td><button type="submit" name="searchinvoice" style="float:right; padding:5px 15px;">Submit</button></td>
		<td><button type="button" style="float:right; padding:5px 15px;" onclick="return window.print();"><strong>Print</strong></button></td>
		</form>
	</tr>
	
</table>
<?php foreach($userData as $user){ ?>
<?php 
	if(isset($_POST['searchinvoice']))
	{
		if(!empty($_POST['sdate']) && !empty($_POST['edate']))
		{
			$sdate = $_POST['sdate'];
			$edate = $_POST['edate'];
			$this->db->where('invDate >=', $sdate);
			$this->db->where('invDate <=', $edate);
			//$this->db->where("invDate BETWEEN $sdate AND $edate");
		}			
	}	
?>
<?php $invData = $this->tree_model->selectUserInvoice($user->id); ?>
<table  width="100%" cellpadding="10" style="border:1px solid black; margin-bottom:8px;" >
	<tr align="center">
		<td colspan='2'><h3>Invoice<br>USERID - <?php echo $user->id; ?></h3></td>
	</tr>
	<tr>
		<td width="70%">
			<img src="<?php echo base_url('uploads/invoice/'.$invoicedata[0]->logo); ?>">
		</td>
		<td><?php echo $invoicedata[0]->from_address; ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" cellpadding="10">
				<tr>
					<th align="left">Invoice To</th>
					<th align="right">Download Date </th>
				</tr>
				<tr>
					<td align="left"><?php echo $user->name; ?><br>
						<?php echo $user->address; ?>
					</td>
					<td align="right"><?php echo date('d/m/Y'); ?></td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td colspan="2">
			<table width="100%" border='1' bordercolor="black" cellpadding="15">
				<tr>
					<th align="left">SNo.</th>
					<th align="left">Description of Service</th>
					<th align="left">Invoice No.</th>
					<th align="left">Transaction ID</th>
					<th align="left">Invoice Date</th>
					<th align="left">Amount</th>
				</tr>
				<?php $tax = 0; $totalAmount = 0; $finalAmount = 0; $finalAmount=0;?>
				<?php if(count($invData)>0){ $i=0; ?>
				<?php foreach($invData as $inv){ $i++; ?>
				<?php if($inv->status=='paid'){ ?>				
				<?php $finalAmount += $inv->finalAmount; ?>
					
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $inv->planName; ?></td>
					<td><?php echo $inv->invoiceID; ?></td>
					<td><?php echo $inv->transactionID; ?></td>
					<td><?php echo date('d/m/Y h:m a',$inv->invTime); ?></td>
					<td>Rs.<?php echo  $inv->finalAmount; ?></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="4"><strong>Net Amount<strong></td>
					<td>Rs.<?php echo $inv->amount; ?></td>
				</tr>				
				<tr>
					<td></td>
					<td colspan="4"><strong>Service Tax<strong></td>
					<td><?php echo $inv->tax; ?>%</td>
				</tr>				
				
				<tr>
					<td></td>
					<td colspan="4"><strong>Total<strong></td>
					<td>Rs.<?php echo  $inv->finalAmount; ?></td>
				</tr>
				
<?php } } } ?>				
			</table>			
		
		</td>
	</tr>
	<tr>
		<td><?php /* ?><strong>Amount Chargable (in words)</strong>
		<br><?php echo $this->numbertowords->convert_number($finalAmount); ?> <?php */ ?>
		</td>
		<td><strong> E.&.O.E </strong></td>
	</tr>
	<tr>
		<td colspan='2' align="center"><strong>DECLARATION</strong>
		<br><?php echo $invoicedata[0]->declaration; ?></td>
	</tr>
	<tr>
		<td><strong>Terms & Conditions</strong>
		<br><?php echo $invoicedata[0]->terms_condition; ?></td>
		<td><strong><img src="<?php echo base_url('uploads/invoice/'.$invoicedata[0]->signature); ?>"></strong></td>
	</tr>
	
</table>
<?php } ?>
<div>