<table border='1'>
										<thead>
											<tr>
												<th>S.No</th>
												<th>Closing Date</th>	
                                                <th>Left BV</th>
                                                <th>Right BV</th>
                                                <th>CLBV</th>
                                                <th>CRBV</th>
												<th>Income</th>
												<th>TDS(10%)</th>
                                                <th>Admin Charges(5%)</th>
                                                <th>Net Income</th>
                                                <th>Transaction ID</th>											
                                                <th>Date</th>																								
											</tr>
										</thead>
										<tbody>	
											<?php if(count($promotionalincome)>0){ ?>
											<?php $i=0; foreach($promotionalincome as $income){ $i++; ?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $income->closingDate; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $income->left_bv; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $income->right_bv; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $income->clbv; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $income->crbv; ?></td>
												<td><i class="fa fa-rupee"></i><?php echo $income->income; ?></td>
												<td><i class="fa fa-rupee"></i><?php echo $income->tds; ?></td>
												<td><i class="fa fa-rupee"></i> <?php echo $income->admincharge  ; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $income->netIncome; ?></td>
                                                <td><?php echo $income->transactionID; ?></td>												
                                                <td><?php echo $income->crDate; ?></td>                                                
                                            </tr>
											<?php } ?>
											<?php }else{ ?>
												<tr>
													<td colspan="12">Record not found</td>
												</tr>
											<?php } ?>
										</tbody>   
									</table>