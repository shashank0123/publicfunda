<table border='1'>
										<thead>
											<tr>
												<th>S.No</th>
												<th>Page Title</th>
												<th>USERID</th>
												<th>Campaign ID</th>
												<th>System IP</th>
												<th>Server IP</th>
												<th>Place</th>
												<th>Clicked Time</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
										<?php 
										if(count($workHistoryData)>0){ $i=0;
										foreach($workHistoryData as $workData){	 $i++;
										?> 
										<?php $campaign = $this->campaign_model->selectCampaignByID($workData->link_id); ?>	
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $campaign[0]->campaignTitle; ?></td>
													<td><?php echo $workData->user_id; ?></td>
													<td><?php echo $workData->link_id; ?></td>
													<td><?php echo($workData->system_ip!="")?$workData->system_ip:'N/A'; ?></td>
													<td><?php echo($workData->clickedIP!="")?$workData->clickedIP:'N/A'; ?></td>
													<td><?php echo($workData->clickedIP!="")?getLocation($workData->clickedIP):'N/A';//$workData->clickedIP; ?></td>
													<td><?php echo(!empty($workData->clickDate))?date('d-m-Y h:m'):'N/A'; ?></td>
													<td>
														<?php if($workData->status==1){ ?>
															<button class="btn btn-danger">Pending</button>
														<?php }else{ ?>
															<button class="btn btn-success">Clicked</button>
														<?php } ?>
													</td> 
												</tr>
										<?php 
										} }
										?>		
										</tbody>    
									</table>