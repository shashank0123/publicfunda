<table border="1">
										<thead>
											<tr>
												<th>S.No</th>
                                                <th>Date</th>
												<th>CTP Points</th>
                                                <th>Amount</th>
                                                <th>TDS</th>
                                                <th>Admin Charges</th>
                                                <th>Net Amount</th>
											</tr>
										</thead>
										<tbody>
										<?php if(count($payoutdata)>0){ ?>
											<?php $i=0; foreach($payoutdata as $paydata){ $i++;?>
											<tr>
												<td><?php echo $i; ?></td> 
                                                <td><?php echo date('d-m-Y',strtotime($paydata->redeemdate)); ?></td>
												<td><?php echo $paydata->points; ?></td>
												<td><i class="fa fa-rupee"></i> <?php echo $paydata->amount; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo ($paydata->amount*$paydata->tds)/100; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo ($paydata->amount*$paydata->admincharge)/100;?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $paydata->finalAmount; ?></td>
                                            </tr>
											<?php } ?>
                                        <?php } ?>
										</tbody>                                       	
                                            
									</table>