<a href="<?php echo base_url('mis/user_download_mis/'.$userdata[0]->id); ?>"><button>Download</button></a>
<h2>Personal Details</h2>
<table width="100%" border="1">
	<tr>
		<th>Name</th>
		<th>Email</th>
		<th>Mobile</th>
		<th>Nominee</th>
		<th>Gender</th>
		
				
	</tr>
	<tr>
		<td><?php echo $userdata[0]->name; ?></td>
		<td><?php echo $userdata[0]->email; ?></td>
		<td><?php echo $userdata[0]->contact_no; ?></td>
		<td><?php echo $userdata[0]->nominee; ?></td>
		<td><?php echo $userdata[0]->gender; ?></td>
		
			
	</tr>

	<tr>
	    <th>Address</th>
		<th>City</th>
		<th>State</th>
		<th>Pin Code</th>
		<th>Payout Mode</th>
		
		
								
	</tr>
	<tr>
	    <td><?php echo $userdata[0]->address; ?></td>
		<td><?php echo $userdata[0]->city; ?></td>
		<td><?php echo $userdata[0]->state; ?></td>
		<td><?php echo $userdata[0]->pincode; ?></td>
		<td><?php echo $userdata[0]->payoutmode; ?></td>
		
		
		
					
	</tr>

	<tr>
	   <th>Colleges</th>
		<th>School</th>
		<th>Mobile Models</th>
		<th>Highest Degree</th>			
		<th>Extra Skills</th>
		
		
	</tr>
	<tr>
	  <td><?php echo $userdata[0]->colleges; ?></td>
	    <td><?php echo $userdata[0]->schools; ?></td>
		<td><?php echo $userdata[0]->mobile_handset; ?></td>
		<td><?php echo $userdata[0]->highest_degree; ?></td>	
		<td><?php echo $userdata[0]->extra_skills; ?></td>
	</tr>	
	<tr>
	    <th>Favorite Celebrity</th>	
		<th>DOB</th>
		<th>Profession</th>
		<th>Industry</th>
		<th>Current Industry</th>		
	</tr>
	<tr>
	    <td><?php echo $userdata[0]->celebrity; ?></td>
		<td><?php echo $userdata[0]->dob; ?></td>
		<td><?php echo $userdata[0]->profession; ?></td>
		<td><?php echo $userdata[0]->industry; ?></td>
		<td><?php echo $userdata[0]->current_industry; ?></td>
						
	</tr>
	<tr>
		
		<th>Company Name</th>
		<th>Department</th>
		<th>Hobbies</th>
		<th>Education</th>
		<th>Salary</th>		
	</tr>
	<tr>
		<td><?php echo $userdata[0]->company_name; ?></td>
		<td><?php echo $userdata[0]->field_work; ?></td>
		<td><?php echo $userdata[0]->hobbies; ?></td>
		<td><?php echo $userdata[0]->education; ?></td>
		<td><?php echo $userdata[0]->salary; ?></td>				
	</tr>
	
	<tr>
		<th>Turn Over</th>		
		<th>Account Status</th>
		<th>Booster Status</th>
		<th>Course Types</th>
		<th>Total Job or Business Experience</th>		
	</tr>
	<tr>
		
		<td><?php echo $userdata[0]->turn_over; ?></td>
		<td><?php echo($userdata[0]->status==1)?'Active':'Inactive'; ?></td>
		<td>Inactive</td>
		<td><?php echo $userdata[0]->course_type; ?></td>
		<td><?php echo $userdata[0]->total_job; ?></td>				
	</tr>
	<tr>		
		<th>Current Job Or Business Experience</th>
		<th>Current Designation</th>
		<th>Pancard No.</th>
		<th>Aadhar Card No.</th>		
		<th>Voter ID Card</th>
	</tr>
	<tr>		
		<td><?php echo $userdata[0]->current_job; ?></td>
		<td><?php echo $userdata[0]->current_designation; ?></td>		
		<td><?php echo $userdata[0]->pancard_no; ?></td>
		<td><?php echo $userdata[0]->adhaar_no; ?></td>
		<td><?php echo $userdata[0]->voterid_no; ?></td>
	</tr>
	<tr>
		<th>Passport No.</th>
	</tr>
	<tr>
		<td><?php echo $userdata[0]->passport_no; ?></td>		
	</tr>
</table>
<?php $planData = $this->plan_model->selectPlanByID($userdata[0]->plan) ?>
<h2>Package Details</h2>
<table width="100%" border="1">
	<tr>		
		<th>Package Name</th>
		<th>Amount</th>
		<th>Validity</th>
		<th>E-points</th>	
		<th>Link Per Day</th>
		<th>Earning Per Click</th>
		<th>Turnover</th>
		<th>Capping</th>
	</tr>
	<tr>		
		<td><?php echo $planData[0]->planName; ?></td>
		<td><?php echo $planData[0]->planFees; ?></td>
		<td><?php echo $planData[0]->validity; ?></td>
		<td><?php echo $planData[0]->totalVisitors; ?></td>
		<td><?php echo $planData[0]->linkPerDay; ?></td>
		<td><?php echo $planData[0]->earningPerClick; ?></td>	
		<td><?php echo $planData[0]->turnover; ?></td>
		<td><?php echo $planData[0]->capping*3; ?></td>
	</tr>
</table>
<h2>Invoice Details</h2>
<table width="100%" border="1">
	<tr>		
		<th>SNo.</th>
		<th>Invoice ID</th>	
		<th>Transaction ID</th>	
		<th>Total Amount</th>		
	</tr>
	<?php 
	$invData = $this->tree_model->selectUserInvoice($userdata[0]->id); ?>
	<?php 
		if(count($invData)>0){ $r=0;
		foreach($invData as $inv){	 $r++;
	?>
	<tr>		
		<td><?php echo $r; ?></td>
		<td><?php echo $inv->invoiceID; ?></td>
		<td><?php echo $inv->transactionID; ?></td>
		<td><?php echo $inv->finalAmount; ?></td>	
	</tr>
	<?php } } ?>
</table>

<h2>Bank Details</h2>
<table width="100%" border="1">
	<tr>		
		<th>Account Holder</th>
		<th>Bank Name</th>
		<th>Branch Name</th>
		<th>Account No</th>	
		<th>IFSC</th>		
	</tr>
	<tr>		
		<td><?php echo $userdata[0]->account_holder; ?></td>
		<td><?php echo $userdata[0]->bank_name; ?></td>
		<td><?php echo $userdata[0]->branch_name; ?></td>
		<td><?php echo $userdata[0]->bank_accountno; ?></td>
		<td><?php echo $userdata[0]->bank_ifsccode; ?></td>	
	</tr>
</table>
<h2>Campaign Information</h2>
<?php
	$pending_points = 0; 
	foreach($this->campaign_model->selectCampaignByUserID($userdata[0]->id) as $campData)
	{
		$pending_points +=$campData->campaignPoints; 
	}
	$left_points = ($userdata[0]->totalPoints+$userdata[0]->paidPoints)-$pending_points;
?>
<table width="100%" border="1">
	<tr>		
		<th>Campaign Status</th>
		<th>Points</th>
		<th>Purchase</th> 
		<th>Pending Points</th>
		<th>Used Points</th>
		<th>Consumed Points</th>	
		<th>Number of Active Campaign</th>
		<th>Number of Inactive Campaign</th> 		
	</tr>
	<?php 
	$consumed = 0;
	$used_points = 0;
	foreach($this->campaign_model->selectCampaignByUserID($userdata[0]->id) as $campaignData1)
	{
		$used_points += $campaignData1->campaignPoints;
		$consumed += $campaignData1->recievedclicked;
	}		
	?>
	<tr>		
		<td><?php echo($userdata[0]->campaign_status==1)?'Active':'Inactive'; ?></td>
		<td><?php echo $userdata[0]->totalPoints; ?></td>
		<td><?php echo $userdata[0]->paidPoints; ?></td>
		<td><?php echo $left_points; ?></td>
		<td><?php echo $used_points; ?></td>
		<td><?php echo $consumed; ?></td>
			
		<td><?php echo count($this->campaign_model->selectCampaignByUserIDActiveOrInactive($userdata[0]->id,1)); ?></td>
		<td><?php echo count($this->campaign_model->selectCampaignByUserIDActiveOrInactive($userdata[0]->id,0)); ?></td>		
	</tr>
	<tr>		
		<?php $r=0; foreach($this->campaign_model->selectCampaignByUserID($userdata[0]->id) as $campaignData){ $r++; ?>
		<th>Campaign <?php echo $r; ?></th>
		<?php } ?>
	</tr>
	<tr>
		<?php foreach($this->campaign_model->selectCampaignByUserID($userdata[0]->id) as $campaignData){ ?>
		<td><b>Status -</b> <?php echo($campaignData->campaignStatus==1)?'Active':'Inactive'; ?><br>
		    <b>ID -</b> <?php echo $campaignData->id; ?><br>
			<b>Used Points -</b> <?php echo $campaignData->campaignPoints; ?><br>
			<b>Consumed Points -</b> <?php echo $campaignData->recievedclicked; ?>
		</td>
		<?php } ?>
	</tr>
</table>

<h2>Wallet</h2>
<table width="100%" border="1">
	<tr>		
		<th>Wallet balance</th>
		<th>Total send tofriend</th>	
		<th>Total received from friend</th>
		<th>Total send to bank</th>	
		<th>Total redeem points</th>
		<th>Total redeem amount</th>
		<th>Total credit into wallet</th>
		<th>Total debit from wallet</th>
		<th>Total NEFT Transaction</th>	
	</tr>
	<tr>		
		<td><?php echo $userdata[0]->wallet; ?></td> 
		<td><?php echo $this->user_model->userWalletInfoForMis($userdata[0]->id,'send money to friend'); ?></td>
		<td><?php echo $this->user_model->userWalletInfoForMis($userdata[0]->id,'Receive from friend'); ?></td>
		<td><?php echo $this->user_model->userWalletInfoForMis($userdata[0]->id,'send money to bank'); ?></td>
		<td><?php echo $this->user_model->totalRedeempoints($userdata[0]->id,'points'); ?></td>
		<td><?php echo $this->user_model->totalRedeempoints($userdata[0]->id,'amount'); ?></td>
		<td><?php echo $this->user_model->userTotalCrDrAmount($userdata[0]->id,'cr'); ?></td> 
		<td><?php echo $this->user_model->userTotalCrDrAmount($userdata[0]->id,'dr'); ?></td> 
		<td><?php echo $this->user_model->getTotalNEFT($userdata[0]->id); ?></td>
	</tr>
</table>

<h2>Team Information</h2>
<table width="100%" border="1">
	<tr>		
		<th>Total active of left</th>
		<th>Total inactive of left</th>	
		<th>Total active of right</th>
		<th>Total inactive of right</th>		
		<th>Total right sale</th>
		<th>Total left sale</th>
		<th>Total Turnover</th>	
	</tr> <!--- getTreeInformation($id,$pos,$status)  --->
	<tr>		
		<td><?php echo count($this->user_model->getTreeInformation($userdata[0]->id,'L',1)); ?></td> 
		<td><?php echo count($this->user_model->getTreeInformation($userdata[0]->id,'L',0)); ?></td>
		<td><?php echo count($this->user_model->getTreeInformation($userdata[0]->id,'R',1)); ?></td>
		<td><?php echo count($this->user_model->getTreeInformation($userdata[0]->id,'L',0)); ?></td>
		<td><?php echo $this->booster_model->getBonusIncome($userdata[0]->id,'R'); ?></td>
		<td><?php echo $this->booster_model->getBonusIncome($userdata[0]->id,'L'); ?></td>
		<td><?php echo ($this->booster_model->getBonusIncome($userdata[0]->id,'R')+$this->booster_model->getBonusIncome($userdata[0]->id,'L')); ?></td>
	</tr>
</table>
