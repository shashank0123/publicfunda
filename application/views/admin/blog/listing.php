

<?php $this->load->view('admin/layout/header'); ?>
<div class="inner-wrapper pt-30W">
            <?php $this->load->view('admin/layout/left-menu'); ?>





                <!-- Page Heading -->

               <section role="main" class="content-body">

                <header class="page-header">
                        <h2 class="page-header">Blog Listing <a href="<?php echo base_url('index.php/blog/add'); ?>" class="btn btn-primary pull-right">Add New</a></h2>
                        </header>
                        <div class="clearfix"></div>
                         <?php echo $this->session->flashdata('message'); ?>

						 <div class="table-responsive">

                            <table class="table table-bordered table-hover">

                                <thead>

                                    <tr>

										<th width="5%">S.No.</th>

                                        <th>Title</th>
                                        <th>USER ID</th>
                                        <th>Spam</th>
                                        <th>Featured</th>
                                        <th>Highlight</th>
                                        <th>Other</th>
                                        <th>Date Added</th>

                                        <th width="8%">Status</th>

                                        <th width="20%">Action</th>

                                    </tr>

                                </thead>

                                <tbody>

								<?php $i=0; foreach($BLOGDATA as $data){ $i++; ?>

                                    <tr>

										<td><?php echo $i; ?></td>

                                        <td><?php echo $data->title; ?></td>										
                                        <td><?php echo $data->user_id; ?></td>                                        
                                        <td><?php echo $this->blog_model->count_inappropriate_status($data->id); ?></td>                                        
                                        <td><input onclick="featuredcheck(<?php echo $data->id?>)" type="checkbox" <?php if ($data->featured == 1) echo "checked";?> name="featured"></td>
                                        <td><input onclick="primarycheck(<?php echo $data->id?>)" type="checkbox" <?php if ($data->highlight_primary == 1) echo "checked";?> name="featured"></td>
                                        <td><input onclick="othercheck(<?php echo $data->id?>)" type="checkbox" <?php if ($data->highlist_other == 1) echo "checked";?> name="featured"></td>                                        
                                        <td><?php echo $data->create_date; ?></td>                                        

                                        <td><img src="<?php echo($data->status=='0')?base_url('assets/admin/image/inactive.png'):base_url('assets/admin/image/active.png'); ?>"> </td>

                                        <td>

											<a href="<?php echo base_url('blog/edit/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

											<a href="<?php echo base_url('blog/delete/'.$data->id); ?>" onclick="return deleteConfirm();" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i>delete</a>																	

										</td>

                                    </tr>

                                <?php } ?>                                       

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

<script src="/assets/blog/js/jquery.js"></script>
<script>

function checkpancard()

{

	if(document.getElementById("nopancard").checked==true)

	{

		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');

		document.getElementById("pancard").style.backgroundColor = "#ccc";

		document.getElementById("pancard").readOnly = true;

	}

	else

	{

		document.getElementById("pancard").style.backgroundColor = "#fff";

		document.getElementById("pancard").readOnly = false;

	}	

}

function featuredcheck(id){
     $.ajax({
        type: "POST",
        url: '/index.php/blog/listingchange',
        data: {'id':id}, //--> send id of checked checkbox on other page
        success: function(data) {            
            // alert(data);
            // $('#container').html(data);
        },
         error: function() {
        },
    });
}
function primarycheck(id){
     $.ajax({
        type: "POST",
        url: '/index.php/blog/primarychange',
        data: {'id':id}, //--> send id of checked checkbox on other page
        success: function(data) {            
            // alert(data);
            // $('#container').html(data);
        },
         error: function() {
        },
    });
}
function othercheck(id){
     $.ajax({
        type: "POST",
        url: '/index.php/blog/otherchange',
        data: {'id':id}, //--> send id of checked checkbox on other page
        success: function(data) {            
            // alert(data);
            // $('#container').html(data);
        },
         error: function() {
        },
    });
}



</script>

</body>



</html>

