<?php $this->load->view('admin/layout/header'); ?>

<?php 

if(isset($adminData[0]->module) && $adminData[0]->module!="")

{

    $module_array = explode(",",$adminData[0]->module);

}

else

{

    $module_array =array();

} 

 ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/style-crop.css" />

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/jquery.Jcrop.min.css" type="text/css" />

            <div class="inner-wrapper pt-100">

                <!-- start: sidebar -->

                <?php $this->load->view('admin/layout/left-menu'); ?>

                <!-- end: sidebar -->
        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h2 class="page-header">Blog Category <a href="<?php echo base_url('index.php/blogcategory/add_cat'); ?>" class="btn btn-primary pull-right">Add New</a></h2>

                         <?php echo $this->session->flashdata('message'); ?>

						 <div class="table-responsive">

                            <table class="table table-bordered table-hover">

                                <thead>

                                    <tr>

										<th width="5%">S.No.</th>

                                        <th>Title</th>

                                        <th width="8%">Status</th>

                                        <th width="20%">Action</th>

                                    </tr>

                                </thead>

                                <tbody>

								<?php $i=0; foreach($BLOGDATA as $data){ $i++; ?>

                                    <tr>

										<td><?php echo $i; ?></td>

                                        <td><?php echo $data->title; ?></td>										

                                        <td><img src="<?php echo($data->status=='0')?base_url('assets/admin/image/inactive.png'):base_url('assets/admin/image/active.png'); ?>"> </td>

                                        <td>

											<a href="<?php echo base_url('blogcategory/edit_cat/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

											<a href="<?php echo base_url('blogcategory/delete_cat/'.$data->id); ?>" onclick="return deleteConfirm();" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i>delete</a>																	

										</td>

                                    </tr>

                                <?php } ?>                                       

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>



<script>

function checkpancard()

{

	if(document.getElementById("nopancard").checked==true)

	{

		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');

		document.getElementById("pancard").style.backgroundColor = "#ccc";

		document.getElementById("pancard").readOnly = true;

	}

	else

	{

		document.getElementById("pancard").style.backgroundColor = "#fff";

		document.getElementById("pancard").readOnly = false;

	}	

}



</script>

</body>



</html>

