

<?php $this->load->view('admin/layout/header'); ?>
<script type="text/javascript" src="<?php echo base_url('assets/admin'); ?>/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        skin : "o2k7",
        forced_root_block : "",
        plugins : "openmanager,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,openmanager",
        //theme_advanced_buttons4 : "openmanager",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,     
        //Open Manager Options
        file_browser_callback: "openmanager",
        open_manager_upload_path: '../../../../uploads/',
        // Example content CSS (should be your site CSS)
        content_css : "css/content.css",
        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Style formats
        style_formats : [
            {title : 'Bold text', inline : 'b'},
            {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
            {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
            {title : 'Example 1', inline : 'span', classes : 'example1'},
            {title : 'Example 2', inline : 'span', classes : 'example2'},
            {title : 'Table styles'},
            {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
        ],

        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
    });
</script>

<div class="inner-wrapper pt-100">
            <?php $this->load->view('admin/layout/left-menu'); ?>



</div>
</div>
        

                <!-- Page Heading -->

                <section role="main" class="content-body">

                    <header class="page-header">

                        <h2 class="page-header">Edit Blog</h2>

                         <form role="form" method="post" enctype="multipart/form-data">

							<div class="form-group">

                                <label> Select Category</label>

                                <select class="form-control" name="cat_id" required>

									<?php foreach($CATEGORY as  $cat){ ?>

										<option value="<?php echo $cat->id; ?>" <?php echo($EDITBLOG[0]->cat_id==$cat->id)?'selected':''; ?>><?php echo $cat->title; ?></option>

									<?php } ?>

								</select>

                            </div>

                            <div class="form-group">

                                <label>Title</label>

                                <input type="text" class="form-control" placeholder="category Title" value="<?php echo $EDITBLOG[0]->title; ?>" name="title">

                            </div>

							<div class="form-group">

								<?php if(!empty($EDITBLOG[0]->image))	{	?>

                                <img src="<?php echo base_url('uploads/blog/'.$EDITBLOG[0]->image); ?>" width="80">

								<?php }	?>

								<input type="hidden" name="oldimage" value="<?php echo $EDITBLOG[0]->image; ?>">								

                            </div>

							<div class="form-group">

                                <label>Image</label>

                                <input type="file" class="form-control"  name="image">

                            </div>

							<div class="form-group">

                                <label> Content</label>

                                <textarea class="form-control" name="content"><?php echo $EDITBLOG[0]->content; ?></textarea>

                            </div>

							<div class="form-group">

                                <label>Status</label>

                                <select name="status" class="form-control">

                                    <option <?php echo($EDITBLOG[0]->status==1)?'selected':''; ?> value="1">Active</option>

                                    <option <?php echo($EDITBLOG[0]->status==0)?'selected':''; ?> value="0">Inactive</option>

                                </select>

                            </div>

                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>

                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>

                    </div>

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    <!-- /#wrapper -->

</body>



</html>

