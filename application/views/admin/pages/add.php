 <?php 

 $admin_id = $this->session->userdata('ADMINID'); 

 if(empty($admin_id))

 {

 	redirect('index.php/admin');

 }	
 $this->load->view('admin/layout/header'); ?>

<style>

.material-icons{

	font-size: 14px;

}

</style>
<script type="text/javascript" src="<?php echo base_url('assets/admin'); ?>/tiny_mce/tiny_mce.js"></script>
	<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
		forced_root_block : "",
		plugins : "openmanager,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,openmanager",
		//theme_advanced_buttons4 : "openmanager",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,		
		//Open Manager Options
		file_browser_callback: "openmanager",
		open_manager_upload_path: '../../../../uploads/',
		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",
		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Style formats
		style_formats : [
			{title : 'Bold text', inline : 'b'},
			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
			{title : 'Example 1', inline : 'span', classes : 'example1'},
			{title : 'Example 2', inline : 'span', classes : 'example2'},
			{title : 'Table styles'},
			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
		],

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>

			<div class="inner-wrapper pt-30">

				<!-- start: sidebar -->

				<?php $this->load->view('admin/layout/left-menu'); ?>

				<!-- end: sidebar -->

				<section role="main" class="content-body">

 				<header class="page-header">
                        <h1 class="page-header">Add New Pages</h1>
</header>
						<div class="col-md-12 col-lg-12 col-xl-12">

						    <div class="panel panel-sign">

					

					<div class="panel-body">



				<form  enctype="multipart/form-data" method="POST">



							<table class="table table-bordered table-striped table-condensed" style="">										

								<tbody>

								   
<tr>

										<td> Title</td>

										<td>

		<input type="text" name="title" onkeyup="makeurl(this);" class="form-control" autocomplete="off" required>

										</td>

									</tr>

									

									<tr>

										<td> Url</td>

										<td>

		<span><?php echo base_url();?></span>
		<input type="text" name="url" id="urltextbox" class="form-control" autocomplete="off" required>

										</td>

									</tr>

									<tr>

										<td>Page Content</td>

										<td>
											<textarea name="content" id="pagecontent" class="form-control" autocomplete="off" required > </textarea>

										</td>

									</tr>


																		<tr>

										<td>Featured Image</td>

										<td>

		<input type="file" name="fimage"  class="form-control">

										</td>

									</tr>

									

								<!--	<tr>

										<td> Meta keyword</td>

										<td>

		<input type="text" name="keyword"  class="form-control" autocomplete="off" >

										</td>

									</tr>
									
									
									
									</tr>
									
									        <td> Meta Description</td>

										<td>

			<input type="text" name="description"  class="form-control" autocomplete="off" >

										</td>
										
										
													        
										
									</tr>
									
									
									
									</tr>
									
									        <td> Meta Author</td>

										<td>

			<input type="text" name="author"  class="form-control" autocomplete="off" >

										</td>
										
										
									</tr>
									
									
									
									</tr>
									
									        <td> Meta Charset</td>

										<td>

			<input type="text" name="charset"  class="form-control" autocomplete="off" >

										</td>
										
										
																			
										
										
									</tr>-->

																						

									<tr>

			<td colspan="2"><button type="submit" class="btn btn-info" name="addpage">Submit</button></td>

									</tr>

								</tbody>

							</table>



						</form>

				</div>

				</div>

						</div>

					<!-- end: page -->

			</div>



 </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

	 </div>

	 

    <!-- /#wrapper -->

<?php //$this->load->view('admin/layout/footer-js'); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
     <script>

     	function makeurl(data){
     		var title = $(data).val().replace(/ /g,'-');
     		$("#urltextbox").val(title);
     	}
                CKEDITOR.replace( 'pagecontent' );
            </script>

<script>/*

$(document).ready(function(){

    $("#btn1").click(function(){

    	var i=0;

    	i++;

$("p").append("<span id="rmdiv'+ i +'"><input type='file' name='image[]' multiple><input type='text' name='image_title[]' multiple><button onclick="return removefile('+ i +')" type="button" class="btn btn-xs btn-danger imgremove"> <b>X</b></button>");

     

        //$("p1").append("<input type='text' name='image_title[]' multiple>");

        //alert("sssssssssss");

    });



  

});*/

</script>

<script>

	var i = 0;

function addimage()

{

	i++;

	var fileHtml = '<span id="rmdiv'+ i +'"><div class="col-md-12" style="padding: 9px;"><div class="col-md-4"><input type="file" class="form-control" name="image[]" id="image" required></div><div class="col-md-7"> <input type="text" class="form-control" name="image_title[]" id="image_title" placeholder="enter image title" required></div><div class="col-md-1"><button onclick="return removefile('+ i +')" type="button" class="btn btn-xs btn-danger imgremove"> <b>X</b></button></div></div></span>';

	$('#appendimage').append(fileHtml);

}

function removefile(ID)

{

	$('#rmdiv'+ ID).remove();

}

function makeurl(data){
     		var title = $(data).val().replace(/ /g,'-');
     		$("#urltextbox").val(title);
     	}

</script>

</body>



</html>