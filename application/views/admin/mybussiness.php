<?php $this->load->view('front/layout/header-inner'); ?>
<style>
.myclass{margin:0px 5px 10px 0px;}

.css-serial {
  counter-reset: serial-number;  /* Set the serial number counter to 0 */
}

.css-serial td:first-child:before {
  counter-increment: serial-number;  /* Increment the serial number counter */
  content: counter(serial-number);  /* Display the counter */
}
</style>
<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/select2/select2.css" />		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/css/datatables.css" />
	<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>My Bussiness</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>My Bussiness</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12">
							<section class="panel">
									<header class="panel-heading">	
											<h2 class="panel-title">My Bussiness
													<button class="pull-right btn btn-info "><strong>Total Turnover    </strong> Left : <i class="fa fa-rupee"></i><?php echo $this->booster_model->getBonusIncome($login_user[0]->id,'L'); ?> | Right : <i class="fa fa-rupee"></i><?php echo $this->booster_model->getBonusIncome($login_user[0]->id,'R'); ?> </button>									
											</h2> 
									</header>
									
									<div class="panel-body" >
									<form class="form-horizontal" method="post">
											<h4 class="mb-xlg"></h4>
											<fieldset>
												<div class="form-group">
													<div class="col-md-5">
														<div class="input-daterange input-group" data-plugin-datepicker="" >
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="start" required value="<?php echo(isset($_POST['start']))?$_POST['start']:''; ?>">
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="end" required value="<?php echo(isset($_POST['end']))?$_POST['end']:''; ?>">                                                        
														</div>
													</div>                                                   
													<div class="col-md-3">
														<select class="form-control mb-md" name="status" required>
															<option value="">Select Status</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='all')?'selected':''; ?> value="all">All</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='1')?'selected':''; ?> value="1">Active</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='0')?'selected':''; ?> value="0">Inactive</option>
														</select>                                                        
													</div>
													
                                                    <div class="col-md-2">
														<select class="form-control mb-md" name="position" required>
															<option value="">Select Position</option>															
															<option <?php echo(isset($_POST['position']) && $_POST['position']=='L')?'selected':''; ?> value="L">Left</option>
															<option <?php echo(isset($_POST['position']) && $_POST['position']=='R')?'selected':''; ?> value="R">Right</option>
															<option <?php echo(isset($_POST['position']) && $_POST['position']=='all')?'selected':''; ?> value="all">Both</option>
														</select>                                                        
													</div>
                                                    
                                                    <div class="col-md-2">
														<button type="submit" name="searchMyTeam" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
														<button type="button" onclick="window.location.reload()" class="btn btn-warning"><i class="fa  fa-repeat"></i> Reset </button>   
													</div>
												</div> 
                                                
											</fieldset>
											
											
											
											
											

										</form>
                                        
                                        <div class="table-responsive">
									<table class="table table-bordered table-striped css-serial" id="datatable-default">
										<thead>
											<tr>
												<th>S.No</th>
                                                <th>User ID</th>
                                                <th>Name</th>
												<th>Plan Name</th>
												<th>Invoice ID</th>
												<th>Transaction ID</th>
                                            	<th>Amount</th>
                                                <th>Status</th>
                                                <th>Position</th>
											</tr>
										</thead>
										<tbody>	
											<?php 
												if(isset($_POST['searchMyTeam']))
												{
													$sdate = date('Y-m-d',strtotime($this->input->post('start')));
													$esdate = date('Y-m-d',strtotime($this->input->post('end')));
													$status = $this->input->post('status');
													$postion = $this->input->post('position');
													if($postion=='all')
													{
														$left_ids  = $this->team_model->getTreeLeftOrRight($login_user[0]->id,'L',$status,$sdate,$esdate);
														$right_ids  = $this->team_model->getTreeLeftOrRight($login_user[0]->id,'R',$status,$sdate,$esdate);
											?>
													<?php if(count($left_ids)>0){ ?>
													<?php foreach($left_ids as $luser){ ?>
														<?php $user = $this->user_model->selectUserByID($luser['ids']); ?>
														<?php $userInvData = $this->tree_model->selectUserInvoiceByFilter($user[0]->id); ?>
														<?php foreach($userInvData as $userInv){ ?>
														<tr>
															<td><?php //echo $k; ?></td>
															<td><?php echo $user[0]->id; ?></td>
															<td><?php echo $user[0]->name; ?></td>
															<td><?php echo(isset($userInv->planName))?$userInv->planName:'-';  ?></td>
															<td><?php echo(isset($userInv->invoiceID))?$userInv->invoiceID:'-'; ?></td>
															<td><?php echo(isset($userInv->transactionID))?$userInv->transactionID:'-'; ?></td>
															<td><?php echo(isset($userInv->amount))?$userInv->amount:'-'; ?></td>
															<td><?php echo($user[0]->status==1)?'Active':'Inactive'; ?></td>
															<td>Left</td>
														</tr>
													<?php }}} ?>
													<?php if(count($right_ids)>0){ ?>
													<?php foreach($right_ids as $ruser){ ?>
														<?php $user = $this->user_model->selectUserByID($ruser['ids']); ?>
														<?php $userInvData = $this->tree_model->selectUserInvoiceByFilter($user[0]->id); ?>
														<?php foreach($userInvData as $userInv){ ?>
														<tr>
															<td><?php //echo $k; ?></td>
															<td><?php echo $user[0]->id; ?></td>
															<td><?php echo $user[0]->name; ?></td>
															<td><?php echo(isset($userInv->planName))?$userInv->planName:'-';  ?></td>
															<td><?php echo(isset($userInv->invoiceID))?$userInv->invoiceID:'-'; ?></td>
															<td><?php echo(isset($userInv->transactionID))?$userInv->transactionID:'-'; ?></td>
															<td><?php echo(isset($userInv->amount))?$userInv->amount:'-'; ?></td>
															<td><?php echo($user[0]->status==1)?'Active':'Inactive'; ?></td>
															<td>Right</td>
														</tr>
													<?php }}}  ?>
												<?php
													} 	
													if($postion=='L') // close left position
													{
														$left_ids  = $this->team_model->getTreeLeftOrRight($login_user[0]->id,'L',$status,$sdate,$esdate);
												?>
													<?php if(count($left_ids)>0){ ?>
													<?php foreach($left_ids as $luser){ ?>
														<?php $user = $this->user_model->selectUserByID($luser['ids']); ?>
														<?php $userInvData = $this->tree_model->selectUserInvoiceByFilter($user[0]->id); ?>
														<?php foreach($userInvData as $userInv){ ?>
														<tr>
															<td><?php //echo $k; ?></td>
															<td><?php echo $user[0]->id; ?></td>
															<td><?php echo $user[0]->name; ?></td>
															<td><?php echo(isset($userInv->planName))?$userInv->planName:'-';  ?></td>
															<td><?php echo(isset($userInv->invoiceID))?$userInv->invoiceID:'-'; ?></td>
															<td><?php echo(isset($userInv->transactionID))?$userInv->transactionID:'-'; ?></td>
															<td><?php echo(isset($userInv->amount))?$userInv->amount:'-'; ?></td>
															<td><?php echo($user[0]->status==1)?'Active':'Inactive'; ?></td>
															<td>Left</td>
														</tr>
													<?php }}}  ?>													
												<?php
													} // close left position
													if($postion=='R') // close right position
													{
														$right_ids  = $this->team_model->getTreeLeftOrRight($login_user[0]->id,'R',$status,$sdate,$esdate);
												?>
													<?php if(count($right_ids)>0){ ?>
													<?php foreach($right_ids as $ruser){ ?>
														<?php $user = $this->user_model->selectUserByID($ruser['ids']); ?>
														<?php $userInvData = $this->tree_model->selectUserInvoiceByFilter($user[0]->id); ?>
														<?php foreach($userInvData as $userInv){ ?>
														<tr>
															<td><?php //echo $k; ?></td>
															<td><?php echo $user[0]->id; ?></td>
															<td><?php echo $user[0]->name; ?></td>
															<td><?php echo(isset($userInv->planName))?$userInv->planName:'-';  ?></td>
															<td><?php echo(isset($userInv->invoiceID))?$userInv->invoiceID:'-'; ?></td>
															<td><?php echo(isset($userInv->transactionID))?$userInv->transactionID:'-'; ?></td>
															<td><?php echo(isset($userInv->amount))?$userInv->amount:'-'; ?></td>
															<td><?php echo($user[0]->status==1)?'Active':'Inactive'; ?></td>
															<td>Righ</td>
														</tr>
													<?php }}}  ?>													
												<?php
													} // close right position
												}
												else
												{
													
												}
											?>											
										</tbody>
                                        	
                                            
									</table>
                                    
								</div>
								</div>
								</section>                         
						</div>
						

					</div>
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
					<!-- end: page -->
				</section>
			</div>
			
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		
		<script src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
				
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.tabletools.js"></script>
<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	</body>


</html>