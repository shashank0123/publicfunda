  <?php $this->load->view('admin/layout/header'); ?>

			<div class="inner-wrapper pt-100">

				<!-- start: sidebar -->

				<?php $this->load->view('admin/layout/left-menu'); ?>

				<!-- end: sidebar -->

				<section role="main" class="content-body">

					<header class="page-header">

						<h2> Edit Invoice</h2>					

						<div class="right-wrapper pull-right">

							<ol class="breadcrumbs">

								<li>

									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">

										<i class="fa fa-home"></i>

									</a>

								</li>								

								<li><a href=""><span>Manage Invoice</span></a></li>

							</ol>					

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>

						</div>

					</header>



					<!-- start: page -->



					<div class="row">						

						<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>                           

							<section class="panel">

							

							<div class="panel-body">								

								<div class="table-responsive">									

										<table class="table" style="">										

											<tbody>

												<form method="post" enctype="multipart/form-data" >

												<tr>

													<td>USERID</td>

													<td><input type="text" required id="uid" name="user_id" class="form-control" value="">

													    <button type="button" onclick="return logintouser();">Login</button>

													</td>

												</tr>	

												<tr>

													<td>Invoice Description</td>

													<td><textarea name="planName" class="form-control"></textarea></td>

												</tr>

												<tr>

													<td>Invoice ID</td>

													<td><input type="text" readonly name="invoiceID" class="form-control" value="<?php echo 'MOTIF/'.date('Y').'/'.time(); ?>"></td>

												</tr>

												<tr>

													<td>Transaction ID</td>

													<td><input type="text" name="transactionID" class="form-control" value=""></td>

												</tr>

												<tr>

													<td>Net Amount</td>

													<td><input type="text" name="amount" id="amount" class="form-control" onkeyup="return calculteFinalPrice('amount');" autocomplete="off" value=""></td>

												</tr>
												<tr>

													<td>Tax(%)</td>

													<td><input  type="text" name="tax" id="tax" class="form-control" value="" autocomplete="off"></td>

												</tr>

												

												<tr>

													<td>Total Amount</td>

													<td><input type="text" name="finalAmount" onkeyup="return calculteFinalPrice('final');" id="finalAmount" class="form-control" value="" autocomplete="off"></td>

												</tr>

												

												<tr>

													<td>Payment Status</td>

													<td>

														<select name="status" class="form-control">

															<option value='pending'>Pending</option>

															<option value='paid'>Paid</option>

														</select>

													</td>

												</tr>

												<tr>

													<td>Edit Profile Fields Lock/Unlock</td>

													<td>

														<select class="form-control" name="profilelock">

														    <option selected value="none">None</option>

															<option value="1">Unblock</option>

															<option value="0">Block</option>     

														</select>

													</td>

												</tr>

												<tr>

													<td>Bank Details</td>

													<td>

														<select class="form-control" name="bank_details">

														    <option selected value="none">None</option>       

															<option value="1">Approved</option>

															<option value="0">Disapproved</option>     

														</select>

													</td>

												</tr>

												<tr>

													<td>Promotional Income</td>

													<td>

														<select class="form-control" name="promotional_income">

														    <option selected value="none">None</option>

															<option value="1" >Visible</option>

															<option value="0">Hide</option>

														</select>

													</td>

												</tr>

												<tr>

													<td>KYC</td>

													<td>

														<select class="form-control" name="kyc_status">

														    <option selected value="none">None</option>

															<option value="1">Approved</option>

															<option value="0">Unapproved</option>

														</select>

													</td>

												</tr>

												

												<tr>

													<td>Lock Campaign Fields</td>

													<td>

														<select class="form-control" name="lock_field">

														    <option selected value="none">None</option>

															<option value="1">Unlock</option>

															<option value="0">Lock</option>

														</select>

													</td>

												</tr>

												<tr>

													<td>CTP Points</td>

													<td><input type="text" class="form-control" name="totalPoints" value="0"></td>

												</tr>

												<tr>

													<td colspan="2"><button type="submit" class="btn btn-info" name="updateData">Submit Data</button></td>

												</tr>

												</form>

											</tbody>

										</table>					

								</div>

							</div>

						</section>

                         

						</div>

						



					</div>

					

					<!-- end: page -->

				</section>

			</div>

		</section>

		<!-- Vendor --><?php //echo base_url('index.php/admin/user_ligin/'.$USER->id); ?>

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		

		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>

		<script>

		    function logintouser()

			{

			   var loginid = document.getElementById('uid').value;

			   if(loginid!="")

			   {

			     window.open('<?php echo base_url('index.php/admin/user_ligin/'); ?>/'+loginid, '_blank');

			   } 	 

			}

			

			function calculteFinalPrice(type)

			{

				var tax_field = document.getElementById('tax');
				var amount_field = document.getElementById('amount')

				var final_amount = document.getElementById('finalAmount');

				
						

					tax = tax_field.value ;
					amount = amount_field.value;
					final = final_amount.value;
				if (type == 'amount'){
					final_amount.value = parseInt(amount)+((tax*amount)/100);

				}
				// if (type == 'tax'){
				// 	tax_field.value = (parseInt(final)-parseInt(amount))/parseInt(amount)*100;

				// }
				if (type == 'final'){
					amount_field.value = (parseInt(final)*100)/(100+parseInt(tax));

				}


				

				

			}

		</script>		

	</body>



<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->

</html>