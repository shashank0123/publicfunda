 <?php $this->load->view('admin/layout/header'); ?>
<style>
.material-icons{
	font-size: 14px;
}
</style>
			<div class="inner-wrapper pt-30">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Campaign Status</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php //echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Campaign Status</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">	
						<div class="col-md-12"> <br>
							<?php echo $this->session->flashdata('message'); ?> 							
							<section class="panel">								
							<div class="panel-body">
								<form method="post">
									<div class="col-md-5">
										<div class="input-daterange input-group" data-plugin-datepicker="">
												<span class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</span>
												<input type="text" class="form-control" name="startDate" value="<?php echo(isset($_POST['startDate']))?$_POST['startDate']:''; ?>" id="sd">
												<span class="input-group-addon">to</span>
												<input type="text" class="form-control" name="endDate" value="<?php echo(isset($_POST['endDate']))?$_POST['endDate']:''; ?>" id="ed">
										</div>
									</div>	
									<div class="col-md-3">
										
										<select name="status" class="form-control">
											<option value='1' <?php echo(isset($_POST['status']) && $_POST['status']==1)?'selected':''; ?>>Clicked</option>
											<option value='0' <?php echo(isset($_POST['status']) && $_POST['status']==0)?'selected':''; ?>>Pending</option>
										</select>
										
									</div>
									<div class="col-md-3">
										<button name="download" type="button" class="btn btn-warning pull-right">Download</button>
										<button name="search" type="submit" class="btn btn-info pull-right">Search</button>										
									</div>
								</form>
								<br><br><br>
								<div class="row">                             
								<table class="table table-bordered table-striped" id="datatable-default">										
									<thead>
								<tr class="gradeX">
										<th>Date</th>
										<th>Views Status</th>
										<th>Created By(USERID)</th>
										<th>Campaign Title</th>
										<th>User ID</th>
										<th>Campaign ID</th>
										<th>System IP</th>
										<th>Server IP</th>
										<th>Location</th>
										<th>Time</th>											
									</tr>
									</thead>
                                <tbody>
											<?php foreach($cstatus as $status){ ?>
											<?php $campaignxx = $this->campaign_model->selectCampaignByID($status->link_id); ?>
											<tr>
												<td><?php echo $status->distributionDate; ?></td>
												<td>
													<?php if($status->status==1){ ?>
														<button type="button" class="btn btn-danger btn-xs"><b>Pending</b></button>
													<?php }else{ ?>
														<button type="button" class="btn btn-success btn-xs"><b>Clicked</b></button>
													<?php } ?>
												</td>
												<td><?php echo $campaignxx[0]->user_id; ?></td>
												<td><?php echo $campaignxx[0]->campaignTitle; ?></td>
												<td><?php echo $status->user_id; ?></td>	
                                                <td><?php echo $status->link_id; ?></td>
												<td><?php  ?></td> 
												<td><?php echo $status->clickedIP; ?></td> 
												<td><?php //echo $status->clickedIP; ?></td>
                                                <td><?php if($status->clickDate!=""){ echo date('d-m-Y h:i:s a',$status->clickDate); } ?> </td>
                                            </tr>
											<?php } ?>
										</tbody>
						</table>
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>
	


		
<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		
		<script src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
				
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.tabletools.js"></script>
		
	</body>

</html>