 <?php $this->load->view('admin/layout/header'); ?>
 <style>
#sliderimg{ width: 25%; display: inline; }
.imgremove{ padding: 6px 14px; margin-right: 13px; }
</style>
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2> Edit Slider</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Slider</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>                           
							<section class="panel">
							
							<div class="panel-body">								
								<div class="table-responsive">
									
										 <form role="form" method="post" enctype="multipart/form-data" id="myform">
							<div class="form-group">
                                <label>Select Slider Type</label>
                                <select class="form-control" name="type" onchange="return checktype(this.value);">
									<option <?php echo($EDITBANNER[0]->type=='youtube')?'selected':''; ?> value="youtube">Youtube Video</option>
									<option <?php echo($EDITBANNER[0]->type=='flash')?'selected':''; ?> value="flash">Content </option>
									<option <?php echo($EDITBANNER[0]->type=='slider')?'selected':''; ?> value="slider">Slider</option>
								</select>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" placeholder="Banner Title" value="<?php echo $EDITBANNER[0]->title; ?>" name="title" disabled>
                            </div>
							
							<div class="form-group" id="youtubeblock" <?php echo($EDITBANNER[0]->type!='youtube')?'style="display:none"':''; ?>>
                                <label>Youtube Link</label>
								<input type="text" name="youtube" class="form-control" value="<?php echo $EDITBANNER[0]->youtube; ?>">
                            </div>
							
							<div class="form-group" id="flashblock" <?php echo($EDITBANNER[0]->type!='flash')?'style="display:none"':''; ?>>
                                <label>Content</label>
                                <textarea name="content" class="form-control"><?php echo $EDITBANNER[0]->flash; ?>	</textarea>
                            </div>
							
							<div class="form-group" id="sliderblock" <?php echo($EDITBANNER[0]->type!='slider')?'style="display:none"':''; ?>>
                                <label>Slider Image  <button type="button" class="btn btn-xs btn-success" onclick="return addslider();"> <b>+</b> </button></label>
                                <div id="appendslider"></div>
								<?php $sliderimages = $this->banner_model->selectSliderBanner($EDITBANNER[0]->id); ?>
								<?php if(count($sliderimages)>0){ ?>
								<?php echo $this->session->flashdata('message'); ?>
								<table class="table table-bordered table-hover">
									<tr>
										<th>SNo.</th>
										<th>Image</th>
										<th>Action</th>
									</tr>
									<?php $j=0; foreach($sliderimages as $data){ $j++; ?>
									<tr>
										<th><?php echo $j; ?></th>
										<th><img src="<?php echo base_url('uploads/banner/'.$data->image); ?>" width="50px" ></th>
										<th><a href="<?php echo base_url('index.php/banner/deleteSlider/'.$data->id.'/'.$EDITBANNER[0]->id); ?>">Delete</a></th>
									</tr>
									<?php } ?>
								</table>
								<?php } ?>
                            </div>
                            
                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                            
                        </form>
									
					
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>

		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>		
<script>
var i = 0;
function addslider()
{
	i++;
	var fileHtml = '<span id="rmdiv'+ i +'"><input type="file" class="form-control" name="sliderimg[]" id="sliderimg" required> <button onclick="return removefile('+ i +')" type="button" class="btn btn-xs btn-danger imgremove"> <b>X</b></button></span>';
	$('#appendslider').append(fileHtml);
}


function removefile(ID)
{
	$('#rmdiv'+ ID).remove();
}
function checktype(VALUEDATA)
{	
	if(VALUEDATA=='youtube')
	{		
		$('#flashblock').hide();
		$('#sliderblock').hide();
		$('#youtubeblock').show();
	}
	if(VALUEDATA=='flash')
	{		
		$('#youtubeblock').hide();
		$('#sliderblock').hide();
		$('#flashblock').show();
	}
	if(VALUEDATA=='slider')
	{		
		$('#flashblock').hide();
		$('#youtubeblock').hide();
		$('#sliderblock').show();
	}
	//$('#myform').trigger("reset");
}
</script>
		
		
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>