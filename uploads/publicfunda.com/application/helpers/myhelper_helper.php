<?php
function createUniqueFile($file)
{
	$data = time().'_'.strtolower($file);
	return $data;
}

function calculateProfileComplate($userdata)
{
	$field = 0;
	$field += ($userdata[0]->name!="")?2.380:'0';
	$field += ($userdata[0]->image!="")?2.380:'0';
	$field += ($userdata[0]->email!="")?2.380:'0';
	$field += ($userdata[0]->gender!="")?2.380:'0';
	$field += ($userdata[0]->dob!="")?2.380:'0';
	$field += ($userdata[0]->contact_no!="")?2.380:'0';
	$field += ($userdata[0]->address!="")?2.380:'0';
	$field += ($userdata[0]->pincode!="")?2.380:'0';
	$field += ($userdata[0]->city!="")?2.380:'0';
	$field += ($userdata[0]->state!="")?2.380:'0';
	$field += ($userdata[0]->pancard_no!="")?2.380:'0';
	$field += ($userdata[0]->adhaar_no!="")?2.380:'0';
	$field += ($userdata[0]->colleges!="")?2.380:'0';
	$field += ($userdata[0]->university!="")?2.380:'0';
	$field += ($userdata[0]->schools!="")?2.380:'0';
	$field += ($userdata[0]->mobile_handset!="")?2.380:'0';
	$field += ($userdata[0]->highest_degree!="")?2.380:'0';
	$field += ($userdata[0]->extra_skills!="")?2.380:'0';
	$field += ($userdata[0]->celebrity!="")?2.380:'0';
	$field += ($userdata[0]->profession!="")?2.380:'0';
	$field += ($userdata[0]->industry!="")?2.380:'0';
	$field += ($userdata[0]->current_industry!="")?2.380:'0';
	$field += ($userdata[0]->company_name!="")?2.380:'0';
	$field += ($userdata[0]->current_company!="")?2.380:'0';
	$field += ($userdata[0]->field_work!="")?2.380:'0';
	$field += ($userdata[0]->flight_travel!="")?2.380:'0';
	$field += ($userdata[0]->hobbies!="")?2.380:'0';
	$field += ($userdata[0]->education!="")?2.380:'0';
	$field += ($userdata[0]->course_type!="")?2.380:'0';
	$field += ($userdata[0]->salary!="")?2.380:'0';
	$field += ($userdata[0]->turn_over!="")?2.380:'0';
	$field += ($userdata[0]->total_job!="")?2.380:'0';
	$field += ($userdata[0]->current_job!="")?2.380:'0';
	$field += ($userdata[0]->current_designation!="")?2.380:'0';
	$field += ($userdata[0]->nominee!="")?2.380:'0';
	$field += ($userdata[0]->sponsor_id!="")?2.380:'0';
	$field += ($userdata[0]->sponsor_name!="")?2.380:'0';
	$field += ($userdata[0]->account_holder!="")?2.380:'0';
	$field += ($userdata[0]->bank_name!="")?2.380:'0';
	$field += ($userdata[0]->branch_name!="")?2.380:'0';
	$field += ($userdata[0]->bank_accountno!="")?2.380:'0';
	$field += ($userdata[0]->bank_ifsccode!="")?2.380:'0';
	//return $field;
	$percantage = ($field*100)/100;
	return round($percantage);
}

function returnAllDisplayAddPage()
{
	$pages = array(
		'home'=>'Home',
		'about-us'=>'About Us',
		'why-us'=>'Why Us',
		'contact-us'=>'Contact Us',
		'terms-condition'=>'Terms & Conditions',
		'how-it-work'=>'How it Works',
		'privacy-policy'=>'Privacy Policy',
		'what-is-sme'=>'What is SME',
		'faq'=>'Faq',
		'what-makes-us-different'=>'What Makes us Different',
		'task-holidays'=>'Task Holidays',
		'bank-details'=>'Bank Details',
		'legal-documents'=>'Legal Documents'
	);
	return $pages;
}

function returnAllVideoAddPage()
{
	$pages = array(
		'home'=>'Home',
		'about-us'=>'About Us',
		'contact-us'=>'Contact Us'		
	);
	return $pages;
}

function getLocation($ip)
{
        error_reporting(0);
	/*Get user ip address*/
			$ip_address = '223.190.25.0'; //$ip; //'223.190.25.0'; //$_SERVER['REMOTE_ADDR'];

			/*Get user ip address details with geoplugin.net*/
			$url='http://www.geoplugin.net/php.gp?ip='.$ip_address;
			//  Initiate curl
			$ch = curl_init();
			// Disable SSL verification
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// Will return the response, if false it print the response
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// Set the url
			curl_setopt($ch, CURLOPT_URL,$url);
			// Execute
			$result=unserialize(curl_exec($ch));
			// Closing
			curl_close($ch);
			/*Get City name by return array*/
			$city = $result['geoplugin_city']; 
			/*Get Country name by return array*/
			$country = $result['geoplugin_countryName'];			

			if(!$city){
			   $city='Not Define';
			}if(!$country){
			   $country='Not Define';
			}
			echo $city.','.$country;
}

function getadminmodule()
{
	$array = array('users'=>'Manage Users',
	'plan'=>'Manage Plan',
	'rewards'=>'Manage Rewards',
	'helpdesk'=>'Manage Helpdesk',
	'bookadd'=>'Book Your Add',
	'purchasecalculator'=>'Purchase Calculator',
	'bookingcalculator'=>'Book Your Add Calculator',
	'banner'=>'Manage Slider',
	'dashboard'=>'Manage Dashboard',
	'staticblock'=>'Manage Static Block',
	'workhistory'=>'Manage Work History',
	'comments'=>'Manage Comments',
	'invoicecontent'=>'Manage Invoice Content',
	'alltransactiondetails'=>'Manage Transaction Details',
	'managepromotionalincome'=>'Manage Promotional Income',
	'popup'=>'Manage Popup',
	'tax_settings'=>'Manage Settings',
	'managewalletstatement'=>'Download Wallet Statement',
	'dpromotionalincome'=>'Distribute Promotional Income', 
	'downloadalluserinvoise'=>'Download All User Invoice',
    'useractivationpanel'=>'Manage Users Activation Panel',
    'trackadminwork'=>'Trace Sub Admin',
	'salesreport'=>'Sponsor ID Sales Report'
	);
	
	return $array;
}
?>