<?php $this->load->view('front/layout/header-inner'); ?>
<?php 
$myplandata = $this->plan_model->selectPlanByID($login_user[0]->plan);
if(count($myplandata)>0)
{
	$expcam = 0;
	$expcam = $myplandata[0]->validity*30;
	/*if($myplandata[0]->validity==12)
	{
		$expcam = 365;
	}
	else
	{
		$expcam = 182;
	}*/
}
else
{
	$expcam = 0;
}
?>

										<?php if(count($KYCDATA)>0){ ?>
											<input type="hidden" name="oldIdentityproof" value="<?php echo $KYCDATA[0]->identityproof; ?>" >
											<input type="hidden" name="oldIpancard" value="<?php echo $KYCDATA[0]->pancard; ?>" >
										<?php
											if($KYCDATA[0]->idstatus=="1"){	$idproof = 'disabled'; }else{ $idproof = ''; }
											if($KYCDATA[0]->pancardstatus=="1"){	$pancard = 'disabled'; }else{ $pancard = ''; }
											if($KYCDATA[0]->bankdetailsstatus=="1"){	$bankdetails = 'disabled'; }else{ $bankdetails = ''; }
										}
										else
										{
											$idproof = ''; $pancard = ''; $bankdetails = '';
										}
										?>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>User Profile</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>User Profile</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">
						<div class="col-md-4 col-lg-3">
							<section class="panel">
								<div class="panel-body">
									<div class="thumb-info mb-md">
										<img src="<?php echo base_url('uploads/user/'.$login_user[0]->image); ?>" class="rounded img-responsive" alt="<?php echo $login_user[0]->name; ?>">
									</div>

									<div class="widget-toggle-expand mb-md">
										<div class="widget-header">										
                                        	<h4><?php echo $login_user[0]->name; ?></h4>
											<h6>Profile Completion <span><?php echo calculateProfileComplate($login_user); ?>%</span></h6>
											<div class="widget-toggle">+</div>
										</div>
										<div class="widget-content-collapsed">
											<div class="progress progress-xs light">
												<div class="progress-bar progress-bar-success" aria-valuenow="<?php echo calculateProfileComplate($login_user); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo calculateProfileComplate($login_user); ?>%;">
													<?php echo calculateProfileComplate($login_user); ?>%
												</div>
											</div>
										</div>
										<div class="widget-content-expanded">                                        
											<ul class="simple-todo-list">
												<li class="completed"><strong>Id:</strong> <?php echo $login_user[0]->id; ?></li>
                                                <br>
												<li class="completed"><strong>Last Login :</strong> <?php if($login_user[0]->last_login_time!=""){ echo date('d-m-Y, h:i A',$login_user[0]->last_login_time);}; ?></li>
												<li class="completed"><strong>Login IP :</strong> <?php echo $login_user[0]->last_login_ip; ?></li>
											</ul>
										</div>
									</div>
									<div class="clearfix">
									</div>

									<hr class="dotted short">
									<div class="social-icons-list">
										<a rel="tooltip" data-placement="bottom" target="_blank" href="http://www.facebook.com/" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
										<a rel="tooltip" data-placement="bottom" href="http://www.twitter.com/" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
										<a rel="tooltip" data-placement="bottom" href="http://www.linkedin.com/" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
									</div>
								</div>
							</section>
						</div>
						<div class="col-md-8 col-lg-6">
							<?php echo $this->session->flashdata('message'); ?>
							<div class="tabs">
								<ul class="nav nav-tabs tabs-primary">
									<li class="active">
										<a href="#overview" data-toggle="tab">My Profile</a>
									</li>
                                    <li>
										<a href="#edit" data-toggle="tab">Edit Profile</a>
									</li>
									<li>
										<a href="#bank" data-toggle="tab">Edit Bank Details</a>
									</li>
                                    <li>
										<a href="#change-password" data-toggle="tab">Change Password</a>
									</li>                                    
								</ul>
								<div class="tab-content">
									<div id="overview" class="tab-pane active">
										<h4 class="mb-md">My Profile</h4>
                                        <hr>										
                                        <div class="mt-xlg mb-md">
											<div class="tm-body">											
												<div class="form-group">
													<div class="col-md-3"><p><strong>Full Name</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->name; ?></p></div>
												</div>   
												<div class="form-group">
													<div class="col-md-3"><p><strong>Email</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->email; ?></p></div>
												</div>   
												<div class="form-group">
													<div class="col-md-3"><p><strong>Mobile</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->contact_no; ?></p></div>
												</div> 
												<div class="form-group">	
													<div class="col-md-3"><p><strong>Nominee</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->nominee; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Gender</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->gender; ?></p></div>
												 </div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Address</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->address; ?></p></div>
												 </div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>City</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->city; ?> </p></div>
												 </div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>State</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->state; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Pin Code</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->pincode; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Colleges</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->colleges; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>School</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->schools; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Mobile Models</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->mobile_handset; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Highest Degree</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->highest_degree; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Extra Skills</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->extra_skills; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Pancard No.</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->pancard_no; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Aadhar Card No.</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->adhaar_no; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Voter ID Card</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->voterid_no; ?></p></div>
												</div>
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Passport No.</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->passport_no; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Favorite Celebrity</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->celebrity; ?></p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3"><p><strong>Date Of Birth</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->dob; ?></p></div>
												</div> 
												<h4 class="mb-md">&nbsp;</h4><hr>
													   
												<div class="form-group">        
													<div class="col-md-3"><p><strong>Profession</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->profession; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Industry</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->industry; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Current Industry</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->current_industry; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Company Name</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->company_name; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Department</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->field_work; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Hobbies</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->hobbies; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Education</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->education; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Course Type</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->course_type; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Salary</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->salary; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Turn Over</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->turn_over; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Total Job or Business Experience</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->total_job; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Current Job or Business Experience</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->current_job; ?></p></div>
												</div> 
												<div class="form-group">    
													<div class="col-md-3"><p><strong>Current Designation</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p><?php echo $login_user[0]->current_designation; ?></p></div>
												</div>  
												<div class="form-group">  
													<div class="col-md-3"><p><strong>Account Status</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p>
														<?php if($login_user[0]->status==1){ ?>
															<button type="button" class="btn btn-success btn-xs"><b>Active</b></button>
														<?php }else{ ?>
															<button type="button" class="btn btn-danger btn-xs"><b>Inactive</b></button>
														<?php } ?>	
														</p>
													</div>
												</div> 
												<div class="form-group">  
													<div class="col-md-3"><p><strong>Booster Status</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-9"><p>													
														<?php if($login_user[0]->boosterStatus==1){ ?>
															<button type="button" class="btn btn-success btn-xs"><b>Active</b></button>
														<?php }else{ ?>
															<button type="button" class="btn btn-danger btn-xs"><b>Inactive</b></button>
														<?php } ?>													
													</div>
												</div>
												<?php /*	
												<div class="form-group"> 
													<div class="col-md-12"><p>You Are 14 Days Left For Activate Booster.</p></div>
												</div>
												*/ ?>	
											</div>
										</div>
                                        <div class="col-md-12" >
														<div class="panel-group" id="accordion2">
															<div class="panel panel-accordion panel-accordion-primary">
																<div class="panel-heading">
																	<h4 class="panel-title" style="background-color:black !important;">
																		<a style="background-color:black !important;" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2One">
																			Invoice Details
																		</a>
																	</h4>
																</div>
																<div id="collapse2One" class="accordion-body collapse in" style="">
																	<div class="panel-body">
																	<a class="btn btn-warning pull-right" target="_blank" href="<?php echo base_url('index.php/user/user_invoice/'.$login_user[0]->id); ?>" class="pull-right">
																		Print 
																	</a>
																		<form method="post">
<div class="table-responsive">
										<table class="table table-bordered table-striped table-condensed mb-none">
											<thead>
												<tr>
													<th>S.No</th>
													<th>Amount</th>
													<th class="text-right">Tax</th>
													<th class="text-right">Plan Name</th>
													<th class="text-right">Invoice ID</th>	
													<th class="text-right">Transaction ID</th>
													<th class="text-right">Date</th>	
													<th class="text-right">Total</th>	
												</tr>
											</thead>
											<tbody>
											<?php $userInvData = $this->tree_model->selectUserInvoice($login_user[0]->id);  ?>
											<?php $l=0; foreach($userInvData as $invData){ $l++; ?>	
											<?php if($invData->status=='paid'){ ?>
												<tr>
													<td><?php echo $l; ?></td>
													<td><i class="fa fa-rupee"></i> <?php echo $invData->amount; ?></td>
													<td class="text-right"> <?php echo $invData->tax; ?></td>
													<td class="text-right"> <?php echo $invData->planName; ?></td>
													<td class="text-right"> <?php echo $invData->invoiceID; ?></td>
													<td class="text-right"> <?php echo $invData->transactionID; ?></td>
													<td class="text-right"> <?php echo date('d-m-Y h:m a',$invData->invTime); ?></td>
													<td class="text-right"><i class="fa fa-rupee"></i> <?php echo $invData->finalAmount; ?></td>
												</tr>
											<?php }} ?>	
											</tbody>																								
										</table>										
									</div>																		</form>
																	</div>
																</div>
															</div>
															<div class="panel panel-accordion panel-accordion-primary">
																<div class="panel-heading">
																	<h4 class="panel-title" style="background-color:black !important;">
																		<a style="background-color:black !important;" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2Two">
																			Purchase Request (NEFT/IMPS)
																		</a>
																	</h4>
																</div>
																<div id="collapse2Two" class="accordion-body collapse" style="height: 0px;">
																	<div class="panel-body">
																		<table class="table table-bordered table-striped table-condensed mb-none">
											<thead>
												<tr>
													<th>Payment ID</th>
													<th>A/C Holder Name</th>
													<th>UTR</th>
													<th>From A/C</th>
													<th>NEFT Date</th>
													<th>Epoints</th>
													<th>Amount</th>													
												</tr>
											</thead>
											<tbody>
											<?php $planInvData = $this->user_model->selectPlanInvByUid($login_user[0]->id); ?>
											<?php if(count($planInvData)>0){ ?>
												<tr>
													<td><?php echo $planInvData[0]->id; ?></td>
													<td><?php echo $planInvData[0]->accountHolderName; ?></td>
													<td><?php echo $planInvData[0]->utrNumber; ?></td>
													<td><?php echo $planInvData[0]->fromAccount; ?></td>
													<td><?php echo $planInvData[0]->neftDate; ?></td>
													<td><?php echo $planInvData[0]->ePoints; ?></td>
													<td><?php echo $planInvData[0]->amount; ?></td>
												</tr>
											<?php }else{ ?>	
												<tr>
													<td colspan="7"><strong>Purchase request not found.</strong></td>													
												</tr>
											<?php } ?>
											</tbody>																								
										</table>
																	</div>
																</div>
															</div>
															
														</div>
														
													</div>
                                     
                            		
                          				
                                        
                                    </div>                                                                        
									<div id="edit" class="tab-pane">
										<form class="form-horizontal" method="post" enctype="multipart/form-data">
											<h4 class="mb-xlg">My Profile</h4>
											<fieldset>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Full Name</label>
													<div class="col-md-8">
														<input type="text" name="name" class="form-control" value="<?php echo $login_user[0]->name; ?>" data-validation="required" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Profile Picture</label>
													<div class="col-md-8">
														<input type="file" name="image" class="form-control">
														<input type="hidden" name="oldImage" class="form-control" value="<?php echo $login_user[0]->image; ?>" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Email</label>
													<div class="col-md-8">
													    <?php $isAdmin = $this->session->userdata('LOGINBY'); ?>
														<?php if($isAdmin!='ADMIN'){$readEmail ='readonly';}else{$readEmail =""; } ?>
														<input type="email" name="email" <?php echo $readEmail; ?> class="form-control" value="<?php echo $login_user[0]->email; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileLastName">Mobile</label>
													<div class="col-md-8">
														<input type="number" name="contact_no" id="phone" onkeypress="return isNumberKey(event);" size="10" class="form-control" value="<?php echo $login_user[0]->contact_no; ?>" data-validation="required">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileAddress">Nominee</label>
													<div class="col-md-8">
														<input type="text" name="nominee" class="form-control" value="<?php echo $login_user[0]->nominee; ?>" data-validation="required">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Gender</label>
													<div class="col-md-8">
														<select type="text" class="form-control" name="gender" data-validation="required">
                                                        	<option <?php echo($login_user[0]->gender=='Male')?'selected':'' ?> value="Male">Male</option>
                                                            <option <?php echo($login_user[0]->gender=='Male')?'selected':'' ?> value="Female">Female</option>
                                                        </select>
													</div>
												</div>                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label"  for="profileCompany">Address</label>
													<div class="col-md-8">
														<input type="text" name="address" class="form-control" value="<?php echo $login_user[0]->address; ?>" data-validation="required" >
													</div>
												</div>                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label"  for="profileCompany">City</label>
													<div class="col-md-8">
														<input type="text" name="city" class="form-control" value="<?php echo $login_user[0]->city; ?>" data-validation="required" >
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label"  for="profileCompany">State</label>
													<div class="col-md-8">
														<input type="text" name="state" class="form-control" value="<?php echo $login_user[0]->state; ?>" data-validation="required" >
													</div>
												</div>
												
												<div class="form-group">   
													<div class="col-md-3 control-label" ><p><strong>Pancard No.</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-8"><p>
														<input type="text" name="pancard_no" class="form-control" value="<?php echo $login_user[0]->pancard_no; ?>" data-validation="required" >
													</p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3 control-label"><p><strong>Aadhar Card No.</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-8"><p>
													<input type="text" name="adhaar_no" class="form-control" value="<?php echo $login_user[0]->adhaar_no; ?>" data-validation="required" >
													</p></div>
												</div> 
												<div class="form-group">   
													<div class="col-md-3 control-label"><p><strong>Voter ID Card</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-8"><p>
													<input type="text" name="voterid_no" class="form-control" value="<?php echo $login_user[0]->voterid_no; ?>" data-validation="required" >
													</p></div>
												</div>
												<div class="form-group">   
													<div class="col-md-3  control-label"><p><strong>Passport No.</strong><span class="pull-right">:</span></p></div>
													<div class="col-md-8"><p>
													<input type="text" name="passport_no" class="form-control" value="<?php echo $login_user[0]->passport_no; ?>" data-validation="required" >
													</p></div>
												</div>
												
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Pin Code</label>
													<div class="col-md-8">
														<input type="text" name="pincode" class="form-control" value="<?php echo $login_user[0]->pincode; ?>"data-validation="required" >
													</div>
												</div>
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Colleges</label>
													<div class="col-md-8">
														<input type="text" name="colleges" class="form-control" value="<?php echo $login_user[0]->colleges; ?>" >
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">University</label>
													<div class="col-md-8">
														<input type="text" name="university"  class="form-control" value="<?php echo $login_user[0]->university; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Schools</label>
													<div class="col-md-8">
														<input type="text" name="schools"  class="form-control" value="<?php echo $login_user[0]->schools; ?>">
													</div>
												</div>
                                                
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Mobile Handset Models</label>
													<div class="col-md-8">
														<input type="text" name="mobile_handset" class="form-control" value="<?php echo $login_user[0]->mobile_handset; ?>">
													</div>
												</div>
                                                
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Highest Degree</label>
													<div class="col-md-8">
														<input type="text" name="highest_degree" class="form-control" value="<?php echo $login_user[0]->highest_degree; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Extra Skills</label>
													<div class="col-md-8">
														<input type="text" name="extra_skills" class="form-control" value="<?php echo $login_user[0]->extra_skills; ?>">
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Favorite Celebrity</label>
													<div class="col-md-8">
														<input type="text" name="celebrity" class="form-control" value="<?php echo $login_user[0]->celebrity; ?>">
													</div>
												</div>
                                                
                                               <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Date Of Birth</label>
													<div class="col-md-8">
														<input type="text" name="dob" data-plugin-datepicker class="form-control" value="<?php echo $login_user[0]->dob; ?>" data-validation="required" >
													</div>
												</div>
                                                
											</fieldset>
											<hr class="dotted tall">
											
											<fieldset>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Profession</label>
													<div class="col-md-8">
														<input type="text" name="profession" class="form-control" value="<?php echo $login_user[0]->profession; ?>">
													</div>
												</div>
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Industry</label>
													<div class="col-md-8">
														<input type="text" name="industry" class="form-control" value="<?php echo $login_user[0]->industry; ?>">
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Current Industry</label>
													<div class="col-md-8">
														<input type="text" name="current_industry" class="form-control" value="<?php echo $login_user[0]->current_industry; ?>">
													</div>
												</div>
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Company Name</label>
													<div class="col-md-8">
														<input type="text" name="company_name" class="form-control" value="<?php echo $login_user[0]->company_name; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Campaign Title</label>
													<div class="col-md-8">
														<input type="text" name="campaign_title" class="form-control" value="<?php echo $login_user[0]->campaign_title; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Current Company Name</label>
													<div class="col-md-8">
														<input type="text" name="current_company" class="form-control" value="<?php echo $login_user[0]->current_company; ?>">
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Department</label>
													<div class="col-md-8">
														<input type="text" name="field_work" class="form-control" value="<?php echo $login_user[0]->field_work; ?>">
													</div>
												</div>
												                                                
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Flight Travel</label>
													<div class="col-md-8">
														<select type="text" class="form-control" name="flight_travel">
                                                        	<option value="">-Select-</option>
                                                            <option <?php echo($login_user[0]->flight_travel=='yes')?'selected':''; ?> value="yes">Yes</option>
                                                            <option <?php echo($login_user[0]->flight_travel=='no')?'selected':''; ?>  value="no">No</option>
                                                        </select>
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Hobbies</label>
													<div class="col-md-8">
														<input type="text" name="hobbies" class="form-control" value="<?php echo $login_user[0]->hobbies; ?>">
													</div>
												</div>
                                                
                                               	<div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Education</label>
													<div class="col-md-8">
														<input type="text" name="education" class="form-control" value="<?php echo $login_user[0]->education; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Course Type Done</label>
													<div class="col-md-8">
														<input type="text" name="course_type" class="form-control" value="<?php echo $login_user[0]->course_type; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Salary</label>
													<div class="col-md-8">
														<input type="text" name="salary" onkeypress="return isNumberKey(event);"  class="form-control" value="<?php echo $login_user[0]->salary; ?>" data-validation="required" >
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Turn Over</label>
													<div class="col-md-8">
														<input type="text" name="turn_over" onkeypress="return isNumberKey(event);"  class="form-control" value="<?php echo $login_user[0]->turn_over; ?>" data-validation="required" >
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Total Job or Business Experience</label>
													<div class="col-md-8">
														<input type="text" name="total_job" class="form-control" value="<?php echo $login_user[0]->total_job; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Current Job Or Business Experience</label>
													<div class="col-md-8">
														<input type="text" name="current_job" class="form-control" value="<?php echo $login_user[0]->current_job; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Current Designation</label>
													<div class="col-md-8">
														<input type="text" name="current_designation" class="form-control" value="<?php echo $login_user[0]->current_designation; ?>">
													</div>
												</div>
											</fieldset>
											
											
										
										
										<?php 
										/*
										if(count($KYCDATA)>0){ ?>
											<input type="hidden" name="oldIdentityproof" value="<?php echo $KYCDATA[0]->identityproof; ?>" >
											<input type="hidden" name="oldIpancard" value="<?php echo $KYCDATA[0]->pancard; ?>" >
										<?php	if($KYCDATA[0]->idstatus=="1"){	$idproof = 'disabled'; }else{ $idproof = ''; }
											if($KYCDATA[0]->pancardstatus=="1"){	$pancard = 'disabled'; }else{ $pancard = ''; }
											if($KYCDATA[0]->bankdetailsstatus=="1"){	$bankdetails = 'disabled'; }else{ $bankdetails = ''; }
										}
										else
										{
											$idproof = ''; $pancard = ''; $bankdetails = '';
										}*/
										?>
										<br>
										<div class="table-responsive">
										<table class="table table-bordered table-striped table-condensed mb-none">
											<thead>
												<tr>
													<th width="15%">Document</th>
													<th  width="35%">Document Type</th>
													<th width="10%">Select File</th>
													<th ></th>
												</tr>
											</thead>
											<?php if(count($KYCDATA)>0){ ?>
											<input type="hidden" name="oldIdentityproof" value="<?php echo $KYCDATA[0]->identityproof; ?>" >
											<input type="hidden" name="oldIpancard" value="<?php echo $KYCDATA[0]->pancard; ?>" >
											<?php } ?>
											<tbody>												
												<tr>											
													<td>Identity Proof</td>
													<td>
														<select class="form-control" name="idtype" <?php echo $idproof; ?>>
															<option value="Aadhar Card">Aadhar Card</option> 
															<option value="Voter ID">Voter ID</option>
															<option value="Driving Licence">Driving Licence</option>
															<option value="Passport">Passport</option>
														</select>
													</td>
													<td width="13%">
														<input type="file" style="width: 178px;"  name="identityproof" <?php echo $idproof; ?>>													
													</td>
													<td>
														<?php if(isset($KYCDATA[0]->identityproof)){ if($KYCDATA[0]->identityproof!=""){  ?>
															<img src="<?php echo base_url('uploads/document/'.$KYCDATA[0]->identityproof); ?>" width="100" height="50">
														<?php } } ?>
													</td>
												</tr>												
												<tr>
													<td>PAN Card</td>
													<td>
														<select class="form-control" name="pancardType" <?php echo $pancard; ?>>
															<option value="Pan Card">Pan Card</option>
														</select>
													</td>
													<td class="text-right"><input style="width: 178px;" <?php echo $pancard; ?> type="file" name="pancard"></td>
													<td>
														<?php if(isset($KYCDATA[0]->pancard)){ if($KYCDATA[0]->pancard!=""){ ?>
															<img src="<?php echo base_url('uploads/document/'.$KYCDATA[0]->pancard); ?>" width="100" height="50">
														<?php } }  ?>
													</td>										
												</tr>																						
											</tbody>
										</table>
									</div>
										
									<div class="panel-footer">
											<div class="row">
												<div class="col-md-9 col-md-offset-3">
													<button type="submit" name="updateInformation" class="btn btn-primary">Update Information</button>
												</div>
											</div>
										</div>
									</form>	
										
										
									</div>                                    
                                    <?php 
										if($login_user[0]->bank_details==1){ $bankDetalsLock = "disabled"; }else{ $bankDetalsLock= ""; }  
									?>
									<div id="bank" class="tab-pane">
										<form class="form-horizontal" method="post" enctype="multipart/form-data">
											<h4 class="mb-xlg">Bank Details</h4>
											<fieldset>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Account Holder</label>
													<div class="col-md-8">
														<input type="text" data-validation="required" <?php echo $bankDetalsLock; ?> name="account_holder" class="form-control" value="<?php echo $login_user[0]->account_holder; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Bank Name</label>
													<div class="col-md-8">
														<input type="text" data-validation="required" <?php echo $bankDetalsLock; ?> name="bank_name" class="form-control" value="<?php echo $login_user[0]->bank_name; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Branch Name</label>
													<div class="col-md-8">
														<input type="text" data-validation="required" <?php echo $bankDetalsLock; ?> name="branch_name" class="form-control" value="<?php echo $login_user[0]->branch_name; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileLastName">Account No.</label>
													<div class="col-md-8">
														<input type="text" data-validation="required" <?php echo $bankDetalsLock; ?> name="bank_accountno" class="form-control" value="<?php echo $login_user[0]->bank_accountno; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileLastName">IFSC</label>
													<div class="col-md-8">
														<input type="text" data-validation="required" <?php echo $bankDetalsLock; ?> name="bank_ifsccode" class="form-control" value="<?php echo $login_user[0]->bank_ifsccode; ?>">
													</div>
												</div>
												
												<?php if(count($KYCDATA)>0){ ?>
												<input type="hidden" name="oldBankPassbook" value="<?php echo $KYCDATA[0]->bankdetails; ?>" >
												<input type="hidden" name="oldCancelCheque" value="<?php echo $KYCDATA[0]->cancelCheque; ?>" >
												<?php } ?>
												
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileLastName">Upload Bank Passbook</label>
													<div class="col-md-6">
														<input type="file" name="bankdetails" <?php echo $bankDetalsLock; ?> class="form-control" data-validation="required" >
													</div>
													<div class="col-md-2">
														<?php if(count($KYCDATA)>0){ if($KYCDATA[0]->cancelCheque!=""){ ?>
															<img src="<?php echo base_url('uploads/document/'.$KYCDATA[0]->bankdetails); ?>" width="70" >
														<?php } } ?>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileLastName">Upload Cancel Cheque</label>
													<div class="col-md-6">
														<input type="file" name="cancelCheque" <?php echo $bankDetalsLock; ?> class="form-control" data-validation="required" >
													</div>
													<div class="col-md-2">
														<?php if(count($KYCDATA)>0){ if($KYCDATA[0]->cancelCheque!=""){ ?>
															<img src="<?php echo base_url('uploads/document/'.$KYCDATA[0]->cancelCheque); ?>" width="70" >
														<?php }} ?>
													</div>
												</div>													
											</fieldset>
											
											<div class="panel-footer">
												<div class="row">
													<div class="col-md-9 col-md-offset-3">
														<button type="submit" name="updateBankInformation" <?php echo $bankDetalsLock; ?> class="btn btn-primary">Update Bank Information</button>
													</div>
												</div>
											</div>
										</form>
									</div>
									
                                    <div id="change-password" class="tab-pane">

										<form class="form-horizontal" method="post">
											<input type="hidden" name="hiddenpassword" value="<?php echo $login_user[0]->password; ?>">											
											<h4 class="mb-xlg">Change Password</h4>
											<fieldset class="mb-xl">
												<div class="form-group">
													<label class="col-md-4 control-label" for="opwd">Old Password</label>
													<div class="col-md-7">
														<input type="password" class="form-control" data-validation="required" id="opwd" name="opwd">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label" for="npwd">New Password</label>
													<div class="col-md-7">
														<input type="password" class="form-control" data-validation="required" id="npwd" name="npwd">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label" for="rpwd">Repeat New Password</label>
													<div class="col-md-7">
														<input type="password" class="form-control" data-validation="required" id="rpwd" name="rpwd">
													</div>
												</div>
                                                
											</fieldset>
											<div class="panel-footer">
												<div class="row">
													<div class="col-md-9 col-md-offset-3">
														<button type="submit" name="changePassword" class="btn btn-primary">Update Password</button>
													</div>
												</div>
											</div>

										</form>

									</div>
								</div>
							</div>
                          
						</div>
						<div class="col-md-12 col-lg-3">
                        <section class="panel">
							<?php 
								if($login_user[0]->boosterused==0 && $login_user[0]->boosterStatus==0)
								{
									$reg_date = $login_user[0]->rdate;//'2017-02-6';
									$exp_date = date('Y-m-d',strtotime("$reg_date +20 days"));
									if(date('Y-m-d')<$exp_date)
									{
							?>
							<a href="<?php echo base_url('index.php/booster/acivate'); ?>" class="btn btn-info">Activate Your Booster</a>
								<?php } } ?>	
							<div class="panel-body">
                                <h4 class="mb-md">Bank Details <br/><span class="title-sec">(Transfer Your Payouts)</span></h4>
                                <hr>
                                <ul class="simple-user-list mb-xlg">
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Account Holder</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        <span class="text-sec"><?php echo $login_user[0]->account_holder; ?></span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Bank Name</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        <span class="text-sec"><?php echo $login_user[0]->bank_name; ?></span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Branch Name</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        <span class="text-sec"><?php echo $login_user[0]->branch_name; ?></span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Account No</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><?php echo $login_user[0]->bank_accountno; ?></span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>IFSC </strong><div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><?php echo $login_user[0]->bank_ifsccode; ?></span>
                                    </li>							
									
									<li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Bank Passbook</strong><div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec">
											<?php if(isset($KYCDATA[0]->bankdetails)){ if($KYCDATA[0]->bankdetails!=""){  ?>
												<img src="<?php echo base_url('uploads/document/'.$KYCDATA[0]->bankdetails); ?>" width="100">
											<?php } } ?>
										</span>
                                    </li>
									<li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Identity Proof</strong><div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec">
										<?php if(isset($KYCDATA[0]->identityproof)){ if($KYCDATA[0]->identityproof!=""){  ?>
											<img src="<?php echo base_url('uploads/document/'.$KYCDATA[0]->identityproof); ?>" width="100">
										<?php } } ?>
										</span>
                                    </li>
									<li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Pan Card</strong><div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec">
										<?php if(isset($KYCDATA[0]->pancard)){ if($KYCDATA[0]->pancard!=""){ ?>
											<img src="<?php echo base_url('uploads/document/'.$KYCDATA[0]->pancard); ?>" width="100">
										<?php } } ?>
										</span>
                                    </li>
									
                                </ul>
                            </div>
                            </section>
                            <?php //$myplandata = $this->plan_model->selectPlanByID($login_user[0]->plan); //print_r($myplandata); ?>
                            <section class="panel">
                            <div class="panel-body">
                                <h4 class="mb-md">Plan Details </h4>
                                <hr>
                                <ul class="simple-user-list mb-xlg">
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Plan Name</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        <span class="text-sec"><?php echo $myplandata[0]->planName; ?></span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Plan Fees </strong><div class="pull-right">:</div></span>
                                        </figure>
                                        <span ><i class="fa fa-rupee"></i> <?php echo $myplandata[0]->planFees; ?></span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>CTP Points</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        <span class="text-sec"><?php echo $myplandata[0]->totalVisitors; ?> </span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Validity </strong><div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><?php echo $myplandata[0]->validity; ?> Months</span>
                                    </li>
                                    <?php 
											if($login_user[0]->boosterStatus==1)
											{
												$tasklink = ($login_user[0]->linkPerDay+$login_user[0]->booster_task);
											}
											else
											{
												$tasklink =  $login_user[0]->linkPerDay;
											}
										?>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Work</strong> <div class="pull-right">:</div></span>
                                        </figure>                                        
                                        <span class="text-sec"><?php echo $tasklink; ?> Links Per Day</span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Earning Per Click</strong> <div class="pull-right">:</div></span>
                                        </figure>                                        
                                        <span class="text-sec"><i class="fa fa-rupee"></i> <?php echo $login_user[0]->clickprice; ?></span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Earning Per Day</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><i class="fa fa-rupee"></i> <?php echo $myplandata[0]->linkPerDay*$myplandata[0]->earningPerClick; ?></span>
                                    </li>
                                    
                                    
                                     <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Capping</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><?php echo $myplandata[0]->capping; ?></span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Turnover</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><?php echo $myplandata[0]->turnover; ?></span>
                                    </li>                                   
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Joining Date</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><?php echo date('d-M-Y',strtotime($login_user[0]->expireuser)); ?></span>
                                    </li>
                                    
                                     <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Expiry Date</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><?php  echo date('d-M-Y',strtotime($login_user[0]->expireuser . " + $expcam day")); ?></span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Payout Mode</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><?php echo $login_user[0]->payoutmode; ?></span>
                                    </li>
                                    <li>
										<div class="panel-footer">
											<a type="button" <?php echo ($login_user[0]->payout_button=='0')?'disabled':''; ?> class="btn btn-warning" href="<?php echo base_url('index.php/user/paymode'); ?>">Change Payout Mode</a>
										</div>
										<?php if($login_user[0]->pay_botton==1){ ?>
											<a href="<?php echo $myplandata[0]->paymentLink; ?>" name="purchaseNow" class="btn btn-info">Pay Now</a>
										<?php } ?>
                                    </li>
                                </ul> 
                            </div>
                           </section>
						</div>

					</div>
					<!--footer start-->
						<?php $this->load->view('front/layout/footer'); ?>	
					<!--end start-->
					<!-- end: page -->
				</section>
			</div>
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
	<!-- Analytics to Track Preview Website -->	
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
  $.validate({
    lang: 'en'
  });
  
  function isNumberKey(evt)
{	
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false;
	}
	else
	{
		/*if (document.getElementById('phone').value.length <10)
		{
			return true;
		}
		else
		{
			return false;
		}*/
		return true;
	}    
}
</script>

	</body>


</html>