<?php 
class Popup extends CI_Controller
{
	function listing()
	{
		$args = func_get_args();
		$this->load->model('popup_model');
		$data['bookdata'] = $this->popup_model->getAllPopup();
		$this->load->view('admin/popup/listing',$data);
	}
		
	function edit()
	{
		$args = func_get_args();
		$this->load->model('popup_model');
		
		if(isset($_POST['editData']))
		{
			if($_FILES['image']['name']!="")
			{
				$file = time().'_'.$_FILES['image']['name'];
				$file_tmp = $_FILES['image']['tmp_name'];
				$path = 'uploads/popup/'.$file;
				move_uploaded_file($file_tmp,$path);
			}
			else
			{
				$file = $_POST['oldImage'];
			}		
		    $edata['image'] = $file;			
		    $edata["title"]= $this->input->post("title");
        	$edata['youtube'] = $this->input->post('youtube');
        	$edata['link'] = $this->input->post('link');
        	$edata['content'] = $this->input->post('content');
			$edata['type'] = $this->input->post('type');
			$edata['status'] = $this->input->post('status');
			$this->popup_model->updateData($args[0],$edata);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/popup/listing/');	
		}
		$data['REWARDS'] = $this->popup_model->selectPopupByID($args[0]);
		//print_r($popup);
		$this->load->view('admin/popup/edit',$data);
	}
	
	
}
