<?php 
class Staticblock extends CI_Controller
{
	function listing()
	{
		$data['bookdata'] = $this->staticblock_model->getAllBlock();
		$this->load->view('admin/staticblock/listing',$data);
	}
	function edit()
	{
		$args = func_get_args();
		$data['bookdata'] = $this->staticblock_model->getBlockById($args[0]);
		if(isset($_POST['editData']))
		{
			$edata['content']= $this->input->post('content');
			$this->staticblock_model->updateBlockData($args[0],$edata);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/staticblock/listing');	
		}	
		
		$this->load->view('admin/staticblock/edit',$data);
	}
	
}
