<?php 
class Bookingcalculator extends CI_Controller
{
	function listing()
	{
		$this->load->model('bookingcalculator_model');
		$data['bookdata'] = $this->bookingcalculator_model->getAlldata();
		$this->load->view('admin/bookingcalculator/listing',$data);
	}
	
	function add()
	{
		
		$this->load->model('bookingcalculator_model');
		if(isset($_POST['editData']))
		{
			$edata['addtype']= $this->input->post('addtype');
			$edata['page_name']= $this->input->post('page_name');
			$edata['amount']= $this->input->post('amount');
			$this->bookingcalculator_model->insertData($edata);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully added.</div>');
			redirect('index.php/bookingcalculator/listing');	
		}	
		
		$this->load->view('admin/bookingcalculator/add');
	}
	function edit()
	{
		$args = func_get_args();
		//echo $args[0];
		$this->load->model('bookingcalculator_model');
		
		if(isset($_POST['editData']))
		{
			$edata['addtype']= $this->input->post('addtype');
			$edata['page_name']= $this->input->post('page_name');
			$edata['amount']= $this->input->post('amount');
			$this->bookingcalculator_model->updateData($args[0],$edata);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/bookingcalculator/listing');	
		}	
		$data['bookdata'] = $this->bookingcalculator_model->selectByPurchaseID($args[0]);
		$this->load->view('admin/bookingcalculator/edit',$data);
	}
	
	function deleteRecord()
	{
		$args = func_get_args();
		$this->load->model('bookingcalculator_model');		
		$this->bookingcalculator_model->deleteData($args[0]);			
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/bookingcalculator/listing');	
	}	
	
}
