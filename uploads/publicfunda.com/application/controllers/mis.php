<?php 
class Mis extends CI_Controller
{
	function user_mis()
	{
			$this->load->model('tree_model');
		$args = func_get_args();			
		$data['userdata'] = $this->user_model->selectUserByID($args[0]);
		
		// Load all views as normal
		$this->load->view('admin/mis/user-information',$data);
		/*
		// Get output html
		$html = $this->output->get_output();		
		// Load library
		$this->load->library('dompdf_gen');		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."mis-report_$args[0].pdf");	
		*/
	}

	
	function user_download_mis()
	{
		$this->load->model('tree_model');
		$args = func_get_args();			
		$data['userdata'] = $this->user_model->selectUserByID($args[0]);
		
		// Load all views as normal
		$this->load->view('admin/mis/user-information',$data);
		
		// Get output html
		$html = $this->output->get_output();		
		// Load library
		$this->load->library('dompdf_gen');		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."mis-report_$args[0].pdf");			
	}
	
	function download_all_helpdesk_query_mis()
	{					
		$this->load->model('helpdesk_model');
		$data['helpdesk']= $this->helpdesk_model->getAllQuery();		
		$this->load->view('admin/mis/all-query-download',$data);
		
		$html = $this->output->get_output();		
		$this->load->library('dompdf_gen');		
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."mis-report_query.pdf");			
	}
	
	function download_myworkpayout()
	{	
		$args = func_get_args();
		$data['walletData']= $this->user_model->downloadWalletStatementHistory($args[0]);		
		$this->load->view('admin/mis/myworkpayouts_download',$data);
		
		$html = $this->output->get_output();		
		$this->load->library('dompdf_gen');		
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."myworkpayout_".$args[0].".pdf");			
	}
	
	function download_myearnings()
	{	
		$args = func_get_args();
		$data['payoutdata']= $this->campaign_model->getAllRedeemPointsByUserID($args[0]);		
		$this->load->view('admin/mis/my-earnings',$data);
		
		$html = $this->output->get_output();		
		$this->load->library('dompdf_gen');		
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."myearnings_".$args[0].".pdf");			
	}
	
	function work_history_mis()
	{
		$args = func_get_args();
		if(count($args)>0)
		{			
			$user_id = $args[0];
			$campaign_id = $args[4];
			$first_date = date('Y-m-d',$args[1]);
			$second_date = date('Y-m-d',$args[2]);
			$status = $args[3];
			$workHistoryData = $this->campaign_model->getMyWorkHistoryMis($user_id,$campaign_id,$first_date,$second_date,$status);
		}
		else
		{
			$workHistoryData = array();	
		}		
		
		$data['workHistoryData'] = $workHistoryData;			
		// Load all views as normal
		$this->load->view('admin/mis/work-history-mis',$data);		
		// Get output html
		$html = $this->output->get_output();		
		// Load library
		$this->load->library('dompdf_gen');		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."work_history.pdf");	
	}
	
	function promotionalincome_mis()
	{
		$args = func_get_args();
		$this->load->model('tree_model');
		$user_id = $args[0];		
		$sdate = "";
		$esdate = "";
		$status = "";		
		$data['promotionalincome'] = $this->user_model->getPromostionalIncome($user_id,$sdate,$esdate,$status);		
		
		// Load all views as normal
		$this->load->view('admin/mis/promotional_income',$data);		
		// Get output html
		$html = $this->output->get_output();		
		// Load library
		$this->load->library('dompdf_gen');		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."promotional_income_".$args[0].".pdf");	
	}
	
	
	function my_team_mis()
	{
		$args = func_get_args();
		$this->load->model('tree_model');
		$data['sd'] = $args[0];
		$data['ed'] = $args[1];
		$data['position'] = $args[2];
		$data['status'] =$args[3];
			
		$data['login_user'] = $this->user_model->selectUserByID($args[4]);		
		// Load all views as normal
		$this->load->view('admin/mis/my-team-mis',$data);		
		// Get output html
		$html = $this->output->get_output();		
		// Load library
		$this->load->library('dompdf_gen');		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."my_team_".$args[4].".pdf");	
	}
	
	function my_bussiness_mis()
	{
		$args = func_get_args();
		$this->load->model('tree_model');
		$data['sd'] = $args[0];
		$data['ed'] = $args[1];
		$data['position'] = $args[2];
		$data['status'] =$args[3];
			
		$data['login_user'] = $this->user_model->selectUserByID($args[4]);		
		// Load all views as normal
		$this->load->view('admin/mis/my-bussiness-mis',$data);		
		// Get output html
		$html = $this->output->get_output();		
		// Load library
		$this->load->library('dompdf_gen');		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."my_team_".$args[4].".pdf");	
	}
	
	function my_network_mis()
	{
		$args = func_get_args();
		$this->load->model('tree_model');
		$data['login_user'] = $this->user_model->selectUserByID($args[0]);		
		// Load all views as normal
		$this->load->view('admin/mis/my-network-mis',$data);		
		// Get output html
		$html = $this->output->get_output();		
		// Load library
		$this->load->library('dompdf_gen');		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."my_team_".$args[4].".pdf");	
	}
	
	/*
	function zip()
	{
		$this->load->library('zip');
		$path = FCPATH.'/uploads/document/';
		$this->zip->read_dir($path,FALSE);
		$this->zip->download('my_backup.zip');
	}
	*/
}
