<?php 
class Work extends CI_controller
{	
	function request()
	{
		$args = func_get_args();
		$user_login_id = $this->session->userdata('USERID');
		if($user_login_id!=$args[0])
		{
			redirect('index.php/user/profile');
		}	
		if(count($args)>0)
		{
			$users = $user = $this->user_model->selectUserByID($args[0]);
			if(count($users)>0)
			{
				$user_id = $user[0]->id;
				$linkPerDay = $user[0]->linkPerDay;
				$booster_task = $user[0]->booster_task;
				$boosterused = $user[0]->boosterStatus;
				$daily_link = $this->singleUserLinkForDistribute($linkPerDay,$booster_task,$boosterused);
				$checkTodaysLink = $this->campaign_model->selectDistributionLinkByUserIdAndTodayDate($user[0]->id);
				$checkLink = $this->campaign_model->selectDistributionLinkByUserIdAndTodayDate($user[0]->id);
				$finallink = $this->finallink($daily_link,count($checkLink));				
				if($finallink!=0) //count($checkLink)==0)
				{
					$i=1;
					while($i<=$finallink)
					{				
						$campaignLinks = $this->campaign_model->getAllDistributionCampaignBylimet();						
						$countAllDistributedCampaign = $this->campaign_model->getAllDistributionCampaignById($campaignLinks[0]->id);
						if($campaignLinks[0]->campaignPoints>=count($countAllDistributedCampaign))
						{ $i++;								
							
							$taskData['user_id'] = $user[0]->id;
							$taskData['link_id'] = $campaignLinks[0]->id;
							$taskData['distributionDate'] = date('Y-m-d');
							$taskData['status'] = 1;
							$taskData['redeem'] = 1;
							$this->campaign_model->insertTaskLink($taskData);											
						}				
					}
				}
				else
				{
					echo "<strong>Work request has been completed.<strong>";
				}
			}
			else
			{
				redirect('index.php/user/profile');
			}	
		}
		else
		{
			redirect('index.php/user/profile');
		}
	}
	
	function finallink($dailylink,$distributerdLink)
	{
		$finallinks = $dailylink - $distributerdLink;
		return $finallinks;
	}
	
	function singleUserLinkForDistribute($dailylink,$boosterlink,$boosterstatus)
	{
		if($boosterstatus==1)
		{
			return ($dailylink+$boosterlink);
		}
		else
		{
			return $dailylink;
		}	
	}
} 

?>