<?php 
class Payment extends CI_controller
{
	function pay()
	{
		$user_login_id = $this->session->userdata('USERID');
		$userData = $this->user_model->selectUserByID($user_login_id);
		
		$_POST['userid'] = $userData[0]->id;
		$_POST['firstname'] = $userData[0]->name;
		$_POST['email'] = $userData[0]->email;
		$_POST['phone'] = $userData[0]->contact_no;
		$_POST['address1'] = $userData[0]->address;
		$_POST['pincode'] = $userData[0]->pincode;
		$_POST['city'] = $userData[0]->city;
		$_POST['state'] = $userData[0]->state;  
		$_POST['amount'] = $_GET['totalAmount'];
		$_POST['productinfo'] = $_GET['productName'];
		$_POST['furl'] = base_url('index.php/payment/failure');
		$_POST['surl'] = base_url('index.php/payment/success');
		$_POST['curl'] = base_url('index.php/payment/cancel');		
		$this->load->view('front/payment/pay');
		//print_r($_POST);
			
	}
	function failure()
	{
		echo "<h3>your transaction has been failed.</h3>";
		//$user_login_id = $this->session->userdata('USERID');
		//$userData = $this->user_model->selectUserByID($user_login_id);
	}
	function success()
	{
		echo "<h3>your transaction has been processed successfully.</h3>";
		//$user_login_id = $this->session->userdata('USERID');
		//$userData = $this->user_model->selectUserByID($user_login_id);
	}
	function cancel()
	{
		echo "<h3>your transaction has been failed.</h3>";
		//$user_login_id = $this->session->userdata('USERID');
		//$userData = $this->user_model->selectUserByID($user_login_id);
	}
} 

?>