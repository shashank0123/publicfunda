<?php 
class Naseem extends CI_Controller
{
	function index()
	{ 
			$this->load->model('user_model');	
			$this->load->model('admin_model');	
			$this->load->model('booster_model');
			$userData = $this->user_model->selectActiveUser();  // get active all user
			////foreach($userData as $user)
			//{
				$user_id = 10001; //$user->id;
				$user = $this->user_model->selectUserByID($user_id);    // get user data by id
				$lastdate_promotional = 0000;//$user[0]->promotinalincomme_lastdate;  // last date amount
				$carryuseramount = $user[0]->promotional_carry_amount;  // carryforword amount
				$carryforwarduserside = $user[0]->carry_forword_side;   // carryforword side
				
				$user_invoice_total = $this->getTotalOfUserInvoice($user_id);
				
				$left_carry_amount=0;
				$right_carry_amount=0;
				if($carryforwarduserside!=NULL)
				{
					if($carryforwarduserside=='L')
					{
						$left_carry_amount=$carryuseramount;
						$right_carry_amount=0;
					}
					
					if($carryforwarduserside=='R')
					{
						$left_carry_amount=0;
						$right_carry_amount=$carryuseramount;
					}
				}	
				
				$left_income  = $this->user_model->getBonusIncome($lastdate_promotional,$user_id,'L'); //+ $left_carry_amount;	// get left bussiness
				$right_income  = $this->user_model->getBonusIncome($lastdate_promotional,$user_id,'R');// + $right_carry_amount;  // get right bussiness
				
				//echo $left_income; die();	
				$highestAmount = max($left_income, $right_income);
				$lowerAmount = min($left_income, $right_income);
				
				//$applycappingonacount = $this->applycapping($lowerAmount,$user[0]->plan);
				
				$income = $this->applycapping($this->get_promotional_income($lowerAmount),$user[0]->plan);
				//$this->get_promotional_income($lowerAmount);
				$final_income =  $this->applytax($income);
				$carry_position = 'NULL';
				$carry = 0;
				
				if($highestAmount==$left_income)
				{
					$carry_position = 'L';
					$carry_left_amount = abs($left_income - $right_income);
					$carry = abs($left_income - $right_income);
					$carry_right_amount = 00;
				}
				else
				{
					$carry_position = 'R';
					$carry_left_amount = 00;
					$carry_right_amount = abs($left_income - $right_income);
					$carry = abs($left_income - $right_income);
				}

echo 'left bv---'.$left_income.'<br>';
echo 'right_bv---'.$right_income.'<br>';
echo 'carry_left_amount---'.$carry_left_amount.'<br>';
echo 'carry_right_amount---'.$carry_right_amount.'<br>';
echo 'income---'.$income.'<br>';
echo 'tds---'.$this->my_tds($income).'<br>';
echo 'admin---'.$this->my_admincharge($income).'<br>';
echo 'final_income---'.$final_income.'<br>';

die();
				if($income!=0 && !empty($income))
				{
					//$this->user_model->updateUserInvoiceStatus($user_id);
					if($user_invoice_total>0)
					{
						$inv_data['uid'] = $user_id;
						$inv_data['left_bv'] = $left_income;
						$inv_data['right_bv'] = $right_income;
						$inv_data['clbv'] = $carry_left_amount;
						$inv_data['crbv'] = $carry_right_amount;
						$inv_data['income'] = $income;
						$inv_data['tds'] = $this->my_tds($income); //10;
						$inv_data['admincharge'] =$this->my_admincharge($income);
						$inv_data['netIncome'] = $final_income;
						$inv_data['status'] = 'pending';
						$inv_data['crDate'] = date('Y-m-d');
						$this->user_model->insertBonusIncome($inv_data);
						
						$update_user['promotinalincomme_lastdate']= time(); //date('Y-m-d');
						$update_user['promotional_carry_amount']= $carry;
						$update_user['carry_forword_side']= $carry_position;
						//$this->user_model->updateData($user_id,$update_user);
					}
				}	
			//}
			echo 'command executed';
			//echo 'carry_left_amount'.$carry_left_amount.'<br>'.'carry_right_amount'.$carry_right_amount;
	}	
	
	function getTotalOfUserInvoice($userid)
	{
		$this->load->model('booster_model');
		$invoiceData = $this->booster_model->getUserInvoiceByUserID($userid);
		$bonus =0;
		if(count($invoiceData)>0)
		{
			foreach($invoiceData as $inv)
			{
				if($inv->status=='paid')
				{
					$bonus += $inv->amount;
				}						
			}					
		}
		return $bonus;
	}
	
	function applycapping($amount,$planid)
	{
		error_reporting(0); 
		$this->load->model('plan_model');
		$plandata = $this->plan_model->selectPlanByID($planid);
		$capping = $plandata[0]->capping;	
		//echo $capping; die();	
		if($amount>$capping)
		{
			return $capping;
		}
		else
		{
			return $amount;
		}	

	}
	
	function my_tds($amount)
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$tax = $tax[0]->tds;  //10;
		$tax_amount = ($amount*$tax)/100;
		return $tax_amount;
	}
	function my_admincharge($amount)
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$admin_charge = $tax[0]->admin_charge; //5;
		$tax_amount = ($amount*$admin_charge)/100;
		return $tax_amount;
	}
	
	function applytax($amount)
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$admin_charge = $tax[0]->admin_charge; //5;
		$tax = $tax[0]->tds;  //10;
		$total_tax= $admin_charge+$tax;
		$tax_amount = ($amount*$total_tax)/100;
		//return $tax_amount+$amount;
		return abs($amount-$tax_amount);
	}
	function get_promotional_income($amount)
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$amount_per = $tax[0]->promotinalincomme;  //8;
		$final_amount = ($amount*$amount_per)/100;
		return $final_amount;
	}	


	
	
}

?>