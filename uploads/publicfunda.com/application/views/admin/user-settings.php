 <?php $this->load->view('admin/layout/header'); ?>
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>User Settings</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>User Settings</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">						
						<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>                           
							<section class="panel">							
							<div class="panel-body">								
								<div class="table-responsive">
									<form method="post">
										<h2 class="panel-title pull-left">Campaign Management</h2><br>
										<table class="table table-bordered table-striped table-condensed" >	
											<thead>
												<tr>
													<th>No. of Campaign Display</th>
													<th>Update Campaign</th>
													<th>Campaign Info</th>
													<th>Campaign Status</th> 
													<th>Purchase Points</th>
													<th>Book My Add</th>
													<th>Lock Campaign Fields</th>
													<th>CTP Points - <?php echo $user[0]->totalPoints; ?></th>
													<th>Manage Campaign</th>
													<th>Transaction details</th>	
												</tr>	
											</thead>
											<tbody>
												<tr>
													<td>
														<select class="form-control" name="no_of_campaign">
															<option <?php echo($user[0]->no_of_campaign==1)?'selected':''; ?> value='1'>1</option>
															<option <?php echo($user[0]->no_of_campaign==2)?'selected':''; ?> value='2'>2</option>
															<option <?php echo($user[0]->no_of_campaign==3)?'selected':''; ?> value='3'>3</option>
															<option <?php echo($user[0]->no_of_campaign==4)?'selected':''; ?> value='4'>4</option>
															<option <?php echo($user[0]->no_of_campaign==5)?'selected':''; ?> value='5'>5</option>															
														</select>	
													</td>
													<td>
														<select class="form-control" name="update_campaign">
															<option <?php echo($user[0]->update_campaign==1)?'selected':''; ?> value='1'>Visible</option>
															<option <?php echo($user[0]->update_campaign==0)?'selected':''; ?> value='0'>Hide</option>
														</select>
													</td>													
													<td>
														<select class="form-control" name="campaign_info">
															<option <?php echo($user[0]->campaign_info==1)?'selected':''; ?> value='1'>Visible</option>
															<option <?php echo($user[0]->campaign_info==0)?'selected':''; ?> value='0'>Hide</option>
														</select>
													</td>													
													<td>
														<select class="form-control" name="campaign_status_display">
															<option <?php echo($user[0]->campaign_status_display==1)?'selected':''; ?> value='1'>Visible</option>
															<option <?php echo($user[0]->campaign_status_display==0)?'selected':''; ?> value='0'>Hide</option>
														</select>
													</td>													
													<td>
														<select class="form-control" name="purchase_points">
															<option <?php echo($user[0]->purchase_points==1)?'selected':''; ?> value='1'>Visible</option>
															<option <?php echo($user[0]->purchase_points==0)?'selected':''; ?> value='0'>Hide</option>
														</select>
													</td>													
													<td>
														<select class="form-control" name="book_my_add">
															<option <?php echo($user[0]->book_my_add==1)?'selected':''; ?> value='1'>Visible</option>
															<option <?php echo($user[0]->book_my_add==0)?'selected':''; ?> value='0'>Hide</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="lock_field">
															<option <?php echo($user[0]->lock_field==1)?'selected':''; ?> value='1'>Unlock</option>
															<option <?php echo($user[0]->lock_field==0)?'selected':''; ?> value='0'>Lock</option>
														</select>
													</td>
													<td>
														<input type='text' class="form-control" name="totalPoints" value="0<?php //echo $user[0]->totalPoints; ?>">															
													</td>
													<td>
														<a class="btn btn-primary" href="<?php echo base_url('index.php/admin/user_campaign/'.$user[0]->id) ?>">Manage</a>
													</td>
													<td>
														<a class="btn btn-primary" href="<?php echo base_url('index.php/admin/managecampaintransaction/'.$user[0]->id) ?>">Manage</a>
													</td>
												</tr>
											</tbody>
										</table>
										<button type="submit" name="updateCampaignSetting" class="btn btn-info"> Update </button>
									</form>
									<form method="post">
										<h2 class="panel-title pull-left">User Profile Management</h2><br>
										<table class="table table-bordered table-striped table-condensed" >	
											<thead>
												<tr>
													<th>Account Status</th>
													<th>Booster Status</th>
													<th>Booster Links</th>		
													<th>Bank Details</th>
													<th>PayU Button</th>
													<th>Payout Mode Button</th>
													<th>Invoice Details</th>
													<th>Plan</th>
													<th>Dashboard Comment</th>
													<th>Edit Profile Fields</th>
													<th>Expire Month</th>
												</tr>	
											</thead>
											<tbody>
												<tr>													
													<td>
														<select class="form-control" name="status">
															<option <?php echo($user[0]->status==1)?'selected':''; ?> value='1'>Active</option>
															<option <?php echo($user[0]->status==0)?'selected':''; ?> value='0'>Inactive</option>
														</select>
													</td>													
													<td>
														<select class="form-control" name="boosterStatus">
															<option <?php echo($user[0]->boosterStatus==1)?'selected':''; ?> value='1'>Active</option>
															<option <?php echo($user[0]->boosterStatus==0)?'selected':''; ?> value='0'>Inactive</option>
														</select>
													</td>
                                                    <td>
														<input type="number" name="booster_task" value="<?php echo $user[0]->booster_task; ?>" placeholder="Booster Links" class="form-control" >
													</td>													
													<td>
														<select class="form-control"  name="bank_details">
															<option <?php echo($user[0]->bank_details==1)?'selected':''; ?>  value='1'>Approved</option>
															<option <?php echo($user[0]->bank_details==0)?'selected':''; ?>  value='0'>Disapproved</option>     
														</select>													
													</td>
													
													<td>
														<select class="form-control"  name="pay_botton">
															<option <?php echo($user[0]->pay_botton==1)?'selected':''; ?> value='1'>Visible</option>
															<option <?php echo($user[0]->pay_botton==0)?'selected':''; ?> value='0'>Hide</option>     
														</select>													
													</td>
													<td>
														<select class="form-control"  name="payout_button">
															<option <?php echo($user[0]->payout_button==1)?'selected':''; ?> value='1'>Unlock</option>
															<option <?php echo($user[0]->payout_button==0)?'selected':''; ?> value='0'>Lock</option>     
														</select>													
													</td>
													<td>
														<a class="btn btn-primary" href="<?php echo base_url('index.php/admin/user_invoice/'.$user[0]->id); ?>">Manage</a>
													</td>
													<td>
														<a  class="btn btn-primary" href="<?php echo base_url('index.php/admin/update_user_plan/'.$user[0]->id); ?>">Manage</a>
													</td>
													<td>
														<select class="form-control"  name="comment_section">
															<option <?php echo($user[0]->comment_section==1)?'selected':''; ?> value='1'>Unblock</option>
															<option <?php echo($user[0]->comment_section==0)?'selected':''; ?> value='0'>Block</option>     
														</select>													
													</td>
													<td>
														<select class="form-control"  name="profilelock">
															<option <?php echo($user[0]->profilelock==1)?'selected':''; ?> value='1'>Unblock</option>
															<option <?php echo($user[0]->profilelock==0)?'selected':''; ?> value='0'>Block</option>     
														</select>													
													</td>
													<td>
														<input type="number" name="expiremonths" value="<?php echo $user[0]->expiremonths; ?>"  class="form-control" />													
													</td>
												</tr>
											</tbody>
										</table>
										<button type="submit" name="updateProfileSetting" class="btn btn-info"> Update </button>
										</form>
										<form method="post" >
										<h2 class="panel-title pull-left">Today's Task Management</h2><br>
										<?php 
											if($user[0]->boosterused==1)
											{
												$tasklink = ($user[0]->linkPerDay+$user[0]->booster_task);
											}
											else
											{
												$tasklink =  $user[0]->linkPerDay;
											}
										?>
										<table class="table table-bordered table-striped table-condensed" >	
											<thead>
												<tr>
													<th>Today's Task</th>
													<th>Free View Tab</th>
													<th>Work Click amount</th>
													<th>Number of Clicks <strong>(Click Per Day : <?php echo $tasklink; ?>)</strong></th> 
												</tr>	
											</thead>
											<tbody>
												<tr>													
													<td>
														<select class="form-control" name="today_task">
															<option <?php echo($user[0]->today_task==1)?'selected':''; ?> value='1'>Visible</option>
															<option <?php echo($user[0]->today_task==0)?'selected':''; ?> value='0'>Hide</option>
														</select>
													</td>													
													<td>
														<select class="form-control" name="free_viewtab">
															<option <?php echo($user[0]->free_viewtab==1)?'selected':''; ?> value='1'>Visible</option>
															<option <?php echo($user[0]->free_viewtab==0)?'selected':''; ?> value='0'>Hide</option>
														</select>
													</td>
													<td>														
														<input type="number" name="clickprice" value="<?php echo $user[0]->clickprice; ?>" placeholder="Enter clicks" class="form-control" >
													</td>	 
													<td>
														<select class="form-control" style="width:13%;  display: inline-block;" name="points_action">
															<option value='inc'>+</option>
															<option value='dec'>-</option>     
														</select>
														<input type="number" name="click_task" value="" placeholder="Enter clicks" class="form-control" style="width: 60%;
    display: inline-block;">
													</td>	
												</tr>
											</tbody>
										</table>
										<button type="submit" name="updateTaskSetting" class="btn btn-info"> Update </button>
										</form>
										<form method="post">
											<h2 class="panel-title pull-left">Wallet Management</h2><br>
											<table class="table table-bordered table-striped table-condensed" >	
												<thead>
													<tr>
														<th>Wallet</th>
														<th>Send Money to Friend</th>
														<th>Send Money to Bank</th>														
													</tr>	
												</thead>
												<tbody>
													<tr>
														<td>
															<select class="form-control" name="wallet_display">
																<option <?php echo($user[0]->wallet_display==1)?'selected':''; ?> value='1'>Visible</option>
																<option <?php echo($user[0]->wallet_display==0)?'selected':''; ?> value='0'>Hide</option>
															</select>
														</td>
														<td>
															<select class="form-control" name="smf">
																<option <?php echo($user[0]->smf==1)?'selected':''; ?> value='1'>Visible</option>
																<option <?php echo($user[0]->smf==0)?'selected':''; ?> value='0'>Hide</option>
															</select>
														</td>													
														<td>
															<select class="form-control" name="smb">
																<option <?php echo($user[0]->smb==1)?'selected':''; ?> value='1'>Visible</option>
																<option <?php echo($user[0]->smb==0)?'selected':''; ?> value='0'>Hide</option>
															</select>
														</td>
													</tr>
												</tbody>
											</table>
											<button type="submit" name="updateWalletSetting" class="btn btn-info"> Update </button>
										</form>
										<form method="post">
										<h2 class="panel-title pull-left">My Work Payouts</h2><br>
										<table class="table table-bordered table-striped table-condensed" >	
											<thead>
												<tr>
													<th>My Work Payouts</th>
													<th>My Earnings</th>
													<th>Wallet Account Statement</th>
													<th>Download My Earnings</th> 
													<th>Download Wallet Statement</th> 
												</tr>	
											</thead>
											<tbody>
												<tr>													
													<td>
														<select class="form-control" name="workpayout">
															<option value='1' <?php echo($user[0]->workpayout==1)?'selected':''; ?> >Visible</option>
															<option value='0' <?php echo($user[0]->workpayout==0)?'selected':''; ?> >Hide</option>
														</select>
													</td>													
													<td>
														<select class="form-control" name="myearnings">
															<option value='1' <?php echo($user[0]->myearnings==1)?'selected':''; ?> >Visible</option>
															<option value='0' <?php echo($user[0]->myearnings==0)?'selected':''; ?> >Hide</option>
														</select>
													</td>													
													<td>														
														<select class="form-control" name="walletstatement">
															<option value='1' <?php echo($user[0]->walletstatement==1)?'selected':''; ?> >Visible</option>
															<option value='0' <?php echo($user[0]->walletstatement==0)?'selected':''; ?> >Hide</option>
														</select>
													</td>
													<td>
														<a target="_blank" class="btn btn-primary" href="<?php echo base_url('index.php/mis/download_myearnings/'.$user[0]->id); ?>">Download</a>
													</td>
													<td>
														<a target="_blank" class="btn btn-primary" href="<?php echo base_url('index.php/mis/download_myworkpayout/'.$user[0]->id); ?>">Download</a>
													</td>
												</tr>
											</tbody>
										</table>
										<button type="submit" name="updateWorkPayoutSetting" class="btn btn-info"> Update </button>
									</form>
									
									<form method="post">
										<h2 class="panel-title pull-left">Work History</h2><br>
										<table class="table table-bordered table-striped table-condensed" >	
											<thead>
												<tr>
													<th>Work History</th>
													<th></th>
													<th></th>
													<th></th> 	
												</tr>	
											</thead>
											<tbody>
												<tr>													
													<td width="15%">
														<select class="form-control" name="workHistory">
															<option value='1' <?php echo($user[0]->workHistory==1)?'selected':''; ?> >Visible</option>
															<option value='0' <?php echo($user[0]->workHistory==0)?'selected':''; ?> >Hide</option>
														</select>
													</td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											</tbody>
										</table>
										<button type="submit" name="updateWorkHistorySetting" class="btn btn-info"> Update </button>
									</form>
									<form method="post">
										<h2 class="panel-title pull-left"></h2><br>
										<table class="table table-bordered table-striped table-condensed" >	
											<thead>
												<tr>
													<th>My Network</th>
													<th>Promotional Income</th>
													<th>My Team</th>
													<th>My Bussiness</th>
                                                    <th>KYC</th> 													
												</tr>	
											</thead>
											<tbody>
												<tr>													
													<td>
														<div class="col-md-12">
															<div class="col-md-6">
																<select class="form-control" name="my_network">
																	<option value='1' <?php echo($user[0]->my_network==1)?'selected':''; ?> >Visible</option>
																	<option value='0' <?php echo($user[0]->my_network==0)?'selected':''; ?> >Hide</option>
																</select>
															</div>	
															<div class="col-md-6">
																<a class="btn btn-warning" target="_blank" href="<?php echo base_url('index.php/mis/my_network_mis/'.$user[0]->id); ?>">Download</a>
															</div>
														</div>		
													</td>
													<td>
														<div class="col-md-12">
															<div class="col-md-6">
																<select class="form-control" name="promotional_income">
																	<option value='1' <?php echo($user[0]->promotional_income==1)?'selected':''; ?> >Visible</option>
																	<option value='0' <?php echo($user[0]->promotional_income==0)?'selected':''; ?> >Hide</option>
																</select>
															</div>	
															<div class="col-md-6">
																<a target="_blank" class="btn btn-warning" href="<?php echo base_url('index.php/mis/promotionalincome_mis/'.$user[0]->id); ?>">Download</a>
															</div>
														</div>	
													</td>
													<td>
														<div class="col-md-12">
															<div class="col-md-6">
																<select class="form-control" name="myteam">
																	<option value='1' <?php echo($user[0]->myteam==1)?'selected':''; ?> >Visible</option>
																	<option value='0' <?php echo($user[0]->myteam==0)?'selected':''; ?> >Hide</option>															
																</select>
															</div>
															<div class="col-md-6">
																<a class="btn btn-warning" href="<?php echo base_url('index.php/admin/myteam/'.$user[0]->id); ?>">Manage</a>
															</div>
														</div>
													</td>
													<td>
														<div class="col-md-12">
															<div class="col-md-6">
																<select class="form-control" name="my_bussiness">
																	<option value='1' <?php echo($user[0]->my_bussiness==1)?'selected':''; ?> >Visible</option>
																	<option value='0' <?php echo($user[0]->my_bussiness==0)?'selected':''; ?> >Hide</option>
																</select>
															</div>
															<div class="col-md-6">
																<a class="btn btn-warning" href="<?php echo base_url('index.php/admin/mybussiness/'.$user[0]->id); ?>">Manage</a>
															</div>
														</div>		
													</td>
													<td>
														 <select class="form-control" name="kyc_status">
															<option value='1' <?php echo($user[0]->kyc_status==1)?'selected':''; ?> >Approved</option>
															<option value='0' <?php echo($user[0]->kyc_status==0)?'selected':''; ?> >Unapproved</option>
														</select>																
													</td>
												</tr>
											</tbody>
										</table>
										<button type="submit" name="updatemyNetworkSetting" class="btn btn-info"> Update </button>
									</form>
								</div>
							</div>
						</section>                         
						</div>
					</div>					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>	
		
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>	

	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>