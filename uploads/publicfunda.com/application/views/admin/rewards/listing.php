 <?php $this->load->view('admin/layout/header'); ?>
<style>
.material-icons{
	font-size: 14px;
}
</style>
			<div class="inner-wrapper pt-30">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Rewards</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php //echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Rewards</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">	
						<div class="col-md-12"> <br>
							<?php echo $this->session->flashdata('message'); ?> 							
							<section class="panel">								
							<div class="panel-body">								
								<div class="table-responsive">
								<a href="<?php echo base_url('index.php/rewards/add/'.$type); ?>" class="mb-xs mt-xs mr-xs btn btn-primary pull-right">Add New</a>	
								<a href="<?php echo base_url('index.php/rewards/listing/'); ?>/<?php echo ($type=='common')?'special':'common'; ?>" class="mb-xs mt-xs mr-xs btn btn-primary pull-right">Manage <?php echo ($type=='common')?'Special':''; ?> Rewards</a>	
                                 
									<table class="table table-bordered table-striped" id="datatable-default">									
							<thead>
								<tr class="gradeX">
									<th>SNo.</th>
									<?php if($type=='special'){ ?>
									<th>USERID</th>
									<?php } ?>
									<th>Title</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							 <?php $i=0; foreach($bookdata as $data){ $i++; ?>
								<tr class="gradeX">
									<td><?php echo  $i; ?></td>
									<?php if($type=='special'){ ?>
                                    <td><?php echo $data->user_id; ?></td> 
									<?php } ?>
									<td><?php echo $data->title; ?></td>
									<td><?php echo($data->status==1)?'<i data="2" class="status_checks btn btn-success">Active</i>':'<i data="2" class="status_checks btn btn-danger">Inactive</i>'; ?></td>
									<td>
										<a href="<?php echo base_url('index.php/rewards/edit/'.$data->id.'/'.$type); ?>" title="edit record"><i class="fa fa-pencil"></i></a>
										<a href="<?php echo base_url('index.php/rewards/deleteRecord/'.$data->id.'/'.$type); ?>" title="delete record"><i class="fa fa-trash-o"></i></a>
									</td> 
								</tr>
								<?php } ?>																				
							</tbody>
						</table>
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>
	


		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
	


		
		
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>