 <?php $this->load->view('admin/layout/header'); ?>
<style>
.material-icons{
	font-size: 14px;
}
</style>
			<div class="inner-wrapper pt-30">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Helpdesk Department</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php //echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Department</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">	
						<div class="col-md-12"> <br>
							<?php echo $this->session->flashdata('message'); ?> 							
							<section class="panel">								
							<div class="panel-body">	
								<div class="table-responsive">                                 
									<table class="table table-bordered table-striped" id="datatable-default">							
								<form method="post">
                                <tbody>
                                    <tr>
										<td>Title</td>
                                        <td><input type="text" name="title" class="form-control" value="" required></td>										
                                    </tr>
									<tr>
										<td>Executive Name</td>
                                        <td><input type="text" name="name" class="form-control" value="" required></td>										
                                    </tr>
									 <tr>
										<td>email</td>
                                        <td><input type="text" name="email" class="form-control" value="" required></td>										
                                    </tr>
									 <tr>
										<td>Status</td>
                                        <td>
											<select name="status" class="form-control" >
												<option value='1'>Active</option>
												<option value='0'>Inactive</option>
											</select>
										</td>										
                                    </tr>
									<tr>
										<td></td>
                                        <td>
											<button type="submit" name="updateData" class="btn btn-info"> Update </button>
										</td>										
                                    </tr>
                                                                     
                                </tbody>
								</form>
						</table>
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>
	


		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
	


		
		
	</body>

</html>