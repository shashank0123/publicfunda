 <?php $this->load->view('admin/layout/header'); ?>
 <style>
.inactiveMyTab{display:none;}
.activeMyTab{display:block;}
.mypointclass{display:inline !important; width:20%;}
.myclass{margin-right:5px;}
</style>
<?php
$totalPoints = $userdata[0]->totalPoints+$userdata[0]->paidPoints;
if(count($campaignData)>0)
{
	$tcp_cal_1 = 0; 
	foreach($campaignData as $campData)
	{
		$tcp_cal_1 +=$campData->campaignPoints; 
	}
	$tcp_cal = $totalPoints-$tcp_cal_1;
}
else
{
	$tcp_cal = $totalPoints;
}
?>
			<div class="inner-wrapper pt-30">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					
					<header class="page-header">
						<h2>Manage Campaign</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="#">
										<i class="fa fa-home"></i>
									</a>
								</li>
								
								<li><a href=""><span>Manage Campaign</span></a></li>
							</ol>
					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12">
							<?php echo $this->session->flashdata('message'); ?> 
							<div class="table-responsive">
										<h5>
											<strong class="btn btn-warning">USERID : <?php echo $userdata[0]->id; ?></strong>
											<strong class="btn btn-info">Total E-Ponts : <?php echo ($userdata[0]->totalPoints+$userdata[0]->paidPoints); ?></strong>
											<strong class="btn btn-info">Left E-Ponts : <?php echo $tcp_cal; ?></strong>
										</h5>
											<table class="table table-bordered table-striped table-condensed mb-none">
												<thead>
													<tr>
														<th>SNo.</th>
														<th>Campaign Name</th>
														<th>Total E-Ponts</th>
														<th>Total Recieved Clicks</th>
														<th>Starting Date</th>
														<th>Campaign Status</th>
													</tr>
												</thead>
												<tbody>	
													<?php $snj =0; $campaignInfoTotalPoint=0; $campaignInfoClickedPoint=0; foreach($campaignData as $campaignDataStructure){ $snj++; ?>
													<?php 
														$campaignInfoTotalPoint +=$campaignDataStructure->campaignPoints;
														$campaignInfoClickedPoint +=$campaignDataStructure->recievedclicked;
													?>
													<tr>
														<td><?php echo $snj; ?></td>
														<td><?php echo $campaignDataStructure->campaignTitle; ?></td>
														<td><?php echo $campaignDataStructure->campaignPoints; ?></td>
														<td><?php echo $campaignDataStructure->recievedclicked; ?></td>
														<td><?php echo $campaignDataStructure->sdate; ?></td>
														<td>
															<?php if($campaignDataStructure->campaignStatus==1){ ?>
															<a href="<?php echo base_url('index.php/admin/update_campaign_status/inact/'.$campaignDataStructure->id.'/'.$userdata[0]->id); ?>" class="btn btn-success btn-xs"><b>Active</b></a>
															<?php }else{ ?> 
															<a href="<?php echo base_url('index.php/admin/update_campaign_status/act/'.$campaignDataStructure->id.'/'.$userdata[0]->id); ?>" class="btn btn-danger btn-xs"><b>Inactive</b></a>
															<?php } ?> 
														</td>
													</tr>
													<?php } ?>
													<tr>
														<td></td>
														<td>Grand Total</td>
														<td><?php echo $campaignInfoTotalPoint; ?></td>
														<td><?php echo $campaignInfoClickedPoint; ?></td>
														<td></td>
														<td></td>
													</tr>
												</tbody>
											</table>
											<br>
										</div>
										
							<div class="table-responsive">
											<h2 class="panel-title">Campaign Status</h2><br>
											<div class="form-group">													
													<div class="col-md-5">
														<div class="input-daterange input-group" data-plugin-datepicker="">
															<input type="hidden" name="cuserid" value="1001">
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="startDate" id="sd">
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="endDate" id="ed">
														</div>
													</div>
                                                    
                                                    <div class="col-md-3">
														<select class="form-control mb-md" name="cid" id="camID">
															<option value="all">All Campaign</option>
																	<option value="3">Campaign Title test</option>
																	<option value="6">Campaign Title 2</option>
																	<option value="7">Campaign Title 5</option>
																	<option value="8">Campaign Title</option>
																	<option value="14">Campaign Title</option>
															</select>                                                        
													</div>
                                                    <div class="col-md-4">
														<button type="button" class="btn btn-primary" onclick="return ckeckcampaignstatus();">Submit</button>
														<button type="reset" class="btn btn-default">Download</button>
													</div>
												</div>
											<table class="table table-bordered table-striped table-condensed mb-none">
												<thead>
													<tr>
														<th>SNo.</th>
														<th>Campaign Name</th>
														<th>Total E-Ponts</th>
														<th>Total Recieved Clicks</th>
														<th>Starting Date</th>
														<th>Campaign Status</th>
													</tr>
												</thead>
												<tbody>	
													<?php $snj =0; $campaignInfoTotalPoint=0; $campaignInfoClickedPoint=0; foreach($campaignData as $campaignDataStructure){ $snj++; ?>
													<?php 
														$campaignInfoTotalPoint +=$campaignDataStructure->campaignPoints;
														$campaignInfoClickedPoint +=$campaignDataStructure->recievedclicked;
													?>
													<tr>
														<td><?php echo $snj; ?></td>
														<td><?php echo $campaignDataStructure->campaignTitle; ?></td>
														<td><?php echo $campaignDataStructure->campaignPoints; ?></td>
														<td><?php echo $campaignDataStructure->recievedclicked; ?></td>
														<td><?php echo $campaignDataStructure->sdate; ?></td>
														<td>
															<?php if($campaignDataStructure->campaignStatus==1){ ?>
															<a href="<?php echo base_url('index.php/admin/update_campaign_status/inact/'.$campaignDataStructure->id.'/'.$userdata[0]->id); ?>" class="btn btn-success btn-xs"><b>Active</b></a>
															<?php }else{ ?> 
															<a href="<?php echo base_url('index.php/admin/update_campaign_status/act/'.$campaignDataStructure->id.'/'.$userdata[0]->id); ?>" class="btn btn-danger btn-xs"><b>Inactive</b></a>
															<?php } ?> 
														</td>
													</tr>
													<?php } ?>
													<tr>
														<td></td>
														<td>Grand Total</td>
														<td><?php echo $campaignInfoTotalPoint; ?></td>
														<td><?php echo $campaignInfoClickedPoint; ?></td>
														<td></td>
														<td></td>
													</tr>
												</tbody>
											</table>
											<br>
										</div>			
						
						    <?php /*
							<div class="tabs tabs-success">
								<ul class="nav nav-tabs">
									<?php $i=0; foreach($campaignData as $campData){ $i++; ?>
									<li class="<?php echo($i==1)?'active':''; ?>">
										<a href="#campaigntab<?php echo $campData->id ?>" data-toggle="tab">Campagin <?php echo $i; ?></a>
									</li>
									<?php } ?>	                                   									
								</ul>
								<div class="tab-content">
									<?php $j=0; foreach($campaignData as $campData){ $j++; ?>
									<div id="campaigntab<?php echo $campData->id ?>" class="tab-pane <?php echo($j==1)?'active':''; ?>">
										<form class="form-horizontal" method="post">
														<input type="hidden" value="<?php echo base64_encode($campData->id); ?>" name="cid" >
														<div id="campaignForm" >
														<fieldset>
															<div class="form-group">
																<label class="col-md-3 control-label" for="profileFirstName">Campaign*</label>
																<div class="col-md-8">
																	<select class="form-control" name="campaign">
																		<option value="Inorganic Visits">Inorganic Visits</option>
																	</select>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label" for="profileFirstName">Campaign Type*</label>
																<div class="col-md-8">
																	<select name="campaignType" class="form-control">
																		<option <?php echo($campData->campaignType=="Individual Campaign Type")?'selected':''; ?> value="Individual Campaign Type">Individual Campaign Type</option>
																		<option <?php echo($campData->campaignType=="Business Campaign Type")?'selected':''; ?> value="Business Campaign Type">Business Campaign Type</option>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-3 control-label" >Campaign Title*</label>
																<div class="col-md-8">
																	<input type="text" name="campaignTitle" value="<?php echo $campData->campaignTitle; ?>" class="form-control" data-validation="required">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-3 control-label" >Campaign Link *</label>
																<div class="col-md-8">
																	<input type="text" name="campaignUrl" <?php echo($campData->campaignStatus==1)?'readonly':''; ?> value="<?php echo $campData->campaignUrl; ?>" class="form-control" data-validation="required">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-3 control-label" >Points</label>
																<div class="col-md-8">  
																	<span id="updatecerror<?php echo $campData->id; ?>"></span>
																	<input type="text" name="oldctp" class="form-control mypointclass" value="<?php echo $campData->campaignPoints; //$tcp_cal; ?>" readonly >
																	<input type="text" name="campaignPoints" id="mypinput<?php echo $campData->id; ?>" value="<?php echo 0; ?>" class="form-control mypointclass" onkeyup="return checkPoints(this.value,<?php echo $tcp_cal; ?>,'mypinput','updcambtm<?php echo $campData->id; ?>','updatecerror<?php echo $campData->id; ?>');" required>
																</div>
															</div>
															<div class="form-group" >
																<label class="col-md-3 control-label" >Campaign Date*</label>
																<div class="col-md-8">															
																
																	<div class="col-md-6"><strong>Start Date</strong><input type="text" name="sdate" value="<?php echo $campData->sdate; ?>" class="form-control" readonly></div>
																	<div class="col-md-6"><strong>End Date</strong><input type="text" value="<?php echo $campData->edate; ?>" name="edate" class="form-control" readonly></div>
																													
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-3 control-label" >Age Limit*</label>
																<div class="col-md-8">
																	<div class="input-daterange input-group" >
																	<span class="input-group-addon">
																		From
																	</span>
																	<select class="form-control" name="sage" style="width:100%" onchange="return setAgeRange(this.value,'eage<?php echo $campData->id; ?>');" data-validation="required">
																		<?php for($sa=18; $sa<=80; $sa++){ ?>
																		<option <?php echo($campData->sage==$sa)?'selected':''; ?>  value="<?php echo $sa; ?>"><?php echo $sa; ?> Years</option>
																		<?php } ?>
																	</select>
																	<span class="input-group-addon">To</span>
																	<select class="form-control" name="eage" style="width:100%" id="eage<?php echo $campData->id; ?>" data-validation="required">
																		<option  value="<?php echo $campData->eage; ?>"><?php echo $campData->eage; ?> Years</option>
																	</select>
																</div>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label" >Gender*</label>
																<div class="col-md-8">
																	<select class="form-control" name="gender">
																	<option <?php echo($campData->gender=='Male')?'selected':''; ?> value="Male">Male</option>
																	<option <?php echo($campData->gender=='Female')?'selected':''; ?> value="Female">Female</option>
																	<option <?php echo($campData->gender=='Both')?'selected':''; ?> value="Both">Both</option>
																</select>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label" >Profession*</label>
																<div class="col-md-8">
																	<select name="profession" class="form-control">
																		<option value="">Select Profession</option>
																		<option <?php echo($campData->profession=='Accountant')?'selected':''; ?> value="Accountant">Accountant</option>
																		<option <?php echo($campData->profession=='Agricultural Advisor')?'selected':''; ?> value="Agricultural Advisor">Agricultural Advisor</option>
																		<option <?php echo($campData->profession=='All')?'selected':''; ?> value="All">All</option>
																		<option <?php echo($campData->profession=='Animator')?'selected':''; ?> value="Animator">Animator</option>
																		<option <?php echo($campData->profession=='Archeologist')?'selected':''; ?> value="Archeologist">Archeologist</option>
																		<option <?php echo($campData->profession=='Architect')?'selected':''; ?> value="Architect">Architect</option>
																		<option <?php echo($campData->profession=='Art Photographer')?'selected':''; ?> value="Art Photographer">Art Photographer</option>
																		<option <?php echo($campData->profession=='Athlete (Sportsman/Woman)')?'selected':''; ?> value="Athlete (Sportsman/Woman)">Athlete (Sportsman/Woman)</option>
																		<option <?php echo($campData->profession=='Business Person')?'selected':''; ?> value="Business Person">Business Person</option>
																		<option <?php echo($campData->profession=='Employed')?'selected':''; ?> value="Employed">Employed</option>
																		<option <?php echo($campData->profession=='Entrepreneur')?'selected':''; ?> value="Entrepreneur">Entrepreneur</option>
																		<option <?php echo($campData->profession=='Hotel Manager')?'selected':''; ?> value="Hotel Manager">Hotel Manager</option>
																		<option <?php echo($campData->profession=='House Wife')?'selected':''; ?> value="House Wife">House Wife</option>
																		<option <?php echo($campData->profession=='Other')?'selected':''; ?> value="Other">Other</option>
																		<option <?php echo($campData->profession=='Self Employed')?'selected':''; ?> value="Self Employed">Self Employed</option>
																		<option <?php echo($campData->profession=='Student')?'selected':''; ?> value="Student">Student</option>
																	</select>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label">Industry*</label>
																<div class="col-md-8">
																	<input type="text" readonly class="form-control" value="<?php echo $campData->industry; ?>" name="industry" placeholder="Enter Industry">
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label">Select State*</label>
																<div class="col-md-8"> 
																	<select class="form-control" name="state" onchange="return getCityList(this.value,'citydata<?php echo $campData->id; ?>');">
																		<?php foreach($this->user_model->selectAllState() as $statedata){ ?>
																			<option <?php echo($campData->state==$statedata->id)?'selected':''; ?> value="<?php echo $statedata->id; ?>"><?php echo $statedata->state; ?></option>
																		<?php } ?>    
																	</select>
																</div>
															</div>
															<?php 
																$cityData = $this->user_model->selectCityById($campData->city);
																if(count($cityData)>0)
																{
																	$selecttedCity = "<option selected value='".$cityData[0]->id."'>".$cityData[0]->city_name."</option>";
																}
																else
																{
																	$selecttedCity ="";
																}	
															?>
															<div class="form-group">
																<label class="col-md-3 control-label">City*</label>
																<div class="col-md-8">
																	<select class="form-control" name="city" id="citydata<?php echo $campData->id; ?>" data-validation="required">
																		<option value="">-- Select City --</option>
																		<?php echo $selecttedCity; ?>
																	</select>
																</div>
															</div>															
																
														</fieldset>

												<div class="panel-footer">
													<div class="row">
														<div class="col-md-9 col-md-offset-4">
															<button type="submit" name="updateCampagn" id="updcambtm<?php echo $campData->id; ?>" class="btn btn-default">Update Campagin</button>
														</div>
													</div>
												</div>
												</div>
												</form>
									</div>
									<?php } ?>
								</div>
							</div>	
								*/ ?>
						</div>
					</div>
				</section>
			</div>			
			
		</section>
			
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		
	<script>
  $.validate({
    lang: 'en'
  });
  
 
function setAgeRange(VAL,SELCTID)
{
	if(VAL!="")
	{
	  var options="";
	  var startOption = VAL;
      var endOption=80;
      for(var i=startOption;i<=endOption;i++)
      {
			if(i!=VAL)
			{
				 options+="<option value='"+i+"'>"+i+" Years</option>";
			}
      }       
      $("#"+SELCTID).html(options);  
	}	
}
function checkPoints(POINTSVAL,TOTALPOINTS,INPUTID,BTNID,ERRORDIV)
{
	if(POINTSVAL>0)
	{
		if(POINTSVAL<=TOTALPOINTS)
		{
			$('#'+INPUTID).css({"border": "1px solid #ccc"});
			$('#'+BTNID).removeAttr("disabled", 'disabled');
			$('#'+ERRORDIV).html('');
		}
		else
		{
			$('#'+INPUTID).css({"border": "2px solid #D2312D"});
			$('#'+BTNID).attr("disabled", 'disabled');
			$('#'+ERRORDIV).html('<div class="alert alert-danger"><strong>Invalid points !</strong> please check your total e-points</div>');
		}	
	}
}
  
  function getCityList(SID,CITYDIVID)
  {
	if(SID!="")
	{ 
		jQuery.ajax({
            url: "<?php echo base_url('index.php/user/getCityByStateId/'); ?>",
            data:'SID='+SID,
            type: "GET",			
            success:function(mydata)
			{			
				$('#'+CITYDIVID).html(mydata); //alert('error');
			},
            error:function (mydata){
				alert(mydata);
			}
        });
		
	}	  
  }
  
  
function addCampaign()
{
	$('#campaignForm').slideToggle(100);
}




</script>


	
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>