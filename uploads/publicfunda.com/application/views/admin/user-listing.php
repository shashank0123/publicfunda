 <?php $this->load->view('admin/layout/header'); ?>
			<div class="inner-wrapper pt-30">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Users</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/admin/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Users</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">
						<table class="table table-bordered table-striped"  id="datatable-tabletools">						
							<thead>
								<tr class="gradeX">
									<th>SNo.</th>
									<th>USERID</th>
									<th>Name</th>
									<th>email</th>
									<th>Pan Card</th>
									<th>Contact No</th>
									<th>sponsor_id</th>
									<th>Plan</th>
									<th>Payout Mode</th>
									<th>Place</th> 
									<th>Purchase Points</th>
									<!-- <th>used points </th> -->
									<th>Account Status</th>
									<th>Booster Status</th>
									<th>Campaign Status</th>									
									<th>MIS</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							 <?php $j=0; foreach($USER as $USER){ $j++; ?>
							 <?php 
								$used_points = 0;
								if(count($this->campaign_model->selectCampaignByUserID($USER->id))>0)
								{	$cstatus  = 'Inactive';
									$cstatus  = 0;
									foreach($this->campaign_model->selectCampaignByUserID($USER->id) as $campaignData1)
									{
										$used_points += $campaignData1->campaignPoints;
									}
									foreach($this->campaign_model->selectCampaignByUserID($USER->id) as $campaignData1)
									{
										$cstatus  += $campaignData1->campaignStatus;
									}
								}		
							 ?>
							
								<tr class="gradeX">
									<td><?php echo $j;?></td>
									<td><?php echo $USER->id;?></td>
									<td><?php echo $USER->name;?></td>
									<td><?php echo $USER->email;?></td>
									<td><?php echo $USER->pancard_no;?></td>
									<td><?php echo $USER->contact_no;?></td>
									<td><?php echo $USER->sponsor_id;?></td>
									<td><?php echo $USER->plan;?></td>
									<td><?php echo $USER->payoutmode;?></td>
									<td><?php echo $USER->city; ?></td>
									<td><?php echo $USER->paidPoints;?></td>
									<!--<td><?php //echo $used_points; ?></td> -->
									<td><i class="status_checks btn <?php echo ($USER->status==1)? 'btn-success' : 'btn-danger'?>"><?php echo ($USER->status==1)? 'Active' : 'Inactive'?></i></td>
									<td><i class="status_checks btn btn-danger">Inactive</i></td>
									<td><?php echo ($cstatus!=0)? 'Active' : 'Inactive'?></td>
									<td class="actions">										
										<a href="<?php echo base_url('index.php/mis/user_mis/'.$USER->id); ?>"><button class="mb-xs mt-xs mr-xs btn btn-primary" title="Download MIS Report"><i class="fa fa-download"></i></button></a>
									</td>
									<td class="actions">										
										<a target="_blank" href="<?php echo base_url('index.php/admin/user_ligin/'.$USER->id); ?>"><button class="mb-xs mt-xs mr-xs btn btn-info" title="Login user"><i class="fa fa-sign-in"></i></button></a>
										<a href="<?php echo base_url('index.php/admin/user_setting/'.$USER->id); ?>"><button class="mb-xs mt-xs mr-xs btn btn-warning" title="Settings"><i class="fa fa-cog"></i></button></a> 
									</td>
								</tr>
								<?php } ?>																				
							</tbody>
						</table>
					</div>						
				</section>
			</div>
		</section>
			
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		
		<script src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
				
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.tabletools.js"></script>
	
	</body>

</html>