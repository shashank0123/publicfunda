 <?php $this->load->view('admin/layout/header'); ?>
<style>
.mydiv .col-md-3{padding-bottom:20px;}
</style>
<?php 
$admin_login_id = $this->session->userdata('ADMINID'); 
$admin_data = $this->admin_model->selectAdminById($admin_login_id);
if($admin_data[0]->type==2)
{
	$module_array = explode(",",$admin_data[0]->module);
	$dpromotionalincome = (in_array('dpromotionalincome', $module_array))?'1':'0';
	$downloadalluserinvoise = (in_array('downloadalluserinvoise', $module_array))?'1':'0';
//$module_array = explode(",",$admin_data[0]->module);
	$users = (in_array('users', $module_array))?'1':'0';
	$plan = (in_array('plan', $module_array))?'1':'0';
	$rewards = (in_array('rewards', $module_array))?'1':'0';
	$helpdesk = (in_array('helpdesk', $module_array))?'1':'0';
	$bookadd = (in_array('bookadd', $module_array))?'1':'0';
	$purchasecalculator = (in_array('purchasecalculator', $module_array))?'1':'0';
	$bookingcalculator = (in_array('bookingcalculator', $module_array))?'1':'0';
	$banner = (in_array('banner', $module_array))?'1':'0';
	$dashboard = (in_array('dashboard', $module_array))?'1':'0';
	$staticblock = (in_array('staticblock', $module_array))?'1':'0';
	$workhistory = (in_array('workhistory', $module_array))?'1':'0';
	$comments = (in_array('comments', $module_array))?'1':'0';
	$invoicecontent = (in_array('invoicecontent', $module_array))?'1':'0';
	$alltransactiondetails = (in_array('alltransactiondetails', $module_array))?'1':'0';
	$managepromotionalincome = (in_array('managepromotionalincome', $module_array))?'1':'0';
	$tax_settings = (in_array('tax_settings', $module_array))?'1':'0';
	$managewalletstatement = (in_array('managewalletstatement', $module_array))?'1':'0';
	$popup = (in_array('popup', $module_array))?'1':'0';
	$trackadminwork = (in_array('trackadminwork', $module_array))?'1':'0';
    $useractivationpanel= (in_array('useractivationpanel', $module_array))?'1':'0';
	$campaignreport= (in_array('campaignreport', $module_array))?'1':'0';
	$salesreport = (in_array('salesreport', $module_array))?'1':'0'; 
}
else
{
	$module_array = 1;
	$dpromotionalincome = 1;	
	$downloadalluserinvoise = 1;
		//$module_array = 1;
	$users = 1;
	$plan = 1;
	$rewards = 1;
	$helpdesk = 1;
	$bookadd = 1;
	$purchasecalculator = 1;
	$bookingcalculator = 1;
	$banner = 1;
	$dashboard = 1;
	$staticblock = 1;
	$workhistory = 1;
	$comments = 1;
	$invoicecontent = 1;
	$alltransactiondetails = 1;
	$managepromotionalincome = 1;
	$tax_settings = 1;
	$managewalletstatement = 1;	
	$popup = 1;
	$trackadminwork =1;
    $useractivationpanel= 1; 
	$campaignreport= 1;		
	$salesreport= 1;
}
//print_r($admin_data);
?>
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Dashboard</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/admin/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Dashboard</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">
						<div class="col-md-6 col-lg-12 col-xl-12">
							<div class="row mydiv">  </br></br>                          
								<?php if($dpromotionalincome==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/promotionalincome/index'); ?>" class="btn btn-info" target="_blank">
										Distribute Promotional Income
									</a>
								</div> 
								<?php } ?>
								<?php if($downloadalluserinvoise==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/downloadallinvoice'); ?>" class="btn btn-info" target="_blank">
										Download All User Invoice
									</a>
								</div> 
								<?php } ?>
								<?php if($campaignreport==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/managecampaignreport'); ?>" class="btn btn-info" target="_blank">
										Manage Campaign Report
									</a>
								</div> 
								<?php } ?>
								
								<?php if($salesreport==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/salesreport'); ?>" class="btn btn-info" target="_blank">
										Sponsor ID Sales Report
									</a>
								</div> 
								<?php } ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/salesbonuscalculator'); ?>" class="btn btn-info" target="_blank">
										Sales Bonus Calculator
									</a>
								</div> 
                                <?php if($trackadminwork==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/trackadminwork/listing'); ?>" class="btn btn-info">
										Trace Sub Admin
									</a>
								</div> 
								<?php } ?>						
								<?php if($users==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/list_user'); ?>" class="btn btn-info" >
										Manage Users
									</a>
								</div> 
								<?php } ?>
								<?php if($useractivationpanel==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/useractivationpanel'); ?>" class="btn btn-info" >
										Manage Users Activation Panel
									</a>
								</div> 
								<?php } ?>
								<?php if($plan==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/list_plans'); ?>" class="btn btn-info" >
										Manage Plan
									</a>
								</div> 
								<?php } ?>
								<?php if($rewards==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/rewards/listing/common'); ?>" class="btn btn-info" >
										Manage Rewards
									</a>
								</div> 
								<?php } ?>
								<?php if($helpdesk==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/helpdesk/listing'); ?>" class="btn btn-info" >
										Manage Helpdesk
									</a>
								</div> 
								<?php } ?>
								<?php if($bookadd==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/bookadd/listing'); ?>" class="btn btn-info" >
										Book Your Add
									</a>
								</div> 
								<?php } ?>
								<?php if($purchasecalculator==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/purchasecalculator/listing'); ?>" class="btn btn-info" >
										Purchase Calculator
									</a>
								</div> 
								<?php } ?>
								<?php if($bookingcalculator==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/bookingcalculator/listing'); ?>" class="btn btn-info" >
										Book Your Add Calculator
									</a>
								</div> 
								<?php } ?>
								<?php if($banner==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/banner/listing'); ?>" class="btn btn-info" >
										Manage Slider
									</a>
								</div> 
								<?php } ?>
								<?php if($dashboard==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/dashboard/listing'); ?>" class="btn btn-info" >
										Manage Dashboard
									</a>
								</div> 
								<?php } ?>
								<?php if($staticblock==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/staticblock/listing'); ?>" class="btn btn-info" >
										Manage Static Block
									</a>
								</div> 
								<?php } ?>
								<?php if($workhistory==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/workhistory'); ?>" class="btn btn-info" >
										Manage Work History
									</a>
								</div> 
								<?php } ?>
								<?php if($comments==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/comments/listing'); ?>" class="btn btn-info" >
										Manage Comments
									</a>
								</div> 
								<?php } ?>
								<?php if($invoicecontent==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/invoicecontent/edit'); ?>" class="btn btn-info" >
										Manage Invoice Content
									</a>
								</div> 
								<?php } ?>
								<?php if($alltransactiondetails==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/alltransactiondetails'); ?>" class="btn btn-info" >
										Manage Transaction Details
									</a>
								</div> 
								<?php } ?>
								<?php if($managepromotionalincome==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/managepromotionalincome'); ?>" class="btn btn-info" >
										Manage Promotional Income
									</a>
								</div> 
								<?php } ?>
								<?php if($popup==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/popup/listing'); ?>" class="btn btn-info" >
										Manage Popup
									</a>
								</div> 
								<?php } ?>
								<?php if($tax_settings==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/tax_settings'); ?>" class="btn btn-info" >
										Manage Settings
									</a>
								</div> 
								<?php } ?>
								<?php if($managewalletstatement==1){ ?>
								<div class="col-md-3">
									<a href="<?php echo base_url('index.php/admin/managewalletstatement'); ?>" class="btn btn-info" >
										Download Wallet Statement
									</a>
								</div> 
								<?php } ?>
                            </div>
						</div>
					</div>				
					
				</section>
			</div>			
			
		</section>
	
		
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
		
		<!-- crop image -->
		<!--<script type="text/javascript" src="<?php //echo base_url(); ?>assets/front/img-crop/js/jquery.min.js"></script>-->
		<script src="<?php echo base_url(); ?>assets/front/img-crop/js/jquery.Jcrop.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/img-crop/js/script-crop.js"></script>
		
		
	
	<!-- The popup for upload new photo -->
    <div id="popup_upload" style="display:none;">
        <div class="form_upload">
            <span class="close" onclick="close_popup('popup_upload')">x</span>
            <h2>Upload photo</h2>
            <form action="<?php echo base_url('index.php/user/imagecropsystem/'); ?>" method="post" enctype="multipart/form-data" target="upload_frame" onsubmit="submit_photo()">
                <input type="file" name="photo" id="photo" class="file_input">
                <div id="loading_progress"></div>
                <input type="submit" value="Upload photo" id="upload_btn">
            </form>
            <iframe name="upload_frame" class="upload_frame"></iframe>
        </div>
    </div>

    <!-- The popup for crop the uploaded photo -->
    <div id="popup_crop" style="display:none;">
        <div class="form_crop">
            <span class="close" onclick="close_popup('popup_crop')">x</span>
            <h2>Crop photo</h2>
            <!-- This is the image we're attaching the crop to -->
            <img id="cropbox" />            
            <!-- This is the form that our event handler fills -->
            <form>
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <input type="hidden" id="photo_url" name="photo_url" />
                <input type="button" value="Crop Image" id="crop_btn" onclick="crop_photo(800,500)" />
            </form>
        </div>
    </div>	
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>