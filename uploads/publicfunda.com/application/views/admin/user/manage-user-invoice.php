 <?php $this->load->view('admin/layout/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/style-crop.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/jquery.Jcrop.min.css" type="text/css" />
			<div class="inner-wrapper pt-30">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Users Invoice</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/admin/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Users Invoice</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('message'); ?>
							<a href="<?php echo base_url('index.php/admin/createuserinvoice/'); ?>" class="mb-xs mt-xs mr-xs btn btn-warning pull-right" title="Settings">Create Invoice</a>
						
						
						<div class="panel-body">
								<form class="form-horizontal" method="post">
									<h4 class="mb-xlg"></h4>
									<fieldset>												
										<div class="form-group">													
											<div class="col-md-6">
												<div class="input-daterange input-group" data-plugin-datepicker="">
													<span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
													<input type="text" class="form-control" name="startDate" placeholder="start Date" value="<?php echo(isset($_POST['startDate']))?$_POST['startDate']:''; ?>" required>
													<span class="input-group-addon">to</span>
													<input type="text" class="form-control" name="endDate" placeholder="end Date" value="<?php echo(isset($_POST['endDate']))?$_POST['endDate']:''; ?>" required>                                                        
												</div>
											</div>
											<div class="col-md-1">
USERID 
											</div>
											<div class="col-md-2">
<input type="text" class="form-control" name="user_id" placeholder="Enter USERID" value="<?php echo(isset($_POST['user_id']))?$_POST['user_id']:''; ?>" >
											</div>
                                            <div class="col-md-2">
												<button type="submit" name="checkComments" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
											</div>
										</div>                                                  
									</fieldset>
								</form>
							</div>	
						
						</div>
					</div>	
					<div class="row">
						
						<table class="table table-bordered table-striped" id="datatable-default">						
							<thead>
								<tr class="gradeX">
									<th>SNo.</th>
									<th>USERID</th>
									<th>SPONSOR ID</th>
									<th>Admin Name</th>
									<th>Invoice Type</th>
									<th>Invoice ID</th>
									<th>Transaction ID</th>
									<th>Net Amount</th>
									<th>Tax</th>																		
									<th>Total Amount</th>	
                                    <th>Create Date</th>									
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php $amount = 0; $tax = 0; $finalAmount=0; ?>
							 <?php  foreach($invData as $inv){ 
								$amount += $inv->amount;
								$tax += appytax($inv->tax,$inv->amount); //$inv->tax;
								$finalAmount += $inv->finalAmount;
								 } ?>
								<tr class="gradeX">
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td><i class="fa fa-rupee"></i><?php echo $amount?></td>
									<td><?php echo $tax;?></td>
									<td><i class="fa fa-rupee"></i><?php echo $finalAmount;?></td>	
                                    <td></td>									
									<td class="actions">

									</td>
								</tr>
							<?php $j=0; foreach($invData as $inv){ $j++; ?>
							<?php $adminid = $this->trackadminwork_model->selecttrackadminworkByInvoiceID($inv->invoiceID); 
							$userdata1 = $this->user_model->selectUserByID($inv->user_id);
							  
							  if(count($adminid)>0)
							  {
								  $admin_data = $this->admin_model->selectAdminById($adminid[0]->admin_id); 
								  $admin = $admin_data[0]->username;
							  }  else {	 $admin =  "admin";  }							  
							  error_reporting(0);
							if($userdata1[0]->direct_id=='NULL')
							{ $sname = $userdata1[0]->sponsor_id; }else{ $sname= $userdata1[0]->direct_id; }	
																				?>
								<tr class="gradeX">
									<td><?php echo $j;?></td>
									<td><?php echo $inv->user_id; ?></td>
									<td><?php echo $sname; ?></td>
									<td><?php echo $admin;//$admin_data[0]->username; ?></td>
									<td><?php echo $inv->planName;?></td>
									<td><?php echo $inv->invoiceID;?></td>
									<td><?php echo $inv->transactionID;?></td>
									<td><i class="fa fa-rupee"></i><?php echo $inv->amount;?></td>
									<td><?php echo appytax($inv->tax,$inv->amount); //echo $inv->tax;?></td>
									<td><i class="fa fa-rupee"></i><?php echo $inv->finalAmount;?></td>	
                                    <td><?php echo date('d-m-Y h:m a',$inv->invTime);?></td>									
									<td class="actions">
<?php $admin_login_id = $this->session->userdata('ADMINID');
$admin_data = $this->admin_model->selectAdminById($admin_login_id);
if($admin_data[0]->type==1)
{
 ?>									
										<a href="<?php echo base_url('index.php/admin/edituserinvoice/'.$inv->id); ?>"><button class="mb-xs mt-xs mr-xs btn btn-info" title="edit record"><i class="fa fa-pencil"></i></button></a>
<?php } ?>
									</td>
								</tr>
								<?php } ?>																				
							</tbody>
						</table>
					</div>						
				</section>
			</div>
		</section>
			
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		
		<script src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
				
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.tabletools.js"></script>
	
	</body>
<?php 
function appytax($TAX,$AMOUNT)
{
	$taxamount = ($TAX*$AMOUNT)/100;
	return abs($taxamount);
}
?>
</html>