 <?php $this->load->view('admin/layout/header'); ?>
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2> Edit Invoice</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Invoice</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>                           
							<section class="panel">
							
							<div class="panel-body">								
								<div class="table-responsive">									
										<table class="table" style="">										
											<tbody>
												<form method="post" enctype="multipart/form-data" >
												<tr>
													<td>Invoice Description</td>
													<td><textarea name="planName" class="form-control"><?php echo $invData[0]->planName; ?></textarea></td>
												</tr>	
												<tr>
													<td>Invoice ID</td>
													<td><input readonly type="text" name="invoiceID" class="form-control" value="<?php echo $invData[0]->invoiceID;?>"></td>
												</tr>
												<tr>
													<td>Transaction ID</td>
													<td><input type="text" name="transactionID" class="form-control" value="<?php echo $invData[0]->transactionID?>"></td>
												</tr>
												<tr>
													<td>Net Amount</td>
													<td><input type="text" name="amount" id="amount" class="form-control" onkeyup="return calculteFinalPrice(this.value);" autocomplete="off" value="<?php echo $invData[0]->amount;?>"></td>
												</tr>
												<tr>
													<td>Tax(%)</td>
													<td><input readonly type="text" name="tax" id="tax" class="form-control" value="<?php echo $invData[0]->tax;?>" autocomplete="off"></td>
												</tr>
												<tr>
													<td>Total Amount</td>
													<td><input readonly type="text" name="finalAmount" id="finalAmount" class="form-control" value="<?php echo $invData[0]->finalAmount;?>" autocomplete="off"></td>
												</tr>
												<tr>
													<td>Status</td>
													<td>
														<select name="status" class="form-control">
															<option <?php echo($invData[0]->status=='pending')?'selected':''; ?> value='pending'>Pending</option>
															<option <?php echo($invData[0]->status=='paid')?'selected':''; ?> value='paid'>Paid</option>
														</select>
													</td>
												</tr>
												<tr>
													<td colspan="2"><button type="submit" class="btn btn-info" name="updateData">Update Data</button></td>
												</tr>
												</form>
											</tbody>
										</table>					
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					
					<!-- end: page -->
				</section>
			</div>
		</section>
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		<script>
			function calculteFinalPrice(AMOUNT)
			{
				var amount = AMOUNT;
				var tax =  <?php echo $tax[0]->tax; ?>;
				var tax_field = document.getElementById('tax');
				var final_amount = document.getElementById('finalAmount');
				
				if(AMOUNT>0 && AMOUNT!="")
				{							
					tax_field.value = tax;
					final_amount.value = parseInt(amount)+((tax*amount)/100);
				}
				else
				{

					tax_field.value = "0";
					final_amount.value = "0";
				}	
			}
		</script>		
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>