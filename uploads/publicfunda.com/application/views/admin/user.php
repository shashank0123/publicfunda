<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller
{
	
	function maxValueInArray($array, $keyToSearch)
	{
		$currentMax = NULL;
		foreach($array as $arr)
		{
			foreach($arr as $key => $value)
			{
				if ($key == $keyToSearch && ($value >= $currentMax))
				{
					$currentMax = $value;
				}
			}
		}

		return $currentMax;
	}
	public function signup()
	{
		error_reporting(0);
		if(isset($_POST['userRegistr']))
		{
			$email = $this->input->post('email');
			$user_data = $this->user_model->selectUserByEmail($email);
			
			$planId = $this->input->post('plan');
			$plandata = $this->plan_model->selectPlanByID($planId);
			if(count($plandata))
			{
				$totalPoints = $plandata[0]->totalVisitors;
				$linkPerDay = $plandata[0]->linkPerDay;	
				$clickprice = $plandata[0]->earningPerClick;		
			}
			else
			{
				$totalPoints = 0;
				$linkPerDay = 0;
				$clickprice = 0;
			}	
			
			if(count($user_data)>0)
			{
				unset($_POST['password']);
				unset($_POST['cpassword']);
				$query = http_build_query($_POST);	
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error! </strong>this email address is already in use please try another email</div>');	
				redirect('index.php/user/signup?'.$query);
			}	
			$pancard_no = $this->input->post('pancard_no');	
			if(!empty($pancard_no))
			{
				$user_data_by_pancard = $this->user_model->selectUserByDocument('pancard',$pancard_no);
				if(count($user_data_by_pancard)>0)
				{
					unset($_POST['password']);
					unset($_POST['cpassword']);
					$query = http_build_query($_POST);	
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error! </strong>this pancard no is already in use please try another pancard no</div>');	
					redirect('index.php/user/signup?'.$query);
				}	
			}			
			$postion = $this->input->post('postion');
			$sponcer_id = $this->input->post('sponsor_id');
			$direct_id ='NULL';
			$this->load->model('testtree_model');
			
			$sponserData = $this->testtree_model->getalluserstree($sponcer_id,$postion);
			if(count($sponserData)>0)
			{			
				$direct_id = $sponcer_id;
				$sponcer_id_final = $value = $this->maxValueInArray($sponserData, "uid");
			}
			else
			{
				$direct_id ='NULL';
				$sponcer_id_final = $sponcer_id;
			}	
			
			$name = $this->input->post('name');  
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$data['name'] = $this->input->post('name');   
			$data['image'] = 'no_user.png';
			$data['email'] = $this->input->post('email');
			$data['contact_no'] = $this->input->post('contact_no');
			$data['pancard_no'] = $this->input->post('pancard_no');
			$data['applied_pancard'] = $this->input->post('applied_pancard');
			$data['nominee'] = $this->input->post('nominee');
			$data['sponsor_id'] = $sponcer_id_final; //$this->input->post('sponsor_id');
			$data['sponsor_name'] = $this->input->post('sponsor_name');
			$data['postion'] = $this->input->post('postion');
			$data['direct_id'] = $direct_id;
			$data['plan'] = $this->input->post('plan'); 			
			$data['wallet'] = 0;
			$data['clickprice'] = $clickprice;
			$data['totalPoints'] = $totalPoints;
			$data['linkPerDay'] = $linkPerDay;
			$data['password'] = base64_encode($this->input->post('password'));
			$data['registration_date'] = time();
			$data['promotinalincomme_lastdate'] = time(); 
			$data['rdate'] = date('Y-m-d');
			$data['expireuser'] = date('Y-m-d');
			$data['status'] = 1; //$this->input->post('status');			
			$insertedData = $this->user_model->insertData($data);
			if($insertedData)
			{	
					$this->email->set_newline("\r\n");
					$this->email->set_mailtype("html");
					$this->email->to($email);
					$this->email->from('info@samridhbharat.biz', 'samridhbharat.biz');
					$this->email->subject('Account successfully created');
					/*
					$msg = "Dear $name,<br>Thanks for registering with us.<br><br>Your account has been successfully created.<br>"; 
					$msg .="please find your login information as below<br>";
					$msg .= "User ID :  ".$insertedData."<br>";
					$msg .= "Password : ".$password."<br>";
					$msg .= "samridhbharat.biz Support Team";
					*/
					$search  = array('{$name}','{$USERID}','{$password}');
					$replace = array($name,$insertedData,$password);		
					$tax = $this->admin_model->getAllTaxes(1);
					
					$msg =  str_replace($search,$replace,$tax[0]->regmailcontent);
		
					$this->email->message($msg);
					$this->email->send();
				
					$invData['invoiceID']='MOTIF/'.date('Y').'/'.time();
					$invData['user_id']= $insertedData;					
					$invData['amount']= 00;
					$invData['tax']= 00;
					$invData['finalAmount']= 00;	//$plandata[0]->planFees;
					$invData['planID']= $plandata[0]->id;
					$invData['planName']= $plandata[0]->planName;
					$invData['invDate']= date('Y-m-d');
					$invData['invTime']= time();
					$invData['status']= '0';
					$this->user_model->insertPlanInvoice($invData);
			}		
			$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Thank You for Registration! </strong> Please check your email to get your login details.</div>');	
			redirect('index.php/user/signup/');	
		}	
		//$this->linkdistribution();		
		$this->load->view('front/signup');
	}	
	
	function user_invoice()
	{
		$args = func_get_args();
		$this->load->library('numbertowords');
		$data['user'] = $this->user_model->selectUserByID($args[0]);
		$this->load->view('front/user-invoice',$data);
	}
	
	function checkuserexpire($uid)
	{
		$userdata = $this->user_model->selectUserByID($uid);
		//print_r($userdata);
		$myplandata = $this->plan_model->selectPlanByID($userdata[0]->plan);
		$userdate = $userdata[0]->expireuser;
		$valid_days = $myplandata[0]->validity*30;
		$expdate = date('Y-m-d',strtotime($userdata[0]->expireuser . " + $valid_days day"));  //$myplandata[0]->validity*30;]
		$now = date('Y-m-d');
		if($expdate<$now)
		{
			$udata['status'] = 0;
			$this->user_model->updateData($userdata[0]->id,$udata);	
			$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>this account is currently inactive. please contact the account administrator to activate</div>');	
			redirect('index.php/user/login');
			//echo 'expire';
		}
		else
		{
			//echo 'available';
		}
	}
	
	public function login()
	{		
		//echo $_SERVER['REMOTE_ADDR'];
		if(isset($_POST['submitLogin']))
		{
			$userid = $this->input->post('userid');
			$password = $this->input->post('password');
			if(empty($userid) || empty($password))
			{
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error! </strong>Invalid User ID or Password</div>');	
				redirect('index.php/user/login');					
			}
			else
			{
				$userdata = $this->user_model->userLogin($userid,$password);
				if(count($userdata)>0)
				{
					if($userdata[0]->status=='1')
					{
						$user_id = $userdata[0]->id;
						$this->checkuserexpire($userdata[0]->id);
						$user_email = $userdata[0]->email;
						$user_array = array('USERID'=>$user_id,'USEREMAIL'=>$user_email,'LOGINBY'=>'USER','USERLOGIN'=>true);
						$this->session->set_userdata($user_array);
						$data['last_login_time'] = time();
						$data['last_login_ip'] = $_SERVER['REMOTE_ADDR'];
						$this->user_model->updateData($user_id,$data);						
						redirect('index.php/user/dashboard');
					}
					else
					{
						$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>this account is currently inactive. please contact the account administrator to activate</div>');	
						redirect('index.php/user/login');
					}	
				}
				else
				{
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error! </strong>Invalid User ID or Password</div>');	
					redirect('index.php/user/login');
				}
			}	
		}
		//$this->linkdistribution();	
		$this->load->view('front/login');
	}
	
	public function forgotpassword()
	{	
		if(isset($_POST['resetPassword']))
		{
			$userid = $this->input->post('userid');
			if(!empty($userid))
			{
				$userdata = $this->user_model->selectUserByID($userid);
				if(count($userdata)>0)
				{
					$this->load->library('email');
					$this->email->set_newline("\r\n");
					$this->email->set_mailtype("html");
					$this->email->to($userdata[0]->email);
					$this->email->from('info@campaigntrade.com', 'Samridh Bharat');
					$this->email->subject('Forgot Password');					
					// message start				
					$message = "Dear ".$userdata[0]->name.",<br>";
					$message .= "Thank you for registering with us.";
					$message .= "Your account has been created, you can login with the following credentials.<br>";
					$message .= "------------------------<br>";
					$message .= "USER ID: ".$userdata[0]->id." <br>";
					$message .= "PASSWORD: ".$userdata[0]->password." <br>";
					$message .= "------------------------<br><br>";
					$message .= "Thank you <br> Samridh Bharat support team";
					// message end					
					$this->email->message($message);
					$this->email->send();
					$this->session->set_flashdata('message', '<div class="alert alert-success">your login details have been emailed to you</div>');
				}
				else
				{
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error! </strong>Invalid User ID</div>');
				}		
			}
			else
			{
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error! </strong>Invalid User ID</div>');	
			}
			redirect('index.php/user/forgotpassword');
		}	
		$this->load->view('front/forgot-password');
	}
	
	function like_comments()
	{
		$user_login_id = $this->session->userdata('USERID'); 
		$args = func_get_args();
		$like_data = $this->comment_model->selectLikeDataByUserId($user_login_id);
		if(count($args)>0)
		{
			if($args[0]=='LIKE')
			{
				$like = 1;
			}
			else
			{
				$like = 0;
			} 			
			$ldata['like'] = $like;
			$ldata['uid'] = $user_login_id;
			$ldata['likeTime'] = time();
			$ldata['like_date'] = date('Y-m-d');
			$ldata['clickedIP'] = getenv('REMOTE_ADDR');
			if(count($like_data)>0)
			{
				$this->comment_model->updateLikeData($user_login_id,$ldata);
			}
			else
			{
				$this->comment_model->insertLikeData($ldata);				
			}				
			redirect('index.php/user/dashboard');
		}
		else
		{
			redirect('index.php/user/dashboard');
		}	
	}
	
	public function dashboard()
	{	
		$this->load->model('dashboard_model');
		$user_login_id = $this->session->userdata('USERID'); 
		if(date('d')=='01')
		{
			$this->apply_promotional_income($user_login_id);
		}
		
		if(isset($_POST['postComments']))
		{
			$comment = $this->input->post('comment');	//comment_model
			if(!empty($comment))
			{
				$cdata['uid'] = $user_login_id;
				$cdata['comments'] = $comment;
				$cdata['comment_time'] = time();
				$cdata['comment_date'] = date('Y-m-d');
				$cdata['status'] = 1;
				$this->comment_model->insertData($cdata);
				redirect('index.php/user/dashboard');
			}	
		}	
		
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$data["Dashboard"]=$this->dashboard_model->list_dashboard();		
		$data["Dashboard"]=$this->dashboard_model->list_dashboards();
		$data["image"]=$this->dashboard_model->list_image();
		$data["slider"] = $this->banner_model->selectbannerbyid(1);
		
		$data["popup"]= $this->popup_model->selectPopupByID(1);
		
		$this->load->view('front/dashboard',$data);
	}
	public function rewards()
	{	
		$this->load->model('rewards_model');
		$user_login_id = $this->session->userdata('USERID'); 
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$data["popup"]= $this->popup_model->selectPopupByID(12);
		$this->load->view('front/rewards',$data);
	}
	public function profile()
	{		
		$user_login_id = $this->session->userdata('USERID'); 
		$kycDetails = $this->user_model->getKycDocument($user_login_id);
		if(isset($_POST['updateInformation']))
		{
			
			if($_FILES['image']['name']!="")
			{
				$file = time().'_'.$_FILES['image']['name'];
				$file_tmp = $_FILES['image']['tmp_name'];
				$path = 'uploads/user/'.$file;
				move_uploaded_file($file_tmp,$path);
			}
			else
			{
				$file = $_POST['oldImage'];
			}	
			$pancard_no = $this->input->post('pancard_no');
			$adhaar_no = $this->input->post('adhaar_no');
			$voterid_no = $this->input->post('voterid_no');
			$passport_no = $this->input->post('passport_no');
			if(!empty($pancard_no))
			{
				$user_data_by_pancard = $this->user_model->checkDocumentWhenEditProfile('pancard',$pancard_no,$user_login_id);
				if(count($user_data_by_pancard)>0)
				{						
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error! </strong>this pancard no is already in use please try another pancard no</div>');	
					redirect('index.php/user/profile');
				}	
			}
			
			if(!empty($adhaar_no))
			{
				$user_data_by_adhaar = $this->user_model->checkDocumentWhenEditProfile('adhaar',$adhaar_no,$user_login_id);
				if(count($user_data_by_adhaar)>0)
				{						
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error! </strong>this adhaar card is already in use please try another adhaar card</div>');	
					redirect('index.php/user/profile');
				}	
			}
			
			if(!empty($voterid_no))
			{
				$user_data_by_voterid = $this->user_model->checkDocumentWhenEditProfile('voterid',$voterid_no,$user_login_id);
				if(count($user_data_by_voterid)>0)
				{						
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error! </strong>this voterid card is already in use please try another voterid card</div>');	
					redirect('index.php/user/profile');
				}	
			}
			
			if(!empty($passport_no))
			{
				$user_data_by_passport = $this->user_model->checkDocumentWhenEditProfile('passport',$passport_no,$user_login_id);
				if(count($user_data_by_passport)>0)
				{						
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error! </strong>this passport is already in use please try another passport</div>');	
					redirect('index.php/user/profile');
				}	
			}
			
			$userdata['name'] = $this->input->post('name');
			$userdata['contact_no'] = $this->input->post('contact_no');
			$userdata['image'] = $file;
			$userdata['email'] = $this->input->post('email');
			$userdata['nominee'] = $this->input->post('nominee');
			$userdata['gender'] = $this->input->post('gender');
			$userdata['dob'] = $this->input->post('dob');
			$userdata['address'] = $this->input->post('address');
			$userdata['pincode'] = $this->input->post('pincode');
			$userdata['city'] = $this->input->post('city');
			$userdata['state'] = $this->input->post('state');
			//$userdata['pancard_no'] = $this->input->post('pancard_no');
			$userdata['colleges'] = $this->input->post('colleges');
			$userdata['university'] = $this->input->post('university');
			$userdata['schools'] = $this->input->post('schools');
			$userdata['highest_degree'] = $this->input->post('highest_degree');
			$userdata['extra_skills'] = $this->input->post('extra_skills');
			$userdata['celebrity'] = $this->input->post('celebrity');
			$userdata['profession'] = $this->input->post('profession');
			$userdata['industry'] = $this->input->post('industry');
			$userdata['current_industry'] = $this->input->post('current_industry');
			
			
			$userdata['pancard_no'] = $this->input->post('pancard_no');
			$userdata['adhaar_no'] = $this->input->post('adhaar_no');
			$userdata['voterid_no'] = $this->input->post('voterid_no');
			$userdata['passport_no'] = $this->input->post('passport_no');
			
			
			$userdata['company_name'] = $this->input->post('company_name');
			$userdata['current_company'] = $this->input->post('current_company');
			$userdata['campaign_title'] = $this->input->post('campaign_title');
			$userdata['field_work'] = $this->input->post('field_work');
			$userdata['flight_travel'] = $this->input->post('flight_travel');
			$userdata['hobbies'] = $this->input->post('hobbies');
			$userdata['education'] = $this->input->post('education');
			$userdata['course_type'] = $this->input->post('course_type');
			$userdata['salary'] = $this->input->post('salary');
			$userdata['turn_over'] = $this->input->post('turn_over');
			$userdata['total_job'] = $this->input->post('total_job');
			$userdata['current_job'] = $this->input->post('current_job');
			$userdata['current_designation'] = $this->input->post('current_designation');
			$userdata['mobile_handset'] = $this->input->post('mobile_handset');
			$userdata['kyc_status'] = 0;
			
			if(count($kycDetails)>0)
			{
					if($_FILES['identityproof']['name']!="")
					{
						$idfile = time().'_'.$_FILES['identityproof']['name'];
						$file_tmp = $_FILES['identityproof']['tmp_name'];
						$path = 'uploads/document/'.$idfile;
						move_uploaded_file($file_tmp,$path);
					}else { $idfile = $_POST['oldIdentityproof']; }
					
					if($_FILES['pancard']['name']!="")
					{
						$pancardfile = time().'_'.$_FILES['pancard']['name'];
						$file_tmp = $_FILES['pancard']['tmp_name'];
						$path = 'uploads/document/'.$pancardfile;
						move_uploaded_file($file_tmp,$path);
					}else { $pancardfile = $_POST['oldIpancard']; }
					
					$iddata['user_id'] = $user_login_id;	
					$iddata['identityproof'] = $idfile;
					$iddata['idtype'] = $this->input->post('idtype');	
					$iddata['iduploaded_date'] = time();			
					$iddata['idstatus'] = 0;
					$iddata['pancard'] = $pancardfile;
					$iddata['pancardType'] = $this->input->post('pancardType');
					$iddata['pancarduploaded_date'] = time();
					$iddata['pancardstatus'] = 0;
					$this->user_model->updateKyc($kycDetails[0]->id,$iddata);				
						
			}
			else
			{
					if($_FILES['identityproof']['name']!="")
					{
						$idfile = time().'_'.$_FILES['identityproof']['name'];
						$file_tmp = $_FILES['identityproof']['tmp_name'];
						$path = 'uploads/document/'.$idfile;
						move_uploaded_file($file_tmp,$path);
					}
					else
					{
						$idfile = "";
					}
					
					if($_FILES['pancard']['name']!="")
					{
						$pancardfile = time().'_'.$_FILES['pancard']['name'];
						$file_tmp = $_FILES['pancard']['tmp_name'];
						$path = 'uploads/document/'.$pancardfile;
						move_uploaded_file($file_tmp,$path);
					}
					else
					{
						$pancardfile = "";
					}	
					$iddata['user_id'] = $user_login_id;	
					$iddata['identityproof'] = $idfile;
					$iddata['idtype'] = $this->input->post('idtype');	
					$iddata['iduploaded_date'] = time();			
					$iddata['idstatus'] = 0;
					$iddata['pancard'] = $pancardfile;
					$iddata['pancardType'] = $this->input->post('pancardType');
					$iddata['pancarduploaded_date'] = time();
					$iddata['pancardstatus'] = 0;
					$this->user_model->insertKyc($iddata);
			}
			
			$this->user_model->updateData($user_login_id,$userdata);
			$this->session->set_flashdata('message','<div class="alert alert-success">your profile has been updated successfully.</div>');
			redirect('index.php/user/profile');				
		}	

		if(isset($_POST['changePassword']))
		{
			$hidepassword = $this->input->post('hiddenpassword');
			$opwd = $this->input->post('opwd');
			$npwd = $this->input->post('npwd');
			$rpwd = $this->input->post('rpwd');
			
			if($hidepassword==base64_encode($opwd))
			{
				if($npwd==$rpwd)
				{
					$userdata['password'] = base64_encode($this->input->post('npwd'));			
					$this->user_model->updateData($user_login_id,$userdata);
					$this->session->set_flashdata('message','<div class="alert alert-success">your password has been changed successfully.</div>');
					redirect('index.php/user/profile');
				}
				else
				{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error : </strong> Your password and confirmation password do not match.</div>');
					redirect('index.php/user/profile');
				}	
			}
			else
			{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error : </strong>old password invalid.</div>');
				redirect('index.php/user/profile');
			}
			
		}		
		
		
		if(isset($_POST['updateBankInformation']))
		{
		
			$userdata['account_holder'] = $this->input->post('account_holder');	
			$userdata['bank_name'] = $this->input->post('bank_name');	
			$userdata['branch_name'] = $this->input->post('branch_name');	
			$userdata['bank_accountno'] = $this->input->post('bank_accountno');	
			$userdata['bank_ifsccode'] = $this->input->post('bank_ifsccode');			
			$this->user_model->updateData($user_login_id,$userdata);	
			
			if(count($kycDetails)>0)
			{
					if($_FILES['bankdetails']['name']!="")
					{
						$bankdetailsfile = time().'_'.$_FILES['bankdetails']['name'];
						$file_tmp = $_FILES['bankdetails']['tmp_name'];
						$path = 'uploads/document/'.$bankdetailsfile;
						move_uploaded_file($file_tmp,$path);
					}else { $bankdetailsfile = $_POST['oldBankPassbook']; }
					
					if($_FILES['cancelCheque']['name']!="")
					{
						$cancelChequefile = time().'_'.$_FILES['cancelCheque']['name'];
						$file_tmp = $_FILES['cancelCheque']['tmp_name'];
						$path = 'uploads/document/'.$cancelChequefile;
						move_uploaded_file($file_tmp,$path);
					}else { $cancelChequefile = $_POST['oldCancelCheque']; }
					
					$bankdata['user_id'] = $user_login_id;
					$bankdata['bankdetails'] = $bankdetailsfile;					
					$bankdata['bankdetailstype'] = 'Bank Passbook';
					$bankdata['bankdetailsuploaded_date'] = time();
					$bankdata['bankdetailsstatus'] = 0;
					$bankdata['cancelCheque'] = $cancelChequefile;
					$this->user_model->updateKyc($kycDetails[0]->id,$bankdata);				
		
			}
			else
			{
					if($_FILES['bankdetails']['name']!="")
					{
						$bankdetailsfile = time().'_'.$_FILES['bankdetails']['name'];
						$file_tmp = $_FILES['bankdetails']['tmp_name'];
						$path = 'uploads/document/'.$bankdetailsfile;
						move_uploaded_file($file_tmp,$path);
					}else{ $bankdetailsfile = ""; }	
					if($_FILES['cancelCheque']['name']!="")
					{
						$cancelChequefile = time().'_'.$_FILES['cancelCheque']['name'];
						$file_tmp = $_FILES['cancelCheque']['tmp_name'];
						$path = 'uploads/document/'.$cancelChequefile;
						move_uploaded_file($file_tmp,$path);
					}
					else { $cancelChequefile = ""; }
					$bankdata['user_id'] = $user_login_id;	
					$bankdata['bankdetails'] = $bankdetailsfile;					
					$bankdata['bankdetailstype'] = 'Bank Passbook';
					$bankdata['bankdetailsuploaded_date'] = time();
					$bankdata['bankdetailsstatus'] = 0;
					$bankdata['cancelCheque'] = $cancelChequefile;
					$this->user_model->insertKyc($bankdata);
			}			
			$this->session->set_flashdata('message','<div class="alert alert-success">your Bank Details has been updated successfully.</div>');
			redirect('index.php/user/profile');
		}		
		$data['KYCDATA'] = $kycDetails;
		$data["popup"]= $this->popup_model->selectPopupByID(2);
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$this->load->view('front/profile',$data);
	}
	
	public function mynetworks()
	{
		$this->load->model('tree_model');
		$user_login_id = $this->session->userdata('USERID');
		
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$data["popup"]= $this->popup_model->selectPopupByID(7);
		$this->load->view('front/mynetwork',$data);
		//$this->load->view('tree',$data);
	}
	
	public function getchildtree()
	{
		$this->load->model('tree_model');	
		$data['userData'] = $this->user_model->selectUserByID($_POST['UID']);
		$this->load->view('front/ajax-mynetwork',$data);
	}
	public function myteam()
	{
		$this->load->model('tree_model');
		$user_login_id = $this->session->userdata('USERID');
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$data["popup"]= $this->popup_model->selectPopupByID(9);
		$this->load->view('front/myteam',$data);
	}
	
	public function mybusiness()
	{
		error_reporting(0);
		$this->load->model('tree_model');
		$user_login_id = $this->session->userdata('USERID');
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$data["popup"]= $this->popup_model->selectPopupByID(10);
		$this->load->view('front/mybussiness',$data);
	}
	
	public function promotionalincome()
	{
		$this->load->model('tree_model');
		$user_login_id = $this->session->userdata('USERID');
		
		if(isset($_POST['searchMyTeam']))
		{
			$sdate = date('Y-m-d',strtotime($this->input->post('start')));
			$esdate = date('Y-m-d',strtotime($this->input->post('end')));
			//$status = $this->input->post('status');	
            $status = "";			
		}
		else
		{
			$sdate = "";
			$esdate = "";
			$status = "";
		}
		
		$data['pIncome'] = $this->user_model->getPromostionalIncome($user_login_id,$sdate,$esdate,$status);
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$data["popup"]= $this->popup_model->selectPopupByID(8);
		$this->load->view('front/promotionalincome',$data);
	}
	
	function getBonus()
	{
		$this->load->model('tree_model');
		$user_login_id = $this->session->userdata('USERID');
		if(date('d')==01)
		{
			$left_income  = $this->user_model->getBonusIncome($user_login_id,'L');
			$right_income  = $this->user_model->getBonusIncome($user_login_id,'R');			
			
			$lowerAmount = 	min($left_income, $right_income);
			$highestAmount = max($left_income, $right_income);
			if($highestAmount==$left_income)
			{
				$carry_left_amount = abs($left_income - $right_income);
				$carry_right_amount = 00;
			}
			else
			{
				$carry_left_amount = 00;
				$carry_right_amount = abs($left_income - $right_income);
			}
			
			$bonus = ($lowerAmount*8)/100;
			$tds_amount = ($bonus*10)/100;
			$admin_amount = ($bonus*5)/100;
			
			$bonusData['uid'] = $user_login_id;
			$bonusData['left_bv'] = $left_income;
			$bonusData['right_bv'] = $right_income;
			$bonusData['clbv'] = $carry_left_amount;
			$bonusData['crbv'] = $carry_right_amount;
			$bonusData['balancing'] = 
			$bonusData['income'] = $bonus;
			$bonusData['tds'] = $tds_amount;
			$bonusData['admincharge'] = $admin_amount;
			$bonusData['netIncome'] = $bonus-($tds_amount+$admin_amount);
			$bonusData['crDate'] =  date('Y-m-d');
			$this->user_model->insertBonusIncome($bonusData);
			
			$userdata['lastBonusDate'] = date('Y-m-d');
			$this->user_model->updateData($user_login_id,$userdata);
		}
	}
	
	 	
	function kyc()
	{		
		$user_login_id = $this->session->userdata('USERID');
		$kycDetails = $this->user_model->getKycDocument($user_login_id);		
		
			if(count($kycDetails)>0)
			{
				if(isset($_POST['uploadIdProof']))
				{
					if($_FILES['identityproof']['name']!="")
					{
						$idfile = time().'_'.$_FILES['identityproof']['name'];
						$file_tmp = $_FILES['identityproof']['tmp_name'];
						$path = 'uploads/document/'.$idfile;
						move_uploaded_file($file_tmp,$path);
					}
					$iddata['user_id'] = $user_login_id;	
					$iddata['identityproof'] = $idfile;
					$iddata['idtype'] = $this->input->post('idtype');
					$iddata['iduploaded_date'] = time();
					$iddata['idstatus'] = 0;
					$this->user_model->updateKyc($kycDetails[0]->id,$iddata);
					$this->session->set_flashdata('message','<div class="alert alert-success">document has been uploaded successfully.</div>');
					redirect('index.php/user/kyc');
				}
				
				if(isset($_POST['uploadPancard']))
				{
					if($_FILES['pancard']['name']!="")
					{
						$pancardfile = time().'_'.$_FILES['pancard']['name'];
						$file_tmp = $_FILES['pancard']['tmp_name'];
						$path = 'uploads/document/'.$pancardfile;
						move_uploaded_file($file_tmp,$path);
					}
					$pandata['user_id'] = $user_login_id;	
					$pandata['pancard'] = $pancardfile;
					$pandata['pancardType'] = $this->input->post('pancardType');
					$pandata['pancarduploaded_date'] = time();
					$pandata['pancardstatus'] = 0;
					$this->user_model->updateKyc($kycDetails[0]->id,$pandata);
					$this->session->set_flashdata('message','<div class="alert alert-success">document has been uploaded successfully.</div>');
					redirect('index.php/user/kyc');
				}
				
				if(isset($_POST['uploadBank']))
				{
					if($_FILES['bankdetails']['name']!="")
					{
						$bankdetailsfile = time().'_'.$_FILES['bankdetails']['name'];
						$file_tmp = $_FILES['bankdetails']['tmp_name'];
						$path = 'uploads/document/'.$bankdetailsfile;
						move_uploaded_file($file_tmp,$path);
					}
					$bankdata['user_id'] = $user_login_id;	
					$bankdata['bankdetails'] = $bankdetailsfile;
					$bankdata['bankdetailstype'] = $this->input->post('bankdetailstype');
					$bankdata['bankdetailsuploaded_date'] = time();
					$bankdata['bankdetailsstatus'] = 0;
					$this->user_model->updateKyc($kycDetails[0]->id,$bankdata);
					$this->session->set_flashdata('message','<div class="alert alert-success">document has been uploaded successfully.</div>');
					redirect('index.php/user/kyc');
				}	
			}
			else
			{
				if(isset($_POST['uploadIdProof']))
				{
					if($_FILES['identityproof']['name']!="")
					{
						$idfile = time().'_'.$_FILES['identityproof']['name'];
						$file_tmp = $_FILES['identityproof']['tmp_name'];
						$path = 'uploads/document/'.$idfile;
						move_uploaded_file($file_tmp,$path);
					}
					$iddata['user_id'] = $user_login_id;	
					$iddata['identityproof'] = $idfile;
					$iddata['idtype'] = $this->input->post('idtype');
					$iddata['iduploaded_date'] = time();
					$iddata['idstatus'] = 0;
					$this->user_model->insertKyc($iddata);
				}
				
				if(isset($_POST['uploadPancard']))
				{
					if($_FILES['pancard']['name']!="")
					{
						$pancardfile = time().'_'.$_FILES['pancard']['name'];
						$file_tmp = $_FILES['pancard']['tmp_name'];
						$path = 'uploads/document/'.$pancardfile;
						move_uploaded_file($file_tmp,$path);
					}
					$pandata['user_id'] = $user_login_id;	
					$pandata['pancard'] = $pancardfile;
					$pandata['pancardType'] = $this->input->post('pancardType');
					$pandata['pancarduploaded_date'] = time();
					$pandata['pancardstatus'] = 0;
					$this->user_model->insertKyc($pandata);
				}
				
				if(isset($_POST['uploadBank']))
				{
					if($_FILES['bankdetails']['name']!="")
					{
						$bankdetailsfile = time().'_'.$_FILES['bankdetails']['name'];
						$file_tmp = $_FILES['bankdetails']['tmp_name'];
						$path = 'uploads/document/'.$bankdetailsfile;
						move_uploaded_file($file_tmp,$path);
					}
					$bankdata['user_id'] = $user_login_id;	
					$bankdata['bankdetails'] = $bankdetailsfile;
					$bankdata['bankdetailstype'] = $this->input->post('bankdetailstype');
					$bankdata['bankdetailsuploaded_date'] = time();
					$bankdata['bankdetailsstatus'] = 0;
					$this->user_model->insertKyc($bankdata);
				}
			}	
		
		$data['KYCDATA'] = $kycDetails;
		$data["popup"]= $this->popup_model->selectPopupByID(11);
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$this->load->view('front/kyc',$data);
	}
	
	function checkUserByID()
	{
		if(isset($_GET['SID']))
		{
			$user_id = $_GET['SID'];
			$user = $this->user_model->selectUserByID($user_id);
			if(count($user)>0)
			{
				echo $user[0]->name;
			}
			else
			{
				echo '0';
			}	
		}
		else
		{
			echo '0';
		}	
	}
	

	
	function todaytask()
	{		
		$user_login_id = $this->session->userdata('USERID');	
		$this->booster_activate($user_login_id);
		$data['todayTaskLink'] = $this->campaign_model->selectDistributionLinkByUserIdAndTodayDate($user_login_id);
		$data['TOTALCLICKEDLINK'] = $this->campaign_model->getTotalClickedLink($user_login_id); 
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$data["popup"]= $this->popup_model->selectPopupByID(4);
		$this->setpopupexpire();
		$this->load->view('front/today-task',$data);
	}
	
	function setpopupexpire()
	{
		setcookie("TODAYTASKEXPIRE", true, time() + (60 * 30)); 
	}
	
	function paymode()
	{		
		$user_login_id = $this->session->userdata('USERID');
		if(isset($_POST['submitButton']))
		{
			$mode = $_POST['payoutmode'];
			if(!empty($mode))
			{
				$userupdade['payoutmode'] = $mode;						
				$updateopt = $this->user_model->updateData($user_login_id,$userupdade);
				$this->session->set_flashdata('message','<div class="alert alert-success">Payout Mode successfully changed.</div>');
				redirect('index.php/user/paymode');
			}	
		}	
		
		$data['login_user'] = $this->user_model->selectUserByID($user_login_id);
		$data["popup"]= $this->popup_model->selectPopupByID(1);
		$this->load->view('front/payout-mode',$data);
	}
	
	function checkUserAndSendOtp()
	{
		$user_login_id = $this->session->userdata('USERID');
		$loginUser = $this->user_model->selectUserByID($user_login_id);
		$walletAmount = $loginUser[0]->wallet;
		
		if(isset($_POST) && count($_POST)>0)
		{
			$amount = $this->input->post('amount');
			$friendId = $this->input->post('friendId');
			if(!empty($friendId))
			{
				if($amount<=$walletAmount)
				{
					$friendData = $this->user_model->selectUserByID($friendId);
					if(count($friendData)>0)
					{
						if($friendId!=$user_login_id)
						{
							$otp = substr(str_shuffle(time().'1234567890'),0,6);
							$userupdade['otp'] = $otp;						
							$updateopt = $this->user_model->updateData($user_login_id,$userupdade);
							if($updateopt)
							{
								$this->email->set_newline("\r\n");
								$this->email->set_mailtype("html");
								$this->email->to($loginUser[0]->email);
								$this->email->from('info@samridhbharat.biz', 'samridhbharat.biz');
								$this->email->subject('samridhbharat.biz One Time Password');
								$msg = "Dear ".$loginUser[0]->email.",<br>"; 
								$msg .="your OTP(One Time Password) is ".$otp."<br>";						
								$msg .= "samridhbharat.biz Support Team";
								$this->email->message($msg);
								$this->email->send();
								echo $friendData[0]->name;
								//echo 1;
							}	
							else
							{
								echo 0;
							}							
						}
						else
						{
							echo 0;
						}	
					}
					else
					{
						echo 0;
					}
				}
				else
				{
					echo 0;
				}	
			}
			else
			{
				echo 0;
			}	
		}
		else
		{
			echo 0;
		}
	}
	
	function getTodayWalletTotalTransaction()
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$user_login_id = $this->session->userdata('USERID');
		$totalTtansaction = $this->user_model->getDailyWalletStatementCount(1001);
		//$totalTransaction = $totalTtansaction;
		echo $totalTtansaction.'<br>';
		if(2 >= $totalTtansaction+2)
		{
			echo 'ok';
		}
		else
		{
			echo 'error';
		
		}
			
		
	}
	function sendMoneyTofriend()
	{
		$user_login_id = $this->session->userdata('USERID');
		$loginUser = $this->user_model->selectUserByID($user_login_id);
		$walletAmount = $loginUser[0]->wallet;
		
		if(isset($_POST) && count($_POST)>0)
		{
			$amount = $this->input->post('amount');
			$friendId = $this->input->post('friendId');			
			$otpnumber = $this->input->post('otpnumber');	
			$checkuser = $this->user_model->selectUserByIdAndOtp($user_login_id,$otpnumber);
			$friendData = $this->user_model->selectUserByID($friendId);
			if(count($friendData)>0)
			{
				if(count($checkuser)>0)
				{				
					$friendUpdateData['wallet'] = $friendData[0]->wallet+$amount;
					$this->user_model->updateData($friendData[0]->id,$friendUpdateData);
					
					$userdata['otp'] = 100000;	
					$userdata['wallet'] = $walletAmount-$amount;
					$this->user_model->updateData($user_login_id,$userdata);
					
					$walletStatement['statementId'] = $friendData[0]->id;	//sender information			
					$walletStatement['user_id'] = $user_login_id;
					$walletStatement['another_user'] = $friendData[0]->id;
					$walletStatement['debit_amount'] = $amount;
					$walletStatement['credit_amount'] = '';
					$walletStatement['finalAmount'] = $walletAmount-$amount;
					$walletStatement['information'] = "Gift to Friend";
					$walletStatement['trDate'] =  date('Y-m-d');
					$walletStatement['trTime'] = time(); 
					$walletStatement['tranType'] = 'debit';
					$walletStatement['status'] = 'complete';
					$this->user_model->insertWalletStatement($walletStatement);				
					
					$walletStatement['statementId'] = $user_login_id;	//receiver information			
					$walletStatement['user_id'] = $friendData[0]->id;
					$walletStatement['another_user'] = $user_login_id;
					$walletStatement['debit_amount'] = '';
					$walletStatement['credit_amount'] = $amount;
					$walletStatement['finalAmount'] = $friendData[0]->wallet+$amount;
					$walletStatement['information'] = 'Receive from friend';
					$walletStatement['trDate'] =  date('Y-m-d');
					$walletStatement['trTime'] = time(); 
					$walletStatement['tranType'] = 'credit';
					$walletStatement['status'] = 'complete';
					$this->user_model->insertWalletStatement($walletStatement);
					echo $friendData[0]->name;
				}												
				else
				{
					echo 0;
				}
			}
			else
			{
				echo 0;
			}	
		}
		else
		{
			echo 0;
		}
	}
	
	function wallet()
	{	
		$user_login_id = $this->session->userdata('USERID');
		$tax = $this->admin_model->getAllTaxes(1);
		$loginUser = $this->user_model->selectUserByID($user_login_id);
		$clickprice = $loginUser[0]->clickprice;
		if(isset($_POST['redeemPoints']))
		{
			$totalPoints = $this->campaign_model->getTotalPoints($user_login_id);
			if(count($totalPoints)>0)
			{
				$tatalTax = ($tax[0]->tds+$tax[0]->admin_charge);
				$amountTotal = (count($totalPoints)*$clickprice*$tatalTax)/100;
				$userdata['wallet'] = (count($totalPoints)*$clickprice-$amountTotal)+$loginUser[0]->wallet;
				$this->user_model->updateData($user_login_id,$userdata);
				$this->campaign_model->updateRedeemPoints($user_login_id);
					
				$redeemhistory['user_id']= $user_login_id;
				$redeemhistory['points']= count($totalPoints);
				$redeemhistory['amount']= count($totalPoints)*$clickprice;
				$redeemhistory['finalAmount']= count($totalPoints)*$clickprice-$amountTotal;
				$redeemhistory['admincharge']= $tax[0]->admin_charge;
				$redeemhistory['tds']= $tax[0]->tds;
				$redeemhistory['redeemdate']= date('Y-m-d');
				$redeemhistory['redeemTime']= time();
				$redeemId= $this->campaign_model->insertRedeemHistoryPoints($redeemhistory);
				if($redeemId)
				{
					$walletStatement['statementId'] = 'REDEEMPOINT'.$redeemId;	//sender information			
					$walletStatement['user_id'] = $user_login_id;
					$walletStatement['another_user'] = 0;
					$walletStatement['debit_amount'] = '';
					$walletStatement['credit_amount'] = (count($totalPoints)*$clickprice-$amountTotal);
					$walletStatement['finalAmount'] = (count($totalPoints)*$clickprice-$amountTotal)+$loginUser[0]->wallet;
					$walletStatement['information'] = 'Redeem CTP Points';
					$walletStatement['trDate'] =  date('Y-m-d');
					$walletStatement['trTime'] = time(); 
					$walletStatement['tranType'] = 'credit';
					$walletStatement['status'] = 'complete';
					$this->user_model->insertWalletStatement($walletStatement);
				}	
				
				$this->session->set_flashdata('message','<div class="alert alert-success">Points redeem successfully.</div>');
				redirect('index.php/user/wallet');
			}			
		} 
		
		if(isset($_POST['genrateInvoice']))
		{
			 //$tax = $this->admin_model->getAllTaxes(1);
			 $totalMonthlyTtansaction = $this->user_model->getMonthlyWalletStatementCount($loginUser[0]->id); 
			 $totalDailyTtansaction = $this->user_model->getDailyWalletStatementCount($loginUser[0]->id);
			 
			if($tax[0]->no_transaction_day >= $totalDailyTtansaction+1)
			{}
			else
			{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>error!</strong>.</div>');
				redirect('index.php/user/wallet');	
			}
			
			if($tax[0]->no_transaction_month >= $totalMonthlyTtansaction+1)
			{}
			else
			{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>error!</strong>.</div>');
				redirect('index.php/user/wallet');
			}
			
				$walletAmount = $loginUser[0]->wallet;
				$invoiceAmount = $this->input->post('invamount');
				if(!empty($invoiceAmount) && !empty($walletAmount))
				{
					if($invoiceAmount<=$walletAmount)
					{
						$invoiceData['user_id']= $user_login_id;
						$invoiceData['amount']= $invoiceAmount;
						$invoiceData['neftNo']= 'N/A'; 
						$invoiceData['transferDate']= 'N/A';  
						$invoiceData['tds']= '0';
						$invoiceData['adminCharge']= '0';
						$invoiceData['invDate']= date('Y-m-d'); 
						//$invoiceData['finalAmount']= $invoiceAmount;
						$invoiceData['payoutMode']= $loginUser[0]->payoutmode;
						$invoiceData['status']= 'Pending';
						$invdata = $this->campaign_model->insertInvoiceData($invoiceData);
						if($invdata)
						{
							$walletStatement['statementId'] = 'SMB'.$invdata;				
							$walletStatement['user_id'] = $user_login_id;
							$walletStatement['another_user'] = '0';
							$walletStatement['debit_amount'] = $invoiceAmount;
							$walletStatement['credit_amount'] = '';
							$walletStatement['finalAmount'] = $walletAmount-$invoiceAmount;
							$walletStatement['information'] = 'Worth of Discount Value Spent';
							$walletStatement['trDate'] =  date('Y-m-d');
							$walletStatement['trTime'] = time(); 
							$walletStatement['tranType'] = 'debit';
							$walletStatement['status'] = 'pending';
							$this->user_model->insertWalletStatement($walletStatement);	
							
							$userdata['wallet'] = $walletAmount-$invoiceAmount;
							$this->user_model->updateData($user_login_id,$userdata);
						}
						$finalamount = $walletAmount-$invoiceAmount;
						$invHtml = "<table class='table mb-none'><tr><th>Date Time</th><th>Particular</th><th>Debit Amount</th><th>Total Balance</th></tr>
									<tr><td>".date('d-m-Y H:i a')."</td><td>Worth of Discount Value Spent</td><td>$invoiceAmount </td><td> ".$finalamount." </td></tr>	
						</table>";
						//$this->session->set_flashdata('invmessage',$invHtml);
						$this->session->set_flashdata('message',"<div class='alert alert-success'>Invoice successfully created. </div>");
						redirect('index.php/user/wallet?mydata='.urlencode(base64_encode($invHtml)));						
					}
					else
					{
						$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>error !</strong> invalid amount.</div>');
						redirect('index.php/user/wallet');
					}	
				}					
		}
		
		$data['todayTaskLink'] = $this->campaign_model->selectDistributionLinkByUserIdAndTodayDate($user_login_id);
		$data['TOTALCLICKEDLINK'] = $this->campaign_model->getTotalClickedLink($user_login_id); 
		$data['REDEEMHISTORY'] = $this->campaign_model->getAllRedeemHistoryPointsList($user_login_id); 
		$data['login_user'] = $loginUser;
		$data["slider"] = $this->banner_model->selectbannerbyid(4);
		$data["popup"]= $this->popup_model->selectPopupByID(14);
		$this->load->view('front/wallet',$data);
	}
	
	function workHistory()
	{				
		$user_login_id = $this->session->userdata('USERID');		
		$loginUser = $this->user_model->selectUserByID($user_login_id);
		if(isset($_POST['checkWorkHistory']))
		{
			$first_date = date('Y-m-d',strtotime($_POST['startDate']));
			$second_date = date('Y-m-d',strtotime($_POST['endDate']));
			$status = $this->input->post('status');
			$workHistoryData = $this->campaign_model->getMyWorkHistory($user_login_id,$first_date,$second_date,$status);
		}		
		else
		{
			$workHistoryData = array();	
		}
		$data['login_user'] = $loginUser;
		$data['workHistoryData'] = $workHistoryData;
		$data["popup"]= $this->popup_model->selectPopupByID(6);
		$this->load->view('front/work-history',$data);
	}
	
	function myworkpayouts()
	{
		$user_login_id = $this->session->userdata('USERID');
		if(isset($_POST['ckeckWorkPayout']))
		{ 
			$first_date = date('Y-m-d',strtotime($_POST['startDate']));
			$second_date = date('Y-m-d',strtotime($_POST['endDate']));
			$payoutdata = $this->campaign_model->getAllRedeemPointsByFilters($user_login_id,$first_date,$second_date);
		}
		else
		{
			$first_date = date('Y-m-d');
			$second_date = date('Y-m-d',strtotime('+7 days'));
			$payoutdata = $this->campaign_model->getAllRedeemPointsByFilters($user_login_id,$first_date,$second_date);
		}
		
		if(isset($_POST['checkWallet']))
		{			
			$first_date = date('Y-m-d',strtotime($_POST['start']));
			$second_date = date('Y-m-d',strtotime($_POST['end']));
			$neftno = $_POST['neftNo'];
			$walletData = $this->user_model->getWalletStatementHistory($neftno,$user_login_id,$first_date,$second_date);
		}
		else
		{
			$first_date = date('Y-m-d');
			$second_date = date('Y-m-d',strtotime('+7 days'));
			$neftno = "";
			$walletData = $this->user_model->getWalletStatementHistory($neftno,$user_login_id,$first_date,$second_date);
		}			
		$loginUser = $this->user_model->selectUserByID($user_login_id);
		$data['walletData'] = $walletData;
		$data['login_user'] = $loginUser;
		$data['payoutdata'] = $payoutdata;
		$data["popup"]= $this->popup_model->selectPopupByID(5);
		$this->load->view('front/myworkpayouts',$data);
	}
	
	function updateDuplicateWindowTask()
	{
		if(count($_POST)>0)
		{
			if(isset($_POST['UID']) && !empty($_POST['UID']))
			{
				if(isset($_POST['CID']))
				{
					//$this->campaign_model->updateCampaignLinkInactive($_POST['LID']);  //update link clicked (inactive)					
					$login_user = $this->user_model->selectUserByID($_POST['UID']);
					
					$campaignData = $this->campaign_model->selectCampaignByID($_POST['CID']); // get campaign data by campaign id					
					$cam_user = $this->user_model->selectUserByID($campaignData[0]->user_id);
					
					$this->email->set_newline("\r\n");
					$this->email->set_mailtype("html");
					$this->email->to($cam_user[0]->email);
					$this->email->from('info@samridhbharat.biz', 'samridhbharat.biz');
					$this->email->subject('samridhbharat user information');
					$msg = "Name - ".$login_user[0]->name.",<br>"; 
					$msg .= "email - ".$login_user[0]->email.",<br>"; 	
					$msg .= "Phone No. - ".$login_user[0]->contact_no.",<br>"; 	
					$msg .= "samridhbharat.biz Support Team";
					$this->email->message($msg);
					$this->email->send();
					
					$data['recievedclicked'] = ($campaignData[0]->recievedclicked+1); 
					$this->campaign_model->updateCampaignData($_POST['CID'],$data);  // update campaign recievedclicked					
					echo 1;
				}				
			}			
		}
	}
	
	function updateMyTask()
	{
		if(count($_POST)>0)
		{
			if(isset($_POST['LID']) && !empty($_POST['LID']))
			{
				if(isset($_POST['CID']))
				{
					$this->campaign_model->updateCampaignLinkInactive($_POST['LID']);  //update link clicked (inactive)					
					$campaignData = $this->campaign_model->selectCampaignByID($_POST['CID']); // get campaign data by campaign id
					$data['recievedclicked'] = ($campaignData[0]->recievedclicked+1); 
					$this->campaign_model->updateCampaignData($_POST['CID'],$data);  // update campaign recievedclicked
				}				
			}			
		}
	}
	
	function refreshLinkCampaign()
	{
		$user_login_id = $this->session->userdata('USERID');
		if(count($_POST)>0)
		{
			if(isset($_POST['LID']) && !empty($_POST['LID']))
			{
				if(isset($_POST['CID']))
				{										
					$camdata = $this->campaign_model->selectCampaignRandomly($user_login_id);
					$data['link_id'] = $camdata[0]->id;
					$this->campaign_model->updateCampaignLink($_POST['LID'],$data); 
				}				
			}			
			//print_r($_POST);
		}	
	}
	
	function userList()
	{
		$data['USERLIST'] = $this->user_model->getAllUsers();
		$this->load->view('admin/user/list',$data);
	}
	
	function checkDemand()
	{
		$userData = $this->user_model->selectActiveUser();
		$tatalLinks = 0;
		foreach($userData as $user)
		{
			$tatalLinks += $user->linkPerDay;
		}
		echo $tatalLinks;
	}
	
	function checkAvailability()
	{				
		$campaignData = $this->campaign_model->selectAllCampaignLinkAvailability();		
		//echo count($campaignData);
		$links_av = 0;
		foreach($campaignData as $link)
		{
			if($link->campaignPoints>=$link->recievedclicked)
			{
				$links_av += $link->campaignPoints-$link->recievedclicked;
			}	
		}
		echo $links_av;	
	}
	
	
	function checkStorage($demand,$availability)
	{
		if($demand>=$availability*60)
		{
			echo 'green';
		}
		if($demand>=$availability*45)
		{
			echo 'yellow';
		}
		if($demand>=$availability*30)
		{
			echo 'orange';
		}
		if($demand>=$availability*15)
		{
			echo 'red';
		}			
	}
		
	function linkdistribution()
	{
		//error_reporting(0);
		$userData = $this->user_model->selectActiveUser();  // get active all user
		foreach($userData as $user)
		{
			//echo $user->id.'<br>';
			$planData = $this->plan_model->selectPlanByID($user->plan);  // get active plan
			$campaignLinks = $this->campaign_model->selectCampaignForDistribution();	 // get all campaign links data
			$k =0;	
			$checkLink = $this->campaign_model->selectDistributionLinkByUserIdAndTodayDate($user->id);
			if(count($checkLink)==0)
			{
				foreach($campaignLinks as $c_link)
				{					
					if($c_link->campaignStatus==1)
					{
						if($c_link->user_id!=$user->id)
						{
							$countAllDistributedCampaign = $this->campaign_model->getAllDistributionCampaignById($c_link->id);
							if($c_link->campaignPoints>=count($countAllDistributedCampaign))
							{ $k++;											
								//if($k<=$planData[0]->linkPerDay) 
								//if($k<=$user->linkPerDay)
								if($k<=$user->linkPerDay)
								{
									$taskData['user_id'] = $user->id;
									$taskData['link_id'] = $c_link->id;
									$taskData['distributionDate'] = date('Y-m-d');
									$taskData['status'] = 1;
									$taskData['redeem'] = 1;
									$this->campaign_model->insertTaskLink($taskData);
								}							
							}
						}
					}											
				}	
			}							
		}
	}
	
	
	function getCityByStateId()
	{
		if(isset($_GET['SID']))
		{
			$cityArray = $this->user_model->selectAllCitiesBystateId($_GET['SID']);
			if(count($cityArray))
			{
				foreach($cityArray as $city)
				{
					echo "<option value='".$city->id."'>".$city->city_name."</option>";
				}
			}
			else
			{
				echo "<option value=''>City not found</option>";
			}	
		}			
	}
	
	function logout()
	{
		$this->session->set_userdata(array('USERID' => '','USEREMAIL' => '','LOGINBY' => '','USERLOGIN' => ''));
		$this->session->sess_destroy();
		$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully logout.</div>');
		redirect('index.php/');	
	}
	
	function createWalletStatementInPdf()
	{
		$args = func_get_args();
		$user_login_id = $this->session->userdata('USERID');
		if(count($args)>0)
		{
			$neftno = ($args[0]!=0)?$args[0]:'';
			$first_date = ($args[1]!=0)?date('Y-m-d',$args[1]):date('Y-m-d'); 			////date('Y-m-d',$args[1]);
			$second_date = ($args[2]!=0)?date('Y-m-d'):date('Y-m-d',strtotime('+7 days'));   //date('Y-m-d',$args[2]);
		}
		else
		{
			$first_date = date('Y-m-d');
			$second_date = date('Y-m-d',strtotime('+7 days'));
			$neftno = "";
		}		
		
		$data['walletData'] = $this->user_model->getWalletStatementHistory($neftno,$user_login_id,$first_date,$second_date);			
		// Load all views as normal
		$this->load->view('front/wallet-statement',$data);		
		// Get output html
		$html = $this->output->get_output();		
		// Load library
		$this->load->library('dompdf_gen');		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream(date('d_m_Y_').time()."bank-statement.pdf");		
	}
	
	function apply_promotional_income($user_id)
	{
			$this->load->model('user_model');			
			//$user_id = 1001;
			$user = $this->user_model->selectUserByID($user_id);    // get user data by id
			$lastdate_promotional = $user[0]->promotinalincomme_lastdate;  // last date amount
			$carryuseramount = $user[0]->promotional_carry_amount;  // carryforword amount
			$carryforwarduserside = $user[0]->carry_forword_side;   // carryforword side
			$left_carry_amount=0;
			$right_carry_amount=0;
			if($carryforwarduserside!=NULL)
			{
				if($carryforwarduserside=='L')
				{
					$left_carry_amount=$carryuseramount;
					$right_carry_amount=0;
				}
				
				if($carryforwarduserside=='R')
				{
					$left_carry_amount=0;
					$right_carry_amount=$carryuseramount;
				}
			}	
			
			$left_income  = $this->user_model->getBonusIncome($lastdate_promotional,$user_id,'L') + $left_carry_amount;	// get left bussiness
			$right_income  = $this->user_model->getBonusIncome($lastdate_promotional,$user_id,'R') + $right_carry_amount;  // get right bussiness
			
			//echo $left_income; die();	
			$highestAmount = max($left_income, $right_income);
			$lowerAmount = min($left_income, $right_income);
			$income = $this->get_promotional_income($lowerAmount);
			$final_income =  $this->applytax($income);
			$carry_position = 'NULL';
			$carry = 0;
			
			if($highestAmount==$left_income)
			{
				$carry_position = 'L';
				$carry_left_amount = abs($left_income - $right_income);
				$carry = abs($left_income - $right_income);
				$carry_right_amount = 00;
			}
			else
			{
				$carry_position = 'R';
				$carry_left_amount = 00;
				$carry_right_amount = abs($left_income - $right_income);
				$carry = abs($left_income - $right_income);
			}
			if($income!=0 && !empty($income))
			{
				//$this->user_model->updateUserInvoiceStatus($user_id);
				$inv_data['uid'] = $user_id;
				$inv_data['left_bv'] = $left_income;
				$inv_data['right_bv'] = $right_income;
				$inv_data['clbv'] = $carry_left_amount;
				$inv_data['crbv'] = $carry_right_amount;
				$inv_data['income'] = $income;
				$inv_data['tds'] = 10;
				$inv_data['admincharge'] = 5;
				$inv_data['netIncome'] = $final_income;
				$inv_data['status'] = 'pending';
				$inv_data['crDate'] = date('Y-m-d');
				$this->user_model->insertBonusIncome($inv_data);
				
				$update_user['promotinalincomme_lastdate']= date('Y-m-d');
				$update_user['promotional_carry_amount']= $carry;
				$update_user['carry_forword_side']= $carry_position;
				$this->user_model->updateData($user_id,$update_user);	
			}	
		
			//echo 'carry_left_amount'.$carry_left_amount.'<br>'.'carry_right_amount'.$carry_right_amount;
	}	
	function applytax($amount)
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$admin_charge = $tax[0]->admin_charge; //5;
		$tax = $tax[0]->tds;  //10;
		$total_tax= $admin_charge+$tax;
		$tax_amount = ($amount*$total_tax)/100;
		return $tax_amount+$amount;
	}
	function get_promotional_income($amount)
	{
		$tax = $this->admin_model->getAllTaxes(1);
		$amount_per = $tax[0]->promotinalincomme;  //8;
		$final_amount = ($amount*$amount_per)/100;
		return $final_amount;
	}
	
	
	function booster_activate($uid)
	{
			$this->load->model('booster_model');						
			$user_id = $uid;
			$user = $this->user_model->selectUserByID($user_id);    // get user data by id
			if($user[0]->rdate < $this->getboosterexpiredate($user[0]->rdate))
			{
				if($user[0]->boosterused==0)
				{
					$user_invoice_total = $this->getTotalOfAllInvoice($user_id);
					if($user_invoice_total>0)
					{
						$left_income  = $this->booster_model->getBonusIncome($user_id,'L');	// get left bussiness
						$right_income  = $this->booster_model->getBonusIncome($user_id,'R');  // get right bussiness
						
						$lowerAmount = $this->getLowerAmount($left_income,$right_income);             
						//$boosterlink = round($lowerAmount*($user[0]->linkPerDay/$user_invoice_total));						
						
						$finallink = $this->returnfinallink($lowerAmount,$user_invoice_total,$user[0]->linkPerDay);
						//echo $finallink;
						
						if($finallink!=0 && !empty($finallink))
						{	
							$update_user['boosterused'] = 0;
							$update_user['boosterStatus'] = 1;
							$update_user['booster_task'] = $finallink;
							$this->user_model->updateData($user_id,$update_user);	
						}
					}				
				}
			}	
								
			
	}

	function returnfinallink($lowerAmount,$user_invoice_total,$linkperday)
	{
		if($lowerAmount<=$user_invoice_total)
		{
			return round($lowerAmount*($linkperday/$user_invoice_total));
		}
		else
		{
			return $linkperday;
		}
	}
	
	function getboosterexpiredate($date)
	{
		//$date = date("Y-m-d");
		$newDate = date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +20 day"));
		return $newDate;		
	}
	
	function getTotalOfAllInvoice($userid)
	{
		$invoiceData = $this->booster_model->getUserInvoiceByUserID($userid);
		$bonus =0;
		if(count($invoiceData)>0)
		{
			foreach($invoiceData as $inv)
			{
				if($inv->status=='paid')
				{
					$bonus += $inv->amount;
				}						
			}					
		}
		return $bonus;
	}
	
	function getLowerAmount($amount_one,$amount_two)
	{
		return min($amount_one,$amount_two);
	}
	
}

