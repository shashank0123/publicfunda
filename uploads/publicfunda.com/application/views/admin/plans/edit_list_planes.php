 <?php $this->load->view('admin/layout/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/style-crop.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/jquery.Jcrop.min.css" type="text/css" />
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2> Edit Plan</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Plan</span></a></li>
								<li><a href=""><span>Edit Plan</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>                           
							<section class="panel">
							
							<div class="panel-body">								
								<div class="table-responsive">
									<form  method="POST" action="">
										<table class="table table-bordered table-striped table-condensed" style="">										
											<tbody>
											<?php //print_r($PLAN); ?>
												<tr>
													<td>Plan Name</td>
													<td><input type="text" name="planName" class="form-control" value="<?php echo $PLAN[0]->planName; ?>"></td>
												</tr>
												<tr>
													<td>Plan Fees</td>
													<td><input type="text" name="planFees" class="form-control" value="<?php echo $PLAN[0]->planFees; ?>"></td>
												</tr>
												<tr>
													<td>Total Visitors</td>
													<td><input type="text" name="totalVisitors" class="form-control" value="<?php echo $PLAN[0]->totalVisitors;?>"></td>
												</tr>
												<tr>
													<td>Validity</td>
													<td><input type="text" name="validity" class="form-control" value="<?php echo $PLAN[0]->validity;?>"></td>
												</tr>
												<tr>
													<td>Link Per Day</td>
													<td><input type="text" name="linkPerDay" class="form-control" value="<?php echo $PLAN[0]->linkPerDay;
													?>">
												</tr>
												<tr>
													<td>Earning Per Click</td>
													<td><input type="text" name="earningPerClick" class="form-control" value="<?php echo $PLAN[0]->earningPerClick; ?>"></td>
												</tr>
												<tr>
													<td>Capping</td>
													<td><input type="text" name="capping" class="form-control" value="<?php echo $PLAN[0]->capping;?>"></td>
												</tr>
												<tr>
													<td>Turnover</td>
												<td><input type="text" name="turnover" class="form-control" value="<?php echo $PLAN[0]->turnover;?>"></td>
												</tr>
												
												<input type="hidden" name="plancode" value="<?php echo $PLAN[0]->plancode; ?>">
												<tr>
													<td>Payment Link</td>
													<td><input type="text" name="paymentLink" class="form-control" value="<?php echo $PLAN[0]->paymentLink;?>"> </td>
												</tr>
												<tr>
													<td>Status</td>
													<td>
														<select name="status" class="form-control">
															<option <?php echo($PLAN[0]->status==1)?'selected':''; ?> value="1">Active</option>
															<option <?php echo($PLAN[0]->status==0)?'selected':''; ?> value="0">Inctive</option>
														</select>
													</td>
												</tr>
												<tr>
													<td colspan="2"><button type="submit" class="btn btn-info" name="edit_list_planes">Submit</button></td>
												</tr>
							
											</tbody>
										</table>
									</form>
									
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>
	
		<!--<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).on('click','.status_checks',function(){
var status = ($(this).hasClass("btn-success")) ? '0' : '1';
var msg = (status=='0')? 'Deactivate' : 'Activate';
if(confirm("Are you sure to "+ msg)){
	var current_element = $(this);
	url = "ajax.php";
	$.ajax({
	type:"POST",
	url: url,
	data: {id:$(current_element).attr('data'),status:status},
	success: function(data)
		{   
			location.reload();
		}
	});
	}      
});
</script>-->
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		
		<script src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
		
		
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.tabletools.js"></script>
		
	
	<!-- The popup for upload new photo -->
    <div id="popup_upload" style="display:none;">
        <div class="form_upload">
            <span class="close" onclick="close_popup('popup_upload')">x</span>
            <h2>Upload photo</h2>
            <form action="<?php echo base_url('index.php/user/imagecropsystem/'); ?>" method="post" enctype="multipart/form-data" target="upload_frame" onsubmit="submit_photo()">
                <input type="file" name="photo" id="photo" class="file_input">
                <div id="loading_progress"></div>
                <input type="submit" value="Upload photo" id="upload_btn">
            </form>
            <iframe name="upload_frame" class="upload_frame"></iframe>
        </div>
    </div>

    <!-- The popup for crop the uploaded photo -->
    <div id="popup_crop" style="display:none;">
        <div class="form_crop">
            <span class="close" onclick="close_popup('popup_crop')">x</span>
            <h2>Crop photo</h2>
            <!-- This is the image we're attaching the crop to -->
            <img id="cropbox" />            
            <!-- This is the form that our event handler fills -->
            <form>
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <input type="hidden" id="photo_url" name="photo_url" />
                <input type="button" value="Crop Image" id="crop_btn" onclick="crop_photo(800,500)" />
            </form>
        </div>
    </div>	
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>