									<table border='1'>
										<thead>											
											<tr>
												<th>S.No</th>
                                                <th>Date Time</th>
												<th>Action USER ID</th>
												<th>NEFT</th>
                                                <th>Particular</th>
												<th>Debit</th>
												<th>Credit</th>
												<th>Total Balance</th>												
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php if(count($walletData)>0){ $s=0; ?>
											<?php foreach($walletData as $wd){ $s++; ?>
											<?php 
												if($wd->tranType=="credit")
												{
													$type= 'CR';													
												} 
												else
												{
													$type= 'DR';													
												}													
											?>
											<tr>
												<td><?php echo $s; ?></td>
												<td><?php echo $wd->trDate.' '. date('h:i:s A',$wd->trTime); ?></td>
												<td><?php echo($wd->another_user!="0")?$wd->another_user:'N/A'; ?></td>
												<td><?php echo($wd->neftNo!="0" && $wd->neftNo!="")?$wd->neftNo:'N/A'; ?></td>
												<td><?php echo $wd->information; ?></td>
												<td><?php echo($wd->debit_amount!="")?$wd->debit_amount:'N/A'; ?></td>
												<td><?php echo($wd->credit_amount!="")?$wd->credit_amount:'N/A'; ?></td>
												<td>																								
												<?php echo $wd->finalAmount.' '.$type; ?></td>												
												<td><?php echo $wd->status; ?></td>
											</tr>
											<?php } }else{ ?>
											<tr>
												<td colspan="9">
													<div class="alert alert-danger"><strong>Record Not Found</strong></div>
												</td>
											</tr>	
											<?php } ?>
										</tbody>										
									</table>                               