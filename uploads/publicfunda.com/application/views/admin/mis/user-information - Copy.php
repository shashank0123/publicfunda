<h2>Personal Details</h2>
<table width="100%">
	<tr>
		<th>Name</th>
		<th>Email</th>
		<th>Mobile</th>
		<th>Nominee</th>
		<th>Gender</th>
		<th>Address</th>
		<th>City</th>
		<th>State</th>
		<th>Pin Code</th>
		<th>Colleges</th>
		<th>School</th>
		<th>Mobile Models</th>
		<th>Highest Degree</th>			
	</tr>
	<tr>
		<td><?php echo $userdata[0]->name; ?></td>
		<td><?php echo $userdata[0]->email; ?></td>
		<td><?php echo $userdata[0]->contact_no; ?></td>
		<td><?php echo $userdata[0]->nominee; ?></td>
		<td><?php echo $userdata[0]->gender; ?></td>
		<td><?php echo $userdata[0]->address; ?></td>
		<td><?php echo $userdata[0]->city; ?></td>
		<td><?php echo $userdata[0]->status; ?></td>
		<td><?php echo $userdata[0]->pincode; ?></td>
		<td><?php echo $userdata[0]->colleges; ?></td>
		<td><?php echo $userdata[0]->schools; ?></td>
		<td><?php echo $userdata[0]->mobile_handset; ?></td>
		<td><?php echo $userdata[0]->highest_degree; ?></td>							
	</tr>
</table>
<table width="100%">
	<tr>
		<th>Extra Skills</th>
		<th>Favorite Celebrity</th>	
		<th>DOB</th>
		<th>Profession</th>
		<th>Industry</th>
		<th>Current Industry</th>
		<th>Company Name</th>
		<th>Department</th>
		<th>Hobbies</th>
		<th>Education</th>
		<th>Salary</th>
		<th>Turn Over</th>		
		<th>Account Status</th>
		<th>Booster Status</th>				
	</tr>
	<tr>
		<td><?php echo $userdata[0]->extra_skills; ?></td>
		<td><?php echo $userdata[0]->celebrity; ?></td>
		<td><?php echo $userdata[0]->dob; ?></td>
		<td><?php echo $userdata[0]->profession; ?></td>
		<td><?php echo $userdata[0]->industry; ?></td>
		<td><?php echo $userdata[0]->current_industry; ?></td>
		<td><?php echo $userdata[0]->company_name; ?></td>
		<td><?php echo $userdata[0]->field_work; ?></td>
		<td><?php echo $userdata[0]->hobbies; ?></td>
		<td><?php echo $userdata[0]->education; ?></td>
		<td><?php echo $userdata[0]->salary; ?></td>
		<td><?php echo $userdata[0]->turn_over; ?></td>
		<td><?php echo($userdata[0]->status==1)?'Active':'Inactive'; ?></td>
		<td>Inactive</td>			
	</tr>
</table>

<h2>Bank Details</h2>
<table width="100%">
	<tr>		
		<th>Account Holder</th>
		<th>Bank Name</th>
		<th>Branch Name</th>
		<th>Account No</th>	
		<th>IFSC</th>		
	</tr>
	<tr>		
		<td><?php echo $userdata[0]->account_holder; ?></td>
		<td><?php echo $userdata[0]->bank_name; ?></td>
		<td><?php echo $userdata[0]->branch_name; ?></td>
		<td><?php echo $userdata[0]->bank_accountno; ?></td>
		<td><?php echo $userdata[0]->bank_ifsccode; ?></td>	
	</tr>
</table>
<h2>Campaign Information</h2>
<?php
	$pending_points = 0; 
	foreach($this->campaign_model->selectCampaignByUserID($userdata[0]->id) as $campData)
	{
		$pending_points +=$campData->campaignPoints; 
	}
	$left_points = ($userdata[0]->totalPoints+$userdata[0]->paidPoints)-$pending_points;
?>
<table width="100%">
	<tr>		
		<th>Campaign Status</th>
		<th>Ponts</th>
		<th>Purchase</th> 
		<th>Pending Points</th>
		<th>Campaign Status</th>
		<th>Campaign Status</th>
		<th>Number of Active Campaign</th>
		<th>Number of Inactive Campaign</th> 
		<?php $r=0; foreach($this->campaign_model->selectCampaignByUserID($userdata[0]->id) as $campaignData){ $r++; ?>
		<th>Campaign <?php echo $r; ?></th>
		<?php } ?>
	</tr>
	<tr>		
		<td><?php echo($userdata[0]->bank_ifsccode==1)?'Active':'Inactive'; ?></td>
		<td><?php echo $userdata[0]->totalPoints; ?></td>
		<td><?php echo $userdata[0]->paidPoints; ?></td>
		<td><?php echo $left_points; ?></td>	
		<td><?php echo count($this->campaign_model->selectCampaignByUserIDActiveOrInactive($userdata[0]->id,1)); ?></td>
		<td><?php echo count($this->campaign_model->selectCampaignByUserIDActiveOrInactive($userdata[0]->id,0)); ?></td>
		<?php foreach($this->campaign_model->selectCampaignByUserID($userdata[0]->id) as $campaignData){ ?>
		<td><?php echo($campaignData->campaignStatus==1)?'Active':'Inactive'; ?></td>
		<?php } ?>
	</tr>
</table>
<h2>Wallet</h2>
<table width="100%">
	<tr>		
		<th>Wallet balance</th>
		<th>Total send tofriend</th>	
		<th>Total received from friend</th>
		<th>Total send to bank</th>	
		<th>Total redeem points</th>
		<th>Total redeem amount</th>
		<th>Total credit into wallet</th>
		<th>Total debit from wallet</th>	
	</tr>
	<tr>		
		<td><?php echo $userdata[0]->wallet; ?></td> 
		<td><?php echo $this->user_model->userWalletInfoForMis($userdata[0]->id,'send money to friend'); ?></td>
		<td><?php echo $this->user_model->userWalletInfoForMis($userdata[0]->id,'Receive from friend'); ?></td>
		<td><?php echo $this->user_model->userWalletInfoForMis($userdata[0]->id,'send money to bank'); ?></td>
		<td><?php echo $this->user_model->totalRedeempoints($userdata[0]->id,'points'); ?></td>
		<td><?php echo $this->user_model->totalRedeempoints($userdata[0]->id,'amount'); ?></td>
		<td><?php echo $this->user_model->userTotalCrDrAmount($userdata[0]->id,'cr'); ?></td> 
		<td><?php echo $this->user_model->userTotalCrDrAmount($userdata[0]->id,'dr'); ?></td> 
	</tr>
</table>

<h2>Team Information</h2>
<table width="100%">
	<tr>		
		<th>Total active of left</th>
		<th>Total inactive of left</th>	
		<th>Total active of right</th>
		<th>Total inactive of right</th>		
		<th>Total right sale</th>
		<th>Total left sale</th>		
	</tr> <!--- getTreeInformation($id,$pos,$status)  --->
	<tr>		
		<td><?php echo count($this->user_model->getTreeInformation($userdata[0]->id,'L',1)); ?></td> 
		<td><?php echo count($this->user_model->getTreeInformation($userdata[0]->id,'L',0)); ?></td>
		<td><?php echo count($this->user_model->getTreeInformation($userdata[0]->id,'R',1)); ?></td>
		<td><?php echo count($this->user_model->getTreeInformation($userdata[0]->id,'L',0)); ?></td>
		<td><?php echo $this->user_model->getBonusIncome($userdata[0]->id,'R'); ?></td>
		<td><?php echo $this->user_model->getBonusIncome($userdata[0]->id,'L'); ?></td>
	</tr>
</table>
