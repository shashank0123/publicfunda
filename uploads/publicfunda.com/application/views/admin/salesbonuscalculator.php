 <?php $this->load->view('admin/layout/header'); ?>

<!-- Theme CSS -->
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Sales Bonus Calculator</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Sales Bonus Calculator</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->
					<div class="row">						
						<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>                           
							<section class="panel">							
							<div class="panel-body">
								<form class="form-horizontal" method="post">
											<h4 class="mb-xlg"></h4>
											<fieldset>
												<div class="form-group">
												   <div class="col-md-2">
														<input type="text" class="form-control" name="userid" required value="<?php echo(isset($_POST['userid']))?$_POST['userid']:''; ?>" placeholder="Sponsor ID"> 
													</div>
													<div class="col-md-2">
														<input type="number" class="form-control" name="days" required value="<?php echo(isset($_POST['days']))?$_POST['days']:''; ?>" placeholder="Enter Days">                                                        
													</div>
                                                    <div class="col-md-3">
														<button type="submit" name="searchMyTeam" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
														<button type="button" onclick="window.location.reload()" class="btn btn-warning"><i class="fa  fa-repeat"></i> Reset </button>   
													</div>
											
												</div> 
                                                
											</fieldset>
											
											
											
											
											

										</form>
								 <div class="table-responsive">
									<table class="table table-bordered table-striped css-serial" id="datatable-default">
										<thead>
											<tr>
                                                <th>User ID</th>
                                                <th>Name</th>
												<th>Invoice Creation Date</th>
												<th>Invoice ID</th>
												<th>Transaction ID</th>
                                            	<th>Amount</th>
												<th>Position</th>
											</tr>
										</thead>
										<tbody>	
											<?php 
												if(isset($_POST['searchMyTeam']))
												{
													$uid = $_POST['userid'];
													//rdate
													$days = $_POST['days'];
													$user = $this->user_model->selectUserByID($uid);
													$user_joining_date = $user[0]->rdate;
													
													$sdate = $user_joining_date;
													$esdate = date("Y-m-d", strtotime("+$days days",strtotime($user_joining_date)));
													$status = 1;
													$postion = 'all';
													if($postion=='all')
													{
														$left_ids  = $this->team_model->getTreeLeftOrRight_withdirectid($uid,'L',$status,$sdate,$esdate);
														$right_ids  = $this->team_model->getTreeLeftOrRight_withdirectid($uid,'R',$status,$sdate,$esdate);
														$left_sale = 0;
														$right_sale = 0;
														$leftinv = 0;
														$rightinv = 0;
											?>
													<?php if(count($left_ids)>0){ ?>
													<?php foreach($left_ids as $luser){ ?>
														<?php $user = $this->user_model->selectUserByID($luser['ids']); ?>
														<?php $userInvData = $this->tree_model->selectUserInvoiceByFilter($user[0]->id); ?>
														<?php foreach($userInvData as $userInv){ $left_sale +=$userInv->amount; $leftinv +=1; ?>
														<tr>
															<td><?php echo $user[0]->id; ?></td>
															<td><?php echo $user[0]->name; ?></td>
															<td><?php echo $userInv->invDate;  ?></td>
															<td><?php echo(isset($userInv->invoiceID))?$userInv->invoiceID:'-'; ?></td>
															<td><?php echo(isset($userInv->transactionID))?$userInv->transactionID:'-'; ?></td>
															<td><?php echo(isset($userInv->amount))?$userInv->amount:'-'; ?></td>
															<td>Left</td>
														</tr>
													<?php }}} ?>
													<?php if(count($right_ids)>0){ ?>
													<?php foreach($right_ids as $ruser){  ?>
														<?php $user = $this->user_model->selectUserByID($ruser['ids']); ?>
														<?php $userInvData = $this->tree_model->selectUserInvoiceByFilter($user[0]->id); ?>
														<?php foreach($userInvData as $userInv){  $right_sale +=$userInv->amount; $rightinv +=1; ?>
														<tr>
															<td><?php echo $user[0]->id; ?></td>
															<td><?php echo $user[0]->name; ?></td>
															<td><?php echo $userInv->invDate;  ?></td>
															<td><?php echo(isset($userInv->invoiceID))?$userInv->invoiceID:'-'; ?></td>
															<td><?php echo(isset($userInv->transactionID))?$userInv->transactionID:'-'; ?></td>
															<td><?php echo(isset($userInv->amount))?$userInv->amount:'-'; ?></td>
															<td>Right</td>
														</tr>
													<?php }}}  ?>
													<tr>
															<td colspan="8"><b>Left Sale :</b><?php echo $left_sale; ?> | <b>Right Sale :</b><?php echo $right_sale; ?> | 
															<b>Pairing Amount :</b><?php echo min($left_sale,$right_sale); ?>
															<?php $tax = $this->admin_model->getAllTaxes(1); ?>
															| <b>Bonus :</b> <?php echo bunus($days,$leftinv,$rightinv,min($left_sale,$right_sale),$tax); ?>
															  || Status : 
															<form method="post" action="<?php echo base_url('admin/updateuserbonus'); ?>">  
																<select name='bonusstatus' <?php echo($user[0]->bonusstatus=='paid')?'disabled':''; ?>>
																	<option <?php echo($user[0]->bonusstatus=='unpaid')?'selected':''; ?> value='unpaid'>Unpaid</option>
																	<option <?php echo($user[0]->bonusstatus=='paid')?'selected':''; ?> value='paid'>Paid</option>
																</select>
																<input type="hidden" name="huid" value="<?php echo $user[0]->id; ?>" >
																<input type="text" name="bonusinvoice" value="<?php echo $user[0]->bonusinvoice; ?>" <?php echo($user[0]->bonusinvoice!='')?'readonly':''; ?>>
																<input type="submit" name="submitdata" <?php echo($user[0]->bonusstatus=='paid')?'disabled':''; ?> >
															</form>
															</td>
															
													</tr>
												<?php
													} 	
												}
												else
												{
													
												}
											?>											
										</tbody>
                                        	
                                            
									</table>
                                    
								</div>
							</div>
						</section>                         
						</div>
					</div>					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>	
<?php

function bunus($days,$leftinv,$rightinv,$amount,$tax)
{
	if($days<=$tax[0]->cond_oneday)
	{
		if($leftinv>=$tax[0]->cond_onesale && $rightinv>=$tax[0]->cond_onesale)
		{
			return ($amount*$tax[0]->cond_onebonus)/100 - ((($amount*$tax[0]->cond_onebonus)/100)*$tax[0]->tds)/100;
		}	
	}
	if($days<=$tax[0]->cond_twoday)
	{
		if($leftinv>=$tax[0]->cond_twosale && $rightinv>=$tax[0]->cond_twosale)
		{
			return ($amount*$tax[0]->cond_twobonus)/100 - ((($amount*$tax[0]->cond_twobonus)/100)*$tax[0]->tds)/100;
		}	
	}	
	if($days<=$tax[0]->cond_threeday)
	{
		if($leftinv>=$tax[0]->cond_threesale && $rightinv>=$tax[0]->cond_threesale)
		{
			return ($amount*$tax[0]->cond_threebonus)/100 - ((($amount*$tax[0]->cond_threebonus)/100)*$tax[0]->tds)/100;
		}	
	}
	if($days<=$tax[0]->cond_fourday)
	{
		if($leftinv>=$tax[0]->cond_foursale && $rightinv>=$tax[0]->cond_foursale)
		{
			return ($amount*$tax[0]->cond_fourbonus)/100 - ((($amount*$tax[0]->cond_fourbonus)/100)*$tax[0]->tds)/100;
		}	
	}
	if($days<=$tax[0]->cond_fiveday)
	{
		if($leftinv>=$tax[0]->cond_fivesale && $rightinv>=$tax[0]->cond_fivesale)
		{
			return ($amount*$tax[0]->cond_fivebonus)/100 - ((($amount*$tax[0]->cond_fivebonus)/100)*$tax[0]->tds)/100;
		}	
	}
	return 0;
}
?>	
<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		
		<script src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
				
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.tabletools.js"></script>
<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	</body>

</html>