<?php 
$admin_login_id = $this->session->userdata('ADMINID'); 
if(empty($admin_login_id))
{	
	redirect('index.php/admin/index'); 
}
?>
<!doctype html>
<html class="fixed js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling no-mobile-device custom-scroll sidebar-left-xs js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling sidebar-left-collapsed">
<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>Samridh Bharat</title>
		<meta name="keywords" content="Samridh Bharat" />
		<meta name="description" content="Samridh Bharat">
		<meta name="author" content="Naseem Ahmad">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/css/datepicker3.css" />
		<!-- Specific Page Vendor CSS -->		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/morris/morris.css" />
		
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/select2/select2.css" />		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/stylesheets/theme.css" />
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/stylesheets/theme-custom.css">
		<!-- Head Libs -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/modernizr/modernizr.js"></script>
	</head>
	<body>
		<section class="body">
			<!-- start: header -->
			<header class="header">
				<div class="logo-container" style="    float: left;">
					<a href="<?php echo base_url('index.php/admin/dashboard'); ?>" class="logo">
						<img src="<?php echo base_url(); ?>assets/front/images/logo.png"  alt="Porto Admin" width="90" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">						
			
					<span class="separator"></span>	
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="<?php echo base_url('assets/front/images/%21logged-user.jpg'); ?>"  class="img-circle" />
							</figure>
							<div class="profile-info">
								<span class="name">Admin<?php //echo $login_user[0]->name; ?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<?php $admin_type = $this->session->userdata('ADMINTYPE');  ?>
								<li><a role="menuitem" tabindex="-1" href="<?php echo base_url('index.php/admin/admin_setting'); ?>"><i class="fa fa-user"></i> Settings </a></li>
								<?php if($admin_type==1){ ?>
								<li><a role="menuitem" tabindex="-1" href="<?php echo base_url('index.php/admin/manage_subadmin'); ?>"><i class="fa fa-user"></i>Manage Sub Admin</a></li>
								<?php } ?>
								<li><a role="menuitem" tabindex="-1" href="<?php echo base_url('admin/admin_logout'); ?>"><i class="fa fa-power-off"></i> Logout </a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->