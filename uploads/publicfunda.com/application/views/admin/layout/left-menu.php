<?php 
$admin_login_id = $this->session->userdata('ADMINID'); 
$admin_data = $this->admin_model->selectAdminById($admin_login_id);
if($admin_data[0]->type==2)
{
	$module_array = explode(",",$admin_data[0]->module);
	$users = (in_array('users', $module_array))?'1':'0';
	$plan = (in_array('plan', $module_array))?'1':'0';
	$rewards = (in_array('rewards', $module_array))?'1':'0';
	$helpdesk = (in_array('helpdesk', $module_array))?'1':'0';
	$bookadd = (in_array('bookadd', $module_array))?'1':'0';
	$purchasecalculator = (in_array('purchasecalculator', $module_array))?'1':'0';
	$bookingcalculator = (in_array('bookingcalculator', $module_array))?'1':'0';
	$banner = (in_array('banner', $module_array))?'1':'0';
	$dashboard = (in_array('dashboard', $module_array))?'1':'0';
	$staticblock = (in_array('staticblock', $module_array))?'1':'0';
	$workhistory = (in_array('workhistory', $module_array))?'1':'0';
	$comments = (in_array('comments', $module_array))?'1':'0';
	$invoicecontent = (in_array('invoicecontent', $module_array))?'1':'0';
	$alltransactiondetails = (in_array('alltransactiondetails', $module_array))?'1':'0';
	$managepromotionalincome = (in_array('managepromotionalincome', $module_array))?'1':'0';
	$tax_settings = (in_array('tax_settings', $module_array))?'1':'0';
	$managewalletstatement = (in_array('managewalletstatement', $module_array))?'1':'0';
	$popup = (in_array('popup', $module_array))?'1':'0';	
	$trackadminwork = (in_array('trackadminwork', $module_array))?'1':'0';
	$useractivationpanel= (in_array('useractivationpanel', $module_array))?'1':'0';   
	
	$campaignreport= (in_array('campaignreport', $module_array))?'1':'0'; 	
	$salesreport== (in_array('salesreport', $module_array))?'1':'0'; 
}
else
{
	$module_array = 1;
	$users = 1;
	$plan = 1;
	$rewards = 1;
	$helpdesk = 1;
	$bookadd = 1;
	$purchasecalculator = 1;
	$bookingcalculator = 1;
	$banner = 1;
	$dashboard = 1;
	$staticblock = 1;
	$workhistory = 1;
	$comments = 1;
	$invoicecontent = 1;
	$alltransactiondetails = 1;
	$managepromotionalincome = 1;
	$tax_settings = 1;
	$managewalletstatement = 1;	
	$popup = 1;
	$trackadminwork =1;
    $useractivationpanel= 1;
	$campaignreport=1;
	$salesreport =1;
}
//print_r($admin_data);
?>
<aside id="sidebar-left" class="sidebar-left">				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li class="nav-active">
										<a href="<?php echo base_url('index.php/admin/dashboard'); ?>">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<?php if($salesreport==1){ ?>
									<li>
										<a href="<?php echo base_url('index.php/admin/salesreport'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Sponsor ID Sales Report</span>
										</a>
									</li>
									<?php } ?>
									<li>
										<a href="<?php echo base_url('index.php/admin/salesbonuscalculator'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Sales Bonus Calculator</span>
										</a>
									</li>
									<?php if($users==1){ ?>
									<li>
										<a href="<?php echo base_url('index.php/admin/list_user'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Users</span>
										</a>
									</li>
									<?php } ?>
									<?php if($useractivationpanel==1){ ?>
									<li>
										<a href="<?php echo base_url('index.php/admin/useractivationpanel'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Users Activation Panel</span>
										</a>
									</li>
									<?php } ?>
									<?php if($campaignreport==1){ ?>
									<li>
										<a href="<?php echo base_url('index.php/admin/managecampaignreport'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Campaign Report</span>
										</a>
									</li>
									<?php } ?>
									<?php if($trackadminwork==1){ ?>
									<li>
										<a href="<?php echo base_url('index.php/trackadminwork/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Trace Sub Admin</span>
										</a>
									</li>
									<?php } ?>
									<?php if($plan==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/admin/list_plans'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Plan</span>
										</a>
									</li>
									<?php } ?>
									<?php if($rewards==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/rewards/listing/common'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Rewards</span>
										</a>
									</li>
									<?php } ?>
									<?php if($helpdesk==1){ ?>		
									<li>
										<a href="<?php echo base_url('index.php/helpdesk/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Helpdesk</span>
										</a>
									</li>
									<?php } ?>
									<?php if($bookadd==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/bookadd/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Book Your Add</span>
										</a>
									</li>
									<?php } ?>
									<?php if($purchasecalculator==1){ ?>		
									<li>
										<a href="<?php echo base_url('index.php/purchasecalculator/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Purchase Calculator</span>
										</a>
									</li>
									<?php } ?>
									<?php if($bookingcalculator==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/bookingcalculator/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Book Your Add Calculator</span>
										</a>
									</li>
									<?php } ?>
									<?php if($banner==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/banner/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Slider</span>
										</a>
									</li>
									<?php } ?>
									<?php if($dashboard==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/dashboard/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Dashboard</span>
										</a>
									</li>
									<?php } ?>
									<?php if($staticblock==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/staticblock/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Static Block</span>
										</a>
									</li>
									<?php } ?>
									<?php if($workhistory==1){ ?>		
									<li>
										<a href="<?php echo base_url('index.php/admin/workhistory'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Work History</span>
										</a>
									</li>
									<?php } ?>
									<?php if($comments==1){ ?>		
									<li>
										<a href="<?php echo base_url('index.php/comments/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Comments</span>
										</a>
									</li>
									<?php } ?>
									<?php if($invoicecontent==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/invoicecontent/edit'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Invoice Content</span>
										</a>
									</li>
									<?php } ?>
									<?php if($alltransactiondetails==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/admin/alltransactiondetails'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Transaction Details</span>
										</a>
									</li>
									<?php } ?>
									<?php if($managepromotionalincome==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/admin/managepromotionalincome'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Promotional Income</span>
										</a>
									</li>
									<?php } ?>
									<?php if($popup==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/popup/listing'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Popup</span>
										</a>
									</li>
									<?php } ?>
									<?php if($tax_settings==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/admin/tax_settings'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Manage Settings</span>
										</a>
									</li>
									<?php } ?>
									<?php if($managewalletstatement==1){ ?>	
									<li>
										<a href="<?php echo base_url('index.php/admin/managewalletstatement'); ?>">
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Download Wallet Statement</span>
										</a>
									</li>
									<?php } ?>	
								</ul>
							</nav>
				
						</div>
				
					</div>
				
				</aside>