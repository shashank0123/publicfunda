<?php $this->load->view('front/layout/header-inner'); ?>
<style>
.css-serial {
  counter-reset: serial-number;  /* Set the serial number counter to 0 */
}

.css-serial td:first-child:before {
  counter-increment: serial-number;  /* Increment the serial number counter */
  content: counter(serial-number);  /* Display the counter */
}
</style>
	<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>My Team</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>My Team</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12">
							<section class="panel">
									<header class="panel-heading">	
											<h2 class="panel-title">My Team
												<button class="pull-right btn btn-info "><strong>Total Members</strong> Left : <?php echo count($this->tree_model->countTotalMembers($login_user[0]->id,'L')); ?> | Right : <?php echo count($this->tree_model->countTotalMembers($login_user[0]->id,'R')); ?></button>									
											
											</h2> 
									</header>
									
									<div class="panel-body" >
									<form class="form-horizontal" method="post">
											<h4 class="mb-xlg"></h4>
											<fieldset>
												
												<div class="form-group">
                                                													
													<div class="col-md-5">
														<div class="input-daterange input-group" data-plugin-datepicker="" >
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="start" required value="<?php echo(isset($_POST['start']))?$_POST['start']:''; ?>">
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="end" required value="<?php echo(isset($_POST['end']))?$_POST['end']:''; ?>">                                                        
														</div>
													</div>
                                                    <!---
														<div class="col-md-2">
															<select class="form-control mb-md">
																<option>Select Registration Type</option>
																<option>Direct</option>
																<option>All</option>                                                      
															</select>                                                        
														</div>
													--->
                                                    <div class="col-md-3">
														<select class="form-control mb-md" name="status" required>
															<option value="">Select Account Status</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='all')?'selected':''; ?> value="all">All</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='1')?'selected':''; ?> value="1">Active</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='0')?'selected':''; ?> value="0">Inactive</option>
														</select>                                                        
													</div>
                                                    <div class="col-md-2">
														<select class="form-control mb-md" name="position" required>
															<option value="">Select Position</option>															
															<option <?php echo(isset($_POST['position']) && $_POST['position']=='L')?'selected':''; ?> value="L">Left</option>
															<option <?php echo(isset($_POST['position']) && $_POST['position']=='R')?'selected':''; ?> value="R">Right</option>
															<option <?php echo(isset($_POST['position']) && $_POST['position']=='all')?'selected':''; ?> value="all">Both</option>
														</select>                                                        
													</div>
                                                    
                                                    <div class="col-md-2">
														<button type="submit" name="searchMyTeam" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
														<button type="button" onclick="window.location.reload()" class="btn btn-warning"><i class="fa  fa-repeat"></i> Reset </button>   
													</div>
												</div>                                                
											</fieldset>											
										</form>                                        
                                        <div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed mb-none css-serial">
										<thead>
											<tr>
												<th>S.No</th>
                                                <th>User ID</th>
                                                <th>Name</th>
												<th>Plan</th>
                                            	<th>Status</th>
                                                <th>Activation Date</th>
                                                <th>Booster Status</th>
                                                <th>Position</th>
											</tr>
										</thead>
										<tbody>	
											<?php 
												if(isset($_POST['searchMyTeam']))
												{
													$sdate = date('Y-m-d',strtotime($this->input->post('start')));
													$esdate = date('Y-m-d',strtotime($this->input->post('end')));
													$status = $this->input->post('status');
													$postion = $this->input->post('position');
													if($postion=='all')
													{
														$left_ids  = $this->team_model->getTreeLeftOrRight($login_user[0]->id,'L',$status,$sdate,$esdate);
														$right_ids  = $this->team_model->getTreeLeftOrRight($login_user[0]->id,'R',$status,$sdate,$esdate);
											?>
													<?php if(count($left_ids)>0){ ?>
													<?php foreach($left_ids as $luser){ ?>
														<?php $user = $this->user_model->selectUserByID($luser['ids']); ?>
														<?php $planData = $this->tree_model->selectPlanByID($user[0]->plan); ?> 
														<tr>
															<td><?php //echo $k; ?></td>
															<td><?php echo $user[0]->id; ?></td>
															<td><?php echo $user[0]->name; ?></td>
															<td><?php echo $planData[0]->planName; ?></td>
															<td><?php echo($user[0]->status==1)?'Active':'Inactive'; ?></td>
															<td><?php echo date('d-M-Y',$user[0]->registration_date); ?></td>
															<td><?php echo($user[0]->boosterStatus==1)?'Active':'Inactive'; ?></td>
															<td>Left</td>
														</tr>
													<?php }}  ?>
													<?php if(count($right_ids)>0){ ?>
													<?php foreach($right_ids as $ruser){ ?>
														<?php $user = $this->user_model->selectUserByID($ruser['ids']); ?>
														<?php $planData = $this->tree_model->selectPlanByID($user[0]->plan); ?> 
														<tr>
															<td><?php //echo $k; ?></td>
															<td><?php echo $user[0]->id; ?></td>
															<td><?php echo $user[0]->name; ?></td>
															<td><?php echo $planData[0]->planName; ?></td>
															<td><?php echo($user[0]->status==1)?'Active':'Inactive'; ?></td>
															<td><?php echo date('d-M-Y',$user[0]->registration_date); ?></td>
															<td><?php echo($user[0]->boosterStatus==1)?'Active':'Inactive'; ?></td>
															<td>Right</td>
														</tr>
													<?php }}  ?>
												<?php
													} 	
													if($postion=='L') // close left position
													{
														$left_ids  = $this->team_model->getTreeLeftOrRight($login_user[0]->id,'L',$status,$sdate,$esdate);
												?>
													<?php if(count($left_ids)>0){ ?>
													<?php foreach($left_ids as $luser){ ?>
														<?php $user = $this->user_model->selectUserByID($luser['ids']); ?>
														<?php $planData = $this->tree_model->selectPlanByID($user[0]->plan); ?> 
														<tr>
															<td><?php //echo $k; ?></td>
															<td><?php echo $user[0]->id; ?></td>
															<td><?php echo $user[0]->name; ?></td>
															<td><?php echo $planData[0]->planName; ?></td>
															<td><?php echo($user[0]->status==1)?'Active':'Inactive'; ?></td>
															<td><?php echo date('d-M-Y',$user[0]->registration_date); ?></td>
															<td><?php echo($user[0]->boosterStatus==1)?'Active':'Inactive'; ?></td>
															<td>Left</td>
														</tr>
													<?php }}  ?>													
												<?php
													} // close left position
													if($postion=='R') // close right position
													{
														$right_ids  = $this->team_model->getTreeLeftOrRight($login_user[0]->id,'R',$status,$sdate,$esdate);
												?>
													<?php if(count($right_ids)>0){ ?>
													<?php foreach($right_ids as $ruser){ ?>
														<?php $user = $this->user_model->selectUserByID($ruser['ids']); ?>
														<?php $planData = $this->tree_model->selectPlanByID($user[0]->plan); ?> 
														<tr>
															<td><?php //echo $k; ?></td>
															<td><?php echo $user[0]->id; ?></td>
															<td><?php echo $user[0]->name; ?></td>
															<td><?php echo $planData[0]->planName; ?></td>
															<td><?php echo($user[0]->status==1)?'Active':'Inactive'; ?></td>
															<td><?php echo date('d-M-Y',$user[0]->registration_date); ?></td>
															<td><?php echo($user[0]->boosterStatus==1)?'Active':'Inactive'; ?></td>
															<td>Righ</td>
														</tr>
													<?php }}  ?>													
												<?php
													} // close right position
												}
												else
												{
													
												}
											?>
										</tbody>
                                        	
                                            
									</table>
                                    
								</div>
								</div>
								</section>                         
						</div>
						

					</div>
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
					<!-- end: page -->
				</section>
			</div>
			
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	</body>


</html>