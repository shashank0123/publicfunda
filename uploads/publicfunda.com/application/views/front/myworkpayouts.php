<?php //print_r($payoutdata); ?>
<?php $this->load->view('front/layout/header-inner'); ?>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>My Work Payouts</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>My Work Payouts</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12">										
								
							<div class="tabs">
								<ul class="nav nav-tabs tabs-primary">	
									<?php if($login_user[0]->myearnings=='1'){ ?> 	
                                    <li class="<?php echo(!isset($_POST['checkWallet']))?'active':''; ?>"><a href="#work-payouts" data-toggle="tab">My Earnings</a></li>
									<?php } if($login_user[0]->walletstatement=='1'){ ?> 
									 <li class="<?php echo(isset($_POST['checkWallet']))?'active':''; ?>"><a href="#campaign-info" data-toggle="tab">Wallet Account Statement</a></li>
									 <?php } ?> 
							   </ul>
								<div class="tab-content">	
                                <?php if($login_user[0]->myearnings=='1'){ ?>    
                               <div id="work-payouts" class="tab-pane <?php echo(!isset($_POST['checkWallet']))?'active':''; ?>">										
									<form class="form-horizontal" method="post">
											<h4 class="mb-xlg"></h4>
											<fieldset>												
												<div class="form-group">
													<div class="col-md-3"></div>													
													<div class="col-md-4 mb-md">
														<div class="input-daterange input-group" data-plugin-datepicker="">
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="startDate" value="<?php echo(isset($_POST['startDate']))?$_POST['startDate']:''; ?>" required >
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="endDate" value="<?php echo(isset($_POST['endDate']))?$_POST['endDate']:''; ?>" required >                                                        
														</div>
													</div>													                                                   
                                                    <div class="col-md-3">
														<button type="submit" name="ckeckWorkPayout" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
														<button type="button" onClick="window.location.reload()" name="ckeckWorkPayout" class="btn btn-warning"><i class="fa  fa-repeat"></i> Reset </button>
													</div>
												</div>                                                  
											</fieldset>											
										</form>
                                        
                                        <div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed mb-none">
										<thead>
											<tr>
												<th>S.No</th>
                                                <th>Date</th>
												<th>CTP Points</th>
                                                <th>Amount</th>
                                                <th>TDS</th>
                                                <th>Admin Charges</th>
                                                <th>Net Amount</th>
											</tr>
										</thead>
										<tbody>
										<?php if(count($payoutdata)>0){ ?>
											<?php $i=0; foreach($payoutdata as $paydata){ $i++;?>
											<tr>
												<td><?php echo $i; ?></td> 
                                                <td><?php echo date('d-m-Y',strtotime($paydata->redeemdate)); ?></td>
												<td><?php echo $paydata->points; ?></td>
												<td><i class="fa fa-rupee"></i> <?php echo $paydata->amount; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo ($paydata->amount*$paydata->tds)/100; ?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo ($paydata->amount*$paydata->admincharge)/100;?></td>
                                                <td><i class="fa fa-rupee"></i> <?php echo $paydata->finalAmount; ?></td>
                                            </tr>
											<?php } ?>
                                        <?php } ?>
										</tbody>                                       	
                                            
									</table>
                                    
								</div>						
									</div>
								<?php } ?>	
                                    
                                    <div id="campaign-info" class="tab-pane <?php echo(isset($_POST['checkWallet']))?'active':''; ?>">
										<?php 
											if(isset($_POST['checkWallet']))
											{
												$neftNo = ($_POST['neftNo']!="")?$_POST['neftNo']:'0';
												$sd = strtotime($_POST['start']);
												$ed = strtotime($_POST['end']);
											}
											else
											{
												$neftNo = 0;
												$sd = 0;
												$ed = 0;
											}	
										?>
										<form class="form-horizontal" method="post">
											<h4 class="mb-xlg"></h4>
											<fieldset>												
												<div class="form-group">													
													<div class="col-md-12  mb-md">
														<a href="<?php echo base_url('index.php/user/createWalletStatementInPdf/'.$neftNo.'/'.$sd.'/'.$ed);  ?>" class="btn btn-warning pull-right"><i class="fa  fa-repeat"></i> Download PDF </a>
													</div>	
													<div class="col-md-4">
														<input type="text" name="neftNo" placeholder="NEFT NO. / RTGS" class="form-control" value="<?php echo(isset($_POST['neftNo']))?$_POST['neftNo']:''; ?>">
													</div>
													<div class="col-md-6 mb-md" >
														<div class="input-daterange input-group" data-plugin-datepicker="">
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="start" value="<?php echo(isset($_POST['start']))?$_POST['start']:''; ?>" required>
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="end" value="<?php echo(isset($_POST['end']))?$_POST['end']:''; ?>" required>                                                        
														</div>
													</div>
                                                                                                     
                                                    <div class="col-md-2">
														<button type="submit" name="checkWallet" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
														<button type="button" onClick="window.location.reload()" class="btn btn-warning"><i class="fa  fa-repeat"></i> Reset </button>															
													</div>
												</div>                                                  
											</fieldset>
										</form>
                                        
                                        <div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed mb-none">
										<thead>											
											<tr>
												<th>S.No</th>
                                                <th>Date Time</th>
												<th>USER ID</th>
												<th>NEFT</th>
                                                <th>Particular</th>
												<th>Debit</th>
												<th>Credit</th>
												<th>Total Balance</th>												
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php if(count($walletData)>0){ $s=0; ?>
											<?php foreach($walletData as $wd){ $s++; ?>
											<?php 
												if($wd->tranType=="credit")
												{
													$type= 'CR';
													
												} 
												else
												{
													$type= 'DR';
													
												}
													
											?>
											<tr>
												<td><?php echo $s; ?></td>
												<td><?php echo $wd->trDate.' '. date('h:i:s A',$wd->trTime); ?></td>
												<td><?php echo($wd->another_user!="0")?$wd->another_user:'N/A'; ?></td>
												<td><?php echo($wd->neftNo!="0" && $wd->neftNo!="")?$wd->neftNo:'N/A'; ?></td>
												<td><?php echo $wd->information; ?></td>
												<td><?php echo($wd->debit_amount!="")?$wd->debit_amount:'N/A'; ?></td>
												<td><?php echo($wd->credit_amount!="")?$wd->credit_amount:'N/A'; ?></td>
												<td>
												<?php if($s==1){ ?>
												<?php if($type=='CR'){ $bf_amount = $wd->credit_amount-$wd->finalAmount;  }else{$bf_amount = $wd->finalAmount+$wd->credit_amount;} ?>
												<strong>Broad forward balance ( <?php echo $bf_amount; ?> )</strong><br>
												<?php } ?>
												
												<?php echo $wd->finalAmount.' '.$type; ?></td>												
												<td><?php echo $wd->status; ?></td>
											</tr>
											<?php } }else{ ?>
											<tr>
												<td colspan="9">
													<div class="alert alert-danger"><strong>Record Not Found</strong></div>
												</td>
											</tr>	
											<?php } ?>
										</tbody>
										
									</table>
                                    
								</div>
                                        
                                        

									</div>
                                   
								</div>
							</div> 
							
							
						</div>						

					</div>
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
					<!-- end: page -->
				</section>
			</div>
			
		</section>

									
		
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>

<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	</body>


</html>