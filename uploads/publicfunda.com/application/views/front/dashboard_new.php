 <?php $this->load->view('front/layout/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/style-crop.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/jquery.Jcrop.min.css" type="text/css" />
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body pt-0">	

					<!-- start: page -->
					<div class="row">
						<div class="col-md-6 col-lg-12 col-xl-6">
							<div class="row">
                            
								<div class="col-md-12">
									<section class="panel  ">
										<div class="thumb-info mb-md profile-sec">
											<span class="upload_btn" onclick="show_popup('popup_upload')">
												<img src="<?php echo base_url('uploads/user/'.$login_user[0]->image); ?>" class="rounded img-responsive" alt="<?php echo $login_user[0]->name; ?>">
											</span>
											<div class="thumb-info-title">
												<span class="thumb-info-inner">My Profile</span>
											</div>
											<div id="photo_container"></div>
										</div>
										<div class="cover-img">
										<?php if(count($image)>0){ ?>
										<?php foreach($image as $image){?>
										<img src="<?php echo base_url(); ?>uploads/image/<?php echo $image->image;?>"  class="img-responsive">
										<?php
										}
										}
										?>
										</div>
									</section>
								</div>
                            
								<div class="col-md-8 pull-right">
									<section class="panel panel-featured-left panel-featured-primary">
										<?php $this->load->view('front/layout/advertisement-slider'); ?>
									</section>
								</div>

								<?php if(count($Dashboard)>0){ ?>
								<?php foreach($Dashboard as $Dashboard){?>								
								<div class="col-md-4">
								<section class="panel panel-featured-left panel-featured-primary">
								<div class="panel-body">
								<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon <?php echo $Dashboard->class;?>">
								<i class="<?php echo $Dashboard->icone;?>"></i>
								</div>
								</div>
								<div class="widget-summary-col">
								<div class="summary">
								<a href="<?php echo base_url("$Dashboard->url"); ?>"><h2 class="title"><?php echo $Dashboard->title;?></h2></a>
								<div class="info">
								<p class="panel-subtitle mb-10"><?php echo $Dashboard->description;?></p>
								</div>
								</div>
								<div class="summary-footer">
								<a href="<?php echo base_url("$Dashboard->url"); ?>" class="btn <?php echo $Dashboard->class;?>">View Profile</a>
								</div>
								</div>
								</div>
								</div>
								</section>
								</div>
								<?php
								}
								}
								?>
                                
							 
                            </div>
						</div>
					</div>				
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
				</section>
			</div>			
			
		</section>
	
		
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
		
		<!-- crop image -->
		<!--<script type="text/javascript" src="<?php //echo base_url(); ?>assets/front/img-crop/js/jquery.min.js"></script>-->
		<script src="<?php echo base_url(); ?>assets/front/img-crop/js/jquery.Jcrop.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/img-crop/js/script-crop.js"></script>
		
		
	
	<!-- The popup for upload new photo -->
    <div id="popup_upload" style="display:none;">
        <div class="form_upload">
            <span class="close" onclick="close_popup('popup_upload')">x</span>
            <h2>Upload photo</h2>
            <form action="<?php echo base_url('index.php/user/imagecropsystem/'); ?>" method="post" enctype="multipart/form-data" target="upload_frame" onsubmit="submit_photo()">
                <input type="file" name="photo" id="photo" class="file_input">
                <div id="loading_progress"></div>
                <input type="submit" value="Upload photo" id="upload_btn">
            </form>
            <iframe name="upload_frame" class="upload_frame"></iframe>
        </div>
    </div>

    <!-- The popup for crop the uploaded photo -->
    <div id="popup_crop" style="display:none;">
        <div class="form_crop">
            <span class="close" onclick="close_popup('popup_crop')">x</span>
            <h2>Crop photo</h2>
            <!-- This is the image we're attaching the crop to -->
            <img id="cropbox" />            
            <!-- This is the form that our event handler fills -->
            <form>
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <input type="hidden" id="photo_url" name="photo_url" />
                <input type="button" value="Crop Image" id="crop_btn" onclick="crop_photo(800,500)" />
            </form>
        </div>
    </div>	
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>