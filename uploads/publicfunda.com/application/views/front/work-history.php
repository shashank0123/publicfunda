<?php $this->load->view('front/layout/header-inner'); ?>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Work History</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Work History</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12">										
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions"></div>
									<h2 class="panel-title">Work History</h2>								
								</header>
								<div class="panel-body" >
									<form class="form-horizontal" method="post">
										<h4 class="mb-xlg"></h4>
											<fieldset>												
												<div class="form-group">													
													<div class="col-md-6">
														<div class="input-daterange input-group" data-plugin-datepicker="">
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="startDate" value="<?php echo(isset($_POST['startDate']))?$_POST['startDate']:''; ?>" required>
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="endDate" value="<?php echo(isset($_POST['endDate']))?$_POST['endDate']:''; ?>" required>                                                        
														</div>
													</div>
                                                    <div class="col-md-3">
														<select class="form-control mb-md" name="status">
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='all')?'selected':'' ?> value="all">All</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='1')?'selected':'' ?> value="1">Pending</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='0')?'selected':'' ?> value="0">Clicked</option>
														</select>                                                        
													</div>                                                    
                                                    <div class="col-md-3">
														<button type="submit" name="checkWorkHistory" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
														<button type="submit" name="reset" class="btn btn-warning"><i class="fa  fa-repeat"></i> Reset </button>
													</div>
												</div>                                                  
											</fieldset>
										</form>
                                        
                                        <div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed mb-none">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Page Title</th>
												<th>Date</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
										<?php 
										if(count($workHistoryData)>0){ $i=0;
										foreach($workHistoryData as $workData){	 $i++;
										?> 
										<?php $campaign = $this->campaign_model->selectCampaignByID($workData->link_id); ?>	
												<tr>
													<td><?php echo $i; ?></td>
													<td><a href="<?php echo $campaign[0]->campaignUrl; ?>" target="_blank"><?php echo $campaign[0]->campaignTitle; ?></a></td>
													<td><?php echo $workData->distributionDate; ?></td>
													<td>
														<?php if($workData->status==1){ ?>
															<button class="btn btn-danger">Pending</button>
														<?php }else{ ?>
															<button class="btn btn-success">Clicked</button>
														<?php } ?>
													</td> 
												</tr>
										<?php 
										} }
										?>		
										</tbody>    
									</table>
                                    
								</div>
								</div>
							</section>						
						</div>						

					</div>
					<!--footer start-->
						<?php $this->load->view('front/layout/footer'); ?>	
					<!--end start-->	
					<!-- end: page -->
				</section>
			</div>
			
		</section>									
		
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	</body>


</html>