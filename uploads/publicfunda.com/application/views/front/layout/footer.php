<?php
$block_1 = $this->staticblock_model->getBlockById(7); 
$block_2 = $this->staticblock_model->getBlockById(8); 
$block_3 = $this->staticblock_model->getBlockById(9); 
?>


<div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="colophon wigetized">
						<div class="col-sm-4">
							<div class="widget widget_text">
								<h4 class="widget-title">Usefull Links</h4>
								<div class="textwidget">
								<ul>
									<li><a href="http://samridhbharat.biz/terms-condition">Terms &amp; Conditions </a></li>
									<li><a href="http://samridhbharat.biz/privacy-policy">Privacy Policy</a></li>
									<li><a href="http://samridhbharat.biz/faq">FAQ</a></li>
									<li><a href="http://samridhbharat.biz/task-holidays">Task Holidays</a></li>
									<li><a href="http://samridhbharat.biz/legal-documents">Legal Documents</a></li>
									<li><a href="http://samridhbharat.biz/bank-details">Bank Details</a></li>
								</ul>
								
                                </div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="widget widget_text">
								<h4 class="widget-title">Contact Details</h4>
								<div class="textwidget">
									<p><?php echo $block_1[0]->content; ?></p>
								</div>
							</div>
						</div>
						<div class="col-sm-4" id="newsletterdiv">
							<div class="widget mailchimp-widget">
								<h4 class="widget-title">NEWSLETTER</h4>
								<form class="mc-subscribe-form" method="post" action="http://samridhbharat.biz/index.php/newsletter/savenewsletter">
									<label for="email">
										<?php echo $block_2[0]->content; ?>
																		</label> <br>

                                                                       <label for="email">
																			</label>
									 <div class="mc-contact-wrap form-group">
										<input type="contact" name="contact" class="form-control mc-email" value="" required="" placeholder="Contact No.">
									</div>
									
									<div class="mc-email-wrap">
										<input type="email" name="email" class="form-control mc-email" value="" required="" placeholder="Email address">
									</div> <br>
									<button type="submit" name="saveNewsLetter" class="btn btn-default">Submit</button>
								</form>
							</div>
						</div>
					 </div>
                     
                     
                     
                     <!--- <?php echo base64_encode('Developed by Naseem Ahmad || e-mail anaseem711@gmail.com'); ?> --->
                     	<footer class="colophon site-info">
							<div class="footer-more">
						<div class="container-boxed">
							<div class="row">
								<div class="col-md-12">
									<div class="noo-bottom-bar-content">
										<?php echo $block_3[0]->content; ?>										
									</div>
								</div>
							</div>
						</div>
					</div>
				 		</footer>
                     </div>
					</div>	

<?php if($popup[0]->id==4){ ?>
<?php $autoplay =1; ?>
<?php if(!isset($_COOKIE['TODAYTASKEXPIRE'])){ ?>
<script>
		function loadpopup()
		{
			document.getElementById("loadmypopup").click();
		}
</script>
<?php }else{$autoplay =0;}}else{?>
<?php $autoplay =1; ?>
<script>
		function loadpopup()
		{
			document.getElementById("loadmypopup").click();
		}
</script>
<?php } ?>					
<?php if($popup[0]->status==1){ ?>

					<a style="display:none;" class="mb-xs mt-xs mr-xs modal-sizes btn btn-default" id="loadmypopup" href="#modalLG">popup</a>					
<div id="modalLG" class="modal-block modal-block-lg mfp-hide">
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title"><?php echo $popup[0]->title; ?></h2>
											</header>
											<div class="panel-body">
												<div class="modal-wrapper">
													<div class="modal-text" id="modeldatacontent">
														<p>
														<?php if($popup[0]->type=='image'){ ?>
														<img src="<?php echo base_url('uploads/popup/'.$popup[0]->image); ?>">
														<?php } ?>
														<?php if($popup[0]->type=='content'){ ?>
														<?php echo $popup[0]->content; ?>	
														<?php } ?>
														<?php if($popup[0]->type=='youtube'){ ?>
														 <iframe width="100%"  class="youtubelink" src="https://www.youtube.com/embed/<?php echo $popup[0]->youtube; ?>?autoplay=<?php echo $autoplay;?>" frameborder="0" allowfullscreen ></iframe>
														<?php } ?>
														</p>
													</div>
												</div>
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="col-md-12 text-right">
														<button class="btn btn-default modal-dismiss" onclick="return pauseyoutubevideo();">Cancel</button>
													</div>
												</div>
											</footer>
										</section>
									</div>
<script>
function pauseyoutubevideo()
{
	document.getElementById('modeldatacontent').innerHTML='__';
}
</script>									
<?php } ?>	
								