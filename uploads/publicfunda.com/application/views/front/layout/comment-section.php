<section class="panel panel-default   post-comments">

<div class="media p15 mt-0">
<?php $commentData = $this->comment_model->selectAllActiveComments(); ?>
<?php if(count($commentData)>0){ ?>
<?php foreach($commentData as $comments){ ?>
<?php $comment_user = $this->user_model->selectUserByID($comments->uid); ?>
	<div class="media">
		<a class="pull-left" href="javascript:;">
			<img class="media-object avatar avatar-sm" src="<?php echo base_url('uploads/user/'.$comment_user[0]->image); ?>" alt="">
		</a>
		<div class="comment">
			<div class="comment-author h6 no-m">
				<a href="javascript:void(0);"><b><?php echo $comment_user[0]->name; ?></b></a>
			</div>
			<div class="comment-meta small"><?php echo date('d M Y, h:m a',$comments->comment_time); ?></div>
			<p>
				<?php echo $comments->comments; ?>
			</p>			
			<hr>
		</div>
	</div>
<?php } } ?>
</div>

</section>