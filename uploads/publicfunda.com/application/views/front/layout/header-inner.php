<?php 
$user_login_id = $this->session->userdata('USERID'); 
if(empty($user_login_id)){	redirect('index.php/user/login'); }
//$login_user = $this->user_model->selectUserByID($user_login_id);
?>
<!doctype html>
<html class="fixed js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling no-mobile-device custom-scroll sidebar-left-xs js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling sidebar-left-collapsed">
<head>
		<!-- Basic -->
		<title>Samridh Bharat</title>
		<meta name="keywords" content="Samridh Bharat" />
		<meta name="description" content="Samridh Bharat">
		<meta name="author" content="Naseem Ahmad">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<!-- Vendor CSS -->
		<link rel="stylesheet"href="<?php echo base_url(); ?>assets/front/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet"href="<?php echo base_url(); ?>assets/front/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet"href="<?php echo base_url(); ?>assets/front/vendor/font-awesome/css/font-awesome-animation.min.css" /><!--animation css-->
		<link rel="stylesheet"href="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet"href="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/css/datepicker3.css" />
		<!-- Theme CSS -->
		<link rel="stylesheet"href="<?php echo base_url(); ?>assets/front/stylesheets/theme.css" />
		<!-- Theme Custom CSS -->
		<link rel="stylesheet"href="<?php echo base_url(); ?>assets/front/stylesheets/theme-custom.css">
		<!-- Head Libs -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/modernizr/modernizr.js"></script>
               
	</head>
	<body onload="loadpopup();">
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="<?php echo base_url('index.php/user/dashboard'); ?>" class="logo123">
<img src="<?php echo base_url(); ?>assets/front/images/logo.png" alt="" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
			
					<form class="search nav-form" style="width: 220px;">
						<div class="input-group input-search">
							<a style="margin-right:6px;" href="http://samridhbharat.biz/" class="btn btn-warning">Home</a>
							<a href="<?php echo base_url('index.php/user/dashboard'); ?>" class="btn btn-warning">Dashboard</a>
						</div>						
					</form>
			
					<span class="separator"></span>
			
					<?php 
$block_alert_1 = $this->staticblock_model->getBlockById(3); 
$block_alert_2 = $this->staticblock_model->getBlockById(4); 
$block_alert_3 = $this->staticblock_model->getBlockById(5); 
?>		
					<ul class="notifications">
						<li>
							<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
								<i class="fa fa-tasks"></i>
								<span class="badge">1</span>
							</a>
			
							<div class="dropdown-menu notification-menu large">
								<div class="notification-title">
									Tasks
								</div>			
								<div class="content">
									<ul>
										<li>
											<?php echo $block_alert_1[0]->content; ?>
										</li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
								<i class="fa fa-envelope"></i>
								<span class="badge">1</span>
							</a>
			
							<div class="dropdown-menu notification-menu">
								<div class="notification-title">
									Messages
								</div>
			
								<div class="content">
									<ul>
										<li>
											<?php echo $block_alert_2[0]->content; ?>
										</li>										
									</ul>
								</div>
							</div>
						</li>
						<li>
							<a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
								<i class="fa fa-bell"></i>
								<span class="badge">1</span>
							</a>
			
							<div class="dropdown-menu notification-menu">
								<div class="notification-title">
									Alerts
								</div>
			
								<div class="content">
									<ul>
										<li>
											<?php echo $block_alert_3[0]->content; ?>
										</li>										
									</ul>			
								</div>
							</div>
						</li>
					</ul>
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="<?php echo base_url('uploads/user/'.$login_user[0]->image); ?>" alt="<?php echo $login_user[0]->name; ?>" class="img-circle" />
							</figure>
							<div class="profile-info" data-lock-name="<?php echo $login_user[0]->name; ?>" data-lock-email="<?php echo $login_user[0]->email; ?>">
								<span class="name"><?php echo $login_user[0]->name; ?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li><a role="menuitem" tabindex="-1" href="<?php echo base_url('index.php/user/profile'); ?>"><i class="fa fa-user"></i> My Profile</a></li>
								<?php if($login_user[0]->wallet_display==1){ ?>
								<li><a role="menuitem" tabindex="-1" href="<?php echo base_url('index.php/user/wallet'); ?>"><i class="fa fa-money"></i> Wallet</a></li>
								<?php } ?>
								<li><a role="menuitem" tabindex="-1" href="<?php echo base_url('index.php/user/logout'); ?>"><i class="fa fa-power-off"></i> Logout</a></li>
							</ul>
						</div>
					</div>
				</div>
                                <!---PHP  <?php echo base64_encode('Developed by Naseem Ahmad || e-mail anaseem711@gmail.com'); ?> --->
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->