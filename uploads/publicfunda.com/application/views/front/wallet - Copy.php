<?php $this->load->view('front/layout/header-inner'); ?>

<style>
.inactiveClick{color: #c5c5c5 !important;}
</style>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Wallet</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Wallet</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">
						<div class="col-md-12">
							<section class="panel">
									<header class="panel-heading">
										<div class="panel-actions">											
                                            <p>
												<strong>Total CTP Points: <?php echo count($this->campaign_model->getTotalPoints($login_user[0]->id)); ?></strong><br>										
												<strong>Today's Earned CTP Points : <?php echo count($TOTALCLICKEDLINK); ?></strong><br>
												<button class="pull-right btn btn-info myclass"><strong>Total Wallet Amount : <i class="fa fa-rupee"></i> <?php echo $login_user[0]->wallet; ?></strong></button>							
											</p>
										</div>
										<h2 class="panel-title"><?php if(count($this->campaign_model->getTotalPoints($login_user[0]->id))>0){ ?>
												<button type="button" name="redeemPoints_www" data-toggle="modal" data-target="#mymodel" class="btn btn-warning"> Redeem CTP Points</button>
												<?php }else{ ?>
												<button type="button" name="redeemPoints_www" class="btn btn-warning" data-toggle="modal" data-target="#noepoint"> Redeem CTP Points</button>
												<?php } ?>
												<button class="btn btn-warning" id="viewRedeemPoints" data-toggle="modal" data-target="#modalBootstrap">View Redeem Points History</button>
												</h2><br><br>					
									</header>									
																	
							</section>
						</div>
						<div class="col-md-4">
							<section class="panel">	
									<?php echo $this->session->flashdata('message') ?>
									<header class="panel-heading">																
										<h2 class="panel-title">Send money to bank</h2>                                       
									</header>
									<div class="panel-body">
																				
										<div class="table-responsive">
											<form method="post">
												<table class="table mb-none">													
													<tr>
														<td><strong>Amount</strong></td>
														<td><input type="text" name="invamount" onkeypress="return isNumberKey(event,'genrateInvoice')" class="form-control" placeholder="Amount" required></td>
													</tr>
													<tr>
														<td colspan="2">
															<button style="display:none;" type="submit" id="genrateInvoicebtn"  name="genrateInvoice" class="btn btn-info">Submit</button>
															<button type="submit" id="genrateInvoice" onclick="return disableinvbutton()" name="genrateInvoice123" disabled="disabled" class="btn btn-info">Submit</button>
														</td>
													</tr>
													<?php /*
													<thead>
														<tr>
															<th>Total Amount</th>
															<th>Amount</th>
															<th></th>
															<th width="40%"></th>
														</tr>
													</thead>
													<tbody>																									
														<tr>
															<td><i class="fa fa-rupee"></i> <?php echo $login_user[0]->wallet; ?></td>																												
															<td><input type="text" name="invamount" onkeypress="return isNumberKey(event,'genrateInvoice')" class="form-control" placeholder="Amount" required></td>
															<td><button style="display:none;" type="submit" id="genrateInvoicebtn"  name="genrateInvoice" class="btn btn-info">Submit</button>
															<button type="submit" id="genrateInvoice" onclick="return disableinvbutton()" name="genrateInvoice123" disabled="disabled" class="btn btn-info">Submit</button></td>
															<td></td>
														</tr>
													</tbody>
													*/ ?>
												</table>
											</form>
										</div>
									</div>									
							</section>
<script>
function disableinvbutton()
{
	document.getElementById('genrateInvoicebtn').click();
	document.getElementById('genrateInvoice').disabled=true;	
}

</script>							
							<section class="panel">
									<header class="panel-heading">																
										<h2 class="panel-title">Send money to friends</h2>                                       
									</header>									
									<div class="panel-body">
										<div class="table-responsive">
											<form method="post" id="sendmonyform">
											<span id="errormsg"></span>											
											<table class="table mb-none">
												<tr>
													<td>USER ID</td>
													<td><input type="text" name="friendId" id="userid" class="form-control" placeholder="USER ID" required></td>
												</tr>
												<tr>
													<td>Amount</td>
													<td><input type="text" name="amount" id="sendamount" class="form-control" onkeypress="return isNumberKey(event,'sendmoneybutton')" placeholder="Amount" required></td>
												</tr>
												<tr>
													<td colspan="2"><a type="button" id="sendmoneybutton" onclick="return checkusertosendotp();" disabled="disabled" name="sendMoneyToFriend" class="btn btn-info">Generate OTP</a></td>
												</tr>
												<?php /*
												<thead>
													<tr>
														<th>Total Amount</th>
														<th>USER ID</th>
														<th>Amount</th>
														<th></th>
														<th width="40%"></th>
													</tr>
												</thead>
												<tbody>																						
													<tr>
														<td><i class="fa fa-rupee"></i> <?php echo $login_user[0]->wallet; ?> </td>
														<td><input type="text" name="friendId" id="userid" class="form-control" placeholder="USER ID" required></td>
														<td><input type="text" name="amount" id="sendamount" class="form-control" onkeypress="return isNumberKey(event,'sendmoneybutton')" placeholder="Amount" required></td>
														<td><a type="button" id="sendmoneybutton" onclick="return checkusertosendotp();" disabled="disabled" name="sendMoneyToFriend" class="btn btn-info">Generate OTP</a></td>
														<td></td>
													</tr>
												</tbody>
												*/ ?>
											</table>
												
												<div class="modal fade" id="otpModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<div class="modal-dialog">
														<div class="modal-content" >
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" id="closeotp"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
																<h4 class="modal-title" id="myModalLabel">Enter OTP</h4>
															</div>
															<div class="modal-body" >
																 <div class="table-responsive">
																	<span id="lastmsg"></span>
																	<table class="table mb-none">															
																		<tr>
																			<td>
																				<span id="optmsg"></span>	
																				<input type="text" name="otpnumber" id="otpnumber" placeholder="Enter OTP" class="form-control"><br>
																				<button type="button" id="sendmoneybutton" onclick="return sendMoneyToUser();" name="sendMoneyToFriendbyOtp" class="btn btn-info pull-right">Submit</button>
																			</td>
																		</tr>																	
																	</table>
																</div>
															</div>												
														</div>
													</div>
												</div>
									
											</form>
										</div>
									</div>									
							</section>							
						</div>
						<div class="col-md-8">
							<?php $this->load->view('front/layout/advertisement-slider'); ?>
						</div>
						
					</div>
					<!--footer start-->
						<?php $this->load->view('front/layout/footer'); ?>	
					<!--end start-->
					<!-- end: page -->
				</section>
			</div>			
		</section>
		<div class="modal fade" id="mymodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" id="closeotp"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
																<h4 class="modal-title" id="myModalLabel"> Redeem CTP Points</h4>
															</div>
															<div class="modal-body">
																 <div class="table-responsive">
																 <?php $totalEpoints = count($this->campaign_model->getTotalPoints($login_user[0]->id)); ?>
																 <button type="button" name="redeemPoints_www" class="btn btn-warning pull-right"></strong>Total Wallet Amount - <i class="fa fa-rupee"></i><?php echo $login_user[0]->wallet; ?></strong></button>
																	<table class="table mb-none">
																		<thead>	
																			<tr>
																				<th>Total E-points</th>
																				<th>Amount</th>
																				<th>TDS(10%)</th>
																				<th>Admin Charge(5%)</th>
																				<th>Net Amount</th>
																			</tr>
																		</thead>
																		<tbody>	
																		<form action="<?php echo base_url('index.php/user/wallet'); ?>" method="post">
																			<tr>
																				<td><?php echo $totalEpoints; ?></td>
																				<td><i class="fa fa-rupee"></i><?php echo $totalEpoints*5; ?></td>
																				<td><i class="fa fa-rupee"></i><?php echo ($totalEpoints*5)*10/100; ?></td>
																				<td><i class="fa fa-rupee"></i><?php echo ($totalEpoints*5)*5/100; ?></td>
																				<td><i class="fa fa-rupee"></i><?php echo $totalEpoints*5-(($totalEpoints*5)*15/100); ?></td>
																			</tr>
																			<tr>
																			<td colspan="5">
																				<button type="submit" name="redeemPoints" class="btn btn-info pull-right"> Redeem Now</button>
																			</td>
																		</tr>
																		</form>	
																		</tbody>																			
																	</table>
																</div>
															</div>												
														</div>
													</div>
												</div>	
<div class="modal fade" id="modalBootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
													<h4 class="modal-title" id="myModalLabel">Redeem Points History</h4>
												</div>
												<div class="modal-body">
													 <div class="table-responsive">
														<table class="table mb-none">
															<thead>
																<tr>
																	<th>S No.</th>
																	<th>Points</th>
																	<th>Amount</th>
																	<th>Tax (15%)</th>
																	<th>Final Amount</th>
																	<th>Date</th>
																</tr>
															</thead>
															<tbody>	
															<?php $p =0; if(count($REDEEMHISTORY)>0){ ?>
															<?php foreach($REDEEMHISTORY as $rh){ $p++;?>	
																<tr>
																	<td><?php echo $p; ?></td>
																	<td><?php echo $rh->points; ?> X 5</td>
																	<td><?php echo $rh->points*5; ?></td>
																	<td><?php echo ($rh->points*5)*15/100; //$rh->tax; ?></td>	
																	<td><?php echo $rh->amount-($rh->points*5)*15/100; ?></td>
																	<td><?php echo $rh->redeemdate; ?></td>
																</tr>
															<?php }} ?>	
															</tbody>
														</table>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									<?php if(isset($_GET['mydata'])){ ?>
									<div class="modal fade" id="modalBootstrapsendmodeytobank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title" id="myModalLabel">Send money to bank</h4>
												</div>
												<div class="modal-body">
													 <div class="table-responsive">
														<?php echo urldecode(base64_decode($_GET['mydata']));//$this->session->flashdata('invmessage') ?>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									<?php } ?>	
		<div class="modal fade" id="noepoint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">	
					<div class="modal-header">
						<button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					</div>
					<div class="modal-body">
						<div class="table-responsive">
						<center><strong>There have no CTP Points to redeem.<!--You have no CTP Points to redeem.---></strong></center>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<button id="otppopupmodalBootstrapsendmodeytobank" data-toggle="modal" data-target="#modalBootstrapsendmodeytobank" style="display:none;">openpopup</button>							
		<button id="otppopup" data-toggle="modal" data-target="#otpModel" style="display:none;">openpopup</button>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		
<?php if(isset($_GET['mydata'])){ ?>
<script>
document.getElementById('otppopupmodalBootstrapsendmodeytobank').click();
</script>
<?php } ?>

<script>
function isNumberKey(evt,BTNID)
{	
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false;
	}
	else
	{
		 $("#"+BTNID).removeAttr("disabled");
		return true;
	}    
}


function sendMoneyToUser()
{
	var uid = $('#userid').val();
	var send_amount = $('#sendamount').val();
	var OTP = $('#otpnumber').val();
	
	if(OTP!="")
	{	
		var sendmoneyform = $("#sendmonyform").serialize();
		jQuery.ajax({
            url: "<?php echo base_url('index.php/user/sendMoneyTofriend'); ?>",
            data:sendmoneyform,
            type: "POST",				
            success:function(mydata)
			{
				//alert(sendmoneyform);
				if(mydata!=0)
				{
					$('#closeotp').click();
					$('#errormsg').html('<div class="alert alert-success">Amount successfully transferred.</div>');
					$('#lastmsg').html('');
					$('#optmsg').html('');
					$('#userid').val('');
					$('#sendamount').val('');
					$('#otpnumber').val('');
					//setTimeout(function () { location.reload(1); }, 7000);
					return false;
				}
				else
				{
					$('#optmsg').html('<div class="alert alert-danger"><strong>error ! </strong> Invalid OTP </div>');
					return false;
				}	
            },
            error:function ()
			{
				$('#optmsg').html('<div class="alert alert-danger"><strong>error ! </strong> Invalid OTP </div>');
				return false;
			}
        });	
	}
	else
	{
		$('#optmsg').html('<div class="alert alert-danger"><strong>error ! </strong>Invalid OTP </div>');
	}	return false;
}


function checkusertosendotp()
{
	var uid = $('#userid').val();
	var send_amount = $('#sendamount').val();
	
	if(uid!="" && send_amount!="")
	{	
		if(send_amount>=10001)
		{
			$('#errormsg').html('<div class="alert alert-danger"><strong>Invalid Amount! </strong>you can send only Rs.10000 amount to your friend</div>');
			return false;
		}
		
		var sendmoneyform = $("#sendmonyform").serialize();
		jQuery.ajax({
            url: "<?php echo base_url('index.php/user/checkUserAndSendOtp'); ?>",
            data:sendmoneyform,
            type: "POST",  
			beforeSend : function()
			{
				$('#sendmoneybutton').append('<i class="fa fa-spinner faa-spin animated"></i>');
			},	
            success:function(mydata)
			{
				if(mydata!=0)
				{
					$('#lastmsg').html('<div class="alert alert-success">An amount  of  <i class="fa fa-rupee"></i><strong>'+ send_amount +'</strong> is being transferred to <strong>'+ mydata +'</strong> USER ID <strong>'+ uid +'</strong> please confirm to proceed with otp.</div>');
					document.getElementById('otppopup').click();
					//$('#optmsg').html('<div class="alert alert-success">Please check your email and know your OTP</div>');
					$('#errormsg').html('');
					$('#sendmoneybutton').html('Generate OTP');
					return false;
				}
				else
				{
					$('#errormsg').html('<div class="alert alert-danger"><strong>error ! </strong> Invalid USER ID or Amount</div>');
					$('#sendmoneybutton').html('Generate OTP');
					return false;
				}	
            },
            error:function ()
			{
				$('#errormsg').html('<div class="alert alert-danger"><strong>error ! </strong>Invalid USER ID or Amount</div>');
				$('#sendmoneybutton').html('Generate OTP');
				return false;
			}
        });	
	}
	else
	{
		$('#errormsg').html('<div class="alert alert-danger"><strong>error ! </strong>Invalid USER ID or Amount</div>');
		$('#sendmoneybutton').html('Generate OTP');
	}	return false;
}

</script>	
	

	</body>


</html>