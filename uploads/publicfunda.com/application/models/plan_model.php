<?php 
class Plan_model extends CI_Model
{
	var $paln_table = 'tbl_plans';
	public function insertData($data)
	{
		$this->db->insert($this->paln_table,$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
		//return  $returnData;
	}
	
	public function updateData($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update($this->paln_table,$data);
		return $returnData;
	}
	
	public function getAllPlan()
	{
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}
	
	public function getAllActivePlan()
	{
		$this->db->where('status',1);
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}
	
	public function selectPlanByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}

}