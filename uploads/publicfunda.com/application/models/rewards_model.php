<?php 
class Rewards_model extends CI_Model
{
	var $paln_table = 'tbl_rewards';
	public function insertData($data)
	{
		$this->db->insert($this->paln_table,$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
		//return  $returnData;
	}
	
	public function updateData($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update($this->paln_table,$data);
		return $returnData;
	}
	
	public function getAllRewardsByType($type)
	{
		$this->db->where('type',$type);
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}
	
	public function getAllActiveRewards()
	{
		$this->db->where('type','common');
		$this->db->where('status',1);
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}
	
	public function getAllRewardsByUser($uid)
	{
		$this->db->where('user_id',$uid);
		$this->db->where('type','special');
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}
	public function getAllRewards()
	{
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}
	
	public function selectRewardsByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->paln_table);
		return $data->result();
	}
	
	public function deleteRewards($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_rewards');
	}

}