<?php 
class Comment_model extends CI_Model
{
	var $tbl_comments = 'tbl_comments';
	
	function selectAllComments()
	{
		$this->db->order_by('id','DESC');
		$data= $this->db->get("tbl_comments");
		return $data->result();		
	}
	function selectAllActiveComments()
	{
		$this->db->order_by('id','DESC');
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_comments");
		return $data->result();		
	}
	
	function selectAllCommentsByStatus($status)
	{
		$this->db->where('status', $status);
		$data= $this->db->get("tbl_comments");
		return $data->result();		
	}
	
	function insertData($data)
	{
		$result= $this->db->insert('tbl_comments', $data);
		return $result;
	}
	
	function updateData($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_comments',$data);
	}
	
	function selectLikeDataByUserId($uid)
	{
		$this->db->order_by('id','DESC');
		$this->db->where('uid', $uid);
		$data= $this->db->get("tbl_like");
		return $data->result();
	}
	function selectAllLikeUnlikeData($args)
	{
		$this->db->order_by('id','DESC');
		$this->db->where('like', $args);
		$data= $this->db->get("tbl_like");
		return $data->result();
	}
	function selectAllLikeData()
	{
		$data= $this->db->get("tbl_like");
		return $data->result();
	}
	function insertLikeData($data)
	{
		$result= $this->db->insert('tbl_like', $data);
		return $result;
	}
	function updateLikeData($id,$data)
	{
		$this->db->where('uid', $id);
		return $this->db->update('tbl_like',$data);
	}
	
    function selectAllLinkByStatus($status)
	{
		$this->db->where('like', $status);
		$data= $this->db->get("tbl_like");
		return $data->result();		
	}
	
	function deleteComments($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_comments');
	}
	function deleteAllLikes()
	{
		$this->db->empty_table('tbl_like');
		///return $this->db->delete('tbl_like');
	}
}
