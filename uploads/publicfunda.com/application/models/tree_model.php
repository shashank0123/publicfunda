<?php 
class Tree_model extends CI_Model
{
	var $user_table = 'tbl_users';
	public function getTreeData($id)
	{
		$this->db->where('sponsor_id',$id);
		$this->db->order_by('postion', 'ASC');
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
?>
		<ul>
		<?php foreach($result as $res){ ?>
		<li>
															<a href="#" class="thumbnail1" onclick="return openChildTree('<?php echo $res->id;  ?>');">
																<center>
																	<img  src="<?php echo base_url(''); ?>assets/front/images/<?php echo($res->status==1)?'activate.png':'deactivate.png';  ?>" style="height:30px;width:30px;">
																</center><?php echo $res->name;  ?>
															</a>
															<div id="title">
																<span id="ctl00_ContentPlaceHolder1_lbl0_Root">
																	<table style="text-align:Left;">
																		<tbody>
																			<tr>
																				<td width="100px">Name</td>
																				<td width="5px">:</td>
																				<td style="padding-left:5px;" width="160px"><?php echo $res->name; ?></td>
																			</tr>                                                                            																				
																			<tr>
																				<td>User ID</td>
																				<td>:</td>
																				<td style="padding-left:5px;"> <?php echo $res->id; ?> </td>
																			</tr>																			
																			<tr>
																				<td>Sponsor ID</td>
																				<td>:</td>
																				<td style="padding-left:5px;"> <?php 
																				if($res->direct_id=='NULL')
																				{
																					echo $res->sponsor_id;
																				}
																				else
																				{
																					echo $res->direct_id;
																				}	
																				?>
																				 </td>
																			</tr>
																			<tr>
																				<td width="100px">Position</td>
																				<td width="5px">:</td>
																				<td style="padding-left:5px;" width="160px"><strong>(<?php echo($res->postion=='R')?'Right':'Left'; ?>)</strong></td>
																			</tr>			
																			<tr>
																				<td>Date Of Registration</td>
																				<td>:</td>
																				<td style="padding-left:5px;"> <?php echo date('d-M-Y',$res->registration_date); ?> </td>
																			</tr>
																		</tbody>
																	</table>
																</span>
															</div>		
														<?php $this->getTreeData($res->id);  ?>	
														</li>		
		<?php } ?>
		</ul>
<?php			
		}
	}
	
	function myTeamView($id,$position,$status,$first_date,$second_date)
	{
		//error_reporting(0);
		$this->db->where('sponsor_id',$id);
		if($position!="all")
		{			
			$this->db->where('postion',$position);
		}		
		$this->db->where('rdate >=', $first_date);
		$this->db->where('rdate <=', $second_date);
		if($status!='all')
		{
			$this->db->where('status',$status);
		}			
		
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			$k=0;
?>
		<?php foreach($result as $res){ $k++; ?>
		<?php $planData = $this->selectPlanByID($res->plan); ?> 
		<tr>
			<td><?php //echo $k; ?></td>
            <td><?php echo $res->id; ?></td>
            <td><?php echo $res->name; ?></td>
            <td><?php echo $planData[0]->planName; ?></td>
			<td><?php echo($res->status==1)?'Active':'Inactive'; ?></td>
            <td><?php echo date('d-M-Y',$res->registration_date); ?></td>
            <td><?php echo($res->boosterStatus==1)?'Active':'Inactive'; ?></td>
            <td>
				<?php 
					if($position!='all')
					{
						echo ($position=='L')?'Left':'Right'; 
					}
					else
					{
						echo($res->postion=='L')?'Left':'Right'; 
					}
				?>
			</td>
        </tr>
		<?php $this->myTeamView($res->id,'all',$status,$first_date,$second_date);  ?>			
		<?php } ?>
<?php			
		}
	}
	
	function myBussiness($id,$position,$status,$first_date,$second_date)
	{
		$this->db->where('sponsor_id',$id);
		if($position!="all")
		{		
			$this->db->where('postion',$position);
		}	
		$this->db->where('rdate >=', $first_date);
		$this->db->where('rdate <=', $second_date);		
		
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{  $k=0;
?>
		<?php foreach($result as $res){ $k++; ?>
		<?php $userInvData = $this->selectUserInvoiceByFilter($res->id); ?>
		<?php $planData = $this->selectPlanByID($res->plan); ?> 		
		<?php foreach($userInvData as $userInv){ ?>
		<?php if(isset($userInv->status) && $status==$userInv->status){ ?>
		<tr>
			<td><?php //echo $k; ?></td>
            <td><?php echo $res->id; ?></td>
            <td><?php echo $res->name; ?></td>
            <td><?php echo(isset($userInv->planName))?$userInv->planName:'-';  ?></td>
			<td><?php echo(isset($userInv->invoiceID))?$userInv->invoiceID:'-'; ?></td>
			<td><?php echo(isset($userInv->transactionID))?$userInv->transactionID:'-'; ?></td>
			<td><?php echo(isset($userInv->amount))?$userInv->amount:'-'; ?></td>
            <td><?php echo($res->status==1)?'Active':'Inactive'; ?></td> 
            <td>
				<?php 
					if($position!='all')
					{
						echo ($position=='L')?'Left':'Right'; 
					}
					else
					{
						echo($res->postion=='L')?'Left':'Right'; 
					}
				?>
			</td>
            <td><?php echo $res->rdate; ?></td>
        </tr>
		<?php } }
		if($status=='all'){
			 foreach($userInvData as $userInv){ 
		?> 
		<tr>
			<td><?php //echo $k; ?></td>
            <td><?php echo $res->id; ?></td>
            <td><?php echo $res->name; ?></td>
            <td><?php echo(isset($userInv->planName))?$userInv->planName:'-';  ?></td>
			<td><?php echo(isset($userInv->invoiceID))?$userInv->invoiceID:'-'; ?></td>
			<td><?php echo(isset($userInv->transactionID))?$userInv->transactionID:'-'; ?></td>
			<td><?php echo(isset($userInv->amount))?$userInv->amount:'-'; ?></td>
            <td><?php echo($res->status==1)?'Active':'Inactive'; ?></td>
            <td><?php 
					if($position!='all')
					{
						echo ($position=='L')?'Left':'Right'; 
					}
					else
					{
						echo($res->postion=='L')?'Left':'Right'; 
					}
				?>
			</td>
            <td><?php echo $res->rdate; ?></td>
        </tr>
		<?php } } ?>
		<?php $this->myBussiness($res->id,'all',$status,$first_date,$second_date);  ?>			
		<?php } ?>
<?php			
		}
	}
	
	public function countTotalMembers($id,$pos)
	{
		$category_data = array();
		$this->db->where('sponsor_id',$id);
		if($pos!="0")
		{
			$this->db->where('postion',$pos);
		}
		$this->db->where('status',1);
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				$category_data[] = array('id' => $res->id);
				$children = $this->countTotalMembers($res->id,0);
				if($children)
				{
					$category_data = array_merge($children, $category_data);
				}
			}
			return $category_data;
		}	   
	}
	
	
	public function selectUserInvoiceByFilter($id)
	{
		$this->db->where('user_id',$id);
		$this->db->where('status','paid');
		$data = $this->db->get('tbl_user_invoice');
		return $data->result();
	}
	
	public function selectUserInvoice($id)
	{
		$this->db->where('user_id',$id);
		$data = $this->db->get('tbl_user_invoice');
		return $data->result();
	}
	
	public function selectAllUserInvoiceOrderBy()
	{
		$this->db->order_by('id','DESC');
		$data = $this->db->get('tbl_user_invoice');
		return $data->result();
	}
	
	public function selectUserInvoiceById($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_user_invoice');
		return $data->result();
	}
	public function selectAllUserInvoice($id,$tr_id)
	{
		$this->db->where('id !=',$id);
		$this->db->where('transactionID',$tr_id);
		$data = $this->db->get('tbl_user_invoice');
		return $data->result();
	}
     public function selectAllUserInvoicebytrid($tr_id)
	{
		//this->db->where('id !=',$id);
		$this->db->where('transactionID',$tr_id);
		$data = $this->db->get('tbl_user_invoice');
		return $data->result();
	}
	function updateUserInvoice($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_user_invoice',$data);
	}
	public function deleteUserInvoice($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_user_invoice');
	}
	public function selectPlanByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('tbl_plans');
		return $data->result();
	}

	public function tree_mis($id)
	{
		$this->db->where('sponsor_id',$id);
		$this->db->order_by('postion', 'ASC');
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{

			echo '<ul>';
			foreach($result as $res)
			{ 
				echo '<li>'.$res->name.'('.$res->postion.')'; 
				$this->tree_mis($res->id); 
				echo '</li>';		
			} 
			echo '</ul>';			
		}
	}
}
