<?php 
class Booster_model extends CI_Model
{
	var $user_table = 'tbl_users';	
	public function getTreeIncome($id,$pos)
	{
		$category_data = array();
		$this->db->where('sponsor_id',$id);
		if($pos!="0")
		{
			$this->db->where('postion',$pos);
		}
		$this->db->where('status',1);
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				$category_data[] = array('uid' => $res->id);
				$children = $this->getTreeIncome($res->id,0);
				if($children)
				{
					$category_data = array_merge($children, $category_data);
				}
			}
			return $category_data;
		}
	}	
	function getBonusIncome($id,$pos)
	{
		$user_Ids = $this->getTreeIncome($id,$pos);
		$bonus = 0;
		if(count($user_Ids)>0)
		{
			foreach($user_Ids as $user)
			{
				$invoiceData = $this->getUserInvoiceByUserID($user['uid']);//$plan['planid'];
				if(count($invoiceData)>0)
				{
					foreach($invoiceData as $inv)
					{
						if($inv->status=='paid')
						{
							$bonus += $inv->amount;
						}						
					}					
				}	
			}
			return $bonus;
		}
		else
		{
			return $bonus;
		}	
	}
	public function getUserInvoiceByUserID($id)
	{
		$this->db->where('user_id',$id);
		$this->db->where('status !=','pending');
		$invdata = $this->db->get('tbl_user_invoice')->result();
		return $invdata;
	}
	public function getPlanAmountByID($id)
	{
		$this->db->where('id',$id);
		$plan = $this->db->get('tbl_plans')->result();
		return $plan[0]->planFees;
	}
	
	
	
}