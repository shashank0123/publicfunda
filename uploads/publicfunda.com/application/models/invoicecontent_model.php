<?php 
class Invoicecontent_model extends CI_Model
{
	var $tbl_staticblock = 'tbl_invoice_content';
	public function insertData($data)
	{
		$this->db->insert($this->tbl_staticblock,$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	public function updateBlockData($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update($this->tbl_staticblock,$data);
		return $returnData;
	}
	
	public function getAllBlock()
	{
		$data = $this->db->get($this->tbl_staticblock);
		return $data->result();
	}
	
	public function getBlockById($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->tbl_staticblock);
		return $data->result();
	}
}