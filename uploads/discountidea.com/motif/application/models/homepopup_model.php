<?php
class Homepopup_Model extends CI_Model
{
	function selectallpopup()
	{
		$data= $this->db->get("tbl_homepopup");
		return $data->result();		
	}
	
	function selecthomepopubyid($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_homepopup');
		return $data->result();
		
	}
	function update($id,$data)
	{
		
		$this->db->where('id',$id);
		$this->db->update('tbl_homepopup',$data);
	}
	
}
?>