<?php
class Filters_model extends CI_Model
{
	function selectAllFilters()
	{
		$this->db->where('p_id',0);
		$this->db->where('type','filter');
		$data= $this->db->get("tbl_filters");
		return $data->result();		
	}
	
	function selectAllFiltersOptions($id)
	{
		$this->db->where('p_id',$id);
		$this->db->where('type','option');
		$data= $this->db->get("tbl_filters");
		return $data->result();		
	}
	
	function selectAllActiveFilters()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_filters");
		return $data->result();		
	}
	
	function selectFiltersById($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_filters');
		return $data->result();
	}
	
	function insert_filters($data)
	{
		$result= $this->db->insert('tbl_filters', $data);
		return $result;
	}
	
	function update_filters($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_filters',$data);
	}
	
	function delete_filters($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_filters');
	}
	function selectFiltersWhereId_in($id)
	{
		$this->db->where_in('id', $id);
		$data= $this->db->get('tbl_filters');
		return $data->result();
	}
	
	function selectOptionWhereId_in($cid,$id)
	{
		$this->db->where('p_id', $cid);
		$this->db->where_in('id', $id);
		$data= $this->db->get('tbl_filters');
		return $data->result();
	}
	
	// for product section
	function insertProductFiltersOption($data)
	{
		$result= $this->db->insert('tbl_product_filters', $data);
		return $result;
	}
	function delete_product_options($id)
	{
		$this->db->where('p_id', $id);
		return $this->db->delete('tbl_product_filters');
	}
	public function selectOptionByProductID($id)
	{
		
		$this->db->where('p_id',$id);
		$query = $this->db->get('tbl_product_filters');		
		 $data=array();
		 foreach($query->result() as $val)
		 {
			array_push($data,$val->o_id);			 
		 }
		 return $data;
		 
	}
	
	function selectProductOptionByoptionId_in($id)
	{
		$this->db->where_in('o_id', $id);
		$data= $this->db->get('tbl_product_filters');
		return $data->result();
	}
	
	public function getAvailableOptions($cid,$id)
   {	  
	   $query = $this->db->query("select f.*,o.o_id,o.p_id as pid from tbl_filters f left join tbl_product_filters o on f.id=o.o_id where o.p_id=$id AND f.p_id=$cid");
	   return $query->result();
   }
   
    function getSelectedOption($id)
    {
		$option_data = $this->selectFiltersWhereId_in($id);
		$option_cat_data = $this->selectFiltersWhereId_in($option_data[0]->p_id);
		$html = "<small><a href='#'>".$option_cat_data[0]->title." : <strong>".$option_data[0]->title."</strong></a></small><br>";
		return $html;
    }
	
}
?>