<?php
class User_model extends CI_Model
{
    public $table = 'table_user';
	function selectAllUser()
	{
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function selectAllActiveUser()
	{
		$this->db->where('status',1);
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function insert_user($data)
	{
		$result= $this->db->insert($this->table, $data);
		return $result;
	}
	
	
	function selectuserby_email($email)
	{
		$this->db->where('email',$email);
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function selectUserByActivateToken($token)
	{
		$this->db->where('activate_token',$token);
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function user_login($email,$password)
	{
		//$this->db->where('profile_type',$profile_type);
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function selectuserby_id($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->table)->result();
		return $data;		
	}
	
	function selectUserByPassword($id,$pwd)
	{
		//echo "dsfsdfdd";die;
		$this->db->where('id',$id);
		$this->db->where('password',$pwd);
		$data = $this->db->get($this->table);
		//print_r($data);
		return $data;		
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table,$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}
	
	function getLoginUserVar($var)
	{
		$data = $this->session->userdata($var);
		return $data;
	}
	
	// experience query	
	function insert_user_exp($data)
	{
		$result= $this->db->insert('tbl_experience', $data);
		return $result;
	}
	function select_user_exp($id)
	{
		$this->db->where('user_id',$id);
		$data = $this->db->get('tbl_experience')->result();
		return $data;
	}
	function delete_user_exp($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_experience');
	}
	
	// education query
	function insert_user_edu($data)
	{
		$result= $this->db->insert('tbl_education', $data);
		return $result;
	}
	function select_user_edu($id)
	{
		$this->db->where('user_id',$id);
		$data = $this->db->get('tbl_education')->result();
		return $data;
	}
	function delete_user_edu($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_education');
	}
	
	// specialization query
	function insert_user_spl($data)
	{
		$result= $this->db->insert('tbl_specialization', $data);
		return $result;
	}
	function select_user_spl($id)
	{
		$this->db->where('user_id',$id);
		$data = $this->db->get('tbl_specialization')->result();
		return $data;
	}
	function delete_user_spl($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_specialization');
	}
	function checkUserTokenForgotPassword($token)
	{
		$this->db->where('password_token', $token);
		$data = $this->db->get('tbl_users');
		return $data->result();
	}
	
	function insertUserWishlistItem($data)
	{
		$res = $this->db->insert('tbl_wishlist',$data);
		return $res;
	}
	function checkUserWishlistItem($uid,$pid)
	{
		$this->db->where('user_id', $uid);
		$this->db->where('pro_id', $pid);
		$data = $this->db->get('tbl_wishlist');
		return $data->result();
	}
	function selectUserWishlistItem($uid)
	{
		$this->db->where('user_id', $uid);
		$data = $this->db->get('tbl_wishlist');
		return $data->result();
	}
	
	function productwislist($id)
	{
		$this->db->where('status',1);
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function productwilimage($id)
	{
		
		$this->db->where('product_id',$id);
		$data=$this->db->get('tbl_product_image');
		return $data->result();
	}
	function whislistdelete($id)
	{
		$this->db->where('pro_id',$id);
		$data=$this->db->delete('tbl_wishlist');
		return $data;
				
	}
	function myorderdetail1($userid)
	{
		$this->db->where('user_id',$userid);
		$data=$this->db->get('tbl_item');
		return $data->result();
	}
	function getOrderItemsByOrderId($order_id)
	{
		$this->db->where('order_id',$order_id);
		$data=$this->db->get('tbl_item');
		return $data->result();
	}
	function getOrderItemsByProductId($pro_id)
	{
		$this->db->where('pro_id',$pro_id);
		$data=$this->db->get('tbl_item');
		return $data->result();
	}
	function getSoldOutProducts($pro_id)
	{
		$this->db->where('pro_id',$pro_id);
		$data=$this->db->get('tbl_item');
		$sold_qty = 0;
		if(count($data->result())>0)
		{
			foreach($data->result() as $res_data)
			{
				$sold_qty += $res_data->qty;
			}
		}
		return $sold_qty;	
	}
	function getOrderByUserId($userid)
	{
		$this->db->where('user_id',$userid);
		$data=$this->db->get('tbl_order');
		return $data->result();
	}
	function getOrderByOrderId($order_id)
	{
		$this->db->where('id',$order_id);
		$data=$this->db->get('tbl_order');
		return $data->result();
	}
	function productmyorderdetail($ord)
	{
		$this->db->where('id',$ord);
		$data=$this->db->get('tbl_order');
		return $data->result();
	}
	function productmyorderproductimag($pro_id)
	{
		$this->db->where('product_id',$pro_id);
		$data=$this->db->get('tbl_product_image');
		return $data->result();
	}
	function productmyorderprice($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function selectorder($id)
	{
		$this->db->where('id',$id);
		$data= $this->db->get("tbl_order");
		return $data->result();		
	}
	function detailcomment($data)
	{
		$data=$this->db->insert('tbl_review',$data);
		return $data;
	}
	function selectreviewbyid($id)
	{
		$this->db->where('pro_id',$id);
		$this->db->where('status',1);
		$data=$this->db->get('tbl_review');
		return $data->result();
		
	}
	function searchallproduct($searchproduct)
	{
		$this->db->where('category_id',$searchproduct);
		$this->db->like('name',$searchproduct);
		$this->db->or_like('price',$searchproduct);
		$data=$this->db->get('tbl_product');
		//print_r($data);die;
		return $data->result();
		
	}
	function selectallreviewcount($id)
	{
		$this->db->where('pro_id',$id);
		$data=$this->db->get('tbl_review');
		//print_r($data);
		return $data->num_rows();
	}
}
?>