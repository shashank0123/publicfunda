<?php
class Size_model extends CI_Model
{

	public function insertsize($data)

	{
		$qry=$this->db->insert('tbl_size',$data);
		return $qry;
	
	}
	public function selectsize()
	{

		$data=$this->db->get('tbl_size');
		//print_r($data);
		return $data->result();
	}

	public function selectsizeby_id($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_size');
		return $data->result();
	}
	public function updateseize($id,$data)
	{

		$this->db->where('id',$id);
		$data=$this->db->update('tbl_size',$data);
		return $data;

	}
	public function deletesizeby_id($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tbl_size');

	}
	public function selectsizebysizeproid($id)
	{
		
		$this->db->where('pro_id',$id);
		$query = $this->db->get('tb_sizeproduct');
		//print_r($qry);
		 //$res=$query->num_rows();
		 $data=array();
		 foreach($query->result() as $val)
		 {
			 array_push($data,$val->size_id);			 
			 
		 }
		 return $data;
		 
	}
   
   public function selectProductSizeBySizeId($id)
   {
	   $this->db->where('size_id',$id);
		$data=$this->db->get('tb_sizeproduct');
		return $data->result();
   }
   
   public function getAvailableSizes($id)
   {
	   //select S.*,C.color_id,C.pro_id as pid from tbl_color S left join tb_colorproduct C on S.id=C.color_id where C.pro_id=1
	    $query = $this->db->query("select S.*,C.size_id,C.pro_id as pid from tbl_size S left join tb_sizeproduct C on S.id=C.size_id where C.pro_id=$id");
	   return $query->result();
   }
   
}
?>