<?php 
class product_model extends CI_Model{
	public function selectAllProduct()
	{
		$data=$this->db->get("tbl_product");
		return $data->result();
	}	
	public function listing(){
		$data=$this->db->get("tbl_product_color");
		return $data->result();
	}
	public function selectProductById($id){
         $this->db->where('id',$id);
         $data=$this->db->get("tbl_product");
         return $data->result();
	}
	public function Productupdate($id,$data){
		$this->db->where('id',$id);
		return $this->db->update("tbl_product",$data);	
	}
	function insert($data)
	{
		$this->db->insert('tbl_product', $data);
		$result= $this->db->insert_id();
		return $result;		
	}
	function insertProductImage($data)
	{
		$result= $this->db->insert('tbl_product_image', $data);
		return $result;
	}
	
	function categoryProductListing($id,$limit = null, $start= null)
	{
		$this->db->where('status',1);
		$this->db->where('category_id',$id);
		$this->db->limit($limit, $start);
		$data=$this->db->get("tbl_product");
		return $data->result();
	}
	public function getTotalcategoryProduct($id)
	{
		$this->db->where('category_id',$id); 
        $query = $this->db->get('tbl_product');
        return $query->num_rows();
    }
	
	function selectActiveProductByCategoryId($id)
	{
		$this->db->where('status',1);
		$this->db->where('category_id',$id);
		$data=$this->db->get("tbl_product");
		return $data->result();
	}
	public function selectProductSingleImage($id){
		$this->db->where('product_id',$id); 
		$this->db->limit(1);
		$data=$this->db->get("tbl_product_image");
		return $data->result();
	}
	public function selectProductImages($id){
		$this->db->where('product_id',$id); 
		$data=$this->db->get("tbl_product_image");
		return $data->result();
	}
	public function selectProductImageById($id){
		$this->db->where('id',$id);
		$data=$this->db->get("tbl_product_image");
		return $data->result();
	}
    function deleteProductImageById($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_product_image');
	}
	function deleteimage($id)
	{
		$this->db->where('product_id', $id);
		return $this->db->delete('tbl_product_image');
	}
	function selectAllReletedProduct($ids)
	{
		$this->db->where_in('id', $ids);
		$data=$this->db->get("tbl_product");
		return $data->result();
	}
	
	function update_image($id,$data){
		$this->db->where('id',$id);
		$data=$this->db->update("tbl_product_image",$data);
		return $data;
	}
	public function selectProductDoubleImage($id){
		$this->db->where('product_id',$id); 
		$this->db->limit(2);
		$data=$this->db->get("tbl_product_image");
		return $data->result();
	}
	function insertsizeproductbyid($sizedata)
	{
		$qry=$this->db->insert('tb_sizeproduct',$sizedata);
		return $qry;		
	}
	function deletesize($id)
	{
		$this->db->where('pro_id',$id);
		return $this->db->delete('tb_sizeproduct');
		
	}
	function insertcolorproductbyid($colordata)
	{
		$qry=$this->db->insert('tb_colorproduct',$colordata);
		return $qry;
	}
	function updatesizebyid($data)
	{
		$qry=$this->db->insert("tb_sizeproduct",$data);
	}
	function updatecolorbyid($data)
	{		
		$qry=$this->db->insert("tb_colorproduct",$data);		
	}
	function deletecolor($id)
	{
		$this->db->where('pro_id',$id);
		return $this->db->delete('tb_colorproduct');		
	}
	
	public function selectProductByVedorID($vender_id){
         $this->db->where('vender_type',$vender_id);
         $data=$this->db->get("tbl_product");
         return $data->result();
	}
        function selectactiveallproduct()
	{
		$this->db->where('status',1);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function ExportCSV()
	{

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $filename = "filename_you_wish.csv";
        $query = "SELECT * FROM tbl_product WHERE status='1'";
        $result = $this->db->query($query);
        $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
        $res=force_download($filename, $data);
		return $res;

}
	function selectreviewbyproid($id)
	{
		$this->db->where('pro_id',$id);
		$data=$this->db->get('tbl_review');
		return $data->result();
		
	}
	function selectreviewid($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_review');
		return $data->result();
	}
	function homeproductall($filed)
	{
		$this->db->where($filed,1);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function selecproducthomerand()
	{
		$this->db->order_by('id','RANDOM');
		$this->db->limit(8);
		$data=$this->db->get('tbl_product');
		return $data->result();
	}
	function selecthomeproductall($fild)
	{
		$this->db->where($fild,1);
		$data=$this->db->get('tbl_product');
		return $data->result();		
	}
	
	function getRecentlyViewedProduct()
	{
		session_start();	
		if(isset($_SESSION['mydata']))
		{
			return preg_replace("/[^a-zA-Z 0-9]+/", "",$_SESSION['mydata']);
		}
		else
		{
			return array(0);
 		}	
	}
	
	function getproductdiscountwise($dicount)
	{
		//$this->db->where($filed,1);
		$this->db->where("descount >= ",$dicount);
		$data=$this->db->get('tbl_product');
		return $data->result();	
	}
	
}