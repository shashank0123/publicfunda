<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Filesystem {
  public function upload($fineName,$uploadPath,$allow_ext) 
  {
		$fileType =$allow_ext;//array('jpg','png');
		$file = time().'_'.str_replace(' ','_',$_FILES[$fineName]['name']);
		$file_tmp= $_FILES[$fineName]['tmp_name'];
		$path = $uploadPath.$file;//'media/uploads/banner/'.$file;
			
		$file_ext = pathinfo($file,PATHINFO_EXTENSION);
		if($fileType!='*')
		{	
			if(!in_array($file_ext,$fileType))
			{
				echo "<div class='isa_error'>Error in file upload. please check file extention.</div>";
				return false;
			}
			else
			{
				move_uploaded_file($file_tmp,$path);
				return $file;
			}
		}	
		
  }
  
  public function multipleUpload($fineName,$uploadPath,$allow_ext) 
  {
		$fileType =$allow_ext;
		foreach($_FILES[$fineName]['name'] as $key => $value)
		{
			$file = time().'_'.str_replace(' ','_',$_FILES[$fineName]['name'][$key]);
			$file_tmp= $_FILES[$fineName]['tmp_name'][$key];
			$path = $uploadPath.$file;//'media/uploads/banner/'.$file;
				
			$file_ext = pathinfo($file,PATHINFO_EXTENSION);
			if(!in_array($file_ext,$fileType))
			{
				echo "<div class='isa_error'>Error in file upload. please check file extention.</div>";
				return false;
			}
			else
			{
				move_uploaded_file($file_tmp,$path);
				return $file;
			}
		}	
  }  
  	  
  
}