<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        
						<?php echo $this->session->flashdata('message'); ?>	
						<div class="table-responsive">
                             <table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="text-left" colspan="2"><h4 class="page-header">Order Information</h4></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-left"> 
							<b>Order ID:</b> #<?php echo $order[0]->id; ?><br>
							<b>Date Added:</b> <?php echo date('d/m/Y',$order[0]->ord_time); ?>
						</td>
						<td class="text-left"> 
							<b>Payment Method:</b> <?php echo $order[0]->payment_method; ?><br>
						</td>
					</tr>
				</tbody>
			</table>
			<?php $billing = $this->shoppingdetail_model->billingdetail($order[0]->id); ?>
			<?php $shipping = $this->shoppingdetail_model->shippingdetail($order[0]->id); ?>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="text-left"><b>Billing Address</b></td>
						<td class="text-left"><b>Shipping Address</b></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-left">
							<?php echo $billing[0]->firstname.' '.$billing[0]->lastname; ?><br>
							<?php echo $billing[0]->mobile;?><br>
							<?php echo $billing[0]->address; ?><br>	
                            <?php echo $billing[0]->pincode?><br> 							
							<?php echo $billing[0]->city;?><br>
							<?php echo $billing[0]->state;?><br>
							<?php echo $billing[0]->country?>							
						</td>
						<td class="text-left">
							<?php echo $shipping[0]->firstname.' '.$shipping[0]->lastname; ?><br>
							<?php //echo $shipping[0]->mobile;?>
							<?php echo $shipping[0]->address; ?><br>	
                            <?php echo $shipping[0]->pincode?><br> 							
							<?php echo $shipping[0]->city;?><br>
							<?php echo $shipping[0]->state;?><br>
							<?php echo $shipping[0]->country?>
						</td>
					</tr>
				</tbody>
			</table>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="text-left">Product Name</td>
						<td class="text-right">Price</td>
						<td class="text-right">Quantity</td>						
						<td class="text-right">Total</td>
					</tr>
				</thead>
				<tbody>
				   <?php $items = $this->shoppingdetail_model->prodcutdetail($order[0]->id); ?>
				   <?php if(count($items)>0){ ?>
				   <?php foreach($items as $item){ ?>
					<tr>
						<td class="text-left"><?php echo $item->pro_name; ?>
						<?php if(!empty($item->color) && $item->color!='0'){ ?><br>
						&nbsp;<small><b>Color :</b> <?php echo $item->color; ?></small>
						<?php } ?>
						<?php if(!empty($item->size) && $item->size!='0'){ ?><br>
						&nbsp;<small><b>Size :</b> <?php echo $item->size; ?></small>
						<?php } ?>
						</td>
						<td class="text-right">$<?php echo $item->unit_price; ?></td>
						<td class="text-right"><?php echo $item->qty; ?></td>						
						<td class="text-right">$<?php echo $item->total_amount; ?></td>
					</tr>
				   <?php } ?> 
				   <?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="2"></td>
						<td class="text-right"><b>Sub-Total</b></td>
						<td class="text-right">$<?php echo $order[0]->total_amount; ?></td>
					</tr>
					<tr>
						<td colspan="2"></td>
						<td class="text-right"><b>Shipping</b></td>
						<td class="text-right">Free</td>
					</tr>
					<tr>
						<td colspan="2"></td>
						<td class="text-right"><b>Total</b></td>
						<td class="text-right">$<?php echo $order[0]->total_amount; ?></td>
					</tr>
				</tfoot>
			</table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
