<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo base_url('index.php/admin/dashboard'); ?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>	

					
			<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#popup">
							<i class="fa fa-fw fa-arrows-v"></i>Home Page Popup<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="popup" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/homepagepopup/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Popup Listing
								</a>
                            </li>
							
                        </ul>
                    </li>		
					
							<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Vender<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/admin/userList'); ?>">Vender List</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/adduser'); ?>">Add Vender</a>
                            </li>
                        </ul>
                    </li>   
					
							<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#register">
							<i class="fa fa-fw fa-arrows-v"></i>Register User<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="register" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/user/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Register User</a>
                            </li>
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#cat">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Category<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="cat" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/categorycontroller/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Category Listing</a>
                            </li>
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#product">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Products<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="product" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/adminproducts/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Product  Listing</a>
                            </li>
                        </ul>
                    </li>
					
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#csv">
							<i class="fa fa-fw fa-arrows-v"></i>Upload Csv<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="csv" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/csv/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Upload Csv</a>
                            </li>
                        </ul>
                    </li>	

					
						<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#filters">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Filters<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="filters" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/filters/listing'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Filters Master</a>
                            </li>
                        </ul>
                    </li>	
	

						<!--<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#size">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Size Master<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="size" class="collapse">
                            <li>
                                <a href="<?php //echo base_url('index.php/admin_sizemaster/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Size Master</a>
                            </li>
                        </ul>
                    </li>	-->


						<!--<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#color">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Color Master<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="color" class="collapse">
                            <li>
                                <a href="<?php //echo base_url('index.php/admin_color/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Color Master</a>
                            </li>
                        </ul>
                    </li>	-->
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#coupon">
                            <i class="fa fa-fw fa-arrows-v"></i>Manage Coupon Code<i class="fa fa-fw fa-caret-down"></i>
                        </a>
                        <ul id="coupon" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/offer/listing'); ?>">
                                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                    Manage Coupon Listing
                                </a>
                            </li>
                            
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#faq">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Faq<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="faq" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/faq/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									FAQ Listing
								</a>
                            </li>							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#cms">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Cms<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="cms" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/cms/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Page Listing
								</a>
                            </li>
							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#newsletter">
							<i class="fa fa-fw fa-arrows-v"></i>New Letter<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="newsletter" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/newsletter/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									New Letter
								</a>
                            </li>
							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#clint">
							<i class="fa fa-fw fa-arrows-v"></i>Our Clients<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="clint" class="collapse">
						
						
						
                            <li>
                                <a href="<?php echo base_url('index.php/our_client/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Our Clients
								</a>
                            </li>
							
							
							
                        </ul>
						
                    </li>
					<li>
                        <a href="<?php echo base_url('index.php/banner/listing/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Banner</i>
						</a>
                    </li>
					<li>
                        <a href="<?php echo base_url('index.php/order/listing/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Order Detail</i>
						</a>
                    </li> 
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#metatg">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Meta Tag<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="metatg" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/admin_meta_tag/'); ?>">
								<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
								Meta Tag</a>
                            </li>
                        </ul>
                    </li>
					
            </div>