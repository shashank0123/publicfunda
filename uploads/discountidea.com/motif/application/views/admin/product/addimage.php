 <?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<style>
div.gallery {
    margin: 5px;
    border: 1px solid #ccc;
    float: left;
    width: 180px;
}

div.gallery:hover {
    border: 1px solid #777;
}

div.gallery img {
    width: 100%;
    height: auto;
}

div.desc {
    padding: 5px;
	font-size: 22px;
    text-align: center;
}
</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

				
					 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Manage Product Image</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">

				<form  enctype="multipart/form-data"  method="POST">

							<table class="table table-bordered table-striped table-condensed" style="">										
								<tbody>
								  
									
									
									
									
									
								
									<tr>
										<td>Image</td>
										<td>
										<label>add Image  <button type="button" class="btn btn-xs btn-success" onclick="return addimage();"> <b>add</b> </button></label>
										<div id="appendimage"></div>
										<!--<span id="btn1" class="status_checks btn btn-success">ADD Image Slider</span>-->
										<input type="hidden" name="oldImage" class="form-control">
										</td>
									</tr>

									<tr>
										<td>Status</td>
										
										<td><select  name="status" class="form-control">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
										</select>
										</td>
									</tr>
									
																	
									<tr>
			<td colspan="2"><button type="submit" class="btn btn-info" name="add_product">Submit</button></td>
									</tr>
								</tbody>
							</table>

						</form>
				</div>
				</div>
						</div>
					<!-- end: page -->
			</div>

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
	 </div>
	 
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>/*
$(document).ready(function(){
    $("#btn1").click(function(){
    	var i=0;
    	i++;
$("p").append("<span id="rmdiv'+ i +'"><input type='file' name='image[]' multiple><input type='text' name='image_title[]' multiple><button onclick="return removefile('+ i +')" type="button" class="btn btn-xs btn-danger imgremove"> <b>X</b></button>");
     
        //$("p1").append("<input type='text' name='image_title[]' multiple>");
        //alert("sssssssssss");
    });

  
});*/
</script>
<script>
	var i = 0;
function addimage()
{
	i++;
	var fileHtml = '<span id="rmdiv'+ i +'"><div class="col-md-12" style="padding: 9px;"><div class="col-md-4"><input type="file" class="form-control" name="image[]" id="image" required></div><div class="col-md-7"> <input type="text" class="form-control" name="image_title[]" id="image_title" placeholder="enter image title" required></div><div class="col-md-1"><button onclick="return removefile('+ i +')" type="button" class="btn btn-xs btn-danger imgremove"> <b>X</b></button></div></div></span>';
	$('#appendimage').append(fileHtml);
}
function removefile(ID)
{
	$('#rmdiv'+ ID).remove();
}
</script>
</body>

</html>