 <?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<style>
.naseemdiv
{
	height: 210px;
    overflow: scroll;
    border: 2px solid rgba(34, 34, 34, 0.41);
    padding: 10px;
}
.naseemdiv label{padding: 3px;}
</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

				
					 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Add Product</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">

				<form  enctype="multipart/form-data" method="POST">

							<table class="table table-bordered table-striped table-condensed" style="">										
								<tbody>
								
								
									
								   <tr>
										<td> Category</td>
										<td>
											<select  name="category_id" class="form-control">
											<?php 
 $data = $this->category_model->getSelectOption(0);
												/*$query = $this->category_model->selectAllCategory();												
												foreach ($query as $row)
												{
												echo "<option value='" . $row->id. "'>" . $row->title .  "</option>";
												} */
											?>									
											</select>
										</td>
									</tr>
									
									<tr>
										<td> Vender Name</td>
										<td>
											<select  name="vendername" class="form-control">
											<option value="0">Select Vender</option>
											<?php 
												$query = $this->admin_model->selectvendername();												
												foreach ($query as $row)
												{
												echo "<option value='" . $row->id. "'>" . $row->fname.  "</option>";
												} 
											?>									
											</select>
										</td>
									</tr>
									
									
									<tr>
										<td>Product Name</td>
										<td>
		<input type="text" name="name"  class="form-control" autocomplete="off" required>
										</td>
									</tr>
								
									<tr>
										<td> Sort Description</td>
										<td>
<textarea name="sortdesc" value="" class="form-control" rows="3"></textarea>
										</td>
									</tr>
									<tr>
										<td> Description</td>
										<td>
<textarea name="description"  class="form-control" rows="4"></textarea>
										</td>
									</tr>
									<tr>
										<td>Price</td>
										<td>
<input type="text" name="price"  class="form-control" autocomplete="off" required>
										</td>
									</tr>
									<tr>
										<td>Spacel Price</td>
										<td>
<input type="text" name="spacel_price"  class="form-control" autocomplete="off">
										</td>
									</tr>
									<tr>
										<td> Descount</td>
										<td>
<input type="text" name="descount"  class="form-control" autocomplete="off">
										</td>
									</tr>
									<tr>
										<td>Releted Product</td>
										<td>
											<div class="naseemdiv">
											<?php 
											$query = $this->product_model->selectAllProduct();
											foreach ($query as $row){ ?>
												<label><input type="checkbox"  name="reletedproduct[]"  value="<?php echo $row->id;?>" /> <?php echo $row->name; ?></label><br>
											<?php } ?>
											</div>
										</td>
									</tr>
									
									
									<!--<tr>
										<td>Product Size</td>
										<td>
											<div class="naseemdiv">
											<?php 
											$query = $this->size_model->selectsize();
											foreach ($query as $row){ ?>
								<label><input type="checkbox"  name="sizeproduct[]"  value="<?php //echo $row->id;?>" /> <?php echo $row->size; ?></label><br>
											<?php } ?>
											</div>
										</td>
									</tr>-->
									
									<!--<tr>
										<td>Product Color</td>
										<td>
											<div class="naseemdiv">
											<?php 
											$query = $this->color_model->selectcolor();
											foreach ($query as $row){ ?>
								<label><input type="checkbox"  name="coloproduct[]"  value="<?php //echo $row->id;?>" /> <?php //echo $row->color; ?></label><br>
											<?php } ?>
											</div>
										</td>
									</tr>-->
									
									<tr>
										<td>Product Stock</td>
										<td>
                                            <input type="text" name="product_stock"  class="form-control" autocomplete="off">
										</td>
									</tr>
									
									<tr>
										<td>Deal Of The Day</td>
										
										<td><select  name="dealday" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>The Grand Gadget Sale</td>
										
										<td><select  name="grandgadget" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Top Selling Smart Phone</td>
										
										<td><select  name="smartphone" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Disocunt For You</td>
										
										<td><select  name="discount" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Offer For You</td>
										
										<td><select  name="offer" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									
									<tr>
										<td>Laptop And Desktop</td>
										
										<td><select  name="laptop" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Gas Stove</td>
										
										<td><select  name="gas" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Women's Kurti's</td>
										
										<td><select  name="kurti" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Great Offer On Furniture</td>
										
										<td><select  name="furniture" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Sports & Fitness Essentials</td>
										
										<td><select  name="sports" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Small Home Appliances</td>
										
										<td><select  name="appliance" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Deal In IT</td>
										
										<td><select  name="it" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Fashion And Travle Accessories</td>
										
										<td><select  name="travel" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>Best Selling Tv's</td>
										
										<td><select  name="tv" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									<tr>
										<td>New Arrivals</td>
										
										<td><select  name="arrival" class="form-control">
										<option value="0">No</option>
										<option value="1">Yes</option>
										</select>
										</td>
									</tr>
									
									
									
									
									
									
									<tr>
										<td>Meta Title</td>
										<td>
                                            <input type="text" name="producttitle"  class="form-control" autocomplete="off">
										</td>
									</tr>
									
									<tr>
										<td>Meta Keyword</td>
										<td>
                                            <input type="text" name="productkeyword"  class="form-control" autocomplete="off">
										</td>
									</tr>
									
									<tr>
										<td>Meta Description</td>
										<td>
                                            <input type="text" name="productdescription"  class="form-control" autocomplete="off">
										</td>
									</tr>
									

									<tr>
										<td>Status</td>
										
										<td><select  name="status" class="form-control">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
										</select>
										</td>
									</tr>
									
																	
									<tr>
			<td colspan="2"><button type="submit" class="btn btn-info" name="add_product">Submit</button></td>
									</tr>
								</tbody>
							</table>

						</form>
				</div>
				</div>
						</div>
					<!-- end: page -->
			</div>

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
	 </div>
	 
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>