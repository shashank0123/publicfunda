<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Color</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">
 
				<form enctype="multipart/form-data"  action="" method="POST">
                                   <?php //print_r($EDITCOLOR);?>
							<table class="table table-bordered table-striped table-condensed" style="">										
								<tbody>
								  
									<tr>
										<td>Color</td>
										<td>
		<input type="text" name="color" value="<?php echo  $EDITCOLOR[0]->color; ?>" class="form-control">
										</td>
									</tr>							
									<tr>
			<td colspan="2"><button type="submit" class="btn btn-info" name="edit">Submit</button></td>
									</tr>
								</tbody>
							</table>

						</form>
					
					   </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    </div>
    </div>
    </div>
    <
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
