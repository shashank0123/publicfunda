<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Newsletter listing</h2>
						<?php echo $this->session->flashdata('message'); ?>	
						<div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
										<th width="5%">S.No.</th>
                                        <th>Email</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php $i=0; foreach($NEWSLETTERDATA as $data){ $i++; ?>
                                    <tr>
										<td><?php echo $i; ?></td>
                                        <td><?php echo $data->email; ?></td>
								
											<td><a href="<?php echo base_url('index.php/newsletter/edit/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<a href="<?php echo base_url('index.php/newsletter/delete/'.$data->id); ?>" onclick="return deleteConfirm();" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i></a></td>																				
										</td>
                                    </tr>
                                <?php } ?>                                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
