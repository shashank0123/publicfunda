<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(!empty($admin_id))
{
	redirect('index.php/admin/dashboard');
}	
?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login Form</title>
  <link href="<?php echo base_url('assets/admin'); ?>/css/login-form.css" rel="stylesheet">
  <style>
	.isa_error 
	{
		margin: 10px 0px;
		padding:12px;	
		color: #D8000C;
		background-color: #FFBABA;
		border-radius: 5px;
        font-weight: bold;
	}
  </style>
</head>

<body>
  <div class="login">  
	<h1>Login</h1>
	<?php echo $this->session->flashdata('message'); ?>
    <form method="post">
    	<input type="text" name="username" placeholder="Username" />
        <input type="password" name="password" placeholder="Password" />
		<p><a href="<?php echo base_url('index.php/admin/forgot_password'); ?>">Forgot password</a></p>
        <button type="submit" name="loginbutton" class="btn btn-primary btn-block btn-large">Login</button>
    </form>
</div>
</body>
</html>
