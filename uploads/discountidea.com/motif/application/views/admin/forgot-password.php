<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(!empty($admin_id))
{
	redirect('index.php/admin/dashboard');
}	
?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login Form</title>
  <link href="<?php echo base_url('assets/admin'); ?>/css/login-form.css" rel="stylesheet">
</head>

<body>
  <div class="login">  
	<h1>Forgot Password</h1>
	<?php echo $this->session->flashdata('message'); ?>
    <form method="post">
    	<input type="text" name="email" placeholder="Enter email address" />		
        <button type="submit" name="submitbutton" class="btn btn-primary btn-block btn-large">Submit</button>
		<p><a href="<?php echo base_url('index.php/admin/index'); ?>">Login</a></p>
    </form>
</div>
</body>
</html>
