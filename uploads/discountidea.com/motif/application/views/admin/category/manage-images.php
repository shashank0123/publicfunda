 <?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<style>
div.gallery {
    margin: 5px;
    border: 1px solid #ccc;
    float: left;
    width: 180px;
}

div.gallery:hover {
    border: 1px solid #777;
}

div.gallery img {
    width: 100%;
    height: auto;
}

div.desc {
    padding: 5px;
	font-size: 22px;
    text-align: center;
}
</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
<h3 >Manage Category Image <a class="btn btn-success pull-right" href="<?php echo base_url('index.php/categorycontroller/listing');?>" style="float: right;">Back</a></h3>

					<!-- start: page -->
					<div class="row">
					<div class="col-md-12">						
						<br><?php echo $this->session->flashdata('message'); ?>
						<div class="col-md-6 col-lg-12 col-xl-6">
							<div class="panel panel-sign">					
								<div class="panel-body">								    
									<?php if(count($LISTCATEGORYIMAGE)>0){ ?>
									<?php foreach($LISTCATEGORYIMAGE as $data){ ?>
									<div class="gallery">
										<img src="<?php echo base_url('uploads/category/'.$data->image); ?>" alt="Fjords" width="300" height="200">									  
										<div class="desc"><a href="<?php echo base_url('index.php/categorycontroller/deletecategoryimages/'.$data->id.'/'.$data->category_id);?>" title="delete this record" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
									</div>
									<?php } }  ?>
                                    <form enctype="multipart/form-data" method="POST">									
										<table class="table table-bordered table-striped mb-none">
											<tr>
												<td>
													<a class="btn btn-xs btn-warning" onclick="return addimage();" style="float: right;"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>
													<div id="appendimage"></div>
													<button class="btn btn-success" id="uploadbutton" name="uploadfiles">Upload</button>
												</td>
											</tr>										
										</table>
									</form>
								</div>
							</div>
						</div>
					<!-- end: page -->
				</div>

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
	 </div>
	  </div>
	   </div>
    <!-- /#wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>	
<script>
	var i = 0;
function addimage()
{
	i++;
	var fileHtml = '<span id="rmdiv'+ i +'"><div class="col-md-12" style="padding: 9px;"><div class="col-md-11"><input type="file" class="form-control" name="image[]" id="image" required></div><div class="col-md-1"><button onclick="return removefile('+ i +')" type="button" class="btn btn-xs btn-danger imgremove"> <b>X</b></button></div><strong style="margin-left: 67px;">size 1365 * 525</strong></div></span><br> ';
	$('#appendimage').append(fileHtml);
}
function removefile(ID)
{
	$('#rmdiv'+ ID).remove();
}
</script>	
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>