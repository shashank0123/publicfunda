<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit Category</h1>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">
 
				<form enctype="multipart/form-data"  action="" method="POST">
                                   <?php //print_r($EDITCATEGORY);?>
							<table class="table table-bordered table-striped table-condensed" style="">										
								<tbody>
								   <tr>
										<td>Parent Category</td>
										<td>
											<select  name="parent_id" class="form-control">
											<option value="0">Root</option>
											<?php 
												$query = $this->category_model->selectAllCategory();
												foreach ($query as $row)
												{												
													if($row->id!=$EDITCATEGORY[0]->id)
													{	
											?>
											<option <?php echo ($row->id==$EDITCATEGORY[0]->parent_id)?"selected" :"" ?>  value="<?php echo $row->id; ?>" > <?php echo $row->title; ?>
											</option>
												<?php
													} }
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td>Title</td>
										<td>
		<input type="text" name="name" value="<?php echo  $EDITCATEGORY[0]->title; ?>" class="form-control">
										</td>
									</tr>
									
									<!--<tr>
										<td>Image</td>
										<td>
											<?php //if($EDITCATEGORY[0]->icon!=""){ ?>
												<img src="<?php //echo base_url('uploads/category/'.$EDITCATEGORY[0]->image); ?>" width="80">
											<?php ?>
											<input type="file" name="image" class="form-control">
											<input type="hidden" name="oldImage" class="form-control" value="<?php //echo  $EDITCATEGORY[0]->image; ?>" >
										</td>
									</tr>-->
																											
									<tr>
										<td>Display On Home</td>
										<td>
											<select  name="display_on_home" class="form-control">
												<option <?php echo ($EDITCATEGORY[0]->display_on_home==0)?"selected" :"" ?> value="0">No</option>
												<option <?php echo ($EDITCATEGORY[0]->display_on_home==1)?"selected" :"" ?> value="1">Yes</option>
											</select>
										</td>
									</tr>
									
									<?php 
										if(!empty($EDITCATEGORY[0]->filters_id))
										{
											$filters_array = explode(",",$EDITCATEGORY[0]->filters_id);
										}
										else
										{
											$filters_array = array();
										}	
									?>
									<tr>
										<td>Display Filters</td>
										<td>
											<?php $filter_data = $this->filters_model->selectAllFilters(); ?>
											<select  name="filters_id[]" class="form-control" multiple>
												<?php foreach($filter_data as $filter){ ?>
													<option <?php echo (in_array($filter->id, $filters_array))?"selected" :"" ?> value="<?php echo $filter->id; ?>"><?php echo $filter->title; ?></option>
												<?php } ?>	
											</select>
										</td>
									</tr>
									
									
									
									<tr>
										<td>Meta Title</td>
										<td>
		<input type="text" name="metatitle" value="<?php echo  $EDITCATEGORY[0]->meta_title; ?>" class="form-control">
										</td>
									</tr>
									
									<tr>
										<td>Meta keyword</td>
										<td>
		<input type="text" name="metakeyword" value="<?php echo  $EDITCATEGORY[0]->meta_keyword; ?>" class="form-control">
										</td>
									</tr>
									
									
									<tr>
										<td>Meta Description</td>
										<td>
		<input type="text" name="metadescription" value="<?php echo  $EDITCATEGORY[0]->meta_description; ?>" class="form-control">
										</td>
									</tr>
									
									<tr>
										<td>Category Status</td>
										<td>
										<select  name="status" class="form-control">
										<option <?php echo ($EDITCATEGORY[0]->status==1)?"selected" :"" ?> value="1">Active</option>
										<option <?php echo ($EDITCATEGORY[0]->status==0)?"selected" :"" ?> value="0">Inactive</option>
										</select>
										</td>
									</tr>
									
																	
									<tr>
			<td colspan="2"><button type="submit" class="btn btn-info" name="edit">Submit</button></td>
									</tr>
								</tbody>
							</table>

						</form>
					
					   </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
