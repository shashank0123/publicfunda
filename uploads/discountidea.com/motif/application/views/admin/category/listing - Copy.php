 <?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
	<link rel="stylesheet" href="<?php echo base_url('assets/tree'); ?>/css/tree-style.css">
<style>
.btnclass{    position: absolute;
    right: 41px;
    top: 23px;}
</style>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    <h3 >Manage Category  
						<a class="btn btn-success pull-right" href="<?php echo base_url('index.php/categorycontroller/add');?>" style="float: right;">Add Category</a>
					</h3>	
					<!-- start: page -->
					<div class="row">
					<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">
<div class="tree">	
		/* ?>
		<ul>
				<li><div class="mytree" ><a>First Level</a> </div><span style="float: right; margin-top: -2%;"> fhfghfghfghfg </span>
				
					<ul>
						<li><a>Second Level</a><span style="float: right;"> fhfghfghfghfg </span></li>
						<li><a>Second Level</a><span style="float: right;"> fhfghfghfghfg </span></li>
					</ul>
					
				</li>

		</ul>*/ ?>
</div>


<?php /*
									<table class="table table-bordered table-striped mb-none">									
							<thead>
								<tr class="gradeX">
								    <th>S.No</th>								    
									<th>Title</th>
									<th>Parent Category</th>
									<th>Status</th>									
									<th>action</th>
								</tr>
							</thead>
							<tbody>
							
							<?php if(count($LISTCATEGORY)>0){ ?>
							 <?php $i=0; foreach($LISTCATEGORY as $LISTCATEGORY){ $i++;?>
								<tr class="gradeX">
									<td>
										<?php echo $i;?>	
										</td>
										<td><?php echo $LISTCATEGORY->title;?></td>	
										<td>
										<?php 
										$query = $this->category_model->selectCategoryByID($LISTCATEGORY->parent_id);                              
											echo($LISTCATEGORY->parent_id==0)?'<strong>Root</strong>':$query[0]->title; 
										?>						
									</td>																
									<td>
										<i data="<?php echo $LISTCATEGORY->id;?>" class="status_checks btn <?php echo ($LISTCATEGORY->status==1)? 'btn-success' : 'btn-danger'?>"><?php echo ($LISTCATEGORY->status)? 'Active' : 'Inactive'?></i>
									</td>
									<td>
										<a href="<?php echo site_url('index.php/categorycontroller/manageimage/'.$LISTCATEGORY->id.''); ?>" title="Manage category Image"><i class="fa fa-picture-o" aria-hidden="true"></i></a>
										<a href="<?php echo site_url('index.php/categorycontroller/edit/'.$LISTCATEGORY->id.''); ?>" title="edit this record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
										<a href="<?php echo site_url('index.php/categorycontroller/deletecategory/'.$LISTCATEGORY->id.''); ?>" title="delete this record" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</td>
								</tr>
								<?php
							}
						}
							?>																				
							</tbody>
						</table>
*/ ?>
					
					</div>
				</div>
						</div>
					<!-- end: page -->
			</div>

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
	 </div>
	  </div>
	   </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script src="http://code.jquery.com/jquery-1.7.2.min.js" type="text/javascript" > </script>	
	<script type="text/javascript">

		$( document ).ready( function( ) {
				$( '.tree li' ).each( function() {
						if( $( this ).children( 'ul' ).length > 0 ) {
								$( this ).addClass( 'parent' );     
						}
				});
				
				$( '.tree li.parent > .mytree' ).click( function( ) {
						$( this ).parent().toggleClass( 'active' );
						$( this ).parent().children( 'ul' ).slideToggle( 'fast' );
				});
				
				$( '#all' ).click( function() {
					
					$( '.tree li' ).each( function() {
						$( this ).toggleClass( 'active' );
						$( this ).children( 'ul' ).slideToggle( 'fast' );
					});
				});
				
		});        
	</script>
</body>

</html>