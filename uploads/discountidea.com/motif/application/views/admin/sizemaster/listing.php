 <?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

<a class="btn btn-success" href="<?php echo base_url('index.php/admin_sizemaster/add');?>" style="float: right;">Add Size</a>
					<!-- start: page -->
					<div class="row">
					<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>
						<div class="col-md-6 col-lg-12 col-xl-6">
						    <div class="panel panel-sign">
					
					<div class="panel-body">

			<form enctype="multipart/form-data" action="<?php echo base_url();?>index.php/category/list_sub_admin/" method="POST">

									<table class="table table-bordered table-striped mb-none">
									<?php
									//print_r($LISTPRODUCT);
									?>
									
							<thead>
								<tr class="gradeX">
								    <th>Sr No</th>
								    <th>Product Category</th>
									<th>Size</th>
									<th>Product Status</th>
									<th>action</th>
								</tr>
							</thead>
							<tbody>
							
							<?php if(count($sizelist)>0){ ?>
							 <?php $i=0; foreach($sizelist as $LISTPRODUCT){ $i++;?>
								<tr class="gradeX">
									<td>
									<?php echo $i;?>	
									</td>
									<td>
									<?php
									
                                    $query = $this->category_model->selectCategoryByID($LISTPRODUCT->category_id);
                                    //print_r($query);
                                    echo (isset($query[0]->title))?$query[0]->title:'No Define Category';                                       
									?>
									</td>
									<td><?php echo $LISTPRODUCT->size;?></td>
												
								<td><i data="<?php echo $LISTPRODUCT->id;?>" class="status_checks btn <?php echo ($LISTPRODUCT->status)? 'btn-success' : 'btn-danger'?>"><?php echo ($LISTPRODUCT->status)? 'Active' : 'Inactive'?></i>
								</td>
		<td>
		<a href="<?php echo site_url('index.php/admin_sizemaster/editsize/'.$LISTPRODUCT->id.''); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;" ?>
		<a href="<?php echo site_url('index.php/admin_sizemaster/deletesize/'.$LISTPRODUCT->id.''); ?>" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i></a>
								</td>
								</tr>
								<?php
							}
						}
							?>																				
							</tbody>
						</table>

						</form>
					</div>
				</div>
						</div>
					<!-- end: page -->
			</div>

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
	 </div>
	  </div>
	   </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>