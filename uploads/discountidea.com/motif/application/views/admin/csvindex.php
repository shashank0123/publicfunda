<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
    redirect('index.php/admin');
}   
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

                <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Upload Product Using Csv File</h1>
                        <div class="col-md-6  col-xl-6">
                            <div class="panel panel-sign">
                    
                    <div class="panel-body">
					

                 
     
                 <?php if (isset($error)): ?>
                    <div class="alert alert-error"><?php echo $error; ?></div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('success') == TRUE): ?>
                    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                    <form method="post" action="<?php echo base_url() ?>index.php/csv/importcsv" enctype="multipart/form-data">
                        <input type="file" name="userfile" ><br><br>
                        <input type="submit" name="submit" value="UPLOAD" class="btn btn-primary">
                    </form>
                </div>
                </div>
                        </div>
						<div class="col-md-6 col-xl-6">
						<a href="<?php echo base_url('adminproducts/dowloadcsv');?>"><img src="<?php echo base_url('uploads/Logo Images.jpg');?>" style="    height: 51px;
    width: 61px;"><strong>Download Here</strong></a>
	

						</div>
                    <!-- end: page -->
            </div>
			

 </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
     </div>
    
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>