﻿<?php $this->load->view('front/layout/header');?>  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
           
            <li class="category13"><strong>Contact Us</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 

  <!-- Main Container -->
  <?php
			  echo $this->session->flashdata('message1');
			  ?>
  <div class="main-container col1-layout wow bounceInUp animated">
    <div class="main container">
      <div class="row">
        <section class="col-main col-sm-12">
          <div class="page-title">
            <h2>Contact Us</h2>
          </div>
          <div id="contact1" class="page-content page-contact">
            
            <div class="row">
              <?php
			  $privacy=12;
			  $data=$this->cms_model->selectprivacypolicy($privacy);
  ?>
	
		<?php echo $data[0]->slide_text;?>	
			  <form method="post" action="<?php echo base_url('home/contact_us');?>">
              <div class="col-sm-6">
                <h3 class="page-subheading">Make an enquiry</h3>
                <div class="contact-form-box">
                  <div class="form-selector">
                    <label>Name</label>
                    <input type="text" class="form-control input-sm" name="name" id="name"  required/>
                  </div>
                  <div class="form-selector">
                    <label>Email</label>
                    <input type="email" class="form-control input-sm"  name="email" id="email" required/>
                  </div>
                  <div class="form-selector">
                    <label>Phone</label>
                    <input type="number" class="form-control input-sm" name="phone" id="phone" required />
                  </div>
                  <div class="form-selector">
                    <label>Message</label>
                    <textarea class="form-control input-sm" rows="10" name="message" id="message"></textarea>
                  </div>
                  <div class="form-selector">
                    <button class="button" name="contactbutn"><i class="fa fa-send"></i>&nbsp; <span>Send Message</span></button>
                    &nbsp; <a href="#" class="button">Clear</a> </div>
                </div>
              </div>
			  </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <?php $this->load->view('front/layout/footer');?>