
  <!-- end header --> 
  
  <!-- Navbar -->
 <?php $this->load->view('front/layout/header');?>
  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
           
            <li class="category13"><strong>FAQ</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End -->
    <div class="container faq-page">
<div class="row">
<div class="col-xs-12">
          <div class="page-title">
            <h2>Frequently Asked Questions</h2>
            
                      </div></div>
          <div class="col-xs-12 col-sm-9">
          <div class="panel-group accordion-faq" id="faq-accordion">
            
            <?php
			foreach($FAQDATA as $data)
			{
				
			?>
            
            <div class="panel">
              <div class="panel-heading"> <a data-toggle="collapse" data-parent="#faq-accordion" href="#<?php echo $data->id;?>" class="collapsed"> <span class="arrow-down"><i class="fa fa-question-circle"></i></span> <span class="arrow-up"><i class="fa fa-question-circle"></i></span> <?php echo $data->title;?>. </a> </div>
              <div id="<?php echo $data->id;?>" class="panel-collapse collapse">
                <div class="panel-body"> <?php echo $data->description;?>. </div>
              </div>
            </div>
			<?php
			}
			?>
			
			
			
          </div></div>
          <div class="col-sm-3 col-xs-12">
          
          
        <div><img src="<?php echo base_url('assets/front/');?>/images/faq-banner.png" alt="faq banner"></div>
      </div>
        </div>
</div>
  
 <!-- our clients Slider -->
 <?php $this->load->view('front/layout/footer');?>