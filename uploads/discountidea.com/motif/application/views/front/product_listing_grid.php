<?php $this->load->view('front/layout/header');?>
<?php $breadcrumbs = $this->category_model->breadcrumbs($pagedata[0]->id,$pagedata[0]->id); ?>
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
		
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url(); ?>">Home</a><span>&raquo;</span></li>
            <?php if(count($breadcrumbs)>0)
			{
				foreach($breadcrumbs as $breadcrumb)
				{
					echo $breadcrumb['breadcrumbs'];
				}
			} ?>			
		 </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  <!-- Main Container -->
  <div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
        <aside class="left sidebar col-sm-3 col-xs-12">      
          <?php $this->load->view('front/layout/left-side-bar');?>
        </aside>
        <div class="col-main col-sm-9">
          <div class="page-title">
            <h2><?php echo (isset($pagedata[0]->title))?$pagedata[0]->title:''; ?></h2>
          </div>
          <div class="category-description std">
            <div class="slider-items-products">
              <div id="category-desc-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col1 owl-carousel owl-theme"> 
           
                  <!-- Item -->
                  <?php if(count($categoryimages)>0){ ?>
				  <?php foreach($categoryimages as $cat_image){ ?>
				  <div class="item"><a href="#x"><img alt="" src="<?php echo base_url('uploads/category/'.$cat_image->image); ?>"></a></div>
                  <?php }}else{ ?>
                  <div class="item"> <a href="#x"><img alt="" src="http://placehold.it/850x320?text=Image not found"></a> </div>
                  <?php } ?>
                  <!-- End Item --> 
                  
                </div>
              </div>
            </div>
          </div>
          <div class="toolbar">
            <div class="view-mode">
              <ul id="productdisplay">
                <li class="active" onclick="return mytab('griddata','listdata');"> <a href="javascript:void(0);" > <i class="fa fa-th-large"></i> </a> </li>
                <li onclick="return mytab('listdata','griddata');"> <a href="javascript:void(0);"> <i class="fa fa-th-list"></i> </a> </li>
              </ul>
            </div>
            <div class="sorter">
			  <!--
              <div class="short-by">
                <label>Sort By:</label>
                <select>
                  <option selected="selected">Position</option>
                  <option>Name</option>
                  <option>Price</option>
                  <option>Size</option>
                </select>
              </div>
              <div class="short-by page">
                <label>Show:</label>
                <select>
                  <option selected="selected">8</option>
                  <option>12</option>
                  <option>16</option>
                  <option>30</option>
                </select>
              </div>
			  -->
            </div>
          </div>
		  <span id="filterresponse" >
          <div class="product-grid-area" id="griddata">
            <ul class="products-grid">
			<?php //print_r($productdata); ?>
			  <?php if(count($productdata)>0){ ?>
			  <?php foreach($productdata as $product){ ?>
              <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 wow fadeInUp">
                <div class="product-item">
                  <div class="item-inner">
                    <div class="product-thumbnail">
                      <!--<div class="icon-sale-label sale-left">Sale</div>
                      <div class="icon-new-label new-right">New</div>-->
                      <div class="pr-img-area">
						  <figure>
							<?php $product_image = $this->product_model->selectProductDoubleImage($product->id); ?>
							<?php if(count($product_image)>0){ ?>
							<img class="first-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[0]->image; ?>" alt="<?php echo $product->name; ?>"> 
							<img class="hover-img" src="<?php echo base_url('uploads/product/thumbs'); ?>/<?php echo $product_image[1]->image; ?>" alt="<?php echo $product->name; ?>">
							<?php }else{ ?>
							<img class="first-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $product->name; ?>"> 
							<img class="hover-img" src="http://placehold.it/264x264?text=Image not found" alt="<?php echo $product->name; ?>">
							<?php } ?>
						 </figure>
						 <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
						   <input type="hidden" name="pid" value="<?php echo $product->id; ?>" >
						   <input type="hidden" name="qty" value="1" >
<button type="button" name="addtocartbutton1" onclick="return addtocart(<?php echo $product->id; ?>);" class="add-to-cart-mt"> <i class="fa fa-shopping-cart"></i><span> Add to Cart</span> </button>
						   <button type="submit" name="addtocartbutton" class="add-to-cart-mt1"> <i class="fa fa-shopping-cart"></i><span> Boy Now</span> </button>
						</form>
					 </div>					  
                      <div class="pr-info-area">
                        <div class="pr-button">
                            
						  
						  <?php
						$loginuserid=$this->user_model->getLoginUserVar('USER_ID');
						if(!empty($loginuserid))
						{
						?>
                          <div class="mt-button add_to_wishlist"> <a href="<?php echo base_url('index.php/user/addtowishlist/'.$loginuserid.'/'.$product->id);?>"> <i class="fa fa-heart"></i> </a> </div>
						  <?php
						}else {
							?>
							<div class="mt-button add_to_wishlist"> <a href="<?php echo base_url('user/loginuser');?>"> <i class="fa fa-heart"></i> </a> </div>
						<?php 
						}
						?>
						  
						  
						  
						  
                        </div>						
                      </div>
                    </div>
                    <div class="item-info">
                      <div class="info-inner">
                        <div class="item-title"> 
							<a title="<?php echo $product->name; ?>" href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">
								<?php echo $product->name; ?>
							</a>
						</div>
                        <div class="item-content">
                          <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                          <div class="price-box">
							  <?php if($product->spacel_price!=""){ ?>
                              <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $product->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $product->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $product->price; ?> </span> </p>
							  <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <?php } } ?>
            </ul>
          </div>
		     <div class="product-list-area" id="listdata" style="display:none;">
            <ul class="products-list" id="products-list" >
              <?php if(count($productdata)>0){ ?>
			  <?php foreach($productdata as $product){ ?>
			  <?php $proimages = $this->product_model->selectProductSingleImage($product->id); ?>
              <li class="item wow fadeInUp">
                <div class=" product-img col-sm-4 col-lg-4 col-md-4">                  
				   <?php if(count($proimages)>0){ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>"><figure><img src="<?php echo base_url('uploads/product/thumbs/'.$proimages[0]->image); ?>"></figure></a>
					  <?php }else{ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>"><figure><img  src="http://placehold.it/100x100?text=Image not found" ></figure></a>
					  <?php } ?>
                </div>
                <div class="col-sm-8 col-lg-8 col-md-8">
					<div class="product-shop">
					  <h2 class="product-name">
							<a title="<?php echo $product->name; ?>" href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">
								<?php echo $product->name; ?>
							</a>
						</h2>
						<div class="ratings">
						  <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
						  <p class="rating-links"> <a href="#/">0 Review(s)</a> <span class="separator">|</span> <a href="#review-form">Add Your Review</a> </p>
						</div>
					  <div class="desc std"><?php echo $product->sortdesc; ?>
					  <a class="link-learn" title="Read More" href="<?php echo base_url('product/'.slugurl($product->name)).'/'.encodeurlval($product->id); ?>">Read More</a> 
					  </div>
					  <div class="price-box">
						<?php if($product->spacel_price!=""){ ?>
                             <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $product->spacel_price; ?> </span> </p>
                              <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $<?php echo $product->price; ?> </span> </p>
							  <?php }else{ ?>
							  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $<?php echo $product->price; ?> </span> </p>
						<?php } ?>
					  </div>
					  
						
						 <form method="post" action="<?php echo base_url('index.php/cart/addtocart'); ?>">
						   <input type="hidden" name="pid" value="<?php echo $product->id; ?>" >
						   <input type="hidden" name="qty" value="1" >
						   <div class="actions">


						  <button class="button cart-button" title="Add to Cart" type="submit" name="addtocartbutton" ><i class="fa fa-shopping-cart"></i><span>Add to Cart</span></button>
						  
						   </div>
						</form>
					</div>
				</div>
              </li>
			  <?php } } ?>
            </ul>
		  </div>
		  
		  <div class="pagination-area wow fadeInUp">
            <ul>
				<?php echo $pagination; ?>
            </ul>
          </div>
		  
		  </span>	  
        </div>
      </div>
    </div>
  </div>
  <!-- Main Container End -->   			
<?php $this->load->view('front/layout/footer');?>
<script>
	function mytab(SHOWDIV,HIDEDIV)
	{
		//$("#productdisplay .active").removeClass("active");
		//alert($(this).html());
		
		$('#'+HIDEDIV).hide();
		$('#'+SHOWDIV).show();		
	}


function addtocart(pid)
{
	jQuery.ajax({
        	url: "<?php echo base_url('index.php/cart/addtocart_ajax'); ?>",
			type: "POST",
			data:   { product_id : pid},
			//beforeSend: function(){$(this).html('OKKKKKK')},
			success: function(data)
		    {
				jQuery('#cartresponsedata').html(data);
				jQuery(".mini-cart").attr("tabindex",-1).focus(500);
			},
		  	error: function() 
	    	{
				
	    	} 	        
	   });
}
</script>