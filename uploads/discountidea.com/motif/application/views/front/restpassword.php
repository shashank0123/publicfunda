﻿<?php $this->load->view('front/layout/header');?>
  <!-- end header --> 
  
  <!-- Navbar -->
  <?php //$this->load->view('front/layout/other_page_left_menu');?>
  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>Reset Password </strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login">
        <div class="page-title">
          <h2>Reset Password</h2>
        </div>
        <div class="page-content">
          <div class="row">
		  <?php echo $this->session->flashdata('message');?>
									
		  <form method="post">
            <div class="col-sm-12">
              <div class="box-authentication">
                <h3>Reset Password</h3>
                <p>Please enter your detail .</p>
                <label for="emmail_register">Password</label>
                <input id="emmail_register" type="password" name="password" class="form-control" required>
				
				<label for="emmail_register">Confirm Password</label>
                <input id="emmail_register" type="password" name="cpassword" class="form-control" required>
				
				
                <button class="button" name="resetPassword"><i class="fa fa-user"></i>&nbsp; <span>Reset Password</span></button>
              </div>
            </div>
			</form>
			
          </div>
        </div>
      </div>
      <br>
      <br>
      <br>
      <br>
      <br>
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <section class="our-clients">
    <div class="container">
      <div class="slider-items-products">
        <div id="our-clients-slider" class="product-flexslider hidden-buttons"> 
          <!-- Begin page header-->
          <div class="page-header-wrapper">
            <div class="container">
              <div class="page-header text-center wow fadeInUp"> <h2>Our <span class="text-main">Clients</span></h2>
                <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
                <p class="lead text-gray"> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. sed diam nonummy nibh euismod tincidunt.</p>
              </div>
            </div>
          </div>
          <!-- End page header-->
          <div class="slider-items slider-width-col6"> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand1.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand2.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand3.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand4.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand5.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand6.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand7.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand3.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- home contact -->
  <section id="contact" class="gray">
    <div class="container"> 
      
      <!-- Begin page header-->
      <div class="page-header-wrapper">
        <div class="container">
          <div class="page-header text-center wow fadeInUp">
            <h2>Contact <span class="text-main">Us</span></h2>
            <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
            <p class="lead text-gray"> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. sed diam nonummy nibh euismod tincidunt.</p>
          </div>
        </div>
      </div>
      <!-- End page header-->
      
      <div class="row">
        <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-home fa-2x"></i>
          <h3>Our Address</h3>
          <span class="font-l">7064 Lorem Ipsum Vestibulum 666/13</span> </div>
        <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-comment fa-2x"></i>
          <h3>Our mail</h3>
          <span class="font-l"><a href="mailto:support@justtheme.com">support@justtheme.com</a></span> </div>
        <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-phone fa-2x"></i>
          <h3>Our phone</h3>
          <span class="font-l">+012 315 678 1234</span> </div>
      </div>
    </div>
  </section>
  <!-- Footer -->
  <?php $this->load->view('front/layout/footer');?>