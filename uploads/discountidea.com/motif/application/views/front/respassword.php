﻿<?php $this->load->view('front/layout/header');?>
		<section class="noo-page-heading eff">
				<div class="container">
					<div class="noo-heading-content">
						<h1 class="page-title eff">My Account</h1>
						<div class="noo-page-breadcrumb eff">
							<a href="index.html" class="home">Organici</a>/<span>My Account</span>
						</div>
					</div>
				</div>
			</section>
			<div class="main">
				<div class="commerce commerce-account noo-shop-main">
					<div class="container">
						<div class="row">
							<div class="noo-main col-md-12">
								<div id="customer_login">
	                				<div class="col-md-6">
	                					
										<?php
									echo $this->session->flashdata('message');?>
									
	                					<form class="login" method="post" >
	                						<div class="form-row form-row-wide">
												<label for="username">
													 Password 
													<span class="required">*</span>
												</label>
												<input type="text" class="input-text" name="password" id="username" value="" required/>
											</div>
											
											
											<div class="form-row form-row-wide">
												<label for="username">
													 Confitm Password 
													<span class="required">*</span>
												</label>
												<input type="text" class="input-text" name="cpassword" id="username" value="" required/>
											</div>
											
											<div class="form-row">
												<input type="submit" class="button" name="resetPassword" value="Change Password" />
											</div>
											
										</form>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="noo-footer-shop-now">
					<div class="container">
						<div class="col-md-7">
							<h4>- Every day fresh -</h4>
							<h3>organic food</h3>
						</div>
						<img src="<?php echo base_url('assets/front');?>/images/organici-love-me.png" class="noo-image-footer" alt="" />
					</div>
				</div>
			</div>
<?php $this->load->view('front/layout/footer');?>