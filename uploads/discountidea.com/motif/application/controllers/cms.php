<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cms extends CI_Controller 
{
	function listing()
	{		
		$data['CMSDATA']= $this->cms_model->selectAllCms();
		$this->load->view('admin/cms/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{		
						
			if($_FILES['image']['name']!="")
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/cms/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}	

			if($_FILES['image2']['name']!="")
			{
				$file2 = createUniqueFile($_FILES['image2']['name']);
				$file_temp = $_FILES['image2']['tmp_name'];
				$path = 'uploads/cms/'.$file2;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file2 = "";
			}

			if($_FILES['image3']['name']!="")
			{
				$file3 = createUniqueFile($_FILES['image3']['name']);
				$file_temp = $_FILES['image3']['tmp_name'];
				$path = 'uploads/cms/'.$file3;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file3 = "";
			}	
			
			$data['title'] = $this->input->post('title');
			$data['youtube'] = $this->input->post('youtube');
			$data['slide_text'] = $this->input->post('slide_text');
			$data['image'] = $file;
			$data['image2'] = $file2;
			$data['image3'] = $file3;
			$data['description'] = $this->input->post('description');
			$data['status'] = $this->input->post('status');	
			$data['create_date'] = time();
				
			$this->cms_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/cms/listing');			
		}
		$this->load->view('admin/cms/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{
			if($_FILES['image']['name']=="")
			{
				$file= $_POST['oldimage'];
			}
			else
			{
				$file = $_FILES['image']['name'];
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/cms/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}			
			
			if($_FILES['image2']['name']!="")
			{
				$file2 = $_FILES['image2']['name'];
				$file_temp = $_FILES['image2']['tmp_name'];
				$path = 'uploads/cms/'.$file2;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file2 = $_POST['oldimage2'];
			}

			if($_FILES['image3']['name']!="")
			{
				$file3 = $_FILES['image3']['name'];
				$file_temp = $_FILES['image3']['tmp_name'];
				$path = 'uploads/cms/'.$file3;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file3 = $_POST['oldimage3'];
			}
			
			
			
			$data['title'] = $this->input->post('title');
			$data['youtube'] = $this->input->post('youtube');
			$data['slide_text'] = $this->input->post('slide_text');
			$data['image'] = $file;
			$data['image2'] = $file2;
			$data['image3'] = $file3;
			$data['description'] = $this->input->post('description');
			$data['status'] = $this->input->post('status');
			$this->cms_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/cms/listing');
		}
		$data['EDITCMS']= $this->cms_model->selectCmsById($args[0]);
		$this->load->view('admin/cms/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();
		$data= $this->cms_model->selectCmsById($args[0]);
		$path ='media/uploads/cms/'.$data[0]->image;
		//@unlink($path);
		$this->cms_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/cms/listing');
	}	

}