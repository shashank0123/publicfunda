<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Homepagepopup extends CI_Controller 
{
	function listing()
	{		
		$data['popupdata']= $this->homepopup_model->selectallpopup();
		$this->load->view('admin/homepopup/list',$data);
	}
	
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{	
			
			if($_FILES['image']['name']!="")
			{
				$file = $_FILES['image']['name'];
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/cms/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = $_POST['oldimage'];
			}
			$data['title']=$this->input->post('title');
			$data['image'] = $file;
			$data['content'] = $this->input->post('slide_text');
			$this->homepopup_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/homepagepopup/listing');
		}
		$data['edithomepopup']= $this->homepopup_model->selecthomepopubyid($args[0]);
		$this->load->view('admin/homepopup/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();
		$data= $this->cms_model->selectCmsById($args[0]);
		$path ='media/uploads/cms/'.$data[0]->image;
		//@unlink($path);
		$this->cms_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/cms/listing');
	}	

}