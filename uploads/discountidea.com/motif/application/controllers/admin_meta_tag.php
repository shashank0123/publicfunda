<?php
class Admin_Meta_Tag extends CI_controller
{
	
		function index()
		{
			
			$data['listingmeta']=$this->meta_tag_model->selectallmetatag();
			$this->load->view('admin/meta/listing',$data);
		}
	
	function addmeta()
	{
		if(isset($_POST['addmeta']))
		{
			$data['url']=$this->input->post('url');
			$data['meta_title']=$this->input->post('title');
			$data['meta_keyword']=$this->input->post('keyword');
			$data['meta_description']=$this->input->post('description');
			$this->meta_tag_model->insertdata($data);
			redirect('index.php/admin_meta_tag');
			
		}
		
		$this->load->view('admin/meta/add');
		
	}
	function editmeta()
	{
		$args=func_get_args();
		
		if(isset($_POST['edit']))
		{
			$data['url']=$this->input->post('url');
			$data['meta_title']=$this->input->post('title');
			$data['meta_keyword']=$this->input->post('keyword');
			$data['meta_description']=$this->input->post('description');
			$this->meta_tag_model->updatemetabyid($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Meta Tag Update successsully</div>');
			redirect('index.php/admin_meta_tag');
			
			
		}
		
		
		$data['editmetabyid']=$this->meta_tag_model->selectmetabyid($args[0]);
		$this->load->view('admin/meta/edit',$data);
		
	}
	function deletemeta($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tbl_metatag');
		
		$this->session->set_flashdata('message','<div class="alert alert-success">Meta Tag Delete successsully</div>');
			redirect('index.php/admin_meta_tag');
		
		
	}
	
}

?>