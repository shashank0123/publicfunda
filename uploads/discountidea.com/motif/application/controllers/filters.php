<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Filters extends CI_Controller 
{
	public function __construct()
    {
        // call Grandpa's constructor		
        parent::__construct();
    }
	function listing()
	{		
		$this->load->model('filters_model');
		$data['FILTERDATA']= $this->filters_model->selectAllFilters();
		$this->load->view('admin/filters/list',$data);
	}
	function add()
	{	
		$this->load->model('filters_model');
		$args = func_get_args();
		if(isset($_POST['savedata']))
		{						
			$data['p_id'] = $args[0]; 
			$data['title'] = $this->input->post('title');
			$data['type'] = ($args[0]==0)?'filter':'option';	
			
			$this->filters_model->insert_filters($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/filters/listing');			
		}
		$this->load->view('admin/filters/add');
	}
	function edit()
	{	
		$this->load->model('filters_model');
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{			
			$data['title'] = $this->input->post('title');
			$this->filters_model->update_filters($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/filters/listing');
		}
		$data['EDITfilters']= $this->filters_model->selectFiltersById($args[0]);
		$this->load->view('admin/filters/edit',$data);
	}
	
	function deletefilter()
	{
		$this->load->model('filters_model');
		$args=func_get_args();		
		$this->filters_model->delete_filters($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/filters/listing');
	}
		
}