<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	
	public function index()
	{
		$this->load->model('admin_model');
		
		if(isset($_POST['loginbutton']))
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');			
			
			if(!empty($username) && !empty($password))
			{
				$admin_data = $this->admin_model->adminLogin($username,$password);
				
				if(count($admin_data)>0)
				{
					$login_data = array(
						'admin_id'=>$admin_data[0]->id,
						'admin_username'=>$admin_data[0]->username,
						'admin_type' =>'0',
						'logged_in'=>true);						
					$this->session->set_userdata($login_data);
					redirect('index.php/admin/dashboard');
				}
				else
				{
					$this->session->set_flashdata('message','<div class="isa_error">Invalid username or password !</div>');
					redirect('index.php/admin');
				}	
			}
			else
			{
				$this->session->set_flashdata('message','<div class="isa_error">Invalid username or password !</div>');
				redirect('index.php/admin');
			}					
		}	
		$this->load->view('admin/login');
	}
	
	public function settings()
	{
		$this->load->model('admin_model');
		$admin_id = $this->session->userdata('admin_id'); 
		if(isset($_POST['updatedata']))
		{
			if($_FILES['image']['name']=="")
			{
				$file= $_POST['oldimage'];
			}
			else
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/image/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}

            $password = $this->input->post('pwd');
			$cpassword = $this->input->post('cpwd');
			if(!empty($password) && !empty($cpassword))
			{
				if($password==$cpassword)
				{
				  $data['password'] = $password;
				}	
			}	
				
			$data['logo'] = $file;
			$this->admin_model->updateAdminById($admin_id,$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/admin/settings');
		}
		
		$data['admindata']= $this->admin_model->checkAdminByID($admin_id);
		$this->load->view('admin/settings',$data);
	}
	
	public function forgot_password()
	{
		$this->load->model('admin_model');
		if(isset($_POST['submitbutton']))
		{
			$email = $this->input->post('email');
			if(!empty($email))
			{
				$admin_data = $this->admin_model->checkAdminByEmail($email);
				if(count($admin_data)>0)
				{
					$token = md5($admin_data[0]->email.time().$admin_data[0]->id);
					$data['forgot_password'] = $token;
					$this->admin_model->updateAdminById($admin_data[0]->id,$data);
					
					$this->email->set_newline("\r\n");
					$this->email->set_mailtype("html");
					$this->email->to($email);
					$this->email->from(ADMIN_EMAIL, 'Samridh Bharat Forgot Password');
					$this->email->subject('Samridh Bharat Forgot Password');
					
					$reset_password_link = base_url('index.php/admin/resetpassword/'.$token);
					
					$button_reset = "<div style='margin:0 auto;margin-top: 20px;  margin-bottom: 20px;width:180px;line-height:35px;text-align:center;background:#ff5c01;text-transform:uppercase;border-radius:5px'>
    <a href='".$reset_password_link."' style='width:100%;text-decoration:none;color:white;'> reset password</a>
</div>";
					// message start
					$message = "<div style=' max-width: 70%; padding:10px; border: 2px solid #948c8c; margin: 0 auto; text-align: center; margin-top:12%'>";	
					$message .= "<h2>Dear Admin</h2>,<br>";
					$message .= "Want to change your password? Please click on the link given below to reset the password of your Samridh Bharat.<br>";
					$message .= $button_reset;
					$message .= "If you are not able to click on the above link, please copy and paste the entire URL into your browser's address bar and press Enter.";
					$message .= '<br>'.$reset_password_link;					
					$message .= "<br><br>";
					$message .= "Thank you <br> Samridh Bharat support team</div>";
					// message end
					
					//echo $message; die();
					
					$this->email->message($message);
					$this->email->send();
					
					$this->session->set_flashdata('message','<div class="isa_success">password change instructions send on your email address</div>');
					redirect('index.php/admin/forgot_password');
					
				}
				else
				{
					$this->session->set_flashdata('message','<div class="isa_error">Invalid email address!</div>');
					redirect('index.php/admin/forgot_password');
				}	
			}
			else
			{
				$this->session->set_flashdata('message','<div class="isa_error">Please enter email address!</div>');
				redirect('index.php/admin/forgot_password');
			}	
		}	
		
		$this->load->view('admin/forgot-password');
	}
	
	public function resetpassword()
	{
		$this->load->model('admin_model');
		$args = func_get_args();
		if(count($args)>0)
		{
			$admin_data = $this->admin_model->checkAdminByToken($args[0]);
			if(count($admin_data)>0)
			{
				
			}
			else
			{
				$this->session->set_flashdata('message','<div class="isa_error">Invalid token!</div>');
				redirect('index.php/admin/index');
			}		
		}
		else
		{
			$this->session->set_flashdata('message','<div class="isa_error">Invalid token!</div>');
			redirect('index.php/admin/index');
		}	
		$this->load->view('admin/reset-password');
	}
	
	public function dashboard()
	{
		$this->load->view('admin/dashboard');
	}
	
	public function adduser()
	{
		if(isset($_POST['saveuser']))
		{
			
			$email= $this->input->post('email');
			$user_data=$this->user_model->selectuserby_email($email);
			$checkfile= count($user_data);
			if($checkfile >0)
			{
				//echo "sdfdsfds";die;
				
				$this->session->set_flashdata("message","<br><div class='alert alert-danger'><strong>".$email."</strong> Email address already exists choose a different one.</div>");
				redirect('user/create_account');
			}
			$token=md5(time().$email);
			$data['activate_token']=$token;
			$data['fname'] = $this->input->post('username');
			if($_FILES['image']['name']!="")
			{
				$file = $_FILES['image']['name'];
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/banner/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}			
					
			$data['image'] = $file; 
			$data['email'] = $email;
			$data['gender'] = $this->input->post('gender');
		    $data['mobile'] = $this->input->post('contact_no');
			$data['address'] = $this->input->post('address');
			$data['city'] = $this->input->post('city');
			$data['password']=$this->input->post('password');
			$data['state'] = $this->input->post('state');
			$data['vender_type']=$this->input->post('usertype');
			$data['status'] = $this->input->post('status');
	
			$insertedData = $this->admin_model->insertData($data);
			redirect('index.php/admin/userList');
			
		}	
		$this->load->view('admin/user/add-user');
	}	
	
	
	public function edituser()
	{
		$args = func_get_args();
		if(isset($_POST['saveuser']))
		{
			$email = $this->input->post('email');
			$data['fname'] = $this->input->post('username');
			if($_FILES['image']['name']=="")
			{
				$image= $_POST['oldimage'];
			}
			else
			{
				$file_allow=array('jpg','png','gif','jpeg');
				$path = 'uploads/banner/';
				$image= $this->upload('image',$path,$file_allow);
			}
			$data['image'] = $image;
			$data['email'] = $this->input->post('email');
			$data['password']=$this->input->post('password');
			$data['gender'] = $this->input->post('gender');
		    $data['mobile'] = $this->input->post('contact_no');
			$data['address'] = $this->input->post('address');
			$data['city'] = $this->input->post('city');
			$data['vender_type']=$this->input->post('usertype');
			$data['state'] = $this->input->post('state');
			$data['status'] = $this->input->post('status');
			
			$insertedData = $this->admin_model->updateAdminById($data,$args[0]);
			redirect('index.php/admin/userlist');
				
		}	
		
		$data['USERDATA'] = $this->admin_model->selectUserByID($args[0]);
		$this->load->view('admin/user/edit-user',$data);
	}
	
	
	function userList()
	{
		
		$data['USERLIST'] = $this->admin_model->getAllUsersvender();
		$this->load->view('admin/user/list',$data);
	}
	
	
	public function viewuser()
	{	
		$args = func_get_args();
		$data['USERDATA'] = $this->admin_model->selectUserByID($args[0]);
		$this->load->view('admin/user/view-user',$data);
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		$this->session->set_userdata(array('admin_id' => '', 'admin_username' => '','admin_type'=>'', 'logged_in' => ''));
		$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully lougout.</div>');
		redirect('index.php/admin');	
	}
}

