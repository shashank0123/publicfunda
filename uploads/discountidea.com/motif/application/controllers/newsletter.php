<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Newsletter extends CI_Controller 
{
	function listing()
	{		
		$data['NEWSLETTERDATA']= $this->newsletter_model->selectAllNewsletter();
		$this->load->view('admin/newsletter/list',$data);
	}
	function savenewsletter()
	{		
		if(isset($_POST['saveNewsLetter']))
		{				
			//echo "sdfdsfdsfds";die;
			$data['email'] = $this->input->post('email');
			
			$this->newsletter_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/home#newsletterdiv');			
		}
		else
		{
			redirect('index.php/home');	
		}
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{			
			$data['email'] = $this->input->post('email');
			/*$data['status'] = $this->input->post('status'); */
			$this->newsletter_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/newsletter/listing');
		}
		$data['EDITNEWSLETTER']= $this->newsletter_model->selectNewsletterById($args[0]);
		$this->load->view('admin/newsletter/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();		
		$this->newsletter_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/newsletter/listing');
	}
	
}