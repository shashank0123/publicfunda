<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class banner extends CI_Controller 
{
	function listing()
	{		
		$data['BANNERDATA']= $this->banner_model->selectAllBanner();
		$this->load->view('admin/banner/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{		
						
			if($_FILES['image']['name']!="")
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/banner/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}			
			
			
			$data['url']= $_POST['url'];			
			
			$data['title'] = $this->input->post('title');
			$data['image'] = $file; 
			$data['status'] = $this->input->post('status');	
			$data['create_date'] = time();
				
			$this->banner_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/banner/listing');			
		}
		$this->load->view('admin/banner/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{
			if($_FILES['image']['name']=="")
			{
				$file= $_POST['oldimage'];
			}
			else
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/banner/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}
			
			$data['url']= $_POST['url'];			
						
			$data['title'] = $this->input->post('title');
			$data['image'] = $file;
			$data['status'] = $this->input->post('status');
			$this->banner_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/banner/listing');
		}
		$data['EDITBANNER']= $this->banner_model->selectbannerbyid($args[0]);
		$this->load->view('admin/banner/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();
		//$data= $this->banners->selectbannerbyid($args[0]);
		//$path ='media/uploads/banner/'.$data[0]->image;
		//@unlink($path);
		$this->banner_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/banner/listing');
	}
	
	function chech_admin_login()
	{
		$ci = & get_instance();
		$USERID      = $ci->session->userdata('USERID');	
		$USERNAME      = $ci->session->userdata('USERNAME');	
		$logged_in      = $ci->session->userdata('logged_in');	
		if($USERID=="" && $USERNAME=="")
		{
			redirect('index.php/admin/index');
		}
	}
}