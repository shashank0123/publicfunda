-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 05, 2017 at 01:43 PM
-- Server version: 5.6.35-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `discountida`
--

-- --------------------------------------------------------

--
-- Table structure for table `biiling_deail`
--

CREATE TABLE IF NOT EXISTS `biiling_deail` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) NOT NULL,
  `ord_id` varchar(255) NOT NULL,
  `user_id` varchar(244) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(245) NOT NULL,
  `address` text NOT NULL,
  `secondaddress` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `add_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `biiling_deail`
--

INSERT INTO `biiling_deail` (`id`, `country`, `ord_id`, `user_id`, `firstname`, `lastname`, `address`, `secondaddress`, `city`, `state`, `pincode`, `email`, `mobile`, `add_date`) VALUES
(1, 'India', '3', '34', 'Naseem', 'ahmad', 'delhi', '', 'delhi', 'delhi', '110009', 'naseem.infotrench@gmail.com', '09899004500', '2017-04-26 10:43:11'),
(2, 'India', '4', '34', 'Naseem', 'ahmad', 'delhi', '', 'delhi', 'delhi', '110009', 'naseem.infotrench@gmail.com', '09899004500', '2017-04-26 10:45:08'),
(3, 'india', '5', '34', 'rahul', 'verma', 'wegfk', '', 'delhi', 'delhi', '110092', 'rahulsingh41787@gmail.com', '9999502240', '2017-04-26 10:49:37'),
(4, 'india', '6', '34', 'rahul', 'VERMA', 'asfd', '', 'asd', 'delhi', '1123', 'rahulsingh41787@gmail.com', '9999502240', '2017-04-26 11:01:00'),
(5, 'india', '7', '34', 'rahul', 'verma', 'wfasf', '', 'afscas', 'delhi', '123123', 'rahulsingh41787@gmail.com', '9999502240', '2017-04-26 11:02:22'),
(6, 'India', '8', '38', 'Abhishek', 'kumar', 'G-123, Basement, Sector 63', '', 'Noida', 'Uttar Pradesh', '201301', 'abhishek@gmail.com', '09711480065', '2017-04-26 11:59:01'),
(7, 'India', '9', '38', 'Abhishek', 'kumar', 'G-123, Basement, Sector 63', '', 'Noida', 'Uttar Pradesh', '201301', 'abhishek@gmail.com', '09711480065', '2017-04-26 11:59:46'),
(8, 'India', '10', '36', 'Abhishek', 'kumar', 'G-123, Basement, Sector 63', '', 'Noida', 'Uttar Pradesh', '201301', 'abhishek.infotrench@@gmail.com', '09711480065', '2017-04-26 12:57:03'),
(9, 'bsd', '11', '36', 'Abhishek', 'kkunb', 'bfg', '', 'bfg', 'sb', '52', 'abhishek.infotrench@gmail.com', '9015965829', '2017-04-26 13:57:15'),
(10, 'bsd', '12', '36', 'Abhishek', 'kkunb', 'bfg', '', 'bfg', 'sb', '52', 'abhishek.infotrench@gmail.com', '9015965829', '2017-04-26 13:57:21'),
(11, 'n.', '13', '35', 'j,ln', 'n,.mn', 'nm.', '', '.n', '.m', '63453', 'ajay@gmail.com', '4354', '2017-04-26 14:01:05'),
(12, 'india', '14', '34', 'rahul', 'verma', 'af', '', 'delhi', 'delhi', '110092', 'rahulsingh41787@gmail.com', '9999502240', '2017-04-26 14:06:22');

-- --------------------------------------------------------

--
-- Table structure for table `our_clint`
--

CREATE TABLE IF NOT EXISTS `our_clint` (
  `id` int(90) NOT NULL,
  `image` text NOT NULL,
  `status` varchar(233) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `our_clint`
--

INSERT INTO `our_clint` (`id`, `image`, `status`) VALUES
(5, '1491204277_brand1.png', ''),
(6, '1491204278_brand2.png', ''),
(7, '1491204278_brand3.png', ''),
(8, '1491204278_brand4.png', ''),
(9, '1491204278_brand5.png', ''),
(10, '1491204278_brand6.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `shiping_detail`
--

CREATE TABLE IF NOT EXISTS `shiping_detail` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `ord_id` varchar(25) NOT NULL,
  `user_id` varchar(244) NOT NULL,
  `country` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `secondaddress` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `email` varchar(234) NOT NULL,
  `state` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `add_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `shiping_detail`
--

INSERT INTO `shiping_detail` (`id`, `ord_id`, `user_id`, `country`, `firstname`, `lastname`, `address`, `secondaddress`, `city`, `email`, `state`, `pincode`, `add_date`) VALUES
(1, '3', '34', 'India', 'Naseem', 'ahmad', 'delhi', '', 'delhi', 'naseem.infotrench@gmail.com', 'delhi', '110009', '2017-04-26 10:43:11'),
(2, '4', '34', 'India', 'Naseem', 'ahmad', 'delhi', '', 'delhi', 'naseem.infotrench@gmail.com', 'delhi', '110009', '2017-04-26 10:45:08'),
(3, '5', '34', 'india', 'rahul', 'verma', 'wegfk', '', 'delhi', 'rahulsingh41787@gmail.com', 'delhi', '110092', '2017-04-26 10:49:37'),
(4, '6', '34', 'india', 'rahul ', 'VERMA', 'asfd', '', 'asd', 'rahulsingh41787@gmail.com', 'delhi', '1123', '2017-04-26 11:01:00'),
(5, '7', '34', 'india', 'rahul', 'verma', 'wfasf', '', 'afscas', 'rahulsingh41787@gmail.com', 'delhi', '123123', '2017-04-26 11:02:22'),
(6, '8', '38', 'India', 'Abhishek', 'kumar', 'G-123, Basement, Sector 63', '', 'Noida', 'abhishek@gmail.com', 'Uttar Pradesh', '201301', '2017-04-26 11:59:01'),
(7, '9', '38', 'India', 'Abhishek', 'kumar', 'G-123, Basement, Sector 63', '', 'Noida', 'abhishek@gmail.com', 'Uttar Pradesh', '201301', '2017-04-26 11:59:46'),
(8, '10', '36', 'India', 'Abhishek', 'kumar', 'G-123, Basement, Sector 63', '', 'Noida', 'abhishek.infotrench@@gmail.com', 'Uttar Pradesh', '201301', '2017-04-26 12:57:03'),
(9, '11', '36', 'bsd', 'Abhishek', 'kkunb', 'bfg', '', 'bfg', 'abhishek.infotrench@gmail.com', 'sb', '52', '2017-04-26 13:57:15'),
(10, '12', '36', 'bsd', 'Abhishek', 'kkunb', 'bfg', '', 'bfg', 'abhishek.infotrench@gmail.com', 'sb', '52', '2017-04-26 13:57:21'),
(11, '13', '35', 'n.', 'j,ln', 'n,.mn', 'nm.', '', '.n', 'ajay@gmail.com', '.m', '63453', '2017-04-26 14:01:05'),
(12, '14', '34', 'india', 'rahul', 'verma', 'af', '', 'delhi', 'rahulsingh41787@gmail.com', 'delhi', '110092', '2017-04-26 14:06:22');

-- --------------------------------------------------------

--
-- Table structure for table `table_user`
--

CREATE TABLE IF NOT EXISTS `table_user` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `activate_token` text NOT NULL,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(200) NOT NULL,
  `vender_type` enum('0','1') NOT NULL DEFAULT '0',
  `image` text NOT NULL,
  `pincode` varchar(233) NOT NULL,
  `state` varchar(200) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `table_user`
--

INSERT INTO `table_user` (`id`, `email`, `password`, `activate_token`, `fname`, `lname`, `mobile`, `gender`, `address`, `city`, `vender_type`, `image`, `pincode`, `state`, `status`) VALUES
(32, 'mukesh.infotrench@gmail.com', '123456', 'c3c4a7f7c5b090c45ec0439562265ead', 'mukesh bisht', '', 'bvbcvbcv', 'M', 'laxmi nagar new delhi east', 'new delhi', '0', '', '110052', 'east delhio', ''),
(33, 'adityaverma8444@gmail.com', '1234', '2c05e8115b1b4ad90f1523f83ea131ea', '', '', '', '', '', '', '0', '', '', '', '1'),
(34, 'rahulsingh41787@gmail.com', '1234', 'bf9cc3efaebec4b95433436b7fd503ce', 'rahul', '', '9999502240', 'Male', '', '', '1', '', '', 'delhi', '1'),
(35, 'ajay@gmail.com', '123', '4a447b54ad1ff104fbe79c1835c28073', '', '', '', '', '', '', '0', '', '', '', '1'),
(36, 'abhishek.infotrench@gmail.com', 'info@0123', '834fd72a0065efaebd76f60791ef9ebb', 'Abhishek', '', '9015965829', '', '', '', '0', '', '', '', '1'),
(37, 'mukesh123@gmail.com', '123', '5835bf159e8118fb3b3ab70f8562e96e', 'Mukesh', '', '9845461254', 'Male', 'laxmi nagar new delhi east', 'new delhi', '1', '', '', 'east delhi', '1'),
(38, 'abhishek@gmail.com', 'info@0123', '8a4def8038f3eb7958960271ab429121', 'Abhishek', '', '9015965829', 'Male', 'G-123, Basement, Sector 63', 'Noida', '1', 'IMG_0820.JPG', '', 'Uttar Pradesh', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `logo` varchar(250) NOT NULL,
  `image` varchar(255) NOT NULL,
  `forgot_password` varchar(250) NOT NULL,
  `last_login` varchar(255) NOT NULL,
  `forget_key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `username`, `password`, `email`, `gender`, `contact_no`, `address`, `city`, `state`, `status`, `logo`, `image`, `forgot_password`, `last_login`, `forget_key`) VALUES
(1, '', 'admin', 'admin', 'admin@gmail.com', '', '', '', '', '', 0, '', '', '', '', ''),
(2, 'homes_connect', '', '', 'ajays2612@gmail.com', 'male', '9899004500', 'Mamura Sector-66, Noida', 'Noida', 'Uttar Pradesh', 0, '', 'about-us1.jpg', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner`
--

CREATE TABLE IF NOT EXISTS `tbl_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `url` text NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `create_date` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_banner`
--

INSERT INTO `tbl_banner` (`id`, `title`, `image`, `content`, `url`, `status`, `create_date`) VALUES
(11, 'BIG BASS', '1493382885_slide-02.jpg', '', '#', '1', '1491211248'),
(13, 'Banner 3', '1493382893_slide-01.jpg', '', '#', '0', '1492242044');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_block`
--

CREATE TABLE IF NOT EXISTS `tbl_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `create_date` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_block`
--

INSERT INTO `tbl_block` (`id`, `title`, `image`, `description`, `status`, `create_date`) VALUES
(1, 'a', '', '', '0', '1481962669'),
(2, 'b', '', '', '0', '1481962699'),
(3, 'c', '', '', '0', '1481962715'),
(4, 'Copy right text', '', '', '0', '1481962765'),
(5, 'NEWSLETTER', '', '', '0', '1481962846'),
(6, 'contact us map', '', '', '0', '1481962954'),
(7, 'Header left contact', '', '', '0', '1481977653'),
(8, 'Header right contact no', '', '', '0', '1481978264'),
(9, 'Header right email', '', '', '0', '1481978296');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(20) NOT NULL,
  `display_on_home` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `filters_id` varchar(250) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `createdate` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `meta_title` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `meta_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `parent_id`, `display_on_home`, `name`, `title`, `image`, `filters_id`, `icon`, `createdate`, `status`, `meta_title`, `meta_keyword`, `meta_description`) VALUES
(5, '0', 0, '', 'Men', '', '', '', '1490782178', '1', '', '', ''),
(6, '0', 0, '', 'Women', '', '', '', '1490781730', '1', '', '', ''),
(7, '0', 0, '', 'Kids', '', '', '', '1490781748', '1', '', '', ''),
(8, '5', 1, '', 'Clothing', '', '', '', '1492250863', '1', '', '', ''),
(9, '8', 0, '', 'Coats & Jackets ', '', '1,7,12,15', '', '1492256933', '1', '', '', ''),
(10, '8', 0, '', 'Raincoats', '', '', '', '1490857536', '1', '', '', ''),
(11, '8', 0, '', 'Blazers', '', '', '', '1490857560', '1', '', '', ''),
(12, '8', 0, '', 'Jackets', '', '', '', '1490857581', '1', '', '', ''),
(13, '5', 0, '', 'Accessories', '', '', '', '1490857616', '1', '', '', ''),
(14, '13', 0, '', 'Wallets', '', '', '', '1490857638', '1', '', '', ''),
(15, '13', 0, '', 'Laptop Bags', '', '', '', '1490857652', '1', '', '', ''),
(16, '13', 0, '', 'Belts', '', '', '', '1490857670', '1', '', '', ''),
(17, '13', 0, '', 'Bakpacks', '', '', '', '1490857697', '1', '', '', ''),
(18, '6', 0, '', 'Dresses', '', '', '', '1490857734', '1', '', '', ''),
(19, '18', 1, '', 'Accessories', '', '', '', '1492427057', '1', '', '', ''),
(20, '18', 0, '', 'Hats and Gloves', '', '', '', '1490857775', '1', '', '', ''),
(21, '18', 0, '', 'Lifestyle', '', '', '', '1490857788', '1', '', '', ''),
(22, '18', 0, '', 'Bras', '', '', '', '1490857804', '1', '', '', ''),
(23, '6', 0, '', 'Jwellery', '', '', '', '1490857831', '1', '', '', ''),
(24, '23', 0, '', 'Bracelets', '', '', '', '1490857880', '1', '', '', ''),
(25, '23', 0, '', 'Necklaces & Pendent', '', '', '', '1490857870', '1', '', '', ''),
(26, '23', 0, '', 'Pendants', '', '', '', '1490857920', '1', '', '', ''),
(27, '23', 0, '', 'Pins & Brooches', '', '', '', '1490857932', '1', '', '', ''),
(28, '6', 0, '', 'Shoes', '', '', '', '1490858050', '1', '', '', ''),
(29, '28', 0, '', 'Flat Shoes', '', '', '', '1490858069', '1', '', '', ''),
(30, '28', 0, '', 'Flat Sandals', '', '', '', '1490858093', '1', '', '', ''),
(31, '28', 0, '', 'Boots', '', '', '', '1490858124', '1', '', '', ''),
(32, '28', 0, '', 'Heels', '', '1,7', '', '1492492723', '1', '', '', ''),
(33, '7', 0, '', 'Cloths', '', '', '', '1492422953', '1', '', '', ''),
(34, '33', 1, '', 'Coats & Jackets', '', '', '', '1492493153', '1', '', '', ''),
(35, '33', 0, '', 'Raincoats', '', '', '', '1490860169', '1', '', '', ''),
(36, '7', 0, '', 'Accessories', '', '', '', '1490860197', '1', '', '', ''),
(37, '36', 0, '', 'Wallets', '', '', '', '1490860245', '1', '', '', ''),
(38, '36', 0, '', 'Laptop Bags', '', '1,7,12', '', '1492493064', '1', '', '', ''),
(39, '8', 1, '', 'Jeans', '', '1,7,12,15', '', '1492493128', '1', 'Jeans', 'Jeans pants', 'This is pants and jeans'),
(40, '8', 0, '', 'T-shirts', '', '1,7,12', '', '1492411564', '1', 'T-shirts for Men', 'T-shirt shirt kurta ', 'Kurta shirt tshirt '),
(41, '8', 0, '', 'Shirt', '', '1,7,12', '', '1492412737', '1', 'shirt for all mens', 'mens shirt here', 'all mens shirt to purchase'),
(42, '8', 0, '', 'Mens Shoes', '', '7,12', '', '1492417799', '1', 'shoes all men', 'juta shoes ', 'juta soes '),
(43, '8', 0, '', 'Watchs', '', '', '', '1492415266', '1', 'watch and ghdi purchase', 'watch and ghdi purchase', 'watch and ghdi purchase'),
(44, '18', 0, '', 'jean for Women', '', '1,7,12', '', '1492420335', '1', 'Jeans for Womens', 'Jeans for Womens', 'Jeans for Womens'),
(45, '18', 0, '', 'T-shirts For Women', '', '1,7,12', '', '1492420870', '1', 'T-shirt for Women', 'T-shirt for Women', 'T-shirt for Women'),
(46, '18', 0, '', 'Shirt For Women', '', '1,7,12', '', '1492421651', '1', 'Shirt For Men', 'Shirt For Men', 'Shirt For Men'),
(47, '18', 0, '', 'sandals', '', '1,7,12', '', '1492422057', '1', 'sandals', 'sandals', 'sandals'),
(48, '18', 0, '', 'Watchs For Women', '', '1', '', '1492422509', '1', 'watch for women', 'watch for women', 'watch for women'),
(49, '33', 0, '', 'Jeans For Kids', '', '1,7,12', '', '1492422998', '1', 'Jeans kides', 'Jeans kides', 'Jeans kides'),
(50, '33', 0, '', 'Kids T-shirt', '', '1,7', '', '1492423918', '1', 'shirt for kids', 'shirt for kids', 'shirt for kids'),
(51, '33', 0, '', 'Kids Shirts', '', '1,7', '', '1492424327', '1', 'kids shirt', 'kids shirt', 'kids shirt'),
(52, '33', 0, '', 'Kids Shoes', '', '1,7', '', '1492425053', '1', 'Shoes for Kids', 'Shoes for Kids', 'Shoes for Kids'),
(53, '0', 0, '', 'Furniture', '', '1,7', '', '1493383706', '1', 'Home & Furniture', 'Home & Furniture', 'Home & Furniture'),
(54, '0', 0, '', 'Books', '', '7', '', '1493383589', '1', 'Books & More', 'Books & More', 'Books & More'),
(55, '0', 0, '', 'Electronics', '', '', '', '1492425648', '1', 'Electronics', 'Electronics', 'Electronics'),
(56, '0', 0, '', 'Appliances', '', '', '', '1492425662', '1', 'Appliances', 'Appliances', 'Appliances'),
(57, '0', 0, '', 'Daily Needs', '', '', '', '1492425673', '1', 'Daily Needs', 'Daily Needs', 'Daily Needs'),
(58, '0', 1, '', 'Accessories', '', '1,7,12,15', '', '1493383422', '1', 'Accessories', 'fsdf', 'sdfsd'),
(59, '0', 0, '', 'Sports', '', '1,7,12,15', '', '1493383526', '1', 'Sports', 'Sports', 'Sports');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_image`
--

CREATE TABLE IF NOT EXISTS `tbl_category_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `tbl_category_image`
--

INSERT INTO `tbl_category_image` (`id`, `category_id`, `image`) VALUES
(3, 5, '1490793191_4.3.png'),
(4, 5, '1490793191_2.png'),
(5, 8, '1492078642_slider5.jpg'),
(6, 13, '1492078753_slider-h9a.jpg'),
(9, 19, '1492427027_1492242044_c.jpg'),
(10, 19, '1492427027_1492242044_c.jpg'),
(11, 20, '1492427111_1492242044_c.jpg'),
(12, 20, '1492427111_1492242044_c.jpg'),
(13, 44, '1492427169_1492242044_c.jpg'),
(14, 44, '1492427169_1492242044_c.jpg'),
(15, 45, '1492427195_1492242044_c.jpg'),
(16, 45, '1492427195_1492242044_c.jpg'),
(17, 47, '1492427218_1492242044_c.jpg'),
(18, 47, '1492427218_1492242044_c.jpg'),
(19, 46, '1492427268_1492242044_c.jpg'),
(20, 46, '1492427268_1492242044_c.jpg'),
(21, 48, '1492427308_1492242044_c.jpg'),
(22, 48, '1492427308_1492242044_c.jpg'),
(23, 9, '1492490531_2.jpg'),
(24, 10, '1492490562_2.jpg'),
(25, 11, '1492490599_3.jpg'),
(26, 12, '1492490629_3.jpg'),
(27, 39, '1492490700_2.jpg'),
(28, 39, '1492490700_3.jpg'),
(29, 40, '1492490765_2.jpg'),
(30, 41, '1492490786_3.jpg'),
(31, 42, '1492490803_3.jpg'),
(32, 43, '1492490820_3.jpg'),
(33, 14, '1492490914_2.jpg'),
(34, 15, '1492490937_3.jpg'),
(35, 17, '1492491233_3.jpg'),
(36, 16, '1492491263_3.jpg'),
(37, 16, '1492491263_2.jpg'),
(38, 21, '1492491466_1.jpg'),
(39, 21, '1492491466_0.jpg'),
(40, 22, '1492491495_0.jpg'),
(41, 24, '1492492438_0.jpg'),
(42, 25, '1492492467_1.jpg'),
(43, 26, '1492492522_1.jpg'),
(44, 27, '1492492551_0.jpg'),
(45, 29, '1492492583_1.jpg'),
(46, 30, '1492492607_1.jpg'),
(47, 31, '1492492634_0.jpg'),
(48, 32, '1492492659_1.jpg'),
(49, 34, '1492492784_4.jpg'),
(50, 35, '1492492849_4.jpg'),
(51, 35, '1492492849_5.jpg'),
(52, 49, '1492492887_4.jpg'),
(53, 50, '1492492957_5.jpg'),
(54, 51, '1492492976_4.jpg'),
(55, 52, '1492492998_5.jpg'),
(56, 52, '1492492998_4.jpg'),
(57, 37, '1492493021_4.jpg'),
(58, 38, '1492493040_5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms`
--

CREATE TABLE IF NOT EXISTS `tbl_cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `youtube` varchar(250) NOT NULL,
  `slide_text` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `image2` varchar(100) NOT NULL,
  `image3` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `create_date` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_cms`
--

INSERT INTO `tbl_cms` (`id`, `title`, `youtube`, `slide_text`, `image`, `image2`, `image3`, `description`, `status`, `create_date`) VALUES
(5, 'Privacy Policy', '0', '<div class="col-xs-12">\r\n<div class="page-title">\r\n<h2>Privacy Policy</h2>\r\n<p>Aliquam posuere erat et orci eleifend cursus. Nullam tempus odio sem, lacinia pellentesque neque mollis a. In ut tempor ligula. Vestibulum ultricies bibendum lorem, a sollicitudin tellus euismod vel. Nam suscipit, diam ut volutpat lobortis, nibh ipsum tempor nibh, a vehicula tellus justo id nibh. Nulla pretium mollis convallis. Phasellus a nibh aliquet, ullamcorper quam aliquam, faucibus nulla. Phasellus mollis tristique vehicula. Vivamus sagittis, sem sed posuere aliquet, massa odio lobortis.</p>\r\n</div>\r\n</div>\r\n<div class="col-xs-12 col-sm-9">\r\n<div class="panel-group">\r\n<div class="panel-body">\r\n<h4>Frequently Asked</h4>\r\n<p>Vestibulum justo. Nulla mauris ipsum, convallis ut, vestibulum eu, tincidunt vel, nisi. Curabitur molestie euismod erat. Proin eros odio, mattis rutrum, pulvinar et, egestas vitae, magna. Integer semper, velit ut interdum malesuada, diam dolor pellentesque lacus, vitae commodo orci nisi et sem. Pellentesque adipiscing nisi. Nulla facilisi. Mauris lacinia lectus sit amet felis. Aliquam erat volutpat. Nulla porttitor tortor at nisl. Nam lectus nulla, bibendum pretium, dictum a, mattis nec, dolor. Nullam id justo sed diam aliquam tincidunt. Duis sollicitudin, dui ac commodo iaculis, mi risus sagittis odio, vel ultrices enim sem ut pede. Aenean quam. Nulla neque purus, ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. Integer ligula magna, dictum et, pulvinar non, ultricies ac, nibh.</p>\r\n<p>Aliquam posuere erat et orci eleifend cursus. Nullam tempus odio sem, lacinia pellentesque neque mollis a. In ut tempor ligula. Vestibulum ultricies bibendum lorem, a sollicitudin tellus euismod vel. Nam suscipit, diam ut volutpat lobortis, nibh ipsum tempor nibh, a vehicula tellus justo id nibh. Nulla pretium mollis convallis. Phasellus a nibh aliquet, ullamcorper quam aliquam, faucibus nulla. Phasellus mollis tristique vehicula. Vivamus sagittis, sem sed posuere aliquet, massa odio lobortis. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet.</p>\r\n</div>\r\n<div class="panel-body">\r\n<h4>Frequently Asked</h4>\r\n<p>Vestibulum justo. Nulla mauris ipsum, convallis ut, vestibulum eu, tincidunt vel, nisi. Curabitur molestie euismod erat. Proin eros odio, mattis rutrum, pulvinar et, egestas vitae, magna. Integer semper, velit ut interdum malesuada, diam dolor pellentesque lacus, vitae commodo orci nisi et sem. Pellentesque adipiscing nisi. Nulla facilisi. Mauris lacinia lectus sit amet felis. Aliquam erat volutpat. Nulla porttitor tortor at nisl. Nam lectus nulla, bibendum pretium, dictum a, mattis nec, dolor. Nullam id justo sed diam aliquam tincidunt. Duis sollicitudin, dui ac commodo iaculis, mi risus sagittis odio, vel ultrices enim sem ut pede. Aenean quam. Nulla neque purus, ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. Integer ligula magna, dictum et, pulvinar non, ultricies ac, nibh.</p>\r\n<p>Aliquam posuere erat et orci eleifend cursus. Nullam tempus odio sem, lacinia pellentesque neque mollis a. In ut tempor ligula. Vestibulum ultricies bibendum lorem, a sollicitudin tellus euismod vel. Nam suscipit, diam ut volutpat lobortis, nibh ipsum tempor nibh, a vehicula tellus justo id nibh. Nulla pretium mollis convallis. Phasellus a nibh aliquet, ullamcorper quam aliquam, faucibus nulla. Phasellus mollis tristique vehicula. Vivamus sagittis, sem sed posuere aliquet, massa odio lobortis. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet.</p>\r\n</div>\r\n<div class="panel-body">\r\n<h4>Frequently Asked</h4>\r\n<p>Vestibulum justo. Nulla mauris ipsum, convallis ut, vestibulum eu, tincidunt vel, nisi. Curabitur molestie euismod erat. Proin eros odio, mattis rutrum, pulvinar et, egestas vitae, magna. Integer semper, velit ut interdum malesuada, diam dolor pellentesque lacus, vitae commodo orci nisi et sem. Pellentesque adipiscing nisi. Nulla facilisi. Mauris lacinia lectus sit amet felis. Aliquam erat volutpat. Nulla porttitor tortor at nisl. Nam lectus nulla, bibendum pretium, dictum a, mattis nec, dolor. Nullam id justo sed diam aliquam tincidunt. Duis sollicitudin, dui ac commodo iaculis, mi risus sagittis odio, vel ultrices enim sem ut pede. Aenean quam. Nulla neque purus, ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. Integer ligula magna, dictum et, pulvinar non, ultricies ac, nibh.</p>\r\n<p>Aliquam posuere erat et orci eleifend cursus. Nullam tempus odio sem, lacinia pellentesque neque mollis a. In ut tempor ligula. Vestibulum ultricies bibendum lorem, a sollicitudin tellus euismod vel. Nam suscipit, diam ut volutpat lobortis, nibh ipsum tempor nibh, a vehicula tellus justo id nibh. Nulla pretium mollis convallis. Phasellus a nibh aliquet, ullamcorper quam aliquam, faucibus nulla. Phasellus mollis tristique vehicula. Vivamus sagittis, sem sed posuere aliquet, massa odio lobortis. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet.</p>\r\n</div>\r\n</div>\r\n</div>', 'faq-banner.png', '', '', '0', '1', ''),
(6, 'Term & Condition', '0', '<div class="col-xs-12">\r\n<div class="page-title">\r\n<h2>Term &amp; Condition</h2>\r\n<p>Aliquam posuere erat et orci eleifend cursus. Nullam tempus odio sem, lacinia pellentesque neque mollis a. In ut tempor ligula. Vestibulum ultricies bibendum lorem, a sollicitudin tellus euismod vel. Nam suscipit, diam ut volutpat lobortis, nibh ipsum tempor nibh, a vehicula tellus justo id nibh. Nulla pretium mollis convallis. Phasellus a nibh aliquet, ullamcorper quam aliquam, faucibus nulla. Phasellus mollis tristique vehicula. Vivamus sagittis, sem sed posuere aliquet, massa odio lobortis.</p>\r\n</div>\r\n</div>\r\n<div class="col-xs-12 col-sm-9">\r\n<div class="panel-group">\r\n<div class="panel-body">\r\n<h4>Frequently Asked</h4>\r\n<p>Vestibulum justo. Nulla mauris ipsum, convallis ut, vestibulum eu, tincidunt vel, nisi. Curabitur molestie euismod erat. Proin eros odio, mattis rutrum, pulvinar et, egestas vitae, magna. Integer semper, velit ut interdum malesuada, diam dolor pellentesque lacus, vitae commodo orci nisi et sem. Pellentesque adipiscing nisi. Nulla facilisi. Mauris lacinia lectus sit amet felis. Aliquam erat volutpat. Nulla porttitor tortor at nisl. Nam lectus nulla, bibendum pretium, dictum a, mattis nec, dolor. Nullam id justo sed diam aliquam tincidunt. Duis sollicitudin, dui ac commodo iaculis, mi risus sagittis odio, vel ultrices enim sem ut pede. Aenean quam. Nulla neque purus, ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. Integer ligula magna, dictum et, pulvinar non, ultricies ac, nibh.</p>\r\n<p>Aliquam posuere erat et orci eleifend cursus. Nullam tempus odio sem, lacinia pellentesque neque mollis a. In ut tempor ligula. Vestibulum ultricies bibendum lorem, a sollicitudin tellus euismod vel. Nam suscipit, diam ut volutpat lobortis, nibh ipsum tempor nibh, a vehicula tellus justo id nibh. Nulla pretium mollis convallis. Phasellus a nibh aliquet, ullamcorper quam aliquam, faucibus nulla. Phasellus mollis tristique vehicula. Vivamus sagittis, sem sed posuere aliquet, massa odio lobortis. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet.</p>\r\n</div>\r\n<div class="panel-body">\r\n<h4>Frequently Asked</h4>\r\n<p>Vestibulum justo. Nulla mauris ipsum, convallis ut, vestibulum eu, tincidunt vel, nisi. Curabitur molestie euismod erat. Proin eros odio, mattis rutrum, pulvinar et, egestas vitae, magna. Integer semper, velit ut interdum malesuada, diam dolor pellentesque lacus, vitae commodo orci nisi et sem. Pellentesque adipiscing nisi. Nulla facilisi. Mauris lacinia lectus sit amet felis. Aliquam erat volutpat. Nulla porttitor tortor at nisl. Nam lectus nulla, bibendum pretium, dictum a, mattis nec, dolor. Nullam id justo sed diam aliquam tincidunt. Duis sollicitudin, dui ac commodo iaculis, mi risus sagittis odio, vel ultrices enim sem ut pede. Aenean quam. Nulla neque purus, ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. Integer ligula magna, dictum et, pulvinar non, ultricies ac, nibh.</p>\r\n<p>Aliquam posuere erat et orci eleifend cursus. Nullam tempus odio sem, lacinia pellentesque neque mollis a. In ut tempor ligula. Vestibulum ultricies bibendum lorem, a sollicitudin tellus euismod vel. Nam suscipit, diam ut volutpat lobortis, nibh ipsum tempor nibh, a vehicula tellus justo id nibh. Nulla pretium mollis convallis. Phasellus a nibh aliquet, ullamcorper quam aliquam, faucibus nulla. Phasellus mollis tristique vehicula. Vivamus sagittis, sem sed posuere aliquet, massa odio lobortis. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet.</p>\r\n</div>\r\n<div class="panel-body">\r\n<h4>Frequently Asked</h4>\r\n<p>Vestibulum justo. Nulla mauris ipsum, convallis ut, vestibulum eu, tincidunt vel, nisi. Curabitur molestie euismod erat. Proin eros odio, mattis rutrum, pulvinar et, egestas vitae, magna. Integer semper, velit ut interdum malesuada, diam dolor pellentesque lacus, vitae commodo orci nisi et sem. Pellentesque adipiscing nisi. Nulla facilisi. Mauris lacinia lectus sit amet felis. Aliquam erat volutpat. Nulla porttitor tortor at nisl. Nam lectus nulla, bibendum pretium, dictum a, mattis nec, dolor. Nullam id justo sed diam aliquam tincidunt. Duis sollicitudin, dui ac commodo iaculis, mi risus sagittis odio, vel ultrices enim sem ut pede. Aenean quam. Nulla neque purus, ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. Integer ligula magna, dictum et, pulvinar non, ultricies ac, nibh.</p>\r\n<p>Aliquam posuere erat et orci eleifend cursus. Nullam tempus odio sem, lacinia pellentesque neque mollis a. In ut tempor ligula. Vestibulum ultricies bibendum lorem, a sollicitudin tellus euismod vel. Nam suscipit, diam ut volutpat lobortis, nibh ipsum tempor nibh, a vehicula tellus justo id nibh. Nulla pretium mollis convallis. Phasellus a nibh aliquet, ullamcorper quam aliquam, faucibus nulla. Phasellus mollis tristique vehicula. Vivamus sagittis, sem sed posuere aliquet, massa odio lobortis. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet. ullamcorper nec, eleifend at, fermentum ut, turpis. Mauris et ligula quis erat dignissim imperdiet.</p>\r\n</div>\r\n</div>\r\n</div>', 'faq-banner.png', '', '', '0', '1', ''),
(11, 'About Us', '0', '<h1>Welcome to <span class="text_color">Metric</span></h1>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacus metus, convallis ut leo nec, tincidunt eleifend justo. Ut felis orci, hendrerit a pulvinar et, gravida ac lorem. Sed vitae molestie sapien, at sollicitudin tortor.<br /> <br /> Duis id volutpat libero, id vestibulum purus.Donec euismod accumsan felis, <a href="#">egestas lobortis velit tempor</a> vitae. Integer eget velit fermentum, dignissim odio non, bibendum velit.</p>\r\n<ul>\r\n<li>&nbsp; <a href="#">Suspendisse potenti. Morbi mollis tellus ac sapien.</a></li>\r\n<li>&nbsp; <a href="#">Cras id dui. Nam ipsum risus, rutrum vitae, vestibulum eu.</a></li>\r\n<li>&nbsp; <a href="#">Phasellus accumsan cursus velit. Pellentesque egestas.</a></li>\r\n<li>&nbsp; <a href="#">Lorem Ipsum generators on the Internet tend to repeat predefined.</a></li>\r\n</ul>', 'about_us_slide1.jpg', '', '', '0', '1', ''),
(12, 'Contact Us', '0', '<div id="contact_form_map" class="col-xs-12 col-sm-6">\r\n<h3 class="page-subheading">Let''s get in touch</h3>\r\n<p>Lorem ipsum dolor sit amet onsectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor dapibus eget. Mauris tincidunt aliquam lectus sed vestibulum. Vestibulum bibendum suscipit mattis.</p>\r\n<br />\r\n<ul>\r\n<li>Praesent nec tincidunt turpis.</li>\r\n<li>Aliquam et nisi risus.&nbsp;Cras ut varius ante.</li>\r\n<li>Ut congue gravida dolor, vitae viverra dolor.</li>\r\n</ul>\r\n<br />\r\n<ul class="store_info">\r\n<li>7064 Lorem Ipsum Vestibulum 666/13</li>\r\n<li><span>+ 012 315 678 1234</span></li>\r\n<li><span>+39 0035 356 765</span></li>\r\n<li>Email: <span><a href="mailto:support@justtheme.com">support@justtheme.com</a></span></li>\r\n</ul>\r\n</div>', '', '', '', '0', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_color`
--

CREATE TABLE IF NOT EXISTS `tbl_color` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `category_id` int(90) NOT NULL,
  `color` varchar(244) NOT NULL,
  `status` varchar(22) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_color`
--

INSERT INTO `tbl_color` (`id`, `category_id`, `color`, `status`) VALUES
(3, 2, 'Red', '1'),
(4, 3, 'Blue', '1'),
(5, 3, 'Black', '1'),
(6, 2, 'Green', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

CREATE TABLE IF NOT EXISTS `tbl_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `create_date` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_faq`
--

INSERT INTO `tbl_faq` (`id`, `title`, `image`, `description`, `status`, `create_date`) VALUES
(1, 'Tristique', '', '<p>Vestibulum justo. Nulla mauris ipsum, convallis ut, vestibulum eu, tincidunt vel, nisi. Curabitur molestie euismod erat. Proin eros odio, mattis rutrum, pulvinar et, egestas vitae, magna. Integer semper, velit ut interdum malesuada, diam dolor pellentesque lacus</p>', '1', ''),
(2, 'Alyssa Hiyama', '', 'Vestibulum justo. Nulla mauris ipsum, convallis ut, vestibulum eu, tincidunt vel, nisi. Curabitur molestie euismod erat. Proin eros odio, mattis rutrum, pulvinar et, egestas vitae, magna. Integer semper, velit ut interdum malesuada, diam dolor pellentesque lacus,', '1', '1488525544'),
(3, 'Alberto Trombin', '', 'Vestibulum justo. Nulla mauris ipsum, convallis ut, vestibulum eu, tincidunt vel, nisi. Curabitur molestie euismod erat. Proin eros odio, mattis rutrum, pulvinar et, egestas vitae, magna. Integer semper, velit ut interdum malesuada, diam dolor pellentesque lacus', '1', '1488526278');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_filters`
--

CREATE TABLE IF NOT EXISTS `tbl_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `tbl_filters`
--

INSERT INTO `tbl_filters` (`id`, `p_id`, `title`, `type`) VALUES
(1, 0, 'Color', 'filter'),
(3, 1, 'Red', 'option'),
(4, 1, 'Black', 'option'),
(5, 1, 'Green', 'option'),
(6, 1, 'White', 'option'),
(7, 0, 'Size', 'filter'),
(8, 7, 'M', 'option'),
(9, 7, 'L', 'option'),
(10, 7, 'XL', 'option'),
(11, 7, 'XXL', 'option'),
(12, 0, 'Offers', 'filter'),
(13, 12, 'Offer 1', 'option'),
(14, 12, 'Offer 2', 'option'),
(15, 0, 'Material', 'filter'),
(16, 15, 'Bone', 'option'),
(17, 15, 'Alloy', 'option'),
(18, 15, 'Nickel', 'option'),
(19, 15, 'Silver', 'option'),
(20, 7, '6', 'option'),
(21, 7, '7', 'option'),
(22, 7, '8', 'option'),
(23, 7, '9', 'option'),
(24, 1, 'black', 'option');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_homepopup`
--

CREATE TABLE IF NOT EXISTS `tbl_homepopup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_homepopup`
--

INSERT INTO `tbl_homepopup` (`id`, `title`, `content`, `image`) VALUES
(1, 'vbnvn334344334gyjghjghjgh', 'Integer ac a ac est vestibulum fermentum leo a mattis sem tempus dignissim pulvinar pretium et ad eu ligula natoque vestibulum consectetur.Mi pharetra blandit quis ultrices suspendisse a suscipit vestibulum curae ligula sagittis vivamus eleifend tristique sit parturient vestibulum.434443443', 'newletter-bg.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item`
--

CREATE TABLE IF NOT EXISTS `tbl_item` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(22) NOT NULL,
  `pro_id` varchar(222) NOT NULL,
  `user_id` varchar(232) NOT NULL,
  `pro_name` varchar(222) NOT NULL,
  `color` varchar(22) NOT NULL,
  `size` varchar(222) NOT NULL,
  `unit_price` varchar(20) NOT NULL,
  `options` varchar(200) NOT NULL,
  `qty` varchar(222) NOT NULL,
  `total_amount` varchar(222) NOT NULL,
  `order_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_item`
--

INSERT INTO `tbl_item` (`id`, `order_id`, `pro_id`, `user_id`, `pro_name`, `color`, `size`, `unit_price`, `options`, `qty`, `total_amount`, `order_date`) VALUES
(2, '4', '1', '34', 'GoldCartz Full Sleeve Solid Mens Jacket', '', '', '40', '<small><a href=''#''>Color : <strong>Red</strong></a></small><br>', '1', '40', '2017-04-26'),
(3, '5', '11', '34', 'Chic Fashion', '', '', '4444', '', '1', '4444', '2017-04-26'),
(4, '6', '51', '34', 'T-shirt ', '', '', '100', '', '1', '100', '2017-04-26'),
(5, '7', '23', '34', 'Jeans Fashion', '', '', '600', '', '4', '2400', '2017-04-26'),
(6, '8', '63', '38', 'watch for women', '', '', '100', '', '1', '100', '2017-04-26'),
(7, '9', '2', '38', 'United Colors of Benetton', '', '', '30', '', '1', '30', '2017-04-26'),
(8, '10', '56', '36', 'shirt', '', '', '100', '', '1', '100', '2017-04-26'),
(9, '11', '1', '36', 'GoldCartz Full Sleeve Solid Mens Jacket', '', '', '40', '', '1', '40', '2017-04-26'),
(10, '11', '12', '36', 'Chic Fashion', '', '', '30000', '', '1', '30000', '2017-04-26'),
(11, '13', '48', '35', 'Reebok jeans', '', '', '500', '', '1', '500', '2017-04-26'),
(12, '14', '11', '34', 'Chic Fashion', '', '', '4444', '', '1', '4444', '2017-04-26'),
(13, '14', '12', '34', 'Chic Fashion', '', '', '30000', '', '1', '30000', '2017-04-26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_metatag`
--

CREATE TABLE IF NOT EXISTS `tbl_metatag` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `meta_title` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `meta_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_newsletter`
--

CREATE TABLE IF NOT EXISTS `tbl_newsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_newsletter`
--

INSERT INTO `tbl_newsletter` (`id`, `email`, `status`) VALUES
(2, 'admin@gmail.com', 0),
(3, 'ajays2612@gmail.com', 0),
(4, 'ajays2612@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_offer`
--

CREATE TABLE IF NOT EXISTS `tbl_offer` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `couponcode` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_offer`
--

INSERT INTO `tbl_offer` (`id`, `title`, `couponcode`, `discount`, `createdate`, `status`) VALUES
(0, 'free50', 'free50', '20', '2017-04-25 10:47:30', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE IF NOT EXISTS `tbl_order` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `ord_time` varchar(20) NOT NULL,
  `payment_method` varchar(233) NOT NULL,
  `ord_date` datetime NOT NULL,
  `coupon_title` varchar(20) NOT NULL,
  `coupon_discount` varchar(20) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `user_id`, `ord_time`, `payment_method`, `ord_date`, `coupon_title`, `coupon_discount`, `total_amount`, `ip_address`) VALUES
(4, '34', '1493203508', 'Credit Card', '2017-04-26 10:45:08', '0', '0', '40', '223.190.98.32'),
(5, '34', '1493203777', 'Money_order', '2017-04-26 10:49:37', '0', '0', '4444', '223.190.98.32'),
(6, '34', '1493204460', 'Money_order', '2017-04-26 11:01:00', '0', '0', '100', '223.190.98.32'),
(7, '34', '1493204542', 'Money_order', '2017-04-26 11:02:22', '0', '0', '2400', '223.190.98.32'),
(8, '38', '1493207941', 'Money_order', '2017-04-26 11:59:01', '0', '0', '100', '223.190.98.32'),
(9, '38', '1493207986', 'Money_order', '2017-04-26 11:59:46', '0', '0', '30', '223.190.98.32'),
(10, '36', '1493211423', 'Credit Card', '2017-04-26 12:57:03', '0', '0', '100', '223.190.98.32'),
(11, '36', '1493215035', 'Money_order', '2017-04-26 13:57:15', '0', '0', '30040', '223.190.98.32'),
(12, '36', '1493215041', 'Credit Card', '2017-04-26 13:57:21', '0', '0', '0', '223.190.98.32'),
(13, '35', '1493215265', 'Money_order', '2017-04-26 14:01:05', '0', '0', '500', '223.190.98.32'),
(14, '34', '1493215582', 'Money_order', '2017-04-26 14:06:22', '0', '0', '34444', '223.190.98.32');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` varchar(20) NOT NULL,
  `product_sku` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `sortdesc` varchar(400) NOT NULL,
  `description` varchar(500) NOT NULL,
  `price` varchar(100) NOT NULL,
  `spacel_price` varchar(100) NOT NULL,
  `descount` varchar(100) NOT NULL,
  `reletedproduct` varchar(100) NOT NULL,
  `product_color` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `product_brand` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `vender_type` varchar(230) DEFAULT NULL,
  `dealday` varchar(20) NOT NULL,
  `grand_gadget_sale` varchar(22) NOT NULL,
  `selling_smart_phone` varchar(22) NOT NULL,
  `discount_you` varchar(22) NOT NULL,
  `offer_for_you` varchar(22) NOT NULL,
  `laptop_desktop` varchar(22) NOT NULL,
  `gas_stove` varchar(22) NOT NULL,
  `women_kurti` varchar(22) NOT NULL,
  `offer_furniture` varchar(22) NOT NULL,
  `sport_fitness` varchar(22) NOT NULL,
  `home_appline` varchar(22) NOT NULL,
  `deal_it` varchar(22) NOT NULL,
  `new_arrival` varchar(22) NOT NULL,
  `fasion_travel` varchar(22) NOT NULL,
  `best_tv` varchar(22) NOT NULL,
  `product_stock` int(11) NOT NULL,
  `filter_ids` varchar(250) NOT NULL,
  `create_date` time NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `meta_title` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `meta_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `category_id`, `product_sku`, `name`, `title`, `sortdesc`, `description`, `price`, `spacel_price`, `descount`, `reletedproduct`, `product_color`, `size`, `product_brand`, `image`, `vender_type`, `dealday`, `grand_gadget_sale`, `selling_smart_phone`, `discount_you`, `offer_for_you`, `laptop_desktop`, `gas_stove`, `women_kurti`, `offer_furniture`, `sport_fitness`, `home_appline`, `deal_it`, `new_arrival`, `fasion_travel`, `best_tv`, `product_stock`, `filter_ids`, `create_date`, `update_date`, `status`, `meta_title`, `meta_keyword`, `meta_description`) VALUES
(1, '5', '00012', 'GoldCartz Full Sleeve Solid Mens Jacket', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '42', '40', '10%', '4', '11,12,13', 11, '3', '', '34', '1', '1', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '1', 100, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(2, '5', '0001', 'United Colors of Benetton', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '34', '30', '3%', '1', '14,15', 0, '', '', '34', '1', '0', '0', 'No', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', 5, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(3, '9', '0001', 'Black Casual Jacket', '', 'bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.<sup id="cite_ref-3" class="reference"><a href="https://en.wikipedia.org/wiki/Description#cite_note-3">[3]</a></sup> The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coache', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '57', '53', '6%', '2', '11,12,13', 22, '', '', '0', '0', '0', '1', 'No', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', 4, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(4, '10', '0001', 'Coal Black Hooded', '', 'bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.<sup id="cite_ref-3" class="reference"><a href="https://en.wikipedia.org/wiki/Description#cite_note-3">[3]</a></sup> The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coache', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '40', '34', '5%', '3', '35,36', 33, '', '', '0', '0', '0', '0', 'No', '0', '0', '0', '1', '1', '1', '1', '0', '1', '0', '0', 4, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(5, '10', '0001', 'Benetton Black Casual', '', 'bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.<sup id="cite_ref-3" class="reference"><a href="https://en.wikipedia.org/wiki/Description#cite_note-3">[3]</a></sup> The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coache', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '25', '23', '7%', '4', '20,27,28', 0, '', '', '0', '0', '0', '0', 'No', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', 5, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(6, '10', '0001', 'Benetton Red Quilted', '', 'bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.<sup id="cite_ref-3" class="reference"><a href="https://en.wikipedia.org/wiki/Description#cite_note-3">[3]</a></sup> The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coache', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '35', '32', '9%', '5', '11,12,13', 0, '', '', '0', '0', '0', '0', 'No', '0', '0', '0', '0', '1', '0', '1', '0', '0', '1', '0', 5, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(7, '11', '0001', 'Tinted Navy Solid', '', 'bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.<sup id="cite_ref-3" class="reference"><a href="https://en.wikipedia.org/wiki/Description#cite_note-3">[3]</a></sup> The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coache', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '23', '21', '8%', '6', '13', 0, '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 6, '', '00:00:00', '0000-00-00 00:00:00', '1', '', '', ''),
(8, '32', '0001', 'Black Casual Shoes', '', 'bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.<sup id="cite_ref-3" class="reference"><a href="https://en.wikipedia.org/wiki/Description#cite_note-3">[3]</a></sup> The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coache', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '12', '11', '5%', '7', '11,12,13,14,15,16,17,18', 0, '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 4, '', '00:00:00', '0000-00-00 00:00:00', '1', '', '', ''),
(9, '38', '0001', 'Queen Brown Faux', '', 'bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.<sup id="cite_ref-3" class="reference"><a href="https://en.wikipedia.org/wiki/Description#cite_note-3">[3]</a></sup> The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coache', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '45', '34', '5%', '8', '32,33,34,35,36', 0, '', '', '0', '0', '0', '0', 'No', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', 6, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(10, '5', '0001', 'Glam Tree', '', 'bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.<sup id="cite_ref-3" class="reference"><a href="https://en.wikipedia.org/wiki/Description#cite_note-3">[3]</a></sup> The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coache', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '23', '21', '6%', '9', '11,12,13,18,19,20,31,32,33', 0, '', '', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '0', '1', 5, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(11, '5', '0001', 'Chic Fashion', '', 'bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.<sup id="cite_ref-3" class="reference"><a href="https://en.wikipedia.org/wiki/Description#cite_note-3">[3]</a></sup> The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coache', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '5000', '4444', '23%', '1,2,3,4,5,6,7,8,9,10', '11,12,13,14,15,16,17,18,19,20', 0, '', '', '0', '1', '1', '1', 'No', '1', '1', '1', '0', '0', '0', '0', '0', '1', '1', '0', 5, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(12, '5', '0001', 'Chic Fashion', '', 'bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.<sup id="cite_ref-3" class="reference"><a href="https://en.wikipedia.org/wiki/Description#cite_note-3">[3]</a></sup> The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coache', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '40000', '30000', '7%', '8,9,10,11', '11,12,13,14,15,16,17,18,19,20,27', 0, '', '', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', 5, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(15, '28', '', 'Canvas Shoes', '', '<span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</span>', '<span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</span>', '1000', '800', '20', '', '', 0, '', '', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 10, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(22, '39', '', 'Men Jeans', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '800', '20', '', '', 0, '', '', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 10, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'Jeans', 'Jeans for men', 'this is pant and jeans'),
(23, '39', '', 'Jeans Fashion', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '800', '600', '20', '22', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', 10, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'jeans', 'jeans', 'jeans'),
(24, '39', '', 'Blud Color Jeans', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1200', '1000', '20', '23', '', 0, '', '', '0', '1', '1', '0', '1', '1', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'Jeans men', 'jens', 'jens here we can ourchase'),
(25, '39', '', 'Jeans For Men', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '2000', '1800', '10', '22,23,24,27', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'jeans', 'jeans', 'jean'),
(27, '40', '', 'T-shirt puma', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '500', '400', '10', '27', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 't-shirts ', 't-shirts here for men', 'all tshirts for men'),
(28, '40', '', 'Zara T-shirt', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1200', '1000', '20', '27', '', 0, '', '', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'tshirt here', 't-shirt here now we can purchase', 't-shirt le lo bhai'),
(29, '40', '', 'Addidas', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '800', '20', '27,28,29', '', 0, '', '', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 't-shirt puma ', 't-shirt all bran ', 't-shirts now here'),
(30, '40', '', 'Puma', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1200', '1000', '20', '27,28,29', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', 13, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'tshirt here purchase', 'all t-shirt hre', 'dskfhdskfjdskjfdsh'),
(31, '41', '', 'Puma Shirts', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1200', '1000', '20', '31', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'shirt ', 'shirt shirt', 'kdhfkjdsfds'),
(32, '41', '', 'Rebook shirts', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '900', '700', '20', '31', '', 0, '', '', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'shirt rebook here all', 'sfsjklfdklfjdsl', 'sfsdfkjsdlkfjldsfjldskfdkls'),
(33, '41', '', 'Shirts ', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1300', '1200', '10', '31,32', '', 0, '', '', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kfdshfkh', 'khskjfhskj', 'kjhkj'),
(34, '41', '', 'Shirt all product', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1200', '1200', '', '31,32,33', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'fkdsjhk', 'kfdskjh', 'xkfdshfdsfadsfdsf'),
(38, '42', '', 'Addidas For men', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '2000', '1800', '20', '35,36,37', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'shoes reebok', 'reebok here purchase juta', 'reebok here purchase juta'),
(40, '43', '', 'watch ', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '800', '20', '', '', 0, '', '', '0', '1', '1', '0', '1', '1', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'watch here', 'watch here', 'watch here'),
(41, '43', '', 'Watch', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '800', '20', '40', '', 0, '', '', '0', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'watch and ghdi', 'watch and ghdi', 'watch and ghdi'),
(42, '43', '', 'Watch', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '600', '500', '20', '40,41', '', 0, '', '', '0', '0', '1', '0', '1', '1', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', '', '', ''),
(44, '42', '', 'shoes ', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '900', '10', '38', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'shoes for all product', 'shoes for all product', 'shoes for all product'),
(45, '42', '', 'Puma', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1200', '1000', '20', '38,44', '', 0, '', '', '0', '1', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'Shoes for men', 'Shoes for men', 'Shoes for men'),
(46, '42', '', 'Shoes Reebok', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1200', '1000', '20', '38,44,45', '', 0, '', '', '0', '0', '1', '0', '1', '1', '0', '0', '0', '0', '1', '0', '0', '1', '1', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'Reebok shoes', 'Reebok shoes', 'Reebok shoes'),
(47, '44', '', 'Jean ', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '800', '500', '30', '', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'Jeans and paint for girl', 'Jeans and paint for girl', 'Jeans and paint for girl'),
(48, '44', '', 'Reebok jeans', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '500', '50', '47', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'Jeans for Women', 'Jeans for Women', 'Jeans for Women'),
(49, '44', '', 'Jeans', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1200', '1000', '20', '47,48', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'Jeans for Women', 'Jeans for Women', 'Jeans for Women'),
(50, '44', '', 'Puma', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1900', '1800', '10', '47,48,49', '', 0, '', '', '0', '0', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '0', '1', '1', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'jeans for women', 'jeans for women', 'jeans for women'),
(51, '45', '', 'T-shirt ', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '200', '100', '50', '', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 't-shirt for men', 't-shirt for men', 't-shirt for men'),
(52, '45', '', 'T-shirt women', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '800', '20', '51', '', 0, '', '', '0', '0', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'T-shirt for women', 'T-shirt for women', 'T-shirt for women'),
(53, '45', '', 'Reebok T-shirt', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '800', '20', '51,52', '', 0, '', '', '0', '0', '1', '0', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'T-shir women', 'T-shir women', 'T-shir women'),
(54, '45', '', 'T-shir women', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '200', '100', '10', '51,52,53', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'T-shirt women', 'T-shirt women', 'T-shirt women'),
(55, '46', '', 'Shirt addidas', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '500', '50', '', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'shirt for women', 'shirt for women', 'shirt for women'),
(56, '46', '', 'shirt', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '200', '100', '50', '55', '', 0, '', '', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'shirt for women', 'shirt for women', 'shirt for women'),
(57, '46', '', 'shirt zara', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '300', '200', '10', '55,56', '', 0, '', '', '0', '0', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', 10, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'shirt for women', 'shirt for women', 'shirt for women'),
(58, '46', '', 'shirts puma', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '500', '250', '50', '55,56,57', '', 0, '', '', '0', '0', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'shirt for men', 'shirt for men', 'shirt for men'),
(59, '47', '', 'sandals', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '500', '50', '', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '0', 10, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'sandals for women', 'sandals for women', 'sandals for women'),
(60, '47', '', 'sandals for women', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '500', '50', '59', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'sandals for women', 'sandals for women', 'sandals for women'),
(61, '47', '', 'sandals', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '200', '100', '50', '59,60', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'sandals for women', 'sandals for women', 'sandals for women'),
(62, '47', '', 'sandals', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '100', '50', '50', '59,60,61', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'sandals for women', 'sandals for women', 'sandals for women'),
(63, '48', '', 'watch for women', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '200', '100', '50', '', '', 0, '', '', '0', '1', '1', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'watch for women', 'watch for women', 'watch for women'),
(64, '48', '', 'watch', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '500', '50', '63', '', 0, '', '', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 30, '', '838:59:59', '0000-00-00 00:00:00', '1', 'watch for women', 'watch for women', 'watch for women'),
(65, '48', '', 'watch ', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '500', '300', '40', '63,64', '', 0, '', '', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'watch for women', 'watch for women', 'watch for women'),
(66, '48', '', 'watch titan', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '4000', '4000', '0', '63,64,65', '', 0, '', '', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'watch for women', 'watch for women', 'watch for women'),
(67, '49', '', 'Kids jeans', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '600', '40', '', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'Kids product', 'Kids product', 'Kids product'),
(68, '49', '', 'kids', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '2000', '1000', '50', '67', '', 0, '', '', '0', '1', '1', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 12, '', '838:59:59', '0000-00-00 00:00:00', '1', 'Kids for jeans', 'Kids for jeans', 'Kids for jeans');
INSERT INTO `tbl_product` (`id`, `category_id`, `product_sku`, `name`, `title`, `sortdesc`, `description`, `price`, `spacel_price`, `descount`, `reletedproduct`, `product_color`, `size`, `product_brand`, `image`, `vender_type`, `dealday`, `grand_gadget_sale`, `selling_smart_phone`, `discount_you`, `offer_for_you`, `laptop_desktop`, `gas_stove`, `women_kurti`, `offer_furniture`, `sport_fitness`, `home_appline`, `deal_it`, `new_arrival`, `fasion_travel`, `best_tv`, `product_stock`, `filter_ids`, `create_date`, `update_date`, `status`, `meta_title`, `meta_keyword`, `meta_description`) VALUES
(69, '49', '', 'Kids Jeans', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '500', '50', '67,68', '', 0, '', '', '0', '1', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 10, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids for jeans', 'kids for jeans', 'kids for jeans'),
(70, '49', '', 'kids jeans addidas', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '400', '200', '50', '67,68,69', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids jeans', 'kids jeans', 'kids jeans'),
(71, '50', '', 'T-shirt', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '200', '100', '50', '', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids product', 'kids product', 'kids product'),
(72, '50', '', 'Kids', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '200', '100', '50', '71', '', 0, '', '', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids shirt', 'kids shirt', 'kids shirt'),
(73, '50', '', 'puma Kids ', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '400', '300', '10', '71,72', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids product ', 'kids product ', 'kids product '),
(74, '50', '', 'kids', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '300', '200', '10', '71,72,73', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids shirt', 'kids shirt', 'kids shirt'),
(75, '51', '', 'Shirt', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '100', '100', '', '', '', 0, '', '', '0', '0', '0', '0', 'No', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids dress', 'kids dress', 'kids dress'),
(76, '51', '', 'kids shirts', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '600', '40', '75', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids shirt ', 'kids shirt ', 'kids shirt '),
(77, '51', '', 'kids puma', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '100', '50', '12', '75,76', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids dress', 'kids dress', 'kids dress'),
(78, '51', '', 'All kids shirt', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '400', '200', '50', '75,76,77', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids shirt', 'kids shirt', 'kids shirt'),
(79, '52', '', 'Shoes For Kids', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '1000', '500', '50', '', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 12, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids shoes', 'kids shoes', 'kids shoes'),
(80, '52', '', 'Shoes', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '700', '700', '', '79,80', '', 0, '', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', 10, '1,7', '838:59:59', '0000-00-00 00:00:00', '1', 'kids shoes', 'kids shoes', 'kids shoes'),
(81, '52', '', 'Shoes for Kids', '', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '<strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</span>', '300', '300', '', '79,80', '', 0, '', '', '0', '0', '0', '0', 'No', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 10, '7', '838:59:59', '0000-00-00 00:00:00', '1', 'Kids shoes', 'Kids shoes', 'Kids shoes');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_filters`
--

CREATE TABLE IF NOT EXISTS `tbl_product_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `o_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=353 ;

--
-- Dumping data for table `tbl_product_filters`
--

INSERT INTO `tbl_product_filters` (`id`, `p_id`, `o_id`) VALUES
(10, 3, 3),
(11, 3, 10),
(12, 3, 13),
(55, 22, 3),
(56, 22, 4),
(57, 22, 6),
(58, 22, 8),
(59, 22, 9),
(60, 22, 10),
(61, 22, 11),
(62, 23, 3),
(63, 23, 5),
(64, 23, 6),
(65, 23, 8),
(66, 23, 9),
(67, 23, 10),
(68, 23, 11),
(79, 25, 3),
(80, 25, 4),
(81, 25, 6),
(82, 25, 8),
(83, 25, 9),
(84, 25, 10),
(85, 25, 11),
(86, 27, 3),
(87, 27, 4),
(88, 27, 5),
(89, 27, 6),
(90, 27, 8),
(91, 27, 9),
(92, 27, 10),
(93, 27, 11),
(94, 27, 13),
(95, 27, 14),
(96, 28, 3),
(97, 28, 4),
(98, 28, 6),
(99, 28, 9),
(100, 28, 10),
(101, 28, 11),
(109, 29, 3),
(110, 29, 4),
(111, 29, 6),
(112, 29, 8),
(113, 29, 9),
(114, 29, 10),
(115, 29, 11),
(116, 30, 3),
(117, 30, 4),
(118, 30, 5),
(119, 30, 6),
(120, 30, 8),
(121, 30, 9),
(122, 30, 11),
(123, 30, 13),
(124, 30, 14),
(125, 31, 3),
(126, 31, 4),
(127, 31, 5),
(128, 31, 6),
(129, 31, 8),
(130, 31, 9),
(131, 31, 11),
(132, 33, 3),
(133, 33, 4),
(134, 33, 5),
(135, 33, 8),
(136, 33, 9),
(137, 33, 10),
(138, 34, 3),
(139, 34, 4),
(140, 34, 8),
(141, 34, 9),
(142, 34, 10),
(143, 34, 11),
(147, 35, 3),
(148, 35, 4),
(149, 35, 6),
(150, 35, 20),
(151, 35, 21),
(152, 35, 22),
(153, 35, 23),
(160, 38, 3),
(161, 38, 4),
(162, 38, 5),
(163, 38, 20),
(164, 38, 21),
(165, 38, 22),
(166, 38, 23),
(167, 37, 3),
(168, 37, 4),
(169, 37, 20),
(170, 37, 21),
(171, 37, 22),
(172, 37, 23),
(173, 36, 3),
(174, 36, 4),
(175, 36, 6),
(176, 36, 21),
(177, 36, 22),
(178, 36, 23),
(179, 44, 3),
(180, 44, 4),
(181, 44, 6),
(182, 44, 20),
(183, 44, 21),
(184, 44, 22),
(185, 44, 23),
(186, 47, 3),
(187, 47, 4),
(188, 47, 5),
(189, 47, 8),
(190, 47, 9),
(191, 47, 10),
(192, 47, 11),
(193, 48, 3),
(194, 48, 4),
(195, 48, 5),
(196, 48, 8),
(197, 48, 9),
(198, 48, 10),
(199, 49, 3),
(200, 49, 4),
(201, 49, 5),
(202, 49, 6),
(203, 49, 8),
(204, 49, 9),
(205, 49, 10),
(206, 51, 3),
(207, 51, 4),
(208, 51, 5),
(209, 51, 8),
(210, 51, 9),
(211, 51, 11),
(212, 52, 3),
(213, 52, 4),
(214, 52, 5),
(215, 52, 8),
(216, 52, 9),
(217, 52, 11),
(218, 54, 3),
(219, 54, 4),
(220, 54, 5),
(221, 54, 8),
(222, 54, 9),
(223, 54, 10),
(224, 54, 11),
(225, 55, 3),
(226, 55, 5),
(227, 55, 6),
(228, 55, 8),
(229, 55, 9),
(230, 55, 11),
(231, 57, 3),
(232, 57, 4),
(233, 57, 5),
(234, 57, 8),
(235, 57, 9),
(236, 57, 10),
(237, 59, 3),
(238, 59, 4),
(239, 59, 5),
(240, 59, 20),
(241, 59, 21),
(242, 59, 22),
(243, 60, 3),
(244, 60, 4),
(245, 60, 5),
(246, 60, 8),
(247, 60, 9),
(248, 60, 10),
(249, 60, 11),
(250, 61, 3),
(251, 61, 4),
(252, 61, 5),
(253, 61, 20),
(254, 61, 21),
(255, 61, 22),
(256, 61, 23),
(257, 62, 3),
(258, 62, 4),
(259, 62, 22),
(260, 62, 23),
(261, 67, 3),
(262, 67, 4),
(263, 67, 5),
(264, 67, 9),
(265, 67, 10),
(266, 67, 11),
(267, 69, 3),
(268, 69, 4),
(269, 69, 6),
(270, 69, 8),
(271, 69, 9),
(272, 69, 10),
(273, 70, 3),
(274, 70, 4),
(275, 70, 8),
(276, 70, 9),
(277, 71, 3),
(278, 71, 4),
(279, 71, 5),
(280, 71, 8),
(281, 71, 9),
(282, 71, 10),
(283, 72, 3),
(284, 72, 4),
(285, 72, 5),
(286, 72, 8),
(287, 72, 9),
(288, 72, 10),
(289, 72, 11),
(290, 73, 3),
(291, 73, 4),
(292, 73, 8),
(293, 73, 9),
(294, 74, 3),
(295, 74, 4),
(296, 74, 5),
(297, 74, 9),
(298, 74, 10),
(299, 74, 11),
(300, 75, 3),
(301, 75, 4),
(302, 75, 6),
(303, 75, 8),
(304, 75, 9),
(305, 76, 3),
(306, 76, 4),
(307, 76, 6),
(308, 76, 8),
(309, 76, 9),
(310, 76, 11),
(311, 77, 3),
(312, 77, 4),
(313, 77, 8),
(314, 77, 9),
(315, 78, 3),
(316, 78, 4),
(317, 78, 5),
(318, 78, 8),
(319, 78, 9),
(320, 78, 10),
(321, 79, 3),
(322, 79, 4),
(323, 79, 6),
(324, 79, 20),
(325, 79, 21),
(326, 80, 3),
(327, 80, 4),
(328, 80, 20),
(329, 80, 21),
(330, 80, 22),
(331, 81, 4),
(332, 81, 5),
(333, 81, 20),
(334, 81, 21),
(335, 81, 22),
(343, 1, 3),
(344, 1, 5),
(345, 1, 8),
(346, 1, 9),
(347, 1, 10),
(348, 1, 13),
(349, 1, 14),
(350, 2, 3),
(351, 2, 4),
(352, 2, 9);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_image`
--

CREATE TABLE IF NOT EXISTS `tbl_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_title` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=236 ;

--
-- Dumping data for table `tbl_product_image`
--

INSERT INTO `tbl_product_image` (`id`, `product_id`, `image`, `image_title`, `status`) VALUES
(20, '1', '14908698403825.jpg', '', '0'),
(21, '1', '14908698407509.jpg', '', '0'),
(23, '2', '14908699029093.jpg', '', '0'),
(24, '2', '14908699028155.jpg', '', '0'),
(25, '3', '14908699457586.jpg', '', '0'),
(26, '3', '14908699452959.jpg', '', '0'),
(28, '4', '14908700293324.jpg', '', '0'),
(29, '5', '14908700779206.jpg', '', '0'),
(30, '5', '14908700775150.jpg', '', '0'),
(31, '6', '14908701691005.jpeg', '', '0'),
(32, '6', '14908701697771.jpeg', '', '0'),
(33, '7', '14908716458998.jpg', '', '0'),
(34, '7', '1490871645399.jpg', '', '0'),
(35, '8', '14908717471775.jpg', '', '0'),
(36, '8', '14908717478482.jpg', '', '0'),
(37, '1', '14910302287404.jpg', '', '0'),
(38, '1', '14910302335131.jpg', '', '0'),
(39, '4', '14919933598952.jpg', '', '0'),
(40, '9', '14920707669167.jpg', '', '0'),
(41, '9', '14920707667019.jpg', '', '0'),
(42, '12', '14920708188967.jpg', '', '0'),
(43, '12', '14920708185444.jpg', '', '0'),
(44, '11', '14920708471319.jpg', '', '0'),
(45, '11', '14920708474723.jpg', '', '0'),
(46, '10', '14920720193263.jpg', '', '0'),
(47, '10', '14920720191549.jpg', '', '0'),
(51, '15', '14922582515454.jpg', '', '0'),
(52, '15', '14922582511841.jpg', '', '0'),
(53, '15', '1492258251363.jpg', '', '0'),
(54, '22', '1492409788409.jpg', '', '0'),
(55, '22', '14924097893967.jpg', '', '0'),
(56, '22', '14924097896571.jpg', '', '0'),
(57, '23', '1492410719321.jpg', '', '0'),
(58, '23', '14924107193260.jpg', '', '0'),
(59, '23', '1492410719279.jpg', '', '0'),
(60, '24', '14924113298156.jpg', '', '0'),
(61, '24', '14924113305967.jpg', '', '0'),
(62, '24', '14924113309149.jpg', '', '0'),
(63, '25', '14924114637299.jpg', '', '0'),
(64, '25', '14924114631312.jpg', '', '0'),
(65, '25', '14924114635421.jpg', '', '0'),
(66, '27', '14924118835666.jpg', '', '0'),
(67, '27', '14924118837913.jpg', '', '0'),
(68, '27', '14924118838864.jpg', '', '0'),
(69, '28', '14924121213271.jpg', '', '0'),
(70, '28', '14924121213106.jpg', '', '0'),
(71, '28', '14924121226137.jpg', '', '0'),
(72, '29', '14924123575769.jpg', '', '0'),
(73, '29', '1492412357463.jpg', '', '0'),
(74, '29', '149241235867.jpg', '', '0'),
(75, '30', '14924126386767.jpg', '', '0'),
(76, '30', '14924126388053.jpg', '', '0'),
(77, '30', '14924126381164.jpg', '', '0'),
(78, '31', '14924128653819.jpg', '', '0'),
(79, '31', '14924128664969.jpg', '', '0'),
(80, '31', '14924128667685.jpg', '', '0'),
(81, '32', '14924129964238.jpg', '', '0'),
(82, '32', '14924129969614.jpg', '', '0'),
(83, '32', '14924129964714.jpg', '', '0'),
(84, '33', '14924131855584.jpg', '', '0'),
(85, '33', '14924131853619.jpg', '', '0'),
(86, '33', '14924131851775.jpg', '', '0'),
(87, '34', '14924135868383.jpg', '', '0'),
(88, '34', '14924135864466.jpg', '', '0'),
(89, '34', '14924135874239.jpg', '', '0'),
(99, '38', '14924148626761.jpg', '', '0'),
(100, '38', '14924148625513.jpg', '', '0'),
(101, '38', '14924148629898.jpg', '', '0'),
(102, '40', '14924153758964.JPG', '', '0'),
(103, '40', '14924153756428.JPG', '', '0'),
(104, '40', '14924153754813.JPG', '', '0'),
(105, '41', '14924154887166.jpg', '', '0'),
(106, '41', '14924154892187.jpg', '', '0'),
(107, '41', '14924154899951.jpg', '', '0'),
(108, '42', '14924156719896.jpg', '', '0'),
(109, '42', '14924156726075.jpg', '', '0'),
(110, '42', '14924156721985.jpg', '', '0'),
(111, '44', '14924178779325.jpg', '', '0'),
(112, '44', '14924178774686.jpg', '', '0'),
(113, '44', '14924178775689.jpg', '', '0'),
(114, '45', '14924197697439.jpg', '', '0'),
(115, '45', '14924197693611.jpg', '', '0'),
(116, '45', '14924197692472.jpg', '', '0'),
(117, '46', '14924198681868.jpg', '', '0'),
(118, '46', '14924198688841.jpg', '', '0'),
(119, '46', '14924198684987.jpg', '', '0'),
(120, '47', '14924199877319.jpg', '', '0'),
(121, '47', '14924199879567.jpg', '', '0'),
(122, '47', '14924199879535.jpg', '', '0'),
(123, '48', '14924205678259.jpg', '', '0'),
(124, '48', '14924205673025.jpg', '', '0'),
(125, '48', '14924205678605.jpg', '', '0'),
(126, '49', '14924206811059.jpg', '', '0'),
(127, '49', '14924206818380.jpg', '', '0'),
(128, '49', '14924206819323.jpg', '', '0'),
(129, '50', '14924207815669.jpg', '', '0'),
(130, '50', '14924207819183.jpg', '', '0'),
(131, '50', '14924207819497.jpg', '', '0'),
(132, '51', '14924209465264.jpg', '', '0'),
(133, '51', '14924209462508.jpg', '', '0'),
(134, '51', '14924209479443.jpg', '', '0'),
(135, '52', '14924213875840.jpg', '', '0'),
(136, '52', '14924213874184.jpg', '', '0'),
(137, '52', '14924213882878.jpg', '', '0'),
(138, '53', '14924214902583.jpg', '', '0'),
(139, '53', '14924214904435.jpg', '', '0'),
(140, '53', '14924214909284.jpg', '', '0'),
(141, '54', '14924215565384.jpg', '', '0'),
(142, '54', '14924215566552.jpg', '', '0'),
(143, '54', '14924215562381.jpg', '', '0'),
(144, '55', '14924217311412.jpg', '', '0'),
(145, '55', '14924217315081.jpg', '', '0'),
(146, '55', '14924217322427.jpg', '', '0'),
(147, '56', '14924218484195.jpg', '', '0'),
(148, '56', '14924218487323.jpg', '', '0'),
(149, '56', '14924218487600.jpg', '', '0'),
(150, '57', '14924219144478.jpg', '', '0'),
(151, '57', '14924219145447.jpg', '', '0'),
(152, '57', '14924219146463.jpg', '', '0'),
(153, '58', '14924220113530.jpg', '', '0'),
(154, '58', '14924220124941.jpg', '', '0'),
(155, '58', '14924220127873.jpg', '', '0'),
(156, '59', '14924221342352.jpg', '', '0'),
(157, '59', '14924221348539.jpg', '', '0'),
(158, '59', '14924221341497.jpg', '', '0'),
(159, '60', '14924222389179.jpg', '', '0'),
(160, '60', '14924222385973.jpg', '', '0'),
(161, '60', '14924222389435.jpg', '', '0'),
(162, '61', '14924223322547.jpg', '', '0'),
(163, '61', '14924223339828.jpg', '', '0'),
(164, '61', '14924223332005.jpg', '', '0'),
(165, '62', '1492422419645.jpg', '', '0'),
(166, '62', '1492422419149.jpg', '', '0'),
(167, '62', '14924224201497.jpg', '', '0'),
(168, '64', '14924226343293.jpg', '', '0'),
(169, '64', '14924226341997.jpg', '', '0'),
(170, '64', '1492422634568.jpg', '', '0'),
(171, '63', '14924227377732.jpg', '', '0'),
(172, '63', '14924227375545.jpg', '', '0'),
(173, '63', '14924227373234.jpg', '', '0'),
(174, '65', '14924227575610.jpg', '', '0'),
(175, '65', '14924227574415.jpg', '', '0'),
(176, '65', '14924227577155.jpg', '', '0'),
(177, '66', '14924228275997.jpg', '', '0'),
(178, '66', '14924228273469.jpg', '', '0'),
(179, '66', '14924228273411.jpg', '', '0'),
(180, '67', '14924230763151.jpg', '', '0'),
(181, '67', '14924230769062.jpg', '', '0'),
(182, '67', '14924230763199.jpg', '', '0'),
(183, '68', '14924231824518.jpg', '', '0'),
(184, '68', '14924231828086.jpg', '', '0'),
(185, '68', '14924231836754.jpg', '', '0'),
(186, '69', '14924236927130.jpg', '', '0'),
(187, '69', '14924236939200.jpg', '', '0'),
(188, '69', '14924236936483.jpg', '', '0'),
(189, '70', '14924237807676.jpg', '', '0'),
(190, '70', '14924237818174.jpg', '', '0'),
(191, '70', '14924237813588.jpg', '', '0'),
(192, '71', '14924239922946.jpg', '', '0'),
(193, '71', '14924239927836.jpg', '', '0'),
(194, '71', '14924239928729.jpg', '', '0'),
(195, '72', '14924240784754.jpg', '', '0'),
(196, '72', '14924240788140.jpg', '', '0'),
(197, '72', '14924240799617.jpg', '', '0'),
(198, '73', '14924241805984.jpg', '', '0'),
(199, '73', '14924241806135.jpg', '', '0'),
(200, '73', '14924241806753.jpg', '', '0'),
(201, '74', '14924242653956.jpg', '', '0'),
(202, '74', '14924242662125.jpg', '', '0'),
(203, '74', '14924242666167.jpg', '', '0'),
(204, '75', '14924243994591.jpg', '', '0'),
(205, '75', '14924243992442.jpg', '', '0'),
(206, '75', '14924243999881.jpg', '', '0'),
(207, '76', '14924244988105.jpg', '', '0'),
(208, '76', '14924244985010.jpg', '', '0'),
(209, '76', '14924244988705.jpg', '', '0'),
(210, '77', '14924246037760.jpg', '', '0'),
(211, '77', '14924246035357.jpg', '', '0'),
(212, '77', '14924246035120.jpg', '', '0'),
(213, '78', '1492424708720.jpg', '', '0'),
(214, '78', '14924247085140.jpg', '', '0'),
(215, '78', '14924247084940.jpg', '', '0'),
(216, '79', '1492425121411.jpg', '', '0'),
(217, '79', '14924251218475.jpg', '', '0'),
(218, '79', '14924251214824.jpg', '', '0'),
(219, '80', '14924252138470.jpg', '', '0'),
(220, '80', '14924252136792.jpg', '', '0'),
(221, '80', '14924252144341.jpg', '', '0'),
(222, '81', '14924253296684.jpg', '', '0'),
(223, '81', '14924253297632.jpg', '', '0'),
(224, '81', '14924253292011.jpg', '', '0'),
(225, '10', '14924267689361.jpg', '', '0'),
(226, '3', '14924296276615.jpg', '', '0'),
(227, '2', '14924296522969.jpg', '', '0'),
(228, '4', '14924296715845.jpg', '', '0'),
(229, '5', '14924296892647.jpg', '', '0'),
(230, '6', '14924297041091.jpeg', '', '0'),
(231, '7', '14924297189253.jpg', '', '0'),
(232, '8', '14924297336447.jpg', '', '0'),
(233, '9', '14924297474110.jpg', '', '0'),
(234, '11', '14924297683870.jpg', '', '0'),
(235, '12', '14924297844748.jpg', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_review`
--

CREATE TABLE IF NOT EXISTS `tbl_review` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `pro_id` int(90) NOT NULL,
  `comment_date` date NOT NULL,
  `user_id` int(90) NOT NULL,
  `comment` text NOT NULL,
  `start_rating` varchar(23) NOT NULL,
  `status` varchar(233) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_size`
--

CREATE TABLE IF NOT EXISTS `tbl_size` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `category_id` int(90) NOT NULL,
  `size` varchar(250) NOT NULL,
  `status` varchar(244) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_size`
--

INSERT INTO `tbl_size` (`id`, `category_id`, `size`, `status`) VALUES
(9, 2, 'M', '1'),
(10, 3, 'XL', '1'),
(11, 3, 'XXL', '1'),
(12, 1, 'S', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wishlist`
--

CREATE TABLE IF NOT EXISTS `tbl_wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `actiondate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_wishlist`
--

INSERT INTO `tbl_wishlist` (`id`, `user_id`, `pro_id`, `actiondate`) VALUES
(1, 34, 58, '2017-04-25'),
(2, 34, 48, '2017-04-25'),
(4, 35, 2, '2017-04-26'),
(6, 36, 3, '2017-04-26'),
(7, 35, 11, '2017-04-26'),
(8, 35, 3, '2017-04-26'),
(9, 35, 41, '2017-04-26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_colorproduct`
--

CREATE TABLE IF NOT EXISTS `tb_colorproduct` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `pro_id` int(90) NOT NULL,
  `color_id` int(90) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `tb_colorproduct`
--

INSERT INTO `tb_colorproduct` (`id`, `pro_id`, `color_id`) VALUES
(14, 5, 4),
(42, 3, 3),
(43, 3, 4),
(49, 2, 3),
(50, 2, 6),
(51, 1, 3),
(52, 1, 5),
(53, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tb_sizeproduct`
--

CREATE TABLE IF NOT EXISTS `tb_sizeproduct` (
  `id` int(90) NOT NULL AUTO_INCREMENT,
  `size_id` int(90) NOT NULL,
  `pro_id` int(90) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `tb_sizeproduct`
--

INSERT INTO `tb_sizeproduct` (`id`, `size_id`, `pro_id`) VALUES
(13, 9, 5),
(14, 11, 5),
(40, 9, 3),
(41, 10, 3),
(46, 9, 2),
(47, 12, 2),
(48, 9, 1),
(49, 11, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
