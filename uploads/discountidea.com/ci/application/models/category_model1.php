<?php 
class Category_model extends CI_Model
{
	public function selectAllCategory()
	{
		$data=$this->db->get("tbl_category");
		return $data->result();
	}
	public function selectCategoryForHome()
	{
		$this->db->where('display_on_home', 1);
		$data=$this->db->get("tbl_category");
		return $data->result();
	}
	public function updateBycategory($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update("tbl_category",$data);	
	}
	public function selectCategoryByID($id)
	{
        $this->db->where('id',$id);
		$data = $this->db->get("tbl_category");
		return $data->result();	
	}
	public function insertCategory($data)
	{
		$this->db->insert("tbl_category",$data);
	}
	public function listallcategory(){
		$this->db->where("status",1);
		$data=$this->db->get("tbl_category");
		return $data->result();
	}
	public function deletecategory($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_category');
	}
	
	public function selectAllParentCategory()
	{
		$this->db->where('parent_id',0);
		$this->db->where("status",1);
		$data=$this->db->get("tbl_category");
		return $data->result();
	}
	
	public function selectAllCategoryParentID($id=0)
	{
		$this->db->where('parent_id',$id);
		$this->db->where("status",1);
		$data=$this->db->get("tbl_category");
		return $data->result();
	}
	
	// category images 
	public function insertCategoryImages($data)
	{
		$this->db->insert("tbl_category_image",$data);
	}
	 public function selectAllCategoryImagesByid($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->get("tbl_category_image");
		return $data->result();
	}
    public function selectAllCategoryImages($id)
	{
		$this->db->where('category_id',$id);
		$data=$this->db->get("tbl_category_image");
		return $data->result();
	}
	public function deletecategoryimagesbyid($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_category_image');
	}
	function getNavigation($cid)
	{
		$html ="";
		$this->db->where("status",1);
		$this->db->where("parent_id",$cid);
		$data=$this->db->get("tbl_category");
		$categorydata = $data->result();
		if(count($categorydata)>0)
		{
			$html .= '<div class="wrap-popup column2">';
            $html .= '<div class="popup">';
            $html .= '<div class="row">';
			foreach($categorydata as $category)
			{
				$html .= '<div class="col-sm-6">';
				$html .= "<h3><a href='".base_url('products/'.slugurl($category->title)).'/'.encodeurlval($category->id)."'>".$category->title."</a></h3>";
				$child_nav = $this->countChildNav($category->id);
				if(count($child_nav)>0)
				{
					$html .= '<ul class="nav">';
                  	foreach($child_nav as $child_category)
                    {
						$html .=  '<li>';
						$html .= "<a href='".base_url('products/'.slugurl($child_category->title)).'/'.encodeurlval($child_category->id)."'>".$child_category->title."</a>";
						//$html .=  $child_category->title;
						$html .=  '</li>';
					}  					
					$html .= '</ul>';
				}
				$html .= '<br></div>';
			}
			$html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
		}
		return $html;
	}
	function countChildNav($cid)
	{
		$this->db->where("status",1);
		$this->db->where("parent_id",$cid);
		$data=$this->db->get("tbl_category");
		$categorydata = $data->result();
		return $categorydata;		
	}
	
	function getAdminMenus($cid)
	{
		$html ="";
		$this->db->where("status",1);
		$this->db->where("parent_id",$cid);
		$data=$this->db->get("tbl_category");
		$categorydata = $data->result();
		if(count($categorydata)>0)
		{
			$html .= '<ul>';
			foreach($categorydata as $category)
			{
				$html .= '<li>';
				$html .= "<div class='mytree' ><a>".$category->title."</a></div>";				
				$html .= '<span style="float: right; margin-top: -3%;"> ';
				$html .='<a class="btn btn-warning btn-xs" href="'.site_url('index.php/categorycontroller/manageimage/'.$category->id.'').'" title="Manage category Image"><i class="fa fa-picture-o" aria-hidden="true"></i></a>';
				$html .='<a class="btn btn-primary btn-xs" href="'.site_url('index.php/categorycontroller/edit/'.$category->id.'').'" title="edit this record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
				$html .='<a class="btn btn-info btn-xs" href="'.site_url('index.php/categorycontroller/deletecategory/'.$category->id.'').'" title="delete this record" onclick="return confirm('."Are you sure you want to delete this item?".');"><i class="fa fa-trash" aria-hidden="true"></i></a>';
				$html .= "</span>";
				
				$child_nav = $this->countChildNav($category->id);
				if(count($child_nav)>0)
				{
					$html .= '<ul>';
                  	foreach($child_nav as $child_category)
                    {
						$html .=  '<li>';
						$html .=  "<div class='mytree'><a>".$child_category->title."</a></div>";
						$html .= '<span style="float: right; margin-top: -3%;"> ';
						$html .='<a class="btn btn-warning btn-xs" href="'.site_url('index.php/categorycontroller/manageimage/'.$child_category->id.'').'" title="Manage category Image"><i class="fa fa-picture-o" aria-hidden="true"></i></a>';
						$html .='<a class="btn btn-primary btn-xs" href="'.site_url('index.php/categorycontroller/edit/'.$child_category->id.'').'" title="edit this record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
						$html .='<a class="btn btn-info btn-xs" href="'.site_url('index.php/categorycontroller/deletecategory/'.$child_category->id.'').'" title="delete this record" onclick="return confirm('."Are you sure you want to delete this item?".');"><i class="fa fa-trash" aria-hidden="true"></i></a>';
						$html .= "</span>";
						
						$html .=  $this->getAdminMenus($category->id);
						$html .=  '</li>';
					}  					
					$html .= '</ul>';
				}
				$html .= '</li>';
			}
			$html .= '</ul>';           
		}
		return $html;
	}
	
	function getLeftSideBarFilterMenus($cid)
	{
		$html ="";		  
	       $html .= '<ul class="children">';
		   $child_nav = $this->countChildNav($cid);
		   foreach($child_nav as $child_category)
           {
			    $child_nav2 = $this->countChildNav($child_category->id); 
				$li_class = (count($child_nav2)>0)?'cat-parent':'';
				$html .=  '<li class="cat-item '.$li_class.'">';
				$html .=  "<a href='".base_url('products/'.slugurl($child_category->title)).'/'.encodeurlval($child_category->id)."'>".$child_category->title."</a>";
				$html .=  $this->category_model->getLeftSideBarFilterMenus($child_category->id);
				$html .= '</li>'; 
		   }
		   $html .= '</ul>';  
		return $html;
	}
	
}	