<?php
class offer_model extends CI_Model{
	public function insert($data){
		 $data=$this->db->insert("tbl_offer",$data);
		 return $data;
	}
	public function listing(){
		$data=$this->db->get("tbl_offer");
		return $data->result();
	}
	public function delete($id){
		$this->db->where("id",$id);
		return $this->db->delete("tbl_offer");
	}
	public function update($id,$data){
		$this->db->where("id",$id);
		return  $this->db->update("tbl_offer",$data);
	
	} 
	public function offerById($id){
		$this->db->where("id",$id);
		$data=$this->db->get("tbl_offer");
		return $data->result();
	}

	public function selectCouponCode($code){
		$this->db->where('couponcode',$code);
		$data=$this->db->get("tbl_offer");
		return $data->result();
	}
}
?>