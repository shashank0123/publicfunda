<?php
class Color_Model extends CI_Model
{

	public function insertcolor($data)

	{
		$qry=$this->db->insert('tbl_color',$data);
		return $qry;
	
	}
	public function selectcolor()
	{

		$data=$this->db->get('tbl_color');
		return $data->result();
	}

	public function selectcolorby_id($id)
	{
		$this->db->where('id',$id);
		$data=$this->db->get('tbl_color');
		return $data->result();
	}
	public function updatecolor($id,$data)
	{

		$this->db->where('id',$id);
		$data=$this->db->update('tbl_color',$data);
		return $data;

	}
	public function deletecolor_id($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tbl_color');

	}
	public function selectcolorbyproid($id)
	{
		
		$this->db->where('pro_id',$id);
		$query = $this->db->get('tb_colorproduct');
		//print_r($qry);
		 //$res=$query->num_rows();
		 $data=array();
		 foreach($query->result() as $val)
		 {
			 array_push($data,$val->color_id);			 
			 
		 }
		 return $data;
		 
	}
	
   public function selectProductColorByColorId($id)
   {
	   $this->db->where('color_id',$id);
		$data=$this->db->get('tb_colorproduct');
		return $data->result();
   }
   
   public function getAvailableColors($id)
   {	  
	   $query = $this->db->query("select S.*,C.color_id,C.pro_id as pid from tbl_color S left join tb_colorproduct C on S.id=C.color_id where C.pro_id=$id");
	   return $query->result();
   }
}
?>