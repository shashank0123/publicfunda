<?php
class Newsletter_model extends CI_Model
{
	function selectAllNewsletter()
	{
		$data= $this->db->get("tbl_newsletter");
		return $data->result();		
	}
	
	function selectAllActiveNewsletter()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_newsletter");
		return $data->result();		
	}
	
	function selectNewsletterById($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_newsletter');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_newsletter', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_newsletter',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_newsletter');
	}
}
?>