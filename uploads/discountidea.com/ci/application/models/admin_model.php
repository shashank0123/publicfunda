<?php 
class Admin_model extends CI_Model
{
	var $admin_table = 'tbl_admin';
	public function adminLogin($username,$password)
	{
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByEmail($email)
	{
		$this->db->where('email',$email);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByToken($key)
	{
		$this->db->where('forgot_password',$key);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function updateAdminById($data,$id)
	{
		$this->db->where('id', $id);
		return $this->db->update('table_user',$data);	
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_user');
	}
	
	public function insertData($data)
	{
		$returnData = $this->db->insert('table_user',$data);
		return  $returnData;
	}
	
	public function updateData($data)
	{
		$returnData = $this->db->update($this->admin_table,$data);
		return $returnData;
	}
	
	public function getAllUsersvender()
	{
		$this->db->where('vender_type','1');
		$data = $this->db->get('table_user');
		//print_r($data);
		return $data->result();
	}
	
	public function selectUserByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get('table_user');
		return $data->result();
	}
	
	public function userLogin($id,$password)
	{
		$this->db->where('id',$id);
		$this->db->where('password',$password);
		//$this->db->where('status',1);
		$data = $this->db->get($this->admin_table);
		return $data->result();
	}
	public function selectvendername()
	{
		$this->db->where('vender_type','1');
		$data=$this->db->get('table_user');
		return $data->result();		
	}
}