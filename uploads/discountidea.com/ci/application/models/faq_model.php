<?php
class Faq_model extends CI_Model
{
	function selectAllFaq()
	{
		$data= $this->db->get("tbl_faq");
		return $data->result();		
	}
	
	function selectAllActiveFaq()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_faq");
		return $data->result();		
	}
	
	function selectFaqById($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_faq');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_faq', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_faq',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_faq');
	}
}
?>