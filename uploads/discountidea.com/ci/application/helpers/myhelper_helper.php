<?php
function createUniqueFile($file)
{
	$data = time().'_'.strtolower($file);
	return $data;
}

function slugurl($title, $separator = '-')
{
    // convert String to Utf-8 Ascii
    $title = iconv(mb_detect_encoding($title, mb_detect_order(), true), "UTF-8", $title);
 
    // Convert all dashes/underscores into separator
    $flip = $separator == '-' ? '_' : '-';
 
    $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);
 
    // Remove all characters that are not the separator, letters, numbers, or whitespace.
    $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($title));
 
    // Replace all separator characters and whitespace by a single separator
    $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);
 
    return trim($title, $separator);
}

function encodeurlval($val)
{
	return urlencode(base64_encode($val));
}

function decodeurlval($val)
{
	return base64_decode(urldecode($val));
}
?>