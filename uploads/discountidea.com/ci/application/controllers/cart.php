<?php 
class cart extends CI_Controller
{
	function index()
	{
		$data['cart_data'] = $this->cart->contents();
		$this->load->view('front/cart',$data);
	}
	
	function addtocart()
	{
		if(isset($_POST['addtocartbutton']))
		{
			$product_id = $this->input->post('pid');	
			$qty = $this->input->post('qty');
			if(isset($_POST['size_id'])){ $size = $this->input->post('size_id'); }else{ $size=0; }	
            if(isset($_POST['color_id'])){ $color = $this->input->post('color_id'); }else{ $color=0; }	
			$product = $this->product_model->selectProductById($product_id);
			if($product[0]->spacel_price!="")
			{
				$price = $product[0]->spacel_price;
			}
			else
			{
				$price = $product[0]->price;
			}	
			$data = array('id'=> $product[0]->id, 'qty'=> $qty, 'size'=> $size, 'color'=> $color, 'price'=> $price, 'name' => $product[0]->name,);
			$this->cart->insert($data);
			$this->session->set_flashdata("message","<div class='alert alert-success'><strong>".$product[0]->name."</strong> successfully added to your cart.</div>");
			redirect('cart/');
		}
	}
		
	function remove($id)
	{
		$args = func_get_args($id);
		$data= array('rowid' =>$args[0], 'qty' =>0);
		$this->cart->update($data);
		$this->session->set_flashdata("message","<div class='alert alert-success'>item successfully removed to your cart.</div>");
		redirect('cart/');
	}
	function update()
	{
		if(isset($_POST['updatecart']))
		{
			if(count($_POST['rowid'])>0)
			{
				foreach($_POST['rowid'] as $key => $value)
				{
					if($_POST['qty'][$key]!="0")
					{	
						$data= array('rowid' =>$_POST['rowid'][$key], 'qty' =>$_POST['qty'][$key]);
						$this->cart->update($data);
						$this->session->set_flashdata("message","<div class='alert alert-success'>Cart has been successfully updated.</div>");
					}	
				}	
			}
			redirect('cart/');
		}
		else
		{
			redirect('');
		}		
	}
			
	function emptycart($data="")
	{
		$this->session->unset_userdata('SUBSIDOLD');
		$this->cart->destroy();
		redirect('cart/');
	}
	
	function checkout()
	{
		$user_id = $this->user_model->getLoginUserVar('USER_ID');
		$cart_data = $this->cart->contents();
		if(isset($_POST['checkout']))
		{
			$order['user_id'] = $this->session->userdata('id');
			$order['payment_method']=$this->input->post('paymentmethod');
			$order['ord_date']=date('Y-m-d H:i:s');
			$order['ord_time']=time();
			$order['total_amount']=$this->cart->total();
			$order['ip_address']=$this->input->post('ipaddress');
			$order=$this->checkout_model->insertorder($order);
			$ordr_id=mysql_insert_id();
			if(!empty($ordr_id))
			{
				foreach($cart_data as $items)
				{
					$item['order_id']=$ordr_id;
					$item['pro_id']=$items['id'];
					$item['user_id']=$this->session->userdata('id');
					$item['pro_name']=$items['name'];
					$item['unit_price']=$items['price'];  
					$item['total_amount']=$items['subtotal'];	//$this->cart->total();
					$item['qty']=$items['qty'];
					$item['color']=$items['color'];
					$item['size']=$items['size'];					
					$item['order_date']= date('Y-m-d');
					
					$this->checkout_model->inseritem($item);
					
				}
		$data['ord_id']=$ordr_id;
		$data['user_id']=$this->session->userdata('id');
		$data['country']=$this->input->post('countryb');
		$data['firstname']=$this->input->post('firstnameb');
		$data['lastname']=$this->input->post('lastnameb');
		$data['address']=$this->input->post('addb');
		$data['city']=$this->input->post('cityb');
		$data['state']=$this->input->post('stateb');
		$data['pincode']=$this->input->post('pincodeb');
		$data['email']=$this->input->post('emailb');
		$data['mobile']=$this->input->post('phoneb');
		$data['add_date']=date('Y-m-d H:i:s');
		$billing=$this->checkout_model->insert_data($data);
		$id=mysql_insert_id();
		if(!empty($id))
		{
			$data1['ord_id']=$ordr_id;
			$data1['user_id']=$this->session->userdata('id');
			$data1['country']=$this->input->post('countrys');
		$data1['firstname']=$this->input->post('firstnames');
		$data1['lastname']=$this->input->post('lastnames');
		$data1['email']=$this->input->post('emails');
		$data1['address']=$this->input->post('adds');
		//$data1['secondaddress']=$this->input->post('secondaddress1');
		$data1['city']=$this->input->post('citys');
		$data1['state']=$this->input->post('states');
		$data1['pincode']=$this->input->post('pincodes');
		$data1['add_date']=date('Y-m-d H:i:s');
		$this->checkout_model->insertshipping($data1);
			
		}
				
		}
			$this->emptycart();
		}
		$data['LOGIN_USER'] = $this->user_model->selectuserby_id($user_id);
		$data['cart_data'] = $cart_data;
		$this->load->view('front/checkout',$data);
	}                             
	
} 
?>