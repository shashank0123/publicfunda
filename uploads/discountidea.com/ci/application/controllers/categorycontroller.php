<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorycontroller extends CI_Controller {

	public function add()
	{
		$args=func_get_args();
        if(isset($_POST["add"]))
		{
			
									
			//$data["image"] = $file;
			$data['parent_id']=$this->input->post('parent_id');		
			$data['meta_title']=$this->input->post('metatile');
			$data['meta_keyword']=$this->input->post('metakeyword');
			$data['meta_description']=$this->input->post('metadescription');
			$data['display_on_home']=$this->input->post('display_on_home');
			$data['title']=$this->input->post('name');
			$data["createdate"]= time();
			$data['status']=$this->input->post('status');
			
			$filters_id = $this->input->post('filters_id');
			if(!empty($filters_id))
			{				
				$data['filters_id'] = implode(",",$filters_id);	
			}
				
			$this->category_model->insertCategory($data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Add Category successsully</div>'); 
			redirect("index.php/categorycontroller/listing/"); 
		}
		$this->load->view('admin/category/add');
	}
	public function listing()
	{
		$data["LISTCATEGORY"]=$this->category_model->selectAllCategory();
		$this->load->view("admin/category/listing",$data);
	}
	public function edit()
	{
		$args=func_get_args();
		if(isset($_POST["edit"]))
		{
			
			
						
			//$data["image"] = $file;
			$data['meta_title']=$this->input->post('metatitle');
			$data['meta_keyword']=$this->input->post('metakeyword');
			$data['meta_description']=$this->input->post('metadescription');
			$data['parent_id']=$this->input->post('parent_id');
			$data['display_on_home']=$this->input->post('display_on_home');
			$data['title']=$this->input->post('name');
			$data["createdate"]= time();
			$data['status']=$this->input->post('status');
			$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully update planes</div>'); 
						
			$filters_id = $this->input->post('filters_id');
			if(!empty($filters_id))
			{				
				$data['filters_id'] = implode(",",$filters_id);	
			}
			
			$this->category_model->updateBycategory($args[0],$data);
			
			redirect('index.php/categorycontroller/listing/');  
		}
		$data["EDITCATEGORY"]=$this->category_model->selectCategoryByID($args[0]);
		$this->load->view("admin/category/edit",$data);
	}
	public function deletecategory($id)
	{
		$args = func_get_args();
		$this->category_model->deletecategory($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully Delete Category</div>'); 
		redirect('index.php/categorycontroller/listing/'.$id);  
    }
	
	public function manageimage()
    {
		$args = func_get_args();
		if(count($args)>0)
		{
			if(isset($_POST['uploadfiles']))
			{
				if(count($_FILES)>0)
				{
					$filesCount = count($_FILES['image']['name']);
					for($i = 0; $i < $filesCount; $i++)
					{
						$file = time().'_'.$_FILES['image']['name'][$i];
						$file_tmp = $_FILES['image']['tmp_name'][$i];
						//$image_title=$_POST["image_title"][$i];
						$path = 'uploads/category/'.$file;
						move_uploaded_file($file_tmp,$path);
						$datasd["category_id"]=$args[0];						
						$datasd["image"] = $file;
						$this->category_model->insertCategoryImages($datasd);
					}
				}
				
				$this->session->set_flashdata('message','<div class="alert alert-success">file has been successfully uploaded</div>'); 
				redirect('index.php/categorycontroller/manageimage/'.$args[0]);					
			}	
			
			$data["LISTCATEGORYIMAGE"]=$this->category_model->selectAllCategoryImages($args[0]);
			$this->load->view("admin/category/manage-images",$data);			
		}
		else
		{
			redirect('index.php/categorycontroller/listing');
		}	
	}
		
	public function deletecategoryimages($id)
	{
		$args = func_get_args();
		$img_data = $this->category_model->selectAllCategoryImagesByid($args[0]);
		if(count($img_data)>0)
		{
			$image = 'uploads/category/'.$img_data[0]->image;
			unlink($image);
		}	
		$this->category_model->deletecategoryimagesbyid($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">file successsully Deleted  </div>'); 
		redirect('index.php/categorycontroller/manageimage/'.$args[1]);	
    }	
	
}
