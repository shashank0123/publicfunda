<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order extends CI_Controller 
{
	function listing()
	{		
		$data['orderdetail']= $this->orderdetail_model->selectorder();
		$this->load->view('admin/order/listing',$data);
	}
	
	function details()
	{		
		$args=func_get_args();
		$data['order']= $this->shoppingdetail_model->selectorder($args[0]);
		$this->load->view('admin/order/order-details',$data);
	}
		
	function invoice()
	{		
		$args=func_get_args();
		$data['shpping']= $this->shoppingdetail_model->selectorder($args[0]);
		$this->load->view('admin/order/invoice',$data);
	}	
}