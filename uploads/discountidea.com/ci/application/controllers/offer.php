<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class offer extends CI_Controller {
	public function add(){
		if(isset($_POST["add"])){
			$data["title"]=$this->input->post("title");
			$data["couponcode"]=$this->input->post("couponcode");
			$data["discount"]=$this->input->post("discount");
			$data["status"]=$this->input->post("status");
			$this->offer_model->insert($data);
			redirect("offer/listing/");
		}
		$this->load->view("admin/offer/add");
	}
	public function listing(){
		$data["LISTING"]=$this->offer_model->listing();
		$this->load->view("admin/offer/listing",$data);
	}
	public function edit(){
		$args=func_get_args();
		if(isset($_POST["edit"])){
			$data["title"]=$this->input->post("title");
			$data["couponcode"]=$this->input->post("couponcode");
			$data["discount"]=$this->input->post("discount");
			//$data["status"]=$this->input->post("status");
			$this->offer_model->update($args[0],$data);
			redirect("offer/listing/");
		}
		$data["EDITOFFER"]=$this->offer_model->offerById($args[0]);
		$this->load->view("admin/offer/edit",$data);
	}
	public function delete($id){
        $this->offer_model->delete($id);
        redirect("offer/listing/".$id);
	}
}