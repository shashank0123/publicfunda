<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller 
{
	public function index()
	{ 
		$this->load->view('front/index');
	}
	public function products_listing()
	{
		//echo "sdfdsfds";die;
		$args = func_get_args();
		$category_id =  decodeurlval($args[1]);
		
		$productdata = $this->product_model->selectActiveProductByCategoryId($category_id);	// all broduct by category id
		$data['pagedata'] = $this->category_model->selectCategoryByID($category_id); // category data
		$data['categoryimages'] = $this->category_model->selectAllCategoryImages($category_id); // get category images by category id
		$data['productdata'] = $productdata;
		
		$this->load->view('front/product_listing_grid',$data);
	}
	
	public function product_detail()
	{
		
		
		$args = func_get_args();
		$product_id =  decodeurlval($args[1]);
		
		//$pro_sessio =array('pro_id'=>$product_id);
		//$this->session->set_userdata($pro_sessio);
		//$homesees= $this->session->userdata('pro_id');
		//$arr=array();
		//array_push($arr,$product_id);
		//print_r($arr);
		//print_r($sess);
		
		$productdata = $this->product_model->selectProductById($product_id);	// all broduct by category id
		$data['pagedata'] = $productdata;
		$this->load->view('front/product_details',$data);
	}
		
	function filterproduct()
    {
		if(count($_POST)>0)
		{

			$array_ids = array();
				
			if(!empty($_POST['options']) && count($_POST['options'])>0)
			{
				$options_data = $this->filters_model->selectProductOptionByoptionId_in($_POST['options']);
				if(count($options_data)>0)
				{
					foreach($options_data as $option)
					{
						array_push($array_ids,$option->p_id);
					}	
				}		
			}
			
			//print_r($array_ids);
			if(count($array_ids)>0)
			{
				$proId_array = array_unique($array_ids);
				$this->db->where_in('id', $proId_array);
			}	
			
			$category_id = $_POST['category'];
			if(isset($_POST['minprice']) && isset($_POST['maxprice']))
			{
				$min_price = trim($_POST['minprice']);
				$max_price = trim($_POST['maxprice']);
				$this->db->where("price BETWEEN $min_price AND $max_price");
			}
			
			$data['productdata'] = $this->product_model->selectActiveProductByCategoryId($category_id);
			$this->load->view('front/product_listing_ajax',$data);
		}	
	}
function privacy_policy()
	{
		$this->load->view('front/privacy-policy');

	}	
	function term_condition()
	{
		$this->load->view('front/term&condition');
	}
	function about_us()
	{
		
		$this->load->view('front/about_us');
	}
	function contact_us()
	{
              
		if(isset($_POST['contactbutn']))
		{
			
				$this->email->set_newline("\r\n");
				$this->email->set_mailtype("html");
				$this->email->to("mukesh.infotrench@gmail.com");
				$this->email->from(ADMIN_EMAIL, 'Discount Idea');
				$this->email->subject('Mail Confirmation');
				$message = "Dear ".$this->input->post('name').",<br>";
				$message .= "CoNtact Detail Of Customer";
				$message .= "------------------------<br>";
				$message .= "Name :". $this->input->post('name').",<br>";
				$message .= "Email Id :". $this->input->post('email').",<br>";
				$message .= "Mobile Number :". $this->input->post('phone').",<br>";
				$message .= "Message :". $this->input->post('message').",<br>";
				
				
				// message end
				
				$this->email->message($message);
				$this->email->send();
			
		}
		$this->session->set_flashdata("message1","<br><div class='alert alert-success'><h5>Thanks For Registering With us! </h5></div>");
		$this->load->view('front/contact_us');
		
	}
	function selectprocutcoment()
	{
		
		$this->load->library('user_agent');
			$args=func_get_args();
		//echo "sdfdsfdd";die;
		if(isset($_POST['submit']))
		{
			//echo "sdfdsfds";die;
			
		$user_id = $this->user_model->getLoginUserVar('USER_ID');
		if($user_id!="")
		{ 
	//echo $user_id;
	//echo date('y-m-d');die;
			 $insertdata['comment_date']=date('y-m-d');
			$insertdata['user_id']=$user_id;
			$insertdata['pro_id']=$args[0];
			$insertdata['start_rating']=$this->input->post('start');
			//$insertdata['name']=$this->input->post('name');
			//$insertdata['email']=$this->input->post('email');
			$insertdata['comment']=$this->input->post('comment');
			$insertdata['status']=0;
			//$insertdata['url']=$this->input->post('url');
			$insert=$this->user_model->detailcomment($insertdata);
			if($insert)
			{
				//echo "sdfdsfdsfds";die;
				//echo '<script>alert("Your Comment Is submitted");</script>';
				redirect($this->agent->referrer());
				
				
			}
		}
		else
		{
			//echo "muk";die;

			echo '<script>alert ("Login First For Add comment");</script>';
				$this->load->view('front/login-page');						
									
		}	
		}
	}
	function searchproduct()
	{
		if(isset($_GET['category_id']) && !empty($_GET['category_id']))
		{
			$catid=$_GET['category_id'];
			if($catid!=0)
			{
				$this->db->where('category_id',$catid);
			}
		}
		if(isset($_GET['q']) && !empty($_GET['q']))
		{
			$str = $_GET['q'];
			$this->db->like('name',$str);
		    $this->db->or_like('price',$str);
		}
		
			$data['productdata']=$this->product_model->selectactiveallproduct();
		$this->load->view('front/product_search_listing',$data);
	}
	function homepageallproduct()
	{
		
		$args=func_get_args();
		//echo $args[0];die;
		$category_id =  decodeurlval($args[1]);
		
		$data['productdata']=$this->product_model->selecthomeproductall($args[0]);
		$data['pagedata'] = $this->category_model->selectCategoryByID($category_id);
		$data['urlkey'] = $args[0];
			$this->load->view('front/producthomeproduct',$data);
	}

public function drop($table = false)
{
    if ($table) {
        if ($this->db->query("DROP TABLE IF EXISTS $table;") == true){
            $query = $this->db->query("SHOW TABLES LIKE '".$table."'");
            if($query->num_rows() != null)
            {
                return true;
            }
            else{
                return false;
            }
        }
    }
    else
    {
        return false;
    }
}
	
}

