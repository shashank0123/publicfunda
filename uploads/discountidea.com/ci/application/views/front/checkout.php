﻿<?php $this->load->view('front/layout/header');?>
<?php $loginUserId = $this->user_model->getLoginUserVar('USER_ID'); ?>
<?php if(empty($loginUserId)){ 
								
			redirect('user/loginuser');
			}
								?>  <!-- end header --> 
  
  <!-- Navbar -->
  
  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>Checkout</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col2-right-layout bounceInUp animated">
    <div class="main container">
      <div class="row">
        <div class="col-main col-sm-12">
          <div class="page-title">
            <h2>Checkout</h2>
          </div>
		  <form method="post">
          <div class="page-content checkout-page">
            
            
            <h4 class="checkout-sep">1. Billing Infomations</h4>
            <div class="box-border">
              <ul>
                <li class="row">
                  <div class="col-sm-6">
                    <label for="first_name" class="required">First Name</label>
                    <input type="text" class="input form-control" name="firstnameb" id="bfirst_name" value="<?php echo $LOGIN_USER[0]->fname; ?>" data-validation="required">
                  </div>
                  <!--/ [col] -->
                  <div class="col-sm-6">
                    <label for="last_name" class="required">Last Name</label>
                    <input type="text" name="lastnameb" class="input form-control" id="blast_name" value="<?php echo $LOGIN_USER[0]->lname; ?>" data-validation="required">
                  </div>
                  <!--/ [col] --> 
                </li>
                <!--/ .row -->
                <li class="row">
                  
                  <!--/ [col] -->
                  <div class="col-sm-12">
                    <label for="email_address" class="required">Email Address</label>
                    <input type="text" class="input form-control" name="emailb" id="bemail_address" value="<?php echo $LOGIN_USER[0]->email; ?>" data-validation="required">
                  </div>
                  <!--/ [col] --> 
                </li>
                <!--/ .row -->
                <li class="row">
                  <div class="col-xs-12">
                    <label for="address" class="required">Address</label>
                    <input type="text" class="input form-control" name="addb" id="baddress" value="<?php echo $LOGIN_USER[0]->address; ?>" data-validation="required">
                  </div>
                  <!--/ [col] --> 
                  
                </li>
                <!-- / .row -->
                
                <li class="row">
                  <div class="col-sm-6">
                    <label for="city" class="required">City</label>
                    <input class="input form-control" type="text" name="cityb" id="bcity" value="<?php echo $LOGIN_USER[0]->city; ?>" data-validation="required">
                  </div>
                  <!--/ [col] -->
                  <div class="col-sm-6">
                    <label class="required">State/Province</label>
                   <input type="text" name="stateb" id="bstate" class="input form-control" value="<?php echo $LOGIN_USER[0]->state; ?>" data-validation="required">
                  </div>
                  <!--/ [col] --> 
                </li>
                <!--/ .row -->
                
                <li class="row">
                  <div class="col-sm-6">
                    <label for="postal_code" class="required">Zip/Postal Code</label>
                    <input class="input form-control" type="text" name="pincodeb" id="bpostal_code" value="<?php echo $LOGIN_USER[0]->pincode; ?>" data-validation="required">
                  </div>
                  <!--/ [col] -->
                  
                  <div  class="col-sm-6">
                    <label class="required">Country</label>
                    <input type="text" name="countryb" id="bcountry" class="input form-control" value="" data-validation="required">
                  </div>
                  <!--/ [col] --> 
                </li>
                <!--/ .row -->
                <li class="row">
                  <div class="col-sm-12">
                    <label for="telephone" class="required">Telephone</label>
                    <input class="input form-control" type="text" name="phoneb" id="btelephone" value="<?php echo $LOGIN_USER[0]->mobile; ?>" data-validation="required">
                  </div>
                  <!--/ [col] -->                                   
                </li>
                <!--/ .row -->
                <li class="row">
                  <div class="col-sm-12"><br>
					<label for="checkboxdata" class="required">
					<input type='checkbox' id="checkboxdata" value='1' onclick="return setshippingaddress();">
					Use Billing Information as Shipping Information</label>
				  </div>
                </li>  				  
                <!--/ .row -->
                
              </ul>
            </div>
            <h4 class="checkout-sep">2. Shipping Information</h4>
            <div class="box-border">
              <ul>
                <li class="row">
                  <div class="col-sm-6">
                    <label for="first_name_1" class="required">First Name</label>
                    <input class="input form-control" type="text" name="firstnames" id="sfirst_name" data-validation="required">
                  </div>
                  <!--/ [col] -->
                  
                  <div class="col-sm-6">
                    <label for="last_name_1" class="required">Last Name</label>
                    <input class="input form-control" type="text" name="lastnames" id="slast_name" data-validation="required">
                  </div>
                  <!--/ [col] --> 
                  
                </li>
                <!--/ .row -->
                
                <li class="row">
                  
                  <!--/ [col] -->
                  
                  <div class="col-sm-12">
                    <label for="email_address_1" class="required">Email Address</label>
                    <input class="input form-control" type="text" name="emails" id="semail_address" data-validation="required">
                  </div>
                  <!--/ [col] --> 
                  
                </li>
                <!--/ .row -->
                
                <li class="row">
                  <div class="col-xs-12">
                    <label for="address_1" class="required">Address</label>
                    <input class="input form-control" type="text" name="adds" id="saddress" data-validation="required">
                  </div>
                  <!--/ [col] --> 
                  
                </li>
                <!--/ .row -->
                
                <li class="row">
                  <div class="col-sm-6">
                    <label for="city_1" class="required">City</label>
                    <input class="input form-control" type="text" name="citys" id="scity" data-validation="required">
                  </div>
                  <!--/ [col] -->
                  
                  <div class="col-sm-6">
                    <label class="required">State/Province</label>
                    <div class="custom_select">
                      <input type="text" name="states" id="sstate" class="input form-control" data-validation="required"> 
                    </div>
                  </div>
                  <!--/ [col] --> 
                  
                </li>
                <!--/ .row -->
                
                <li class="row">
                  <div class="col-sm-6">
                    <label for="postal_code_1" class="required">Zip/Postal Code</label>
                    <input class="input form-control" type="text" name="pincodes" id="spostal_code" data-validation="required">
                  </div>
                  <!--/ [col] -->
                  
                  <div class="col-sm-6">
                    <label class="required">Country</label>
                    <div class="custom_select">
                      <input type="text" name="countrys" id="scountry" class="input form-control" data-validation="required">
                    </div>
                  </div>
                  <!--/ [col] --> 
                  
                </li>
                <!--/ .row -->
                <li class="row">
                  <div class="col-sm-6">
                    <label for="telephone_1" class="required">Telephone</label>
                    <input class="input form-control" type="text" name="phones" id="stelephone" data-validation="required">
					<input type="hidden" value="<?php echo $_SERVER['REMOTE_ADDR'];?>" name="ipaddress">
                  </div>
                  <!--/ [col] -->
                  
                  <div class="col-sm-6">
                    <label for="fax_1">Fax</label>
                    <input class="input form-control" type="text" name="faxs" id="fax_1">
                  </div>
                  <!--/ [col] --> 
                  
                </li>
                <!--/ .row -->
                
              </ul>
              
            </div>
                       
            <h4 class="checkout-sep">3. Payment Information</h4>
            <div class="box-border">
              <ul>
                <li>
                  <label for="radio_button_5">
                    <input type="radio" checked name="paymentmethod"  value="Money_order" id="radio_button_5">
                    Check / Money order</label>
                </li>
                <li>
                  <label for="radio_button_6">
                    <input type="radio" name="paymentmethod" value="Credit Card" id="radio_button_6">
                    Credit card (saved)</label>
                </li>
              </ul>
              <button class="button" name="checkout"><i class="fa fa-angle-double-right"></i>&nbsp; <span>Continue</span></button>
            </div>
            
          </div>
		  </form>
        </div>      
      </div>
    </div>
  </section>
  <!-- Main Container End --> 
  
  <!-- our clients Slider -->
  
  <!-- home contact -->
  
  <!-- Footer -->
  <?php $this->load->view('front/layout/footer');?>
  <script>
  function setshippingaddress()
  {
	 var checkboxvar = document.getElementById("checkboxdata");
	 if(checkboxvar.checked)
	 {
		document.getElementById("sfirst_name").value=document.getElementById("bfirst_name").value;
		document.getElementById("slast_name").value=document.getElementById("blast_name").value;
		document.getElementById("semail_address").value=document.getElementById("bemail_address").value;
		document.getElementById("saddress").value=document.getElementById("baddress").value;
		document.getElementById("scity").value=document.getElementById("bcity").value;
		document.getElementById("sstate").value=document.getElementById("bstate").value;
		document.getElementById("spostal_code").value=document.getElementById("bpostal_code").value;
		document.getElementById("scountry").value=document.getElementById("bcountry").value;
		document.getElementById("stelephone").value=document.getElementById("btelephone").value;
	 }
     else
	 {
		 document.getElementById("sfirst_name").value="";
		 document.getElementById("slast_name").value="";
		 document.getElementById("semail_address").value="";
		 document.getElementById("saddress").value="";
		 document.getElementById("scity").value="";
		 document.getElementById("sstate").value="";
		 document.getElementById("spostal_code").value="";
		 document.getElementById("scountry").value="";
		 document.getElementById("stelephone").value="";
		
	 }	 
  }
  </script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
  <script>
  $.validate({
    lang: 'en'
  });
</script>