<?php $this->load->view('front/layout/header');?>
<!-- flexslider CSS -->
 <!-- Main Container -->
  <section class="main-container col1-layout wow bounceInUp animated">
    <div class="main container">
      <div class="col-main">
        <div class="cart">
          <div class="page-title">
            <h2>Shopping Cart Summary</h2>
			<?php echo $this->session->flashdata("message"); ?>
          </div>
          <div class="page-content page-order">
            
            <div class="heading-counter warning">Your shopping cart contains: <span><?php echo count($cart_data); ?> Product</span> </div>
			<?php if(count($this->cart->contents())>0){ ?> 
            <div class="order-detail-content">
              <div class="table-responsive">
                <form method="post" action="<?php echo base_url('index.php/cart/update'); ?>"> 			  
                <table class="table table-bordered cart_summary">
                  <thead>
                    <tr>
                      <th class="cart_product">Product</th>
                      <th>Description</th>
                      <th>Unit price</th>
                      <th>Qty</th>
                      <th>Total</th>
                      <th  class="action"><!--<i class="fa fa-trash-o">--></i></th>
                    </tr>
                  </thead>				  
                  <tbody>
				  <?php $i=0; foreach($cart_data as $items){ $i++; ?>	
				  <?php $proimages = $this->product_model->selectProductSingleImage($items['id']); ?>
				  <?php $product = $this->product_model->selectProductById($items['id']);  ?>
				  <input type="hidden" name="rowid[]" value="<?php echo $items['rowid'] ?>" />
                    <tr>
                      <td class="cart_product">
					  <?php if(count($proimages)>0){ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>"><img src="<?php echo base_url('uploads/product/'.$proimages[0]->image); ?>" width="100" height="100"></a>
					  <?php }else{ ?>
							<a  href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>"><img  src="http://placehold.it/100x100?text=Image not found" width="100" height="100"></a>
					  <?php } ?>
					  </td>
                      <td class="cart_description">
						  <p class="product-name">
							<a href="<?php echo base_url('product/'.slugurl($product[0]->name)).'/'.encodeurlval($product[0]->id); ?>"><?php echo $product[0]->name; ?></a> 
						  </p>
						<?php if($items['color']!='0'){ ?>  
                        <small><a href="#">Color : <strong><?php echo $items['color']; ?></strong></a></small><br>
						<?php } ?>
						<?php if($items['size']!='0'){ ?>
                        <small><a href="#">Size : <strong><?php echo $items['size']; ?></strong></a></small>
						<?php } ?>
						</td>
                      <td class="price"><span><?php echo CURRENCY.$items['price']; ?></span></td>
                      <td class="qty"><input class="form-control input-sm" type="number"  name="qty[]" value="<?php echo $items['qty']; ?>" ></td>
                      <td class="price"><span><?php echo CURRENCY.$items['subtotal']; ?></span></td>
                      <td class="action"><a href="<?php echo base_url('cart/remove/'.$items['rowid']); ?>"><i class="icon-close"></i></a></td>
                    </tr>
                  <?php } ?>					
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2" rowspan="2"></td>
                      <td colspan="3">Total products (tax incl.)</td>
                      <td colspan="1"><?php echo CURRENCY.$this->cart->total(); ?> </td>
                    </tr>
                    <tr>
                      <td colspan="3"><strong>Total</strong></td>
                      <td colspan="1"><strong><?php echo CURRENCY.$this->cart->total(); ?></strong></td>
                    </tr>
                  </tfoot>
                </table>
				<button style="display:none;" id="updatecart" type="submit" name="updatecart">Update Shopping Cart</button>
                </form> 				
              </div>
              <div class="cart_navigation"> 
				<a class="continue-btn" href="<?php echo base_url(); ?>"><i class="fa fa-arrow-left"> </i>&nbsp; Continue shopping</a>
				
				<a class="checkout-btn" href="<?php echo base_url('cart/checkout');?>"><i class="fa fa-check"></i> Proceed to checkout</a>				
				<a class="checkout-btn" onclick="return updatecartbtn();" href="#"><i class="fa fa-check"></i> Update Cart</a> 
				<a class="checkout-btn" href="<?php echo base_url('cart/emptycart/'); ?>"><i class="fa fa-check"></i> Clear Shopping Cart</a>
			</div>
            </div>
			<?php } ?>
          </div>
        </div>
      </div>
    </div>
  </section>

		
<?php $this->load->view('front/layout/footer');?>
<script>
	function updatecartbtn()
	{
		document.getElementById('updatecart').click();
	}
</script>	


