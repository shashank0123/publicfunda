
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="print">
.main-table {
	background-color: #ffffff !important;
}
</style>
<style type="text/css" media="screen,print">
body,p,div,td {
	color: #000000;
	font: 12px Arial;
}
body {
	padding: 0;
	margin: 0;
}
a, a:link, a:visited, a:hover, a:active {
	color: #000000;
	text-decoration: underline;
}
a:hover {
	text-decoration: none;
}
</style>

</head>

<body>


			

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="main-table" style="height: 100%; background-color: #f4f6f8; font-size: 12px; font-family: Arial;">
<tbody><tr>
	<td align="center" style="width: 100%; height: 100%;">
	<table cellpadding="0" cellspacing="0" border="0" style=" width: 602px; table-layout: fixed; margin: 24px 0 24px 0;">
	<tbody><tr>
		<td style="background-color: #ffffff; border: 1px solid #e6e6e6; margin: 0px auto 0px auto; padding: 0px 30px 22px 30px; text-align: left;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 27px 0px 0px 0px; border-bottom: 1px solid #868686; margin-bottom: 8px;">
			<tbody><tr>
				<td align="left" style="padding-bottom: 3px;" valign="middle">
                                	<h2>Bantom Labs
                                </h2></td>
				<td width="100%" valign="bottom" style="text-align: right;  font: bold 26px Arial; text-transform: uppercase;  margin: 0px;">Order Detail</td>
			</tr>
			</tbody></table>
		
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tbody><tr valign="top">
								<td style="width: 50%; padding: 14px 0px 0px 2px; font-size: 12px; font-family: Arial;">
					<h2 style="font: bold 12px Arial; margin: 0px 0px 3px 0px;">Bantom Labs</h2>
					3048-3050,Bhagat Singh Street No. 1 ,Chunamandi , Paharganj ,<br>
					New Delhi, Delhi 110055<br>
					India
					<table cellpadding="0" cellspacing="0" border="0">
					<!--					<tr valign="top">
						<td style="font-size: 12px; font-family: verdana, helvetica, arial, sans-serif; text-transform: uppercase; color: #000000; padding-right: 10px;	white-space: nowrap;">Phone:</td>
						<td width="100%" style="font-size: 12px; font-family: Arial;">8882818855</td>
					</tr>
															-->
					<!--										<tr valign="top">
						<td style="font-size: 12px; font-family: verdana, helvetica, arial, sans-serif; text-transform: uppercase; color: #000000; padding-right: 10px; white-space: nowrap;">Email:</td>
						<td width="100%" style="font-size: 12px; font-family: Arial;"><a href="mailto:info@ehealthmart.in">info@ehealthmart.in</a></td>
					</tr>
					-->
					</table>
				</td>
												<td style="padding-top: 14px;">
					<h2 style="font: bold 17px Tahoma; margin: 0px;">Order id-&nbsp;<?php echo $shpping[0]->id;?></h2>
					<table cellpadding="0" cellspacing="0" border="0">
					<!--<tr valign="top">
						<td style="font-size: 12px; font-family: verdana, helvetica, arial, sans-serif; text-transform: uppercase; color: #000000; padding-right: 10px; white-space: nowrap;">Status:</td>
						<td width="100%" style="font-size: 12px; font-family: Arial;">COD Confirmed (Auto)</td>
					</tr>-->
					<tbody><tr valign="top">
						<td style="font-size: 12px; font-family: verdana, helvetica, arial, sans-serif; text-transform: uppercase; color: #000000; padding-right: 10px; white-space: nowrap;">Order Date:</td>
						<td style="font-size: 12px; font-family: Arial;"><?php echo $shpping[0]->ord_date;?></td>
					</tr>
					<tr valign="top">
						<td style="font-size: 12px; font-family: verdana, helvetica, arial, sans-serif; text-transform: uppercase; color: #000000; padding-right: 10px; white-space: nowrap;">Payment method:</td>
						<td style="font-size: 12px; font-family: Arial;"><?php echo $shpping[0]->payment_method;?></td>
					</tr>
										</tbody></table>
				</td>
							</tr>
			</tbody></table>
		
												<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 32px 0px 24px 0px;">
			<tbody><tr valign="top">
												<!--<td width="33%" style="font-size: 12px; font-family: Arial;">
					<h3 style="font: bold 17px Tahoma; padding: 0px 0px 3px 1px; margin: 0px;">Customer:</h3>
					<p style="margin: 2px 0px 3px 0px;"></p>
																								<p style="margin: 2px 0px 3px 0px;"><a href="mailto:gorakh.infotrench%40gmail.com">gorakh.infotrench@gmail.com</a></p>
																											
				</td>-->
				
																<?php
																$or=$shpping[0]->id;
																$qry=$this->shoppingdetail_model->billingdetail($or);
																?>
																<td width="34%" style="font-size: 12px; font-family: Arial; padding-right: 10px; padding-left: 10px;">
					<h3 style="font: bold 17px Tahoma; padding: 0px 0px 3px 1px; margin: 0px;">Bill to:</h3>
										<p style="margin: 2px 0px 3px 0px;">
						<?php echo $qry[0]->firstname;?>				</p>
															<p style="margin: 2px 0px 3px 0px;">
						<?php echo $qry[0]->address;?>				</p>
															<p style="margin: 2px 0px 3px 0px;">
						<?php echo $qry[0]->state;?>&nbsp;&nbsp;<?php echo $qry[0]->city;?>&nbsp;&nbsp;<?php echo $qry[0]->pincode;?> 					</p>
															<p style="margin: 2px 0px 3px 0px;">
						<?php echo $qry[0]->country?>
					</p>
															<p style="margin: 2px 0px 3px 0px;">
						<?php echo $qry[0]->mobile;?> 					</p>
										
				</td>
												<?php
												$qry1=$this->shoppingdetail_model->shippingdetail($or);
												?>
																<td width="33%" style="font-size: 12px; font-family: Arial;">
					<h3 style="font: bold 17px Tahoma; padding: 0px 0px 3px 1px; margin: 0px;">Ship to:</h3>
										<p style="margin: 2px 0px 3px 0px;">
						<?php echo $qry1[0]->firstname;?>				</p>
															<p style="margin: 2px 0px 3px 0px;">
						<?php echo $qry1[0]->address;?>,					</p>
															<p style="margin: 2px 0px 3px 0px;">
						<?php echo $qry1[0]->state;?>&nbsp;&nbsp;<?php echo $qry1[0]->city;?>&nbsp;&nbsp;<?php echo $qry1[0]->pincode;?>					</p>
															<p style="margin: 2px 0px 3px 0px;">
						<?php echo $qry1[0]->country;?>
					</p>
															<p style="margin: 2px 0px 3px 0px;">
						9717903733 					</p>
										
				</td>
							</tr>
			</tbody></table>
											
		
						
			<table width="100%" cellpadding="0" cellspacing="1" style="background-color: #dddddd;">
			<tbody><tr>
				<th width="70%" style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial;">Product</th>
				<th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial;">Quantity</th>
				<th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial;">Unit price</th>
									<!--<th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial;">Discount</th>-->
												<!--<th style="background-color: #eeeeee; padding: 6px 10px; white-space: nowrap; font-size: 12px; font-family: Arial;">Subtotal</th>-->
			</tr>
			<?php
			
			$res=$this->shoppingdetail_model->prodcutdetail($or);
		
			foreach($res as $val)
			{
			?>
						<tr>
					<td style="padding: 5px 10px; background-color: #ffffff; font-size: 12px; font-family: Arial;">
						<?php echo $val->pro_name;?>
						
						<?php 
						$id=$val->pro_id;
						$res1=$this->shoppingdetail_model->prode($id); 
						foreach($res1 as $val1)
						{
						?>
						
						<!--<p style="margin: 2px 0px 3px 0px;">Code- SKU: <?php //echo $val1->product_sku;?></p>	-->	<?php
						}
?>						<!--<p style="margin: 2px 0px 3px 0px;">Merch Ref: EHL-ENDURAM3</p>-->
																	</td>
					<td style="padding: 5px 10px; background-color: #ffffff; text-align: center; font-size: 12px; font-family: Arial;"><?php echo $val->qty;?></td>
					<td style="padding: 5px 10px; background-color: #ffffff; text-align: right; font-size: 12px; font-family: Arial;">Rs.<?php echo $val1->spacel_price;?></td>                   									<!--	<td style="padding: 5px 10px; background-color: #ffffff; text-align: right; font-size: 12px; font-family: Arial;">Rs.106</td>-->
					                                      							
					<!--<td style="padding: 5px 10px; background-color: #ffffff; text-align: right; white-space: nowrap; font-size: 12px; font-family: Arial;"><b><?php //echo $val->total_amount;?></b>&nbsp;</td>-->
				</tr>
				<?php
				}
				?>
																
			</tbody></table>
		
						
					
						

			
<!--Payment Total Calculation -->
<div style="width:100%; border:none; float:right;	display:inline;	margin-top:20px;">
<div style="float:left;	display:inline;	width:100%;	margin-top:7px;">
<div style="color: #7C7E80;
    display: inline;
    float: left;
    font: 13px trebuchet ms;
    text-align: right;
    width: 69%;">
Subtotal :
</div>

<div style="float:right; display:inline; width:29%; text-align:right; font:13px trebuchet ms; color:#636566; font-weight:bold;">
Rs.<?php echo $val->total_amount;?></div>
</div>
		
	<div style="float:left;	display:inline;	width:100%;	margin-top:7px;">
		<div style="float:left; display:inline; width:69%; text-align:right; font:13px trebuchet ms; color:#7c7e80;"> Shipping Cost : </div>	
		<div style="float:right; display:inline; width:29%; text-align:right; font:13px trebuchet ms; color:#636566;">
			Rs.0		</div>
	</div>




	

<div style="float:left;	display:inline;	width:100%;	margin-top:7px;">

</div>

			<!--<div style="float:left;	display:inline;	width:100%;	margin-top:7px;">
		<div style="float:left; display:inline; width:69%; text-align:right; font:13px trebuchet ms; color:#7c7e80;">
			COD Processing Fee :
		</div>
	
		<div style="float:right; display:inline; width:29%; text-align:right; font:13px trebuchet ms; color:#636566;">
			Rs.14		</div>
	</div>-->
		<div style="float:left;	display:inline;	width:100%;	margin-top:7px;">
	<div style="float:left; display:inline; width:69%; text-align:right; font:13px trebuchet ms; color:#7c7e80;">
	<span style="float:left; display:inline; width:100%; text-align:right; font:bold 20px trebuchet ms; color:#99002A;">
	Total :
	</span>
	</div>

	<div style="float:right; display:inline; width:29%; text-align:right; font:13px trebuchet ms; color:#636566;">
	<span style="float:left; display:inline; width:100%; text-align:right; font:bold 20px trebuchet ms; color:#99002A;">
	Rs.<?php echo $val->total_amount;?></span>
	</div>
<div style="clear:both;"></div>
<div style="	float:left; display:inline; width:100%; text-align:right; font:11px verdana; color:#7c7e80;">
	</div>




</div>

		</div>



					
					
							
						</td>
	</tr>
	</tbody></table>
	</td>
</tr>
</tbody></table>
       
</body></html>