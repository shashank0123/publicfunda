﻿<?php $this->load->view('front/layout/header');?>  <!-- end header --> 
  
  <!-- Navbar -->
  
  <!-- end nav --> 
  
  <!-- Breadcrumbs -->
  
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="<?php echo base_url('home');?>">Home</a><span>&raquo;</span></li>
            <li class="category13"><strong>My Account</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login mb-30">
        	<div class="row">
            <div class="col-md-3">
              <div id="left-pnl" class="aside-site-left my_account_section   ">
                      
                
    

<div class="action_links">
     <?php $this->load->view('front/layout/my-account-left-sidebar'); ?>   
</div>



                <div class="sidebox-bottom"> <span> &nbsp; </span> </div>
                              </div>
            </div>
            <div class="col-md-8">
              <div class="aside-site-content my_account_section" >
             		<div class="site-content">
        				<div class="central-content"> 
        					<div class="mainbox-container margin margin">
                            	<div class="mainbox-body">
                                <div class="main_section">

    
    <div class="profile_settings">
        

        <div class="my_profile" style="display:block;">
                <div class="uhe_header">
											
											<?php
											if(!empty($user[0]->image))
											{
												?>
												<img src="<?php echo base_url('uploads/image/'.$user[0]->image); ?>" width="100" >
										<?php
											}
											else
											{
												?>
												<i class="avatar_m"></i>
												<?php
											}
											
											?>
                                            
                                            <div class="user_info">
                                                            <p class="name"><?php echo $user[0]->fname;?>   </p>
                                <p><?php echo $user[0]->email;?></p>
                                <p><?php echo $user[0]->mobile;?></p>
                                                    </div>
                </div>
                <div class="default_form">
                <h3>Update your Profile</h3>
				<?php echo $this->session->flashdata('message'); ?>
                <form name="profile_form" action="" method="post" enctype="multipart/form-data">
                    <input id="selected_section" type="hidden" value="general" name="selected_section">
        <input type="hidden" name="token" value="c662f03daae9562c91ad649b716402c3">
		<input id="default_card_id" type="hidden" value="" name="default_cc">
		<input type="hidden" name="profile_id" value="">
		
                                                        <fieldset>
                                                       
                                <div class="s_row_split2">
                                <div class="s_row">
                                        <input type="text" class="label_jump" name="fname" id="firstname" value="<?php echo $user[0]->fname;?>" size="32" maxlength="128">
                                        <span></span>
                                        <label class="label-valid"> First Name</label>
                                        <div class="error-text" style="display: none;" id="fname"><span>Enter correct first name</span></div>
                                        <div class="error-text" style="display: none;" id="checkBlank1"><span>First name cannot be blank</span></div>
                                </div>
                                <div class="s_row">
                                        <input type="tel" class="label_jump" name="lname" id="lastname" size="32" maxlength="128" value="<?php echo $user[0]->lname;?>">
                                        <span></span>
                                        <label class="label-valid">Last Name</label>
                                        <div class="error-text" style="display: none;" id="lname"><span>Enter correct last name</span></div>
                                        <div class="error-text" style="display: none;" id="checkBlank2"><span>Last name cannot be blank</span></div>
                                </div>
                                </div>
                                <div class="s_row_split2">
                                <div class="s_row">
                                        <input type="tel" class="label_jump" pattern="[0-9]*" name="mobile" id="phone" value="<?php echo $user[0]->mobile;?>" size="10" maxlength="10">
                                        <span></span>
                                        <label class="label-valid">Mobile Number</label>
                                        <div class="error-text" style="display: none;" id="mnumber"><span>Enter correct mobile number</span></div>
                                        <div class="error-text" style="display: none;" id="checkBlank3"><span>Mobile number cannot be blank</span></div>
                                </div>
                                <div class="s_row">
                                    <input type="text" class="label_jump" name="email" value="<?php echo $user[0]->email;?>" readonly>
                                        <label class="label-valid">Email Id</label> 
                                        <span></span>
                                </div>
								
                                </div>
								<div class="s_row_split1">
								<div class="s_row">
                                    <input type="file" class="label_jump" name="image">
									<input type="hidden" value="<?php echo $user[0]->image; ?>" name="oldimage">
                                        <label class="label-valid">Image</label> 
                                        <span></span>
                                </div>
								</div>
                                <div class="inline_radio s_row">
                                    <ul>

                                        <li>
                            <input type="radio" id="gnd-radio-1-1" class="regular-radio" name="gender" value="M"<?php echo ($user[0]->gender =='M')?'checked':'';?> checked="checked">
                                        <label for="gnd-radio-1-1">Male</label>
                                      </li>
                                      
                                      <li>
                            <input type="radio" id="gnd-radio-1-2" class="regular-radio" name="gender" value="F" <?php echo ($user[0]->gender =='F')?'checked':'';?>>
                                        <label for="gnd-radio-1-2">Female</label>
                                      </li>
                
                                    </ul>
                            <div class="error-text" style="display: none;" id="checkBlank4"><span>Select gender</span></div>
   
                                </div>
                                                                                        </fieldset>
                <div class="btn_container"> 
                                                                <div class="btn_size"><input type="submit" name="updateprofile" id="save_profile_but" value="Update Profile" class="btn orange"></div>
                                        </div>
                        </form>
                </div>
         </div>
         
         <div class="my_address_book">
                
                    <h3>Address Book  </h3>

        <input type="hidden" name="token" value="">
            <div class="address_list">
               

                    <ul class="regular slider slick-initialized slick-slider" id="address">
                                                     <div aria-live="polite" class="slick-list draggable">
                                                     <div class="slick-track" style="opacity: 1;  transform: translate3d(0px, 0px, 0px);" role="listbox">
                                                     <li class="slick-slide slick-current slick-active" data-slick-index="0" style="min-height: 271px;" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00">
                                    <p class="add_name"></p>
                                   <p class="name"><?php echo $user[0]->fname;?></p>
                                    <p><?php echo $user[0]->mobile;?></p>
                                    <p><?php echo $user[0]->address;?></p>
                                    <p><?php echo $user[0]->state;?></p>
                                    <p><?php echo $user[0]->pincode;?></p>
                                    <p class="link"><a onclick="$('#one').show(); hideErrors(); editAddress(0);" href="javascript:void(0)" tabindex="0">Edit Address</a>
                                                                                               
                                        </p>
                                    
                            </li>
                            						
                                                    
                            
                            
                            </div></div>
                            
                   
                             </ul>
           
            </div>
 
        
        
        
        
                       
       
 
        <div id="one" style="display:none;">
    <div class="default_form" id="rj_edit_address_form">
    <form method="post" action="" name="del_addressbook" id="del_addressbook">
        <input type="hidden" name="selected_section" value="general">
        <input type="hidden" name="mode" value="manage">
        <input type="hidden" name="token" value="">
 


            
            <h3 id="formHeading">Address Form</h3>
         
             
                   
            <fieldset>
    
                            <input type="hidden" name="profile_id" value="0" id="tag1">
                            <div class="s_row_split2">
                            <div class="s_row">
                                    <input type="text"   name="fname" value="<?php echo $user[0]->fname;?>" id="tag2">
                                    <span></span>
                                    <label class="label-valid"> First Name</label>
                                    <div class="error-text" id="check2" style="display:none;">Enter correct first name</div>
                                    <div class="error-text" id="checkBlank2" style="display:none;">First name cannot be blank</div>
                                    
                            </div>
                            <div class="s_row">
                                    <input type="tel" class="label_jump" name="lname" value="<?php echo $user[0]->lname;?>" id="tag3">
                                    <span></span>
                                    <label class="label-valid"> Last Name</label>
                                    <div class="error-text" id="check3" style="display:none;">Enter correct last name</div>
                                    <div class="error-text" id="checkBlank3" style="display:none;">Last name cannot be blank</div>
                            </div>
                           
                            </div>


                            <div class="s_row_split2">
                            <div class="s_row">
                                    <input type="tel" class="label_jump" name="mobile" maxlength="10" value="<?php echo $user[0]->mobile;?>" id="tag4">
                                    <span></span>
                                    <label class="label-valid"> Mobile Number</label>
                                    <div class="error-text" id="check4" style="display:none;">Enter correct mobile number</div>
                                    <div class="error-text" id="checkBlank4" style="display:none;">Mobile number cannot be blank</div>
                            </div>
                                 <div class="s_row">
                                    <input type="text" class="label_jump" name="pincode" maxlength="6" value="<?php echo $user[0]->pincode;?>" id="tag5">
                                    <span></span>
                                    <label class="label-valid"> Pincode</label>
                                    <div class="error-text" id="check5" style="display:none;">Enter correct pincode</div>
                                    <div class="error-text" id="checkBlank5" style="display:none;">Pincode cannot be blank</div>
                            </div>
                            </div>




                            <div class="s_row_split2">
                               <div class="s_row">
                                    <input type="tel" class="label_jump" name="address" value="<?php echo $user[0]->address;?>" id="tag6">
                                    <span></span>
                                    <label class="label-valid"> Address</label>
                                    <div class="error-text" id="check6" style="display:none;">Enter correct House/Plot No</div>
                                    <div class="error-text" id="checkBlank6" style="display:none;">House/Plot No cannot be blank</div>
                                </div>
                           
                                  <div class="s_row">
                                    <input type="text" class="label_jump" name="state" value="<?php echo $user[0]->state;?>" id="tag7">
                                    <span></span>
                                    <label class="label-valid"> State</label>
                                    <div class="error-text" id="check7" style="display:none;">_myaccount_address2_error</div>
                                    <div class="error-text" id="checkBlank7" style="display:none;">_myaccount_address2_blank</div>
                            </div>

                            </div>

                            <div class="s_row_split1">
                            <div class="s_row">
                                    <input type="tel" class="label_jump" name="city" value="<?php echo $user[0]->city;?>" id="tag8">
                                    <span></span>
                                    <label class="label-valid">City</label>
                                    <div class="error-text" id="check8" style="display:none;">Enter correct city</div>
                                    <div class="error-text" id="checkBlank8" style="display:none;">City cannot be blank</div>
                            </div>
                                
                            </div>
                                
                    </fieldset>
              <div class="btn_container"> 
                    <div class="btn_size"> <input class="btn orange address_save_button" type="submit" value="Save Address" name="updateaddress"></div>
                 
                    <!--<div class="btn_size"><input id="cancel" type="button" value="Cancel" class="btn orange"></div>-->
                </div>
    </form>     
            </div>      
        
    </div>
</div>

<div class="change_password">
        <h3>Change Password</h3>
        
            <div class="default_form">
                
				<?php echo $this->session->flashdata('message2'); ?>
				
                    <form name="password_form" action="<?php echo base_url('user/change_password');?>" method="post">
                         <input type="hidden" name="token" value="">
                            <fieldset>
                                    <div class="s_row_split2">
                                        <div class="s_row">
                                            <input for="passwordc" type="password" id="passwordc" name="current_pwd" size="32" maxlength="32" class="label_jump" autocomplete="off" required>
                                            <span></span>
                                            <label for="passwordc" class="label-valid"><span class="error_text"></span>Current Password</label>
                                            <div class="error-text" id="passwordBlank" style="display: none;"><span>Current password cannot be empty</span></div>

                                        </div>
                                    </div>
                                    <div class="s_row_split2">
                                        <div class="s_row">
                                            <input type="password" id="password1" name="npwd" size="32" maxlength="32" class="label_jump" autocomplete="off" required>
                                            <span></span>
                                            <label for="password1" class="label-valid"><span class="error_text"></span>New Password</label>
                                            <div class="error-text" id="password1Blank" style="display: none;"><span>New password cannot be empty</span></div>
                                        </div>
                                    </div>
                                    <div class="s_row_split2">
                                        <div class="s_row">
                                            <input type="password" id="password2" name="cpwd" size="32" maxlength="32" class="label_jump" autocomplete="off" required>
                                            <span></span>
                                            <label for="password2" class="label-valid"><span class="error_text"></span>Confirm new password</label>
                                            <div class="error-text" id="password2Blank" style="display: none;"><span>Confirm password cannot be empty</span></div>
                                            <div class="error-text" id="newEqualToConfirm" style="display: none;">New password and confirm new password do not match</div>

                                        </div>
                                    </div>
                            </fieldset>
                    
            <div class="btn_container"> 
	<div class="btn_size"><input type="submit" value="Change Password" name="updatepassword" id="save_profile_but" class="btn orange" ></div>
           </div>
                </form>    
        </div>
            </div>
    </div>
</div>




</div>
    </div>
            <div class="clear-both"></div>            
            
                
                        <div class="clear-both"></div>
                 </div> 
                   </div>
        
                    </div>
            </div>
          </div>
      </div>
     
    </div>
  </section>
  <!-- Main Container End --> 
  <!-- our clients Slider -->
  <section class="our-clients">
    <div class="container">
      <div class="slider-items-products">
        <div id="our-clients-slider" class="product-flexslider hidden-buttons"> 
          <!-- Begin page header-->
          <div class="page-header-wrapper">
            <div class="container">
              <div class="page-header text-center wow fadeInUp"> <h2>Our <span class="text-main">Clients</span></h2>
                <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
                <p class="lead text-gray"> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. sed diam nonummy nibh euismod tincidunt.</p>
              </div>
            </div>
          </div>
          <!-- End page header-->
          <div class="slider-items slider-width-col6"> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand1.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand2.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand3.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand4.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand5.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand6.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand7.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url();?>assets/front/images/brand3.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- home contact -->
  <section id="contact" class="gray">
    <div class="container"> 
      
      <!-- Begin page header-->
      <div class="page-header-wrapper">
        <div class="container">
          <div class="page-header text-center wow fadeInUp">
            <h2>Contact <span class="text-main">Us</span></h2>
            <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
            <p class="lead text-gray"> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. sed diam nonummy nibh euismod tincidunt.</p>
          </div>
        </div>
      </div>
      <!-- End page header-->
      
      <div class="row">
        <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-home fa-2x"></i>
          <h3>Our Address</h3>
          <span class="font-l">7064 Lorem Ipsum Vestibulum 666/13</span> </div>
        <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-comment fa-2x"></i>
          <h3>Our mail</h3>
          <span class="font-l"><a href="mailto:support@justtheme.com">support@justtheme.com</a></span> </div>
        <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-phone fa-2x"></i>
          <h3>Our phone</h3>
          <span class="font-l">+012 315 678 1234</span> </div>
      </div>
    </div>
  </section>
  <!-- Footer -->
  <?php $this->load->view('front/layout/footer');?>