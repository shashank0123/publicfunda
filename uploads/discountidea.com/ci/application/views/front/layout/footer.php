<!-- our clients Slider -->
  <section class="our-clients">
    <div class="container">
      <div class="slider-items-products">
        <div id="our-clients-slider" class="product-flexslider hidden-buttons"> 
          <!-- Begin page header-->
          <div class="page-header-wrapper">
            <div class="container">
              <div class="page-header text-center wow fadeInUp"> <h2>Our <span class="text-main">Clients</span></h2>
                <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
                <p class="lead text-gray"> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. sed diam nonummy nibh euismod tincidunt.</p>
              </div>
            </div>
          </div>
          <!-- End page header-->
          <div class="slider-items slider-width-col6"> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url('assets/front'); ?>/images/brand1.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url('assets/front'); ?>/images/brand2.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url('assets/front'); ?>/images/brand3.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url('assets/front'); ?>/images/brand4.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url('assets/front'); ?>/images/brand5.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url('assets/front'); ?>/images/brand6.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url('assets/front'); ?>/images/brand7.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
            <!-- Item -->
            <div class="item wow zoomIn"> <a href="#"><img src="<?php echo base_url('assets/front'); ?>/images/brand3.png" alt="Image" class="grayscale"></a> </div>
            <!-- End Item --> 
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- home contact -->
  <section id="contact" class="gray">
    <div class="container"> 
      
      <!-- Begin page header-->
      <div class="page-header-wrapper">
        <div class="container">
          <div class="page-header text-center wow fadeInUp">
            <h2>Contact <span class="text-main">Us</span></h2>
            <div class="divider divider-icon divider-md">&#x268A;&#x268A; &#x2756; &#x268A;&#x268A;</div>
            <p class="lead text-gray"> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. sed diam nonummy nibh euismod tincidunt.</p>
          </div>
        </div>
      </div>
      <!-- End page header-->
      
      <div class="row">
        <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-home fa-2x"></i>
          <h3>Our Address</h3>
          <span class="font-l">7064 Lorem Ipsum Vestibulum 666/13</span> </div>
        <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-comment fa-2x"></i>
          <h3>Our mail</h3>
          <span class="font-l"><a href="mailto:support@justtheme.com">support@justtheme.com</a></span> </div>
        <div class="col-sm-4 adress-element wow zoomIn"> <i class="fa fa-phone fa-2x"></i>
          <h3>Our phone</h3>
          <span class="font-l">+012 315 678 1234</span> </div>
      </div>
    </div>
  </section>
  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-xs-12 col-lg-4">
          <div class="footer-logo"><a href="index.html"><img src="<?php echo base_url('assets/front'); ?>/images/footer-logo.png" alt="fotter logo"></a></div>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. <a href="#">Read more</a></p>
          <div class="payment">
            <ul>
              <li><a href="#"><img title="Visa" alt="Visa" src="<?php echo base_url('assets/front'); ?>/images/visa.png" class="grayscale"></a></li>
              <li><a href="#"><img title="Paypal" alt="Paypal" src="<?php echo base_url('assets/front'); ?>/images/paypal.png" class="grayscale"></a></li>
              <li><a href="#"><img title="Discover" alt="Discover" src="<?php echo base_url('assets/front'); ?>/images/discover.png" class="grayscale"></a></li>
              <li><a href="#"><img title="Master Card" alt="Master Card" src="<?php echo base_url('assets/front'); ?>/images/master-card.png" class="grayscale"></a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
            <h3 class="links-title">Information<a class="expander visible-xs" href="#TabBlock-1">+</a></h3>
            <div class="tabBlock" id="TabBlock-1">
              <ul class="list-links list-unstyled">
                <li><a href="#s">Delivery Information</a></li>
                <li><a href="#">Discount</a></li>
                <li><a href="sitemap.html">Sitemap</a></li>
               <li><a href="<?php echo base_url('home/privacy_policy');?>">Privacy Policy</a></li>
                <li><a href="<?php echo base_url('faq/faqcms');?>">FAQs</a></li>
                <li><a href="<?php echo base_url('home/term_condition');?>">Terms &amp; Condition</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
            <h3 class="links-title">Categories<a class="expander visible-xs" href="#TabBlock-2">+</a></h3>
            <div class="tabBlock" id="TabBlock-2">
              <ul class="list-links list-unstyled">
                <li><a href="#">Women</a></li>
                <li><a href="#">Men</a></li>
                <li><a href="#">Electronics</a></li>
                <li><a href="#">Clothing</a></li>
                <li><a href="#">Lookbook</a></li>
                <li><a href="#">Accessories </a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
            <h3 class="links-title">Insider<a class="expander visible-xs" href="#TabBlock-3">+</a></h3>
            <div class="tabBlock" id="TabBlock-3">
              <ul class="list-links list-unstyled">
                <li> <a href="blog.html">Blog</a> </li>
                <li> <a href="#">News</a> </li>
                <li> <a href="#">Trends</a> </li>
                <li> <a href="<?php echo base_url('home/about_us');?>">About Us</a> </li>
                <li> <a href="<?php echo base_url('home/contact_us');?>">Contact Us</a> </li>
              <!--  <li> <a href="#">My Orders</a> </li>-->
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-12 col-lg-2 collapsed-block">
          <div class="footer-links">
            <h3 class="links-title">Service<a class="expander visible-xs" href="#TabBlock-4">+</a></h3>
            <div class="tabBlock" id="TabBlock-4">
              <ul class="list-links list-unstyled">
                <li> <a href="account_page.html">Account</a> </li>
                <li> <a href="wishlist.html">Wishlist</a> </li>
                <li> <a href="shopping_cart.html">Shopping Cart</a> </li>
                <li> <a href="#">Return Policy</a> </li>
                <li> <a href="#">Special</a> </li>
                <li> <a href="#">Lookbook</a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-newsletter">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h3>Subscribe for Our Newsletter!</h3>
            <p>Enjoy our newsletter to stay updated with the latest news and special sales.</p>
            <p>Let's your email address here!</p>
            <form id="newsletter-validate-detail" method="post" action="#">
              <div class="newsletter-inner">
                <input class="newsletter-email" name='Email' placeholder='Enter Your Email'/>
                <button class="button subscribe" type="submit" title="Subscribe">Subscribe</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-coppyright">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-xs-12 coppyright"> Copyright © 2016 <a href="#"> metric </a>. All Rights Reserved. </div>
          <div class="social col-sm-6 col-xs-12">
            <ul class="inline-mode">
              <li class="social-network fb"><a title="Connect us on Facebook" target="_blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
              <li class="social-network googleplus"><a title="Connect us on Google+" target="_blank" href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
              <li class="social-network tw"><a title="Connect us on Twitter" target="_blank" href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
              <li class="social-network rss"><a title="Connect us on Instagram" target="_blank" href="https://instagram.com/"><i class="fa fa-rss"></i></a></li>
              <li class="social-network linkedin"><a title="Connect us on Linkedin" target="_blank" href="https://www.pinterest.com/"><i class="fa fa-linkedin"></i></a></li>
              <li class="social-network instagram"><a title="Connect us on Instagram" target="_blank" href="https://instagram.com/"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <a href="#" class="totop"> </a> </div>

<!-- End Footer --> 
<!-- JS --> 

<!-- jquery js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/jquery.min.js"></script> 

<!-- bootstrap js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/bootstrap.min.js"></script> 

<!-- owl.carousel.min js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/owl.carousel.min.js"></script> 

<!-- bxslider js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/jquery.bxslider.js"></script> 

<!--jquery-slider js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/slider.js"></script> 

<!-- megamenu js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/megamenu.js"></script> 
<script type="text/javascript">
        /* <![CDATA[ */   
        var mega_menu = '0';
        
        /* ]]> */
        </script> 

<!-- jquery.mobile-menu js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/mobile-menu.js"></script> 

<!-- jquery.waypoints js --> <script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/waypoints.js"></script>  

<!--jquery-ui.min js --> 
<script src="<?php echo base_url('assets/front'); ?>/js/jquery-ui.js"></script> 

<!-- main js --> 
<script type="text/javascript" src="<?php echo base_url('assets/front'); ?>/js/main.js"></script>
</body>

</html>