<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit User</h1>
                         <form enctype="multipart/form-data" role="form" method="post" >
                            <div class="form-group">
                                <label>Full Name</label>
                 <input type="text" class="form-control" placeholder="Full Name" value="<?php echo $USERDATA[0]->fname; ?>" name="username">
                            </div>
							
							<div class="form-group">
							<label>Type of User</label>
							<select class="form-control" name="usertype">
							<option>Select User Type</option>
							<option value="0" <?php echo($USERDATA[0]->vender_type==0)?'selected':''; ?>>Normal User</option>
							<option value="1" <?php echo($USERDATA[0]->vender_type==001)?'selected':''; ?>>Vender User</option>
							</select>
							</div>
							
							<div class="control-group ">
                               <label class="control-label">Old Photo<font color="#FF0000">* </font> </label>
                                <div class="controls">
                                 <img src="<?php echo base_url(); ?>uploads/banner/<?php echo $USERDATA[0]->image; ?>" width="90" height="50"/>
				                 <input type="hidden" name="oldimage" value="<?php echo $USERDATA[0]->image; ?>" />
								 <input type="file" class="form-control" name="image">
                                </div>
                            </div>
							
							<div class="form-group">
                                <label>Email Address</label>
                   <input type="text" class="form-control" placeholder="Email Address" value="<?php echo $USERDATA[0]->email; ?>"  name="email">
                            </div>	
							
							<div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Password" value="<?php echo $USERDATA[0]->password; ?>"  name="password">
                            </div>	
                            
							<div class="form-group">
                                <label>Contact Number</label>
                                <input type="text" class="form-control" placeholder="Contact Number" value="<?php echo $USERDATA[0]->mobile; ?>"  name="contact_no">
                            </div>
							
							<div class="form-group">
                                <label>Gender</label>
                                <select class="form-control" name="gender">
									<option <?php echo($USERDATA[0]->gender=='Male')?'selected':''; ?> value="Male">Male</option>
									<option <?php echo($USERDATA[0]->gender=='Female')?'selected':''; ?>  value="Female">Female</option>
								</select>
                            </div>
							
							<div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" placeholder="Enter Address" value="<?php echo $USERDATA[0]->address; ?>"  name="address">
                            </div>
							
							<div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" placeholder="Enter City" value="<?php echo $USERDATA[0]->city; ?>"  name="city">
                            </div>
							
							<div class="form-group">
                                <label>State</label>
                                <input type="text" class="form-control" placeholder="Enter State" value="<?php echo $USERDATA[0]->state; ?>"  name="state">
                            </div>
							
														
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option <?php echo($USERDATA[0]->status=='1')?'selected':''; ?>  value="1">Active</option>
                                    <option <?php echo($USERDATA[0]->status=='0')?'selected':''; ?>  value="0">Inactive</option>
                                </select>
                            </div>                            

                            <button type="submit" name="saveuser" class="btn btn-default">Submit Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
