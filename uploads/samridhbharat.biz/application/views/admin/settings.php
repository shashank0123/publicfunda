<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Admin Settings</h1>
                        <form role="form" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Logo</label>
			            <img src="<?php echo base_url('uploads/image/'.$admindata[0]->logo); ?>" width="100" >
                                    <input type="file" class="form-control" placeholder="Title" name="image">
				    <input type="hidden" value="<?php echo $admindata[0]->logo; ?>" name="oldimage">
                            </div>

                               
                           <input type="hidden" value="<?php echo $admindata[0]->password; ?>" name="opwd" > 
			  
			   <div class="control-group ">
                <label class="control-label">Admin email<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="text" class="form-control" name="email" value="<?php echo $admindata[0]->email; ?>" >
                </div><br>
              </div>
			  <div class="control-group ">
                <label class="control-label">Facebook Link<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="text" class="form-control" name="facebook" value="<?php echo $admindata[0]->facebook; ?>" >
                </div><br>
              </div>
			  
			  <div class="control-group ">
                <label class="control-label">Twitter Link<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="text" class="form-control" name="twitter" value="<?php echo $admindata[0]->twitter; ?>" >
                </div><br>
              </div>
			  
			  <div class="control-group ">
                <label class="control-label">Google+ Link<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="text" class="form-control" name="google" value="<?php echo $admindata[0]->google; ?>" >
                </div><br>
              </div>
			  
			  <div class="control-group ">
                <label class="control-label">Youtube Link<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="text" class="form-control" name="youtube" value="<?php echo $admindata[0]->youtube; ?>" >
                </div><br>
              </div>
			  
			  <div class="control-group ">
                <label class="control-label">Pinterest Link<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="text" class="form-control" name="pinterest" value="<?php echo $admindata[0]->pinterest; ?>" >
                </div><br>
              </div>
			  
			  <div class="control-group ">
                <label class="control-label">Stumble Link<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="text" class="form-control" name="stumble" value="<?php echo $admindata[0]->stumble; ?>" >
                </div><br>
              </div>
			  
			  <div class="control-group ">
                <label class="control-label">Tumbler Link<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="text" class="form-control" name="tumbler" value="<?php echo $admindata[0]->tumbler; ?>" >
                </div><br>
              </div>
			  
			  
			  <div class="control-group ">
                <label class="control-label">New Password<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="password" class="form-control" name="pwd" >
                </div><br>
              </div> 
			  
		   <div class="control-group ">
                <label class="control-label">Confirm Password<font color="#FF0000">* </font> </label>
                <div class="controls  ">
                  <input type="password" class="form-control" name="cpwd">
                </div><br>
              </div> 

            </br>
						
							<button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>


</body>

</html>
