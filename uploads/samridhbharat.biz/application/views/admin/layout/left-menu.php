<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="#"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li> 
					<?php /*
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Users<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/admin/userList'); ?>">Users List</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('index.php/admin/adduser'); ?>">Add New</a>
                            </li>
                        </ul>
                    </li> */ ?>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#home">
							<i class="fa fa-fw fa-arrows-v"></i>Slider<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="home" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/banner/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Slider Listing
								</a>
                            </li>							                           
                        </ul>
                    </li>
					<li>
                        <a href="<?php echo base_url('index.php/ball/listing/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Home Balls</i>
						</a>
                    </li>
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#advertisement">
							<i class="fa fa-fw fa-arrows-v"></i>Advertisement<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="advertisement" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/advertisement/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Advertisement Listing
								</a>
                            </li>							                           
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#cms">
							<i class="fa fa-fw fa-arrows-v"></i>CMS<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="cms" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/cms/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Page Listing
								</a>
                            </li>
							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#faq">
							<i class="fa fa-fw fa-arrows-v"></i>FAQ<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="faq" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/faq/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									FAQ Listing
								</a>
                            </li>							
                        </ul>
                    </li>
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#news">
							<i class="fa fa-fw fa-arrows-v"></i>News & Events<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="news" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/news/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									News & Events Listing
								</a>
                            </li>
							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#ducuments">
							<i class="fa fa-fw fa-arrows-v"></i>Legal Documents<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="ducuments" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/documents/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Legal Documents Listing
								</a>
                            </li>
							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#plan">
							<i class="fa fa-fw fa-arrows-v"></i>Plan<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="plan" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/plan/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Plan Listing
								</a>
                            </li>
							
                        </ul>
                    </li>
					
					<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#employer">
							<i class="fa fa-fw fa-arrows-v"></i>Featured Employer<i class="fa fa-fw fa-caret-down"></i>
						</a>
                        <ul id="employer" class="collapse">
                            <li>
                                <a href="<?php echo base_url('index.php/employer/listing'); ?>">
									<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
									Featured Employer Listing
								</a>
                            </li>
							
                        </ul>
                    </li>
					
					
					
					<li>
                        <a href="<?php echo base_url('index.php/block/listing/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Static Block</i>
						</a>
                    </li>
					
			<li>
                        <a href="<?php echo base_url('index.php/admin/settings/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Manage Logo</i>
						</a>
                      </li>

                      <li>
                        <a href="<?php echo base_url('index.php/admin/settings/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Social Links</i>
						</a>
                      </li>

                     <li>
                        <a href="<?php echo base_url('index.php/newsletter/listing/'); ?>">
							<i class="fa fa-fw fa-arrows-v"></i>Newsletter</i>
						</a>
                    </li>
					
                </ul>
            </div>