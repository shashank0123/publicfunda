<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Add Plan</h2>
                         <form role="form" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Plan Title</label>
                                <input type="text" class="form-control" placeholder="Plan Title" name="title">
                            </div>
							
							<div class="form-group">
                                <label>Plan Amount</label>
                                <input type="text" class="form-control" placeholder="Plan Amount" name="amount">
                            </div>
							
							<div class="form-group">
                                <label>Text 1</label>
                                <input type="text" class="form-control" placeholder="Enter text 1" name="text1">
                            </div>
							
							<div class="form-group">
                                <label>Text 2</label>
                                <input type="text" class="form-control" placeholder="Enter text 2" name="text2">
                            </div>
							
							<div class="form-group">
                                <label>Text 3</label>
                                <input type="text" class="form-control" placeholder="Enter text 3" name="text3">
                            </div>	
                            
							<div class="form-group">
                                <label>Text 4</label>
                                <input type="text" class="form-control" placeholder="Enter text 4" value="" name="text4">
                            </div>
							
                            <div name="status" class="form-group">
                                <label>Status</label>
                                <select class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>                            

                            <button type="submit" name="savedata" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
