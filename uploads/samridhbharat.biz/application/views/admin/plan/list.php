<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Plan listing
						<a href="<?php echo base_url('index.php/plan/add'); ?>" class="btn btn-primary pull-right">Add New</a></h2>
						<?php echo $this->session->flashdata('message'); ?>
                         <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
										<th width="5%">S.No.</th>
                                        <th>Plan Title</th>
										<th width="8%">Status</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php $i=0; foreach($PLANDATA as $data){ $i++; ?>
                                    <tr>
										<td><?php echo $i; ?></td>
                                        <td><?php echo $data->title; ?></td>
										<td><img src="<?php echo($data->status=='0')?base_url('assets/admin/image/inactive.png'):base_url('assets/admin/image/active.png'); ?>"> </td>
                                        <td>
											<a href="<?php echo base_url('index.php/plan/edit/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<?php /*<a href="<?php echo base_url('index.php/plan/delete/'.$data->id); ?>" onclick="return deleteConfirm();" title="delete record"><i class="fa fa-trash" aria-hidden="true"></i></a>*/ ?>																			
										</td>
                                    </tr>
                                <?php } ?>                                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
