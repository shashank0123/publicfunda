<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Edit Plan</h2>
                         <form role="form" method="post" enctype="multipart/form-data">
							<div class="form-group">
                               
                            </div>
						 
                            <div class="form-group">
                                <label>Plan Title</label>
                                <input type="text" class="form-control" placeholder="Page Title" value="<?php echo $EDITPLAN[0]->title; ?>" name="title">
                            </div>
							
							<div class="form-group">
                                <label>Plan Amount</label>
                                <input type="text" class="form-control" placeholder="Plan Amount" value="<?php echo $EDITPLAN[0]->amount; ?>" name="amount">
                            </div>
							
							<div class="form-group">
                                <label>Text 1</label>
                                <input type="text" class="form-control" placeholder="Enter text1" value="<?php echo $EDITPLAN[0]->text1; ?>" name="text1">
                            </div>
							
							<div class="form-group">
                                <label>Text 2</label>
                                <input type="text" class="form-control" placeholder="Enter text2" value="<?php echo $EDITPLAN[0]->text2; ?>" name="text2">
                            </div>
							
							<div class="form-group">
                                <label>Text 3</label>
                                <input type="text" class="form-control" placeholder="Enter text3" value="<?php echo $EDITPLAN[0]->text3; ?>" name="text3">
                            </div>	
                            
							<div class="form-group">
                                <label>Text 4</label>
                                <input type="text" class="form-control" placeholder="Enter text4" value="<?php echo $EDITPLAN[0]->text4; ?>" name="text4">
                            </div>
							
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option <?php echo($EDITPLAN[0]->status==1)?'selected':''; ?> value="1">Active</option>
                                    <option <?php echo($EDITPLAN[0]->status==0)?'selected':''; ?> value="0">Inactive</option>
                                </select>
                            </div>                            

                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
