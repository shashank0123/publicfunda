<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id)){	redirect('index.php/admin');	}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<style>
#sliderimg{ width: 25%; display: inline; }
.imgremove{ padding: 6px 14px; margin-right: 13px; }
</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Edit advertisement</h2>
                         <form role="form" method="post" enctype="multipart/form-data" id="myform">
							<div class="form-group">
                                <label>Select advertisement Type</label>
                                <select class="form-control" name="type" onchange="return checktype(this.value);">
									<option <?php echo($EDITBANNER[0]->type=='youtube')?'selected':''; ?> value="youtube">Youtube Video</option>
									<option <?php echo($EDITBANNER[0]->type=='flash')?'selected':''; ?> value="flash">SWF File - ( Flash ) </option>
									<option <?php echo($EDITBANNER[0]->type=='slider')?'selected':''; ?> value="slider">Image</option>
								</select>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" placeholder="Banner Title" value="<?php echo $EDITBANNER[0]->title; ?>" name="title" disabled>
                            </div>
							
							<div class="form-group" id="youtubeblock" <?php echo($EDITBANNER[0]->type!='youtube')?'style="display:none"':''; ?>>
                                <label>Youtube Link</label>
								<input type="text" name="youtube" class="form-control" value="<?php echo $EDITBANNER[0]->youtube; ?>">
                            </div>
							
							<div class="form-group" id="flashblock" <?php echo($EDITBANNER[0]->type!='flash')?'style="display:none"':''; ?>>
                                <label>Flash File</label>
                                <input type="file" class="form-control"  name="flash">
								<input type="hidden" name="oldflash" value="<?php echo $EDITBANNER[0]->flash; ?>">
                            </div>
							
							<div class="form-group" id="sliderblock" <?php echo($EDITBANNER[0]->type!='slider')?'style="display:none"':''; ?>>
                                <label>Image </label>
                                <input type="file" class="form-control"  name="image">
                                <label>Url Link </label>
				<input type="text" class="form-control" name="url" value="<?php echo $EDITBANNER[0]->url; ?>">
				<input type="hidden" name="oldimage" value="<?php echo $EDITBANNER[0]->image; ?>">
                            </div>
                            
                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                            
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script>
var i = 0;
function addslider()
{
	i++;
	var fileHtml = '<span id="rmdiv'+ i +'"><input type="file" class="form-control" name="sliderimg[]" id="sliderimg" required> <button onclick="return removefile('+ i +')" type="button" class="btn btn-xs btn-danger imgremove"> <b>X</b></button></span>';
	$('#appendslider').append(fileHtml);
}


function removefile(ID)
{
	$('#rmdiv'+ ID).remove();
}
function checktype(VALUEDATA)
{	
	if(VALUEDATA=='youtube')
	{		
		$('#flashblock').hide();
		$('#sliderblock').hide();
		$('#youtubeblock').show();
	}
	if(VALUEDATA=='flash')
	{		
		$('#youtubeblock').hide();
		$('#sliderblock').hide();
		$('#flashblock').show();
	}
	if(VALUEDATA=='slider')
	{		
		$('#flashblock').hide();
		$('#youtubeblock').hide();
		$('#sliderblock').show();
	}
	//$('#myform').trigger("reset");
}
</script>
</body>

</html>
