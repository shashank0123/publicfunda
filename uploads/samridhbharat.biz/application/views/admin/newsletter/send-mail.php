<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
					   <?php echo $this->session->flashdata("message"); ?>
                        <h2 class="page-header">Newslatter</h2>
                         <form role="form" method="post" enctype="multipart/form-data">                            							
							
                            <div name="status" class="form-group">
                                <label>Subject</label>
                                <input type="text" class="form-control" name="subject" placeholder="subject" >
                            </div>
								
							<div name="status" class="form-group">
                                <label>Body</label>
                                 <textarea name="emailbody" class="form-control mceEditor"></textarea>
                            </div>
							
                            <button type="submit" name="sendmail" class="btn btn-default">Send</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script>
function checkpancard()
{
	if(document.getElementById("nopancard").checked==true)
	{
		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');
		document.getElementById("pancard").style.backgroundColor = "#ccc";
		document.getElementById("pancard").readOnly = true;
	}
	else
	{
		document.getElementById("pancard").style.backgroundColor = "#fff";
		document.getElementById("pancard").readOnly = false;
	}	
}

</script>
</body>

</html>
