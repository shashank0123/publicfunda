<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Add New Banner</h2>
                         <form role="form" method="post" enctype="multipart/form-data">
							<div class="form-group">
                               <label>Display In</label>
								<ul>
									<li><label><input type="checkbox" name="display_home" value="1"> Home</label></li>
									<li><label><input type="checkbox" name="display_about" value="1"> About Us</label></li>
									<li><label><input type="checkbox" name="display_contact" value="1"> Contact Us</label></li>
								</ul>							   
                            </div>
                            <div class="form-group">
                                <label>Banner Title</label>
                                <input type="text" class="form-control" placeholder="Banner Title" name="title" required>
                            </div>
							
							<div class="form-group">
                                <label>Image</label>
                                <input type="file" class="form-control"  name="image" required>
                            </div>	
                            
                            <div name="status" class="form-group">
                                <label>Status</label>
                                <select class="form-control" required>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>                            

                            <button type="submit" name="savedata" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script>
function checkpancard()
{
	if(document.getElementById("nopancard").checked==true)
	{
		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');
		document.getElementById("pancard").style.backgroundColor = "#ccc";
		document.getElementById("pancard").readOnly = true;
	}
	else
	{
		document.getElementById("pancard").style.backgroundColor = "#fff";
		document.getElementById("pancard").readOnly = false;
	}	
}

</script>
</body>

</html>
