<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Edit Banner</h2>
                         <form role="form" method="post" enctype="multipart/form-data">
							<div class="form-group">
                               <label>Display In</label>
								<ul>
									<li><label><input <?php echo($EDITBANNER[0]->display_home==1)?'checked':'' ?> type="checkbox" name="display_home" value="1"> Home</label></li>
									<li><label><input <?php echo($EDITBANNER[0]->display_about==1)?'checked':'' ?> type="checkbox" name="display_about" value="1"> About Us</label></li>
									<li><label><input <?php echo($EDITBANNER[0]->display_contact==1)?'checked':'' ?> type="checkbox" name="display_contact" value="1"> Contact Us</label></li>
								</ul>							   
                            </div>
                            <div class="form-group">
                                <label>Banner Title</label>
                                <input type="text" class="form-control" placeholder="Banner Title" value="<?php echo $EDITBANNER[0]->title; ?>" name="title">
                            </div>
							<div class="form-group">
                                <img src="<?php echo base_url('uploads/banner/'.$EDITBANNER[0]->image); ?>" width="80">
								<input type="hidden" name="oldimage" value="<?php echo $EDITBANNER[0]->image; ?>">
                            </div>	
							<div class="form-group">
                                <label>Image</label>
                                <input type="file" class="form-control"  name="image">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option <?php echo($EDITBANNER[0]->status==1)?'selected':''; ?> value="1">Active</option>
                                    <option <?php echo($EDITBANNER[0]->status==0)?'selected':''; ?> value="0">Inactive</option>
                                </select>
                            </div>
                            <button type="submit" name="updatedata" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
</body>

</html>
