<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Add New User</h1>
                         <form role="form" method="post">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" placeholder="Full Name" name="name">
                            </div>
							
							<div class="form-group">
                                <label>Email Address</label>
                                <input type="text" class="form-control" placeholder="Email Address" name="email">
                            </div>	
                            
							<div class="form-group">
                                <label>Contact Number</label>
                                <input type="text" class="form-control" placeholder="Contact Number" name="contact_no">
                            </div>
							
							<div class="form-group">
                                <label>Pan Card Number</label>
                                <input type="text" class="form-control" placeholder="Pan Card Number" name="pancard_no" id="pancard">
                            </div>	
							
							<div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" id="nopancard" onclick="return checkpancard();" name="applied_pancard"> <strong>Applied for pancard</strong>
                                    </label>
                                </div>                               
                            </div>
							
							<div class="form-group">
                                <label>Nominee Name</label>
                                <input type="text" class="form-control" placeholder="Nominee Name" name="nominee">
                            </div>	
							
							<div class="form-group">
                                <label>Sponsor ID</label>
                                <input type="text" class="form-control" placeholder="Sponsor ID" name="sponsor_id">
                            </div>	
							
							<div class="form-group">
                                <label>Sponsor Name</label>
                                <input type="text" class="form-control" placeholder="Sponsor Name" name="sponsor_name">
                            </div>	
							
							<div class="form-group">
                                <label>Select Postion</label>
                                <select class="form-control" name="postion">
									<option value="">Select Postion</option>
                                    <option value="L">Left</option>
                                    <option value="R">Right</option>
                                </select>
                            </div> 
							
							<div class="form-group">
                                <label>Select Plan</label>
                                 <select class="form-control" name="plan">
									<option value="">Select Plan</option>
                                    <option>test 1</option>
                                    <option>test 2</option>
                                </select>
                            </div>
							
							<div class="form-group">
                                <label>Enter Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Enter Password">
                            </div>	
							
							<div class="form-group">
                                <label>Enter Confirm Password</label>
                                <input type="password" class="form-control" placeholder="Enter Confirm Password">
                            </div>
							
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>                            

                            <button type="submit" name="saveuser" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script>
function checkpancard()
{
	if(document.getElementById("nopancard").checked==true)
	{
		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');
		document.getElementById("pancard").style.backgroundColor = "#ccc";
		document.getElementById("pancard").readOnly = true;
	}
	else
	{
		document.getElementById("pancard").style.backgroundColor = "#fff";
		document.getElementById("pancard").readOnly = false;
	}	
}

</script>
</body>

</html>
