<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12 ">
						<div class="panel panel-green">
						<div class="panel-heading">
                            <h3 class="panel-title">User Details</h3>
						</div>
						<div class="panel-body">	
                            <div class="col-lg-12">
								
								<div class="col-lg-4">
									<div class="form-group">
										<img src="<?php echo base_url('assets/front/images/avatar/user1.jpg'); ?>" >
									</div>
								</div>	
								<div class="col-lg-8">
								
									<ul class="list-group">
										<li class="list-group-item"><strong>Last Login : </strong>	dduuuuunhd </li>
										<li class="list-group-item"><strong>Loging IP : </strong>	dduuuuunhd </li>
									</ul>								
								</div>
																				
								<div class="clearfix"></div>						
								<div class="form-group">
									<ul class="list-group">
										<li class="list-group-item"><strong><?php echo $USERDATA[0]->name; ?> </strong></li>
										<li class="list-group-item"><strong>ID : </strong>	<?php echo $USERDATA[0]->id; ?> </li>
										<li class="list-group-item"><strong>Account Status : </strong>	<?php echo($USERDATA[0]->status=='1')?'Active':'Inactive'; ?></li>
										<li class="list-group-item"><strong>Contact Number : </strong>	<?php echo $USERDATA[0]->contact_no; ?> </li>
										<li class="list-group-item"><strong>Date Of Birth : </strong>	<?php echo $USERDATA[0]->contact_no; ?> </li>
										<li class="list-group-item"><strong>Gender : </strong>	<?php echo $USERDATA[0]->contact_no; ?> </li>
										<li class="list-group-item"><strong>Address : </strong>	<?php echo $USERDATA[0]->address; ?> </li>
										<li class="list-group-item"><strong>City : </strong>	<?php echo $USERDATA[0]->city; ?> </li>
										<li class="list-group-item"><strong>State : </strong>	<?php echo $USERDATA[0]->state; ?> </li>
										<li class="list-group-item"><strong>Pan Card Number : </strong>	<?php echo $USERDATA[0]->contact_no; ?> </li>
										<li class="list-group-item"><strong>Nominee Name : </strong>	<?php echo $USERDATA[0]->contact_no; ?> </li>
										<li class="list-group-item"><strong>Sponsor ID : </strong>	<?php echo $USERDATA[0]->contact_no; ?> </li>
										<li class="list-group-item"><strong>Sponsor Name : </strong>	<?php echo $USERDATA[0]->contact_no; ?> </li>
										<li class="list-group-item"><strong>Postion : </strong>	<?php echo($USERDATA[0]->postion=='L')?'Left':'Right'; ?> </li>
										<li class="list-group-item"><strong>Plan : </strong>	Plan </li>
										
									</ul>
									
									
								</div>	
																								
									
							</div>					
							
							</div>
						</div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script>
function checkpancard()
{
	if(document.getElementById("nopancard").checked==true)
	{
		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');
		document.getElementById("pancard").style.backgroundColor = "#ccc";
		document.getElementById("pancard").readOnly = true;
	}
	else
	{
		document.getElementById("pancard").style.backgroundColor = "#fff";
		document.getElementById("pancard").readOnly = false;
	}	
}

</script>
</body>

</html>
