<?php 
$admin_id = $this->session->userdata('admin_id'); 
if(empty($admin_id))
{
	redirect('index.php/admin');
}	
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/layout/head'); ?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <?php $this->load->view('admin/layout/header'); ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php $this->load->view('admin/layout/left-menu'); ?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit User</h1>
                         <form role="form" method="post">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" placeholder="Full Name" value="<?php echo $USERDATA[0]->name; ?>" name="name">
                            </div>
							
							<div class="form-group">
                                <label>Profile Image</label>
                                <input type="file" class="form-control" name="image">
                            </div>
							
							<div class="form-group">
                                <label>Email Address</label>
                                <input type="text" class="form-control" placeholder="Email Address" value="<?php echo $USERDATA[0]->email; ?>"  name="email">
                            </div>	
                            
							<div class="form-group">
                                <label>Contact Number</label>
                                <input type="text" class="form-control" placeholder="Contact Number" value="<?php echo $USERDATA[0]->contact_no; ?>"  name="contact_no">
                            </div>
							
							<div class="form-group">
                                <label>Gender</label>
                                <select class="form-control" name="gender">
									<option <?php echo($USERDATA[0]->gender=='Male')?'selected':''; ?> value="Male">Male</option>
									<option <?php echo($USERDATA[0]->gender=='Female')?'selected':''; ?>  value="Female">Female</option>
								</select>
                            </div>
							
							<div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" placeholder="Enter Address" value="<?php echo $USERDATA[0]->address; ?>"  name="contact_no">
                            </div>
							
							<div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" placeholder="Enter City" value="<?php echo $USERDATA[0]->city; ?>"  name="contact_no">
                            </div>
							
							<div class="form-group">
                                <label>State</label>
                                <input type="text" class="form-control" placeholder="Enter State" value="<?php echo $USERDATA[0]->state; ?>"  name="contact_no">
                            </div>
							
							<div class="form-group">
                                <label>Pan Card Number</label>
                                <input type="text" class="form-control" placeholder="Pan Card Number" value="<?php echo $USERDATA[0]->pancard_no; ?>"  name="pancard_no" id="pancard">
                            </div>	
														
							<div class="form-group">
                                <label>Nominee Name</label>
                                <input type="text" class="form-control" value="<?php echo $USERDATA[0]->nominee; ?>"  placeholder="Nominee Name" name="nominee">
                            </div>	
							
							<div class="form-group">
                                <label>Sponsor ID</label>
                                <input type="text" class="form-control" value="<?php echo $USERDATA[0]->sponsor_id; ?>"  placeholder="Sponsor ID" name="sponsor_id">
                            </div>	
							
							<div class="form-group">
                                <label>Sponsor Name</label>
                                <input type="text" class="form-control" value="<?php echo $USERDATA[0]->sponsor_name; ?>"  placeholder="Sponsor Name" name="sponsor_name">
                            </div>	
							
							<div class="form-group">
                                <label>Select Postion</label>
                                <select class="form-control" name="postion">
                                    <option <?php echo($USERDATA[0]->postion=='L')?'selected':''; ?>  value="L">Left</option>
                                    <option <?php echo($USERDATA[0]->postion=='L')?'selected':''; ?> value="R">Right</option>
                                </select>
                            </div> 
							
							<div class="form-group">
                                <label>Select Plan</label>
                                 <select class="form-control" name="plan">
                                    <option>test 1</option>
                                    <option>test 2</option>
                                </select>
                            </div>
							
							<div class="form-group">
                                <label>Enter Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Enter Password">
                            </div>	
														
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control">
                                    <option <?php echo($USERDATA[0]->status=='1')?'selected':''; ?>  value="1">Active</option>
                                    <option <?php echo($USERDATA[0]->status=='0')?'selected':''; ?>  value="0">Inactive</option>
                                </select>
                            </div>                            

                            <button type="submit" name="saveuser" class="btn btn-default">Submit Button</button>

                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('admin/layout/footer-js'); ?>
<script>
function checkpancard()
{
	if(document.getElementById("nopancard").checked==true)
	{
		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');
		document.getElementById("pancard").style.backgroundColor = "#ccc";
		document.getElementById("pancard").readOnly = true;
	}
	else
	{
		document.getElementById("pancard").style.backgroundColor = "#fff";
		document.getElementById("pancard").readOnly = false;
	}	
}

</script>
</body>

</html>
