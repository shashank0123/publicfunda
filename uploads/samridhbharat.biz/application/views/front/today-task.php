<?php $this->load->view('front/layout/header-inner'); ?>
<style>
.inactiveClick{color: #c5c5c5 !important;}
</style>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Today's Task</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Today's Task</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12">
							<section class="panel">
									<header class="panel-heading">
										<div class="panel-actions">
                                            <p><strong>Today's Earned CTP Points :</strong> <?php echo count($TOTALCLICKEDLINK); ?></p>
										</div>						
										<h2 class="panel-title">Today's Task <button type="submit" class="btn btn-warning">Redeem CTP Points</button></h2>                                       
									</header>
									
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table mb-none">
												<thead>
													<tr>
														<th>Sr No.	</th>
														<th>Page Title</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=0; foreach($todayTaskLink as $task){ $i++; ?>
													<?php $campaign = $this->campaign_model->selectCampaignByID($task->link_id); ?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $campaign[0]->campaignTitle; ?></td>
														<td>
															<?php if($task->status==1){ ?>
																<button type="button" class="btn btn-danger">Panding</button>
															<?php }else{ ?>
																<button type="button" class="btn btn-success">Clicked</button>
															<?php } ?>
                                                        </td>														
														<td class="actions">
															<ul class="notifications">
																<?php if($task->status==1){ ?>
																<li><a href="javascript:void(0);" onclick="getTaskWindow('<?php echo $campaign[0]->campaignUrl; ?>','<?php echo $task->id; ?>','<?php echo $task->link_id; ?>')" class="notification-icon" rel="tooltip"  data-original-title="Click"> <i class="fa fa-hand-o-up"></i></a></li>                                                           
																<?php }else{ ?>
																<li><a href="javascript:void(0);" class="notification-icon" rel="tooltip"  data-original-title="Clicked"> <i class="fa fa-hand-o-up inactiveClick"></i></a></li>
																<?php } ?>
																<li><a href="javascript:void(0);" onclick="return refreshLinkCampaign('<?php echo $task->id; ?>','<?php echo $task->link_id; ?>','<?php echo $task->user_id; ?>');" class="refresh" rel="tooltip" data-original-title="Refresh"><i class="fa fa-refresh"></i></a></li>
															</ul>                                                            
														</td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</section>                         
						</div>
						

					</div>
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Upcoming Tasks</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
			
								<ul>
									<li>
										<time datetime="2014-04-19T00:00+00:00">04/19/2014</time>
										<span>Company Meeting</span>
									</li>
								</ul>
								
							</div>
						</div>
					</div>
				</div>
			</aside>
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>

<script>
/*function myFunction(winUrl) {
    var myWindow = window.open(winUrl, "", "width=250");
	setTimeout(function () { myWindow.close(); }, 10000);	
}*/

 function getTaskWindow(url, linkID, campaignID) 
 {
    var lnkFlag = JSON.stringify(lnkFlag);
	var w  = 1024;
    var h = 700;
	 var left = (window.screen.width / 2) - ((w / 2) + 10);
    var top = (window.screen.height / 2) - ((h / 2) + 50);
    myWindow = window.open(url,/* this.href, */"myWindow", "width=" + w + ",height=" + h + ",resizable=yes,left="
    + left + ",top=" + top + ",screenX=" + left + ",screenY=" + top + "");
    CloseTimeout = setTimeout('updateMyTask(' + linkID + ',' + campaignID +');', 20000);
    interval = window.setInterval(function () 
	{
		try {
				if (myWindow == null || myWindow.closed) 
				{
					window.clearInterval(interval);
					clearTimeout(CloseTimeout);
				}
			}
			catch (e) {}
     }, 1000);
}

function updateMyTask(LID,CID)
{
	 myWindow.close();
	 jQuery.ajax({
            url: "<?php echo base_url('index.php/user/updateMyTask'); ?>",
            data:'LID='+LID+'&CID='+CID,
            type: "POST",
            success:function(mydata){
				window.location.reload();	
               // alert(mydata);
            },
            error:function (){}
        });	
}

function refreshLinkCampaign(LID, CID, UID)
{
	 jQuery.ajax({
            url: "<?php echo base_url('index.php/user/updateMyTask'); ?>",
            data:'LID='+LID+'&CID='+CID+'&UID='+UID,
            type: "POST",
            success:function(mydata){
				window.location.reload();
            },
            error:function (){}
        });	
}
</script>	
	

	</body>


</html>