<?php if(count($cstatus)>0){
	$totalviews = 0;
	foreach($cstatus as $status1)
	{ 
	  if($status1->status==0)
	  {
		  $totalviews +=1;
	  }
		
	}										
?>
<div class="col-md-4 pl-0 mb-10">
                  		 <table class="table table-bordered table-striped table-condensed mb-none">
                            	 	<tbody>
											<tr>
												<th>Campaign ID</th>
												<td><?php echo $_POST['cid']; ?></td>
											</tr>
											
                                            <tr>
												<th>Campaign Views </th>
												<td><?php echo $totalviews; ?></td>
											</tr>
                                    </tbody>
                        </table>
                    </div>         
                             <div class="table-responsive">
                            	
									<table class="table table-bordered table-striped table-condensed mb-none">
                                    
										<thead>
											<tr>
												<th>Date</th>
												<th>Views Status</th>
												<th>User ID</th>
                                                <th>Campaign ID</th>
                                                <th>IP Address Server</th>
                                                <th>Time</th>											
											</tr>
										</thead>
										<tbody>
											<?php foreach($cstatus as $status){ ?>
											
											<tr>
												<td><?php echo $status->distributionDate; ?></td>
												<td>
													<?php if($status->status==1){ ?>
														<button type="button" class="btn btn-danger btn-xs"><b>Pending</b></button>
													<?php }else{ ?>
														<button type="button" class="btn btn-success btn-xs"><b>Clicked</b></button>
													<?php } ?>
												</td>
												<td><?php echo $status->user_id; ?></td>												
                                                <td><?php echo $status->link_id; ?></td>
                                                <td><?php echo $status->clickedIP; ?></td>
                                                <td><?php echo date('d-m-Y h:i:s a',$status->clickDate); ?> </td>
                                            </tr>
											<?php } ?>
										</tbody>                                        	
                                            
									</table>
                                    
								</div>
<?php }else{ ?>
<div class="alert alert-danger"><strong>No Record Found!</strong></div>
<?php } ?>								