<?php $opportunity = $this->block_model->selectBlockById(3); ?>
<?php $copyright = $this->block_model->selectBlockById(4); ?>
<?php $newslatter = $this->block_model->selectBlockById(5); ?>
 <div class="container-wrap">
                <div class="container container-fullwidth">
                	<div class="row bg-primary-overlay bg-image bg-parallax pt-5  pb-5">
                                    <div class="col-md-12 parallax-content">
                                        <div class="container">
                                            <div class="row pt-2 pb-0">
                                                
                                                <div class="col-md-12 col-sm-12 ">
                                                    <div class="noo-text-block">
                                                        <div class="app-section white">
                                                            <div id="opporitydiv" > 
    <span class=""> <?php echo $opportunity[0]->description; ?> </span>
    <br/>
    <span class="colgren"> <a href="#" class="btn btn-primary"  data-rel="registerModal">Enroll Now</a></span>
    </div>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="parallax app" data-parallax="1" data-parallax_no_mobile="1" data-velocity="0.1" style="background-position: 50% 2px;"></div>
                                </div>
                </div>
            </div>
			
<div class="colophon wigetized">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="widget widget_text">
								<h4 class="widget-title">Usefull Links</h4>
								<div class="textwidget">
								<ul>
									<li><a href="<?php echo base_url('terms-condition'); ?>">Terms & Conditions </a></li>
									<li><a href="<?php echo base_url('privacy-policy'); ?>">Privacy Policy</a></li>
									<li><a href="<?php echo base_url('faq'); ?>">FAQ</a></li>
									<li><a href="<?php echo base_url('task-holidays'); ?>">Task Holidays</a></li>
									<li><a href="<?php echo base_url('legal-documents'); ?>">Legal Documents</a></li>
									<li><a href="<?php echo base_url('bank-details'); ?>">Bank Details</a></li>
								</ul>
								
                                </div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="widget widget_text">
								<h4 class="widget-title">Contact Details</h4>
								<div class="textwidget">
									<p><?php $contactdetails = $this->cms_model->selectCmsById(11); ?>
										<?php echo $contactdetails[0]->description ?>
									</p>
								</div>
							</div>
						</div>
						<div class="col-sm-4" id="newsletterdiv">
							<div class="widget mailchimp-widget">
								<h4 class="widget-title">NEWSLETTER</h4>
								<form class="mc-subscribe-form" method="post" action="<?php echo base_url('index.php/newsletter/savenewsletter'); ?>">
									<label for="email">
										<?php echo $newslatter[0]->description; ?>
									</label> </br>

                                                                       <label for="email">
										<?php echo $this->session->flashdata('message'); ?>
									</label>
									 <div class="mc-contact-wrap">
										<input type="contact" name="contact" class="form-control mc-email" value="" required placeholder="Contact No."/>
									</div>
									
									<div class="mc-email-wrap">
										<input type="email"  name="email" class="form-control mc-email" value="" required placeholder="Email address"/>
									</div> </br>
									<button type="submit" name="saveNewsLetter" class="btn btn-default">Submit</button>
								</form>
							</div>
						</div>
					</div>  
				</div>  
			</div>  
			<footer class="colophon site-info">
				<div class="container-full">
					<div class="footer-more">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="noo-bottom-bar-content">
										<?php echo $copyright[0]->description; ?>									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>  
			</footer>   
		<!--</div>-->  
		<a href="#" class="go-to-top hidden-print"><i class="fa fa-angle-up"></i></a>

<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>/js/jquery.js'></script>
		<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>/js/bootstrap.min.js'></script>
		<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>/js/script.js'></script>
		<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>/js/chosen.jquery.min.js'></script>
		<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>/js/modernizr-2.7.1.min.js'></script>
		<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>/js/jquery.carouFredSel-6.2.1-packed.js'></script>
		<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>/js/custom.js'></script>
		<script type="text/javascript" src="<?php echo base_url('assets/front/'); ?>/js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/front/'); ?>/js/jquery-migrate-1.2.1.min.js"></script> 
		<script type="text/javascript" src="<?php echo base_url('assets/front/'); ?>/js/jquery.fancybox-1.3.4.pack.min.js"></script> 
		
      <script type="text/javascript">
    $(function($){
        var addToAll = true;
        var gallery = true;
        var titlePosition = 'inside';
        $(addToAll ? '' : '.fancybox').each(function(){
            var $this = $(this);
            var title = $this.attr('title');
            var src = $this.attr('data-big') || $this.attr('src');
            var a = $('<a href="#" class="fancybox"></a>').attr('href', src).attr('title', title);
            $this.wrap(a);
        });
        if (gallery)
            $('a.fancybox').attr('rel', 'fancyboxtest3');
        $('a.fancybox').fancybox({
            titlePosition: titlePosition
        });
    });
    $.noConflict(true);
</script>
		
		<script>
			jQuery('document').ready(function ($) {
				$("#slider_employer").carouFredSel({
					responsive	: true,
					auto 		: false,
					items		: {
						width		: 180,
						height		: "variable",
						visible		: {
							min			: 1,
							max			: 6
						}
					},
					pagination: { container : ".pag_slider_employer", keys	: true }
				});
				$("#slider_testimonial").carouFredSel({
					responsive	: true,
					auto 		: false,
					items		: {
						width		: 180,
						height		: "variable",
						visible		: {
							min			: 1,
							max			: 3
						}
					},
					pagination: { container : ".pag_slider_testimonial", keys	: true }
				});
				$('.job-slider .posts-loop-content').carouFredSel({
					infinite: true,
					circular: true,
					responsive: true,
					debug : false,
					items: {
					  start: 0
					},
					scroll: {
					  items: 1,
					  duration: 400,
					  fx: "crossfade"
					},
					auto: {
					  timeoutDuration: 3000,
					  play: true
					},
					swipe: {
					  onTouch: true,
					  onMouse: true
					},
					prev : "#prev",
    				next : "#next"
				});
			});
		</script>
        
        
          <script>
			jQuery('document').ready(function ($) {
				$('#noo-slider-3 .sliders').carouFredSel({
					infinite: true,
					circular: true,
					responsive: true,
					debug : false,
					items: {
					  start: 0
					},
					scroll: {
					  items: 1,
					  duration: 400,

					  fx: "scroll"
					},
					auto: {
					  timeoutDuration: 3000,

					  play: true
					},

					pagination: {
					  container: "#noo-slider-3-pagination"
					},
					swipe: {
					  onTouch: true,
					  onMouse: true
					}
				});
				$('#noo-tabs-2 a:eq(0)').tab('show');
			});
		</script>
		

		<div class="memberModalLogin modal fade" id="userloginModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-member">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Login</h4>
					</div>
					<div class="modal-body">
						
						<form class="noo-ajax-login-form form-horizontal" id="loginform">
						
							<div class="form-group row">
								<div class="col-sm-9">
									<span id="errorlogin"></span>
								</div>
							</div>
							<div class="form-group row">								
								<label class="col-sm-3 control-label">Enter Your User ID Here</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="sponsorid" name="sponsorid" placeholder="Enter Your User ID Here">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 control-label">Enter Your Password</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="sponserpassword" name="password"  placeholder="Enter Your Password">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-9 col-sm-offset-3">
									<div class="checkbox">
										<div class="form-control-flat">
											<label class="checkbox">
												<input type="checkbox" class="rememberme" name="rememberme" value="forever">
												<i></i> Remember Me
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="form-actions form-group text-center">
								<button type="button" class="btn btn-primary" onclick="return userLogin();">Sign In</button>
								<div class="login-form-links">
									<span><a href="#" ><i class="fa fa-question-circle"></i> Forgot Password?</a></span>
									<span>
										Don't have an account yet? 
										<a href="#" class="member-register-link" data-rel="registerModal">
											Register Now <i class="fa fa-long-arrow-right"></i>
										</a>
									</span>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="memberModalRegister modal fade" id="userregisterModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog  modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Enroll to "Samridh Bharat"</h4>
					</div>
					<div class="modal-body">
						<form method="post" id="regform" class="noo-ajax-register-form form-horizontal">
                        <div class="row">
                        <div class="col-md-6">
                        
							<div class="form-group  user_login_container">
								<input type="text" class="form-control" id="user-name" name="name"  placeholder="Enter your Name here">
							</div>
                            
							<div class="form-group ">
								<input type="email" class="form-control" id="user-email" name="email" placeholder="Enter your E-mail ID here">
							</div>
                            
                            <div class="form-group">
								<input type="text" class="form-control" id="user-contact_no" name="contact_no" placeholder="Enter your Contact Number ">
							</div>
                            <div class="form-group">
								<input type="text" class="form-control" id="user-pancard_no" name="pancard_no" placeholder="Enter  Pencard Number">								
							</div>
                            <div class="form-group">
								<div class="checkbox">
									<div class="form-control-flat">
										<label class="checkbox">
											<input class="account_reg_term" value="1" id="nopancard" name="applied_pancard" onclick="return checkpancard();" type="checkbox" title="Please agree with the term">
											<i></i>
											Have your Applied for your Pan Card ?  
										</label>
									</div>
								</div>
							</div>
                            
							<div class="form-group">
								<input type="text" id="user-nominee" class="form-control" required name="nominee" placeholder="Enter your Nominee Name">
							</div>
							
                           
                            </div>
                            
                           <div class="col-md-6">
                            
							 <div class="form-group">
								<input type="text" class="form-control" id="user-sponsor_id"  required name="sponsor_id" placeholder="Enter Sponsor ID">
							</div>
                            
                            <div class="form-group">
								<input type="text" class="form-control" id="user-sponsor_name" name="sponsor_name" placeholder="Enter Sponsor Name">
							</div>	
                            <div class="form-group">
								<div class="form-control-flat">
										<select id="user-postion" class="user_role" name="postion" required>
											<option value="">-- Select Position --</option>
											<option value="R">Right</option>
											<option value="L">Left</option>
										</select>
										<i class="fa fa-caret-down"></i>
									</div>
							</div>
                            
                            <div class="form-group">
								<div class="form-control-flat">
										<select id="user-plan" class="user_role" name="plan" required >
											<option value="">-- Choose Your Plan --</option>
											<?php foreach($this->plan_model->selectAllActivePlan() as $planselect){ ?>
											<option value="<?php echo $planselect->id; ?>"><?php echo $planselect->title; ?></option>
											<?php } ?>
										</select>
										<i class="fa fa-caret-down"></i>
									</div>
							</div>
                            
							<div class="form-group">
								<input type="password" id="password" class="form-control" name="password" placeholder="Password">
							</div>
                            
							<div class="form-group">
								<input type="password" id="cpassword" class="form-control" name="cpassword" placeholder="Repeat password">
							</div>                            
                           
                            </div>
                            
                            </div>
							
							<div class="form-group text-center">
								<div class="checkbox account-reg-term">
									<div class="form-control-flat">
										<label class="checkbox">
											<input class="account_reg_term" type="checkbox" id="term-condition" title="Please agree with the term">
											<i></i>
											I have read the given website Details And Agrees to the <a href="#">Terms And Condition</a>
										</label>
									</div>
								</div>
								<button type="button" onclick="return registrationform();" class="btn btn-primary" name="userRegistr" >Sign Up</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

<script>
function registrationform()
{
	var name = document.getElementById('user-name');
	var email = document.getElementById('user-email');
	var email_pattern = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
	
	var contactno = document.getElementById('user-contact_no');	
	var nominee = document.getElementById('user-nominee');
	var sponsor_id = document.getElementById('user-sponsor_id');
	var sponsor_name = document.getElementById('user-sponsor_name');
	var postion = document.getElementById('user-postion');	
	var plan = document.getElementById('user-plan');
	var password = document.getElementById('password');
	var cpassword = document.getElementById('cpassword');
		
	if(name.value=="")
	{
		name.focus(); name.style.borderColor  = "red"; return false;
	}
	
	if(email.value=="" || !email_pattern.test(email.value))
	{
		email.focus();	email.style.borderColor  = "red";	return false;
	}
	
	if(contactno.value=="")
	{
		contactno.focus();	contactno.style.borderColor  = "red";	return false;
	}
	
	if(nominee.value=="")
	{
		nominee.focus();	nominee.style.borderColor  = "red";	return false;
	}
	
	if(sponsor_id.value=="")
	{
		sponsor_id.focus();	sponsor_id.style.borderColor  = "red";	return false;
	}
	
	if(sponsor_name.value=="")
	{
		sponsor_name.focus(); sponsor_name.style.borderColor  = "red";	return false;
	}
	
	if(postion.value=="")
	{
		postion.focus();	postion.style.borderColor  = "red";	return false;
	}
	
	if(plan.value=="")
	{
		plan.focus();	plan.style.borderColor  = "red";	return false;
	}
	
	if(password.value=="")
	{
		password.focus();	password.style.borderColor  = "red";	return false;
	}
	
	if(cpassword.value=="")
	{
		cpassword.focus();	cpassword.style.borderColor  = "red";	return false;
	}
	
	if(cpassword.value!=password.value)
	{
		cpassword.focus();	cpassword.style.borderColor  = "red";	return false;
	}
	
	var formdata = jQuery("#regform").serialize();
	jQuery.ajax({
			url: "<?php echo base_url('index.php/user/adduser'); ?>",
			data: formdata,			
			type: "POST",
			success:function(data){			
				alert('data saved successfully');
				//jQuery("#regform").reset();
			},
			error:function (){
				alert('error');			
			}
        });
}


function userLogin()
{
	var id = document.getElementById('sponsorid');
	var password = document.getElementById('sponserpassword');
	
	if(id.value=="")
	{
		id.focus(); id.style.borderColor  = "red"; return false;
	}
	if(password.value=="")
	{
		password.focus(); password.style.borderColor  = "red"; return false;
	}
	
	var formdata = jQuery("#loginform").serialize();
	jQuery.ajax({
			url: "<?php echo base_url('index.php/user/login'); ?>",
			data: formdata,			
			type: "POST",
			success:function(data)
			{
				if(data==1)	
				{
					window.location = "<?php echo base_url('index.php/user/dashboard'); ?>";
					//document.getElementById('errorlogin').innerHTML ="ok";
				}
				else
				{
					document.getElementById('errorlogin').innerHTML ='<div class="alert alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Invalid login details !</div>';
				}	
			},
			error:function (){
				alert(data);			
			}
        });
}

function checkpancard()
{
	if(document.getElementById("nopancard").checked==true)
	{
		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');
		document.getElementById("user-pancard_no").style.backgroundColor = "#ccc";
		document.getElementById("user-pancard_no").readOnly = true;
	}
	else
	{
		document.getElementById("user-pancard_no").style.backgroundColor = "#fff";
		document.getElementById("user-pancard_no").readOnly = false;
	}	
}
</script>
<!--- Developed by Naseem Ahmad || e-mail anaseem711@gmail.com --->
	</body>

</html>