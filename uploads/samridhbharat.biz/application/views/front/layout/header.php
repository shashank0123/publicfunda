<!doctype html>
<html lang="en-US">
	
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
		<link rel="shortcut icon" href="images/favicon.ico"/>
		<title>Samridh Bharat Home</title>

		<link rel='stylesheet' href='<?php echo base_url('assets/front/'); ?>/css/style.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='<?php echo base_url('assets/front/'); ?>/css/custom.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='<?php echo base_url('assets/front/'); ?>/css/font-awesome.min.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='<?php echo base_url('assets/front/'); ?>/css/jquery.datetimepicker.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='<?php echo base_url('assets/front/'); ?>/css/chosen.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='<?php echo base_url('assets/front/'); ?>/css/font.css' type='text/css' media='all'/>
		<link id="style-main-color" rel="stylesheet" href="<?php echo base_url('assets/front/'); ?>/css/colors/default.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/'); ?>/css/style5.css" />		
	
	</head>
	
	
	
	<body>
		<div class="site">
                <!--- <?php echo base64_encode('Developed by Naseem Ahmad || e-mail anaseem711@gmail.com'); ?> --->
        	<div class="header-top">                        
				<div class="container">                            
					<div class="row"> 
						<?php $header1 = $this->block_model->selectBlockById(7); ?>
						<?php $header2 = $this->block_model->selectBlockById(8); ?>
						<?php $header3 = $this->block_model->selectBlockById(9); ?>
						<div class="col-lg-5 col-md-6 col-sm-4 hidden-xs">
							<span><?php echo $header1[0]->description; ?></span>                                
						</div>  
						
						<div class="col-lg-4 col-md-6 col-sm-7 col-xs-6">                                    
							<div class="header-top-right pull-right">
							<span class="phone"><i class="fa fa-phone"></i> <?php echo $header2[0]->description; ?></span>
							<span class="pl-30 hidden-xs"><i class="fa fa-envelope"></i> <?php echo $header3[0]->description; ?></span>                                    
							</div>                                
						</div> 

                                    <div class="col-lg-3 col-md-6 col-sm-6">				
				        <?php $admindata = $this->admin_model->checkAdminByID(1); ?>
	                   <aside id="sticky-social">
                         <p><a href="<?php echo $admindata[0]->facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a></p>
                         <p><a href="<?php echo $admindata[0]->pinterest; ?>" target="_blank"><i class="fa fa-pinterest"></i></a></p>
						 <p><a href="<?php echo $admindata[0]->twitter; ?>" target="_blank"><i class="fa fa-twitter"></i></a></p>
                         <p><a href="<?php echo $admindata[0]->google; ?>" target="_blank"><i class="fa fa-google"></i></a></p>
						 <p><a href="<?php echo $admindata[0]->tumbler; ?>" target="_blank"><i class="fa fa-tumblr"></i></a></p>
                         <p><a href="<?php echo $admindata[0]->youtube; ?>" target="_blank"><i class="fa fa-youtube"></i></a></p>  
                         <p><a href="<?php echo $admindata[0]->stumble; ?>" target="_blank"><i class="fa fa-stumbleupon"></i></a></p>
                       </aside>
					 </div>  		 
						
					</div>                        
				</div>                    
			</div>
      
			<header class="noo-header" id="noo-header">
				<div class="navbar-wrapper">
					<div class="navbar navbar-default fixed-top shrinkable">
						<div class="container">
							<div class="navbar-header">
								<h1 class="sr-only">JobMonster</h1> 
								<a class="navbar-toggle collapsed" data-toggle="collapse" data-target=".noo-navbar-collapse">
									<span class="sr-only">Navigation</span>
									<i class="fa fa-bars"></i>
								</a>
								<a class="navbar-toggle member-navbar-toggle collapsed" data-toggle="collapse" data-target=".noo-user-navbar-collapse">
									<i class="fa fa-user"></i>
								</a> <?php $logo = $this->admin_model->checkAdminByID(1); ?>
								<a href="<?php echo base_url('index.php'); ?>" class="navbar-brand">									
									<img class="noo-logo-img noo-logo-normal" src="<?php echo base_url('uploads/image/'.$logo[0]->logo); ?>" alt="">
									<img class="noo-logo-mobile-img noo-logo-normal" src="<?php echo base_url('uploads/image/'.$logo[0]->logo); ?>" alt="">
								</a>
							</div>  
							<nav class="collapse navbar-collapse noo-user-navbar-collapse">
								<ul class="navbar-nav sf-menu">
									<li>
										<a href="https://campaigntrade.com/index.php/user/login"><i class="fa fa-sign-in"></i> Login</a>
									</li>
									
									<li>
                                       <a  href="https://campaigntrade.com/index.php/user/signup"><i class="fa fa-sign-in"></i> Register</a>
                                    </li>
									
								</ul>
							</nav>
							<nav class="collapse navbar-collapse noo-navbar-collapse">
								<ul class="navbar-nav sf-menu">
									<li class="<?php echo($active=='home')?'current-menu-item':''; ?> align-left">
										<a href="<?php echo base_url(''); ?>">Home</a>
									</li>
									<li class="<?php echo($active=='aboutus')?'current-menu-item':''; ?> align-left">
										<a href="<?php echo base_url('about-us'); ?>">About Us</a>
									</li>
									<li class="<?php echo($active=='whyus')?'current-menu-item':''; ?> align-left">
										<a href="<?php echo base_url('why-us'); ?>">Why Us</a>
									</li>
									<li class="<?php echo($active=='plans')?'current-menu-item':''; ?> align-left">
										<a href="<?php echo base_url('plan'); ?>">Plans</a>
									</li>
									<li class="<?php echo($active=='contactus')?'current-menu-item':''; ?> align-left">
										<a href="<?php echo base_url('contact-us'); ?>">Contact US</a>
									</li>
									<?php $user_id = $this->session->userdata('user_id');  ?>
									<?php if(empty($user_id)){ ?>
									<li class="nav-item-member-profile login-link align-center">
										<a href="https://campaigntrade.com/index.php/user/login" class="member-links member-login-link" >
											<i class="fa fa-sign-in"></i>&nbsp;Login
										</a>
									</li>
									<li class="nav-item-member-profile register-link">
										<a class="member-links member-register-link" href="https://campaigntrade.com/index.php/user/signup<?php //echo base_url('app/user/signup'); ?>">
											<i class="fa fa-key"></i>&nbsp;Register
										</a>
									</li>
									<?php }else{ ?>
									<li class="nav-item-member-profile login-link align-center">
										<a href="<?php echo base_url('index.php/user/dashboard'); ?>" class="member-links member-login-link">
											<i class="fa fa-user"></i>&nbsp; My Profile
										</a>
									</li>
									<li class="nav-item-member-profile register-link">
										<a class="member-links member-register-link" href="https://campaigntrade.com/index.php/user/signup<?php //echo base_url('index.php/user/logout'); ?>">
											<i class="fa fa-sign-out"></i>&nbsp; Logout
										</a>
									</li>
									<?php } ?>
								</ul>
							</nav>  
						</div>  
					</div>  
				</div>
			</header>