<?php $this->load->view('front/layout/header'); ?>

<div class="container-wrap">
				<img src="<?php echo base_url('uploads/cms/'.$FAQCONTENT[0]->image); ?>" class="img-full">
				</div>
            
<div class="container-wrap">
            <div class="main-content container-fullwidth">
            <div class="row">
<div class="col-md12">
<div class="marquesec">
<marquee class="text-marquee" scrollamount="10"><?php echo $FAQCONTENT[0]->slide_text; ?></marquee>
</div>
</div> 
</div>
</div>
</div>

			<div class="row__ pt-10 pb-5 bg-white">
								<div class="container-boxed max">
                                <div class="col-md-12 col-sm-12">
					
					<div class="jobs posts-loop jobs-shortcode">
											<div class="posts-loop-title text-center">
												<h3><?php echo $FAQCONTENT[0]->title; ?></h3>
											</div>
                                  		</div>
										
											
                        <div class="job-package clearfix">
								<div class="noo-pricing-table classic pricing-4-col package-pricing">
									<?php foreach($PLANDATA as $plan){ ?>
									<div class="noo-pricing-column featured1">
										<div class="pricing-content">
											<div class="pricing-header">
												<h4 class="pricing-title"><?php echo $plan->title; ?></h4>
												<h3 class="pricing-value">
													<span class="noo-price"><span class="amount"><i class="fa fa-rupee"></i> <?php echo $plan->amount; ?></span></span>
												</h3>
											</div>
											<div class="pricing-info">
												<ul class="noo-ul-icon fa-ul">
													<li class="noo-li-icon">
														<i class="fa fa-check-circle"></i> <?php echo $plan->text1; ?>
													</li>
													<li class="noo-li-icon">
														<i class="fa fa-check-circle"></i> <?php echo $plan->text2; ?>
													</li>
													<li class="noo-li-icon">
														<i class="fa fa-check-circle"></i> <?php echo $plan->text3; ?>
													</li>
                                                    <li class="noo-li-icon">
                                                    <i class="fa fa-check-circle"></i> <?php echo $plan->text4; ?>
                                                    
                                                    </li>
                                                    
												</ul>
											</div>
											<div class="pricing-footer">
									<a class="btn btn-lg btn-primary" href="<?php echo base_url('app/user/signup?plan='.$plan->id); ?>" class="member-links member-register-link">Select</a>
											</div>
										</div>
									</div>
									<?php } ?>
									
									
								</div>
							</div>							
					</div>  
                    
				</div> 
          
			</div>
<?php $this->load->view('front/layout/footer'); ?>