<?php $this->load->view('front/layout/header'); ?>

<div class="container-wrap">
				<img src="<?php echo base_url('uploads/cms/'.$FAQCONTENT[0]->image); ?>" class="img-full">
				</div>
            
<div class="container-wrap">
            <div class="main-content container-fullwidth">
            <div class="row">
<div class="col-md12">
<div class="marquesec">
<marquee class="text-marquee" scrollamount="10"><?php echo $FAQCONTENT[0]->slide_text; ?></marquee>
</div>
</div> 
</div>
</div>
</div>

			<div class="container-wrap">
           
				<div class="container-boxed max offset main-content single-noo_job">
                
					<div class="row">
					<div class="jobs posts-loop jobs-shortcode">
						<div class="posts-loop-title text-center">
							<h3><?php echo $FAQCONTENT[0]->title; ?></h3>
						</div>
                    </div>
											
											
                        <div class="noo-main col-md-9">
							<div class="job-listing">
								<div class="jobs posts-loop">
									
									<div class="posts-loop-content">
										<div class="panel-group" id="accordion">
<?php $i=0; foreach($FAQDATA as $faqdata){ $i++; ?>										
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>">Q.<?php echo $i; ?>.	<?php echo $faqdata->title; ?></a>
      </h4>
    </div>
    <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse <?php echo($i==1)?'in':''; ?>">
      <div class="panel-body">
      Ans. - <?php echo $faqdata->description; ?>

</div>
    </div>
  </div>
<?php } ?> 
  
  
</div>
									</div>
								</div>
							</div>
						</div>  
						<div class="noo-sidebar col-md-3">
							<div class="noo-sidebar-wrap">
								<?php if($add1[0]->type=='youtube'){ ?>
	<iframe class="videosec" src="https://www.youtube.com/embed/<?php echo $add1[0]->youtube; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>
<?php } ?>
<?php if($add1[0]->type=='flash'){ ?>
	<object width="100%" data="<?php echo base_url('uploads/flash/'.$add1[0]->flash); ?>"></object>
<?php } ?>	
<?php if($add1[0]->type=='slider'){ ?>
	 <center><img src="<?php echo base_url('uploads/image/'.$add1[0]->image); ?>"></center>
<?php } ?>
							</div>
						</div>
                        <div class="col-md-12 mt-5"	>
                          <?php if($add2[0]->type=='youtube'){ ?>
									<iframe class="videosec" src="https://www.youtube.com/embed/<?php echo $add2[0]->youtube; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>
								<?php } ?>
								<?php if($add2[0]->type=='flash'){ ?>
									<object width="100%" data="<?php echo base_url('uploads/flash/'.$add2[0]->flash); ?>"></object>
								<?php } ?>	
								<?php if($add2[0]->type=='slider'){ ?>
									 <center><img src="<?php echo base_url('uploads/image/'.$add2[0]->image); ?>"></center>
								<?php } ?>
                        </div>
					</div>  
                    
				</div> 
          
			</div>
<?php $this->load->view('front/layout/footer'); ?>