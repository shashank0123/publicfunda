<?php
class Employer_model extends CI_Model
{
	function selectAllEmployer()
	{
		$data= $this->db->get("tbl_employer");
		return $data->result();		
	}
	
	function selectAllActiveEmployer()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_employer");
		return $data->result();		
	}
	
	
	function selectEmployerbyid($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_employer');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_employer', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_employer',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_employer');
	}
}
?>