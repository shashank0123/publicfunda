<?php
class Ducuments_model extends CI_Model
{
	function selectAllDucuments()
	{
		$data= $this->db->get("tbl_documents");
		return $data->result();		
	}
	
	function selectAllActiveDucuments()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_documents");
		return $data->result();		
	}
	
	
	function selectDucumentsbyid($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_documents');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_documents', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_documents',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_documents');
	}
}
?>