<?php
class Plan_model extends CI_Model
{
	function selectAllPlan()
	{
		$data= $this->db->get("tbl_plan");
		return $data->result();		
	}
	
	function selectAllActivePlan()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_plan");
		return $data->result();		
	}
	
	function selectPlanById($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_plan');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_plan', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_plan',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_plan');
	}
}
?>