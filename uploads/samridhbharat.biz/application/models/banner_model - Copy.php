<?php
class Banner_model extends CI_Model
{
	function selectAllBanner()
	{
		$data= $this->db->get("tbl_banner");
		return $data->result();		
	}
	
	function selectAllActiveBanner()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_banner");
		return $data->result();		
	}
	
	function selectAllHomeBanner()
	{
		$this->db->where('display_home', 1);
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_banner");
		return $data->result();		
	}
	
	function selectbannerbyid($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_banner');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_banner', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_banner',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_banner');
	}
}
?>