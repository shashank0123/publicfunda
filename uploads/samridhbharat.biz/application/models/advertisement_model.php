<?php
class Advertisement_model extends CI_Model
{
	function selectAllBanner()
	{
		$data= $this->db->get("tbl_advertisement");
		return $data->result();		
	}
	
	function selectAllActiveBanner()
	{
		//$this->db->where('status', 1);
		$data= $this->db->get("tbl_advertisement");
		return $data->result();		
	}
	
	function selectAllHomeBanner()
	{
		//$this->db->where('display_home', 1);
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_advertisement");
		return $data->result();		
	}
	
	function selectbannerbyid($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_advertisement');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_advertisement', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_advertisement',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_advertisement');
	}
}
?>