<?php
class News_model extends CI_Model
{
	function selectAllNews()
	{
		$data= $this->db->get("tbl_news");
		return $data->result();		
	}
	
	function selectAllActiveNews()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_news");
		return $data->result();		
	}
	
	function selectNewsById($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_news');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_news', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_news',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_news');
	}
}
?>