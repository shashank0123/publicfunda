<?php 
class Admin_model extends CI_Model
{
	var $admin_table = 'tbl_admin';
	public function adminLogin($username,$password)
	{
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByEmail($email)
	{
		$this->db->where('email',$email);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByToken($key)
	{
		$this->db->where('forgot_password',$key);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function checkAdminByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->admin_table);
		return $data->result();		
	}
	
	public function updateAdminById($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->admin_table,$data);	
	}
}