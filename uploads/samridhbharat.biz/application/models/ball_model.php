<?php
class Ball_model extends CI_Model
{
	function selectAllBall()
	{
		$data= $this->db->get("tbl_ball");
		return $data->result();		
	}
	
	function selectAllActiveBall()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_ball");
		return $data->result();		
	}
	
	function selectBallById($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_ball');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_ball', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_ball',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_ball');
	}
}
?>