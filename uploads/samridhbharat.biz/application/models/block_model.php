<?php
class Block_model extends CI_Model
{
	function selectAllBlock()
	{
		$data= $this->db->get("tbl_block");
		return $data->result();		
	}
	
	function selectAllActiveBlock()
	{
		$this->db->where('status', 1);
		$data= $this->db->get("tbl_block");
		return $data->result();		
	}
	
	function selectBlockById($id)
	{
		$this->db->where('id', $id);
		$data= $this->db->get('tbl_block');
		return $data->result();
	}
	
	function insert($data)
	{
		$result= $this->db->insert('tbl_block', $data);
		return $result;
	}
	
	function update($id,$data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tbl_block',$data);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_block');
	}
}
?>