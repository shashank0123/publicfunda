<?php 
class User_model extends CI_Model
{
	var $user_table = 'tbl_users';
	public function insertData($data)
	{
		$returnData = $this->db->insert($this->user_table,$data);
		return  $returnData;
	}
	
	public function updateData($data)
	{
		$returnData = $this->db->update($this->user_table,$data);
		return $returnData;
	}
	
	public function getAllUsers()
	{
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	
	public function selectUserByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
	
	public function userLogin($id,$password)
	{
		$this->db->where('id',$id);
		$this->db->where('password',$password);
		//$this->db->where('status',1);
		$data = $this->db->get($this->user_table);
		return $data->result();
	}
}