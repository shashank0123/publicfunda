<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Documents extends CI_Controller 
{
	function listing()
	{		
		$data['DDATA']= $this->ducuments_model->selectAllDucuments();
		$this->load->view('admin/documents/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{		
						
			if($_FILES['image']['name']!="")
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/documents/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}			
			
			$data['title'] = $this->input->post('title');
			$data['image'] = $file; 
			$data['create_date'] = time();
				
			$this->ducuments_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/documents/listing');			
		}
		$this->load->view('admin/documents/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{
			if($_FILES['image']['name']=="")
			{
				$file= $_POST['oldimage'];
			}
			else
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/documents/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}						
						
			$data['title'] = $this->input->post('title');
			$data['image'] = $file;
			$this->ducuments_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/documents/listing');
		}
		$data['EDITD']= $this->ducuments_model->selectDucumentsbyid($args[0]);
		$this->load->view('admin/documents/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();
		//$data= $this->ducumentss->selectDucumentsbyid($args[0]);
		//$path ='media/uploads/documents/'.$data[0]->image;
		//@unlink($path);
		$this->ducuments_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/documents/listing');
	}
}