<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Employer extends CI_Controller 
{
	function listing()
	{		
		$data['DDATA']= $this->employer_model->selectAllEmployer();
		$this->load->view('admin/employer/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{		
						
			if($_FILES['image']['name']!="")
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/employer/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}			
			
			$data['title'] = $this->input->post('title');
			$data['image'] = $file; 
			$data['create_date'] = time();
				
			$this->employer_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/employer/listing');			
		}
		$this->load->view('admin/employer/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{
			if($_FILES['image']['name']=="")
			{
				$file= $_POST['oldimage'];
			}
			else
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/employer/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}						
						
			$data['title'] = $this->input->post('title');
			$data['image'] = $file;
			$this->employer_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/employer/listing');
		}
		$data['EDITD']= $this->employer_model->selectEmployerbyid($args[0]);
		$this->load->view('admin/employer/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();
		//$data= $this->employers->selectEmployerbyid($args[0]);
		//$path ='media/uploads/employer/'.$data[0]->image;
		//@unlink($path);
		$this->employer_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/employer/listing');
	}
}