<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Block extends CI_Controller 
{
	function listing()
	{		
		$data['BLOCKDATA']= $this->block_model->selectAllBlock();
		$this->load->view('admin/block/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{						
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$data['create_date'] = time();
				
			$this->block_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/block/listing');			
		}
		$this->load->view('admin/block/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{	
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$this->block_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/block/listing');
		}
		$data['EDITBLOCK']= $this->block_model->selectBlockById($args[0]);
		$this->load->view('admin/block/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();		
		$this->block_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/block/listing');
	}
	
}