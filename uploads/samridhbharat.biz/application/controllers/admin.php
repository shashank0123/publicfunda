<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	
	public function index()
	{
		$this->load->model('admin_model');
		
		if(isset($_POST['loginbutton']))
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');			
			
			if(!empty($username) && !empty($password))
			{
				$admin_data = $this->admin_model->adminLogin($username,$password);
				
				if(count($admin_data)>0)
				{
					$login_data = array(
						'admin_id'=>$admin_data[0]->id,
						'admin_username'=>$admin_data[0]->username,
						'admin_type' =>'0',
						'logged_in'=>true);						
					$this->session->set_userdata($login_data);
					redirect('index.php/admin/dashboard');
				}
				else
				{
					$this->session->set_flashdata('message','<div class="isa_error">Invalid username or password !</div>');
					redirect('index.php/admin');
				}	
			}
			else
			{
				$this->session->set_flashdata('message','<div class="isa_error">Invalid username or password !</div>');
				redirect('index.php/admin');
			}					
		}	
		$this->load->view('admin/login');
	}
	
	public function settings()
	{
		$this->load->model('admin_model');
		$admin_id = $this->session->userdata('admin_id'); 
		if(isset($_POST['updatedata']))
		{
			if($_FILES['image']['name']=="")
			{
				$file= $_POST['oldimage'];
			}
			else
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/image/'.$file;				
				move_uploaded_file($file_temp,$path);	
			}

                         $password = $this->input->post('pwd');
			$cpassword = $this->input->post('cpwd');
			if(!empty($password) && !empty($cpassword))
			{
				if($password==$cpassword)
				{
					$data['password'] = $password;
				}	
			}	
				
            $data['email'] = $this->input->post('email');
			
			$data['facebook'] = $this->input->post('facebook');
			$data['twitter'] = $this->input->post('twitter');
			$data['google'] = $this->input->post('google');
			$data['youtube'] = $this->input->post('youtube');
            $data['pinterest'] = $this->input->post('pinterest');
            $data['stumble'] = $this->input->post('stumble');
            $data['tumbler'] = $this->input->post('tumbler');										
			//$data['title'] = $this->input->post('title');
			$data['logo'] = $file;
			$this->admin_model->updateAdminById($admin_id,$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/admin/settings');
		}
		
		$data['admindata']= $this->admin_model->checkAdminByID($admin_id);
		$this->load->view('admin/settings',$data);
	}
	
	public function forgot_password()
	{
		$this->load->model('admin_model');
		error_reporting(0);
		if(isset($_POST['submitbutton']))
		{
			$email = $this->input->post('email');
			if(!empty($email))
			{
				$admin_data = $this->admin_model->checkAdminByEmail($email);
				if(count($admin_data)>0)
				{
					$token = md5($admin_data[0]->email.time().$admin_data[0]->id);
					$data['forgot_password'] = $token;
					$this->admin_model->updateAdminById($admin_data[0]->id,$data);
				
					$message = "Dear Admin Please find your login details bellow.<br>";
					$message .= "<strong>Username</strong> : ".$admin_data[0]->username.'<br>';
					$message .= "<strong>password</strong> : ".$admin_data[0]->password.'<br>';
					$message .= "Thank you <br> Samridh Bharat support team</div>";
					
					$ci = get_instance();
					$ci->load->library('email');
					$config['protocol'] = "smtp";
					$config['smtp_host'] = "ssl://smtp.gmail.com";
					$config['smtp_port'] = "465";
					$config['smtp_user'] = "phpdeveloper70@gmail.com"; 
					$config['smtp_pass'] = "9899004500";
					$config['charset'] = "utf-8";
					$config['mailtype'] = "html";
					$config['newline'] = "\r\n";

					$ci->email->initialize($config);

					$ci->email->from('info@samridhbharat.biz', 'Forgot password');
					$list = array($admin_data[0]->email);
					$ci->email->to($list);
					$ci->email->subject('forgot password');
					$ci->email->message($message);
					$ci->email->send();
					
					
					$this->session->set_flashdata('message','<div class="isa_success">Password has been send on your email address</div>');
					redirect('index.php/admin/forgot_password');
					
				}
				else
				{
					$this->session->set_flashdata('message','<div class="isa_error">Invalid email address!</div>');
					redirect('index.php/admin/forgot_password');
				}	
			}
			else
			{
				$this->session->set_flashdata('message','<div class="isa_error">Please enter email address!</div>');
				redirect('index.php/admin/forgot_password');
			}	
		}	
		
		$this->load->view('admin/forgot-password');
	}
	
	public function resetpassword()
	{
		$this->load->model('admin_model');
		$args = func_get_args();
		if(count($args)>0)
		{
			$admin_data = $this->admin_model->checkAdminByToken($args[0]);
			if(count($admin_data)>0)
			{
				
			}
			else
			{
				$this->session->set_flashdata('message','<div class="isa_error">Invalid token!</div>');
				redirect('index.php/admin/index');
			}		
		}
		else
		{
			$this->session->set_flashdata('message','<div class="isa_error">Invalid token!</div>');
			redirect('index.php/admin/index');
		}	
		$this->load->view('admin/reset-password');
	}
	
	public function dashboard()
	{
		$this->load->view('admin/dashboard');
	}
	
	public function adduser()
	{
		if(isset($_POST['saveuser']))
		{
			$email = $this->input->post('email');
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['contact_no'] = $this->input->post('contact_no');
			$data['pancard_no'] = $this->input->post('pancard_no');
			$data['pancard_no'] = $this->input->post('pancard_no');
			$data['nominee'] = $this->input->post('nominee');
			$data['sponsor_id'] = $this->input->post('sponsor_id');
			$data['sponsor_name'] = $this->input->post('sponsor_name');
			$data['postion'] = $this->input->post('postion');
			$data['plan'] = $this->input->post('plan');
			$data['registration_date'] = time();
			$data['status'] = $this->input->post('status');

                        $data['facebook'] = $this->input->post('facebook');
			$data['twitter'] = $this->input->post('twitter');
			$data['google'] = $this->input->post('google');
			$data['youtube'] = $this->input->post('youtube');
			$data['pinterest'] = $this->input->post('pinterest');
                        $data['stumble'] = $this->input->post('stumble');
                        $data['tumbler'] = $this->input->post('tumbler');
			
			$insertedData = $this->user_model->insertData($data);
			if($insertedData)
			{
				$this->email->set_newline("\r\n");
				$this->email->set_mailtype("html");
				$this->email->to($email);
				$this->email->from(ADMIN_EMAIL, 'Samridh Bharat');
				$this->email->subject(' required.');
				
				// message start				
				$message = "Dear ".$this->input->post('name').",<br>";
				$message .= "Thank you for registering with us.";
				$message .= "Your account has been created, you can login with the following credentials.<br>";
				$message .= "------------------------<br>";
				$message .= "USER ID: $email <br>";
				$message .= "PASSWORD: $password <br>";
				$message .= "------------------------<br><br>";
				$message .= "Thank you <br> Samridh Bharat support team";
				// message end
				
				$this->email->message($message);
				$this->email->send();
			}	
		}	
		$this->load->view('admin/user/add-user');
	}	
	
	
	public function edituser()
	{
		$args = func_get_args();
		if(isset($_POST['saveuser']))
		{
			$email = $this->input->post('email');
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['contact_no'] = $this->input->post('contact_no');
			$data['pancard_no'] = $this->input->post('pancard_no');
			$data['pancard_no'] = $this->input->post('pancard_no');
			$data['nominee'] = $this->input->post('nominee');
			$data['sponsor_id'] = $this->input->post('sponsor_id');
			$data['sponsor_name'] = $this->input->post('sponsor_name');
			$data['postion'] = $this->input->post('postion');
			$data['plan'] = $this->input->post('plan');
			//$data['registration_date'] = time();
			$data['status'] = $this->input->post('status');

                        $data['facebook'] = $this->input->post('facebook');
			$data['twitter'] = $this->input->post('twitter');
			$data['google'] = $this->input->post('google');
			$data['youtube'] = $this->input->post('youtube');
			$data['pinterest'] = $this->input->post('pinterest');
                        $data['stumble'] = $this->input->post('stumble');
                        $data['tumbler'] = $this->input->post('tumbler');
			
			$insertedData = $this->user_model->insertData($data);
				
		}	
		
		$data['USERDATA'] = $this->user_model->selectUserByID($args[0]);
		$this->load->view('admin/user/edit-user',$data);
	}
	
	function userList()
	{
		
		$data['USERLIST'] = $this->user_model->getAllUsers();
		$this->load->view('admin/user/list',$data);
	}
	
	
	public function viewuser()
	{	
		$args = func_get_args();
		$data['USERDATA'] = $this->user_model->selectUserByID($args[0]);
		$this->load->view('admin/user/view-user',$data);
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		$this->session->set_userdata(array('admin_id' => '', 'admin_username' => '','admin_type'=>'', 'logged_in' => ''));
		$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully lougout.</div>');
		redirect('index.php/admin');	
	}
}

