<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Advertisement extends CI_Controller 
{
	function listing()
	{		
		$data['BANNERDATA']= $this->advertisement_model->selectAllBanner();
		$this->load->view('admin/advertisement/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{		
						
			if($_FILES['image']['name']!="")
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/advertisement/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}			
					
			$data['image'] = $file; 
			$data['create_date'] = time();
				
			$this->advertisement_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/advertisement/listing');			
		}
		$this->load->view('admin/advertisement/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{
			
			$type = $this->input->post('type');			
			if($type=='youtube')
			{
				$data['youtube'] = $this->input->post('youtube');	
			}	
			
			if($type=='flash')
			{
				if($_FILES['flash']['name']=="")
				{
					$file= $_POST['oldflash'];
					$data['flash'] = $file;
				}
				else
				{
					$file = createUniqueFile($_FILES['flash']['name']);
					$file_temp = $_FILES['flash']['tmp_name'];	
					$path = 'uploads/flash/'.$file;						
					$extension=array("swf",'SWF');
					$ext=pathinfo($path,PATHINFO_EXTENSION);
					if(in_array($ext,$extension))
					{	 												
						move_uploaded_file($file_temp,$path);
						$data['flash'] = $file;
					}
				}
				
			}
			
			if($type=='slider')
			{
				if($_FILES['image']['name']=="")
				{
					$file= $_POST['oldimage'];
					$data['image'] = $file;
				}
				else
				{
					$file = createUniqueFile($_FILES['image']['name']);
					$file_temp = $_FILES['image']['tmp_name'];	
					$path = 'uploads/image/'.$file;						
					$extension=array("png",'jpg','JPEG','jpeg','gif');
					$ext=pathinfo($path,PATHINFO_EXTENSION);
					if(in_array($ext,$extension))
					{	 												
						move_uploaded_file($file_temp,$path);
						$data['image'] = $file;
					}
				}
                         $data['url'] = $this->input->post('url');
			}
			
			$data['type'] = $type;
			
			$this->advertisement_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/advertisement/listing');
		}
		$data['EDITBANNER']= $this->advertisement_model->selectbannerbyid($args[0]);
		$this->load->view('admin/advertisement/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();
		
		$this->advertisement_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/advertisement/listing');
	}
	
	
	
	function chech_admin_login()
	{
		$ci = & get_instance();
		$USERID      = $ci->session->userdata('USERID');	
		$USERNAME      = $ci->session->userdata('USERNAME');	
		$logged_in      = $ci->session->userdata('logged_in');	
		if($USERID=="" && $USERNAME=="")
		{
			redirect('index.php/admin/index');
		}
	}
}