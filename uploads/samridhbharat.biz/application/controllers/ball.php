<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ball extends CI_Controller 
{
	function listing()
	{		
		$data['BLOCKDATA']= $this->ball_model->selectAllBall();
		$this->load->view('admin/ball/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{						
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$data['create_date'] = time();
				
			$this->ball_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/ball/listing');			
		}
		$this->load->view('admin/ball/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{	
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$this->ball_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/ball/listing');
		}
		$data['EDITBLOCK']= $this->ball_model->selectBallById($args[0]);
		$this->load->view('admin/ball/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();		
		$this->ball_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/ball/listing');
	}
	
}