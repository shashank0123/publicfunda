<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class banner extends CI_Controller 
{
	function listing()
	{		
		$data['BANNERDATA']= $this->banner_model->selectAllBanner();
		$this->load->view('admin/banner/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{		
						
			if($_FILES['image']['name']!="")
			{
				$file = createUniqueFile($_FILES['image']['name']);
				$file_temp = $_FILES['image']['tmp_name'];
				$path = 'uploads/banner/'.$file;				
				move_uploaded_file($file_temp,$path);				
			}
			else
			{
				$file = "";
			}			
					
			$data['image'] = $file; 
			$data['create_date'] = time();
				
			$this->banner_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/banner/listing');			
		}
		$this->load->view('admin/banner/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{
			
			$type = $this->input->post('type');			
			if($type=='youtube')
			{
				$data['youtube'] = $this->input->post('youtube');	
			}	
			
			if($type=='flash')
			{
				if($_FILES['flash']['name']=="")
				{
					$file= $_POST['oldimage'];
					$data['flash'] = $file;
				}
				else
				{
					$file = createUniqueFile($_FILES['flash']['name']);
					$file_temp = $_FILES['flash']['tmp_name'];	
					$path = 'uploads/flash/'.$file;						
					$extension=array("swf",'SWF');
					$ext=pathinfo($path,PATHINFO_EXTENSION);
					if(in_array($ext,$extension))
					{	 												
						move_uploaded_file($file_temp,$path);
						$data['flash'] = $file;
					}
				}
				
			}
			
			if($type=='slider')
			{
								
				$extension=array("jpeg","jpg","png","gif");				
				foreach($_FILES["sliderimg"]['name'] as $key=>$val)
				{
					$file_name = $_FILES["sliderimg"]["name"][$key];
					$file_tmp = $_FILES["sliderimg"]["tmp_name"][$key];
					$path_slider = 'uploads/banner/'.$file_name;
					$ext=pathinfo($path_slider,PATHINFO_EXTENSION);
					if(in_array($ext,$extension))
					{							
						move_uploaded_file($file_tmp,$path_slider);							
						$sliderdata['slider_id'] = $args[0]; 
						$sliderdata['image'] = $file_name;
                                                $sliderdata['url'] = $_POST["url"][$key]; 
						$this->banner_model->insertSliderImages($sliderdata);	
					}					
				}
				
				
			}
			
			$data['type'] = $type;
			
			$this->banner_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/banner/listing');
		}
		$data['EDITBANNER']= $this->banner_model->selectbannerbyid($args[0]);
		$this->load->view('admin/banner/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();
		
		$this->banner_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/banner/listing');
	}
	
	
	function deleteSlider()
	{
		$args=func_get_args();		
		$this->banner_model->deleteslider($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Slider image has been deleted.</div>');
		redirect('index.php/banner/edit/'.$args[1]);
	}
	
	function chech_admin_login()
	{
		$ci = & get_instance();
		$USERID      = $ci->session->userdata('USERID');	
		$USERNAME      = $ci->session->userdata('USERNAME');	
		$logged_in      = $ci->session->userdata('logged_in');	
		if($USERID=="" && $USERNAME=="")
		{
			redirect('index.php/admin/index');
		}
	}
}