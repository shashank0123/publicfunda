<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Campaign extends CI_Controller
{
	public function index()
	{
		$user_login_id = $this->session->userdata('USERID'); 
		$campaignData = $this->campaign_model->selectCampaignByUserID($user_login_id);
		$login_user = $this->user_model->selectUserByID($user_login_id);
		$total_poits = $this->getUserPoints();
		$total_used_points = $this->getAllUsedPoints();	
		
		if(count($campaignData)<5)
		{ 	//if($total_poits<=$total_used_points)
			//{					
				if(isset($_POST['addCampagn'])) 
				{
					$cdata['user_id'] = $user_login_id;
					$cdata['campaign'] = $this->input->post('campaign');
					$cdata['campaignType'] = $this->input->post('campaignType');
					$cdata['campaignTitle'] = $this->input->post('campaignTitle');
					$cdata['campaignUrl'] = $this->input->post('campaignUrl');
					$cdata['campaignPoints'] = $this->input->post('campaignPoints');
					$cdata['sdate'] = $this->input->post('sdate');
					$cdata['edate'] = $this->input->post('edate');
					$cdata['sage'] = $this->input->post('sage');
					$cdata['eage'] = $this->input->post('eage');
					$cdata['gender'] = $this->input->post('gender');
					$cdata['profession'] = $this->input->post('profession');
					$cdata['industry'] = $this->input->post('industry');
					$cdata['state'] = $this->input->post('state');
					$cdata['city'] = $this->input->post('city');
					//$cdata['updateDate'] = time();
					$cdata['createDate'] = time();
					if($total_poits>=$total_used_points+$this->input->post('campaignPoints'))
					{
						$this->campaign_model->insertCampaignData($cdata);					
						$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>your campaign has been successfully created.</div>');	
						redirect('index.php/campaign/index');					
					}
					else
					{
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>error! </div>');	
						redirect('index.php/campaign/index');
					}
				}
				if(isset($_POST['updateCampagn']))
				{									
					$cdata['campaign'] = $this->input->post('campaign');
					$cdata['campaignType'] = $this->input->post('campaignType');
					$cdata['campaignTitle'] = $this->input->post('campaignTitle');
					$cdata['campaignUrl'] = $this->input->post('campaignUrl');
					$cdata['campaignPoints'] = $this->input->post('campaignPoints');
					$cdata['sdate'] = $this->input->post('sdate');
					$cdata['edate'] = $this->input->post('edate');
					$cdata['sage'] = $this->input->post('sage');
					$cdata['eage'] = $this->input->post('eage');
					$cdata['gender'] = $this->input->post('gender');
					$cdata['profession'] = $this->input->post('profession');
					$cdata['industry'] = $this->input->post('industry');
					$cdata['state'] = $this->input->post('state');
					$cdata['city'] = $this->input->post('city');
					$cdata['updateDate'] = time();
					$cid = base64_decode($this->input->post('cid'));
					$this->campaign_model->updateCampaignData($cid,$cdata);					
					$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>your campaign has been successfully Updated.</div>');	
					redirect('index.php/campaign/index');
					
				}
			//}	
		}
		$data['total_poits'] = $total_poits;
		$data['total_used_points'] = $total_used_points;
		$data['campaignData'] = $campaignData;
		$data['login_user'] = $login_user;
		$this->load->view('front/campaign/manage-campaign',$data);
	}
	
	function getAllUsedPoints()
	{
		$user_login_id = $this->session->userdata('USERID'); 
		$campaignData = $this->campaign_model->selectCampaignByUserID($user_login_id);
		if($campaignData>0)
		{
			$points = 0;
			foreach($campaignData  as $cd)
			{
				$points +=$cd->campaignPoints;
			}
			return $points;
		}
		else
		{
			return 0;
		}		
	}
	
	function getUserPoints()
	{
		$user_login_id = $this->session->userdata('USERID'); 
		$userData = $this->user_model->selectUserByID($user_login_id);
		$totalPoints = ($userData[0]->totalPoints+$userData[0]->paidPoints);
		if($totalPoints>0)
		{	
			return $totalPoints;
		}
		else
		{
			return 0;
		}		
	}
	
	function updatePoints()
	{
		$totalPoints = $this->getUserPoints();
		$usedPoints = $this->getAllUsedPoints();
				
		if(isset($_POST) && !empty($_POST['cid']))
		{
			if($totalPoints>=$usedPoints+$_POST['tp'])
			{
				$cdata['campaignPoints'] = $_POST['tp'];
				$this->campaign_model->updateCampaignDataByUidAndCid($_POST['uid'],$_POST['cid'],$cdata);
				echo 1;
			}
			else
			{
				echo 0;
			}	
		}		
	}
	
	function checkcampaignStatus()
	{
		if(isset($_POST)) 
		{
			if(!empty($_POST['startDate']) && !empty($_POST['endDate']) && !empty($_POST['cid']))
			{
				if($_POST['cid']=='all')
				{
					$userId = $_POST['cuserid'];	
					$campaignData = $this->campaign_model->selectCampaignByUserID($userId);	
					$ids = array();
					foreach($campaignData as $campData)
					{
						array_push($ids,$campData->id);						
					} 
					
					$first_date = date('Y-m-d',strtotime($_POST['startDate']));
					$second_date = date('Y-m-d',strtotime($_POST['endDate']));
					$data['cstatus'] = $this->campaign_model->checkCampaignStatusId_in($_POST['cuserid'],$ids,$first_date,$second_date);
					$this->load->view('front/ajax-campaign-status',$data);
				}
				else
				{
					$first_date = date('Y-m-d',strtotime($_POST['startDate']));
					$second_date = date('Y-m-d',strtotime($_POST['endDate']));
					$data['cstatus'] = $this->campaign_model->checkCampaignStatus($_POST['cuserid'],$_POST['cid'],$first_date,$second_date);
					$this->load->view('front/ajax-campaign-status',$data);
				}	
				
			}	
		}	
	}
	
}	