<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Faq extends CI_Controller 
{
	function listing()
	{		
		$data['FAQDATA']= $this->faq_model->selectAllFaq();
		$this->load->view('admin/faq/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{						
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$data['status'] = $this->input->post('status');	
			$data['create_date'] = time();
				
			$this->faq_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/faq/listing');			
		}
		$this->load->view('admin/faq/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{			
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$data['status'] = $this->input->post('status');
			$this->faq_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/faq/listing');
		}
		$data['EDITFAQ']= $this->faq_model->selectFaqById($args[0]);
		$this->load->view('admin/faq/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();		
		$this->faq_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/faq/listing');
	}
	
}