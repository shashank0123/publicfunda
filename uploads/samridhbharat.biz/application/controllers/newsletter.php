<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Newsletter extends CI_Controller 
{
	function listing()
	{		
		$data['NEWSLETTERDATA']= $this->newsletter_model->selectAllNewsletter();
		$this->load->view('admin/newsletter/list',$data);
	}
	function savenewsletter()
	{		
		if(isset($_POST['saveNewsLetter']))
		{						
			$data['email'] = $this->input->post('email');
			$data['contact'] = $this->input->post('contact');
			
			$this->newsletter_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/home#newsletterdiv');			
		}
		else
		{
			redirect('index.php/home');	
		}
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{			
			$data['email'] = $this->input->post('email');
			$data['contact'] = $this->input->post('contact');
			/*$data['status'] = $this->input->post('status'); */
			$this->newsletter_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/newsletter/listing');
		}
		$data['EDITNEWSLETTER']= $this->newsletter_model->selectNewsletterById($args[0]);
		$this->load->view('admin/newsletter/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();		
		$this->newsletter_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/newsletter/listing');
	}
	
	function sendemailnewslatter()
	{		
		if(isset($_POST['sendmail']))
		{
			error_reporting(0);
			$subject = $this->input->post('subject');
			$message = $this->input->post('emailbody');			
			$users = $this->newsletter_model->selectAllNewsletter();			
			$this->load->library('email');
			
			foreach($users as $useremail)
			{
				$this->email->set_newline("\r\n");
				$this->email->set_mailtype("html");
				$this->email->to($useremail->email);
				$this->email->from('info@campaigntrade.com', 'Samridh Bharat');
				$this->email->subject($subject);
				$this->email->message($message);
				$this->email->send();
			}		
			$tradeuser = $tradeuser = $this->newsletter_model->selectAllUseremail();
			foreach($tradeuser as $trade_email)
			{
				$this->email->set_newline("\r\n");
				$this->email->set_mailtype("html");
				$this->email->to($trade_email->email);
				$this->email->from('info@campaigntrade.com', 'Samridh Bharat');
				$this->email->subject($subject);
				$this->email->message($message);
				$this->email->send();
			}
			
			$this->session->set_flashdata("message", "<div class='alert alert-success'>Thank you for contacting us.We have received your enquiry and will respond to you shortly.</div>");
			redirect('index.php/newsletter/sendemailnewslatter');			
		}	
		$data['NEWSLETTERDATA']= $this->newsletter_model->selectAllNewsletter();
		$this->load->view('admin/newsletter/send-mail',$data);
	}
	
}