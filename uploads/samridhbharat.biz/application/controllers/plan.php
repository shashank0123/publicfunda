<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Plan extends CI_Controller 
{
	function listing()
	{		
		$data['PLANDATA']= $this->plan_model->selectAllPlan();
		$this->load->view('admin/plan/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{						
			$data['title'] = $this->input->post('title');
			$data['amount'] = $this->input->post('amount');
			$data['text1'] = $this->input->post('text1');
			$data['text2'] = $this->input->post('text2');
			$data['text3'] = $this->input->post('text3');
			$data['text4'] = $this->input->post('text4');
			$data['status'] = $this->input->post('status');	
			$data['create_date'] = time();
				
			$this->plan_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/plan/listing');			
		}
		$this->load->view('admin/plan/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{			
			$data['title'] = $this->input->post('title');
			$data['amount'] = $this->input->post('amount');
			$data['text1'] = $this->input->post('text1');
			$data['text2'] = $this->input->post('text2');
			$data['text3'] = $this->input->post('text3');
			$data['text4'] = $this->input->post('text4');
			$data['status'] = $this->input->post('status');
			$this->plan_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/plan/listing');
		}
		$data['EDITPLAN']= $this->plan_model->selectPlanById($args[0]);
		$this->load->view('admin/plan/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();		
		$this->news_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/plan/listing');
	}
	
}