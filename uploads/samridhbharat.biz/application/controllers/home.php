<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller 
{
	public function index()
	{
		$data['active'] = 'home';
		//$data['SLIDERIMAGES'] = $this->banner_model->selectAllHomeBanner();
		
		$data['slider_left'] = $this->banner_model->selectBannerById(4);
		$data['slider_right'] = $this->banner_model->selectBannerById(1);
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(1);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(2);
		
		$data['HOMECONTENT'] = $this->cms_model->selectCmsById(1);
		$data['NEWSEVENTS'] = $this->news_model->selectAllActiveNews();
		$this->load->view('front/home',$data);
	}
	
	public function term_condition()
	{
		$data['active'] = 'termscondition';
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(9);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(10);
		
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(5);
		$this->load->view('front/cms-page',$data);
	}
	
	public function privacy_policy()
	{
		$data['active'] = 'privacy';
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(11);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(12);
		
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(6);
		$this->load->view('front/cms-page',$data);
	}
	
	public function faq()
	{
		$data['active'] = 'faq';
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(13);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(14);
		
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(7);
		$data['FAQDATA'] = $this->faq_model->selectAllActiveFaq();
		$this->load->view('front/faq',$data);
	}
	
	public function task_holidays()
	{
		$data['active'] = 'taskholidays';
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(15);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(16);
		
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(8);
		$this->load->view('front/cms-page',$data);
	}
	
	public function legal_documents()
	{
		$data['active'] = 'legaldocuments';
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(17);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(18);
		
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(9);
		$this->load->view('front/cms-page',$data);
	}
	
	public function bank_details()
	{
		$data['active'] = 'bankdetails';
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(19);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(20);
		
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(10);
		$this->load->view('front/cms-page',$data);
	}
	
	public function howitwork()
	{
		$data['active'] = 'howitwork';
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(33);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(34);
		
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(2);
		$this->load->view('front/cms-page',$data);
	}
	
	public function whatissme()
	{
		$data['active'] = 'whatissme';
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(35);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(36);
		
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(3);
		$this->load->view('front/cms-page',$data);
	}
	
	public function whatmakesusdifferent()
	{
		$data['active'] = 'whatmakesusdifferent';
		$data['add1'] = $this->advertisement_model->selectbannerbyid(37);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(38);
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(4);
		$this->load->view('front/cms-page',$data);
	}
	
	public function contactus()
	{
		if(isset($_POST['submitQuery']))
		{
			$message = '<html><body>';
			$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message .= "<tr style='background: #eee;'><td colspan='2'><strong>Samridh Bharat Contact us Form Enquery</strong> </td></tr>";
			$message .= "<tr><td><strong>Name:</strong> </td><td>" . $_POST['name'] . "</td></tr>";
			$message .= "<tr><td><strong>Email:</strong> </td><td>" . $_POST['email'] . "</td></tr>";
			$message .= "<tr><td><strong>Department:</strong> </td><td>" . $_POST['department'] . "</td></tr>";
			$message .= "<tr><td><strong>Massage:</strong> </td><td>" . $_POST['company_desc'] . "</td></tr>";
			$message .= "</table>";
			$message .= "</body></html>";						
		    $emailto = $this->block_model->selectBlockById(10);
			//echo $message; die();
			$to = $emailto[0]->description;  //'anaseem711@gmail.com';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->set_mailtype("html");
			$this->email->from('info@samridhbharat.biz');
			$this->email->to($to);
			$this->email->subject('Samridh Bharat - '.$_POST['department']);
			$this->email->message($message);
			$mail= $this->email->send();
			$this->session->set_flashdata("message", "<div class='alert alert-success'>Thank you for contacting us.We have received your enquiry and will respond to you shortly.</div>");
			redirect('contact-us#mycontactform');
		}	
		
		$data['active'] = 'contactus';
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(7);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(8);
		
		$data['slider_left'] = $this->banner_model->selectBannerById(8);
		$data['slider_right'] = $this->banner_model->selectBannerById(7);
		$data['SLIDERIMAGES'] = $this->banner_model->selectAllHomeBanner();
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(11);		
		$this->load->view('front/contact-us',$data);
	}
	
	public function aboutus()
	{
		$data['active'] = 'aboutus';
		$data['slider_left'] = $this->banner_model->selectBannerById(6);
		$data['slider_right'] = $this->banner_model->selectBannerById(5);
		
		$data['add1'] = $this->advertisement_model->selectbannerbyid(3);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(4);		
		
		$data['SLIDERIMAGES'] = $this->banner_model->selectAllHomeBanner();
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(12);		
		$this->load->view('front/about-us',$data);
	}
	
	public function whyus()
	{
		$data['active'] = 'whyus';
		$data['add1'] = $this->advertisement_model->selectbannerbyid(5);
		$data['add2'] = $this->advertisement_model->selectbannerbyid(6);
		
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(13);
		$this->load->view('front/cms-page',$data);
	}
	
	public function plan()
	{
		$data['active'] = 'plans';
		$data['PLANDATA'] = $this->plan_model->selectAllActivePlan();
		$data['FAQCONTENT'] = $this->cms_model->selectCmsById(14);
		$this->load->view('front/plan',$data);
	}
	
}

