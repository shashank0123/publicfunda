<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News extends CI_Controller 
{
	function listing()
	{		
		$data['NEWSDATA']= $this->news_model->selectAllNews();
		$this->load->view('admin/news/list',$data);
	}
	function add()
	{		
		if(isset($_POST['savedata']))
		{						
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$data['status'] = $this->input->post('status');
                        $data['newslink'] = $this->input->post('newslink');	
			$data['create_date'] = time();
				
			$this->news_model->insert($data);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully saved.</div>');
			redirect('index.php/news/listing');			
		}
		$this->load->view('admin/news/add');
	}
	function edit()
	{		
		$args=func_get_args();
		if(isset($_POST['updatedata']))
		{			
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$data['status'] = $this->input->post('status');
                        $data['newslink'] = $this->input->post('newslink');
			$this->news_model->update($args[0],$data);
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/news/listing');
		}
		$data['EDITNEWS']= $this->news_model->selectNewsById($args[0]);
		$this->load->view('admin/news/edit',$data);
	}
	
	function delete()
	{
		$args=func_get_args();		
		$this->news_model->delete($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/news/listing');
	}
	
}