<?php 
class Campaign_model extends CI_Model
{
	var $campaign_table = 'tbl_campaign';
	public function insertCampaignData($data)
	{
		$this->db->insert($this->campaign_table,$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	
	public function updateCampaignData($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update($this->campaign_table,$data);
		return $returnData;
	}
	
	public function updateCampaignDataByUidAndCid($uid,$cid,$data)
	{
		$this->db->where('id',$cid);
		$this->db->where('user_id',$uid);
		$returnData = $this->db->update($this->campaign_table,$data);
		return $returnData;
	}
	
	public function updateCampaignDataByUserId($id,$data)
	{
		$this->db->where('user_id',$id);
		$returnData = $this->db->update($this->campaign_table,$data);
		return $returnData;
	}
	
	public function getAllCampaign()
	{
		$data = $this->db->get($this->campaign_table);
		return $data->result();
	}
	
	public function selectCampaignByID($id)
	{
		$this->db->where('id',$id);
		$data = $this->db->get($this->campaign_table);
		return $data->result();
	}
	
	public function selectCampaignRandomly($user_id)
	{
		$this->db->where('user_id !=', $user_id);
		$this->db->where('campaignStatus', 1);
		$this->db->order_by('rand()');
		$this->db->limit(1);
		$data = $this->db->get($this->campaign_table);
		return $data->result();
	}
	
	public function selectCampaignForDistribution()
	{
		$this->db->where('campaignStatus',1);
		$data = $this->db->get($this->campaign_table);
		return $data->result();
	}
		
	public function selectCampaignByUserID($id)
	{
		$this->db->where('user_id',$id);
		$data = $this->db->get($this->campaign_table);
		return $data->result();
	}
	
	public function selectCampaignByUserIDActiveOrInactive($id,$status)
	{
		$this->db->where('user_id',$id);
		$this->db->where('campaignStatus',$status);
		$data = $this->db->get($this->campaign_table);
		return $data->result();
	}
	
	// insert link
	public function insertTaskLink($data)
	{
		$this->db->insert('tbl_linkdistribution',$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	
	public function selectDistributionLinkByUserIdAndTodayDate($id)
	{
		$this->db->where('user_id',$id);
		$this->db->where('distributionDate',date('Y-m-d'));	
		$this->db->order_by("id","asc");
		$data = $this->db->get('tbl_linkdistribution');
		return $data->result();
	}
	
	public function selectAllCampaignLink()
	{
		$this->db->where('campaignStatus',1);
		//$this->db->order_by("id","asc");
		$data = $this->db->get('tbl_campaign');
		return $data->result();
	}
	
	public function selectAllCampaignLinkAvailability()
	{
		$this->db->where('campaignStatus',1);
		$this->db->order_by("sdate","ASC");
		//$this->db->where('edate >=',date('Y-m-d'));
		$data = $this->db->get('tbl_campaign');
		return $data->result();
	}
	
	public function todayTaskLinkDisributionCheckOneLink($id)
	{
		$this->db->where('user_id',$id);
		$this->db->where('distributionDate',date('Y-m-d'));
		$this->db->where('status',1);
		$this->db->order_by("id","asc");
		$this->db->limit(1);	
		$data = $this->db->get('tbl_linkdistribution');
		return $data->result();
	}
	
	public function getTotalClickedLink($id)
	{
		$this->db->where('user_id',$id);
		$this->db->where('distributionDate',date('Y-m-d'));
		$this->db->where('status',0);
		$this->db->where('redeem',1);
		$data = $this->db->get('tbl_linkdistribution');
		return $data->result();
	}
	
	public function getTotalPoints($id)
	{
		$this->db->where('user_id',$id);
		$this->db->where('status',0);	
		$this->db->where('redeem',1);
		$data = $this->db->get('tbl_linkdistribution');
		return $data->result();
	}
	
	public function updateCampaignLinkInactive($id)
	{
		$data['status'] = 0;
		$data['redeem'] = 1;	
		$data['clickedIP'] = getenv('REMOTE_ADDR'); //$_SERVER['SERVER_ADDR']; 
		$data['system_ip'] = gethostbyname(trim(exec("hostname")));
		$data['clickDate'] = time();
		
		$this->db->where('id',$id);
		$returnData = $this->db->update('tbl_linkdistribution',$data);
		return $returnData;
	}
	
	public function updateCampaignLink($id,$data)
	{
		$this->db->where('id',$id);
		$returnData = $this->db->update('tbl_linkdistribution',$data);
		return $returnData;
	}
	
	public function updateRedeemPoints($id)
	{		
		$data['redeem'] = 0;		
		$this->db->where('redeem',1);
		$this->db->where('status',0);
		$this->db->where('user_id',$id);
		$returnData = $this->db->update('tbl_linkdistribution',$data);
		return $returnData;
	}
	
	public function checkCampaignStatus($uid,$cid,$first_date,$second_date)
	{
		//$this->db->where('user_id',$uid);
		$this->db->where('link_id',$cid);
		$this->db->where('distributionDate >=', $first_date);
		$this->db->where('distributionDate <=', $second_date);
		$this->db->where('status',0);
		$data = $this->db->get('tbl_linkdistribution');
		return $data->result();
	}
	
	public function checkCampaignStatusId_in($uid,$cid,$first_date,$second_date)
	{
		//$this->db->where('user_id',$uid);
		//$this->db->where('link_id',$cid);	
		$this->db->where_in('link_id', $cid);	
		$this->db->where('distributionDate >=', $first_date);
		$this->db->where('distributionDate <=', $second_date);
		$this->db->where('status',0);
		
		$data = $this->db->get('tbl_linkdistribution');
		return $data->result();
	}
	
	
	public function getMyWorkPayout($neftno='',$uid,$first_date,$second_date,$status)
	{
		if($neftno!="")
		{
			$this->db->where('neftNo',$neftno);
		}	
		$this->db->where('user_id',$uid);
		$this->db->where('invDate >=', $first_date);
		$this->db->where('invDate <=', $second_date);
		if($status!='all')
		{
			$this->db->where('status',$status);
		}		
		
		$data = $this->db->get('tbl_invoice');
		return $data->result();
	}
	
	public function getAllDistributionCampaignById($cid)
	{
		$this->db->where('link_id',$cid);
		$data = $this->db->get('tbl_linkdistribution');
		return $data->result();
	}
	
	public function getAllDistributionCampaignBylimet()
	{
		//$this->db->where('link_id',$cid);
		$this->db->where('campaignStatus',1);
		$this->db->order_by('rand()');
		$this->db->limit(1);
		$data = $this->db->get('tbl_campaign');
		return $data->result();
	}
	
	public function getMyWorkHistory($uid,$first_date,$second_date,$status)
	{	
		$this->db->where('user_id',$uid);
		$this->db->where('distributionDate >=', $first_date);
		$this->db->where('distributionDate <=', $second_date);
		if($status!='all')
		{
			$this->db->where('status',$status);
		}
		$this->db->order_by('distributionDate','DESC');
		$data = $this->db->get('tbl_linkdistribution');
		return $data->result();
	}
	
	public function getMyWorkHistoryMis($uid,$cid="",$first_date,$second_date,$status)
	{	
		if($uid!="" && $uid!='0'){$this->db->where('user_id',$uid);}
		if($cid!="" && $cid!='0'){$this->db->where('link_id',$cid);}
		$this->db->where('distributionDate >=', $first_date);
		$this->db->where('distributionDate <=', $second_date);
		if($status!='all')
		{
			$this->db->where('status',$status);
		}
		$this->db->order_by('distributionDate','DESC');
		$data = $this->db->get('tbl_linkdistribution');
		return $data->result();
	}
	
	public function getAllRedeemHistoryPointsList($id)
	{
		$this->db->where('user_id',$id);
		$data = $this->db->get('tbl_redeempoints');
		return $data->result();
	}
	
	public function getAllRedeemPointsByFilters($uid,$first_date,$second_date)
	{	
		$this->db->where('user_id',$uid);
		$this->db->where('redeemdate >=', $first_date);
		$this->db->where('redeemdate <=', $second_date);
		$this->db->order_by('id','desc');
		$data = $this->db->get('tbl_redeempoints');
		return $data->result();
	}
	
	public function getAllRedeemPointsByUserID($uid)
	{	
		$this->db->where('user_id',$uid);
		$this->db->order_by('id','desc');
		$data = $this->db->get('tbl_redeempoints');
		return $data->result();
	}
	
	public function insertRedeemHistoryPoints($data)
	{
		$this->db->insert('tbl_redeempoints',$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	
	// invoice qoery
	public function getInvoiceDataByUserId($id)
	{
		$this->db->where('user_id',$id);
		$this->db->order_by('id', 'DESC');
		$data = $this->db->get('tbl_invoice');
		return $data->result();
	}
	
	public function insertInvoiceData($data)
	{
		$this->db->insert('tbl_invoice',$data);
		$insert_id = $this->db->insert_id();		 
		return  $insert_id;
	}
	
	
	public function checkAllCampaignStatus($first_date="",$second_date="",$status)
	{	
		if($first_date!=""){$this->db->where('distributionDate >=', $first_date); }	
		if($second_date!=""){$this->db->where('distributionDate >=', $second_date); }		
		if($status!=""){ $this->db->where('status',$status); }			
		//$this->db->where('status',0);
		$data = $this->db->get('tbl_linkdistribution');
		return $data->result();
	}
	
}