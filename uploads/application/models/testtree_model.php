<?php 
class Testtree_model extends CI_Model
{
	var $user_table = 'tbl_users';		
	
	public function getalluserstree($id,$pos)
	{
		$category_data = array();
		$this->db->where('sponsor_id',$id);
		$this->db->where('postion',$pos);
		
		//$this->db->where('status',1);
		$data = $this->db->get('tbl_users');
		$result = $data->result();
		if(count($result)>0)
		{
			foreach($result as $res)
			{
				$category_data[] = array('uid' => $res->id);
				$children = $this->getalluserstree($res->id,$pos);
				if($children)
				{
					$category_data = array_merge($children, $category_data);
				}
			}
			return $category_data;
		}
	}
}
