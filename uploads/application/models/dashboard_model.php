<?php 
class dashboard_model extends CI_Model{
	 public function list_dashboard(){
	 	  $this->db->limit('12');
	 	$data=$this->db->get("tbl_dashboard");
	 	return $data->result();
	 }
	 public function list_dashboard_limit($limit){
	 	  $this->db->limit($limit);
	 	$data=$this->db->get("tbl_dashboard");
	 	return $data->result();
	 }
	  public function list_dashboards(){
	  	$this->db->where("status",1);
	 	  $this->db->limit('12');
	 	$data=$this->db->get("tbl_dashboard");
	 	return $data->result();
	 }

	 public function edit_listing($id){
        $this->db->where("id",$id);
        $data=$this->db->get("tbl_dashboard");
        return $data->result();
	 }
	 public function update_daseboard($id,$data){
	 	$this->db->where("id",$id);
	 	$data=$this->db->update("tbl_dashboard",$data);
	 	return $data;
	 }
	  public function list_slider(){
	 	$data=$this->db->get("tbl_slider");
	 	return $data->result();
	 }
	 public function list_image(){
	 	  $this->db->order_by("image", "desc");
	 	  $this->db->limit('1');
	 	$data=$this->db->get("tbl_dashboard");
	 	return $data->result();
	 } 
	 public function selectDashboardByID($id){
        $this->db->where("id",$id);
        $data=$this->db->get("tbl_dashboard");
        return $data->result();
	 }
}