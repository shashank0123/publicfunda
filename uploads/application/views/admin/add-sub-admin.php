<?php $this->load->view('admin/layout/header'); ?>
<?php 
if(isset($adminData[0]->module) && $adminData[0]->module!="")
{
	$module_array = explode(",",$adminData[0]->module);
}
else
{
	$module_array =array();
} 
 ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/style-crop.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/img-crop/css/jquery.Jcrop.min.css" type="text/css" />
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Add Sub Admin</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Add Sub Admin</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" ><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>                           
							<section class="panel">
							<header class="panel-heading">
								<div class="panel-actions">									
								</div>						
								<h2 class="panel-title pull-left">Add Sub Admin</h2><br>
							</header>
							<div class="panel-body">								
								<div class="table-responsive">
									<form method="post">
										<table class="table table-bordered table-striped table-condensed" style="">										
											<tbody>
												<tr>
													<td>Username</td>
													<td><input type="text" name="username" value="<?php echo(isset($adminData[0]->username))?$adminData[0]->username:''; ?>" class="form-control" data-validation="required"></td>
												</tr>
												<tr>
													<td>Password</td>
													<td><input type="text" name="password" value="<?php echo(isset($adminData[0]->username))?base64_decode($adminData[0]->password):''; ?>" class="form-control" data-validation="required"></td>
												</tr>
												<tr>
													<td>Email address</td>
													<td><input type="text" name="email" value="<?php echo(isset($adminData[0]->email))?$adminData[0]->email:''; ?>" class="form-control" data-validation="required"></td>
												</tr>
												<tr>
													<td>Admin module</td>
													<td>
														<ul style="float:left;">
															<?php foreach(getadminmodule() as $key=>$value){ ?>
																<li><label><input <?php echo(in_array($key, $module_array))?'checked':''; ?> type="checkbox" name="module[]" value="<?php echo $key; ?>"><?php echo $value; ?><label></li>
															<?php } ?>
														</ul>
													</td>
												</tr>
												<tr>
													<td>Status</td>
													<td>
														<select name="status" class="form-control">
															<option value="1" <?php echo(isset($adminData[0]->status) && $adminData[0]->status==1)?'selected':''; ?>>Active</option>
															<option value="0" <?php echo(isset($adminData[0]->status)  && $adminData[0]->status==0)?'selected':''; ?>>Inactive</option>
														</select>
													</td>
												</tr>
												<tr>
													<td colspan="2"><button type="submit" class="btn btn-info" name="suubmitData">Submit</button></td>
												</tr>
											</tbody>
										</table>
									</form>
								</div>
							</div>
						</section>
                        
						</div>
						

					</div>
					<!-- end: page -->
				</section>
			</div>
		</section>
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- end: page -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>	
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
		<script>
		  $.validate({
			lang: 'en'
		  });
		</script>
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>