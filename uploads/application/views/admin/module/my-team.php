 <?php $this->load->view('admin/layout/header'); ?>
<?php 
		if(isset($_POST['searchMyTeam']))
		{
			$user_id = $login_user[0]->id;
			$sd = strtotime($_POST['start']);
			$ed = strtotime($_POST['end']);
			$position = $_POST['position'];			
			$status = $_POST['status'];
		}
		else
		{
			$user_id = 0;
			$sd = 0;
			$ed = 0;
			$position = 0;
			$status = 0;
		}	
?>
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage My Team</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>My Team</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->					
					<div class="row">						
						<div class="col-md-12">
							<section class="panel"><br><br>
									<header class="panel-heading">	
											<h2 class="panel-title">My Team
												<button class="pull-right btn btn-info "><strong>Total Members</strong> Left : <?php echo count($this->tree_model->countTotalMembers($login_user[0]->id,'L')); ?> | Right : <?php echo count($this->tree_model->countTotalMembers($login_user[0]->id,'R')); ?></button>									
											</h2> 
									</header>									
									<div class="panel-body" >
									<form class="form-horizontal" method="post">
											<h4 class="mb-xlg"></h4>
											<fieldset>												
												<div class="form-group">
                                                													
													<div class="col-md-5">
														<div class="input-daterange input-group" data-plugin-datepicker="" >
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="start" required value="<?php echo(isset($_POST['start']))?$_POST['start']:''; ?>">
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="end" required value="<?php echo(isset($_POST['end']))?$_POST['end']:''; ?>">                                                        
														</div>
													</div>                                                    
                                                    <div class="col-md-2">
														<select class="form-control mb-md" name="status" required>
															<option value="">Select Account Status</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='all')?'selected':''; ?> value="all">All</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='1')?'selected':''; ?> value="1">Active</option>
															<option <?php echo(isset($_POST['status']) && $_POST['status']=='0')?'selected':''; ?> value="0">Inactive</option>
														</select>                                                        
													</div>
                                                    <div class="col-md-2">
														<select class="form-control mb-md" name="position" required>
															<option value="">Select Position</option>															
															<option <?php echo(isset($_POST['position']) && $_POST['position']=='L')?'selected':''; ?> value="L">Left</option>
															<option <?php echo(isset($_POST['position']) && $_POST['position']=='R')?'selected':''; ?> value="R">Right</option>
															<option <?php echo(isset($_POST['position']) && $_POST['position']=='all')?'selected':''; ?> value="all">Both</option>
														</select>                                                        
													</div>
                                                    
                                                    <div class="col-md-3">
														<button type="submit" name="searchMyTeam" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
														<?php if(isset($_POST['searchMyTeam'])){ ?>
															<a href="<?php echo base_url("index.php/mis/my_team_mis/$sd/$ed/$position/$status/$user_id"); ?>" target="_blank" type="button" class="btn btn-warning"><i class="fa  fa-repeat"></i> Download </a>   
														<?php } ?>
													</div>
												</div>                                                
											</fieldset>
											
											
											
											
											

										</form>
                                        
                                        <div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed mb-none css-serial">
										<thead>
											<tr>
												<th>S.No</th>
                                                <th>User ID</th>
                                                <th>Name</th>
												<th>Plan</th>
                                            	<th>Status</th>
                                                <th>Activation Date</th>
                                                <th>Booster Status</th>
                                                <th>Position</th>
											</tr>
										</thead>
										<tbody>	
											<?php 
												if(isset($_POST['searchMyTeam']))
												{
													$sdate = date('Y-m-d',strtotime($this->input->post('start')));
													$esdate = date('Y-m-d',strtotime($this->input->post('end')));
													$status = $this->input->post('status');
													$postion = $this->input->post('position');
													echo $this->tree_model->myTeamView($login_user[0]->id,$postion,$status,$sdate,$esdate);
												}
												else
												{
													
												}
											?>
										</tbody>
                                        	
                                            
									</table>
                                    
								</div>
								</div>
								</section>                         
						</div>
						

					</div>				
					<!-- end: page -->
				</section>
			</div>			
			
		</section>	
		
	</body>

<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script></html>