 <?php $this->load->view('admin/layout/header'); ?>
<style>
.material-icons{
	font-size: 14px;
}
</style>
			<div class="inner-wrapper pt-30">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Helpdesk</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php //echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Helpdesk</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">	
						<div class="col-md-12"> <br>
							<?php echo $this->session->flashdata('message'); ?> 							
							<section class="panel">
							<div class="panel-body">
								<form class="form-horizontal" method="post">
									<h4 class="mb-xlg"></h4>
									<fieldset>												
										<div class="form-group">													
											<div class="col-md-6">
												<div class="input-daterange input-group" data-plugin-datepicker="">
													<span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
													<input type="text" class="form-control" name="startDate" placeholder="start Date" value="<?php echo(isset($_POST['startDate']))?$_POST['startDate']:''; ?>" required>
													<span class="input-group-addon">to</span>
													<input type="text" class="form-control" name="endDate" placeholder="end Date" value="<?php echo(isset($_POST['endDate']))?$_POST['endDate']:''; ?>" required>                                                        
												</div>
											</div>												                                                                                                      
                                            <div class="col-md-3">
												<button type="submit" name="checkHelpDesk" class="btn btn-warning"><i class="fa fa-search"></i> Search</button>
											</div>
										</div>                                                  
									</fieldset>
								</form>
							</div>
							<div class="panel-body">
								<a href="#" class="btn btn-info">Total Pending Query -(<?php echo count($this->helpdesk_model->getAllStatusQuery('pending')); ?>)</a>
								<a href="#" class="btn btn-info">Total Close Query -(<?php echo count($this->helpdesk_model->getAllStatusQuery('close')); ?>)</a>
								<a href="#" class="btn btn-info">Total Open Query -(<?php echo count($this->helpdesk_model->getAllStatusQuery('open')); ?>)</a>
								
                                <a href="<?php echo base_url('index.php/helpdesk/department'); ?>" class="btn btn-info pull-right">Manage Department</a>
								<a href="<?php echo base_url('index.php/mis/download_all_helpdesk_query_mis'); ?>" target="_blank" class="btn btn-warning ">Download All Query</a>	
								<div class="table-responsive">                                 
									<table class="table table-bordered table-striped" id="datatable-default">									
								<thead>
                                    <tr>
										<th width="5%">S.No.</th>
										<th>USERID</th>
                                        <th>Type</th>
										<th>Subject</th>
										<th>Ticket ID</th>
										<th>Executive</th>
										<th>Status</th>
										<th>Date</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php $i=0; foreach($helpdesk as $data){ $i++; ?>
								<?php $department = $this->helpdesk_model->selectDepartmentByID($data->type); ?>	
                                    <tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $data->uid; ?></td>	
                                        <td><?php echo (isset($department[0]->title))?$department[0]->title:''; ?></td>
										<td><?php echo $data->subject; ?></td>	
                                        <td>MOTIF<?php echo $data->id; ?></td>
										<td><?php echo $department[0]->name; ?></td>
                                        <td><?php echo ucfirst($data->status); ?></td>
										<td><?php echo date('d-m-Y h:s a',$data->queryTime); ?></td>		
                                        <td>
											<a href="<?php echo base_url('helpdesk/query/'.$data->id); ?>" title="edit record"><i class="fa fa-eye" aria-hidden="true"></i></a>										
										</td>
                                    </tr>
                                <?php } ?>                                       
                                </tbody>
						</table>
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>
	

<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>		
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/javascripts/tables/examples.datatables.tabletools.js"></script>
		
	</body>

</html>