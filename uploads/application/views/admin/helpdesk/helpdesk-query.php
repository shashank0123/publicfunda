 <?php $this->load->view('admin/layout/header'); ?>
<style>
.material-icons{
	font-size: 14px;
}
.search-results-list {
    max-width: 100%;
}
</style>
			<div class="inner-wrapper pt-30">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Helpdesk</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php //echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Helpdesk</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">	
						<div class="col-md-12"> <br>
							<?php echo $this->session->flashdata('message'); ?>							
							<section class="panel">								
							<div class="panel-body">	
								<div class="table-responsive">                                 
									<table class="table table-bordered table-striped" id="datatable-default">							
									<?php $department = $this->helpdesk_model->selectDepartmentByID($query[0]->type); ?>
										<tbody>
											 <tr>
												<td>Query Date</td>
												<td><b><?php echo date('d-m-Y h:s a',$query[0]->queryTime); ?></b></td>										
											 </tr>
											 <tr>
												<td>Ticket ID</td>
												<td><b>MOTIF<?php echo $query[0]->id; ?></b></td>										
											 </tr>
											 <tr>
												<td>USERID</td>
												<td><b><?php echo $query[0]->uid; ?></b></td>										
											 </tr>
											 <tr>
												<td>Department</td>
												<td><b><?php echo $department[0]->title; ?></b></td>										
											 </tr>
 <tr>
												<td>Attachment</td>
												<td><a class="btn btn-warning" href="<?php echo base_url('uploads/helpdesk/'.$query[0]->file); ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }'><i class="fa fa-search"></i> View</a></td>										
											 </tr>

											 <tr>
												<td>Subject</td>
												<td><b><?php echo $query[0]->subject; ?></b></td>										
											 </tr>	
											 <tr>
												<td>Query</td>
												<td><b><?php echo $query[0]->query; ?></b></td>										
											 </tr>
											<form method="post">	
											 <tr>
												<td>Status</td>
												<td>
													<select name="status">
														<option <?php echo($query[0]->status=='pending')?'selected':''; ?> value="pending">Pending</option>
														<option <?php echo($query[0]->status=='open')?'selected':''; ?> value="open">Open</option>
														<option <?php echo($query[0]->status=='close')?'selected':''; ?> value="close">Close</option>
													</select>
													<button type="submit" class="btn" name="updateSatatus">Update</button>
												</td>										
											 </tr> 
											 </form>
											</tbody>
										</table>
										<form method="post">
											<input type="hidden" name="depatyment" value="<?php echo $query[0]->type; ?>">
											<table class="table table-bordered table-striped" id="datatable-default">
												<tbody>										
													<tr>
														<td><strong>Comment</strong></td>
														<td><textarea name="comment" class="form-control" required></textarea></td>										
													 </tr>	
													 <tr>
														<td colspan="2">
															<button class="btn btn-info pull-right" type="submit" name="submitComment">
																Submit
															</button>
														</td>
													 </tr>
												</tbody>
											</table>
										</form>
									    <br>
										<ul class="list-unstyled search-results-list">
											<?php if(count($query_comments)>0){ ?>
											<?php foreach($query_comments as $comments){ ?>
											<li>												
												<span>
													<i class="fa fa-user"></i> By <?php echo($comments->comment_type=='admin')?'admin':'User'; ?>
													&nbsp;&nbsp;
													<i class="fa fa fa-clock-o"></i> <?php echo date('d-m-Y h:s a',$comments->comment_time); ?>
												</span>
												<div class="result-data">														
													<p class="description"><?php echo $comments->comment; ?></p>
												</div>
											</li>
											<?php } ?>
											<?php } ?>
										</ul>
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>
	


		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
	


		
		
	</body>

</html>