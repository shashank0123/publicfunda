 <?php $this->load->view('admin/layout/header'); ?>
<style>
.material-icons{
	font-size: 14px;
}
</style>
			<div class="inner-wrapper pt-30">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Book Your Add Calculator</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php //echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Book Your Add Calculator</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">	
						<div class="col-md-12"> <br>
							<?php echo $this->session->flashdata('message'); ?> 							
							<section class="panel">								
							<div class="panel-body">								
								<div class="table-responsive">	
                                 <a href="<?php echo base_url('index.php/bookingcalculator/add/'); ?>" class="mb-xs mt-xs mr-xs btn btn-primary pull-right">Add New</a>
									<table class="table table-bordered table-striped" id="datatable-default">									
							<thead>
								<tr class="gradeX">
									<th>SNo.</th>
									<th>Add Type</th>
									<th>Page Name</th>
									<th>Amount</th>
																		
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							 <?php $i=0; foreach($bookdata as $data){ $i++; ?>
								<tr class="gradeX">
									<td><?php echo  $i; ?></td>
									<td><?php echo($data->addtype=='display')?'Display Add':'Video Add'; ?></td>
									<td><?php echo $data->page_name; ?></td>
                                    <td><?php echo $data->amount; ?></td> 
									<td>
										<a href="<?php echo base_url('index.php/bookingcalculator/edit/'.$data->id); ?>" title="edit record"><i class="fa fa-pencil"></i></a>
										<a href="<?php echo base_url('index.php/bookingcalculator/deleteRecord/'.$data->id); ?>" title="delete record"><i class="fa fa-trash-o"></i></a>
									</td> 
								</tr>
								<?php } ?>																				
							</tbody>
						</table>
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					
					<!-- end: page -->
				</section>
			</div>			
			
		</section>
	


		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
	


		
		
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>