<table border='1'>									
								<thead>
                                    <tr>
										<th width="5%">S.No.</th>
										<th>USERID</th>
                                        <th>Type</th>
										<th>Subject</th>
										<th>Ticket ID</th>
										<th>Executive</th>
										<th>Status</th>
										<th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php $i=0; foreach($helpdesk as $data){ $i++; ?>
								<?php $department = $this->helpdesk_model->selectDepartmentByID($data->type); ?>	
                                    <tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $data->uid; ?></td>
                                        <td><?php echo (isset($department[0]->title))?$department[0]->title:''; ?></td>
										<td><?php echo $data->subject; ?></td>	
                                        <td>MOTIF<?php echo $data->id; ?></td>
										<td><?php echo $department[0]->name; ?></td>
                                        <td><?php echo ucfirst($data->status); ?></td>
										<td><?php echo date('d-m-Y h:s a',$data->queryTime); ?></td>
                                    </tr>
                                <?php } ?>                                       
                                </tbody>
						</table>