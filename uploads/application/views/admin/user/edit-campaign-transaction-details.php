 <?php $this->load->view('admin/layout/header'); ?>
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('admin/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2> Edit Transaction Details</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Manage Transaction Details</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12"> <br><?php echo $this->session->flashdata('message'); ?>                           
							<section class="panel">
							<button type="button" class="btn btn-warning pull-right" onclick="javascript:printDiv('printablediv')">Print</button>
							<div class="panel-body">								
								<div class="table-responsive" id="printablediv">
									
										<form method="post">
																		<table class="table table-bordered table-striped table-condensed mb-none">
																			<tr>
																				<td>Account Holder Name</td>
																				<td><input type="text" name="accountHolderName" value="<?php echo(isset($planInvoice[0]->accountHolderName))?$planInvoice[0]->accountHolderName:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>From Account </td>
																				<td><input type="text" name="fromAccount" value="<?php echo(isset($planInvoice[0]->fromAccount))?$planInvoice[0]->fromAccount:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>Bank Name</td>
																				<td><input type="text" name="bankName" value="<?php echo(isset($planInvoice[0]->bankName))?$planInvoice[0]->bankName:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>To Bank</td>
																				<td><input type="text" name="toBank" value="<?php echo(isset($planInvoice[0]->toBank))?$planInvoice[0]->toBank:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>To Account </td>
																				<td><input type="text" name="toAccount" value="<?php echo(isset($planInvoice[0]->toAccount))?$planInvoice[0]->toAccount:'' ?>" value="<?php echo(isset($planInvoice[0]->accountHolderName))?$planInvoice[0]->accountHolderName:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>E-Point(s)</td>
																				<td><input type="text" name="ePoints" value="<?php echo(isset($planInvoice[0]->ePoints))?$planInvoice[0]->ePoints:$myplandata[0]->totalVisitors; ?>" class="form-control" data-validation="required" ></td>
																			</tr>
																			<tr>
																				<td>Amount</td>
																				<td><input type="text" name="amount" value="<?php echo(isset($planInvoice[0]->amount))?$planInvoice[0]->amount:$myplandata[0]->planFees; ?>" value="<?php echo(isset($planInvoice[0]->accountHolderName))?$planInvoice[0]->accountHolderName:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>UTR Number/Ref Number</td>
																				<td><input type="text" name="utrNumber" value="<?php echo(isset($planInvoice[0]->utrNumber))?$planInvoice[0]->utrNumber:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>NEFT Date</td>
																				<td><input type="text" name="neftDate" value="<?php echo(isset($planInvoice[0]->neftDate))?$planInvoice[0]->neftDate:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>																			
																			<tr>
																				<td colspan="2">
																					<center><button type="submit"  name="submitPlanInv" class="btn btn-info"> Submit </button></center>
																				</td>
																			</tr>	
																		</table>
																		</form>
								</div>
							</div>
						</section>
                         
						</div>
						

					</div>
					
					<!-- end: page -->
				</section>
			</div>
		</section>
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		<script>
		function printDiv(divID) {
        var divToPrint=document.getElementById("printablediv");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();

    }
			function calculteFinalPrice(AMOUNT)
			{
				var amount = AMOUNT;
				var tax =  <?php echo $tax[0]->tax; ?>;
				var tax_field = document.getElementById('tax');
				var final_amount = document.getElementById('finalAmount');
				
				if(AMOUNT>0 && AMOUNT!="")
				{							
					tax_field.value = tax;
					final_amount.value = parseInt(amount)-((tax*amount)/100);
				}
				else
				{

					tax_field.value = "0";
					final_amount.value = "0";
				}	
			}
		</script>		
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>