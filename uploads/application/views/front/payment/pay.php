<?php
// Merchant key here as provided by Payu
$MERCHANT_KEY = "STSu33WY";

// Merchant Salt as provided by Payu
$SALT = "4YzG3arebl";

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://secure.payu.in";

$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
	
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
  <head>
  <script>
    var hash = '<?php echo $hash ?>';	
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  </head>
  <body onload="submitPayuForm()"> 
   
    <?php if($formError) { ?>
     
	
    <?php } ?>
	<h3>Please wait ...</h3>
    <form action="<?php echo $action; ?>" method="post" name="payuForm">
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
	  <input type="hidden"  name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>" />
	  <input type="hidden"  name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
	  <input name="email" type="hidden"  type="hidden"  id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" />
	  <input name="phone" type="hidden"   value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" />
	  <input name="productinfo" type="hidden" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>">
     <input name="surl"  type="hidden"  value="<?php echo (empty($posted['surl'])) ? '' : $posted['surl'] ?>" size="64" />
	 <input name="furl" type="hidden"  value="<?php echo (empty($posted['furl'])) ? '' : $posted['furl'] ?>" size="64" />
	 <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
        <input name="lastname" type="hidden"  id="lastname" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" />
<input name="curl" value=""  type="hidden"/>
        <input name="address1" type="hidden"  value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" />
		<input name="address2" type="hidden"  value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" />
		<input name="city" type="hidden"  value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" />
		<input name="state" type="hidden"  value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" />
		<input name="country" type="hidden"  value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" />
		<input name="zipcode" type="hidden"  value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" />
		
       
          <?php if(!$hash) { ?>
			<input type="submit" value="Submit"  id="submitbutton" style="display:none;"/>
          <?php } ?>        
    </form>
	<script>
	document.getElementById("submitbutton").click();
	</script>
  </body>
</html>
