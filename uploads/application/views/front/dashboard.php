 <?php $this->load->view('front/layout/header'); ?>
			<div class="inner-wrapper pt-100">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body pt-0">	

					<!-- start: page -->
					<div class="row">
						<div class="col-md-12">
							<div class="row">                            
								<div class="col-md-12">
									<section class="panel  ">										
										<?php $cover_img = $this->dashboard_model->selectDashboardByID(13); ?>
										<div class="cover-img">
											<img src="<?php echo base_url('uploads/dashboard/'.$cover_img[0]->description); ?>"  class="img-responsive">
										</div>
									</section>
								</div>
                            
								
<div class="col-md-4">
<section class="panel panel-featured-left panel-featured-primary">
										<div class="panel-body" style="padding-top:0px;">
<div class="thumb-info mb-md profile-sec__">
											<span class="upload_btn" onclick="show_popup('popup_upload')">
												<img src="<?php echo base_url('uploads/user/'.$login_user[0]->image); ?>" class="rounded full-img" alt="<?php echo $login_user[0]->name; ?>">
											</span>
											<div class="thumb-info-title">
												<span class="thumb-info-inner">My Profile</span>
											</div>
											<div id="photo_container"></div>
										</div>
										</div>	</section>	
								
								<?php 
									$task = $login_user[0]->today_task;
									$workPayout = $login_user[0]->workpayout;
									$workHistory = $login_user[0]->workHistory;
									
									$my_network = $login_user[0]->my_network;
									$promotional_income = $login_user[0]->promotional_income;
									$myteam = $login_user[0]->myteam;
									$my_bussiness = $login_user[0]->my_bussiness;
								?>	
								<?php if(count($Dashboard)>0){ ?>
								<?php foreach($Dashboard as $Dashboard){ ?>
                                <?php 
									$todayTask = ($Dashboard->id==3 && $task==0)?'display:none':'';
									$myWorkPayout = ($Dashboard->id==4 && $workPayout==0)?'display:none':'';
									$myworkHistory = ($Dashboard->id==5 && $workHistory==0)?'display:none':'';	
									
									$mynetwork = ($Dashboard->id==6 && $my_network==0)?'display:none':'';
									$promotionalincome = ($Dashboard->id==7 && $promotional_income==0)?'display:none':'';
									$team = ($Dashboard->id==8 && $myteam==0)?'display:none':'';
									$mybussiness = ($Dashboard->id==9 && $my_bussiness==0)?'display:none':'';
								?> 

							
								<div  style="<?php echo $todayTask.$myWorkPayout.$myworkHistory.$mynetwork.$promotionalincome.$team.$mybussiness; ?>;">
									<section class="panel panel-featured-left panel-featured-primary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon <?php echo $Dashboard->class;?>">
														<i class="<?php echo $Dashboard->icone;?>"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<a href="<?php echo base_url("$Dashboard->url"); ?>"><h2 class="title"><?php echo $Dashboard->title;?></h2></a>
														<div class="info">
															<p class="panel-subtitle mb-10"><?php echo $Dashboard->description;?></p>
														</div>
													</div>
													<div class="summary-footer">
														<a href="<?php echo base_url("$Dashboard->url"); ?>" class="btn <?php echo $Dashboard->class;?>">Explore</a>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
								<?php }} ?>
                               </div>	

<div class="col-md-8">
									<section class="panel panel-featured-left panel-featured-primary">
										<?php $this->load->view('front/layout/advertisement-slider'); ?>
									</section>
									<?php $this->load->view('front/layout/comment-section'); ?>
								</div>							   
							 
                            </div>
						</div>
					</div>				
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
				</section>
			</div>			
			
		</section>
	
		
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-appear/jquery.appear.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.pie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.categories.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/flot/jquery.flot.resize.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-sparkline/jquery.sparkline.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/raphael/raphael.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/morris/morris.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/gauge/gauge.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/snap-svg/snap.svg.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/liquid-meter/liquid.meter.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/jquery.vmap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/jquery.vmap.world.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/dashboard/examples.dashboard.js"></script>
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	
		
	
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto-admin/1.2.0/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Oct 2014 06:55:34 GMT -->
</html>