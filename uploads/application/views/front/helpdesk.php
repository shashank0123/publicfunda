<?php $this->load->view('front/layout/header-inner'); ?>

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Help Desk</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Help Desk</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start page -->
					<div class="row">						
						<div class="col-md-12">
							<?php echo $this->session->flashdata('message'); ?>
							<div class="tabs">
								<ul class="nav nav-tabs tabs-primary">
									<li class="active"><a href="#query-status" data-toggle="tab">Query Status</a></li>
                                    <li><a href="#submit-query" data-toggle="tab">Submit Query</a></li>   
								</ul>
								<div class="tab-content">
								
								<div id="query-status" class="tab-pane active">        
                                <div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed mb-none">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Ticket ID</th>
                                                <th>Query Type</th>
												<th>Executive </th>
												<th>Date</th>
												<th>Status</th>
                                                <th>View</th>
                                            </tr>
										</thead>
										<tbody>
											<?php $k=0; foreach($queryData as $query){ $k++;?>
											<?php $oprator = $this->helpdesk_model->selectDepartmentByID($query->type); ?>
											<tr>												
												<td><?php echo $k; ?></td>
												<td>MOTIF<?php echo $query->id; ?></td>
                                                <td><?php echo $oprator[0]->title; ?></td>
												<td><?php echo $oprator[0]->name; ?></td>
                                                <td><?php echo date('d-m-Y h:m a',$query->queryTime); ?></td>
												<td><?php echo $query->status; ?></td>
                                                <td><a href="<?php echo base_url('index.php/helpdesk/view/'.$query->id); ?>" class="btn btn-warning"><i class="fa fa-search"></i> View</a></td>                                                
                                            </tr>                                            
											<?php } ?>
										</tbody>  
									</table>
								</div>
</div>
                                    
                                    <div id="submit-query" class="tab-pane">
										<form class="form-horizontal" method="post" enctype="multipart/form-data">												
											<fieldset>												
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Select Query  Type*</label>
													<div class="col-md-8">													
													<select class="form-control" required name="type">
														<option value="">Sleect Query Type</option>
														<?php foreach($this->helpdesk_model->getAllDepartment() as $department){ ?>
															<option value="<?php echo $department->id; ?>"><?php echo $department->title; ?></option>
														<?php } ?>															
													</select>												
												</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileLastName">Subject*</label>
													<div class="col-md-8">
														<input type="text" name="subject" class="form-control" id="profileLastName" data-validation="required">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileLastName">Query*</label>
													<div class="col-md-8">
														<textarea type="text" rows="5"  name="query" class="form-control" id="profileLastName" data-validation="required"></textarea>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileAddress">Upload File*</label>
													<div class="col-md-8">
														<input type="file" name="file" class="form-control" id="profileAddress" >
													</div>
												</div>								
                                                
											</fieldset>
											<div class="panel-footer">
												<div class="row">
													<div class="col-md-9 col-md-offset-4">
														<button type="submit" name="submitQuery" class="btn btn-default">submit Query</button>
													</div>
												</div>
											</div>
										</form>                                
									</div>
                                    <br>
                                  <?php  $block_helpdesk = $this->staticblock_model->getBlockById(2);
										echo $block_helpdesk[0]->content;					
								  ?>
								</div>
							</div>
                         
						</div>
						

					</div>
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
					<!-- end: page -->
				</section>
			</div>
			
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Analytics to Track Preview Website -->	
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
  $.validate({
    lang: 'en'
  });
</script>
<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	</body>


</html>