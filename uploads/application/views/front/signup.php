<?php 
$user_login_id = $this->session->userdata('USERID'); 
if(!empty($user_login_id)){	redirect('index.php/user/dashboard'); }
?>
<!doctype html>
<html class="fixed">	
<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/css/datepicker3.css" />
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/stylesheets/theme.css" />
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/stylesheets/theme-custom.css">
		<!-- Head Libs -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign1">
			<div class="center-sign">
				<a href="#" class="logo pull-left">
					<img src="<?php echo base_url(); ?>assets/front/images/logo.png" height="54" alt="Porto Admin" />
				</a>
                                <!--- <?php echo base64_encode('Developed by Naseem Ahmad || e-mail anaseem711@gmail.com'); ?> --->
				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<a href="<?php echo base_url('index.php/user/login'); ?>"><h2 class="title text-uppercase text-bold m-none">
					  <i class="fa fa-user mr-xs"></i> Sign In</h2></a>
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign Up</h2>
					</div>
					<div class="panel-body">
						<form method="post" id="regform" class="">
                        <div class="row">
							<div class="col-md-12"><?php echo $this->session->flashdata('message'); ?></div>
							<div class="col-md-6">								
								<div class="form-group  user_login_container">
									<input value="<?php echo $_GET['name']; ?>" type="text" class="form-control" id="user-name" name="name" data-validation="required" placeholder="Enter your Name here">
								</div>                            
								<div class="form-group ">
									<input value="<?php echo $_GET['email']; ?>" type="email" class="form-control" id="user-email" name="email" data-validation="email" placeholder="Enter your E-mail ID here">
								</div>                            
								<div class="form-group">
									<input  value="<?php echo $_GET['contact_no']; ?>" type="text" class="form-control" id="user-contact_no" name="contact_no" data-validation="required" placeholder="Enter your Contact Number ">
								</div>
								<div class="form-group">
									<input value="<?php echo $_GET['pancard_no']; ?>" type="text" class="form-control" id="user-pancard_no" name="pancard_no" placeholder="Enter Pancard Number">								
								</div>
								<div class="form-group">
									<div class="checkbox123">
										<div class="form-control-flat">
											<label class="checkbox123">
												<input class="account_reg_term" value="1" id="nopancard" name="applied_pancard" onclick="return checkpancard();" type="checkbox" title="Please agree with the term">
												<i></i>
												 Have you Applied for Pan Card ?
											</label>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<input type="text" value="<?php echo $_GET['nominee']; ?>" id="user-nominee" class="form-control" data-validation="required" name="nominee" placeholder="Enter your Nominee Name">
								</div>					
							   
							</div>
                            
                           <div class="col-md-6">
                            
							<div class="form-group">
								<input type="number" value="<?php echo $_GET['sponsor_id']; ?>" onfocusout="return checksponsor(this.value);" class="form-control" id="user-sponsor_id" data-validation="required" name="sponsor_id" placeholder="Enter Sponsor ID">
								<span class="help-block" style="color:red;" id="serror"></span>
							</div>
                            
                            <div class="form-group">
								<input type="text" value="<?php echo $_GET['sponsor_name']; ?>" readonly class="form-control" id="user-sponsor_name" data-validation="required" name="sponsor_name" placeholder="Enter Sponsor Name">
							</div>	
                            <div class="form-group">
								<div class="form-control-flat">
									<select id="user-postion" class="user_role form-control" name="postion" data-validation="required" >
										<option value="">-- Select Position --</option>
										<option <?php echo($_GET['postion']=='R')?'selected':''; ?> value="R">Right</option>
										<option <?php echo($_GET['postion']=='L')?'selected':''; ?> value="L">Left</option>
									</select>									
								</div>
							</div>
                            
                            <div class="form-group">
								<div class="form-control-flat">
									<select id="user-plan" class="user_role form-control" name="plan" required>
										<option value="">-- Choose Your Plan --</option>
										<?php foreach($this->plan_model->getAllActivePlan() as $plans){ ?>
										<option <?php echo($_GET['plan']=='1')?'selected':''; ?> value="<?php echo $plans->id; ?>"><?php echo $plans->planName; ?></option>
										<?php } ?>
										<?php /*
										<option <?php echo($_GET['plan']=='1')?'selected':''; ?> value="1">Samridh Bharat Prathmik</option>
										<option <?php echo($_GET['plan']=='2')?'selected':''; ?> value="2">Samridh Bharat Dwitiye</option>
										<option <?php echo($_GET['plan']=='3')?'selected':''; ?> value="3">Samridh Bharat  Tritye</option>
										<option <?php echo($_GET['plan']=='4')?'selected':''; ?> value="4">Samridh Bharat  Chaturth</option>
										<option <?php echo($_GET['plan']=='5')?'selected':''; ?> value="5">Samridh Bharat  Pancham</option>
										<option <?php echo($_GET['plan']=='6')?'selected':''; ?> value="6">Samridh Bharat Adarsh Pack</option>
										<option <?php echo($_GET['plan']=='7')?'selected':''; ?> value="7">Samridh Bharat unnat Pack</option>
										<option <?php echo($_GET['plan']=='8')?'selected':''; ?> value="8">SAMRIDH BHARAT Trial Pack </option>
										*/ ?>
									</select>									
								</div>
							</div>
                            
							<div class="form-group">
								<input type="password" id="password" data-validation="required"  class="form-control" name="password" placeholder="Password">
							</div>
                            
							<div class="form-group">
								<input type="password" id="cpassword" data-validation="required" class="form-control" name="cpassword" placeholder="Repeat password">
							</div>                            
                           
                            </div>
                            
                            </div>
							
							<div class="form-group text-center">
								<div class="checkbox123 account-reg-term">
									<div class="form-control-flat">
										<label class="checkbox123">
											<input class="account_reg_term" type="checkbox" id="term-condition" data-validation="required" title="Please agree with the term">
											<i></i>
											I have read the given website Details And Agrees to the <a href="#">Terms And Condition</a>
										</label>
									</div>
								</div>
								<button type="submit" id="mySubmitbtn" class="btn btn-primary" name="userRegistr">Sign Up</button>
							</div>
						</form>
					</div>
				</div>

				
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Analytics to Track Preview Website -->	
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
  $.validate({
    lang: 'en'
  });
</script>
<script>
function checksponsor(SID)
{
	if(SID!="")
	{
		var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() 
	  {
		if (this.readyState == 4 && this.status == 200) {
			//alert(this.responseText);
			var responseTextData = this.responseText;
			if(responseTextData==0)
			{
				//alert('');
				document.getElementById("mySubmitbtn").disabled = true;
				document.getElementById("serror").innerHTML = 'Sorry, You have entered an incorrect Sponsor ID, Kindly check and re-enter it.';
				document.getElementById("user-sponsor_name").value = "";	
			}
			else
			{
				document.getElementById("mySubmitbtn").disabled = false;
				document.getElementById("serror").innerHTML = '';
				document.getElementById("user-sponsor_name").value = responseTextData;				
			}	
		 //document.getElementById("demo").innerHTML = this.responseText;
		}
	  };
	  xhttp.open("GET", "<?php echo base_url('index.php/user/checkUserByID'); ?>?SID="+ SID, true);
	  xhttp.send();
	}	
	  
}	
function checkpancard()
{
	if(document.getElementById("nopancard").checked==true)
	{
		alert('Kindly Note you need to submit PAN Card in 30 days else your home based work will be stopped & previous work will be washed out.');
		document.getElementById("user-pancard_no").value = "";
		document.getElementById("user-pancard_no").style.backgroundColor = "#ccc";
		document.getElementById("user-pancard_no").readOnly = true;
	}
	else
	{
		document.getElementById("user-pancard_no").style.backgroundColor = "#fff";
		document.getElementById("user-pancard_no").readOnly = false;
	}	
}
</script>
<!--- Developed by Naseem Ahmad || e-mail anaseem711@gmail.com --->
	</body>

</html>