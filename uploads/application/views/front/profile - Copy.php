<?php $this->load->view('front/layout/header-inner'); ?>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>User Profile</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>User Profile</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->

					<div class="row">
						<div class="col-md-4 col-lg-3">
							<section class="panel">
								<div class="panel-body">
									<div class="thumb-info mb-md">
										<img src="<?php echo base_url('uploads/user/'.$login_user[0]->image); ?>" class="rounded img-responsive" alt="<?php echo $login_user[0]->name; ?>">
									</div>

									<div class="widget-toggle-expand mb-md">
										<div class="widget-header">
                                        	<h4><?php echo $login_user[0]->name; ?></h4>
											<h6>Profile Completion <span>60%</span></h6>
											<div class="widget-toggle">+</div>
										</div>
										<div class="widget-content-collapsed">
											<div class="progress progress-xs light">
												<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
													60%
												</div>
											</div>
										</div>
										<div class="widget-content-expanded">
                                        
											<ul class="simple-todo-list">
												<li class="completed"><strong>Id:</strong> <?php echo $login_user[0]->id; ?></li>
                                                <br>
												<li class=""><strong>Promotional Link</strong></li>
												<li class="completed"><strong>Last Login :</strong> <?php echo date('d-m-Y, h:i A',$login_user[0]->last_login_time); ?></li>
												<li class="completed"><strong>Login IP :</strong> <?php echo $login_user[0]->last_login_ip; ?></li>
											</ul>
										</div>
									</div>

									<hr class="dotted short">

									<h6 class="text-muted">About</h6>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis vulputate quam. Interdum et malesuada</p>
									<div class="clearfix">
										<!--<a class="text-uppercase text-muted pull-right" href="#">(View All)</a>-->
									</div>

									<hr class="dotted short">

									<div class="social-icons-list">
										<a rel="tooltip" data-placement="bottom" target="_blank" href="http://www.facebook.com/" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
										<a rel="tooltip" data-placement="bottom" href="http://www.twitter.com/" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
										<a rel="tooltip" data-placement="bottom" href="http://www.linkedin.com/" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
									</div>

								</div>
							</section>



						</div>
						<div class="col-md-8 col-lg-6">
							<?php echo $this->session->flashdata('message'); ?>
							<div class="tabs">
								<ul class="nav nav-tabs tabs-primary">
									<li class="active">
										<a href="#overview" data-toggle="tab">My Profile</a>
									</li>
                                    <li>
										<a href="#edit" data-toggle="tab">Edit Profile</a>
									</li>
									<li>
										<a href="#bank" data-toggle="tab">Edit Bank Details</a>
									</li>
                                     <li >
										<a href="#change-password" data-toggle="tab">Change Password</a>
									</li>
                                    
								</ul>
								<div class="tab-content">
									<div id="overview" class="tab-pane active">
										<h4 class="mb-md">My Profile</h4>
                                        <hr>										
                                        <div class="mt-xlg mb-md">
											<div class="tm-body">
											
											<div class="form-group">
												<div class="col-md-3"><p><strong>Email</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->email; ?></p></div>
                                            </div>   
                                            <div class="form-group">
												<div class="col-md-3"><p><strong>Mobile</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->contact_no; ?></p></div>
                                            </div> 
											<div class="form-group">	
                                                <div class="col-md-3"><p><strong>Nominee</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->nominee; ?></p></div>
                                            </div> 
											<div class="form-group">   
                                                <div class="col-md-3"><p><strong>Gender</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->gender; ?></p></div>
                                             </div> 
											<div class="form-group">   
                                                <div class="col-md-3"><p><strong>Address</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->address; ?></p></div>
                                             </div> 
											<div class="form-group">   
                                                <div class="col-md-3"><p><strong>City</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->city; ?> </p></div>
                                             </div> 
											<div class="form-group">   
                                                <div class="col-md-3"><p><strong>State</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->state; ?></p></div>
                                            </div> 
											<div class="form-group">   
                                                <div class="col-md-3"><p><strong>Date Of Birth</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->dob; ?></p></div>
											</div> 
                                                <h4 class="mb-md">&nbsp;</h4>
												   <hr >
                                                   
                                            <div class="form-group">        
                                                <div class="col-md-3"><p><strong>Profession</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->profession; ?></p></div>
                                            </div> 
											<div class="form-group">    
                                                <div class="col-md-3"><p><strong>Industry</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->industry; ?></p></div>
                                            </div> 
											<div class="form-group">    
                                                <div class="col-md-3"><p><strong>Company Name</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->company_name; ?></p></div>
                                            </div> 
											<div class="form-group">    
                                                <div class="col-md-3"><p><strong>Field Of Work</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p><?php echo $login_user[0]->field_work; ?></p></div>
                                            </div> 
											<div class="form-group">  
                                                <div class="col-md-3"><p><strong>Accound Status</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p>
													<?php if($login_user[0]->status==1){ ?>
														<button type="button" class="btn btn-success btn-xs"><b>Active</b></button>
													<?php }else{ ?>
													<button type="button" class="btn btn-danger btn-xs"><b>Inactive</b></button>
													<?php } ?>	
													</p></div>
                                            </div> 
											<div class="form-group">  
                                                <div class="col-md-3"><p><strong>Booster Status</strong><span class="pull-right">:</span></p></div>
                                                <div class="col-md-9"><p>Not Yet Applied</p></div>
                                            </div> 
											<div class="form-group"> 
                                                <div class="col-md-12"><p>You Are 14 Days Left For Activate Booster.</p></div>
                                            </div>   
											</div>
										</div>
                                        
                                        
                            		<div class="table-responsive">
										<table class="table table-bordered table-striped table-condensed mb-none">
											<thead>
												<tr>
													<th>S.No</th>
													<th>Invoice ID</th>
													<th class="text-right">Date</th>
													<th class="text-right">Amount</th>
													<th class="text-right">Acknowledgement</th>
												
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>ABLAZE/2016/182756</td>
													<td class="text-right">19-Sep-2016</td>
													<td class="text-right"><i class="fa fa-rupee"></i> 57500.00</td>
													<td class="text-right">N/A</td>
												</tr>
												
											</tbody>
												<tfoot>
													<tr>
														<th ></th>
														<th >Total</th>
														<th class="text-right"></th>
														<th class="text-right"></th>
														<th class="text-right"><i class="fa fa-rupee"></i> 57500.00</th>
														
													</tr>
												</tfoot>
												
										</table>
										<div class="panel-footer">
											<div class="row">
												<div class="col-md-9 col-md-offset-3">
													<button type="submit" class="btn btn-warning">View/Edit NEFT Details</button>
												</div>
											</div>
										</div>
									</div>
                          				
                                        
                                    </div>                                                                        
									<div id="edit" class="tab-pane">
										<form class="form-horizontal" method="post" enctype="multipart/form-data">
											<h4 class="mb-xlg">My Profile</h4>
											<fieldset>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Full Name</label>
													<div class="col-md-8">
														<input type="text" name="name" class="form-control" value="<?php echo $login_user[0]->name; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Profile Picture</label>
													<div class="col-md-8">
														<input type="file" name="image" class="form-control">
														<input type="hidden" name="oldImage" class="form-control" value="<?php echo $login_user[0]->name; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Email</label>
													<div class="col-md-8">
														<input type="email" readonly class="form-control" value="<?php echo $login_user[0]->email; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileLastName">Mobile</label>
													<div class="col-md-8">
														<input type="text" name="contact_no" class="form-control" value="<?php echo $login_user[0]->contact_no; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileAddress">Nominee</label>
													<div class="col-md-8">
														<input type="text" name="nominee" class="form-control" value="<?php echo $login_user[0]->nominee; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Gender</label>
													<div class="col-md-8">
														<select type="text" class="form-control" name="gender">
                                                        	<option <?php echo($login_user[0]->gender=='Male')?'selected':'' ?> value="Male">Male</option>
                                                            <option <?php echo($login_user[0]->gender=='Male')?'selected':'' ?> value="Female">Female</option>
                                                        </select>
													</div>
												</div>                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label"  for="profileCompany">Address</label>
													<div class="col-md-8">
														<input type="text" name="address" class="form-control" value="<?php echo $login_user[0]->address; ?>" D>
													</div>
												</div>                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label"  for="profileCompany">City</label>
													<div class="col-md-8">
														<input type="text" name="city" class="form-control" value="<?php echo $login_user[0]->city; ?>" >
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label"  for="profileCompany">State</label>
													<div class="col-md-8">
														<input type="text" name="state" class="form-control" value="<?php echo $login_user[0]->state; ?>" >
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Pin Code</label>
													<div class="col-md-8">
														<input type="text" name="pincode" class="form-control" value="<?php echo $login_user[0]->pincode; ?>">
													</div>
												</div>
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Colleges</label>
													<div class="col-md-8">
														<input type="text" name="colleges" class="form-control" value="<?php echo $login_user[0]->colleges; ?>" >
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">University</label>
													<div class="col-md-8">
														<input type="text" name="university"  class="form-control" value="<?php echo $login_user[0]->university; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Schools</label>
													<div class="col-md-8">
														<input type="text" name="schools"  class="form-control" value="<?php echo $login_user[0]->schools; ?>">
													</div>
												</div>
                                                
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Mobile Handset Models</label>
													<div class="col-md-8">
														<input type="text" name="mobile_handset" class="form-control" value="<?php echo $login_user[0]->mobile_handset; ?>">
													</div>
												</div>
                                                
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Highest Degree</label>
													<div class="col-md-8">
														<input type="text" name="highest_degree" class="form-control" value="<?php echo $login_user[0]->highest_degree; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Extra Skills</label>
													<div class="col-md-8">
														<input type="text" name="extra_skills" class="form-control" value="<?php echo $login_user[0]->extra_skills; ?>">
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Celebrity</label>
													<div class="col-md-8">
														<input type="text" name="celebrity" class="form-control" value="<?php echo $login_user[0]->celebrity; ?>">
													</div>
												</div>
                                                
                                               <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Date Of Birth</label>
													<div class="col-md-8">
														<input type="text" name="dob" class="form-control" value="<?php echo $login_user[0]->dob; ?>">
													</div>
												</div>
                                                
											</fieldset>
											<hr class="dotted tall">
											
											<fieldset>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Profession</label>
													<div class="col-md-8">
														<input type="text" name="profession" class="form-control" value="<?php echo $login_user[0]->profession; ?>">
													</div>
												</div>
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Industry</label>
													<div class="col-md-8">
														<input type="text" name="industry" class="form-control" value="<?php echo $login_user[0]->industry; ?>">
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Current Industry</label>
													<div class="col-md-8">
														<input type="text" name="current_industry" class="form-control" value="<?php echo $login_user[0]->current_industry; ?>">
													</div>
												</div>
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Company Name</label>
													<div class="col-md-8">
														<input type="text" name="company_name" class="form-control" value="<?php echo $login_user[0]->company_name; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Campaign Title</label>
													<div class="col-md-8">
														<input type="text" name="campaign_title" class="form-control" value="<?php echo $login_user[0]->campaign_title; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Current Company Name</label>
													<div class="col-md-8">
														<input type="text" name="current_company" class="form-control" value="<?php echo $login_user[0]->current_company; ?>">
													</div>
												</div>
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Field Of Work</label>
													<div class="col-md-8">
														<input type="text" name="field_work" class="form-control" value="<?php echo $login_user[0]->field_work; ?>">
													</div>
												</div>
												
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Booster Status</label>
													<div class="col-md-8">
														<input type="text" class="form-control" value="<?php echo $login_user[0]->email; ?>">
													</div>
												</div>
                                                
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Flight Travel</label>
													<div class="col-md-8">
														<select type="text" class="form-control" name="flight_travel">
                                                        	<option value="">-Select-</option>
                                                            <option <?php echo($login_user[0]->flight_travel=='yes')?'selected':''; ?> value="yes">Yas</option>
                                                            <option <?php echo($login_user[0]->flight_travel=='no')?'selected':''; ?>  value="no">No</option>
                                                        </select>
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Hobbies</label>
													<div class="col-md-8">
														<input type="text" name="hobbies" class="form-control" value="<?php echo $login_user[0]->hobbies; ?>">
													</div>
												</div>
                                                
                                               	<div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Education</label>
													<div class="col-md-8">
														<input type="text" name="education" class="form-control" value="<?php echo $login_user[0]->education; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Course Type Done</label>
													<div class="col-md-8">
														<input type="text" name="course_type" class="form-control" value="<?php echo $login_user[0]->course_type; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Salary</label>
													<div class="col-md-8">
														<input type="text" name="salary" class="form-control" value="<?php echo $login_user[0]->salary; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Turn Over</label>
													<div class="col-md-8">
														<input type="text" name="turn_over" class="form-control" value="<?php echo $login_user[0]->turn_over; ?>">
													</div>
												</div>
                                                
                                                
                                                
                                                 <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Total Job or Business Experience</label>
													<div class="col-md-8">
														<input type="text" name="total_job" class="form-control" value="<?php echo $login_user[0]->total_job; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Current Job Or Business Experience</label>
													<div class="col-md-8">
														<input type="text" name="current_job" class="form-control" value="<?php echo $login_user[0]->current_job; ?>">
													</div>
												</div>
                                                
                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileCompany">Current Designation</label>
													<div class="col-md-8">
														<input type="text" name="current_designation" class="form-control" value="<?php echo $login_user[0]->current_designation; ?>">
													</div>
												</div>
											</fieldset>
											
											<div class="panel-footer">
												<div class="row">
													<div class="col-md-9 col-md-offset-3">
														<button type="submit" name="updateInformation" class="btn btn-primary">Update Information</button>
													</div>
												</div>
											</div>
										</form>
									</div>                                    
                                    
									<div id="bank" class="tab-pane">
										<form class="form-horizontal" method="post" enctype="multipart/form-data">
											<h4 class="mb-xlg">Bank Details</h4>
											<fieldset>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Account Holder</label>
													<div class="col-md-8">
														<input type="text" name="account_holder" class="form-control" value="<?php echo $login_user[0]->account_holder; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Bank Name</label>
													<div class="col-md-8">
														<input type="text" name="bank_name" class="form-control" value="<?php echo $login_user[0]->bank_name; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Branch Name</label>
													<div class="col-md-8">
														<input type="text" name="branch_name" class="form-control" value="<?php echo $login_user[0]->branch_name; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileLastName">Account No.</label>
													<div class="col-md-8">
														<input type="text" name="bank_accountno" class="form-control" value="<?php echo $login_user[0]->bank_accountno; ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileLastName">IFSC</label>
													<div class="col-md-8">
														<input type="text" name="bank_ifsccode" class="form-control" value="<?php echo $login_user[0]->bank_ifsccode; ?>">
													</div>
												</div>		
											</fieldset>
											
											<div class="panel-footer">
												<div class="row">
													<div class="col-md-9 col-md-offset-3">
														<button type="submit" name="updateBankInformation" class="btn btn-primary">Update Bank Information</button>
													</div>
												</div>
											</div>
										</form>
									</div>
									
                                    <div id="change-password" class="tab-pane">

										<form class="form-horizontal" method="post">
											<input type="hidden" name="hiddenpassword" value="<?php echo $login_user[0]->password; ?>">											
											<h4 class="mb-xlg">Change Password</h4>
											<fieldset class="mb-xl">
												<div class="form-group">
													<label class="col-md-4 control-label" for="opwd">Old Password</label>
													<div class="col-md-7">
														<input type="password" class="form-control" data-validation="required" id="opwd" name="opwd">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label" for="npwd">New Password</label>
													<div class="col-md-7">
														<input type="password" class="form-control" data-validation="required" id="npwd" name="npwd">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label" for="rpwd">Repeat New Password</label>
													<div class="col-md-7">
														<input type="password" class="form-control" data-validation="required" id="rpwd" name="rpwd">
													</div>
												</div>
                                                
											</fieldset>
											<div class="panel-footer">
												<div class="row">
													<div class="col-md-9 col-md-offset-3">
														<button type="submit" name="changePassword" class="btn btn-primary">Update Password</button>
													</div>
												</div>
											</div>

										</form>

									</div>
								</div>
							</div>
                          
						</div>
						<div class="col-md-12 col-lg-3">
                        <section class="panel">
							<div class="panel-body">
                                <h4 class="mb-md">Bank Details <br/><span class="title-sec">(Transfer Your Payouts)</span></h4>
                                <hr>
                                <ul class="simple-user-list mb-xlg">
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Account Holder</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        <span class="text-sec"><?php echo $login_user[0]->account_holder; ?></span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Bank Name</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        <span class="text-sec"><?php echo $login_user[0]->bank_name; ?></span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Branch Name</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        <span class="text-sec"><?php echo $login_user[0]->branch_name; ?></span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Account No</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><?php echo $login_user[0]->bank_accountno; ?></span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>IFSC </strong><div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><?php echo $login_user[0]->bank_ifsccode; ?></span>
                                    </li>
                                </ul>
                            </div>
                            </section>
                            
                            <section class="panel">
                            <div class="panel-body">
                                <h4 class="mb-md">Plan Details </h4>
                                <hr>
                                <ul class="simple-user-list mb-xlg">
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Plan Name</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        <span class="text-sec">STP-100</span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Plan Fees </strong><div class="pull-right">:</div></span>
                                        </figure>
                                        <span ><i class="fa fa-rupee"></i> 57500.00</span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Service</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        <span class="text-sec">8000 Clicks</span>
                                        
                                    </li>
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Validity </strong><div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec">12 Months</span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Work</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec">125 Links Per Day</span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Earning Per Click</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><i class="fa fa-rupee"></i> 5.00</span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Earning Per Day</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec"><i class="fa fa-rupee"></i> 625.00</span>
                                    </li>
                                    
                                    
                                     <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Capping</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec">200000 Per Week</span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Business bolume</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec">5BV</span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Joining Date</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec">19-Sep_2016</span>
                                    </li>
                                    
                                     <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Expiry Date</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec">19-Sep-2017</span>
                                    </li>
                                    
                                    <li>
                                        <figure class="image rounded heading-sec">
                                            <span><strong>Payout Mode</strong> <div class="pull-right">:</div></span>
                                        </figure>
                                        
                                        <span class="text-sec">Daily</span>
                                    </li>
                                    
                                    <div class="panel-footer">
										<button type="submit" class="btn btn-warning">Change Payout Mode</button>
									</div>
                                    
                                </ul> 
                            </div>
                           </section>
						</div>

					</div>
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Upcoming Tasks</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
			
								<ul>
									<li>
										<time datetime="2014-04-19T00:00+00:00">04/19/2014</time>
										<span>Company Meeting</span>
									</li>
								</ul>
							</div>
			
							
			
						</div>
					</div>
				</div>
			</aside>
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
	<!-- Analytics to Track Preview Website -->	
			<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
  $.validate({
    lang: 'en'
  });
</script>

	</body>


</html>