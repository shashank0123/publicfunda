<?php $this->load->view('front/layout/header-inner'); ?>
	<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Payout Mode</h2>					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>								
								<li><a href=""><span>Payout Mode</span></a></li>
							</ol>					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>
					<!-- start: page -->

					<div class="row">						
						<div class="col-md-12">
							<section class="panel">
									<header class="panel-heading">	
											<h2 class="panel-title">Payout Mode
											<a href="<?php echo base_url('index.php/user/profile'); ?>" type="submit" name="redeemPoints" class="btn btn-warning pull-right">Back</a>											
											</h2> 
									</header>
									
									<div class="panel-body">
										<div class="table-responsive">
										<?php echo $this->session->flashdata('message'); ?>
											<table class="table mb-none" style="width:50%">												
												<tbody>
													<form method="post">											
													<tr>													
														<td>Select Payout Mode</td>														
														<td>
															<select class="form-control" name="payoutmode">
																<option <?php echo($login_user[0]->payoutmode=='Daily')?'selected':''; ?> value="Daily">Daily</option>
																<option <?php echo($login_user[0]->payoutmode=='Weekly')?'selected':''; ?> value="Weekly">Weekly</option>
																<option <?php echo($login_user[0]->payoutmode=='Monthly')?'selected':''; ?> value="Monthly">Monthly</option>															
																<option <?php echo($login_user[0]->payoutmode=='Quarterly')?'selected':''; ?> value="Quarterly">Quarterly</option>
															</select>
														</td>
														<td><button name="submitButton" type="submit" class="btn btn-info">Submit</button></td>
													</tr>
													</form>
												</tbody>
											</table>
										</div>
									</div>
								</section>                         
						</div>
						

					</div>
					<!--footer start-->
				<?php $this->load->view('front/layout/footer'); ?>	
			<!--end start-->
					<!-- end: page -->
				</section>
			</div>
			
		</section>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->		
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
<script src="<?php echo base_url(); ?>assets/front/javascripts/ui-elements/examples.modals.js"></script>
	</body>


</html>