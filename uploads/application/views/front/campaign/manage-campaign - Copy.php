<?php $this->load->view('front/layout/header-inner'); ?>
<style>
.inactiveMyTab{display:none;}
.activeMyTab{display:block;}
.mypointclass{display:inline !important; width:20%;}
.myclass{margin-right:5px;}
</style>
<?php 
$myplandata = $this->plan_model->selectPlanByID($login_user[0]->plan);
if(count($myplandata)>0)
{
	if($myplandata[0]->validity==12)
	{
		$expcam = 365;
	}
	else
	{
		$expcam = 182;
	}
}
else
{
	$expcam = 0;
}		

$totalPoints = $login_user[0]->totalPoints+$login_user[0]->paidPoints;
if(count($campaignData)>0)
{
	$tcp_cal_1 = 0; 
	foreach($campaignData as $campData)
	{
		$tcp_cal_1 +=$campData->campaignPoints; 
	}
	$tcp_cal = $totalPoints-$tcp_cal_1;
}
else
{
	$tcp_cal = $totalPoints;
}
?>
<?php //print_r($campaignData); ?>
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->load->view('front/layout/left-menu'); ?>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Manage Campaign</h2>
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="<?php echo base_url('index.php/user/dashboard'); ?>">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><a href=""><span>Campaign</span></a></li>
							</ol>
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						
						<div class="col-md-12">
							<div class="tabs">
								<ul class="nav nav-tabs tabs-primary">
                                    <li class="active"><a href="#edit" data-toggle="tab">Update Campaign</a></li>
                                    <li><a href="#campaign-info" data-toggle="tab">Campaign Info</a></li>
                                    <li><a href="#campaign-status" data-toggle="tab">Campaign Status</a></li>
									<li><a href="#purchase-plan" data-toggle="tab">CTP Points</a></li>
                                    <li><a href="#customized-services" data-toggle="tab">Purchase Points</a></li>
                                    <li><a href="#campaign-account" data-toggle="tab">Campaign Account</a></li>
								</ul>
								<div class="tab-content">
									<div id="edit" class="tab-pane active">
									<?php echo $this->session->flashdata('message'); ?>
											<div class="tabs">
												<ul class="nav nav-tabs tabs-danger">
														<?php $cp=0; foreach($campaignData as $campData){ $cp++; ?>
														<li><a href="#Campaign-<?php echo $campData->id; ?>" data-toggle="tab">Campagin <?php echo $cp; ?></a></li>
														<?php } ?>
														<?php if(count($campaignData)<5){ ?>
														<li><a href="#Campaign-add" data-toggle="tab"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Campaign</a></li>
														<?php } ?>
														<li class="pull-right btn btn-info"><strong>Left E-Ponts : <?php echo $tcp_cal; ?></strong></li>
														<li class="pull-right btn btn-info myclass"><strong>Total E-Ponts : <?php echo $totalPoints; ?></strong></li>
														
												</ul>
												
												<div class="tab-content">
													<?php $cp=0; foreach($campaignData as $campData){ $cp++; ?>
													<div id="Campaign-<?php echo $campData->id; ?>" class="tab-pane">                                    	
														<form class="form-horizontal" method="post">
														<input type="hidden" value="<?php echo base64_encode($campData->id); ?>" name="cid" >
														<div id="campaignForm" >
														<fieldset>
															<div class="form-group">
																<label class="col-md-3 control-label" for="profileFirstName">Campaign*</label>
																<div class="col-md-8">
																	<select class="form-control" name="campaign">
																		<option value="Inorganic Visits">Inorganic Visits</option>
																	</select>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label" for="profileFirstName">Campaign Type*</label>
																<div class="col-md-8">
																	<select name="campaignType" class="form-control">
																		<option <?php echo($campData->campaignType=="Individual Campaign Type")?'selected':''; ?> value="Individual Campaign Type">Individual Campaign Type</option>
																		<option <?php echo($campData->campaignType=="Business Campaign Type")?'selected':''; ?> value="Business Campaign Type">Business Campaign Type</option>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-3 control-label" >Campaign Title*</label>
																<div class="col-md-8">
																	<input type="text" name="campaignTitle" value="<?php echo $campData->campaignTitle; ?>" class="form-control" data-validation="required">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-3 control-label" >Campaign Link *</label>
																<div class="col-md-8">
																	<input type="text" name="campaignUrl" <?php echo($campData->campaignStatus==1)?'readonly':''; ?> value="<?php echo $campData->campaignUrl; ?>" class="form-control" data-validation="required">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-3 control-label" >Points</label>
																<div class="col-md-8">  
																	<span id="updatecerror<?php echo $campData->id; ?>"></span>
																	<input type="text" name="oldctp" class="form-control mypointclass" value="<?php echo $campData->campaignPoints; //$tcp_cal; ?>" readonly >
																	<input type="text" name="campaignPoints" id="mypinput<?php echo $campData->id; ?>" value="<?php echo 0; ?>" class="form-control mypointclass" onkeyup="return checkPoints(this.value,<?php echo $tcp_cal; ?>,'mypinput','updcambtm<?php echo $campData->id; ?>','updatecerror<?php echo $campData->id; ?>');" required>
																</div>
															</div>
															<div class="form-group" >
																<label class="col-md-3 control-label" >Campaign Date*</label>
																<div class="col-md-8">															
																
																	<div class="col-md-6"><strong>Start Date</strong><input type="text" name="sdate" value="<?php echo $campData->sdate; ?>" class="form-control" readonly></div>
																	<div class="col-md-6"><strong>End Date</strong><input type="text" value="<?php echo $campData->edate; ?>" name="edate" class="form-control" readonly></div>
																													
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-3 control-label" >Age Limit*</label>
																<div class="col-md-8">
																	<div class="input-daterange input-group" >
																	<span class="input-group-addon">
																		From
																	</span>
																	<select class="form-control" name="sage" style="width:100%" onchange="return setAgeRange(this.value,'eage<?php echo $campData->id; ?>');" data-validation="required">
																		<?php for($sa=18; $sa<=80; $sa++){ ?>
																		<option <?php echo($campData->sage==$sa)?'selected':''; ?>  value="<?php echo $sa; ?>"><?php echo $sa; ?> Years</option>
																		<?php } ?>
																	</select>
																	<span class="input-group-addon">To</span>
																	<select class="form-control" name="eage" style="width:100%" id="eage<?php echo $campData->id; ?>" data-validation="required">
																		<option  value="<?php echo $campData->eage; ?>"><?php echo $campData->eage; ?> Years</option>
																	</select>
																</div>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label" >Gender*</label>
																<div class="col-md-8">
																	<select class="form-control" name="gender">
																	<option <?php echo($campData->gender=='Male')?'selected':''; ?> value="Male">Male</option>
																	<option <?php echo($campData->gender=='Female')?'selected':''; ?> value="Female">Female</option>
																	<option <?php echo($campData->gender=='Both')?'selected':''; ?> value="Both">Both</option>
																</select>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label" >Profession*</label>
																<div class="col-md-8">
																	<select name="profession" class="form-control">
																		<option value="">Select Profession</option>
																		<option <?php echo($campData->profession=='Accountant')?'selected':''; ?> value="Accountant">Accountant</option>
																		<option <?php echo($campData->profession=='Agricultural Advisor')?'selected':''; ?> value="Agricultural Advisor">Agricultural Advisor</option>
																		<option <?php echo($campData->profession=='All')?'selected':''; ?> value="All">All</option>
																		<option <?php echo($campData->profession=='Animator')?'selected':''; ?> value="Animator">Animator</option>
																		<option <?php echo($campData->profession=='Archeologist')?'selected':''; ?> value="Archeologist">Archeologist</option>
																		<option <?php echo($campData->profession=='Architect')?'selected':''; ?> value="Architect">Architect</option>
																		<option <?php echo($campData->profession=='Art Photographer')?'selected':''; ?> value="Art Photographer">Art Photographer</option>
																		<option <?php echo($campData->profession=='Athlete (Sportsman/Woman)')?'selected':''; ?> value="Athlete (Sportsman/Woman)">Athlete (Sportsman/Woman)</option>
																		<option <?php echo($campData->profession=='Business Person')?'selected':''; ?> value="Business Person">Business Person</option>
																		<option <?php echo($campData->profession=='Employed')?'selected':''; ?> value="Employed">Employed</option>
																		<option <?php echo($campData->profession=='Entrepreneur')?'selected':''; ?> value="Entrepreneur">Entrepreneur</option>
																		<option <?php echo($campData->profession=='Hotel Manager')?'selected':''; ?> value="Hotel Manager">Hotel Manager</option>
																		<option <?php echo($campData->profession=='House Wife')?'selected':''; ?> value="House Wife">House Wife</option>
																		<option <?php echo($campData->profession=='Other')?'selected':''; ?> value="Other">Other</option>
																		<option <?php echo($campData->profession=='Self Employed')?'selected':''; ?> value="Self Employed">Self Employed</option>
																		<option <?php echo($campData->profession=='Student')?'selected':''; ?> value="Student">Student</option>
																	</select>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label">Industry*</label>
																<div class="col-md-8">
																	<input type="text" readonly class="form-control" value="All<?php //echo $campData->industry; ?>" name="industry" placeholder="Enter Industry">
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label">Select State*</label>
																<div class="col-md-8"> 
																	<select class="form-control" name="state" onchange="return getCityList(this.value,'citydata<?php echo $campData->id; ?>');">
																		<?php foreach($this->user_model->selectAllState() as $statedata){ ?>
																			<option <?php echo($campData->state==$statedata->id)?'selected':''; ?> value="<?php echo $statedata->id; ?>"><?php echo $statedata->state; ?></option>
																		<?php } ?>    
																	</select>
																</div>
															</div>
															<?php 
																$cityData = $this->user_model->selectCityById($campData->city);
																if(count($cityData)>0)
																{
																	$selecttedCity = "<option selected value='".$cityData[0]->id."'>".$cityData[0]->city_name."</option>";
																}
																else
																{
																	$selecttedCity ="";
																}	
															?>
															<div class="form-group">
																<label class="col-md-3 control-label">City*</label>
																<div class="col-md-8">
																	<select class="form-control" name="city" id="citydata<?php echo $campData->id; ?>" data-validation="required">
																		<option value="">-- Select City --</option>
																		<?php echo $selecttedCity; ?>
																	</select>
																</div>
															</div>
															
															<div class="col-md-10 pull-right">
															<div class="form-group">       
																	<h5><strong>FOR SELF OWNED CAMPAIGN</strong></h5>  
																	<div style="width:100%">
																		<strong><u>Self Declaration for OWN Business or Personal Page</u></strong>
																		<br>
																		I hereby submit a campaign link for promoting my page and it does not contain any
																		malicious or offensive content which will humiliate any person or group.
																		<br>
																		<strong>Terms &amp; Conditions:</strong><br>
																		1. If content in campaign is found to be malicious or offensive then subscription
																		will be suspended immediately without any prior information
																		<br>
																		2. User can not submit any campaign which is already updates by someone else
																	</div>
																</div>
																<div class="form-group">       
																	   <h5><strong>FOR THIRD PARTY CAMPAIGN</strong></h5>        
																	<div  style="width:100%">
																		<strong><u>Self Declaration for THIRD PARTY Business or Personal Page</u></strong><br>
																		I hereby submit a campaign link as an "ON BEHALF' for promoting themselves and it
																		does not contain any malicious or offensive content which will humiliate any person
																		or group.<br>
																		And also state that I am responsible for any query made by the owner of this webpage
																		for any query made regarding promotion made on Social Trade platform.<br>
																		<strong>Terms &amp; Conditions:</strong><br>
																		1. If content in campaign is found to be malicious or offensive then subscription
																		will be suspended immediately without any prior information.<br>
																		2. User can not submit any campaign which is already updated by Someone else.<br>
																		3. In case of Third Party Campaign, all responsibility will be of the user.<br>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-md-3 control-label" style="width: 2%;margin-right: 1%;">
																		<input type="checkbox" data-validation="required" class="radio required" id="chkAccept" name="chkAccept"></label>
																	<div>
																		I have read given campaign details and agree to the above terms &amp; conditions.
																	</div>
																</div>
														</div>													
														</fieldset>

												<div class="panel-footer">
													<div class="row">
														<div class="col-md-9 col-md-offset-4">
															<button type="submit" name="updateCampagn" id="updcambtm<?php echo $campData->id; ?>" class="btn btn-default">Update Campagin</button>
														</div>
													</div>
												</div>
												</div>
												</form>
													</div>
													<?php } ?>

													
													<?php if(count($campaignData)<5){ ?>
													<div id="Campaign-add" class="tab-pane">
											<form class="form-horizontal" method="post">
											<div id="campaignForm">
											<fieldset>
												<div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Campaign*</label>
													<div class="col-md-8">
														<select class="form-control" name="campaign">
															<option value="Inorganic Visits">Inorganic Visits</option>
														</select>
													</div>
												</div>

                                                <div class="form-group">
													<label class="col-md-3 control-label" for="profileFirstName">Campaign Type*</label>
													<div class="col-md-8">
													<select name="campaignType" class="form-control">
														<option value="Individual Campaign Type">Individual Campaign Type</option>
														<option value="Business Campaign Type">Business Campaign Type</option>
													</select>
												</div>

												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" >Campaign Title*</label>
													<div class="col-md-8">
														<input type="text" name="campaignTitle" value="" class="form-control" data-validation="required">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label" >Campaign Link *</label>
													<div class="col-md-8">
														<input type="text" name="campaignUrl" class="form-control" data-validation="required">
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-3 control-label" >Points</label>
													<div class="col-md-8">
														<span id="addcerror"></span>
														<input type="text" name="totalcp" class="form-control mypointclass" value="<?php echo $tcp_cal; ?>" readonly >
														<input type="text" name="campaignPoints" id="mypinput" value="0" class="form-control mypointclass" onkeyup="return checkPoints(this.value,<?php echo $tcp_cal; ?>,'mypinput','addCampagn','addcerror');" data-validation="required">
													</div>
												</div>
                                                <div class="form-group" >
													<label class="col-md-3 control-label" >Campaign Date*</label>
													<div class="col-md-8">
														<div class="col-md-6"><strong>Start Date</strong><input type="text" name="sdate" value="<?php echo date('Y-m-d'); ?>" class="form-control" readonly></div>
														<div class="col-md-6"><strong>End Date</strong><input type="text" name="edate" value="<?php  echo date('Y-m-d',strtotime(date("Y-m-d", $login_user[0]->registration_date) . " + $expcam day")); ?>" class="form-control" readonly></div>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-3 control-label" >Age Limit*</label>
													<div class="col-md-8">
														<div class="input-daterange input-group" >
															<span class="input-group-addon">
																From
															</span>
															<select class="form-control" name="sage" style="width:100%" onchange="return setAgeRange(this.value,'eage');">
																<?php for($sa=18; $sa<=80; $sa++){ ?>
																<option value="<?php echo $sa; ?>"><?php echo $sa; ?> Years</option>
																<?php } ?>
															</select>
															<span class="input-group-addon">To</span>
															<select class="form-control" name="eage" id="eage" style="width:100%" data-validation="required" >
																<option value=""> -- Please Select -- </option>	
															</select>
														</div>
													</div>
												</div>

                                                <div class="form-group">
													<label class="col-md-3 control-label" >Gender*</label>
													<div class="col-md-8">
														<select class="form-control" name="gender">
															<option value="Male">Male</option>
															<option value="Female">Female</option>
															<option value="Both">Both</option>
														</select>
													</div>
												</div>

                                                <div class="form-group">
													<label class="col-md-3 control-label" >Profession*</label>
													<div class="col-md-8">
														<select name="profession" class="form-control" data-validation="required">
																<option value="">Select Profession</option>
																<option value="Accountant">Accountant</option>
																<option value="Agricultural Advisor">Agricultural Advisor</option>
																<option value="All">All</option>
																<option value="Animator">Animator</option>
																<option value="Archeologist">Archeologist</option>
																<option value="Architect">Architect</option>
																<option value="Art Photographer">Art Photographer</option>
																<option value="Athlete (Sportsman/Woman)">Athlete (Sportsman/Woman)</option>
																<option value="Business Person">Business Person</option>
																<option value="Employed">Employed</option>
																<option value="Entrepreneur">Entrepreneur</option>
																<option value="Hotel Manager">Hotel Manager</option>
																<option value="House Wife">House Wife</option>
																<option value="Other">Other</option>
																<option value="Self Employed">Self Employed</option>
																<option value="Student">Student</option>
															</select>
													</div>
												</div>

                                                <div class="form-group">
													<label class="col-md-3 control-label">Industry*</label>
													<div class="col-md-8">
														<input type="text" class="form-control" value="All" readonly name="industry" placeholder="Enter Industry" data-validation="required">
													</div>
												</div>

                                                <div class="form-group">
													<label class="col-md-3 control-label">Select State*</label>
													<div class="col-md-8">
														<select class="form-control" name="state" onchange="return getCityList(this.value,'citydata');">
														<?php foreach($this->user_model->selectAllState() as $statedata){ ?>
                                                            <option value="<?php echo $statedata->id; ?>"><?php echo $statedata->state; ?></option>
                                                        <?php } ?>    
														</select>
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-3 control-label">City*</label>
													<div class="col-md-8">
														<select class="form-control" name="city" data-validation="required" id="citydata">
                                                            <option value="">  -- Select City -- </option>
														</select>
													</div>
												</div>
												<div class="col-md-10 pull-right">
													<div class="form-group">
       
            <h5><strong>FOR SELF OWNED CAMPAIGN</strong></h5>  
        <div style="width:100%">
            <strong><u>Self Declaration for OWN Business or Personal Page</u></strong>
            <br>
            I hereby submit a campaign link for promoting my page and it does not contain any
            malicious or offensive content which will humiliate any person or group.
            <br>
            <strong>Terms &amp; Conditions:</strong><br>
            1. If content in campaign is found to be malicious or offensive then subscription
            will be suspended immediately without any prior information
            <br>
            2. User can not submit any campaign which is already updates by someone else
        </div>
    </div>
	<div class="form-group">       
           <h5><strong>FOR THIRD PARTY CAMPAIGN</strong></h5>        
        <div  style="width:100%">
            <strong><u>Self Declaration for THIRD PARTY Business or Personal Page</u></strong><br>
            I hereby submit a campaign link as an "ON BEHALF' for promoting themselves and it
            does not contain any malicious or offensive content which will humiliate any person
            or group.<br>
            And also state that I am responsible for any query made by the owner of this webpage
            for any query made regarding promotion made on Social Trade platform.<br>
            <strong>Terms &amp; Conditions:</strong><br>
            1. If content in campaign is found to be malicious or offensive then subscription
            will be suspended immediately without any prior information.<br>
            2. User can not submit any campaign which is already updated by Someone else.<br>
            3. In case of Third Party Campaign, all responsibility will be of the user.<br>
        </div>
    </div>
	<div class="form-group">
        <label class="col-md-3 control-label" style="width: 2%;margin-right: 1%;">
            <input type="checkbox" required class="radio required" id="chkAccept" name="chkAccept"></label>
        <div>
            I have read given campaign details and agree to the above terms &amp; conditions.
        </div>
    </div>
												</div>
											
											</fieldset>

											<div class="panel-footer">
												<div class="row">
													<div class="col-md-9 col-md-offset-4">
														<button type="submit" name="addCampagn" id="addCampagn" class="btn btn-default">Submit Campagin</button>
													</div>
												</div>
											</div>
											</div>
											</form>
											</div>
											<?php } ?>
	
												</div>	
											</div>
									</div>




                                    <div id="campaign-info" class="tab-pane">
										<div class="table-responsive">
										<h5><strong>Total E-Ponts : <?php echo ($login_user[0]->totalPoints+$login_user[0]->paidPoints); ?></strong></h5>
											<table class="table table-bordered table-striped table-condensed mb-none">
												<thead>
													<tr>
														<th>SNo.</th>
														<th>Campaign Name</th>
														<th>Total E-Ponts</th>
														<th>Total Recieved Clicks</th>
														<th>Starting Date</th>
														<th>Campaign Status</th>
													</tr>
												</thead>
												<tbody>	
													<?php $snj =0; $campaignInfoTotalPoint=0; $campaignInfoClickedPoint=0; foreach($campaignData as $campaignDataStructure){ $snj++; ?>
													<?php 
														$campaignInfoTotalPoint +=$campaignDataStructure->campaignPoints;
														$campaignInfoClickedPoint +=$campaignDataStructure->recievedclicked;
													?>
													<tr>
														<td><?php echo $snj; ?></td>
														<td><?php echo $campaignDataStructure->campaignTitle; ?></td>
														<td><?php echo $campaignDataStructure->campaignPoints; ?></td>
														<td><?php echo $campaignDataStructure->recievedclicked; ?></td>
														<td><?php echo $campaignDataStructure->sdate; ?></td>
														<td>
															<?php if($campaignDataStructure->campaignStatus==1){ ?>
															<button type="button" class="btn btn-success btn-xs"><b>Active</b></button>
															<?php }else{ ?> 
															<button type="button" class="btn btn-danger btn-xs"><b>Inactive</b></button>
															<?php } ?> 
														</td>
													</tr>
													<?php } ?>
													<tr>
														<td></td>
														<td>Grand Total</td>
														<td><?php echo $campaignInfoTotalPoint; ?></td>
														<td><?php echo $campaignInfoClickedPoint; ?></td>
														<td></td>
														<td></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>

                                    <div id="campaign-status" class="tab-pane">
										<form class="form-horizontal" method="post" id="compaignStatusForm">
											<h4 class="mb-xlg"></h4>
											<fieldset>												
												<div class="form-group">													
													<div class="col-md-5">
														<div class="input-daterange input-group" data-plugin-datepicker="">
															<input type="hidden" name="cuserid" value="<?php echo $login_user[0]->id; ?>">
															<span class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</span>
															<input type="text" class="form-control" name="startDate" id="sd">
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" name="endDate" id="ed">
														</div>
													</div>
                                                    
                                                    <div class="col-md-3">
														<select class="form-control mb-md" name="cid" id="camID">
															<option value="all">All Campaign</option>
															<?php foreach($campaignData as $campData){ ?>
																<option value="<?php echo $campData->id; ?>"><?php echo $campData->campaignTitle; ?></option>
															<?php } ?>
														</select>                                                        
													</div>
                                                    <div class="col-md-4">
														<button type="button" class="btn btn-primary" onclick="return ckeckcampaignstatus();">Submit</button>
														<button type="reset" class="btn btn-default">Reset Filter</button>
													</div>
												</div>
											</fieldset>
										</form>
                                        <span id="cstatusData"></span>
									</div>
									
									<div id="purchase-plan" class="tab-pane">
										<div class="table-responsive">																						
											<table class="table table-bordered table-striped table-condensed mb-none">
												<thead>
													<tr>
														<th width="25%">Plan ePints</th>
														<th>Amount</th>
														<th> </th>	
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><input type="text" class="form-control" name="" value="<?php echo $myplandata[0]->totalVisitors; ?>" readonly></td>
														<td><i class="fa fa-rupee"></i> <?php echo $myplandata[0]->planFees; ?></td>
														<td><button type="buttton" name="purchaseNow" class="btn btn-info" onclick="return mytoggle('toggleplan');">Activate Campaign</button></td>
													</tr> 														
												</tbody>												
											</table>
											<div>
												<div id="toggleplan" style="margin-top:10px; display:none;">
													<div class="col-md-12" >
														
														<div class="panel-group" id="accordion2">
															<div class="panel panel-accordion panel-accordion-primary">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2One">
																			NEFT
																		</a>
																	</h4>
																</div>
																<div id="collapse2One" class="accordion-body collapse in" style="">
																	<div class="panel-body">
																		<form method="post">
																		<table class="table table-bordered table-striped table-condensed mb-none">
																			<tr>
																				<td>Account Holder Name</td>
																				<td><input type="text" name="accountHolderName" value="<?php echo(isset($planInvoice[0]->accountHolderName))?$planInvoice[0]->accountHolderName:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>From Account </td>
																				<td><input type="text" name="fromAccount" value="<?php echo(isset($planInvoice[0]->fromAccount))?$planInvoice[0]->fromAccount:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>Bank Name</td>
																				<td><input type="text" name="bankName" value="<?php echo(isset($planInvoice[0]->bankName))?$planInvoice[0]->bankName:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>To Bank</td>
																				<td><input type="text" name="toBank" value="<?php echo(isset($planInvoice[0]->toBank))?$planInvoice[0]->toBank:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>To Account </td>
																				<td><input type="text" name="toAccount" value="<?php echo(isset($planInvoice[0]->toAccount))?$planInvoice[0]->toAccount:'' ?>" value="<?php echo(isset($planInvoice[0]->accountHolderName))?$planInvoice[0]->accountHolderName:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>E-Point(s)</td>
																				<td><input type="text" name="ePoints" value="<?php echo(isset($planInvoice[0]->ePoints))?$planInvoice[0]->ePoints:$myplandata[0]->totalVisitors; ?>" class="form-control" data-validation="required" readonly></td>
																			</tr>
																			<tr>
																				<td>Amount</td>
																				<td><input type="text" name="amount" value="<?php echo(isset($planInvoice[0]->amount))?$planInvoice[0]->amount:$myplandata[0]->planFees; ?>" value="<?php echo(isset($planInvoice[0]->accountHolderName))?$planInvoice[0]->accountHolderName:'' ?>" readonly class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>UTR Number/Ref Number</td>
																				<td><input type="text" name="utrNumber" value="<?php echo(isset($planInvoice[0]->utrNumber))?$planInvoice[0]->utrNumber:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>
																			<tr>
																				<td>NEFT Date</td>
																				<td><input type="text" name="neftDate" value="<?php echo(isset($planInvoice[0]->neftDate))?$planInvoice[0]->neftDate:'' ?>" class="form-control" data-validation="required"></td>
																			</tr>																			
																			<tr>
																				<td colspan="2">
																					<center><button type="submit"  name="submitPlanInv" class="btn btn-info"> Submit </button></center>
																				</td>
																			</tr>	
																		</table>
																		</form>
																	</div>
																</div>
															</div>
															<div class="panel panel-accordion panel-accordion-primary">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse2Two">
																			 Online Payment
																		</a>
																	</h4>
																</div>
																<div id="collapse2Two" class="accordion-body collapse" style="height: 0px;">
																	<div class="panel-body">
																		<table class="table table-bordered table-striped table-condensed mb-none">
																			<tr>
																				<td>Name</td>
																				<td><?php echo $login_user[0]->name; ?></td>
																			</tr>
																			<tr>
																				<td>Email ID</td>
																				<td><?php echo $login_user[0]->email; ?></td>
																			</tr>
																			<tr>
																				<td>Mobile</td>
																				<td><?php echo $login_user[0]->contact_no; ?></td>
																			</tr>
																			<tr>
																				<td>Amount to pay</td>
																				<td><?php echo $myplandata[0]->planFees; ?></td>
																			</tr>																			
																		</table>
																	</div>
																</div>
															</div>															
														</div>														
													</div>
												</div>											
											</div>
										</div>
                                    </div>
									
                                    <div id="customized-services" class="tab-pane">
										<div class="table-responsive">
											<form method="post" action="https://www.payumoney.com/paybypayumoney/#/257899<?php //echo base_url('index.php/campaign/index'); ?>" >
											<input type="hidden" name="totalAmount" id="totalamount" value="0" >
											<input type="hidden" name="productName" value="Purchase Points" >
											<table class="table table-bordered table-striped table-condensed mb-none">
												<thead>
													<tr>
														<th width="35%">In-Organic Visits</th>
														<th width="15%">Quantity</th>
														<th>Price </th>
														<th>Tax</th>
														<th>Final Price</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Points</td>
														<td><input type="text" class="form-control" name="purchaseTotalPoints" id="qty" onkeyup="return calculatePointsPrice(this.value);" value="0"></td>
														<td><i class="fa fa-rupee"></i> <span id="perUnitPrice">0</span></td>
														<td>10%</td>
														<td><i class="fa fa-rupee"></i> <span id="totalPrice">0</span></td>
													</tr>  
													<tr>
														<th colspan="4" class="text-right">Grand Total: </th>
														<td>
															<i class="fa fa-rupee"></i> 
															<input type="text" name="grandtotal" id="grandPrice" value="0" readonly style="border:none;">
														</td>
													</tr>	
												</tbody>
												<tfoot>
													<tr>
														<th colspan="5" class="text-right">
															<button type="submit" id="paynow" name="paynow" disabled class="btn btn-info">Pay Now</button>
														</th>
													</tr>
												</tfoot>
											</table>
											</form>
										</div>
                                    </div>


                                    <div id="campaign-account" class="tab-pane">
                                        <div class="table-responsive">									
										<h3>coming soon...</h3>
										</div>
                                    </div>
								</div>
							</div>
						</div>
					</div>
					<!--footer start-->
						<?php $this->load->view('front/layout/footer'); ?>	
					<!--end start-->
					<!-- end: page -->
				</section>
			</div>

		</section>
		
		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/vendor/style-switcher/style.switcher.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		<!-- Specific Page Vendor -->
		<script src="<?php echo base_url(); ?>assets/front/vendor/jquery-autosize/jquery.autosize.js"></script>
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.js"></script>
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.custom.js"></script>
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/front/javascripts/theme.init.js"></script>
		<!-- Analytics to Track Preview Website -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>		
<script>
  $.validate({
    lang: 'en'
  });
  
  function mytoggle(TOGGLEDIV)
  {
	  $('#'+TOGGLEDIV).slideToggle();
  }
function setAgeRange(VAL,SELCTID)
{
	if(VAL!="")
	{
	  var options="";
	  var startOption = VAL;
      var endOption=80;
      for(var i=startOption;i<=endOption;i++)
      {
			if(i!=VAL)
			{
				 options+="<option value='"+i+"'>"+i+" Years</option>";
			}
      }       
      $("#"+SELCTID).html(options);  
	}	
}
function checkPoints(POINTSVAL,TOTALPOINTS,INPUTID,BTNID,ERRORDIV)
{
	if(POINTSVAL>0)
	{
		if(POINTSVAL<=TOTALPOINTS)
		{
			$('#'+INPUTID).css({"border": "1px solid #ccc"});
			$('#'+BTNID).removeAttr("disabled", 'disabled');
			$('#'+ERRORDIV).html('');
		}
		else
		{
			$('#'+INPUTID).css({"border": "2px solid #D2312D"});
			$('#'+BTNID).attr("disabled", 'disabled');
			$('#'+ERRORDIV).html('<div class="alert alert-danger"><strong>Invalid points !</strong> please check your total e-points</div>');
		}	
	}
}
  
  function getCityList(SID,CITYDIVID)
  {
	if(SID!="")
	{ 
		jQuery.ajax({
            url: "<?php echo base_url('index.php/user/getCityByStateId/'); ?>",
            data:'SID='+SID,
            type: "GET",			
            success:function(mydata)
			{			
				$('#'+CITYDIVID).html(mydata); //alert('error');
			},
            error:function (mydata){
				alert(mydata);
			}
        });
		
	}	  
  }
  
function updatePoints(FORMID)
{
	/*<i class="fa fa-spinner faa-spin animated"></i>*/
	var formData = $('#cpform'+FORMID).serialize();
	jQuery.ajax({
            url: "<?php echo base_url('index.php/campaign/updatePoints'); ?>",
            data:formData,
			//cache: false,
            type: "POST",
			beforeSend: function() {
			   $('#btnupt'+FORMID).html('<i class="fa fa-spinner faa-spin animated"></i>');    
            },
            success:function(mydata){
				//$('#btnupt'+FORMID).html('Update'); 
				if(mydata==0)
				{
					$('#btnupt'+FORMID).html('Update'); //alert('error');
					//alert(mydata);
				}
				else
				{
					//alert(mydata);
					$('#btnupt'+FORMID).html('Updated'); 
				}
            },
            error:function (){}
        });
}
  
function addCampaign()
{
	$('#campaignForm').slideToggle(100);
}

function mytabcustom(ACTTAB)
{
	$('.tab').removeClass('activeMyTab');
	$('#'+ACTTAB).addClass('activeMyTab');
}

function calculatePointsPrice(QTY)
{	
	if(QTY!="")
	{	
		var unitPrice = 10;
		if(QTY >=500 && QTY <=999 )
		{
			unitPrice = 10;
		}
		if(QTY >=1000 && QTY <=1999 )
		{
			unitPrice = 8;
		}
		if(QTY >=2000 && QTY <=4999 )
		{
			unitPrice = 7;
		}
		if(QTY >=5000 && QTY <=9999 )
		{
			unitPrice = 6;
		}
		if(QTY >=10000 && QTY <=14999 )
		{
			unitPrice = 5.50;
		}
		if(QTY >=15000 && QTY <=10000000000 )
		{
			unitPrice = 5;
		}		
		
		var priceTotal = QTY*unitPrice;
		var serviceTax = 10;
		var tax = (priceTotal*serviceTax)/100;
		var grandTotal = priceTotal+tax;
		
		$('#perUnitPrice').html(unitPrice);
		$('#totalPrice').html(Math.round(grandTotal));
		$('#grandPrice').val(Math.round(grandTotal));
		$('#totalamount').val(Math.round(grandTotal));	
		if($('#grandPrice').val!="")
		{
			$("#paynow").removeAttr("disabled");
			
		}	
	}
	else
	{
		$('#perUnitPrice').html('0');
		$('#totalPrice').html('0');
		$('#grandPrice').val('0');
		$("#paynow").attr( "disabled", "disabled" );
	}	
}

function ckeckcampaignstatus()
{
	var sdate = $('#sd').val();
	var edate = $('#ed').val();
	var cid = $('#camID').val();
	
	if(sdate=="")
	{
		$('#sd').focus();
		$("#sd").css( 'border','1px solid #FF0000');
		return false;
	}	
	if(edate=="")
	{
		$('#ed').focus();
		$("#ed").css( 'border','1px solid #FF0000');
		return false;
	}
	if(cid=="")
	{
		$('#camID').focus();
		$("#camID").css( 'border','1px solid #FF0000');
		return false;
	}
	
	if(sdate!="" && edate!="" && cid!="")
	{
		var formData = $('#compaignStatusForm').serialize();
		jQuery.ajax({
            url: "<?php echo base_url('index.php/campaign/checkcampaignStatus'); ?>",
            data:formData,
            type: "POST",
            success:function(mydata){
				$('#cstatusData').html(mydata);
            },
            error:function (){}
        });
	}	
		
}


</script>
	</body>


</html>