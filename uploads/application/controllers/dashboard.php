<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends CI_Controller{
	public function listing(){
		$this->load->model('dashboard_model');
		$data["Dashboard"]=$this->dashboard_model->list_dashboard();
		//echo '<pre>';
		//print_r($data);die("testingggggg");
        $this->load->view("admin/Dashboard/listing",$data);  
	}
	public function edit_listing($id){
		$this->load->model('dashboard_model');
		$this->data['Dashboard']= $this->dashboard_model->edit_listing($id);
		if(isset($_POST['edit_listing'])){
			$data["title"]=         $this->input->post("title");
			$data["description"]=         $this->input->post("description");
			$data["status"]=         $this->input->post("status");
			$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully update dashboard</div>');
			$this->dashboard_model->update_daseboard($id,$data);
			redirect("Dashboard/listing/".$id);       	
			}  
		$this->load->view('admin/Dashboard/edit_listing', $this->data, FALSE);
		}
	public function list_slider(){
		$this->load->model('dashboard_model');
		$data["slider"]=$this->dashboard_model->list_slider();
        $this->load->view("admin/Dashboard/list_slider",$data);  
	}	
}
