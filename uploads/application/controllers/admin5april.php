<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	
	public function index()
	{	
		$this->load->model('admin_model');
		if(isset($_POST['loginAdmin']))
		{  
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			if(!empty($username) && !empty($password))
			{
				$userdata = $this->admin_model->adminLogin($username,base64_encode($password));
				if(count($userdata)==1)
				{
					if($userdata[0]->status==1)
					{
						$user_array = array('ADMINID'=>$userdata[0]->id,'ADMINTYPE'=>$userdata[0]->type,'ADMINLOGIN'=>true);
						$this->session->set_userdata($user_array);
						redirect('index.php/admin/dashboard');
					}
					else
					{
						$this->session->set_flashdata('message','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Invalid Username or Password !</strong></div>');
						redirect('index.php/admin');
					}	
					
				}	
				else
				{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Invalid Username or Password !</strong></div>');
					redirect('index.php/admin');
				}
			}
			else
			{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Invalid Username or Password !</strong></div>');
				redirect('index.php/admin');
			}	
		}	
		$this->load->view('admin/login');
	}
	
	function dashboard()
	{
		$this->load->view('admin/dashboard');
	}
	
	function admin_logout()
	{
		$this->session->set_userdata(array('ADMINID'=>'','ADMINTYPE'=>'','ADMINLOGIN'=>''));
		$this->session->sess_destroy();
		$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully logout.</div>');
		redirect('index.php/admin/index');	
	}
	function admin_setting()
	{
		$this->load->model('admin_model');
		$admin_login_id = $this->session->userdata('ADMINID'); 
		//echo $admin_login_id;
		if(isset($_POST['changePassword']))
		{
			$opwd = $this->input->post('opwd');
			$npwd = $this->input->post('npwd');
			$cpwd = $this->input->post('cpwd');
			
			if(!empty($opwd) && !empty($npwd) && !empty($cpwd))
			{
				$admindata = $this->admin_model->checkAdminByPassword($admin_login_id,base64_encode($opwd));
				//print_r($admindata); die();
				if(count($admindata)==1)
				{
					if($npwd==$cpwd)
					{
						$pdata['password']= base64_encode($npwd);
						$this->admin_model->updateAdminById($admin_login_id,$pdata);
						$this->session->set_flashdata('message','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>your password has been changed successfully.</strong></div>');
						redirect('index.php/admin/admin_setting');
					}
					else
					{
						$this->session->set_flashdata('message','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>New password does not match the confirm password !</strong></div>');
						redirect('index.php/admin/admin_setting');
					}	
				}
				else
				{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Invalid old password !</strong></div>');
					redirect('index.php/admin/admin_setting');
				}	
				
			}
			else
			{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Please fill all password fields !</strong></div>');
				redirect('index.php/admin/admin_setting');
			}	
		}	
		
		if(isset($_POST['updateinformation']))
		{
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			
			$idata['username']= $username;
			$idata['email']= $email;
			$this->admin_model->updateAdminById($admin_login_id,$idata);
			$this->session->set_flashdata('message','<div class="alert alert-success"><strong>your information has been changed successfully.</strong></div>');
			redirect('index.php/admin/admin_setting');	
		}	
		$mydata['adminData'] = $this->admin_model->selectAdminById($admin_login_id);
		$this->load->view('admin/setting',$mydata);
	}
	
	function useractivationpanel()
	{
		$this->load->model('trackadminwork_model');
		if(isset($_POST['checkComments']))
		{
			$sdate = $this->input->post('startDate');
			$edate = $this->input->post('endDate');
			if(!empty($sdate) && !empty($edate))
			{						
			   //$date_1 = 
				$this->db->where('invDate >=',date('Y-m-d',strtotime($sdate)));
				$this->db->where('invDate <=',date('Y-m-d',strtotime($edate)));
			}	
			if(isset($_POST['user_id']) && !empty($_POST['user_id']))
			{
				$this->db->where('user_id',$_POST['user_id']);
			}	
		}
		$data['invData'] = $this->tree_model->selectAllUserInvoiceOrderBy(); 
        $this->load->view('admin/user/manage-user-invoice',$data);
	}
	function createuserinvoice()
	{ 
	    //error_reporting(0);
		$this->load->model('tree_model');
		$this->load->model('trackadminwork_model');
		$args = func_get_args();
		if(isset($_POST['updateData']))
		{
			$user_id = $this->input->post('user_id');
			if(!empty($user_id))
			{
				$transactionID = $this->input->post('transactionID');
				if(!empty($transactionID))
				{
					$inv = $this->tree_model->selectAllUserInvoicebytrid($transactionID);
					
						if(count($inv)>0)
						{
							$this->session->set_flashdata('message','<div class="alert alert-danger">transaction already exists please choose a different one</div>');
							redirect('index.php/admin/createuserinvoice/');
						}
				
				}
				
				$invData['invoiceID']=$this->input->post('invoiceID');
			    $invData['user_id']= $user_id;
			    $invData['finalAmount']= $this->input->post('finalAmount');
			    $invData['planName']= $this->input->post('planName');
			    $invData['amount']=$this->input->post('amount');
			    $invData['tax']=  $this->input->post('tax');
			    $invData['transactionID']= $this->input->post('transactionID');
			    $invData['status']= $this->input->post('status');
			    $invData['invDate']= date('Y-m-d');
			    $invData['invTime']= time();

				$user_invoice = $this->user_model->getUserInvoiceLattestOne($user_id);
				//echo $user_invoice[0]->amount; die();
				if($user_invoice>0)
				{
					
				  if($this->input->post('amount')>$user_invoice[0]->amount)
				  {  
			        //echo $this->checkexpirevaliditybyamount($this->input->post('amount')); die();
				   // $udata['expireuser'] = date('Y-m-d');		
                    $udata['capping'] = $this->applycappingtouser($this->input->post('amount'));				
			        $udata['expiremonths'] = $this->checkexpirevaliditybyamount($this->input->post('amount'));//$plandata[0]->validity;
				  }
				}

				
			    $this->user_model->insertPlanInvoice($invData);
				
				$trData['user_id']= $this->input->post('user_id');
				$trData['admin_id']= $this->session->userdata('ADMINID');
                $trData['invoice_id']= $this->input->post('invoiceID');					 
				$trData['action_date']= date('Y-m-d');
			    $trData['action_time']= time();			
				
				
				
			    $this->trackadminwork_model->insertData($trData);
				
				$promotional_income =  $this->input->post('promotional_income');
				if($promotional_income!='none')
				{	
				  $udata['promotional_income'] = $promotional_income;
				}
				$kyc_status =  $this->input->post('kyc_status');
				if($kyc_status!='none')
				{	
				  $udata['kyc_status'] = $kyc_status;
				}
				$bank_details =  $this->input->post('bank_details');
				if($bank_details!='none')
				{	
				  $udata['bank_details'] = $bank_details;
				}				
				$profilelock =  $this->input->post('profilelock');
				if($profilelock!='none')
				{	
				  $udata['profilelock'] = $profilelock;
				}
				$lock_field =  $this->input->post('lock_field');
				if($lock_field!='none')
				{	
				  $udata['lock_field'] = $lock_field;
				}
				$totalPoints =  $this->input->post('totalPoints');
				if($totalPoints!='0')
				{	
				  $udata['totalPoints'] = $totalPoints;
				}
				
			    if(isset($udata)){
				$this->user_model->updateData($user_id,$udata);
				}
			    $this->session->set_flashdata('message','<div class="alert alert-success">Invoice has been successfully created.</div>');
			    redirect('index.php/admin/useractivationpanel/');
			}	
			
		}	
		$data['tax'] = $this->admin_model->getAllTaxes(1);
		$this->load->view('admin/user/add-user-invoice',$data);
	}
	
	
	function edituserinvoice()
	{
		$this->load->model('tree_model');
		$this->load->model('trackadminwork_model');
		$args = func_get_args();
		if(isset($_POST['updateData']))
		{
			$user_id = $this->input->post('user_id');
			if(!empty($user_id))
			{
				
				$transactionID = $this->input->post('transactionID');
				if(!empty($transactionID))
				{
					$inv = $this->tree_model->selectAllUserInvoice($args[0],$transactionID);
					//if($inv[0]->id!=$args[0])
					//{
						if(count($inv)>0)
						{
							$this->session->set_flashdata('message','<div class="alert alert-danger">transaction already exists please choose a different one</div>');
							redirect('index.php/admin/edituserinvoice/'.$args[0]);
						}
					//}
				}	
				
				$invData['invoiceID']=$this->input->post('invoiceID');
			    $invData['user_id']= $user_id;
			    $invData['finalAmount']= $this->input->post('finalAmount');
			    $invData['planName']= $this->input->post('planName');
			    $invData['amount']=$this->input->post('amount');
			    $invData['tax']=  $this->input->post('tax');
			    $invData['transactionID']= $this->input->post('transactionID');
			    $invData['status']= $this->input->post('status');
				
				
			    //$invData['invDate']= date('Y-m-d');
			    //$invData['invTime']= time();			
			    //$this->user_model->insertPlanInvoice($invData);
				$this->tree_model->updateUserInvoice($args[0],$invData);
	
				
				
				$trData['admin_id']= $this->session->userdata('ADMINID');
                $trData['user_id']= $this->input->post('user_id');	
                $trData['invoice_id']= $this->input->post('invoiceID');					
				$trData['action_date']= date('Y-m-d');
			    $trData['action_time']= time();			
			    $this->trackadminwork_model->insertData($trData);
				
				$promotional_income =  $this->input->post('promotional_income');
				if($promotional_income!='none')
				{	
				  $udata['promotional_income'] = $promotional_income;
				}
				$kyc_status =  $this->input->post('kyc_status');
				if($kyc_status!='none')
				{	
				  $udata['kyc_status'] = $kyc_status;
				}
				$bank_details =  $this->input->post('bank_details');
				if($bank_details!='none')
				{	
				  $udata['bank_details'] = $bank_details;
				}				
				$profilelock =  $this->input->post('profilelock');
				if($profilelock!='none')
				{	
				  $udata['profilelock'] = $profilelock;
				}
				$lock_field =  $this->input->post('lock_field');
				if($lock_field!='none')
				{	
				  $udata['lock_field'] = $lock_field;
				}
				$totalPoints=  $this->input->post('totalPoints');
				if($totalPoints!='0')
				{	
				 // $udata['totalPoints'] = $totalPoints;
				}
				if(isset($udata)){
				$this->user_model->updateData($user_id,$udata);
                                }             
			    $this->session->set_flashdata('message','<div class="alert alert-success">Invoice has been successfully created.</div>');
			    redirect('index.php/admin/useractivationpanel/');
			}	
			
		}	
		$data['invData'] = $this->tree_model->selectUserInvoiceById($args[0]);
		$data['tax'] = $this->admin_model->getAllTaxes(1);
		$this->load->view('admin/user/edit-user-invoice',$data);
	} 
	function checkexpirevaliditybyamount($amount)
	{
		if($amount>=0 and $amount<=999)
        {
	     return 3;
        }
        if($amount>=1000 and $amount<=9999)
        {
	      return 6;
        }
        if($amount>=10000)
        {
	      return 12;
        }
	}
	function applycappingtouser($amount)
	{        
        if($amount>=500)
        {
	      return 100000;
        }
		else
		{
			return 0; 
		}
	}
	function manage_subadmin()
	{		
		$data['adminData'] = $this->admin_model->getAllSubAdmin();
		$this->load->view('admin/sub-admin-listing',$data);
	}
	
	function add_admin()
	{
		if(isset($_POST['suubmitData']))
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$email = $this->input->post('email');
			$status = $this->input->post('status');
			
			$adminData = $this->admin_model->checkAdminByUsername($username);
			if(count($adminData)>0)
			{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Username is already used please choose another.</strong></div>');
				redirect('index.php/admin/add_admin');
			}
			else
			{
				if(count($_POST['module'])>0)
				{
					$module = implode(",",$_POST['module']);
				}
				else{
					$module = "";
				}	
				
				$adata['username'] = $username;
				$adata['module'] = $module;
				$adata['password'] = base64_encode($password);
				$adata['email'] = $email;
				$adata['type'] = 2;
				$adata['cdate'] = time();	
				$adata['status'] = $status;			
				$ins = $this->admin_model->insertAdmin($adata);
				if($ins)
				{
					$this->session->set_flashdata('message','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>your password has been changed successfully.</strong></div>');
					redirect('index.php/admin/manage_subadmin');
				}
			}	
		}
		
		$this->load->view('admin/add-sub-admin');
	}
	
	function edit_admin()
	{
		$args = func_get_args();
		if(isset($_POST['suubmitData']))
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$email = $this->input->post('email');
			$status = $this->input->post('status');
			//print_r($_POST['module']); die();
			if(count($_POST['module'])>0)
			{
				$module = implode(",",$_POST['module']);
			}
			else{
				$module = "";
			}	
			
			$adata['username'] = $username;
			$adata['module'] = $module;
			$adata['password'] = base64_encode($password);
			$adata['email'] = $email;
			$adata['type'] = 2;
			$adata['cdate'] = time();	
			$adata['status'] = $status;			
			$ins = $this->admin_model->updateAdminById($args[0],$adata);
			if($ins)
			{
				$this->session->set_flashdata('message','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>admin successfully updated.</strong></div>');
				redirect('index.php/admin/manage_subadmin');
			}
			
		}
		$data['adminData'] = $this->admin_model->selectAdminById($args[0]);
		$this->load->view('admin/add-sub-admin',$data);
	}
	
	function admin_delete()
	{
		$args = func_get_args();
		$this->admin_model->deleteAdmin($args[0]);
		$this->session->set_flashdata('message','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>admin has been successfully deleted.</strong></div>');
		redirect('index.php/admin/manage_subadmin');
	}
	
	function list_user(){
		
    	$data['USER'] = $this->user_model->getAllUsers();         
        $this->load->view('admin/user-listing', $data);
	}
	public function update_campaign_status()
	{
		$args = func_get_args();
		if($args[0]=='act')
		{
			$data['campaignStatus'] = 1;
			$this->campaign_model->updateCampaignData($args[1],$data);
			$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>campaign has been successfully active.</div>');	
		}
		else
		{
			$data['campaignStatus'] = 0;
			$this->campaign_model->updateCampaignData($args[1],$data);
			$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>campaign has been successfully inactive.</div>');	
		}	
		redirect('admin/user_campaign/'.$args[2]);		
	}
	function user_campaign()
	{
		$args = func_get_args();
		
		if(isset($_POST['updateCampagn']))
		{									
			$cdata['campaign'] = $this->input->post('campaign');
			$cdata['campaignType'] = $this->input->post('campaignType');
			$cdata['campaignTitle'] = $this->input->post('campaignTitle');
			$cdata['campaignUrl'] = $this->input->post('campaignUrl');
				
			$points = $this->input->post('campaignPoints');
			if(!empty($points) && $points>0)
			{
				$cdata['campaignPoints'] = $this->input->post('campaignPoints')+$_POST['oldctp'];
			}				
					
			//$cdata['sdate'] = $this->input->post('sdate');
			//$cdata['edate'] = $this->input->post('edate');
			$cdata['sage'] = $this->input->post('sage');
			$cdata['eage'] = $this->input->post('eage');
			$cdata['gender'] = $this->input->post('gender');
			$cdata['profession'] = $this->input->post('profession');
			$cdata['industry'] = $this->input->post('industry');
			$cdata['state'] = $this->input->post('state');
			$cdata['city'] = $this->input->post('city');
			$cdata['updateDate'] = time();
			$cid = base64_decode($this->input->post('cid'));
			$this->campaign_model->updateCampaignData($cid,$cdata);					
			$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>your campaign has been successfully Updated.</div>');	
			redirect('index.php/admin/user_campaign/'.$args[0]);
			
		}
		
		
		$data['campaignData'] = $this->campaign_model->selectCampaignByUserID($args[0]);
		$data['userdata'] = $this->user_model->selectUserByID($args[0]);
		$this->load->view('admin/manage-campaign', $data);		
	}
	
	function user_ligin()
	{
		$uid= func_get_args();		
		if(count($uid)>0)
		{
			$userdata = $this->user_model->selectUserByID($uid[0]);
			if(count($userdata)>0)
			{
				$user_id = $userdata[0]->id;
				$user_email = $userdata[0]->email;
				$user_array = array('USERID'=>$user_id,'USEREMAIL'=>$user_email,'LOGINBY'=>'ADMIN','USERLOGIN'=>true);
				$this->session->set_userdata($user_array);			
				redirect('index.php/user/dashboard');
			}
			else
			{
				redirect('index.php/admin/list_user');
			}	
		}
		else
		{
			redirect('index.php/admin/list_user');
		}
	}
	function user_setting()
	{
		$args = func_get_args();
		$user = $this->user_model->selectUserByID($args[0]); 
		if(isset($_POST['updateCampaignSetting']))
		{
			$totalPoints = $this->input->post('totalPoints');
			
			$udata['no_of_campaign'] = $this->input->post('no_of_campaign');
			$udata['update_campaign'] = $this->input->post('update_campaign');
			$udata['campaign_info'] = $this->input->post('campaign_info');
			$udata['campaign_status_display'] = $this->input->post('campaign_status_display');
			$udata['purchase_points'] = $this->input->post('purchase_points');
			$udata['book_my_add'] = $this->input->post('book_my_add');
			$udata['lock_field'] = $this->input->post('lock_field');
			$udata['totalPoints'] = $totalPoints+$user[0]->totalPoints; //$this->input->post('totalPoints');
			$this->user_model->updateData($args[0],$udata);
			redirect('admin/user_setting/'.$args[0]);
		}

		if(isset($_POST['updateProfileSetting']))
		{
			$udata['status'] = $this->input->post('status');
			$udata['boosterStatus'] = $this->input->post('boosterStatus');
			$udata['bank_details'] = $this->input->post('bank_details');
			$udata['pay_botton'] = $this->input->post('pay_botton');
			$udata['payout_button'] = $this->input->post('payout_button');
			$udata['comment_section'] = $this->input->post('comment_section'); 
			$udata['profilelock'] = $this->input->post('profilelock');
			$udata['booster_task'] = $this->input->post('booster_task');
			$udata['expiremonths'] = $this->input->post('expiremonths');
			
			$this->user_model->updateData($args[0],$udata);
			redirect('admin/user_setting/'.$args[0]);
		}	
		
		if(isset($_POST['updateTaskSetting']))
		{
			$action = $this->input->post('points_action');
			$links = $this->input->post('click_task');
			if($links!="")
			{
				if($action=='inc')
				{
					$linkPerDay = ($user[0]->linkPerDay + $links);
				}
				else
				{
					if($links<=$user[0]->linkPerDay)
					{
						$linkPerDay = ($user[0]->linkPerDay - $links);
					}
					else
					{
						$linkPerDay = $user[0]->linkPerDay;
					}
				}
				$udata['linkPerDay'] = $linkPerDay;
			}					
			
			$udata['clickprice'] = $this->input->post('clickprice');
			$udata['today_task'] = $this->input->post('today_task');
			$udata['free_viewtab'] = $this->input->post('free_viewtab');			
			$this->user_model->updateData($args[0],$udata);
			redirect('admin/user_setting/'.$args[0]);
		}
		
		if(isset($_POST['updateWalletSetting']))
		{
			$udata['wallet_display'] = $this->input->post('wallet_display');			
			$udata['smf'] = $this->input->post('smf');
			$udata['smb'] = $this->input->post('smb');
			$this->user_model->updateData($args[0],$udata);
			redirect('admin/user_setting/'.$args[0]);
		}	
		
		if(isset($_POST['updateWorkPayoutSetting']))
		{
			$udata['workpayout'] = $this->input->post('workpayout');			
			$udata['myearnings'] = $this->input->post('myearnings');
			$udata['walletstatement'] = $this->input->post('walletstatement');
			$this->user_model->updateData($args[0],$udata);
			redirect('admin/user_setting/'.$args[0]);
		}
		
		if(isset($_POST['updateWorkHistorySetting']))
		{
			$udata['workHistory'] = $this->input->post('workHistory');
			$this->user_model->updateData($args[0],$udata);
			redirect('admin/user_setting/'.$args[0]);
		}
		
		if(isset($_POST['updatemyNetworkSetting']))
		{
			$udata['my_network'] = $this->input->post('my_network');
			$udata['promotional_income'] = $this->input->post('promotional_income');
			$udata['myteam'] = $this->input->post('myteam');
			$udata['my_bussiness'] = $this->input->post('my_bussiness');
			$udata['kyc_status'] = $this->input->post('kyc_status');
			$this->user_model->updateData($args[0],$udata);
			redirect('admin/user_setting/'.$args[0]);
		}
		
		
		
    	$data['user'] = $user; //$this->user_model->selectUserByID($args[0]);         
        $this->load->view('admin/user-settings', $data);
	}
	
	function update_user_plan()
	{
		$args = func_get_args();
		$this->load->model('tree_model');		
		$campaignData = $this->campaign_model->selectCampaignByUserID($args[0]);
			
		if(isset($_POST['updateData']))
		{		
			$consumed_points = 0;
			if(count($campaignData)>0)
			{
				foreach($campaignData as $campaign)
				{
					$consumed_points += $campaign->recievedclicked;
					$campaign_id = $campaign->id;
					$cdata['campaignPoints'] = 0 ;
					$this->campaign_model->updateCampaignData($campaign_id,$cdata);	
				}
			}			
			$planId = $this->input->post('plan');
			$plandata = $this->plan_model->selectPlanByID($planId);	
			
			$total_ctp_points =  $plandata[0]->totalVisitors-$consumed_points;
			
			$udata['plan'] = $this->input->post('plan');  			
			$udata['totalPoints'] = $total_ctp_points; //$plandata[0]->totalVisitors;
			$udata['linkPerDay'] = $plandata[0]->linkPerDay;
			$udata['expireuser'] = date('Y-m-d');			
			$udata['expiremonths'] = $plandata[0]->validity;
			$udata['capping'] = $plandata[0]->capping;
			$udata['registration_date'] = time();
			$udata['rdate'] = date('Y-m-d');
			
			$this->user_model->updateData($args[0],$udata);
			redirect('admin/user_setting/'.$args[0]);
		}		
    	$data['user'] = $this->user_model->selectUserByID($args[0]);
        $this->load->view('admin/user/update_plan', $data);
	}
	function user_invoice()
	{
		$args = func_get_args();
		$this->load->model('tree_model');
		if(isset($_POST['updateCampaignSetting']))
		{		
			$udata['lock_field'] = $this->input->post('lock_field');
			$this->user_model->updateData($args[0],$udata);
			redirect('admin/user_setting/'.$args[0]);
		}
		
    	$data['user'] = $this->user_model->selectUserByID($args[0]);
		$data['invData'] = $this->tree_model->selectUserInvoice($args[0]); 		
        $this->load->view('admin/user/manage-invoice', $data);
	}
	
	function user_invoice_edit()
	{
		$this->load->model('tree_model');
		$args = func_get_args();
		if(count($args))
		{
			if(isset($_POST['updateData']))
			{
				$transactionID = $this->input->post('transactionID');
				if(!empty($transactionID))
				{
					$inv = $this->tree_model->selectAllUserInvoice($args[0],$transactionID);
					if(count($inv)>0)
					{
						$this->session->set_flashdata('message','<div class="alert alert-danger">transaction already exists please choose a different one</div>');
						redirect('index.php/admin/user_invoice_edit/'.$args[0].'/'.$args[1]);
					}
				}	
					
				
				$invData['invoiceID']=$this->input->post('invoiceID');
				//$invData['user_id']= $args[1];
				$invData['finalAmount']= $this->input->post('finalAmount');
				$invData['planName']= $this->input->post('planName');
				$invData['amount']=$this->input->post('amount');
				$invData['tax']=  $this->input->post('tax');
				$invData['transactionID']= $this->input->post('transactionID');
				$invData['status']= $this->input->post('status');
				$this->tree_model->updateUserInvoice($args[0],$invData);
				$this->session->set_flashdata('message','<div class="alert alert-success">Invoice has been successfully updated.</div>');
				redirect('index.php/admin/user_invoice/'.$args[1]);
			}	
			
			$data['tax'] = $this->admin_model->getAllTaxes(1);
			$data['user'] = $this->user_model->selectUserByID($args[1]);
			$data['invData'] = $this->tree_model->selectUserInvoiceById($args[0]); 
			$this->load->view('admin/user/edit-invoice', $data);
		}
		else
		{
			redirect('index.php/admin/dashboard');
		}	
	}
	function user_invoice_delete()
	{
		$args = func_get_args();
		$this->load->model('tree_model');		
		$this->tree_model->deleteUserInvoice($args[0]);	
		$this->session->set_flashdata('message','<div class="alert alert-success">Invoice has been successfully deleted.</div>');
		redirect('index.php/admin/user_invoice/'.$args[1]);
	}
	
	function create_user_invoice()
	{
		$args = func_get_args();
		if(count($args))
		{
			$invData['invoiceID']='MOTIF/'.date('Y').'/'.time();
			$invData['user_id']= $args[0];
			$invData['finalAmount']= 00;
			$invData['planID']= 00;
			$invData['planName']= 'invoice';
			$invData['invDate']= date('Y-m-d');
			$invData['invTime']= time();
			$invData['amount']=00;
			$invData['tax']= 00;
			$invData['status']= 'pending';
			$this->user_model->insertPlanInvoice($invData);
			$this->session->set_flashdata('message','<div class="alert alert-success">Invoice has been successfully created.</div>');
			redirect('index.php/admin/user_invoice/'.$args[0]);
		}
		else
		{
			redirect('index.php/admin/dashboard');
		}	
	}
	
	/*awdesh coding*/
	function list_plans()
	{ 
      $data['PLANE'] =   $this->admin_model->list_plans(); 
       $this->load->view('admin/plans/list_plans', $data);
	}
    function edit_list_planes($id)
	{
         $args = func_get_args();
         if(isset($_POST['edit_list_planes'])) 
		 {
        	$data['planName'] = $this->input->post('planName');
        	$data['planFees'] = $this->input->post('planFees');
        	$data['totalVisitors'] = $this->input->post('totalVisitors');
        	$data['validity'] = $this->input->post('validity');
        	$data['linkPerDay'] = $this->input->post('linkPerDay');
        	$data['earningPerClick'] = $this->input->post('earningPerClick');
        	$data['capping'] = $this->input->post('capping');
        	$data['turnover'] = $this->input->post('turnover');
        	$data['plancode'] = $this->input->post('plancode');
        	$data['paymentLink'] = $this->input->post('paymentLink');
			$data['status'] = $this->input->post('status');
        	$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully update plan</div>');
        	$this->plan_model->updateData($args[0],$data);
        	redirect('admin/list_plans/');       	
         }  
		$pdata['PLAN']= $this->plan_model->selectPlanByID($args[0]);
		$this->load->view('admin/plans/edit_list_planes', $pdata);
    //redirect('admin/list_plans/'); 
    }
	
	function add_plan()
	{
         if(isset($_POST['add_plan'])) 
		 {
        	$data['planName'] = $this->input->post('planName');
        	$data['planFees'] = $this->input->post('planFees');
        	$data['totalVisitors'] = $this->input->post('totalVisitors');
        	$data['validity'] = $this->input->post('validity');
        	$data['linkPerDay'] = $this->input->post('linkPerDay');
        	$data['earningPerClick'] = $this->input->post('earningPerClick');
        	$data['capping'] = $this->input->post('capping');
        	$data['turnover'] = $this->input->post('turnover');
        	//$data['plancode'] = $this->input->post('plancode');
        	$data['paymentLink'] = $this->input->post('paymentLink');
			$data['status'] = $this->input->post('status');
        	$this->session->set_flashdata('message','<div class="alert alert-success">You have been successsully added plan</div>');
        	$this->plan_model->insertData($data);
        	redirect('admin/list_plans/');       	
         }  
		$this->load->view('admin/plans/add_plan');
    }

    public function tax_settings()
	{
		//$this->load->model('admin_model');
		if(isset($_POST['updateTaxSetting']))
		{
			$udata['tds']= $this->input->post('tds');
			$udata['admin_charge']= $this->input->post('admin_charge');
			$udata['tax']= $this->input->post('tax'); 
			$udata['requestwork']= $this->input->post('requestwork');
			$udata['no_transaction_day']= $this->input->post('no_transaction_day');
			$udata['amount_single_transaction']= $this->input->post('amount_single_transaction');
			$udata['no_transaction_month']= $this->input->post('no_transaction_month');
			$udata['promotinalincomme']= $this->input->post('promotinalincomme');
			$udata['walletTitleOne']= $this->input->post('walletTitleOne');
			$udata['walletTitleTwo']= $this->input->post('walletTitleTwo');
			$udata['regmailcontent']= $this->input->post('regmailcontent');
			
			$this->admin_model->updateTaxes(1,$udata);
			redirect('admin/tax_settings/'); 
		}	
		$data['tax'] = $this->admin_model->getAllTaxes(1);
		$this->load->view('admin/manage-tax',$data);
	}
	
	public function workhistory()
	{
		if(isset($_POST['checkWorkHistory']))
		{
			$user_id = $_POST['user_id'];
			$campaign_id = $_POST['campaign_id'];
			$first_date = date('Y-m-d',strtotime($_POST['startDate']));
			$second_date = date('Y-m-d',strtotime($_POST['endDate']));
			$status = $this->input->post('status');
			$workHistoryData = $this->campaign_model->getMyWorkHistoryMis($user_id,$campaign_id,$first_date,$second_date,$status);
		}		
		else
		{
			$workHistoryData = array();	
		}
		$data['workHistoryData'] = $workHistoryData;
		$this->load->view('admin/module/work-history',$data);
	}
	
	public function myteam()
	{
		$args = func_get_args();
		$this->load->model('tree_model');
		$data['login_user'] = $this->user_model->selectUserByID($args[0]);
		$this->load->view('admin/module/my-team',$data);
		
		//$data['workHistoryData'] = $workHistoryData;
		//$this->load->view('admin/module/my-team',$data);
	}
	
	public function mybussiness()
	{
		$args = func_get_args();
		$this->load->model('tree_model');
		$data['login_user'] = $this->user_model->selectUserByID($args[0]);
		$this->load->view('admin/module/my-bussiness',$data);		
	}
		
	function downloadallinvoice()
	{
		$this->load->library('numbertowords');
		$this->load->model('user_model');	
		if(isset($_POST['searchinvoice']))
		{
			if(isset($_POST['userid']) && $_POST['userid']!="")
			{
				$uid =$_POST['userid'];
				$this->db->where('id',$uid);
			}				
		}	
		$data['userData'] = $this->user_model->selectActiveUser();
		$this->load->view('admin/invoicecontent/user-invoice',$data);
	}
	
	function manageallinvoice()
	{
		$this->load->library('numbertowords');
		$this->load->model('user_model');	
		
		$data['userData'] = $this->user_model->selectActiveUser();
		$this->load->view('admin/invoicecontent/manage-user-invoice',$data);
	}
	//user_invoice 
	function managecampaintransaction()
	{
		$args = func_get_args();
		
		if(isset($_POST['submitPlanInv']))
		{		
			//$invdata['invID'] = $this->input->post();
			$invdata['accountHolderName'] = $this->input->post('accountHolderName');
			$invdata['fromAccount'] = $this->input->post('fromAccount');
			$invdata['bankName'] = $this->input->post('bankName');
			$invdata['toBank'] = $this->input->post('toBank');
			$invdata['toAccount'] = $this->input->post('toAccount');
			$invdata['ePoints'] = $this->input->post('ePoints');
			$invdata['amount'] = $this->input->post('amount');
			$invdata['utrNumber'] = $this->input->post('utrNumber');
			$invdata['neftDate'] = $this->input->post('neftDate');					
	        $planInvData = $this->user_model->selectPlanInvByUid($args[0]); 	
			if(count($planInvData)==0)
			{
				$invdata['invTime'] = time();
				$invdata['uid'] = $user_login_id;
				$invdata['status'] = 0; 
				$this->user_model->insertPlanInv($invdata);
				$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>plan invoice has been successfully created.</div>');
				redirect('index.php/admin/managecampaintransaction/'.$args[0]);
			}
			else
			{
				$this->user_model->updatePlanInv($planInvData[0]->id,$invdata);
				$this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>plan invoice has been successfully Updated.</div>');
				redirect('index.php/admin/managecampaintransaction/'.$args[0]);
			}
		}
		
		$data['planInvoice'] = $this->user_model->gettransactiondetails($args[0]);
		$this->load->view('admin/user/edit-campaign-transaction-details',$data);
	}
	
	function alltransactiondetails()
	{
		$args = func_get_args();
		if(isset($_POST['checkTransaction']))
		{
			$first_date = strtotime($_POST['startDate']);
			$second_date = strtotime($_POST['endDate']);
			$this->db->where("invTime BETWEEN $first_date AND $second_date");
		}	
		$data['planInvoice'] = $this->user_model->selectPlanInvByLattest();
		$this->load->view('admin/user/campaign-transaction-listing',$data);
	}
	
	function managepromotionalincome()
	{
		$args = func_get_args();
		if(isset($_POST['searchproincome']))
		{
			if($_POST['user_id']!="")
			{
				$uid = $_POST['user_id'];
				$this->db->where('uid', $uid);
			}	
			
			if($_POST['status']!="all")
			{
				$status = $_POST['status'];
				$this->db->where('status', $status);
			}
			
			if($_POST['startDate'] && $_POST['endDate'])
			{
				$first_date = date('Y-m-d',strtotime($_POST['startDate']));
				$second_date = date('Y-m-d',strtotime($_POST['endDate']));
				$this->db->where('crDate >=', $first_date);
				$this->db->where('crDate <=', $second_date);
			}	
			
		}	
		$data['incomedata'] = $this->user_model->getPromostionalIncomeAdmin();
		$this->load->view('admin/module/promotional_income',$data);
	}
	
	function managepromotionalincome_edit()
	{
		$args = func_get_args();
		if(isset($_POST['updateData']))
		{
			$pdata['transactionID'] = $this->input->post('transactionID');
			$pdata['status'] = $this->input->post('status');
			$this->user_model->updateDataPromotional($args[0],$pdata);
			$this->session->set_flashdata('message','<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Record successfully updated</strong></div>');
			redirect('index.php/admin/managepromotionalincome');
		}
		$data['incomedata'] = $this->user_model->getPromostionalIncomeAdminByID($args[0]);
		$this->load->view('admin/module/promotional_income_edit',$data);
	}
	
	function managewalletstatement()
	{
		$args = func_get_args();
		if(isset($_POST['searchproincome']))
		{
			if($_POST['user_id']!="")
			{
				$uid = $_POST['user_id'];
				$this->db->where('user_id', $uid);
			}			
						
			if($_POST['startDate'] && $_POST['endDate'])
			{
				$first_date = date('Y-m-d',strtotime($_POST['startDate']));
				$second_date = date('Y-m-d',strtotime($_POST['endDate']));
				$this->db->where('trDate >=', $first_date);
				$this->db->where('trDate <=', $second_date);
			}	
			
		}	
		$data['walletData'] = $this->admin_model->selectAllWalletStatement();
		$this->load->view('admin/module/wallet-statement',$data);
	}
	
	
	public function forgotpassword()
	{	
		if(isset($_POST['resetPassword']))
		{
			$userid = $this->input->post('email');
			if(!empty($userid))
			{
				$userdata = $this->admin_model->checkAdminByEmail($userid);
				if(count($userdata)>0)
				{
					$this->load->library('email');
					$this->email->set_newline("\r\n");
					$this->email->set_mailtype("html");
					$this->email->to($userdata[0]->email);
					$this->email->from('info@campaigntrade.com', 'Samridh Bharat');
					$this->email->subject('Forgot Password');					
					// message start				
					$message = "Dear Admin,<br>";
					$message .= "Thank you for registering with us.";
					$message .= "Your account has been created, you can login with the following credentials.<br>";
					$message .= "------------------------<br>";
					$message .= "USER ID: ".$userdata[0]->id." <br>";
					$message .= "PASSWORD: ".base64_decode($userdata[0]->password)." <br>";
					$message .= "------------------------<br><br>";
					$message .= "Thank you <br> Samridh Bharat support team";
					// message end					
					$this->email->message($message);
					$this->email->send();
					$this->session->set_flashdata('message', '<div class="alert alert-success">your login details have been emailed to you</div>');
				}
				else
				{
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error! </strong>Invalid email address</div>');
				}		
			}
			else
			{
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Error! </strong>Invalid email address</div>');	
			}
			redirect('index.php/admin/forgotpassword');
		}	
		$this->load->view('admin/forgot-password');
	}
	
	function managecampaignreport(){
		
		if(isset($_POST['checkComments']))
		{
			$sdate = $this->input->post('startDate');
			$edate = $this->input->post('endDate');
			if(!empty($sdate) && !empty($edate))
			{						
			   //$date_1 = 
				$this->db->where('rdate >=',date('Y-m-d',strtotime($sdate)));
				$this->db->where('rdate <=',date('Y-m-d',strtotime($edate)));
			}	
			if(isset($_POST['user_id']) && !empty($_POST['user_id']))
			{
				$this->db->where('id',$_POST['user_id']);
			}	
		}
    	$data['USER'] = $this->user_model->getAllUsers();         
        $this->load->view('admin/module/campaign-report', $data);
	}

}

