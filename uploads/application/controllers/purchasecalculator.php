<?php 
class Purchasecalculator extends CI_Controller
{
	function listing()
	{
		$this->load->model('purchasecalculator_model');
		$data['bookdata'] = $this->purchasecalculator_model->getAlldata();
		$this->load->view('admin/purchasecalculator/listing',$data);
	}
	
	function add()
	{
		
		$this->load->model('purchasecalculator_model');
		if(isset($_POST['editData']))
		{
			$edata['qty_one']= $this->input->post('qty_one');
			$edata['qty_two']= $this->input->post('qty_two');
			$edata['price']= $this->input->post('price');
			$this->purchasecalculator_model->insertData($edata);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully added.</div>');
			redirect('index.php/purchasecalculator/listing');	
		}	
		
		$this->load->view('admin/purchasecalculator/add');
	}
	function edit()
	{
		$args = func_get_args();
		//echo $args[0];
		$this->load->model('purchasecalculator_model');
		
		if(isset($_POST['editData']))
		{
			$edata['qty_one']= $this->input->post('qty_one');
			$edata['qty_two']= $this->input->post('qty_two');
			$edata['price']= $this->input->post('price');
			$this->purchasecalculator_model->updateData($args[0],$edata);			
			$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully updated.</div>');
			redirect('index.php/purchasecalculator/listing');	
		}	
		$data['bookdata'] = $this->purchasecalculator_model->selectByPurchaseID($args[0]);
		$this->load->view('admin/purchasecalculator/edit',$data);
	}
	
	function deleteRecord()
	{
		$args = func_get_args();
		$this->load->model('purchasecalculator_model');		
		$this->purchasecalculator_model->deleteData($args[0]);			
		$this->session->set_flashdata('message','<div class="alert alert-success">Record has been successfully deleted.</div>');
		redirect('index.php/purchasecalculator/listing');	
	}	
	
}
